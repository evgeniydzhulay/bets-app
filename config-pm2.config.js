module.exports = {
    apps: [
        {
            name: "app-games",
            script: "./websocket/games/games.js",
            error_file: './runtime/logs/node-games-error.log',
            out_file: './runtime/logs/node-games-debug.log',
            combine_logs: true,
            instances: 1,
            autorestart: true,
            NODE_ENV: "production"
        },
        {
            name: "app-support",
            script: "./websocket/support/support.js",
            error_file: './runtime/logs/node-support-error.log',
            out_file: './runtime/logs/node-support-debug.log',
            combine_logs: true,
            instances: 1,
            autorestart: true,
            NODE_ENV: "production"
        },
        {
            name: "api-bet365-prematch",
            script: "yii.php",
            args: [
                "bet365/prematch",
            ],
            interpreter: 'php',
            error_file: './runtime/logs/api-bet365-prematch.log',
            out_file: './runtime/logs/api-bet365-prematch.log',
            combine_logs: true,
            instances: 1,
            autorestart: true,
        },
        {
            name: "api-bet365-live",
            script: "yii.php",
            args: [
                'bet365/live'
            ],
            interpreter: 'php',
            error_file: './runtime/logs/api-bet365-live.log',
            out_file: './runtime/logs/api-bet365-live.log',
            combine_logs: true,
            instances: 1,
            autorestart: true,
        },
        {
            name: "api-1xbet-prematch",
            script: "yii.php",
            args: [
                "1xbet/prematch",
            ],
            interpreter: 'php',
            error_file: './runtime/logs/api-1xbet-prematch.log',
            out_file: './runtime/logs/api-1xbet-prematch.log',
            combine_logs: true,
            instances: 1,
            autorestart: true,
        },
        {
            name: "api-1xbet-inplay",
            script: "yii.php",
            args: [
                "1xbet/inplay",
            ],
            interpreter: 'php',
            error_file: './runtime/logs/api-1xbet-inplay.log',
            out_file: './runtime/logs/api-1xbet-inplay.log',
            combine_logs: true,
            instances: 1,
            autorestart: true,
        },
        {
            name: "api-1xbet-results",
            script: "yii.php",
            args: [
                "1xbet/results",
            ],
            interpreter: 'php',
            error_file: './runtime/logs/api-1xbet-results.log',
            out_file: './runtime/logs/api-1xbet-results.log',
            combine_logs: true,
            instances: 1,
            autorestart: true,
        },
        {
            name: "queue-outcome-ban",
            script: "yii.php",
            args: [
                "event-queue-outcome-ban/listen",
            ],
            interpreter: 'php',
            error_file: './runtime/logs/event-queue-error.log',
            out_file: './runtime/logs/event-queue-info.log',
            combine_logs: true,
            instances: 4,
            autorestart: true,
        },
        {
            name: "queue-outcome-status",
            script: "yii.php",
            args: [
                "event-queue-outcome-status/listen",
            ],
            interpreter: 'php',
            error_file: './runtime/logs/event-queue-error.log',
            out_file: './runtime/logs/event-queue-info.log',
            combine_logs: true,
            instances: 4,
            autorestart: true,
        },
    ]
};
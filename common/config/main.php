<?php

use common\components\BusComponent;
use common\components\CurrenciesComponent;
use common\components\LocationsComponent;
use common\modules\user\components\DbManager;
use creocoder\flysystem\LocalFilesystem;
use kartik\date\DatePicker;
use kartik\datecontrol\Module as DatecontrolModule;
use kartik\grid\Module as GridModule;
use yii\db\Connection;
use yii\helpers\ArrayHelper;
use yii\i18n\PhpMessageSource;
use yii\log\FileTarget;
use yii\redis\Cache;
use yii\redis\Connection as RedisConnection;
use yii\redis\Session;
use yii\swiftmailer\Mailer;

define("BASE_PATH", dirname(dirname(__DIR__)));

Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@frontapi', dirname(dirname(__DIR__)) . '/frontapi');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@backapi', dirname(dirname(__DIR__)) . '/backapi');
Yii::setAlias('@partners', dirname(dirname(__DIR__)) . '/partners');
Yii::setAlias('@runtime', dirname(dirname(__DIR__)) . '/runtime');
Yii::setAlias('@uploads', dirname(dirname(__DIR__)) . '/uploads');

return ArrayHelper::merge([
	'id' => 'common',
	'name' => 'luxe.bet',
	'basePath' => BASE_PATH,
	'vendorPath' => BASE_PATH . '/vendor',
	'language' => 'ru',
	'sourceLanguage' => 'en',
	'runtimePath' => BASE_PATH . '/runtime',
	'bootstrap' => [
		'log',
	],
	'aliases' => [
		'@bower' => '@vendor/bower-asset',
		'@npm' => '@vendor/npm-asset',
		'@web/frontend' => 'http://luxe.bet',
		'@web/frontapi' => '//frontapi.luxe.bet',
		'@web/backend' => '//backend.luxe.bet',
		'@web/backapi' => '//backapi.luxe.bet',
		'@web/partners' => '//partners.luxe.bet',
	],
	'components' => [
		'bus' => BusComponent::class,
		'locations' => LocationsComponent::class,
		'currencies' => CurrenciesComponent::class,
		'log' => [
			'flushInterval' => 1,
			'targets' => [
				'errors' => [
					'class' => FileTarget::class,
					'levels' => ['error', 'warning'],
					'except' => ['yii\web\HttpException:404', 'yii\web\HttpException:403'],
					'logFile' => '@runtime/logs/app-error.log',
					'enableRotation' => false,
					'exportInterval' => 1,
				],
			],
		],
		'view' => [
			'theme' => [
				'basePath' => '@common/views',
				'pathMap' => [
					'@common/modules/user/views' => '@common/views/user',
					'@common/modules/project/views' => '@common/views/project',
					'@common/modules/call/views' => '@common/views/call',
					'@common/modules/lead/views' => '@common/views/lead',
					'@common/modules/report/views' => '@common/views/report',

					'@common/modules/user/views/mail/layouts' => '@common/views/mail/layouts',
					'@common/modules/project/views/mail/layouts' => '@common/views/mail/layouts',
					'@common/modules/call/views/mail/layouts' => '@common/views/mail/layouts',
					'@common/modules/lead/views/mail/layouts' => '@common/views/mail/layouts',
					'@common/modules/report/views/mail/layouts' => '@common/views/mail/layouts',
				],
			],
		],
		'errorHandler' => [
			'memoryReserveSize' => 1,
		],
		'mailer' => [
			'class' => Mailer::class,
			'transport' => [
				'class' => Swift_NullTransport::class,
			],
		],
		'i18n' => [
			'translations' => [
				'eauth' => [
					'class' => PhpMessageSource::class,
					'basePath' => '@eauth/messages',
				],
				'*' => [
					'class' => PhpMessageSource::class,
					'basePath' => '@common/messages',
					'sourceLanguage' => 'en',
					'fileMap' => [
						'app' => '@common/messages/app.php',
					],
				],
				'app*' => [
					'class' => PhpMessageSource::class,
					'basePath' => '@common/messages',
					'sourceLanguage' => 'en',
				],
				'country' => [
					'class' => PhpMessageSource::class,
					'basePath' => '@common/locations',
					'forceTranslation' => true,
					'sourceLanguage' => 'en',
				],
				'timezone' => [
					'class' => PhpMessageSource::class,
					'basePath' => '@common/locations',
					'forceTranslation' => true,
					'sourceLanguage' => 'en',
				],
			],
		],
		'authManager' => [
			'class' => DbManager::class,
			'cache' => 'cache',
		],
		'db' => [
			'class' => Connection::class,
		],
		'storage' => [
			'class' => LocalFilesystem::class,
			'path' => '@uploads/public',
		],
		'redis' => [
			'class' => RedisConnection::class,
			'hostname' => 'localhost',
			'port' => 6379,
		],
		'cache' => [
			'class' => Cache::class,
			'redis' => [
				'class' => RedisConnection::class,
			],
		],
		'session' => [
			'class' => Session::class,
			'redis' => [
				'class' => RedisConnection::class,
			],
		],
		'reCaptcha' => [
			'class' => 'himiklab\yii2\recaptcha\ReCaptchaConfig',
			'siteKeyV3' => '6LdZNKIUAAAAAMuJRrRAgTEEbNIxg3casRUdm3Wn',
			'secretV3' => '6LdZNKIUAAAAAK4MiNzxjT9CnLzmbywauUfvlumP',
		],

	],
	'modules' => [
		'gridview' => [
			'class' => GridModule::class,
		],
		'datecontrol' => [
			'class' => DatecontrolModule::class,
			'displaySettings' => [
				DatecontrolModule::FORMAT_DATE => 'dd-MM-yyyy',
				DatecontrolModule::FORMAT_TIME => 'HH:mm:ss a',
				DatecontrolModule::FORMAT_DATETIME => 'dd-MM-yyyy HH:mm:ss a',
			],
			'saveSettings' => [
				DatecontrolModule::FORMAT_DATE => 'php:U',
				DatecontrolModule::FORMAT_TIME => 'php:U',
				DatecontrolModule::FORMAT_DATETIME => 'php:U',
			],
			'autoWidget' => true,
			'ajaxConversion' => false,
			'autoWidgetSettings' => [
				DatecontrolModule::FORMAT_DATE => ['type' => 2, 'pickerButton' => false, 'pluginOptions' => ['autoclose' => true]], // example
				DatecontrolModule::FORMAT_DATETIME => [],
				DatecontrolModule::FORMAT_TIME => [],
			],
			'widgetSettings' => [
				DatecontrolModule::FORMAT_DATE => [
					'class' => DatePicker::class,
					'options' => [
						'dateFormat' => 'php:d-M-Y',
						'options' => ['class' => 'form-control'],
					],
				],
			],
		],
	],
	'params' => [
		'esportID' => 228,
		'languages' => [
			'ru' => [
				'flag' => 'ru',
				'label' => 'Русский',
			],
			'en' => [
				'flag' => 'gb',
				'label' => 'English',
			],
			'uk' => [
				'flag' => 'ua',
				'label' => 'Українська',
			],
		],
		'contacts' => [
			'email' => 'support@luxe.bet',
			'skype' => 'skype.luxe.bet',
			'telegram' => 'tg.luxe.bet',
		],
		'icon-framework' => \kartik\icons\Icon::FI,
		'adminEmail' => 'admin@luxe.bet',
		'noreplyEmail' => 'no-reply@luxe.bet',
		'supportEmail' => 'support@luxe.bet',
	],
],
	require_once(BASE_PATH . '/common/modules/user/config/main.php'),
	require_once(BASE_PATH . '/common/modules/games/config/main.php'),
	require_once(BASE_PATH . '/common/modules/casino/config/main.php'),
	require_once(BASE_PATH . '/common/modules/partners/config/main.php'),
	require_once(BASE_PATH . '/common/modules/support/config/main.php'),
	require_once(BASE_PATH . '/common/modules/content/config/main.php'),
	require_once(BASE_PATH . '/common/modules/blog/config/main.php'),
	require_once(BASE_PATH . '/common/modules/access/config/main.php'),
	(is_file(__DIR__ . '/main-local.php') ? require_once(__DIR__ . '/main-local.php') : [])
);

<?php
return [
	'components' => [
		'db' => [
			'dsn' => 'mysql:host=127.0.0.1;dbname=callscrm',
			'username' => 'root',
			'password' => '',
			'charset' => 'utf8',
			'enableSchemaCache' => true,
			'schemaCacheDuration' => 120,
			'schemaCache' => 'cache',
		],
	],
];

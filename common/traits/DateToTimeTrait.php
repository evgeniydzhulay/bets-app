<?php

namespace common\traits;

trait DateToTimeTrait {

	public static function getDateIsValid ($date, $format = 'Y-m-d') {
		$d = \DateTime::createFromFormat($format, $date);
		return $d && $d->format($format) === $date;
	}

	public static function getTimeFromDate ($date, $format = 'Y-m-d H:i:s') {
		if (is_null($date)) {
			return null;
		}
		$timeArray = date_parse_from_format($format, $date);
		return mktime($timeArray['hour'], $timeArray['minute'], $timeArray['second'], $timeArray['month'], $timeArray['day'], $timeArray['year']);
	}

}

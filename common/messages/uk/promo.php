<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    'Amount' => 'Сума',
    'Code' => 'Код',
    'Code content' => 'Контент коду',
    'Code prefix' => 'Префікс коду',
    'Count' => 'Кількість',
    'Count of codes' => 'Кількість кодів',
    'Create new promocode' => 'Створити промокод',
    'Created at' => 'Створено в',
    'If empty will be generated automatically' => 'Якщо порожній буде створено автоматично',
    'Is used' => 'Використаний',
    'New promocode' => 'Новий промокод',
    'Promocode has been deleted' => 'Промокод видалений',
    'Promocodes' => 'Промокоди',
    'Promocodes are deleted' => 'Промокоди видалені',
    'Promocodes has been created' => 'Промокоди створені',
    'The requested promocode does not exist' => 'Запитаний промокод не існує',
    'Used at' => 'Використаний в',
    'Used by' => 'Ким використаний',
	'Delete all' => 'Видалити все',
	'Delete all promocodes like this?' => 'Видалити всі аналогічні промокоди?',
	'Search for a user' => 'Знайти користувача',
];

<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yii message/extract' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE: this file must be saved in UTF-8 encoding.
 */
return [
    ': Fill in your personal details to be able to withdraw money! It is mandatory to fill in all fields marked with an asterisk (*). You can use Latin characters only.' => ': Заполните свои личные данные, чтобы иметь возможность снимать деньги! Обязательно заполните все поля, отмеченные звездочкой (*). Вы можете использовать только латинские символы.',
    'A confirmation message has been sent to your new email address' => 'Ссылка для подтверждения была отправлена вам на почту',
    'A message has been sent to your email address. It contains a confirmation link that you must click to complete registration.' => 'Вам было отправлено письмо. Оно содержит ссылку, по которой вы должны перейти, чтобы завершить регистрацию.',
    'A password will be generated automatically if not provided' => 'Если вы хотите, чтобы пароль был сгенерирован, оставьте поле пустым',
    'About us' => 'О нас',
    'Account' => 'Аккаунт',
    'Account details' => 'Аккаунт',
    'Account details have been updated' => 'Аккаунт пользователя был обновлен',
    'Account number' => 'Номер аккаунта',
    'Account password' => 'Пароль аккаунта',
    'Account settings' => 'Настройки аккаунта',
    'Address' => 'Адрес',
    'Affiliate program' => 'Партнерская программа',
    'Already registered? Sign in!' => 'Уже зарегистрированы? Авторизуйтесь!',
    'An error occurred and your password has not been changed. Please try again later.' => 'Произошла ошибка и ваш пароль не был изменен. Пожалуйста, попробуйте позже.',
    'An error occurred processing your request' => 'Во время выполнения запроса произошла ошибка',
    'Application name' => 'Название приложения',
    'Application type' => 'Тип приложения',
    'Are you sure you want to block this user?' => 'Вы уверены, что хотите заблокировать пользователя?',
    'Are you sure you want to confirm this user?' => 'Вы уверены, что хотите подтвердить аккаунт пользователя?',
    'Are you sure you want to recreate password for this user? User will receive email with new password.' => 'Вы уверены что хотите пересоздать пароль для этого пользователя? Пользователь получит новый пароль на почту.',
    'Are you sure you want to unblock this user?' => 'Вы уверены, что хотите разблокировать пользователя?',
    'Assignments' => 'Права пользователя',
    'At least {digits} letters.' => 'Минимум {digits} символов',
    'Awesome, almost there. Now you need to click the confirmation link sent to your new email address' => 'Почти готово! Осталось перейти по ссылке, отправленной на ваш новый почтовый адрес',
    'Back to home page' => 'Вернуться на главную',
    'Bio' => 'О себе',
    'Block' => 'Блокировать',
    'Block status' => 'Блокировка',
    'Blocked at {0, date, MMMM dd, YYYY HH:mm}' => 'Заблокирован  {0, date, dd MMMM, YYYY HH:mm}',
    'Blog' => 'Блог',
    'Bonus' => 'Акции',
    'Bonuses' => 'Бонусы',
    'By email' => 'По почте',
    'By phone number' => 'По телефону',
    'By staying on the website, you agree to the use of these cookies.' => 'Оставаясь на сайте, вы соглашаетесь на использование этих файлов cookie.',
    'Change password' => 'Изменить пароль',
    'City' => 'Город',
    'Comment' => 'Комментарий',
    'Complete password reset on {0}' => 'Смена пароля на сайте {0}',
    'Confirm' => 'Подтвердить',
    'Confirm account on {0}' => 'Активация аккаунта на сайте {0}',
    'Confirm email change on {0}' => 'Смена почтового адреса на сайте {0}',
    'Confirmation' => 'Активация',
    'Confirmation status' => 'Статус',
    'Confirmation time' => 'Время активации',
    'Confirmed' => 'Активирован',
    'Confirmed at {0, date, MMMM dd, YYYY HH:mm}' => 'Активирован {0, date, dd MMMM, YYYY HH:mm}',
    'Contact info' => 'Контактная информация',
    'Contacts' => 'Контакты',
    'Continue' => 'Продолжить',
    'Country' => 'Страна',
    'Create a user account' => 'Создать новый аккаунт',
    'Created at' => 'Создан в',
    'Credentials will be sent to the user by email' => 'Данные для входа будут отправлены пользователю на почту',
    'Currency' => 'Валюта',
    'Current password' => 'Текущий пароль',
    'Current password is not valid' => 'Текущий пароль введен неправильно',
    'Date of birth' => 'День рождения',
    'Deposit' => 'Пополнить счёт',
    'Didn\'t receive confirmation message?' => 'Не пришло письмо?',
    'Disable' => 'Отключить',
    'Don\'t have an account? Sign up!' => 'Нет аккаунта? Зарегистрируйтесь!',
    'Email' => 'Email',
    'Email (public)' => 'Публичный email',
    'Email or ID' => 'Почта или ID',
    'Enable' => 'Включить',
    'Ended at' => 'Закончено в',
    'Enter a valid e-mail address. A confirmation e-mail will be sent to this address. This address will be used in future for communicating with you.' => 'Введите корректный адрес почты. Письмо с подтверждением будет отправлено на этот адрес. Адрес в будущем будет использоваться для связи с вами.',
    'Enter the country of your permanent residence.' => 'Введите страну вашего постоянного проживания',
    'Enter your promocode if you have it.' => 'Введите промокод если он у вас есть',
    'Fast games' => 'Мини игры',
    'Find out more' => 'Узнать больше',
    'Finish' => 'Завершить',
    'Forgot password?' => 'Забыли пароль?',
    'Gambling' => 'Казино',
    'Games' => 'Игры',
    'Go to profile.' => 'Перейти в профиль.',
    'Hello' => 'Здравствуйте',
    'I accept terms and conditions' => 'Я принимаю условия использования',
    'ID' => 'ID',
    'If you cannot click the link, please try pasting the text into your browser' => 'Если вы не можете нажать на ссылку, скопируйте ее и вставьте в адресную строку вашего браузера',
    'If you did not make this request you can ignore this email' => 'P.S. Если вы получили это сообщение по ошибке, просто удалите его',
    'Important' => 'Важно',
    'In developing' => 'В разработке',
    'In order to complete your registration, please click the link below' => 'Чтобы активировать ваш аккаунт, пожалуйста, нажмите на ссылку ниже',
    'In order to complete your request, please click the link below' => 'Чтобы завершить запрос, нажмите на ссылку ниже',
    'Information' => 'Информация',
    'Invalid login or password' => 'Неправильный логин или пароль',
    'Invalid two factor authentication code' => 'Неверный код двухфакторной авторизации',
    'Issue date' => 'Дата выпуска',
    'Leave your phone and we will send you a personal password.' => 'Введите телефон и мы вышлем вам ваш пароль',
    'Live bets' => 'Live ставки',
    'Log In' => 'Войти',
    'Login' => 'Логин',
    'Logout' => 'Выйти',
    'Luxe.bet uses cookies to enhance your website experience.' => 'Luxe.bet использует cookie для улучшения вашего нахождения на  сайте.',
    'MOBILE APP' => 'ПРИЛОЖЕНИЕ',
    'Main account' => 'Баланс',
    'Make a deposit' => 'Поплнить счет',
    'Manage twofactor authentication' => 'Управление двухфакторной аутентификация',
    'Manage users' => 'Управление пользователями',
    'Message' => 'Сообщение',
    'My account' => 'Мой аккаунт',
    'My info' => 'Моя информация',
    'Name' => 'Имя',
    'Need two factor authentication code' => 'Необходим код двухфакторной аутентификации',
    'New email' => 'Новый email',
    'New password' => 'Новый пароль',
    'New user' => 'Создать пользователя',
    'Next' => 'Дальше',
    'Not blocked' => 'Пользователь незаблокирован',
    'One click' => 'В 1 клик',
    'Passport issue date' => 'Дата выпуска паспорта',
    'Passport issuer' => 'Эмитент паспорта',
    'Passport series' => 'Серия паспорта',
    'Password' => 'Пароль',
    'Passwords don\'t match' => 'Пароли не совпадают',
    'Patrynomic' => 'Отчество',
    'Phone' => 'Телефон',
    'Please click the link below to complete your password reset' => 'Чтобы восстановить пароль, нажмите на ссылку ниже',
    'Please save this credentials to login next time, or change account settings.' => 'Пожалуйста, сохраните эти данные для дальнейших авторизаций, или измените настройки аккаунта.',
    'Profile' => 'Профиль',
    'Profile details' => 'Профиль',
    'Profile details have been updated' => 'Профиль пользователя был обновлен',
    'Promocode' => 'Промокод',
    'Purse amount' => 'На счету',
    'Recover your password' => 'Восстановить пароль',
    'Recovery link is invalid or expired. Please try requesting a new one.' => 'Ссылка для смены пароля неправильна или устарела. Пожалуйста, попробуйте запросить новую ссылку.',
    'Recovery message sent' => 'Письмо для сброса пароля было отправлено',
    'Recreate password' => 'Пересоздать пароль',
    'Registration' => 'Регистрация',
    'Registration IP' => 'IP при регистрации',
    'Registration ip' => 'Регистрационный IP',
    'Registration successful!' => 'Регистрация успешна!',
    'Registration time' => 'Время регистрации',
    'Remember me next time' => 'Запомнить меня',
    'Repeat password' => 'Повторите пароль',
    'Request new confirmation message' => 'Повторная отправка инструкций',
    'Reset filter' => 'Сбросить фильтр',
    'Reset your password' => 'Сбросить пароль',
    'Results' => 'Результаты',
    'Results live' => 'Результаты live',
    'SPORTS' => 'Линия',
    'Save' => 'Сохранить',
    'Scan the QrCode with Google Authenticator App, then insert its temporary code on the box and submit.' => 'Просканируйте QR-код приложением Google Authenticator App, затем вставьте временный код в поле и отправьте.',
    'Sign in' => 'Авторизоваться',
    'Sign up' => 'Зарегистрироваться',
    'Someone tries to login to your account at {website}' => 'Кто-то пытается войти в Ваш аккаунт на {website}',
    'Something went wrong and your account has not been confirmed.' => 'Что-то пошло не так, и ваш аккаунт не был активирован.',
    'Statistics' => 'Статистика',
    'Status' => 'Статус',
    'Surname' => 'Фамилия',
    'Target' => 'Направление',
    'Terms and conditions' => 'Пользовательское соглашение',
    'Thank you for signing up on {0}' => 'Спасибо за регистрацию на сайте {0}',
    'Thank you, registration is now complete.' => 'Ваш аккаунт был успешно активирован.',
    'The confirmation link is invalid or expired. Please try requesting a new one.' => 'Ссылка для активации аккаунта неправильна или она устарела. Вы можете запросить новую.',
    'The currency you select will affect your deposit and withdrawal transaction methods. Once set, account currency cannot be changed.' => 'Выбранная вами валюта повлияет на методы поплнения и вывода. Введенную валюту нельзя сменить после регистрации.',
    'The selected bonus cannot be changed since you\'ve been registered.' => 'Выбранный бонус не может быть сменен после регистрации.',
    'There is no user with this email address' => 'Нет пользователя с таким email',
    'There is not enough money in your account' => 'На вашем счету недостаточно денег',
    'This account has already been confirmed' => 'Этот аккаунт уже был активирован',
    'This email address has already been taken' => 'Этот email уже используется',
    'This phone has already been taken' => 'Этот телефон уже используется',
    'Token' => 'Токен',
    'Too many failed attempts. Please wait for 5 minutes' => 'Слишком много неудачных попыток входа. Подождите 5 минут.',
    'Too many failed logins on {0}' => 'Слишком много неудачных попыток входа на {0}',
    'Top-up account' => 'Пополнить счет',
    'Transaction history' => 'История транзакций',
    'Two factor authentication' => 'Двухфакторная аутентификация',
    'Two factor authentication code' => 'Код двухфакторной аутентификации',
    'Two factor authentication has been disabled.' => 'Двухфакторная аутентификация отключена.',
    'Two factor authentication is enabled' => 'Двухфакторная аутентификация включена',
    'Two factor authentication successfully enabled.' => 'Двухфакторная аутентификация успешно включена.',
    'Twofactor authentication is enabled: {enabled}' => 'Двухфакторная аутентификация включена: {enabled}',
    'Type' => 'Тип',
    'Unable to disable Two factor authentication.' => 'Не удалось отключить двухфакторную аутентификацию.',
    'Unblock' => 'Разблокировать',
    'Unconfirmed' => 'Аккаунт неподтвержден',
    'Update' => 'Обновить',
    'Update user account' => 'Обновить аккаунт пользователя',
    'User' => 'Пользователь',
    'User ID' => 'ID пользователя',
    'User deposit was accepted' => 'Поплнение пользователя подтверждено',
    'User has been blocked' => 'Пользователь был блокирован',
    'User has been confirmed' => 'Пользователь был активирован',
    'User has been created' => 'Пользователь был создан',
    'User has been unblocked' => 'Пользователь был разблокирован',
    'User name' => 'Имя пользователя',
    'Users' => 'Пользователи',
    'Verification failed. Please, enter new code.' => 'Проверка не удалась. Пожауйста, введите новый код.',
    'Warning!' => 'Внимание!',
    'We have received a request to change the email address for your account on {0}' => 'Вы запросили смену email на сайте {0}',
    'We have received a request to reset the password for your account on {0}' => 'Вы запросили смену пароля на сайте {0}',
    'Website access' => 'Доступ к сайту',
    'Weekly tournament' => 'Еженедельный турнир',
    'Welcome' => 'Здравствуйте!',
    'Welcome to {0}' => 'Добро пожаловать на {0}',
    'Withdraw funds' => 'Вывести деньги',
    'You can not block your own account' => 'Вы не можете заблокировать свой собственный аккаунт',
    'You can\'t deposit less than {amount}' => 'Пополнение не может быть меньше чем {amount}',
    'You can\'t withdraw less than {amount}' => 'Вы не можете снять меньше чем {amount}',
    'You don\'t have enough money. You can withdraw only {amount}' => 'На счету недостаточно денег. Вы можете снять только {amount}',
    'You must accept terms and conditions' => 'Вы  должны принять условия использования',
    'You need to fill your profile data to request withdrawal.' => 'Вы должны заполнить данные профиля перед выводом средств.',
    'Your account details have been updated' => 'Настройки аккаунта были изменены',
    'Your account has been blocked' => 'Ваш аккаунт был блокирован',
    'Your account has been created and a message with further instructions has been sent to your email' => 'Ваш аккаунт был создан и сообщение с дальнейшими инструкциями отправлено на ваш email',
    'Your account has been created.' => 'Ваш аккаунт был создан.',
    'Your account on {0} has been created' => 'Ваш аккаунт на сайте "{0}" был успешно создан',
    'Your account on {0} has been updated' => 'Ваш аккаун на {0} был обновлен',
    'Your account password have been updated' => 'Пароль вашего аккаунта изменен',
    'Your confirmation token is invalid or expired' => 'Ваша ссылка устарела или является ошибочной',
    'Your name as stated in your passport.' => 'Введите имя как написано в паспорте.',
    'Your password has been changed successfully.' => 'Ваш пароль был успешно изменен.',
    'Your surname as stated in your passport.' => 'Введите фамилию как написано в паспорте.',
    'eSports' => 'Киберспорт',
    'password' => 'Пароль',
    'phone' => 'Телефон',
    'you’ll like us:' => 'мы вам понравимся:',
    'Additional' => 'Дополнительно',
];

<?php

namespace common\composer;

use Composer\Installer\LibraryInstaller;
use Composer\Script\Event;
use Composer\Util\Filesystem;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Assets extends LibraryInstaller {

    /**
     * @param Event $event
     */
    public static function clean($event) {
        $params = $event->getComposer()->getPackage()->getExtra();
        if(empty($params[__METHOD__]) || empty($params[__METHOD__]['clean'])) {
            return;
        }
        $list = $params[__METHOD__]['clean'];
        $skip = !empty($params[__METHOD__]['skip']) ? $params[__METHOD__]['skip'] : [];

        $fs = new Filesystem;
        foreach ($list AS $mainDirectory) {
            echo "Assets dir {$mainDirectory} cleanup started ...\n";
            $assetsDirs = glob($mainDirectory . '/*', GLOB_ONLYDIR);
            foreach ($assetsDirs as $dir) {
                if (in_array(basename($dir), $skip)) {
                    echo "ignored {$dir}\n";
                    continue;
                }
                $fs->removeDirectory($dir);
                echo "Assets dir {$dir} removed\n";
            }
            echo "Assets dir {$mainDirectory} cleaned\n";
        }
    }

}

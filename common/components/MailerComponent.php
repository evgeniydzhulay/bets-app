<?php
/**
 * Created by PhpStorm.
 * User: Jarrus
 * Date: 22.11.2018
 * Time: 9:41
 */

namespace common\components;

use Yii;
use yii\base\Component;
use yii\mail\BaseMailer;

class MailerComponent extends Component {

	/** @var string */
	public $viewPath = '@common/views/mail';

	/** @var string|array Default: `Yii::$app->params['adminEmail']` OR `no-reply@example.com` */
	public $sender;

	/**
	 * @param string $to
	 * @param string $subject
	 * @param string $view
	 * @param array $params
	 *
	 * @return bool
	 */
	protected function sendMessage ($to, $subject, $view, $params = []) {
		/** @var BaseMailer $mailer */
		$mailer = Yii::$app->mailer;
		$mailer->viewPath = $this->viewPath;
		$mailer->getView()->theme = Yii::$app->view->theme;
		if ($this->sender === null) {
			$this->sender = isset(Yii::$app->params['noreplyEmail']) ? Yii::$app->params['noreplyEmail'] : 'no-reply@example.com';
		}
		return $mailer->compose([
			'html' => 'html/' . $view,
			'text' => 'text/' . $view,
		], $params)->setTo($to)->setFrom($this->sender)->setReplyTo([
			Yii::$app->params['supportEmail'],
		])->setSubject($subject)->send();
	}

}
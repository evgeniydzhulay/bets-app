<?php

namespace common\components;

use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\redis\Connection;

/**
 * Class BusComponent
 * @package common\components
 *
 * @property Connection $emitter
 */
class BusComponent extends Component {

	/**
	 * @return Connection
	 * @throws InvalidConfigException
	 */
	public function getEmitter (): Connection {
		return Yii::$app->get('redis');
	}

	/**
	 * @param string $channel
	 * @param $params
	 * @return bool
	 */
	public function send (string $channel, string $type, array $params) {
		if (!$this->emitter) {
			return false;
		}
		try {
			if (is_array($params)) {
				$params = json_encode([
					'type' => $type,
					'params' => $params
				]);
			}
			$this->emitter->publish($channel, $params);
		} catch (\Throwable $e) {
			return false;
		}
		return true;
	}
}
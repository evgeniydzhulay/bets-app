<?php

namespace common\components;

use Yii;
use yii\base\Component;

/**
 * @property-read array $list
 */
class CurrenciesComponent extends Component {

	public function getList() {
		return [
			//'USD' => Yii::t('purse', 'US dollar (USD)'),
			'RUR' => Yii::t('purse', 'Russian ruble (RUR)'),
			//'UAH' => Yii::t('purse', 'Ukrainian hryvnia (UAH)'),
		];
	}
}
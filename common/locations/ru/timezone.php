<?php
return [
	'UTC' => 'Стандартное время',
	'Atlantic' => 'Атлантика',
	'Europe' => 'Европа',
	'Asia' => 'Азия',
	'America' => 'Америка',
	'Pacific' => 'Тихий океан',
];
<?php

namespace common\modules\casino;

use Yii;
use yii\base\Module as BaseModule;

class Module extends BaseModule {

	public function getGamesList() {
		return [
			'roulette' => [
				'name' => 'Roulette',
				'categories' => [],
			],
			'rock-paper-scissors' => [
				'name' => Yii::t('casino.rps', 'Rock Paper Scissors'),
				'categories' => [],
			],
			'heads-or-tails' => [
				'name' => Yii::t('casino.hot', 'Heads or Tails'),
				'categories' => [],
			],
			'lucky-card' => [
				'name' => Yii::t('casino.luckycard','Lucky card'),
				'categories' => [],
			],
			'thimbles' => [
				'name' => Yii::t('casino.thimbles','Thimbles'),
				'categories' => [],
			],
		];
	}
}
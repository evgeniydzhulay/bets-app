<?php

namespace common\modules\casino\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "game".
 *
 * @property int $id
 * @property int $user_id
 * @property int $bet_type
 * @property int $user_bet_type
 * @property int $amount_bet
 * @property int $amount_win
 * @property int $status
 * @property int $game_type
 * @property int $created_at
 */
class ThimblesModel extends \yii\db\ActiveRecord {

	const STATUS_VICTORY = 1;
	const STATUS_DEFEAT = 2;

	const ONE_BALL_RATE = 3;
	const TWO_BALLS_RATE = 1.5;

	const GAME_TYPE_ONE_BALL = 1;
	const GAME_TYPE_TWO_BALLS = 2;

	const BET_TYPE_LEFT = 1;
	const BET_TYPE_CENTER = 2;
	const BET_TYPE_RIGHT = 3;

	const MIN_AMOUNT = 5;
	const MAX_AMOUNT = 1000;

	public static function tableName () {
		return '{{%casino_thimbles}}';
	}

	/** {@inheritdoc} */
	public function behaviors () {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['user_id', 'bet_type', 'user_bet_type', 'status', 'game_type'], 'required'],
			[['user_id', 'bet_type', 'user_bet_type', 'status', 'game_type'], 'integer'],
			['amount_win', 'number', 'min' => 0],
			['amount_bet', 'number', 'min' => self::MIN_AMOUNT],
			[['bet_type', 'user_bet_type'], 'in', 'range' => array_keys(self::getBetTypes())],
			[['game_type'], 'in', 'range' => [1, 2]],
		];
	}

	public static function getBetTypes () {
		return [
			self::BET_TYPE_LEFT => Yii::t('casino.thimbles','Left'),
			self::BET_TYPE_CENTER => Yii::t('casino.thimbles','Center'),
			self::BET_TYPE_RIGHT => Yii::t('casino.thimbles', 'Right'),
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('casino', 'ID'),
			'user_id' => Yii::t('casino', 'User'),
			'bet_type' => Yii::t('casino', 'Bet type'),
			'user_bet_type' => Yii::t('casino', 'User bet type'),
			'amount_bet' => Yii::t('casino', 'Bet amount'),
			'amount_win' => Yii::t('casino', 'Victory amount'),
			'status' => Yii::t('casino', 'Status'),
			'created_at' => Yii::t('casino', 'Created at'),
		];
	}

	public function play () {
		if(!is_null($this->status)) {
			return true;
		}
		$this->bet_type = random_int(1, 3);
		if ($this->game_type == self::GAME_TYPE_ONE_BALL) {
			/* Game type: one Ball */
			if ($this->user_bet_type == $this->bet_type) {
				/* win */
				$this->status = self::STATUS_VICTORY;
				$this->amount_win = $this->amount_bet * self::ONE_BALL_RATE;
			} else {
				/* lose */
				$this->status = self::STATUS_DEFEAT;
				$this->amount_win = 0;
			}
		} else {
			/* Game type: two balls */
			if ($this->user_bet_type != $this->bet_type) {
				/* win */
				$this->status = self::STATUS_VICTORY;
				$this->amount_win = $this->amount_bet * self::TWO_BALLS_RATE;
			} else {
				/* lose */
				$this->status = self::STATUS_DEFEAT;
				$this->amount_win = 0;
			}
		}
		return $this->save();
	}

}

<?php

namespace common\modules\casino\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "game".
 *
 * @property int $id
 * @property int $user_id
 * @property int $bet_type
 * @property int $user_bet_type
 * @property int $amount_bet
 * @property int $amount_win
 * @property int $status
 * @property int $created_at
 */
class RockPaperScissorsModel extends \yii\db\ActiveRecord {

	const STATUS_VICTORY = 1;
	const STATUS_DEFEAT  = 2;
	const STATUS_DRAW    = 3;

	const GAME_VICTORY_RATE = 2;
	const GAME_DRAW_RATE    = 0.9;

	const BET_TYPE_ROCK     = 1;
	const BET_TYPE_PAPER    = 2;
	const BET_TYPE_SCISSORS = 3;

	const MIN_AMOUNT = 5;
	const MAX_AMOUNT = 1000;

	public static function tableName () {
		return '{{%casino_rock_paper_scissors}}';
	}

	/** {@inheritdoc} */
	public function behaviors () {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['user_id', 'bet_type', 'user_bet_type', 'status'], 'required'],
			[['user_id', 'bet_type', 'user_bet_type', 'status'], 'integer'],
			['amount_win', 'number', 'min' => 0],
			['amount_bet', 'number', 'min' => self::MIN_AMOUNT],
			[['bet_type', 'user_bet_type'], 'in', 'range' => array_keys(self::getBetTypes())],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('casino', 'ID'),
			'user_id' => Yii::t('casino', 'User'),
			'bet_type' => Yii::t('casino', 'Bet type'),
			'user_bet_type' => Yii::t('casino', 'User bet type'),
			'amount_bet' => Yii::t('casino', 'Bet amount'),
			'amount_win' => Yii::t('casino', 'Victory amount'),
			'status' => Yii::t('casino', 'Status'),
			'created_at' => Yii::t('casino', 'Created at'),
		];
	}

	public function play() {
		if(!is_null($this->status)) {
			return true;
		}
		$this->bet_type = random_int(1, 3);
		if($this->bet_type == $this->user_bet_type){
			$this->status = self::STATUS_DRAW;
			$this->amount_win = $this->amount_bet * self::GAME_DRAW_RATE;
		} else if (
			($this->user_bet_type == self::BET_TYPE_ROCK && $this->bet_type == self::BET_TYPE_SCISSORS) || // rock vs scissor
			($this->user_bet_type == self::BET_TYPE_PAPER && $this->bet_type == self::BET_TYPE_ROCK) || // paper vs rock
			($this->user_bet_type == self::BET_TYPE_SCISSORS && $this->bet_type == self::BET_TYPE_PAPER) // scissor vs paper
		) {
			$this->status = self::STATUS_VICTORY;
			$this->amount_win = $this->amount_bet * self::GAME_VICTORY_RATE;
		} else {
			$this->status = self::STATUS_DEFEAT;
			$this->amount_win = 0;
		}
		return $this->save();
	}

	public static function getBetTypes() {
		return [
			self::BET_TYPE_ROCK => Yii::t('casino.rps','Rock'),
			self::BET_TYPE_PAPER => Yii::t('casino.rps','Paper'),
			self::BET_TYPE_SCISSORS => Yii::t('casino.rps','Scissors'),
		];
	}

}

<?php

namespace common\modules\casino\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "game".
 *
 * @property int $id
 * @property int $user_id
 * @property int $bet_type
 * @property int $user_bet_type
 * @property int $amount_bet
 * @property int $amount_win
 * @property int $status
 * @property int $created_at
 */
class LuckyCardModel extends \yii\db\ActiveRecord {

	const STATUS_VICTORY = 1;
	const STATUS_DEFEAT = 2;

	const GAME_COLOR_RATE = 2; // type 1,2
	const GAME_SUITE_RATE = 4; // type 3,4,5,6

	const BET_TYPE_BLACK = 1; // color black
	const BET_TYPE_RED = 2; // color red

	const BET_TYPE_SPADES = 3; // Пики
	const BET_TYPE_CLUBS = 4; // Трефы
	const BET_TYPE_HEARTS = 5; // Червы
	const BET_TYPE_DIAMONDS = 6; // Бубны

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	const MIN_AMOUNT = 5;
	const MAX_AMOUNT = 1000;

	public static function tableName () {
		return '{{%casino_lucky_card}}';
	}

	/** {@inheritdoc} */
	public function behaviors () {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['user_id', 'bet_type', 'user_bet_type', 'status'], 'required'],
			[['user_id', 'bet_type', 'user_bet_type', 'status'], 'integer'],
			['amount_win', 'number', 'min' => 0],
			['amount_bet', 'number', 'min' => self::MIN_AMOUNT],
			[['bet_type', 'user_bet_type'], 'in', 'range' => array_keys(self::getBetTypes())],
		];
	}

	public static function getBetTypes () {
		return [
			self::BET_TYPE_BLACK => 'Color - Black',
			self::BET_TYPE_SPADES => 'Suit - Spades',
			self::BET_TYPE_CLUBS => 'Suit - Clubs',
			self::BET_TYPE_RED => 'Color - Red',
			self::BET_TYPE_DIAMONDS => 'Suit - Diamonds',
			self::BET_TYPE_HEARTS => 'Suit - Hearts',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('casino', 'ID'),
			'user_id' => Yii::t('casino', 'User'),
			'bet_type' => Yii::t('casino', 'Bet type'),
			'user_bet_type' => Yii::t('casino', 'User bet type'),
			'amount_bet' => Yii::t('casino', 'Bet amount'),
			'amount_win' => Yii::t('casino', 'Victory amount'),
			'status' => Yii::t('casino', 'Status'),
			'created_at' => Yii::t('casino', 'Created at'),
		];
	}

	public function play () {
		if(!is_null($this->status)) {
			return true;
		}
		switch ($this->user_bet_type) {
			case self::BET_TYPE_RED:
			case self::BET_TYPE_BLACK:
				$this->bet_type = random_int(1, 2);
				break;
			default:
				$this->bet_type = random_int(3, 6);
		}
		$this->status = ($this->bet_type === $this->user_bet_type) ? self::STATUS_VICTORY : self::STATUS_DEFEAT;
		if ($this->status == self::STATUS_VICTORY && in_array($this->user_bet_type, [self::BET_TYPE_RED, self::BET_TYPE_BLACK])) {
			$this->amount_win = $this->amount_bet * self::GAME_COLOR_RATE; // x2
		} else if ($this->status == self::STATUS_VICTORY && in_array($this->user_bet_type, [3, 4, 5, 6])) {
			$this->amount_win = $this->amount_bet * self::GAME_SUITE_RATE; // x4
		} else {
			$this->amount_win = 0;
		}
		return $this->save();
	}

}

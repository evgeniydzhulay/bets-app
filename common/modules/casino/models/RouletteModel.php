<?php


namespace common\modules\casino\models;

use common\modules\games\models\BetsModel;
use yii\base\Model;

/**
 * Class RouletteModel
 * @package common\modules\casino\models
 *
 * @property int $user_id
 * @property int $status
 * @property int $bet_result
 * @property int $amount_win
 *
 * @property-read BetsModel $bet
 * @property-write array $bets
 */
class RouletteModel extends Model {

	public function play() {
		return true;
	}
}
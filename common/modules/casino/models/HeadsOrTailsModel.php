<?php

namespace common\modules\casino\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "game".
 *
 * @property int $id
 * @property int $user_id
 * @property int $bet_type
 * @property int $user_bet_type
 * @property int $amount_bet
 * @property int $amount_win
 * @property int $status
 * @property int $created_at
 */
class HeadsOrTailsModel extends \yii\db\ActiveRecord {

	const STATUS_VICTORY = 1;
	const STATUS_DEFEAT = 2;

	const BET_TYPE_HEADS = 1; // Орел
	const BET_TYPE_TAILS = 2; // Решка

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	const GAME_RATE = 2;

	const MIN_AMOUNT = 5;
	const MAX_AMOUNT = 1000;

	public static function tableName () {
		return '{{%casino_heads_or_tails}}';
	}

	/** {@inheritdoc} */
	public function behaviors () {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['user_id', 'bet_type', 'user_bet_type', 'status'], 'required'],
			[['user_id', 'bet_type', 'user_bet_type', 'status'], 'integer'],
			['amount_win', 'number', 'min' => 0],
			['amount_bet', 'number', 'min' => self::MIN_AMOUNT],
			[['bet_type', 'user_bet_type'], 'in', 'range' => array_keys(self::getBetTypes())],
		];
	}

	public static function getBetTypes () {
		return [
			self::BET_TYPE_HEADS => Yii::t('casino.hot', 'Eagle'),
			self::BET_TYPE_TAILS => Yii::t('casino.hot', 'Tails'),
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('casino', 'ID'),
			'user_id' => Yii::t('casino', 'User'),
			'bet_type' => Yii::t('casino', 'Bet type'),
			'user_bet_type' => Yii::t('casino', 'User bet type'),
			'amount_bet' => Yii::t('casino', 'Bet amount'),
			'amount_win' => Yii::t('casino', 'Victory amount'),
			'status' => Yii::t('casino', 'Status'),
			'created_at' => Yii::t('casino', 'Created at'),
		];
	}

	public function play () {
		if(!is_null($this->status)) {
			return true;
		}
		$this->bet_type = random_int(1, 2);
		$this->status = ($this->bet_type === $this->user_bet_type) ? self::STATUS_VICTORY : self::STATUS_DEFEAT;
		$this->amount_win = ($this->status == self::STATUS_VICTORY) ? $this->amount_bet * self::GAME_RATE : 0;
		return $this->save();
	}

}

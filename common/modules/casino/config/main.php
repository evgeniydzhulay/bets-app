<?php

use common\modules\casino\Module;

return [
	'modules' => [
		'casino' => [
			'class' => Module::class,
		],
	],
];
<?php

return [
	'controllerMap' => [
		'migrate' => [
			'migrationNamespaces' => [
				'common\modules\casino\migrations',
			],
		],
	],
];

<?php

namespace common\modules\casino\migrations;

use yii\db\Migration;
use Yii;

/**
 * Class m191003_121814_thimbles_init
 */
class m191003_121814_thimbles_init extends Migration {

	public function up () {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%casino_thimbles}}', [
			'id' => $this->primaryKey(),
			'user_id' => $this->integer()->unsigned()->notNull(),
			'game_type' => $this->integer(1)->unsigned()->notNull(),
			'bet_type' => $this->integer()->unsigned()->notNull(),
			'user_bet_type' => $this->integer()->unsigned()->notNull(),
			'amount_bet' => $this->float(2)->notNull(),
			'amount_win' => $this->float(2),
			'status' => $this->integer(1)->unsigned()->notNull(),
			'created_at' => $this->integer()->unsigned()->notNull(),
		], $tableOptions);
		$this->createIndex('idx-casino_thimbles-user', '{{%casino_thimbles}}', 'user_id');

	}

	public function down () {
		$this->dropTable('{{%casino_thimbles}}');
	}
}

<?php

namespace common\modules\casino\components;

use common\modules\games\models\BetsModel;
use yii\base\Component;
use yii\web\NotFoundHttpException;

class BetsComponent extends Component {

	/**
	 * @param int $uid
	 * @param float $bet
	 * @param float $win
	 * @param string $game
	 * @return bool|BetsModel
	 */
	public function place(int $uid, float $bet, float $win, string $game) {
		$model = new BetsModel();
		$model->scenario = BetsModel::SCENARIO_CREATE;
		$model->setAttributes([
			'user_id' => $uid,
			'amount_bet' => round($bet, 5),
			'amount_win' => round($win, 5),
			'status' => BetsModel::STATUS_ACTIVE,
		]);
		if($model->insertBet([])) {
			return $model;
		}
		return false;
	}

	/**
	 * @param BetsModel $bet Bet
	 * @param float|null $amount
	 * @return bool
	 */
	public function victory(BetsModel $bet, float $amount = null) {
		return $this->updateVictoryAmount($bet, $amount) && $bet->victory();
	}

	/**
	 * @param BetsModel $bet Bet
	 * @param float|null $amount
	 * @return bool
	 */
	public function defeat(BetsModel $bet, float $amount = null) {
		return $this->updateVictoryAmount($bet, $amount) && $bet->defeat();
	}

	/**
	 * @param BetsModel $bet Bet
	 * @param float|null $amount
	 * @return bool
	 */
	public function revert(BetsModel $bet, float $amount = null) {
		return $this->updateVictoryAmount($bet, $amount) && $bet->revert();
	}

	/**
	 * @param BetsModel $bet
	 * @param float|null $amount
	 * @return bool
	 */
	protected function updateVictoryAmount(BetsModel $bet, float $amount = null) {
		if(!is_null($amount) && $bet->amount_win != $amount) {
			$bet->scenario = BetsModel::SCENARIO_UPDATE;
			$bet->amount_win = $amount;
			return $bet->save(true, ['amount_win']);
		}
		return true;
	}

	/**
	 * @param $params
	 * @return BetsModel
	 */
	protected function getBet($params) {
		$bet = BetsModel::findOne($params);
		if(!$bet) {
			throw new NotFoundHttpException();
		}
		return $bet;
	}
}
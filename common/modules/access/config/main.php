<?php

use common\modules\access\Module;

return [
	'modules' => [
		'access' => [
			'class' => Module::class,
		],
	],
];
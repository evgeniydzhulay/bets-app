<?php

namespace common\modules\access\controllers;

use Yii;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;

class FilesController extends Controller {

	public function actionSupport($file) {
		return $this->_allowAccess(Yii::$app->getModule('supportStorage')->getUrl($file, true));
	}

	protected function _denyAccess($file) {
		throw new ForbiddenHttpException("You cannot access {$file}. How you got here?");
	}

	protected function _allowAccess($file, $name = NULL) {
		return Yii::$app->response->xSendFile($file, $name, [
			'xHeader' => 'X-Accel-Redirect'
		])->send();
	}
}
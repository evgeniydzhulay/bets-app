<?php

namespace common\modules\games\models;

use Yii;

/**
 * This is the model class for table "event_game_promo".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $game_id
 * @property int $outcome_1st_id
 * @property int $outcome_2nd_id
 *
 * @property EventGame $game
 * @property EventOutcomes $outcome1st
 * @property EventOutcomes $outcome2nd
 * @property GameSports $sports
 */
class EventGamePromoModel extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'event_game_promo';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sports_id', 'game_id', 'outcome_1st_id', 'outcome_2nd_id'], 'integer'],
            [['sports_id', 'game_id', 'outcome_1st_id', 'outcome_2nd_id'], 'required'],
            [['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventGameModel::class, 'targetAttribute' => ['game_id' => 'id']],
            [['outcome_1st_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventOutcomesModel::class, 'targetAttribute' => ['outcome_1st_id' => 'id']],
            [['outcome_2nd_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventOutcomesModel::class, 'targetAttribute' => ['outcome_2nd_id' => 'id']],
            [['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('games', 'ID'),
            'sports_id' => Yii::t('games', 'Sports ID'),
            'game_id' => Yii::t('games', 'Game ID'),
            'outcome_1st_id' => Yii::t('games', 'Outcome 1st ID'),
            'outcome_2nd_id' => Yii::t('games', 'Outcome 2nd ID'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGame()
    {
        return $this->hasOne(EventGame::className(), ['id' => 'game_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutcome1st()
    {
        return $this->hasOne(EventOutcomes::className(), ['id' => 'outcome_1st_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutcome2nd()
    {
        return $this->hasOne(EventOutcomes::className(), ['id' => 'outcome_2nd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSports()
    {
        return $this->hasOne(GameSports::className(), ['id' => 'sports_id']);
    }
}

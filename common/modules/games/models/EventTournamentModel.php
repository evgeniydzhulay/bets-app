<?php

namespace common\modules\games\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%event_tournament}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $starts_at
 * @property string $flag
 * @property int $sort_order
 * @property int $is_enabled
 * @property int $is_banned [int(1)]
 * @property int $is_hidden [int(1)]
 * @property int $is_finished [int(1)]
 *
 * @property EventGameModel[] $eventGames
 * @property EventGameLocaleModel[] $eventGameLocales
 * @property EventOutcomesModel[] $eventOutcomes
 * @property EventParticipantsModel[] $eventParticipants
 * @property GameSportsModel $sports
 * @property GameSportsLocaleModel[] $gameSportsLocales
 * @property EventTournamentLocaleModel[] $eventTournamentLocales
 */
class EventTournamentModel extends \yii\db\ActiveRecord
{
	const SCENARIO_CREATE         = 'create';
	const SCENARIO_UPDATE         = 'update';
	const SCENARIO_UPDATE_BACKEND = 'update_backend';
	const SCENARIO_SEARCH         = 'search';

	const STATUS_ENABLED  = 1;
	const STATUS_DISABLED = 0;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%event_tournament}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\EventTournamentQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\EventTournamentQuery(get_called_class());
	}

	/** @inheritdoc */
	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['sports_id', 'starts_at', 'flag', 'sort_order', 'is_enabled','is_hidden'],
			self::SCENARIO_UPDATE => ['starts_at', 'flag', 'sort_order', 'is_enabled','is_finished','is_banned','is_hidden'],
			self::SCENARIO_SEARCH => ['id', 'sports_id', 'starts_at', 'flag', 'sort_order', 'is_enabled','is_finished','is_hidden','is_banned'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['sports_id', 'sort_order', 'is_enabled','is_finished','is_hidden','is_banned'], 'integer', 'on' => [
				self::SCENARIO_UPDATE,
				self::SCENARIO_CREATE,
			]],
			/*[['starts_at'], 'integer', 'on' => [
				self::SCENARIO_UPDATE,
				self::SCENARIO_CREATE,
			]],*/
			['starts_at', 'datetime', 'format' => 'php:d/m/Y H:i', 'timestampAttribute' => 'starts_at', 'on' => [self::SCENARIO_UPDATE, self::SCENARIO_CREATE]],
			[['flag'],'string','max' => 5],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class,
				'targetAttribute' => ['sports_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['id', 'sports_id', 'starts_at', 'sort_order', 'is_enabled','is_finished','is_hidden','is_banned'], 'safe', 'on' => [
				self::SCENARIO_SEARCH,
			]]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'starts_at' => Yii::t('games', 'Starts at'),
			'flag' => Yii::t('games', 'Code flag'),
			'sort_order' => Yii::t('games', 'Sort order'),
			'is_enabled' => Yii::t('games', 'Is enabled'),
			'is_hidden' => Yii::t('games', 'Is hidden'),
			'is_banned' => Yii::t('games', 'Is banned'),
			'is_finished' => Yii::t('games', 'Is finished')
		];
	}

	public function search($params) {
		$query = static::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
		]);
		$this->load($params);
		if(!$this->validate()){
			return $dataProvider;
		}
		$query->andFilterWhere([
			'id' => $this->id,
			'sports_id' => $this->sports_id,
			'starts_at' => $this->starts_at,
			'sort_order' => $this->sort_order,
			'is_enabled' => $this->is_enabled,
			'is_hidden' => $this->is_hidden,
			'is_banned' => $this->is_banned,
			'is_finished' => $this->is_finished
		]);

		return $dataProvider;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventGames() {
		return $this->hasMany(EventGameModel::class, ['tournament_id' => 'id'])->inverseOf('tournament');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventGameLocales() {
		return $this->hasMany(EventGameLocaleModel::class, ['tournament_id' => 'id'])->inverseOf('tournament');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventOutcomes() {
		return $this->hasMany(EventOutcomesModel::class, ['tournament_id' => 'id'])->inverseOf('tournament');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventParticipants() {
		return $this->hasMany(EventParticipantsModel::class, ['tournament_id' => 'id'])->inverseOf('tournament');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports() {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameSportsLocales() {
		return $this->hasMany(GameSportsLocaleModel::class, ['sports_id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventTournamentLocales() {
		return $this->hasMany(EventTournamentLocaleModel::class, ['tournament_id' => 'id'])->inverseOf('tournament');
	}

	public function ban($message) {
		foreach($this->eventGames AS $game) {
			$game->ban($message);
		}
		$this->is_banned = true;
		return $this->save(false, ['is_banned']);
	}

	/**
	 * @return array
	 */
	static public function statuses(){
		return [
			self::STATUS_ENABLED => Yii::t('games', 'Enabled'),
			self::STATUS_DISABLED => Yii::t('games', 'Disabled'),
		];
	}
}

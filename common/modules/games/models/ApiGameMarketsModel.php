<?php

namespace common\modules\games\models;

use Yii;

/**
 * This is the model class for table "{{%api_game_markets}}".
 *
 * @property int $id
 * @property int $market_id
 * @property int $service_api
 * @property string $code_api
 *
 * @property GameMarketsModel $market
 */
class ApiGameMarketsModel extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%api_game_markets}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['market_id', 'service_api', 'code_api','sports_id'], 'required'],
			[['market_id','group_id', 'service_api', 'sports_id'], 'integer'],
			[['code_api'], 'string', 'max' => 100],
			[['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsModel::class, 'targetAttribute' => ['market_id' => 'id']],
			[['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsGroupsModel::class, 'targetAttribute' => ['group_id' => 'id']],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'market_id' => 'Market ID',
			'group_id' => 'Group ID',
			'sports_id' => Yii::t('games', 'Sport'),
			'service_api' => 'Service Api',
			'code_api' => 'Code Api',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMarket() {
		return $this->hasOne(GameMarketsModel::class, ['id' => 'market_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroup() {
		return $this->hasOne(GameMarketsGroupsModel::class, ['id' => 'group_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\ApiGameMarketsQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\ApiGameMarketsQuery(get_called_class());
	}
}

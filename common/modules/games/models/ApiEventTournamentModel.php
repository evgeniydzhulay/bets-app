<?php

namespace common\modules\games\models;

use Yii;

/**
 * This is the model class for table "{{%api_event_tournament}}".
 *
 * @property int $id
 * @property int $tournament_id
 * @property int $service_api
 * @property string $code_api
 *
 * @property EventTournamentModel $tournament
 */
class ApiEventTournamentModel extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%api_event_tournament}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['tournament_id', 'service_api', 'code_api'], 'required'],
			[['tournament_id', 'service_api'], 'integer'],
			[['code_api'], 'string', 'max' => 100],
			[['tournament_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventTournamentModel::class, 'targetAttribute' => ['tournament_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'tournament_id' => 'Tournament ID',
			'service_api' => 'Service Api',
			'code_api' => 'Code Api',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTournament() {
		return $this->hasOne(EventTournamentModel::class, ['id' => 'tournament_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\ApiEventTournamentQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\ApiEventTournamentQuery(get_called_class());
	}
}

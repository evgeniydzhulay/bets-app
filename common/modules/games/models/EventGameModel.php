<?php

namespace common\modules\games\models;

use common\modules\games\Module;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%event_game}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $tournament_id
 * @property int $starts_at
 * @property int $sort_order
 * @property int $is_enabled
 * @property int $is_banned [int(1)]
 * @property int $is_hidden [int(1)]
 * @property int $is_finished [int(1)]
 * @property int $is_resulted [int(1)]
 * @property int $is_live [int(1) unsigned]
 * @property string $video_link
 * @property int $outcomes_count [int(11)]
 * @property string $result
 *
 * @property-read GameSportsModel $sports
 * @property-read EventTournamentModel $tournament
 * @property-read EventGameLocaleModel[] $eventGameLocales
 * @property-read EventOutcomesModel[] $eventOutcomes
 * @property-read EventTournamentLocaleModel[] $tournamentLocales
 * @property-read GameSportsLocaleModel[] $sportsLocales
 * @property-read GameMarketsModel[] $markets
 * @property-read GameMarketsGroupsModel[] $marketsGroups
 * @property-read EventParticipantsModel[] $eventParticipants
 */
class EventGameModel extends \yii\db\ActiveRecord
{

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const STATUS_ENABLED  = 1;
	const STATUS_DISABLED = 0;

	private $timer = null;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%event_game}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\EventGameQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\EventGameQuery(get_called_class());
	}

	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['sports_id', 'tournament_id', 'starts_at', 'sort_order', 'is_enabled', 'is_hidden','video_link','result','is_live'],
			self::SCENARIO_UPDATE => ['sort_order', 'is_enabled', 'is_hidden', 'is_banned', 'is_finished', 'starts_at','video_link','result'],
			self::SCENARIO_SEARCH => ['id', 'sports_id', 'tournament_id', 'starts_at', 'sort_order', 'is_enabled', 'is_hidden', 'is_banned', 'is_finished', 'is_resulted','video_link'],
		];
	}

	public function rules() {
		return [
			[['sports_id', 'tournament_id', /*'starts_at',*/
				'sort_order', 'is_enabled', 'is_hidden'], 'integer', 'on' => [
				self::SCENARIO_UPDATE,
				self::SCENARIO_CREATE
			]],
			[['is_banned', 'is_finished'], 'integer', 'on' => [
				self::SCENARIO_UPDATE,
			]],
			[['video_link'],'string','max' => 255],
			['starts_at', 'datetime', 'format' => 'php:d/m/Y H:i', 'timestampAttribute' => 'starts_at', 'on' => [self::SCENARIO_UPDATE, self::SCENARIO_CREATE]],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class,
				'targetAttribute' => ['sports_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['tournament_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventTournamentModel::class,
				'targetAttribute' => ['tournament_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['id', 'sports_id', 'tournament_id', 'starts_at', 'sort_order', 'is_enabled'], 'safe', 'on' => [
				self::SCENARIO_SEARCH,
			]],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'tournament_id' => Yii::t('games', 'Tournament'),
			'starts_at' => Yii::t('games', 'Starts at'),
			'sort_order' => Yii::t('games', 'Sort order'),
			'is_enabled' => Yii::t('games', 'Is enabled'),
			'is_hidden' => Yii::t('games', 'Is hidden'),
			'is_banned' => Yii::t('games', 'Is banned'),
			'is_finished' => Yii::t('games', 'Is finished'),
			'is_resulted' => Yii::t('games', 'Is resulted'),
			'video_link' => Yii::t('games', 'Video link'),
			'result' => Yii::t('games', 'Result'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports() {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSportsLocales() {
		return $this->hasMany(GameSportsLocaleModel::class, ['sports_id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTournament() {
		return $this->hasOne(EventTournamentModel::class, ['id' => 'tournament_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTournamentLocales() {
		return $this->hasMany(EventTournamentLocaleModel::class, ['tournament_id' => 'tournament_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMarketsGroups() {
		return $this->hasMany(GameMarketsGroupsModel::class, ['sports_id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMarkets() {
		return $this->hasMany(GameMarketsModel::class, ['sports_id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getResult() {
		return $this->hasOne(EventGameResultModel::class, ['game_id' => 'id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventGameLocales() {
		return $this->hasMany(EventGameLocaleModel::class, ['game_id' => 'id'])->inverseOf('game');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventOutcomes() {
		return $this->hasMany(EventOutcomesModel::class, ['game_id' => 'id'])->inverseOf('game');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventParticipants() {
		return $this->hasMany(EventParticipantsModel::class, ['game_id' => 'id'])->inverseOf('game');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventGamePromo() {
		return $this->hasOne(EventGamePromoModel::class, ['game_id' => 'id']);
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = static::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);
		$this->load($params);
		if(!$this->validate()){
			return $dataProvider;
		}
		$query->andFilterWhere([
			'id' => $this->id,
			'sports_id' => $this->sports_id,
			'event_game.tournament_id' => $this->tournament_id,
			'starts_at' => $this->starts_at,
			'sort_order' => $this->sort_order,
			'is_enabled' => $this->is_enabled,
			'is_hidden' => $this->is_hidden,
			'is_banned' => $this->is_banned,
			'is_finished' => $this->is_finished,
			'is_resulted' => $this->is_resulted
		]);
		return $dataProvider;
	}

	public function ban($message) {
		$this->is_banned = true;
		if($this->save(false, ['is_banned'])){
			foreach($this->eventOutcomes AS $outcome){
				$outcome->ban($message);
			}
			return true;
		}
		return false;
	}

	/**
	 * @return array
	 */
	static public function statuses() {
		return [
			self::STATUS_ENABLED => Yii::t('games', 'Enabled'),
			self::STATUS_DISABLED => Yii::t('games', 'Disabled'),
		];
	}

	public function afterSave ($insert, $changedAttributes) {
		parent::afterSave($insert, $changedAttributes);
		$changedAttributes = array_filter($changedAttributes, function($key) use ($changedAttributes) {
			return $changedAttributes[$key] != $this->$key;
		}, ARRAY_FILTER_USE_KEY);
		if (!$insert && (
			array_key_exists('is_enabled', $changedAttributes) || array_key_exists('is_hidden', $changedAttributes) ||
			array_key_exists('is_banned', $changedAttributes) || array_key_exists('is_finished', $changedAttributes)
		)) {
			$this->getModule()->send('gstats', [
				'g' => $this->id,
				'f' => +$this->is_finished, 'e' => +$this->is_enabled,
				'b' => +$this->is_banned, 'h' => +$this->is_hidden,
			]);
		}
	}

	public function updateResult ($params) {
		$this->result = $params['resultShort'];
		if(!$this->save(false, ['result'])) {
			return false;
		}
		if(($eventGameResult = EventGameResultModel::find()->andWhere(['game_id' => $this->id])->one()) === null){
			$eventGameResult = new EventGameResultModel();
			$eventGameResult->game_id = $this->id;
		}
		$eventGameResult->game_period = $params['gamePeriod'];
		$eventGameResult->result_extended = $params['resultExtended'];
		$eventGameResult->result_simple = $params['resultShortSimple'];
		$eventGameResult->statistics = $params['statistics'];
		$eventGameResult->sports_id = $this->sports_id;
		if(!$eventGameResult->save(false, ['game_id','sports_id','result_simple','game_period','result_extended','statistics'])) {
			return false;
		}
		$this->getModule()->send('gresult', [
			'g' => $this->id,
			'gp' => $eventGameResult->game_period,
			'r' => $this->result,
			'rs' => $eventGameResult->result_simple,
			're' => json_decode($eventGameResult->result_extended) ?? [],
		]);
		return true;
	}

	public function setTimer($timer){
		$seconds = $timer['m'] * 60 + $timer['s'] + $timer['timeGetData'] - $timer['timeSend'];
		$this->getModule()->send('gtimer', [
			'g' => $this->id,
			't' => (intval($seconds / 60) . gmdate(":s", $seconds % 60)),
			'r' => !!$timer['run'],
		]);
	}

	protected function getModule() : Module {
		return \Yii::$app->getModule('games');
	}

}

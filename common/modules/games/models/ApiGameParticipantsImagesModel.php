<?php

namespace common\modules\games\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%api_game_participants_images}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $participant_id
 * @property int $service_api
 * @property string $code_image 
 * @property int $is_checked
 *
 * @property GameParticipantsModel $participant
 * @property GameSportsModel $sports
 */
class ApiGameParticipantsImagesModel extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%api_game_participants_images}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['sports_id', 'participant_id','service_api','code_image'], 'required'],
			[['sports_id', 'participant_id','service_api','is_checked'], 'integer'],
			[['code_image'], 'string', 'max' => 100],
			[['participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameParticipantsModel::class, 'targetAttribute' => ['participant_id' => 'id']],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'sports_id' => Yii::t('games', 'Sport'),
			'participant_id' => Yii::t('games', 'Participant'),
			'service_api' => 'Service Api',
			'code_image' => 'Code image',
			'is_checked' => 'Is checked',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	public function getParticipant(){
		return $this->hasOne(GameParticipantsModel::class, ['id' => 'participant_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\ApiGameParticipantsImagesQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\ApiGameParticipantsImagesQuery(get_called_class());
	}
}

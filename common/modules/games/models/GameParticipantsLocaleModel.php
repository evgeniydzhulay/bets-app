<?php

namespace common\modules\games\models;

use common\modules\games\traits\LanguageValidateTrait;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%game_participants_locale}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $participant_id
 * @property string $lang_code
 * @property int $is_active
 * @property string $name
 *
 * @property GameParticipantsModel $participant
 * @property GameSportsModel $sports
 */
class GameParticipantsLocaleModel extends \yii\db\ActiveRecord {

	use LanguageValidateTrait;

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%game_participants_locale}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\GameParticipantsLocaleQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\games\queries\GameParticipantsLocaleQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */

	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['sports_id', 'participant_id','lang_code', 'is_active', 'name'],
			self::SCENARIO_UPDATE => ['is_active', 'name'],
			self::SCENARIO_SEARCH => ['id', 'sports_id', 'participant_id', 'lang_code', 'is_active', 'name'],
		];
	}

	public function rules () {
		return [
			[['is_active', 'sports_id', 'participant_id'], 'integer'],
			[['lang_code'], 'string', 'max' => 7],
			[['name'], 'string', 'max' => 255],
			[['participant_id', 'lang_code'], 'unique', 'targetAttribute' => ['participant_id', 'lang_code'],
				'on' => self::SCENARIO_CREATE],
			[['participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameParticipantsModel::class, 'targetAttribute' => ['participant_id' => 'id'],
				'on' => self::SCENARIO_CREATE],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id'],
				'on' => self::SCENARIO_CREATE],
			[['participant_id', 'lang_code'], function() {
				$this->translationNotExists([
					'participant_id' => $this->participant_id,
					'lang_code' => $this->lang_code,
				]);
			}, 'on' => self::SCENARIO_CREATE],
			[['lang_code','name'],'safe', 'on' => self::SCENARIO_SEARCH]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'participant_id' => Yii::t('games', 'Participant'),
			'lang_code' => Yii::t('games', 'Language'),
			'is_active' => Yii::t('games', 'Active'),
			'name' => Yii::t('games', 'Name'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getParticipant () {
		return $this->hasOne(GameParticipantsModel::class, ['id' => 'participant_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = static::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'sports_id' => $this->sports_id,
			'participant_id' => $this->participant_id,
		]);

		$query->andFilterWhere(['like', 'lang_code', $this->lang_code])
			->andFilterWhere(['like', 'name', $this->name]);

		return $dataProvider;
	}
}

<?php

namespace common\modules\games\models;

use Yii;

/**
 * This is the model class for table "{{%api_game_sports}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $service_api
 * @property string $code_api
 *
 * @property GameSportsModel $sports
 */
class ApiGameSportsModel extends \yii\db\ActiveRecord{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%api_game_sports}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['sports_id', 'service_api', 'code_api'], 'required'],
			[['sports_id', 'service_api'], 'integer'],
			[['code_api'], 'string', 'max' => 100],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'sports_id' => 'Sports ID',
			'service_api' => 'Service Api',
			'code_api' => 'Code Api',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports() {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\ApiGameSportsQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\ApiGameSportsQuery(get_called_class());
	}
}

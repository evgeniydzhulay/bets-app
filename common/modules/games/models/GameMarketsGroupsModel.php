<?php

namespace common\modules\games\models;

use common\modules\games\Module;
use common\modules\games\queries\EventOutcomesQuery;
use common\modules\games\queries\GameMarketsGroupsAdditionalQuery;
use common\modules\games\queries\GameMarketsGroupsLocaleQuery;
use common\modules\games\queries\GameMarketsGroupsQuery;
use common\modules\games\queries\GameMarketsLocaleQuery;
use common\modules\games\queries\GameMarketsQuery;
use common\modules\games\queries\GameOutcomesLocaleQuery;
use common\modules\games\queries\GameOutcomesQuery;
use common\modules\games\queries\GameSportsLocaleQuery;
use common\modules\games\queries\GameSportsQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%game_markets_groups}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $is_enabled
 * @property int $sort_order
 *
 * @property GameMarketsModel[] $gameMarkets
 * @property GameSportsModel $sports
 * @property GameMarketsGroupsLocaleModel[] $gameMarketsGroupsLocales
 * @property GameMarketsLocaleModel[] $gameMarketsLocales
 * @property GameOutcomesModel[] $gameOutcomes
 * @property EventOutcomesModel[] $eventOutcomes
 * @property GameMarketsGroupsAdditionalQuery[] $additionalMarkets
 * @property GameSportsLocaleQuery[] $gameSportsLocales
 * @property GameOutcomesLocaleModel[] $gameOutcomesLocales
 */
class GameMarketsGroupsModel extends ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const STATUS_ENABLED = 1;
	const STATUS_DISABLED = 0;

	/** {@inheritdoc} */
	public static function tableName () {
		return '{{%game_markets_groups}}';
	}

	/**
	 * @return array
	 */
	static public function statuses () {
		return [
			self::STATUS_ENABLED => Yii::t('games', 'Enabled'),
			self::STATUS_DISABLED => Yii::t('games', 'Disabled'),
		];
	}

	/** {@inheritdoc} */
	public function scenarios () {
		return [
			self::SCENARIO_CREATE => ['sports_id', 'is_enabled', 'sort_order'],
			self::SCENARIO_UPDATE => ['is_enabled', 'sort_order'],
			self::SCENARIO_SEARCH => ['id', 'sports_id', 'is_enabled', 'sort_order'],
		];
	}

	/** {@inheritdoc} */
	public function rules () {
		return [
			[['sports_id', 'is_enabled', 'sort_order'], 'integer'],
			[['sports_id'], 'exist', 'skipOnError' => true,
				'targetClass' => GameSportsModel::class,
				'targetAttribute' => ['sports_id' => 'id'],
				'on' => [self::SCENARIO_CREATE],
			],
		];
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'is_enabled' => Yii::t('games', 'Is enabled'),
			'sort_order' => Yii::t('games', 'Sort order'),
		];
	}

	public function search ($params) {
		$query = static::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => $this->id]],
		]);
		if ($this->load($params) && $this->validate()) {
			$query->andFilterWhere([
				'id' => $this->id,
				'sports_id' => $this->sports_id,
				'is_enabled' => $this->is_enabled,
				'sort_order' => $this->sort_order,
			]);
		}
		return $dataProvider;
	}

	/**
	 * {@inheritdoc}
	 * @return GameMarketsGroupsQuery the active query used by this AR class.
	 */
	public static function find () {
		return new GameMarketsGroupsQuery(get_called_class());
	}

	/** @return GameMarketsQuery */
	public function getGameMarkets () {
		return $this->hasMany(GameMarketsModel::class, ['group_id' => 'id'])->inverseOf('group');
	}

	/** @return GameMarketsGroupsAdditionalQuery */
	public function getAdditionalMarkets () {
		return $this->hasMany(GameMarketsGroupsAdditionalModel::class, ['group_id' => 'id']);
	}

	/** @return GameSportsQuery */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/** @return GameSportsLocaleQuery */
	public function getGameSportsLocales () {
		return $this->hasMany(GameSportsLocaleModel::class, ['sports_id' => 'sports_id']);
	}

	/** @return GameMarketsGroupsLocaleQuery */
	public function getGameMarketsGroupsLocales () {
		return $this->hasMany(GameMarketsGroupsLocaleModel::class, ['group_id' => 'id'])->inverseOf('group');
	}

	/** @return GameMarketsLocaleQuery */
	public function getGameMarketsLocales () {
		return $this->hasMany(GameMarketsLocaleModel::class, ['group_id' => 'id'])->inverseOf('group');
	}

	/** @return GameOutcomesQuery */
	public function getGameOutcomes () {
		return $this->hasMany(GameOutcomesModel::class, ['group_id' => 'id'])->inverseOf('group');
	}

	/** @return GameOutcomesLocaleQuery */
	public function getGameOutcomesLocales () {
		return $this->hasMany(GameOutcomesLocaleModel::class, ['group_id' => 'id'])->inverseOf('group');
	}

	/** @return EventOutcomesQuery */
	public function getEventOutcomes () {
		return $this->hasMany(EventOutcomesModel::class, ['group_id' => 'id'])->inverseOf('group');
	}

	protected function getModule() : Module {
		return \Yii::$app->getModule('games');
	}

}

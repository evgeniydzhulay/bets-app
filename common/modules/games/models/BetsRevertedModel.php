<?php

namespace common\modules\games\models;

use common\modules\games\queries\BetsRevertedQuery;
use common\modules\user\models\ProfileModel;
use common\modules\user\models\UserModel;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%bets_reverted}}".
 *
 * @property int $id
 * @property int $bet_id
 * @property int $user_id
 * @property string $comment
 * @property int $created_by
 * @property int $created_at
 *
 * @property BetsModel $bet
 * @property UserModel $user
 */
class BetsRevertedModel extends \yii\db\ActiveRecord
{

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%bets_reverted}}';
	}

	/**
	 * {@inheritdoc}
	 * @return BetsRevertedQuery the active query used by this AR class.
	 */
	public static function find() {
		return new BetsRevertedQuery(get_called_class());
	}

	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['bet_id', 'user_id','comment','user_id','bet_id'],
			self::SCENARIO_UPDATE => ['comment'],
			self::SCENARIO_SEARCH => ['bet_id', 'user_id','comment','created_by','created_at','id'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['bet_id', 'user_id','comment'], 'required', 'on' =>
				[self::SCENARIO_CREATE,self::SCENARIO_UPDATE]],
			[['bet_id', 'user_id', 'created_by', 'created_at'], 'integer', 'on' =>
				[self::SCENARIO_CREATE,self::SCENARIO_UPDATE]],
			[['comment'], 'string', 'on' =>
				[self::SCENARIO_CREATE,self::SCENARIO_UPDATE]],
			[['bet_id'], 'exist', 'skipOnError' => true, 'targetClass' => BetsModel::class, 'targetAttribute' => ['bet_id' => 'id'], 'on' =>
				self::SCENARIO_CREATE],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id'], 'on' =>
				self::SCENARIO_CREATE],
			[['bet_id','user_id','comment','id','created_by','created_at'], 'safe', 'on' =>
				self::SCENARIO_SEARCH ],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('bets', 'ID'),
			'bet_id' => Yii::t('bets', 'Bet'),
			'user_id' => Yii::t('bets', 'User'),
			'comment' => Yii::t('bets', 'Comment'),
			'created_by' => Yii::t('bets', 'Created by'),
			'created_at' => Yii::t('bets', 'Created at'),
		];
	}

	public function behaviors() {
		return [
			[
				'class' => BlameableBehavior::class,
				'createdByAttribute' => 'created_by',
				'updatedByAttribute' => false,
			],
			[
				'class' => TimestampBehavior::class,
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
				],
			],

		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getBet() {
		return $this->hasOne(BetsModel::class, ['id' => 'bet_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser() {
		return $this->hasOne(UserModel::class, ['id' => 'user_id']);
	}

	public function getCreator() {
		return $this->hasOne(UserModel::class, ['id' => 'created_by']);
	}

	public function getCreatorProfile() {
		return $this->hasOne(ProfileModel::class, ['user_id' => 'created_by']);
	}
}

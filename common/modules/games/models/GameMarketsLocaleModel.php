<?php

namespace common\modules\games\models;

use common\modules\games\traits\LanguageValidateTrait;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%game_markets_locale}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $market_id
 * @property int $group_id
 * @property string $lang_code
 * @property int $is_active
 * @property string $title
 *
 * @property GameMarketsGroupsModel $group
 * @property GameMarketsModel $market
 * @property GameSportsModel $sports
 */
class GameMarketsLocaleModel extends \yii\db\ActiveRecord {

	use LanguageValidateTrait;

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%game_markets_locale}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\GameMarketsLocaleQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\games\queries\GameMarketsLocaleQuery(get_called_class());
	}

	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['sports_id', 'market_id', 'group_id', 'lang_code', 'is_active', 'title'],
			self::SCENARIO_UPDATE => ['is_active', 'title'],
			self::SCENARIO_SEARCH => ['id', 'sports_id', 'market_id', 'group_id', 'lang_code', 'is_active', 'title'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['is_active', 'sports_id', 'market_id', 'group_id'], 'integer'],
			[['lang_code'], 'string', 'max' => 7],
			[['title'], 'string', 'max' => 255],
			[['market_id', 'lang_code'], 'unique', 'targetAttribute' => ['market_id', 'lang_code'],
				'on' => self::SCENARIO_CREATE],
			[['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsGroupsModel::class,
				'targetAttribute' => ['group_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsModel::class,
				'targetAttribute' => ['market_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class,
				'targetAttribute' => ['sports_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['market_id', 'lang_code'], function() {
				$this->translationNotExists([
					'market_id' => $this->market_id,
					'lang_code' => $this->lang_code,
				]);
			},'on' => self::SCENARIO_CREATE],
			[['id'],'integer','on' => self::SCENARIO_SEARCH],
			[['title'],'safe','on' => self::SCENARIO_SEARCH]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'market_id' => Yii::t('games', 'Market'),
			'group_id' => Yii::t('games', 'Group'),
			'lang_code' => Yii::t('games', 'Language'),
			'is_active' => Yii::t('games', 'Active'),
			'title' => Yii::t('games', 'Title'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroup () {
		return $this->hasOne(GameMarketsGroupsModel::class, ['id' => 'group_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMarket () {
		return $this->hasOne(GameMarketsModel::class, ['id' => 'market_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	public function search($params)
	{
		$query = static::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'sports_id' => $this->sports_id,
			'market_id' => $this->market_id,
			'group_id' => $this->group_id,
		]);

		$query->andFilterWhere(['like', 'lang_code', $this->lang_code])
			->andFilterWhere(['like', 'title', $this->title]);

		return $dataProvider;
	}
}

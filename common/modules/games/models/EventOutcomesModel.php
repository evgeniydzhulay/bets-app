<?php

namespace common\modules\games\models;

use common\modules\games\jobs\EventOutcomeBannedJob;
use common\modules\games\jobs\NewBetStatusJob;
use common\modules\games\Module;
use Yii;

/**
 * This is the model class for table "{{%event_outcomes}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $tournament_id
 * @property int $game_id
 * @property int $market_id
 * @property int $group_id
 * @property int $is_enabled
 * @property int $is_banned [int(1)]
 * @property int $is_hidden [int(1)]
 * @property int $is_finished [int(1)]
 * @property int $outcome_id
 * @property double $rate
 * @property int $status
 *
 * @property EventGameModel $game
 * @property GameMarketsGroupsModel $group
 * @property GameMarketsModel $market
 * @property GameSportsModel $sports
 * @property EventTournamentModel $tournament
 * @property GameOutcomesModel $outcome
 * @property EventTournamentLocaleModel[] $tournamentLocale
 * @property GameSportsLocaleModel[] $sportsLocale
 * @property EventGameLocaleModel[] $gameLocale
 * @property GameSportsLocaleModel[] $marketLocale
 * @property GameOutcomesModel $outcomeLocale
 * @property CartsItemsModel[] $cartItems
 * @property BetsItemsModel[] $betItems
 * @property GameMarketsGroupsLocaleModel[] $groupLocale
 */
class EventOutcomesModel extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%event_outcomes}}';
	}

	const STATUS_ENABLED  = 1;
	const STATUS_DISABLED = 0;

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\EventOutcomesQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\games\queries\EventOutcomesQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['sports_id', 'tournament_id', 'game_id', 'market_id', 'group_id', 'is_enabled','outcome_id','is_banned','is_hidden','is_finished', 'status'], 'integer'],
			[['rate'], 'number'],
			[['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventGameModel::class, 'targetAttribute' => ['game_id' => 'id']],
			[['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsGroupsModel::class, 'targetAttribute' => ['group_id' => 'id']],
			[['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsModel::class, 'targetAttribute' => ['market_id' => 'id']],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id']],
			[['tournament_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventTournamentModel::class, 'targetAttribute' => ['tournament_id' => 'id']],
			[['outcome_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameOutcomesModel::class, 'targetAttribute' => ['outcome_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'tournament_id' => Yii::t('games', 'Tournament'),
			'game_id' => Yii::t('games', 'Game'),
			'market_id' => Yii::t('games', 'Market'),
			'group_id' => Yii::t('games', 'Group'),
			'outcome_id' => Yii::t('games', 'Outcome'),
			'is_enabled' => Yii::t('games', 'Is enabled'),
			'rate' => Yii::t('games', 'Rate'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGame () {
		return $this->hasOne(EventGameModel::class, ['id' => 'game_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroup () {
		return $this->hasOne(GameMarketsGroupsModel::class, ['id' => 'group_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMarket () {
		return $this->hasOne(GameMarketsModel::class, ['id' => 'market_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOutcome () {
		return $this->hasOne(GameOutcomesModel::class, ['id' => 'outcome_id']);
	}

	/**@return \yii\db\ActiveQuery */
	public function getTournament () {
		return $this->hasOne(EventTournamentModel::class, ['id' => 'tournament_id']);
	}

	/**@return \yii\db\ActiveQuery */
	public function getTournamentLocale () {
		return $this->hasMany(EventTournamentLocaleModel::class, ['tournament_id' => 'tournament_id']);
	}

	/**@return \yii\db\ActiveQuery */
	public function getSportsLocale () {
		return $this->hasMany(GameSportsLocaleModel::class, ['sports_id' => 'sports_id']);
	}

	/**@return \yii\db\ActiveQuery */
	public function getGameLocale () {
		return $this->hasMany(EventGameLocaleModel::class, ['game_id' => 'game_id']);
	}

	/**@return \yii\db\ActiveQuery */
	public function getGroupLocale () {
		return $this->hasMany(GameMarketsGroupsLocaleModel::class, ['group_id' => 'group_id']);
	}

	/**@return \yii\db\ActiveQuery */
	public function getMarketLocale () {
		return $this->hasMany(GameMarketsLocaleModel::class, ['market_id' => 'market_id']);
	}

	/**@return \yii\db\ActiveQuery */
	public function getOutcomeLocale () {
		return $this->hasMany(GameOutcomesLocaleModel::class, ['outcome_id' => 'outcome_id']);
	}

	/**@return \yii\db\ActiveQuery */
	public function getBetItems () {
		return $this->hasMany(BetsItemsModel::class, ['outcome_id' => 'id']);
	}

	/**@return \yii\db\ActiveQuery */
	public function getCartItems () {
		return $this->hasMany(CartsItemsModel::class, ['outcome_id' => 'outcome_id']);
	}

	public function ban($message) {
		$this->is_banned = true;
		if($this->save(false, ['is_banned'])) {
			$this->getModule()->getOutcomeBanQueue()->push(new EventOutcomeBannedJob([
				'g' => $this->game_id,
				'o' => $this->id,
				'message' => $message,
			]));
			return true;
		}
		return false;
	}

	public function setRate(float $rate) {
		if($this->rate == $rate) {
			return true;
		}
		$this->rate = $rate;
		if($this->save(false, ['rate'])) {
			$this->getModule()->send('orate', [
				'g' => $this->game_id,
				'o' => $this->id,
				'r' => $rate
			]);
			return true;
		}
		return false;
	}

	public function setStatus(int $status) {
		if($this->is_finished) {
			return true;
		}
		$this->status = $status;
		$this->is_finished = true;
		if(!$this->save(false, ['status', 'is_finished'])) {
			return false;
		}
		$this->getModule()->send('ostatus', [
			'g' => $this->game_id,
			'o' => $this->id,
			's' => $status
		]);
		$this->getModule()->getOutcomeStatusQueue()->push(new NewBetStatusJob([
			'g' => $this->game_id,
			'o' => $this->id,
			's' => $status
		]));
		return true;
	}

	public function afterSave ($insert, $changedAttributes) {
		if(array_key_exists('is_enabled', $changedAttributes) && $changedAttributes['is_enabled'] != $this->is_enabled) {
			$this->getModule()->send('oenabled', [
				'g' => $this->game_id,
				'o' => $this->id,
				'is' =>  +$this->is_enabled
			]);
		}
		if(array_key_exists('is_hidden', $changedAttributes) && $changedAttributes['is_hidden'] != $this->is_hidden) {
			$this->getModule()->send('ohidden', [
				'g' => $this->game_id,
				'o' => $this->id,
				'is' =>  +$this->is_hidden
			]);
		}
		if(array_key_exists('is_finished', $changedAttributes) && $changedAttributes['is_finished'] != $this->is_finished) {
			$this->getModule()->send('ofinished', [
				'g' => $this->game_id,
				'o' => $this->id,
				'is' =>  +$this->is_finished
			]);
		}
		if(array_key_exists('is_banned', $changedAttributes) && $changedAttributes['is_banned'] != $this->is_banned) {
			$this->getModule()->send('oban', [
				'g' => $this->game_id,
				'o' => $this->id,
				'is' => +$this->is_banned
			]);
		}
		return parent::afterSave($insert, $changedAttributes);
	}

	protected function getModule() : Module {
		return \Yii::$app->getModule('games');
	}
}

<?php

namespace common\modules\games\models;

use common\modules\games\traits\LanguageValidateTrait;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%event_game_locale}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $tournament_id
 * @property int $game_id
 * @property string $lang_code
 * @property int $is_active
 * @property string $title
 * @property string $description
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property EventGameModel $game
 * @property GameSportsModel $sports
 * @property EventTournamentModel $tournament
 */
class EventGameLocaleModel extends \yii\db\ActiveRecord {

	use LanguageValidateTrait;

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%event_game_locale}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\EventGameLocaleQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\games\queries\EventGameLocaleQuery(get_called_class());
	}

	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['sports_id', 'tournament_id', 'game_id','description', 'meta_keywords', 'meta_description', 'is_active', 'lang_code', 'title'],
			self::SCENARIO_UPDATE => ['is_active', 'description', 'meta_keywords', 'meta_description','title'],
			self::SCENARIO_SEARCH => ['id', 'sports_id', 'tournament_id', 'game_id','description', 'meta_keywords', 'meta_description', 'lang_code', 'is_active', 'title'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['is_active', 'sports_id', 'tournament_id', 'game_id'], 'integer'],
			[['description', 'meta_keywords', 'meta_description'], 'string'],
			[['lang_code'], 'string', 'max' => 7],
			[['title'], 'string', 'max' => 255],
			[['game_id', 'lang_code'], 'unique', 'targetAttribute' => ['game_id', 'lang_code'],
				'on' => self::SCENARIO_CREATE],
			[['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventGameModel::class,
				'targetAttribute' => ['game_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class,
				'targetAttribute' => ['sports_id' => 'id'],'on' => self::SCENARIO_CREATE],
			[['tournament_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventTournamentModel::class,
				'targetAttribute' => ['tournament_id' => 'id'],'on' => self::SCENARIO_CREATE],
			[['game_id', 'lang_code'], function() {
				$this->translationNotExists([
					'game_id' => $this->game_id,
					'lang_code' => $this->lang_code,
				]);
			},'on' => self::SCENARIO_CREATE],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'tournament_id' => Yii::t('games', 'Tournament'),
			'game_id' => Yii::t('games', 'Game'),
			'lang_code' => Yii::t('games', 'Language'),
			'is_active' => Yii::t('games', 'Active'),
			'title' => Yii::t('games', 'Title'),
			'description' => Yii::t('games', 'Description'),
			'meta_keywords' => Yii::t('games', 'Meta keywords'),
			'meta_description' => Yii::t('games', 'Meta description'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGame () {
		return $this->hasOne(EventGameModel::class, ['id' => 'game_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTournament () {
		return $this->hasOne(EventTournamentModel::class, ['id' => 'tournament_id']);
	}

	public function search($params)
	{
		$query = static::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'sports_id' => $this->sports_id,
			'tournament_id' => $this->tournament_id,
			'game_id' => $this->game_id,
		]);

		$query->andFilterWhere(['like', 'lang_code', $this->lang_code])
			->andFilterWhere(['like', 'title', $this->title])
			->andFilterWhere(['like', 'description', $this->description])
			->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
			->andFilterWhere(['like', 'meta_description', $this->meta_description]);

		return $dataProvider;
	}
}

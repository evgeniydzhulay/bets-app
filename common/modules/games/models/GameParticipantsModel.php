<?php

namespace common\modules\games\models;

use backend\modules\games\actions\FileUploadModel;
use League\Flysystem\Filesystem;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%game_participants}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property string $image
 * @property int $image_is_approved
 *
 * @property GameSportsModel $sports
 * @property GameParticipantsLocaleModel[] $gameParticipantsLocales
 */
class GameParticipantsModel extends \yii\db\ActiveRecord
{
	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%game_participants}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\GameParticipantsQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\GameParticipantsQuery(get_called_class());
	}

	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['sport_id'],
			self::SCENARIO_UPDATE => ['image'],
			self::SCENARIO_SEARCH => ['sport_id','id'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['sports_id','image_is_approved'], 'integer'],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class,
				'targetAttribute' => ['sports_id' => 'id'],'on' => self::SCENARIO_CREATE],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'image' => Yii::t('games', 'Image'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports() {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameParticipantsLocales() {
		return $this->hasMany(GameParticipantsLocaleModel::class, ['participant_id' => 'id'])->inverseOf('participant');
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search($params){
		$query = static::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);

		if(!$this->validate()){
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'game_participants.sports_id' => $this->sports_id,
		]);

		return $dataProvider;
	}

	/** {@inheritdoc} */
	public function save ($runValidation = true, $attributeNames = null) {
		if(($image = $this->getImage())) {
			$model = Yii::createObject([
				'class' => FileUploadModel::class,
				'file' => $image
			]);
			if ($model->upload()) {
				$this->deleteImage();
				$result = $model->getResponse();
				$this->image = $result['fileName'];
			}
		}
		return parent::save($runValidation, $attributeNames);
	}

	/** {@inheritdoc} */
	protected function getImage() {
		return UploadedFile::getInstance($this, 'image');
	}

	/** {@inheritdoc} */
	public function deleteImage() {
		if(!empty($this->image) && $this->getStorage()->has($this->image)) {
			return $this->getStorage()->delete($this->image);
		}
		return false;
	}

	/** {@inheritdoc} */
	public function delete() {
		if(($res = parent::delete())) {
			$this->deleteImage();
			return $res;
		}
		return false;
	}

	public function getStorage() : Filesystem {
		return Yii::$app->getModule('gamesStorage')->get('storage');
	}
}

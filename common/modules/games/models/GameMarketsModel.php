<?php

namespace common\modules\games\models;

use common\modules\games\Module;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%game_markets}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $group_id
 * @property int $is_enabled
 * @property int $is_main
 * @property int $sort_order
 * @property int $result_type [int(1) unsigned]
 *
 * @property GameMarketsGroupsModel $group
 * @property GameSportsModel $sports
 * @property GameMarketsLocaleModel[] $gameMarketsLocales
 * @property GameOutcomesModel[] $gameOutcomes
 * @property EventOutcomesModel[] $eventOutcomes
 * @property EventParticipantsModel[] $eventParticipants
 * @property GameOutcomesLocaleModel[] $gameOutcomesLocales
 */
class GameMarketsModel extends \yii\db\ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const STATUS_ENABLED  = 1;
	const STATUS_DISABLED = 0;

	const RESULT_TYPE_DEFAULT = 0;
	const RESULT_TYPE_2_OPTIONS = 1;
	const RESULT_TYPE_3_OPTIONS = 2;

	public static function tableName () {
		return '{{%game_markets}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\GameMarketsQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\games\queries\GameMarketsQuery(get_called_class());
	}

	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['sports_id','is_enabled','sort_order','is_main','group_id','result_type'],
			self::SCENARIO_UPDATE => ['is_enabled','sort_order','is_main','result_type','group_id'],
			self::SCENARIO_SEARCH => ['id','sports_id', 'group_id', 'is_enabled','sort_order','is_main'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['sports_id'], 'required','on' => self::SCENARIO_CREATE],
			[['sports_id', 'group_id', 'is_enabled','sort_order','is_main','result_type'], 'integer'],
			[['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsGroupsModel::class,
				'targetAttribute' => ['group_id' => 'id'],'on' => self::SCENARIO_CREATE],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class,
				'targetAttribute' => ['sports_id' => 'id'],'on' => self::SCENARIO_CREATE],
			[['id'],'integer','on' => self::SCENARIO_SEARCH]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'group_id' => Yii::t('games', 'Group'),
			'is_enabled' => Yii::t('games', 'Is enabled'),
			'sort_order' => Yii::t('games', 'Sort order'),
			'is_main' => Yii::t('games', 'Is main'),
			'result_type' => Yii::t('games', 'Result type'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroup () {
		return $this->hasOne(GameMarketsGroupsModel::class, ['id' => 'group_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameMarketsLocales () {
		return $this->hasMany(GameMarketsLocaleModel::class, ['market_id' => 'id'])->inverseOf('market');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameOutcomes () {
		return $this->hasMany(GameOutcomesModel::class, ['market_id' => 'id'])->inverseOf('market');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameOutcomesLocales () {
		return $this->hasMany(GameOutcomesLocaleModel::class, ['market_id' => 'id'])->inverseOf('market');
	}

	public function getGameSportsLocales () {
		return $this->hasMany(GameSportsLocaleModel::class, ['sports_id' => 'sports_id']);
	}

	public function getGameMarketsGroupsLocales () {
		return $this->hasMany(GameMarketsGroupsLocaleModel::class, ['group_id' => 'group_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventOutcomes () {
		return $this->hasMany(EventOutcomesModel::class, ['market_id' => 'id'])->inverseOf('market');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventParticipants () {
		return $this->hasMany(EventParticipantsModel::class, ['participant_id' => 'id'])->inverseOf('market');
	}

	public function search($params)
	{
		$query = static::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);


		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'sports_id' => $this->sports_id,
			'group_id' => $this->group_id,
			'is_enabled' => $this->is_enabled,
			'is_main' => $this->is_main,
			'sort_order' => $this->sort_order,
			'result_type' => $this->result_type,
		]);

		return $dataProvider;
	}

	protected function getModule() : Module {
		return \Yii::$app->getModule('games');
	}

	/**
	 * @return array
	 */
	static public function statuses(){
		return [
			self::STATUS_ENABLED => Yii::t('games', 'Enabled'),
			self::STATUS_DISABLED => Yii::t('games', 'Disabled'),
		];
	}
}

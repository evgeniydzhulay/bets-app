<?php

namespace common\modules\games\models;

use Yii;

/**
 * This is the model class for table "{{%api_event_game}}".
 *
 * @property int $id
 * @property int $game_id
 * @property int $sports_id
 * @property int $tournament_id
 * @property int $service_api
 * @property string $code_api
 * @property int $api_event_id
 * @property int $is_main_provider
 * @property int $is_checked
 * @property string $hash
 *
 * @property EventGameModel $game
 * @property GameSportsModel $sports
 * @property EventTournamentModel $tournament
 */
class ApiEventGameModel extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%api_event_game}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['game_id', 'service_api', 'code_api','sports_id', 'tournament_id','api_event_id'], 'required'],
			[['game_id', 'service_api','sports_id', 'tournament_id','api_event_id','is_checked','is_main_provider'], 'integer'],
			[['code_api'], 'string', 'max' => 100],
			[['hash'], 'string', 'max' => 32],
			[['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventGameModel::class, 'targetAttribute' => ['game_id' => 'id']],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id']],
			[['tournament_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventTournamentModel::class, 'targetAttribute' => ['tournament_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'game_id' => 'Game ID',
			'sports_id' => Yii::t('games', 'Sport'),
			'tournament_id' => Yii::t('games', 'Tournament'),
			'service_api' => 'Service Api',
			'code_api' => 'Code Api',
			'api_event_id' => 'api event id',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGame() {
		return $this->hasOne(EventGameModel::class, ['id' => 'game_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTournament () {
		return $this->hasOne(EventTournamentModel::class, ['id' => 'tournament_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\ApiEventGameQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\ApiEventGameQuery(get_called_class());
	}
}

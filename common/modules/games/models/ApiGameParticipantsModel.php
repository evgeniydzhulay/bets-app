<?php

namespace common\modules\games\models;

use Yii;

/**
 * This is the model class for table "{{%api_game_participants}}".
 *
 * @property int $id
 * @property int $participant_id
 * @property int $service_api
 * @property string $code_api
 *
 * @property GameParticipantsModel $participant
 */
class ApiGameParticipantsModel extends \yii\db\ActiveRecord{
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%api_game_participants}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['participant_id', 'service_api', 'code_api'], 'required'],
			[['participant_id', 'service_api'], 'integer'],
			[['code_api'], 'string', 'max' => 100],
			[['participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameParticipantsModel::class, 'targetAttribute' => ['participant_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'participant_id' => 'Participant ID',
			'service_api' => 'Service Api',
			'code_api' => 'Code Api',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getParticipant() {
		return $this->hasOne(GameParticipantsModel::class, ['id' => 'participant_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\ApiGameParticipantsQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\ApiGameParticipantsQuery(get_called_class());
	}
}

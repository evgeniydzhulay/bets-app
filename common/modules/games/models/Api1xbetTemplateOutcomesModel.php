<?php

namespace common\modules\games\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%api_1xbet_template_outcomes}}".
 *
 * @property int $id
 * @property int $market_id
 * @property int $outcome_id
 * @property string $name
 * @property string $params
 *
 */
class Api1xbetTemplateOutcomesModel extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%api_1xbet_template_outcomes}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['market_id', 'outcome_id','name'], 'required'],
			[['market_id', 'outcome_id'], 'integer'],
			[['name','params'], 'string', 'max' => 255],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'name' => 'Outcome name',
		];
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\Api1xbetTemplateOutcomesQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\Api1xbetTemplateOutcomesQuery(get_called_class());
	}
}

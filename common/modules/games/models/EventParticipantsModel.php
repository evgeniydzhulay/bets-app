<?php

namespace common\modules\games\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%event_participants}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $tournament_id
 * @property int $game_id
 * @property int $participant_id
 * @property int $sort_order
 *
 * @property EventGameModel $game
 * @property GameMarketsModel $market
 * @property GameSportsModel $sports
 * @property EventTournamentModel $tournament
 */
class EventParticipantsModel extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%event_participants}}';
	}

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\EventParticipantsQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\games\queries\EventParticipantsQuery(get_called_class());
	}

	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['sports_id','tournament_id','game_id','participant_id','sort_order'],
			self::SCENARIO_UPDATE => [],
			self::SCENARIO_SEARCH => ['id','sports_id','tournament_id','game_id','participant_id','sort_order'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['sports_id', 'tournament_id', 'game_id', 'participant_id', 'sort_order'], 'integer'],
			[['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventGameModel::class, 'targetAttribute' => ['game_id' => 'id']],
			[['participant_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameParticipantsModel::class, 'targetAttribute' => ['participant_id' => 'id']],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id']],
			[['tournament_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventTournamentModel::class, 'targetAttribute' => ['tournament_id' => 'id']],

			[['id','sports_id','tournament_id','game_id','participant_id','sort_order'],'safe','on' => self::SCENARIO_SEARCH]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'tournament_id' => Yii::t('games', 'Tournament'),
			'game_id' => Yii::t('games', 'Game'),
			'participant_id' => Yii::t('games', 'Participant'),
			'sort_order' => Yii::t('games', 'Sort order'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGame () {
		return $this->hasOne(EventGameModel::class, ['id' => 'game_id']);
	}

	public function getGameLocale(){
		return $this->hasMany(EventGameLocaleModel::class, ['game_id' => 'game_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMarket () {
		return $this->hasOne(GameMarketsModel::class, ['id' => 'participant_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getTournament () {
		return $this->hasOne(EventTournamentModel::class, ['id' => 'tournament_id']);
	}

	public function getParticipant(){
		return $this->hasOne(GameParticipantsModel::class, ['id' => 'participant_id']);
	}

	public function getParticipantLocale(){
		return $this->hasMany(GameParticipantsLocaleModel::class, ['participant_id' => 'participant_id']);
	}

	public function search($params)
	{
		$query = static::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'sports_id' => $this->sports_id,
			'tournament_id' => $this->tournament_id,
			'game_id' => $this->game_id,
			'participant_id' => $this->participant_id,
			'sort_order' => $this->sort_order,
		]);

		return $dataProvider;
	}
}

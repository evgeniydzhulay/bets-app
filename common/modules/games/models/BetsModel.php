<?php

namespace common\modules\games\models;

use common\modules\games\queries\BetsItemsQuery;
use common\modules\games\queries\BetsQuery;
use common\modules\user\models\ProfileModel;
use common\modules\user\models\UserModel;
use common\modules\user\models\UserPurseHistoryModel;
use common\modules\user\queries\UserPurseHistoryQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\behaviors\TimestampBehavior;
use yii\web\NotAcceptableHttpException;
use yii\web\ServerErrorHttpException;

/**
 * This is the model class for table "{{%bets}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $withdraw_id
 * @property int $refill_id
 * @property float $amount_bet
 * @property float $amount_win
 * @property int $status
 * @property int $created_at
 *
 * @property UserModel $user
 * @property ProfileModel $userProfile
 * @property UserPurseHistoryModel $withdraw
 * @property UserPurseHistoryModel $refill
 * @property BetsItemsModel[] $items
 */
class BetsModel extends \yii\db\ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const STATUS_ACTIVE = 0;
	const STATUS_VICTORY = 1;
	const STATUS_DEFEAT = 2;
	const STATUS_BANNED = 8;
	const STATUS_REVERTED = 9;

	const TYPE_SINGLE = 0;
	const TYPE_MULTI = 1;
	const TYPE_CASINO = 41;

	const EVENT_VICTORY = 'eventVictory';
	const EVENT_BANNED = 'eventBanned';
	const EVENT_DEFEAT = 'eventDefeat';
	const EVENT_REVERT = 'eventRevert';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%bets}}';
	}

	public static function types() {
		return [
			self::TYPE_SINGLE => Yii::t('bets', 'Single'),
			self::TYPE_MULTI => Yii::t('bets', 'Multi'),
		];
	}

	public static function statuses() {
		return [
			self::STATUS_ACTIVE => Yii::t('bets', 'Active'),
			self::STATUS_VICTORY => Yii::t('bets', 'Victory'),
			self::STATUS_DEFEAT => Yii::t('bets', 'Defeat'),
			self::STATUS_BANNED => Yii::t('bets', 'Banned'),
			self::STATUS_REVERTED => Yii::t('bets', 'Reverted'),
		];
	}

	/**
	 * {@inheritdoc}
	 * @return BetsQuery the active query used by this AR class.
	 */
	public static function find () {
		return new BetsQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['user_id', 'withdraw_id', 'amount_bet', 'amount_win', 'status'],
			self::SCENARIO_UPDATE => ['amount_bet', 'amount_win', 'status', 'refill_id'],
			self::SCENARIO_SEARCH => ['id', 'user_id', 'withdraw_id', 'refill_id', 'amount_bet', 'amount_win', 'status','created_at'],
		];
	}

	/** {@inheritdoc} */
	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['user_id', 'withdraw_id', 'refill_id', 'amount_bet', 'amount_win', 'status'], 'required', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['user_id', 'withdraw_id', 'refill_id', 'status'], 'integer', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['amount_bet', 'amount_win'], 'number', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE,
			]],
			[['withdraw_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserPurseHistoryModel::class, 'targetAttribute' => ['withdraw_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE,
			]],
			[['refill_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserPurseHistoryModel::class, 'targetAttribute' => ['refill_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['id', 'user_id', 'withdraw_id', 'refill_id', 'amount_bet', 'amount_win', 'status', 'created_at'], 'safe', 'on' => [
				self::SCENARIO_SEARCH,
			]],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'user_id' => Yii::t('games', 'User'),
			'withdraw_id' => Yii::t('games', 'Withdraw'),
			'refill_id' => Yii::t('games', 'Refill'),
			'amount_bet' => Yii::t('games', 'Bet amount'),
			'amount_win' => Yii::t('games', 'Victory amount'),
			'status' => Yii::t('games', 'Status'),
			'created_at' => Yii::t('games', 'Created at'),
		];
	}

	/**
	 * @return ActiveQuery
	 */
	public function getUser () {
		return $this->hasOne(UserModel::class, ['id' => 'user_id']);
	}

	public function getUserProfile () {
		return $this->hasOne(ProfileModel::class, ['user_id' => 'user_id']);
	}

	/**
	 * @return UserPurseHistoryQuery
	 */
	public function getWithdraw () {
		return $this->hasOne(UserPurseHistoryModel::class, ['id' => 'withdraw_id']);
	}

	/**
	 * @return UserPurseHistoryQuery
	 */
	public function getRefill () {
		return $this->hasOne(UserPurseHistoryModel::class, ['id' => 'refill_id']);
	}

	/**
	 * @return BetsItemsQuery
	 */
	public function getItems () {
		return $this->hasMany(BetsItemsModel::class, ['bet_id' => 'id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getBetsReverted () {
		return $this->hasOne(BetsRevertedModel::class, ['bet_id' => 'id']);
	}

	/**
	 * @param $params
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = static::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);
		$this->load($params);
		if ($this->validate()) {
			$query->andFilterWhere([
				'id' => $this->id,
				'user_id' => $this->user_id,
				'withdraw_id' => $this->withdraw_id,
				'refill_id' => $this->refill_id,
				'status' => $this->status,
				'amount_bet' => $this->amount_bet,
				'amount_win' => $this->amount_win,
			]);
		}
		return $dataProvider;
	}

	public function insertBet ($items) {
		$tr = Yii::$app->db->beginTransaction();
		try {
			$withdraw = Yii::createObject(UserPurseHistoryModel::class);
			$withdraw->setAttributes([
				'user_id' => $this->user_id,
				'amount' => -$this->amount_bet,
				'type' => UserPurseHistoryModel::TYPE_BET,
				'source' => null,
				'status' => UserPurseHistoryModel::STATUS_NEW,
			]);
			if(!$withdraw->save()) {
				throw new \Exception(json_encode($withdraw->errors));
			}
			$this->withdraw_id = $withdraw->id;
			if(!UserModel::updatePurse($this->user_id, -$this->amount_bet, $withdraw)) {
				$this->addError('amount_bet', Yii::t('user', 'There is not enough money in your account'));
				throw new NotAcceptableHttpException('Purse error');
			}
			if(!$this->save()) {
				throw new ServerErrorHttpException('Failed cart save');
			}
			foreach ($items as $item) {
				if(!$this->addItem($item['game_id'], $item['sport_id'], $item['market_id'], $item['outcome_id'], $item['outcomeRate'])) {
					throw new ServerErrorHttpException('Failed adding item to cart');
				}
			}
			$tr->commit();
			return true;
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		$tr->rollBack();
		return false;
	}

	public function addItem($game, $sport, $market, $outcome, $rate) {
		$item = new BetsItemsModel();
		$item->scenario = BetsItemsModel::SCENARIO_CREATE;
		$item->setAttributes([
			'bet_id' => $this->id,
			'user_id' => $this->user_id,
			'status' => self::STATUS_ACTIVE,
			'game_id' => $game,
			'sports_id' => $sport,
			'market_id' => $market,
			'outcome_id' => $outcome,
			'rate' => $rate,
		]);
		if($item->save()) {
			return $item;
		}
		return false;
	}

	public function victory() {
		if($this->status != self::STATUS_ACTIVE) {
			return false;
		}
		$tr = Yii::$app->db->beginTransaction();
		try {
			$refill = Yii::createObject(UserPurseHistoryModel::class);
			$refill->setAttributes([
				'user_id' => $this->user_id,
				'amount' => +$this->amount_win,
				'type' => UserPurseHistoryModel::TYPE_WIN,
				'source' => null,
				'status' => UserPurseHistoryModel::STATUS_SUCCESS,
			]);
			if($refill->save() && !!$this->updateAttributes([
				'status' => self::STATUS_VICTORY,
				'refill_id' => $refill->id
			]) && UserModel::updatePurse($this->user_id, +$this->amount_win, $refill)) {
				$this->trigger(self::EVENT_VICTORY);
				$tr->commit();
				return true;
			}
		} catch(\Throwable $e) {}
		$tr->rollBack();
		return false;
	}

	public function revert() {
		if($this->status != self::STATUS_ACTIVE) {
			return false;
		}
		$tr = Yii::$app->db->beginTransaction();
		try {
			$withdraw = $this->withdraw;
			if(
				!!$withdraw->updateAttributes(['status' => UserPurseHistoryModel::STATUS_REVERTED]) &&
				!!$this->updateAttributes(['status' => self::STATUS_REVERTED]) &&
				UserModel::updatePurse($this->user_id, +$this->amount_bet, $withdraw)
			) {
				$this->trigger(self::EVENT_REVERT);
				$tr->commit();
				return true;
			}
		} catch(\Throwable $e) {}
		$tr->rollBack();
		return false;
	}

	public function defeat() {
		if($this->status != self::STATUS_ACTIVE) {
			return false;
		}
		if(
			count(array_filter($this->items, function(BetsItemsModel $item) {
				return $item->rate >= 1.7;
			})) >= 7 &&
			count(array_filter($this->items, function(BetsItemsModel $item) {
				return $item->status == BetsItemsModel::STATUS_DEFEAT;
			})) === 1
		) {
			$bonus = Yii::createObject(UserPurseHistoryModel::class);
			$bonus->setAttributes([
				'user_id' => $this->user_id,
				'amount' => $this->amount_bet,
				'type' => UserPurseHistoryModel::TYPE_BONUS,
				'source' => UserPurseHistoryModel::BONUS_EXPRESS_RETURN,
				'description' => $this->id,
				'status' => UserPurseHistoryModel::STATUS_SUCCESS,
			]);
			$bonus->save(false);
		}
		$this->trigger(self::EVENT_DEFEAT);
		return !!$this->updateAttributes([
			'status' => self::STATUS_DEFEAT
		]);
	}

	public function ban($message) {
		if($this->status != self::STATUS_ACTIVE) {
			return false;
		}
		$this->status = self::STATUS_BANNED;
		if($this->save(false, ['status'])) {
			$this->trigger(self::EVENT_BANNED);
			\backend\modules\games\models\BetsRevertedModel::saveReport($this, $message); // for testing
			// TODO: ban processing
			return true;
		}
		return false;
	}

	public function processResults() {
		if($this->status != self::STATUS_ACTIVE) {
			return false;
		}
		$resultRate = 1;
		$amountReturn = 0;
		$amountBet = $this->amount_bet;
		$amountItem = $this->amount_bet / count($this->items);
		foreach($this->items AS $item) {
			if($item->status == BetsItemsModel::STATUS_REVERTED) {
				return $this->revert();
			} else if($item->status == BetsItemsModel::STATUS_DEFEAT) {
				return $this->defeat();
			} else if($item->status == BetsItemsModel::STATUS_HALF_VICTORY) {
				$amountReturn += $amountItem / 2;
				$amountBet -= $amountItem / 2;
				$resultRate *= $item->rate;
			} else if($item->status == BetsItemsModel::STATUS_HALF_DEFEAT) {
				$amountReturn += $amountItem / 2;
				$amountBet -= $amountItem;
			} else if($item->status == BetsItemsModel::STATUS_VICTORY) {
				$resultRate *= $item->rate;
			}
		}

		$this->setAttributes([
			'amount_win' => $amountReturn + ($amountBet * $resultRate)
		]);
		return $this->victory();
	}
}

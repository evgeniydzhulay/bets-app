<?php

namespace common\modules\games\models;

use common\modules\games\queries\CartsItemsQuery;
use common\modules\games\queries\CartsQuery;
use common\modules\user\models\UserModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "{{%carts}}".
 *
 * @property int $id
 * @property string $key
 * @property float $amount
 * @property int $on_change
 * @property int $created_at [int(11)]
 *
 * @property CartsItemsModel[] $items
 */
class CartsModel extends \yii\db\ActiveRecord {

	const ON_CHANGE_AGREE = 0;
	const ON_CHANGE_CONFIRM = 1;
	const ON_CHANGE_ACCEPT_INCREASE = 2;

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	public static function getMinimalBet() {
		return 10;
	}

	public static function getMaximalBet() {
		return 1000;
	}

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%carts}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios () {
		return [
			self::SCENARIO_CREATE => ['amount', 'on_change', 'key'],
			self::SCENARIO_UPDATE => ['amount', 'on_change'],
			self::SCENARIO_SEARCH => ['id', 'amount', 'on_change', 'key', 'created_at'],
		];
	}

	/** {@inheritdoc} */
	public function behaviors () {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['amount', 'on_change'], 'required', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['on_change'], 'integer', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['amount'], 'number', 'min' => self::getMinimalBet(), 'max' => self::getMaximalBet(), 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['key'], 'string', 'max' => 64, 'on' => [
				self::SCENARIO_CREATE,
			]],
			[['id', 'amount', 'on_change', 'key'], 'safe', 'on' => [
				self::SCENARIO_SEARCH,
			]],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'key' => Yii::t('games', 'Key'),
			'amount' => Yii::t('games', 'Amount'),
			'on_change' => Yii::t('games', 'On change'),
		];
	}

	/**
	 * @return CartsItemsQuery
	 */
	public function getItems () {
		return $this->hasMany(CartsItemsModel::class, ['cart_id' => 'id']);
	}

	/**
	 * @param $params
	 * @return ActiveDataProvider
	 */
	public function search ($params) {
		$query = static::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if ($this->validate()) {
			$query->andFilterWhere([
				'id' => $this->id,
				'key' => $this->key,
				'on_change' => $this->on_change,
				'amount' => $this->amount,
			]);
		}
		return $dataProvider;
	}

	/**
	 * {@inheritdoc}
	 * @return CartsQuery the active query used by this AR class.
	 */
	public static function find () {
		return new CartsQuery(get_called_class());
	}

	public function delete () {
		foreach ($this->getItems()->all() AS $item) {
			$item->delete();
		}
		return parent::delete();
	}

	/**
	 * @param $outcome
	 * @param bool $override
	 * @return CartsItemsModel
	 */
	public function addItem ($outcome, $override = false) {
		$item = new CartsItemsModel();
		$item->scenario = CartsItemsModel::SCENARIO_CREATE;
		$item->cart_id = $this->id;
		$item->outcome_id = $outcome;
		if ($item->save()) {
			if($override) {
				CartsItemsModel::deleteAll([
					'cart_id' => $item->cart_id,
					'game_id' => $item->outcome->game_id,
				]);
			}
		}
		return $item;
	}
	
	public function placeBet(UserModel $user, int $type) {
		$items = $this->getItems()->joinWith([
			'outcome', 'game',
		], false)->addSelect([
			CartsItemsModel::tableName() . '.*',
			'outcomeRate' => EventOutcomesModel::tableName() . '.rate',
			'o_enabled' => EventOutcomesModel::tableName() . '.is_enabled',
			'o_banned' => EventOutcomesModel::tableName() . '.is_banned',
			'o_finished' => EventOutcomesModel::tableName() . '.is_finished',
			'o_hidden' => EventOutcomesModel::tableName() . '.is_hidden',
			'g_finished' => EventGameModel::tableName() . '.is_finished',
			'g_resulted' => EventGameModel::tableName() . '.is_resulted',
		])->asArray()->all();
		if (count($items) == 0) {
			throw new NotFoundHttpException(Yii::t('games', 'No events in cart'));
		}
		foreach ($items as $item) {
			if (!$item['o_enabled'] || !!$item['o_hidden'] || !!$item['o_banned'] || !!$item['o_finished'] || !!$item['g_finished'] || !!$item['g_resulted']) {
				throw new BadRequestHttpException(Yii::t('games', 'One or more events have been blocked'));
			}
			if (
				($this->on_change == CartsModel::ON_CHANGE_CONFIRM && $item['rate'] != $item['outcomeRate']) &&
				($this->on_change == CartsModel::ON_CHANGE_ACCEPT_INCREASE && $item['rate'] < $item['outcomeRate'])
			) {
				throw new BadRequestHttpException(Yii::t('games', 'One or more events have changed odds'));
			}
		}
		try {
			if($type === BetsModel::TYPE_MULTI) {
				$win = array_reduce($items, function($sum, $item) {
					return $sum * $item['outcomeRate'];
				}, $this->amount);
				$this->placeBetItem($user->id, $this->amount, $win, $items);
			} else if ($type === BetsModel::TYPE_SINGLE) {
				$bet = ($this->amount / count($items));
				foreach ($items as $item) {
					$this->placeBetItem($user->id, $bet, $bet * $item['outcomeRate'], [$item]);
				}
			}
		} catch (\Exception $e) {
			throw $e;
		}
	}

	/**
	 * @param $uid int
	 * @param $bet float
	 * @param $win float
	 * @param BetsItemsModel[] $items
	 * @return BetsModel
	 */
	protected function placeBetItem(int $uid, float $bet, float $win, $items) {
		$model = new BetsModel();
		$model->scenario = BetsModel::SCENARIO_CREATE;
		$model->setAttributes([
			'user_id' => $uid,
			'amount_bet' => round($bet, 5),
			'amount_win' => round($win, 5),
			'status' => BetsModel::STATUS_ACTIVE,
		]);
		if (!$model->insertBet($items)) {
			return null;
		}
		return $model;
	}
	
	
}

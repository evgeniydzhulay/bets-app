<?php

namespace common\modules\games\models;

use backend\modules\games\actions\FileUploadModel;
use League\Flysystem\Filesystem;
use Yii;
use yii\data\ActiveDataProvider;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%game_sports}}".
 *
 * @property int $id
 * @property int $is_enabled
 * @property int $sort_order
 * @property string $image
 *
 * @property GameMarketsModel[] $gameMarkets
 * @property GameMarketsGroupsModel[] $gameMarketsGroups
 * @property GameMarketsGroupsLocaleModel[] $gameMarketsGroupsLocales
 * @property GameMarketsLocaleModel[] $gameMarketsLocales
 * @property GameOutcomesModel[] $gameOutcomes
 * @property GameOutcomesLocaleModel[] $gameOutcomesLocales
 * @property GameParticipantsModel[] $gameParticipants
 * @property GameParticipantsLocaleModel[] $gameParticipantsLocales
 * @property GameSportsLocaleModel[] $gameSportsLocales
 *
 * @property EventTournamentModel[] $eventTournaments
 * @property EventTournamentLocaleModel[] $eventTournamentLocales
 * @property EventGameModel[] $eventGames
 * @property EventGameLocaleModel[] $eventGameLocales
 * @property EventOutcomesModel[] $eventOutcomes
 * @property EventParticipantsModel[] $eventParticipants
 */
class GameSportsModel extends \yii\db\ActiveRecord
{

	public $upload_image;

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const STATUS_ENABLED  = 1;
	const STATUS_DISABLED = 0;
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%game_sports}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\GameSportsQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\GameSportsQuery(get_called_class());
	}

	/** @inheritdoc */
	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['is_enabled','sort_order'],
			self::SCENARIO_UPDATE => ['is_enabled', 'sort_order', 'image'],
			self::SCENARIO_SEARCH => ['is_enabled', 'sort_order'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['is_enabled', 'sort_order'], 'integer'],
			[['image'], 'string', 'max' => 255],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => Yii::t('games', 'ID'),
			'is_enabled' => Yii::t('games', 'Is enabled'),
			'sort_order' => Yii::t('games', 'Sort order'),
			'image' => Yii::t('games', 'Image'),
			'upload_image' => Yii::t('games', 'Image'),
		];
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params){
		$query = static::find() ;

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_ASC]]
		]);

		$this->load($params);

		if(!$this->validate()){
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'is_enabled' => $this->is_enabled,
		]);

		return $dataProvider;
	}

	/** {@inheritdoc} */
	public function save ($runValidation = true, $attributeNames = null) {
		if(($image = $this->getImage())) {
			$model = Yii::createObject([
				'class' => FileUploadModel::class,
				'file' => $image
			]);
			if ($model->upload()) {
				$this->deleteImage();
				$result = $model->getResponse();
				$this->image = $result['fileName'];
			}
		}
		return parent::save($runValidation, $attributeNames);
	}

	/** {@inheritdoc} */
	protected function getImage() {
		return UploadedFile::getInstance($this, 'upload_image');
	}

	/** {@inheritdoc} */
	public function deleteImage() {
		if(!empty($this->image) && $this->getStorage()->has($this->image)) {
			return $this->getStorage()->delete($this->image);
		}
		return false;
	}

	/** {@inheritdoc} */
	public function delete() {
		if(($res = parent::delete())) {
			$this->deleteImage();
			return $res;
		}
		return false;
	}

	public function getStorage() : \creocoder\flysystem\Filesystem {
		return Yii::$app->getModule('gamesStorage')->get('storage');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameMarkets() {
		return $this->hasMany(GameMarketsModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameMarketsGroups() {
		return $this->hasMany(GameMarketsGroupsModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameMarketsGroupsLocales() {
		return $this->hasMany(GameMarketsGroupsLocaleModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameMarketsLocales() {
		return $this->hasMany(GameMarketsLocaleModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameOutcomes() {
		return $this->hasMany(GameOutcomesModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameOutcomesLocales() {
		return $this->hasMany(GameOutcomesLocaleModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameParticipants() {
		return $this->hasMany(GameParticipantsModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameParticipantsLocales() {
		return $this->hasMany(GameParticipantsLocaleModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameSportsLocales() {
		return $this->hasMany(GameSportsLocaleModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventGames() {
		return $this->hasMany(EventGameModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventGameLocales() {
		return $this->hasMany(EventGameLocaleModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventOutcomes() {
		return $this->hasMany(EventOutcomesModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventParticipants() {
		return $this->hasMany(EventParticipantsModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventTournaments() {
		return $this->hasMany(EventTournamentModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventTournamentLocales() {
		return $this->hasMany(EventTournamentLocaleModel::class, ['sports_id' => 'id'])->inverseOf('sports');
	}

	static public function statuses(){
		return [
			self::STATUS_ENABLED => Yii::t('games', 'Enabled'),
			self::STATUS_DISABLED => Yii::t('games', 'Disabled'),
		];
	}

}

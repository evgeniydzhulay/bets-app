<?php

namespace common\modules\games\models;

use common\modules\games\queries\BetsItemsQuery;
use common\modules\games\queries\EventGameLocaleQuery;
use common\modules\games\queries\EventGameQuery;
use common\modules\games\queries\EventParticipantsQuery;
use common\modules\games\queries\GameMarketsLocaleQuery;
use common\modules\games\queries\GameMarketsQuery;
use common\modules\games\queries\GameOutcomesLocaleQuery;
use common\modules\games\queries\GameSportsQuery;
use common\modules\user\models\UserModel;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%bets_items}}".
 *
 * @property int $id
 * @property int $bet_id
 * @property int $outcome_id
 * @property int $user_id
 * @property float $rate
 * @property int $status
 * @property int $sports_id [int(11) unsigned]
 * @property int $game_id [int(11) unsigned]
 * @property int $market_id [int(11) unsigned]
 *
 * @property BetsModel $bet
 * @property EventOutcomesModel $outcome
 * @property GameSportsModel $sport
 * @property GameOutcomesLocaleModel[] $outcomeLocales
 * @property EventGameModel $game
 * @property GameMarketsLocaleModel[] $marketLocales
 * @property EventGameLocaleModel[] $gameLocales
 * @property GameMarketsModel $market
 * @property UserModel $user
 */
class BetsItemsModel extends \yii\db\ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const STATUS_ACTIVE = 0;
	const STATUS_VICTORY = 1;
	const STATUS_DEFEAT = 2;
	const STATUS_HALF_VICTORY = 3;
	const STATUS_HALF_DEFEAT = 4;
	const STATUS_REFUND = 5;
	const STATUS_REVERTED = 9;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%bets_items}}';
	}

	public static function statuses () {
		return [
			self::STATUS_ACTIVE => Yii::t('bets', 'Active'),
			self::STATUS_VICTORY => Yii::t('bets', 'Victory'),
			self::STATUS_DEFEAT => Yii::t('bets', 'Defeat'),
			self::STATUS_REVERTED => Yii::t('bets', 'Reverted'),
			self::STATUS_HALF_DEFEAT => Yii::t('bets', 'Half/Defeat'),
			self::STATUS_HALF_VICTORY => Yii::t('bets', 'Half/Victory'),
			self::STATUS_REFUND => Yii::t('bets', 'Refund'),
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios () {
		return [
			self::SCENARIO_CREATE => ['bet_id', 'game_id', 'sports_id', 'market_id', 'outcome_id', 'user_id', 'rate', 'status'],
			self::SCENARIO_UPDATE => ['rate', 'status'],
			self::SCENARIO_SEARCH => ['id', 'bet_id', 'game_id', 'sports_id', 'market_id', 'outcome_id', 'user_id', 'rate', 'status'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['bet_id', 'game_id', 'sports_id', 'market_id', 'outcome_id', 'user_id', 'status'], 'required', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],
			[['rate'], 'number', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],
			[['bet_id', 'game_id', 'sports_id', 'market_id', 'outcome_id', 'user_id', 'status'], 'integer', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['bet_id'], 'exist', 'skipOnError' => true, 'targetClass' => BetsModel::class, 'targetAttribute' => ['bet_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['outcome_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventOutcomesModel::class, 'targetAttribute' => ['outcome_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['id', 'bet_id', 'game_id', 'sports_id', 'market_id', 'outcome_id', 'user_id', 'rate', 'status'], 'safe', 'on' => [
				self::SCENARIO_SEARCH,
			]],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'bet_id' => Yii::t('games', 'Bet'),
			'game_id' => Yii::t('games', 'Game'),
			'sports_id' => Yii::t('games', 'Sport'),
			'market_id' => Yii::t('games', 'Market'),
			'outcome_id' => Yii::t('games', 'Outcome'),
			'user_id' => Yii::t('games', 'User'),
			'rate' => Yii::t('games', 'Rate'),
			'status' => Yii::t('games', 'Status'),
		];
	}

	/**
	 * @return ActiveQuery
	 */
	public function getBet () {
		return $this->hasOne(BetsModel::class, ['id' => 'bet_id']);
	}

	/**
	 * @return EventGameQuery
	 */
	public function getGame () {
		return $this->hasOne(EventGameModel::class, ['id' => 'game_id']);
	}

	/**
	 * @return EventGameLocaleQuery
	 */
	public function getGameLocales () {
		return $this->hasMany(EventGameLocaleModel::class, ['game_id' => 'game_id']);
	}

	/**
	 * @return EventParticipantsQuery
	 */
	public function getEventParticipants () {
		return $this->hasMany(EventParticipantsModel::class, ['game_id' => 'game_id']);
	}

	/**
	 * @return GameSportsQuery
	 */
	public function getSport () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * @return GameMarketsQuery
	 */
	public function getMarket () {
		return $this->hasOne(GameMarketsModel::class, ['id' => 'market_id']);
	}

	/**
	 * @return GameMarketsLocaleQuery
	 */
	public function getMarketLocales () {
		return $this->hasMany(GameMarketsLocaleModel::class, ['market_id' => 'market_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getOutcome () {
		return $this->hasOne(EventOutcomesModel::class, ['id' => 'outcome_id']);
	}

	/**
	 * @return GameOutcomesLocaleQuery
	 */
	public function getOutcomeLocales () {
		return $this->hasMany(GameOutcomesLocaleModel::class, ['outcome_id' => 'outcome_id'])->via('outcome');
	}

	/**
	 * @return ActiveQuery
	 */
	public function getUser () {
		return $this->hasOne(UserModel::class, ['id' => 'user_id']);
	}

	/**
	 * @param $params
	 * @return ActiveDataProvider
	 */
	public function search ($params) {
		$query = static::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]],
		]);
		$this->load($params);
		if ($this->validate()) {
			$query->andFilterWhere([
				'id' => $this->id,
				'sports_id' => $this->bet_id,
				'tournament_id' => $this->outcome_id,
				'game_id' => $this->user_id,
				'status' => $this->status,
				'rate' => $this->rate,
			]);
		}
		return $dataProvider;
	}

	/**
	 * {@inheritdoc}
	 * @return BetsItemsQuery the active query used by this AR class.
	 */
	public static function find () {
		return new BetsItemsQuery(get_called_class());
	}

	public function ban ($message) {
		$this->bet->ban($message);
	}

	public function setStatus ($status) {
		$this->status = $status;
		if ($this->save(false, ['status'])) {
			if (self::find()->andWhere(['bet_id' => $this->bet_id, 'status' => self::STATUS_ACTIVE])->count() == 0) {
				return $this->bet->processResults();
			}
			return true;
		}
		return false;
	}
}

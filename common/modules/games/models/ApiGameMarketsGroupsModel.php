<?php

namespace common\modules\games\models;

use Yii;

/**
 * This is the model class for table "{{%api_game_markets_groups}}".
 *
 * @property int $id
 * @property int $group_id
 * @property int $service_api
 * @property string $code_api
 * @property int $sports_id
 *
 * @property GameMarketsGroupsModel $group
 */
class ApiGameMarketsGroupsModel extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%api_game_markets_groups}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['group_id', 'service_api', 'code_api', 'sports_id'], 'required'],
			[['group_id', 'service_api', 'sports_id'], 'integer'],
			[['code_api'], 'string', 'max' => 100],
			[['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsGroupsModel::class, 'targetAttribute' => ['group_id' => 'id']],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'group_id' => 'Group ID',
			'service_api' => 'Service Api',
			'code_api' => 'Code Api',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroup() {
		return $this->hasOne(GameMarketsGroupsModel::class, ['id' => 'group_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\ApiGameMarketsGroupsQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\ApiGameMarketsGroupsQuery(get_called_class());
	}
}

<?php

namespace common\modules\games\models;

use common\modules\games\queries\CartsItemsQuery;
use common\modules\games\queries\CartsQuery;
use common\modules\games\queries\EventGameLocaleQuery;
use common\modules\games\queries\EventGameQuery;
use common\modules\games\queries\EventOutcomesQuery;
use common\modules\games\queries\EventParticipantsQuery;
use common\modules\games\queries\GameMarketsLocaleQuery;
use common\modules\games\queries\GameMarketsQuery;
use common\modules\games\queries\GameOutcomesLocaleQuery;
use common\modules\games\queries\GameSportsQuery;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%carts_items}}".
 *
 * @property int $id
 * @property int $cart_id
 * @property int $game_id [int(11) unsigned]
 * @property int $sport_id [int(11) unsigned]
 * @property int $market_id [int(11) unsigned]
 * @property int $outcome_id
 * @property float $rate
 *
 * @property-read CartsModel $cart
 * @property-read GameSportsModel $sport
 * @property-read EventGameModel $game
 * @property-read GameMarketsModel $market
 * @property-read GameOutcomesLocaleModel[] $outcomeLocales
 * @property-read EventParticipantsModel[] $eventParticipants
 * @property-read GameMarketsLocaleModel[] $marketLocales
 * @property-read EventGameLocaleModel[] $gameLocales
 * @property-read EventOutcomesModel $outcome
 */
class CartsItemsModel extends ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%carts_items}}';
	}

	/**
	 * {@inheritdoc}
	 * @return CartsItemsQuery the active query used by this AR class.
	 */
	public static function find () {
		return new CartsItemsQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['cart_id', 'game_id', 'sport_id', 'market_id', 'outcome_id', 'rate'],
			self::SCENARIO_UPDATE => ['amount'],
			self::SCENARIO_SEARCH => ['id', 'cart_id', 'game_id', 'sport_id', 'market_id', 'outcome_id', 'rate'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['cart_id', 'game_id', 'sport_id', 'market_id', 'outcome_id'], 'required', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['cart_id', 'game_id', 'sport_id', 'market_id', 'outcome_id'], 'integer', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['rate'], 'number', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['cart_id', 'outcome_id'], 'unique', 'targetAttribute' => ['cart_id', 'outcome_id'],'on' => self::SCENARIO_CREATE],
			[['cart_id'], 'exist', 'skipOnError' => true, 'targetClass' => CartsModel::class, 'targetAttribute' => ['cart_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventGameModel::class, 'targetAttribute' => ['game_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['sport_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sport_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsModel::class, 'targetAttribute' => ['market_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['outcome_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventOutcomesModel::class, 'targetAttribute' => ['outcome_id' => 'id'], 'on' => self::SCENARIO_CREATE],
			[['id', 'cart_id', 'game_id', 'sport_id', 'market_id', 'outcome_id', 'amount'], 'safe', 'on' => [
				self::SCENARIO_SEARCH,
			]],
			['outcome_id', function() {
				if (
					!!$this->outcome->is_finished || !$this->outcome->is_enabled ||
					!!$this->outcome->is_banned || !!$this->outcome->is_hidden ||
					!!$this->outcome->game->is_finished || !!$this->outcome->game->is_resulted
				) {
					$this->addError('outcome_id', Yii::t('bets', 'You cannot add finished item to cart'));
				}
			}, 'on' => self::SCENARIO_CREATE]
		];
	}

	public function save ($runValidation = true, $attributeNames = null) {
		if($this->getIsNewRecord()) {
			$this->rate = $this->outcome->rate;
			$this->sport_id = $this->outcome->sports_id;
			$this->game_id = $this->outcome->game_id;
			$this->market_id = $this->outcome->market_id;
		}
		return parent::save($runValidation, $attributeNames);
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'cart_id' => Yii::t('games', 'Cart'),
			'game_id' => Yii::t('games', 'Game'),
			'sport_id' => Yii::t('games', 'Sport'),
			'market_id' => Yii::t('games', 'Market'),
			'outcome_id' => Yii::t('games', 'Outcome'),
			'rate' => Yii::t('games', 'Rate'),
		];
	}

	/**
	 * @return CartsQuery
	 */
	public function getCart () {
		return $this->hasOne(CartsModel::class, ['id' => 'cart_id']);
	}

	/**
	 * @return EventGameQuery
	 */
	public function getGame () {
		return $this->hasOne(EventGameModel::class, ['id' => 'game_id']);
	}

	/**
	 * @return EventParticipantsQuery
	 */
	public function getEventParticipants() {
		return $this->hasMany(EventParticipantsModel::class, ['game_id' => 'game_id']);
	}

	/**
	 * @return EventGameLocaleQuery
	 */
	public function getGameLocales () {
		return $this->hasMany(EventGameLocaleModel::class, ['game_id' => 'game_id']);
	}

	/**
	 * @return GameSportsQuery
	 */
	public function getSport () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sport_id']);
	}

	/**
	 * @return GameMarketsQuery
	 */
	public function getMarket () {
		return $this->hasOne(GameMarketsModel::class, ['id' => 'market_id']);
	}

	/**
	 * @return GameMarketsLocaleQuery
	 */
	public function getMarketLocales () {
		return $this->hasMany(GameMarketsLocaleModel::class, ['market_id' => 'market_id']);
	}

	/**
	 * @return EventOutcomesQuery
	 */
	public function getOutcome () {
		return $this->hasOne(EventOutcomesModel::class, ['id' => 'outcome_id']);
	}

	/**
	 * @return GameOutcomesLocaleQuery
	 */
	public function getOutcomeLocales () {
		return $this->hasMany(GameOutcomesLocaleModel::class, ['outcome_id' => 'outcome_id'])->via('outcome');
	}

	/**
	 * @param $params
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = static::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);
		$this->load($params);
		if ($this->validate()) {
			$query->andFilterWhere([
				'id' => $this->id,
				'cart_id' => $this->cart_id,
				'game_id' => $this->game_id,
				'sport_id' => $this->sport_id,
				'market_id' => $this->market_id,
				'outcome_id' => $this->outcome_id,
				'rate' => $this->rate,
			]);
		}
		return $dataProvider;
	}

	public function ban($message) {
		// TODO: ban processing
	}
}

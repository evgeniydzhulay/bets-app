<?php

namespace common\modules\games\models;

use common\modules\games\traits\LanguageValidateTrait;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%game_outcomes_locale}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $group_id
 * @property int $market_id
 * @property int $outcome_id
 * @property string $lang_code
 * @property int $is_active
 * @property string $title
 *
 * @property GameMarketsGroupsModel $group
 * @property GameMarketsModel $market
 * @property GameOutcomesModel $outcome
 * @property GameSportsModel $sports
 */
class GameOutcomesLocaleModel extends \yii\db\ActiveRecord {

	use LanguageValidateTrait;

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%game_outcomes_locale}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\GameOutcomesLocaleQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\games\queries\GameOutcomesLocaleQuery(get_called_class());
	}

	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['sports_id', 'group_id', 'market_id', 'outcome_id', 'lang_code', 'is_active', 'title'],
			self::SCENARIO_UPDATE => ['is_active', 'title'],
			self::SCENARIO_SEARCH => ['id', 'sports_id', 'group_id', 'market_id', 'outcome_id', 'lang_code', 'is_active', 'title'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['is_active', 'sports_id', 'group_id', 'market_id', 'outcome_id'], 'integer'],
			[['lang_code'], 'string', 'max' => 7],
			[['title'], 'string', 'max' => 255],
			[['outcome_id', 'lang_code'], 'unique', 'targetAttribute' => ['outcome_id', 'lang_code'],'on' => self::SCENARIO_CREATE],
			[['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsGroupsModel::class,
				'targetAttribute' => ['group_id' => 'id'],'on' => self::SCENARIO_CREATE],
			[['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsModel::class,
				'targetAttribute' => ['market_id' => 'id'],'on' => self::SCENARIO_CREATE],
			[['outcome_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameOutcomesModel::class,
				'targetAttribute' => ['outcome_id' => 'id'],'on' => self::SCENARIO_CREATE],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class,
				'targetAttribute' => ['sports_id' => 'id'],'on' => self::SCENARIO_CREATE],
			[['outcome_id', 'lang_code'], function() {
				$this->translationNotExists([
					'outcome_id' => $this->outcome_id,
					'lang_code' => $this->lang_code,
				]);
			},'on' => self::SCENARIO_CREATE],
			[['id'],'integer','on' => self::SCENARIO_SEARCH]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'group_id' => Yii::t('games', 'Group'),
			'market_id' => Yii::t('games', 'Market'),
			'outcome_id' => Yii::t('games', 'Outcome'),
			'lang_code' => Yii::t('games', 'Language'),
			'is_active' => Yii::t('games', 'Active'),
			'title' => Yii::t('games', 'Title'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroup () {
		return $this->hasOne(GameMarketsGroupsModel::class, ['id' => 'group_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMarket () {
		return $this->hasOne(GameMarketsModel::class, ['id' => 'market_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOutcome () {
		return $this->hasOne(GameOutcomesModel::class, ['id' => 'outcome_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = static::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'sports_id' => $this->sports_id,
			'group_id' => $this->group_id,
			'market_id' => $this->market_id,
			'outcome_id' => $this->outcome_id,
		]);

		$query->andFilterWhere(['like', 'lang_code', $this->lang_code])
			->andFilterWhere(['like', 'title', $this->title]);

		return $dataProvider;
	}
}

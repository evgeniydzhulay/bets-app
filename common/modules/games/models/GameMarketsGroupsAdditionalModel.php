<?php

namespace common\modules\games\models;

use common\modules\games\queries\GameMarketsGroupsAdditionalQuery;
use common\modules\games\queries\GameMarketsGroupsQuery;
use common\modules\games\queries\GameMarketsQuery;
use common\modules\games\queries\GameSportsQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%game_markets_groups_additional}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $group_id
 * @property int $market_id
 * @property int $sort_order
 * @property int $is_enabled
 *
 * @property GameMarketsGroupsModel $group
 * @property GameMarketsModel $market
 * @property GameSportsModel $sports
 */
class GameMarketsGroupsAdditionalModel extends ActiveRecord {

	/** {@inheritdoc} */
	public static function tableName () {
		return '{{%game_markets_groups_additional}}';
	}

	/**
	 * {@inheritdoc}
	 * @return GameMarketsGroupsAdditionalQuery the active query used by this AR class.
	 */
	public static function find () {
		return new GameMarketsGroupsAdditionalQuery(get_called_class());
	}

	/** {@inheritdoc} */
	public function rules () {
		return [
			[['sports_id', 'group_id', 'market_id'], 'required'],
			[['sports_id', 'group_id', 'market_id', 'sort_order', 'is_enabled'], 'integer'],
			[['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsGroupsModel::class, 'targetAttribute' => ['group_id' => 'id']],
			[['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsModel::class, 'targetAttribute' => ['market_id' => 'id']],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id']],
		];
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'group_id' => Yii::t('games', 'Group'),
			'market_id' => Yii::t('games', 'Market'),
			'sort_order' => Yii::t('games', 'Sort order'),
			'is_enabled' => Yii::t('games', 'Is enabled'),
		];
	}

	/** @return GameMarketsGroupsQuery */
	public function getGroup () {
		return $this->hasOne(GameMarketsGroupsModel::class, ['id' => 'group_id']);
	}

	/** @return GameMarketsQuery */
	public function getMarket () {
		return $this->hasOne(GameMarketsModel::class, ['id' => 'market_id']);
	}

	/** @return GameSportsQuery */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}
}

<?php

namespace common\modules\games\models;

use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%api_1xbet_template_markets}}".
 *
 * @property int $id
 * @property int $market_id
 * @property string $name
 * @property int $count_fields
 *
 */
class Api1xbetTemplateMarketsModel extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%api_1xbet_template_markets}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['market_id', 'name'], 'required'],
			[['market_id', 'count_fields'], 'integer'],
			[['name'], 'string', 'max' => 255],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'name' => 'Market name',
			'count_fields' => 'Count fields',
		];
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\Api1xbetTemplateMarketsQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\Api1xbetTemplateMarketsQuery(get_called_class());
	}
}

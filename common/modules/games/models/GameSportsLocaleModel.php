<?php

namespace common\modules\games\models;

use common\modules\games\traits\LanguageValidateTrait;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%game_sports_locale}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property string $lang_code
 * @property int $is_active
 * @property string $title
 * @property string $description
 * @property string $meta_keywords
 * @property string $meta_description
 *
 * @property GameSportsModel $sports
 */
class GameSportsLocaleModel extends \yii\db\ActiveRecord {

	use LanguageValidateTrait;

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%game_sports_locale}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\GameSportsLocaleQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\games\queries\GameSportsLocaleQuery(get_called_class());
	}

	/** @inheritdoc */
	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['sports_id', 'lang_code', 'is_active', 'title', 'description', 'meta_keywords', 'meta_description'],
			self::SCENARIO_UPDATE => ['is_active', 'title', 'description', 'meta_keywords', 'meta_description'],
			self::SCENARIO_SEARCH => ['id','sports_id', 'lang_code', 'is_active', 'title', 'description', 'meta_keywords', 'meta_description'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['is_active', 'sports_id'], 'integer'],
			[['description', 'meta_keywords', 'meta_description'], 'string'],
			[['lang_code'], 'string', 'max' => 7],
			[['title'], 'string', 'max' => 255],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE
			]],
			[['sports_id', 'lang_code'], function() {
				$this->translationNotExists([
					'sports_id' => $this->sports_id,
					'lang_code' => $this->lang_code,
				]);
			}, 'on' => [
				self::SCENARIO_CREATE, self::SCENARIO_UPDATE
			]],
			[['lang_code', 'title', 'description', 'meta_keywords', 'meta_description'], 'safe', 'on' => [
				self::SCENARIO_SEARCH
			]],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'lang_code' => Yii::t('games', 'Language'),
			'is_active' => Yii::t('games', 'Active'),
			'title' => Yii::t('games', 'Title'),
			'description' => Yii::t('games', 'Description'),
			'meta_keywords' => Yii::t('games', 'Meta keywords'),
			'meta_description' => Yii::t('games', 'Meta description'),
		];
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search($params)	{
		$query = static::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'sports_id' => $this->sports_id,
		]);

		$query->andFilterWhere(['like', 'lang_code', $this->lang_code])
			->andFilterWhere(['like', 'title', $this->title])
			->andFilterWhere(['like', 'description', $this->description])
			->andFilterWhere(['like', 'meta_keywords', $this->meta_keywords])
			->andFilterWhere(['like', 'meta_description', $this->meta_description]);

		return $dataProvider;
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}
}

<?php

namespace common\modules\games\models;

use Yii;

/**
 * This is the model class for table "{{%api_game_outcomes}}".
 *
 * @property int $id
 * @property int $outcome_id
 * @property int $service_api
 * @property string $code_api
 *
 * @property GameOutcomesModel $outcome
 */
class ApiGameOutcomesModel extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName()
	{
		return '{{%api_game_outcomes}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['outcome_id', 'service_api', 'code_api'], 'required'],
			[['outcome_id', 'service_api'], 'integer'],
			[['code_api'], 'string', 'max' => 100],
			[['outcome_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameOutcomesModel::class, 'targetAttribute' => ['outcome_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'outcome_id' => 'Outcome ID',
			'service_api' => 'Service Api',
			'code_api' => 'Code Api',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getOutcome() {
		return $this->hasOne(GameOutcomesModel::class, ['id' => 'outcome_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\ApiGameOutcomesQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\ApiGameOutcomesQuery(get_called_class());
	}
}

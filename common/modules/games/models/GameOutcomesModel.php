<?php

namespace common\modules\games\models;

use common\modules\games\Module;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * This is the model class for table "{{%game_outcomes}}".
 *
 * @property int $id
 * @property int $sports_id
 * @property int $group_id
 * @property int $market_id
 * @property int $is_enabled
 * @property int $sort_order
 * @property int $is_main
 * @property string $condition
 * @property string $short_title
 *
 * @property GameMarketsGroupsModel $group
 * @property GameMarketsModel $market
 * @property GameSportsModel $sports
 * @property GameOutcomesLocaleModel[] $gameOutcomesLocales
 * @property EventOutcomesModel[] $eventOutcomes
 * @property ApiGameOutcomesModel[] $apiGameOutcomes
 */
class GameOutcomesModel extends \yii\db\ActiveRecord
{

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const STATUS_ENABLED  = 1;
	const STATUS_DISABLED = 0;

	/**
	 * {@inheritdoc}
	 */
	public static function tableName(){
		return '{{%game_outcomes}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\GameOutcomesQuery the active query used by this AR class.
	 */
	public static function find(){
		return new \common\modules\games\queries\GameOutcomesQuery(get_called_class());
	}

	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['sports_id', 'group_id', 'market_id', 'is_enabled', 'sort_order', 'condition','is_main','short_title'],
			self::SCENARIO_UPDATE => ['is_enabled','sort_order','is_main','short_title'],
			self::SCENARIO_SEARCH => ['id','sports_id', 'group_id', 'market_id', 'is_enabled','is_main','short_title'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules(){
		return [
			[['sports_id', 'group_id', 'market_id', 'is_enabled','sort_order','is_main'], 'integer'],
			[['condition'], 'string', 'max' => 255],
			[['short_title'], 'string', 'max' => 10],
			[['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsGroupsModel::class,
				'targetAttribute' => ['group_id' => 'id'],'on' => self::SCENARIO_CREATE],
			[['market_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameMarketsModel::class,
				'targetAttribute' => ['market_id' => 'id'],'on' => self::SCENARIO_CREATE],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class,
				'targetAttribute' => ['sports_id' => 'id'],'on' => self::SCENARIO_CREATE],
			[['id'],'integer','on' => self::SCENARIO_SEARCH]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels(){
		return [
			'id' => Yii::t('games', 'ID'),
			'sports_id' => Yii::t('games', 'Sport'),
			'group_id' => Yii::t('games', 'Group'),
			'market_id' => Yii::t('games', 'Market'),
			'is_enabled' => Yii::t('games', 'Is enabled'),
			'is_main' => Yii::t('games', 'Is main'),
			'short_title' => Yii::t('games', 'Short title'),
			'sort_order' => Yii::t('games', 'Sort order'),
			'condition' => 'Condition',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGroup(){
		return $this->hasOne(GameMarketsGroupsModel::class, ['id' => 'group_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getMarket(){
		return $this->hasOne(GameMarketsModel::class, ['id' => 'market_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports(){
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGameOutcomesLocales(){
		return $this->hasMany(GameOutcomesLocaleModel::class, ['outcome_id' => 'id'])->inverseOf('outcome');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getEventOutcomes(){
		return $this->hasMany(EventOutcomesModel::class, ['outcome_id' => 'id'])->inverseOf('outcome');
	}

	public function getApiGameOutcomes(){
		return $this->hasMany(ApiGameOutcomesModel::class, ['outcome_id' => 'id']);
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = static::find();

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'sports_id' => $this->sports_id,
			'group_id' => $this->group_id,
			'market_id' => $this->market_id,
			'is_enabled' => $this->is_enabled,
			'is_main' => $this->is_main,
		]);

		$query->andFilterWhere(['like', 'short_title', $this->short_title]);

		return $dataProvider;
	}

	protected function getModule() : Module {
		return \Yii::$app->getModule('games');
	}

	/**
	 * @return array
	 */
	static public function statuses(){
		return [
			self::STATUS_ENABLED => Yii::t('games', 'Enabled'),
			self::STATUS_DISABLED => Yii::t('games', 'Disabled'),
		];
	}

}

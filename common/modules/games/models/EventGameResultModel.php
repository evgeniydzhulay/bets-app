<?php

namespace common\modules\games\models;

use Yii;

/**
 * This is the model class for table "{{%event_game_result}}".
 *
 * @property int $id
 * @property int $game_id
 * @property int $sports_id
 * @property string $game_period
 * @property string $result_simple
 * @property string $result_extended
 * @property string $statistics
 * @property string $events
 *
 * @property EventGameModel $game
 */
class EventGameResultModel extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return '{{%event_game_result}}';
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[['game_id', 'sports_id'], 'required'],
			[['game_id', 'sports_id'], 'integer'],
			[['result_extended', 'statistics','events'], 'string'],
			[['game_period'], 'string', 'max' => 100],
			[['result_simple'], 'string', 'max' => 50],
			[['game_id'], 'exist', 'skipOnError' => true, 'targetClass' => EventGameModel::class, 'targetAttribute' => ['game_id' => 'id']],
			[['sports_id'], 'exist', 'skipOnError' => true, 'targetClass' => GameSportsModel::class, 'targetAttribute' => ['sports_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'id' => 'ID',
			'game_id' => 'Game ID',
			'game_period' => 'Game Period',
			'result_simple' => 'Result Simple',
			'result_extended' => 'Result Extended',
			'statistics' => 'Statistics',
			'events' => 'Events',
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getGame() {
		return $this->hasOne(EventGameModel::class, ['id' => 'game_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSports () {
		return $this->hasOne(GameSportsModel::class, ['id' => 'sports_id']);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\queries\EventGameResultQuery the active query used by this AR class.
	 */
	public static function find() {
		return new \common\modules\games\queries\EventGameResultQuery(get_called_class());
	}
}

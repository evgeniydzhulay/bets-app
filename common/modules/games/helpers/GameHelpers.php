<?php

namespace common\modules\games\helpers;

use Yii;
use yii\base\Event;
use yii\web\Response;

class GameHelpers
{
	const STATUS_ENABLED  = 1;
	const STATUS_NO_BANNED = 0;
	const STATUS_VISIBLE = 0;
	const STATUS_UNFINISHED = 0;

	public static function getLocalization($list) {
		foreach($list as $item) {
			if($item['lang_code'] === Yii::$app->language) {
				return $item;
			}
		}
		foreach($list as $item) {
			if($item['lang_code'] === Yii::$app->sourceLanguage) {
				return $item;
			}
		}
		return false;
	}

	public static function formatResponse(Event $event) {
		$response = $event->sender;
		if ($response->format == Response::FORMAT_JSON && $response->data !== null && is_array($response->data)) {
			$response->data['success'] = $response->data['success'] ?? $response->isSuccessful;
			if (!$response->isSuccessful) {
				$response->data['status'] = $response->statusCode;
			}
		}
	}

	/**
	 * @param $string string
	 * @param $arrayNames array ([sort_order => name])
	 * @return string
	 */
	static public function replacingCodeWithName($string, $arrayNames) {
		$search = preg_match('/{[iI]tem([0-9]+)}/',$string,$matches); // [0] - code (string), [1] - sort order / key in array (int)
		if($search && key_exists($matches[1],$arrayNames)){
			$string = str_replace($matches[0],$arrayNames[$matches[1]],$string);
			return self::replacingCodeWithName($string,$arrayNames);
		}
		return $string;
	}

}

<?php

namespace common\modules\games\search;

use common\modules\games\models\BetsModel;
use common\traits\DateToTimeTrait;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class BetsSearch extends BetsModel {

	use DateToTimeTrait;

	protected $pageSize = 20;

	public $created_from;
	public $created_to;

	public $amount_bet_from;
	public $amount_bet_to;

	public $amount_win_from;
	public $amount_win_to;

	public function scenarios () {
		return ArrayHelper::merge(parent::scenarios(), [
			self::SCENARIO_SEARCH => [
				'created_from', 'created_to',
			]
		]);
	}

	public function rules () {
		return parent::rules() + [
			'searchCreated' => [['crated_from', 'created_to'], 'safe', 'on' => [self::SCENARIO_SEARCH]]
		];
	}

	public function load ($data, $formName = null) {
		$loaded = parent::load($data, $formName);
		$this->created_from = $this->created_from ?? date('Y-m-d', time() - 24 * 3600 * 14);
		$this->created_to = $this->created_to ?? date('Y-m-d');
		return$loaded;
	}

	public function search($params) {
		$query = self::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => $this->pageSize,
				'defaultPageSize' => $this->pageSize,
				'forcePageParam' => false,
				'pageSizeLimit' => false,
			],
		]);
		if ($this->load($params) && $this->validate()) {
			$query->andFilterWhere(['>=', 'amount_bet', (!empty($this->amount_bet_from) ? $this->amount_bet_from : null)]);
			$query->andFilterWhere(['<=', 'amount_bet', (!empty($this->amount_bet_to) ? $this->amount_bet_to : null)]);
			$query->andFilterWhere(['>=', 'created_at', self::getTimeFromDate($this->created_from, 'Y-m-d')]);
			$query->andFilterWhere(['<=', 'created_at', self::getTimeFromDate($this->created_to, 'Y-m-d') + 86399]);
			$query->andFilterWhere([
				'id' => $this->id,
				'user_id' => $this->user_id,
				'status' => $this->status,
			]);
		} else {
			$query->andFilterWhere([
				'user_id' => $params['user_id'] ?? null,
			]);
		}
		return $dataProvider;
	}

	public function formName () {
		return '';
	}
}
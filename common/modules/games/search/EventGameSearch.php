<?php

namespace common\modules\games\search;

use common\modules\games\models\EventGameModel;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

class EventGameSearch extends EventGameModel {

	public $starts_from;
	public $starts_to;

	public function formName () {
		return '';
	}

	public function rules () {
		return ArrayHelper::merge(parent::rules(), [
			'startTimeRange' => [['starts_from', 'starts_to'], 'integer']
		]);
	}

	public function scenarios () {
		return [
			self::SCENARIO_SEARCH => ArrayHelper::merge(parent::scenarios()[self::SCENARIO_SEARCH], ['starts_from', 'starts_to']),
		];
	}

	/**
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search ($params) {
		$query = self::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query
		]);
		\Yii::debug($this->scenario);
		if($this->load($params) && $this->validate()) {
			$query->andFilterWhere([
				self::tableName() . '.sports_id' => $this->sports_id,
				self::tableName() . '.tournament_id' => $this->tournament_id,
			])->andFilterWhere([
				'>=', self::tableName() . '.starts_at', !empty($this->starts_from) ? $this->starts_from : NULL
			])->andFilterWhere([
				'<=', self::tableName() . '.starts_at', !empty($this->starts_to) ? $this->starts_to : NULL
			]);
		}
		return $dataProvider;
	}

}

<?php

namespace common\modules\games\components;

use yii\base\Component;

/**
 * @property-read array $list
 */
class MiniGamesComponent extends Component {

	protected $_list = [];

	const CATEGORY_CARDS = 1;
	const CATEGORY_SIMPLE = 2;

	public function getList() {
		if(empty($this->_list)) {
			$this->_list = array_filter(require(dirname(__DIR__) . '/config/minigames-common.php'), function($item) {
				return !empty($item['name']) && !empty($item['url']);
			});
		}
		return $this->_list;
	}
}
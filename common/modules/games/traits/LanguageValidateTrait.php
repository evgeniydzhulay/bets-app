<?php

namespace common\modules\games\traits;

use yii\base\Model;
use yii\db\ActiveQuery;

/**
 * Trait KeyCodeValidateTrait
 * @package common\modules\games\traits
 *
 * @property int $id Current model primary key
 *
 * @method ActiveQuery find() see [[ActiveQuery::find()]] for more info
 */
trait LanguageValidateTrait {

    public function translationNotExists($attributes) {
    	$item = self::find()->andWhere($attributes)->select(['id'])->asArray()->one();
        if (!empty($item) && $item['id'] != $this->id) {
	        /** @var Model $this */
	        $this->addError('lang_code', \Yii::t('content', 'Key must be unique for selected language'));
        }
    }

}

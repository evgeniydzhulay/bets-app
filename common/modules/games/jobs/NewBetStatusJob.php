<?php

namespace common\modules\games\jobs;

use common\modules\games\models\BetsItemsModel;
use yii\base\BaseObject;
use yii\queue\Queue;

class NewBetStatusJob extends BaseObject implements \yii\queue\JobInterface {

	/** @var int Game id */
	public $g;

	/** @var int Outcome id */
	public $o;

	/** @var int Outcome status */
	public $s;

	/**
	 * @param Queue $queue which pushed and is handling the job
	 * @return void|mixed result of the job execution
	 */
	public function execute ($queue) {
		foreach(BetsItemsModel::find()->andWhere([
			'outcome_id' => $this->o,
			'game_id' => $this->g,
		])->andWhere([
			'!=', 'status', $this->s
		])->each() AS $item) {
			$item->setStatus($this->s);
		}
	}
}
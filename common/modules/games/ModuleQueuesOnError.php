<?php

namespace common\modules\games;

use yii\base\Component;
use yii\queue\JobEvent;
use yii\queue\Queue;
use Yii;

class ModuleQueuesOnError extends Component {

	public function init() {
		Yii::$app->getModule('games')->eventQueueOutcomeStatus->on(Queue::EVENT_AFTER_ERROR, function (JobEvent $event) {
			$event->sender->push($event->job);
		});
		Yii::$app->getModule('games')->eventQueueOutcomeBan->on(Queue::EVENT_AFTER_ERROR, function (JobEvent $event) {
			$event->sender->push($event->job);
		});
	}

}
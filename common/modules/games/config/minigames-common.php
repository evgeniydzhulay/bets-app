<?php

use common\modules\games\components\MiniGamesComponent;
use yii\helpers\ArrayHelper;

return ArrayHelper::merge([
	'roulette' => [
		'name' => 'Roulette',
		'url' => 'https://luxe.bet:8093',
		'categories' => [
		]
	],
	'rock-paper-scissors' => [
		'name' => 'Rock Paper Scissors',
		'url' => 'https://luxe.bet:8094',
		'categories' => [
			MiniGamesComponent::CATEGORY_SIMPLE,
		]
	],
	'heads-or-tails' => [
		'name' => 'Heads or Tails',
		'url' => 'https://luxe.bet:8092',
		'categories' => [
			MiniGamesComponent::CATEGORY_SIMPLE,
		]
	],
	'lucky-card' => [
		'name' => 'Lucky card',
		'url' => 'https://luxe.bet:8091',
		'categories' => [
			MiniGamesComponent::CATEGORY_SIMPLE,
			MiniGamesComponent::CATEGORY_CARDS
		]
	],
], (is_file(__DIR__ . '/minigames-local.php') ? require_once(__DIR__ . '/minigames-local.php') : []));
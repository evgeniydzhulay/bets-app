<?php

use common\modules\games\components\MiniGamesComponent;
use common\modules\Storage;
use creocoder\flysystem\LocalFilesystem;

return [
	'modules' => [
		'games' => [
			'class' => \common\modules\games\Module::class,
			'components' => [
				'miniGames' => [
					'class' => MiniGamesComponent::class
				],
			],
		],
		'gamesStorage' => [
			'class' => Storage::class,
			'uploadUrl' => '@web/uploads/games',
			'internalUrl' => '@web/uploads/games',
			'uploadDir' => '@uploads/public/games',
			'components' => [
				'storage' => [
					'class' => LocalFilesystem::class,
					'path' => '@uploads/public/games'
				]
			]
		],
	],
	'components' => [
		'eventQueueOutcomeStatus' => [
			'class' => \yii\queue\sync\Queue::class,
		],
		'eventQueueOutcomeBan' => [
			'class' => \yii\queue\sync\Queue::class,
		],
	],
	'params' => [
		'socketGames.path' => '/socket-games',
	],
];

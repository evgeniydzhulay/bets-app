<?php

use common\modules\games\ModuleQueuesOnError;

return [
	'bootstrap' => [
		'eventQueueOnError',
		'eventQueueOutcomeStatus',
		'eventQueueOutcomeBan',
	],
	'components' => [
		'eventQueueOnError' => ModuleQueuesOnError::class
	],
	'controllerMap' => [
		'migrate' => [
			'migrationNamespaces' => [
				'common\modules\games\migrations',
			],
		],
	],
];

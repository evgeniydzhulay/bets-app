<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190828_075843_game_participants_image extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%game_participants}}', 'image', $this->string(255)->after('sports_id'));
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%game_participants}}', 'image');
	}
}

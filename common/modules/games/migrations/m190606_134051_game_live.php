<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190606_134051_game_live extends Migration {

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%event_game}}', 'is_live', $this->integer(1)->defaultValue(0)->unsigned());
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%event_game}}', 'is_live');
	}

}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190528_145206_bet_item_expand extends Migration {

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%bets_items}}', 'sports_id', $this->integer()->after('bet_id')->unsigned());
		$this->createIndex('idx-bets_items-sports', '{{%bets_items}}', 'sports_id');
		$this->addForeignKey('fk-bets_items-sports', '{{%bets_items}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');

		$this->addColumn('{{%bets_items}}', 'game_id', $this->integer()->after('sports_id')->unsigned());
		$this->createIndex('idx-bets_items-game', '{{%bets_items}}', 'game_id');
		$this->addForeignKey('fk-bets_items-game', '{{%bets_items}}', 'game_id', '{{%event_game}}', 'id', 'CASCADE', 'CASCADE');

		$this->addColumn('{{%bets_items}}', 'market_id', $this->integer()->after('game_id')->unsigned());
		$this->createIndex('idx-bets_items-market', '{{%bets_items}}', 'market_id');
		$this->addForeignKey('fk-bets_items-market', '{{%bets_items}}', 'market_id', '{{%game_markets}}', 'id', 'CASCADE', 'CASCADE');

		Yii::$app->db->createCommand('UPDATE {{%bets_items}} INNER JOIN {{%event_outcomes}} ON {{%event_outcomes}}.id = {{%bets_items}}.outcome_id SET {{%bets_items}}.sports_id = {{%event_outcomes}}.sports_id, {{%bets_items}}.game_id = {{%event_outcomes}}.game_id, {{%bets_items}}.market_id = {{%event_outcomes}}.market_id')->execute();

	}

	/** @inheritdoc */
	public function down() {
		$this->dropForeignKey('fk-bets_items-market', '{{%bets_items}}');
		$this->dropForeignKey('fk-bets_items-game', '{{%bets_items}}');
		$this->dropForeignKey('fk-bets_items-sports', '{{%bets_items}}');
		$this->dropColumn('{{%bets_items}}', 'game_id');
		$this->dropColumn('{{%bets_items}}', 'sport_id');
		$this->dropColumn('{{%bets_items}}', 'market_id');
	}
}

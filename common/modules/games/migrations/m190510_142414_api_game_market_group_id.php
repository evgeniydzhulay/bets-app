<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190510_142414_api_game_market_group_id extends Migration {

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%api_game_markets}}', 'group_id', $this->integer()->after('market_id')->unsigned());
		$this->createIndex('idx-api_game_markets-group', '{{%api_game_markets}}', 'group_id');
		$this->addForeignKey('fk-api_game_markets-group', '{{%api_game_markets}}', 'group_id', '{{%game_markets_groups}}', 'id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%api_game_markets}}', 'group_id');
	}
}

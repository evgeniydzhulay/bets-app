<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190519_100528_game_outcomes_condition extends Migration {

	/** @inheritdoc */
	public function safeUp () {
		$this->addColumn('{{%game_outcomes}}', 'condition', $this->string()->after('is_enabled'));
	}

	/** @inheritdoc */
	public function down () {
		$this->dropColumn('{{%game_outcomes}}', 'condition');
	}

}

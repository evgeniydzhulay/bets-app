<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;
use yii\helpers\ArrayHelper;

class m191105_084218_api_fix_take extends Migration {

	/** @inheritdoc */
	public function safeUp () {

		$outcomeIds = ArrayHelper::getColumn((new \yii\db\Query())
			->select('outcome_id')
			->from('{{%api_game_outcomes}}')
			->groupBy('code_api')
			->andHaving(['>', 'COUNT(*)', 1])
			->all(),'outcome_id');

		$this->db->createCommand()->delete('{{%game_outcomes}}', ['id' => $outcomeIds])->execute();

		$this->createIndex('idx-api_game_outcomes-code_api-service_api', '{{%api_game_outcomes}}', 'code_api, service_api', true  );
	}

	/** @inheritdoc */
	public function down () {
		$this->dropIndex('idx-api_game_outcomes-code_api-service_api', '{{%api_game_outcomes}}');
	}
}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190610_152554_game_outcome_status extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%game_outcomes}}', 'status', $this->integer(1)->defaultValue(0)->unsigned());
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%game_outcomes}}', 'status');
	}

}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190718_180652_api_event_game_api_event_id extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%api_event_game}}', 'api_event_id', $this->integer()->unsigned()->notNull()->after('code_api'));
		$this->createIndex('idx-api_event_api_event_id', '{{%api_event_game}}', 'api_event_id');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%api_event_game}}', 'api_event_id');
	}
}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190325_013720_bets_revert_reason extends Migration {

	/** @inheritdoc */
	public function safeUp () {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%bets_reverted}}', [
			'id' => $this->primaryKey()->unsigned(),
			'bet_id' => $this->integer()->unsigned()->notNull(),
			'user_id' => $this->integer()->unsigned()->notNull(),
			'comment' => $this->text(),
			'created_by' => $this->integer()->unsigned(),
			'created_at' => $this->integer(),
		], $tableOptions);

		$this->createIndex('idx-bets_reverted-bet', '{{%bets_reverted}}', 'bet_id');
		$this->createIndex('idx-bets_reverted-user', '{{%bets_reverted}}', 'user_id');

		$this->addForeignKey('fk-bets_reverted-bet', '{{%bets_reverted}}', 'bet_id', '{{%bets}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-bets_reverted-user', '{{%bets_reverted}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down () {
		$this->dropTable('{{%bets_reverted}}');
	}
}
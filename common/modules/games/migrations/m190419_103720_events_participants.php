<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190419_103720_events_participants extends Migration {

	/** @inheritdoc */
	public function safeUp () {
		$this->addColumn('{{%event_participants}}', 'sort_order', $this->integer()->defaultValue(0));
	}

	/** @inheritdoc */
	public function down () {
		$this->dropColumn('{{%event_participants}}', 'sort_order');
	}
}
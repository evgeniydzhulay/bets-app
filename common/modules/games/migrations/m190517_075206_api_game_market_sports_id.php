<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190517_075206_api_game_market_sports_id extends Migration {

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%api_game_markets}}', 'sports_id', $this->integer()->after('group_id')->unsigned());
		$this->createIndex('idx-api_game_markets-sports', '{{%api_game_markets}}', 'sports_id');
		$this->addForeignKey('fk-api_game_markets-sports', '{{%api_game_markets}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%api_game_markets}}', 'sports_id');
	}
}

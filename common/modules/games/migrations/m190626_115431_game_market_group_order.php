<?php

namespace common\modules\games\migrations;

use yii\db\Migration;

class m190626_115431_game_market_group_order extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%game_markets_groups}}', 'sort_order', $this->integer()->defaultValue(0));
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%game_markets_groups}}', 'sort_order');
	}
}

<?php

namespace common\modules\games\migrations;

use yii\db\Migration;

class m190501_072703_event_participants_foreign_key extends Migration {

	/** @inheritdoc */
	public function up() {
		$this->dropForeignKey('fk-event_participants-participant', '{{%event_participants}}');
		$this->addForeignKey('fk-event_participants-participant', '{{%event_participants}}', 'participant_id', '{{%game_participants}}', 'id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down(){
		$this->dropForeignKey('fk-event_participants-participant', '{{%event_participants}}');
		$this->addForeignKey('fk-event_participants-participant', '{{%event_participants}}', 'participant_id', '{{%game_markets}}', 'id', 'CASCADE', 'CASCADE');
	}

}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190724_092223_event_tournament_flag extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%event_tournament}}', 'flag', $this->string(5)->after('starts_at')->defaultValue(null));
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%event_tournament}}', 'flag');
	}
}

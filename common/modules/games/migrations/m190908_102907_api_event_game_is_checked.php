<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190908_102907_api_event_game_is_checked extends Migration {

	/** @inheritdoc */
	public function safeUp () {
		$this->addColumn('{{%api_event_game}}', 'is_checked', $this->integer(1)->after('api_event_id')->defaultValue(0));
	}

	/** @inheritdoc */
	public function down () {
		$this->dropColumn('{{%api_event_game}}', 'is_checked');
	}
}

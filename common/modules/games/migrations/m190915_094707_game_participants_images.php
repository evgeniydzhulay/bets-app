<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;
use common\traits\MigrationTypesTextTrait;

class m190915_094707_game_participants_images extends Migration{

	use MigrationTypesTextTrait;

	/** @inheritdoc */
	public function up() {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%api_game_participants_images}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer()->unsigned(),
			'participant_id' => $this->integer()->unsigned(),
			'service_api' => $this->integer()->unsigned()->notNull(),
			'code_image' => $this->string(100)->notNull(),
			'is_checked' => $this->integer(1)->defaultValue(0),
		], $tableOptions);

		$this->createIndex('idx-api_game_participants_images-sports', '{{%api_game_participants_images}}', 'sports_id');
		$this->createIndex('idx-api_game_participants_images-participant', '{{%api_game_participants_images}}', 'participant_id');
		$this->createIndex('idx-api_game_participants_images-service', '{{%api_game_participants_images}}', 'service_api');

		$this->addForeignKey('fk-api_game_participants_images-sports', '{{%api_game_participants_images}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-api_game_participants_images-participant', '{{%api_game_participants_images}}', 'participant_id', '{{%game_participants}}', 'id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropTable('{{%api_game_participants_images}}');
	}
}

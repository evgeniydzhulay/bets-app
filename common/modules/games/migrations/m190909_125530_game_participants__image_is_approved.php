<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190909_125530_game_participants__image_is_approved extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%game_participants}}', 'image_is_approved', $this->integer(1)->after('image')->defaultValue(0));
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%game_participants}}', 'image_is_approved');
	}
}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190613_152554_event_outcome_status extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->dropColumn('{{%game_outcomes}}', 'status');
		$this->addColumn('{{%event_outcomes}}', 'status', $this->integer(1)->defaultValue(0)->unsigned());
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%event_outcomes}}', 'status');
		$this->addColumn('{{%game_outcomes}}', 'status', $this->integer(1)->defaultValue(0)->unsigned());
	}

}

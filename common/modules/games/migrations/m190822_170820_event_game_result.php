<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;
use common\traits\MigrationTypesTextTrait;

class m190822_170820_event_game_result extends Migration {

	use MigrationTypesTextTrait;

	/** @inheritdoc */
	public function up() {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%event_game_result}}', [
			'id' => $this->primaryKey()->unsigned(),
			'game_id' => $this->integer()->unsigned()->notNull(),
			'game_period' => $this->string(100)->notNull(),
			'result_simple' => $this->string(50)->notNull(),
			'result_extended' => $this->longText(),
			'statistics' => $this->longText(),
			'events' => $this->longText(),
		], $tableOptions);

		$this->createIndex('idx-event_game_result-game', '{{%event_game_result}}', 'game_id');

		$this->addForeignKey('fk-event_game_result-game', '{{%event_game_result}}', 'game_id', '{{%event_game}}', 'id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropTable('{{%event_game_result}}');
	}
}

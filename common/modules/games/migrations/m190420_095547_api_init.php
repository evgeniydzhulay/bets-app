<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190420_095547_api_init extends Migration
{

	/**
	 * Create tables
	 */
	public function up()
	{
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%api_game_sports}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer()->unsigned()->notNull(),
			'service_api' => $this->integer()->unsigned()->notNull(),
			'code_api' => $this->string(100)->notNull(),
		], $tableOptions);

		$this->createIndex('idx-api_game_sports-sports', '{{%api_game_sports}}', 'sports_id');
		$this->createIndex('idx-api_game_sports-service', '{{%api_game_sports}}', 'service_api');
		$this->createIndex('idx-api_game_sports-code', '{{%api_game_sports}}', 'code_api');

		$this->addForeignKey('fk-api_game_sports-sports', '{{%api_game_sports}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%api_game_participants}}', [
			'id' => $this->primaryKey()->unsigned(),
			'participant_id' => $this->integer()->unsigned()->notNull(),
			'service_api' => $this->integer()->unsigned()->notNull(),
			'code_api' => $this->string(100)->notNull(),
		], $tableOptions);

		$this->createIndex('idx-api_game_participants-participant', '{{%api_game_participants}}', 'participant_id');
		$this->createIndex('idx-api_game_participants-service', '{{%api_game_participants}}', 'service_api');
		$this->createIndex('idx-api_game_participants-code', '{{%api_game_participants}}', 'code_api');

		$this->addForeignKey('fk-api_game_participants-participant', '{{%api_game_participants}}', 'participant_id', '{{%game_participants}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%api_game_markets}}', [
			'id' => $this->primaryKey()->unsigned(),
			'market_id' => $this->integer()->unsigned()->notNull(),
			'service_api' => $this->integer()->unsigned()->notNull(),
			'code_api' => $this->string(100)->notNull(),
		], $tableOptions);

		$this->createIndex('idx-api_game_markets-market', '{{%api_game_markets}}', 'market_id');
		$this->createIndex('idx-api_game_markets-service', '{{%api_game_markets}}', 'service_api');
		$this->createIndex('idx-api_game_markets-code', '{{%api_game_markets}}', 'code_api');

		$this->addForeignKey('fk-api_game_markets-market', '{{%api_game_markets}}', 'market_id', '{{%game_markets}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%api_game_outcomes}}', [
			'id' => $this->primaryKey()->unsigned(),
			'outcome_id' => $this->integer()->unsigned()->notNull(),
			'service_api' => $this->integer()->unsigned()->notNull(),
			'code_api' => $this->string(100)->notNull(),
		], $tableOptions);

		$this->createIndex('idx-api_game_outcomes-outcome', '{{%api_game_outcomes}}', 'outcome_id');
		$this->createIndex('idx-api_game_outcomes-service', '{{%api_game_outcomes}}', 'service_api');
		$this->createIndex('idx-api_game_outcomes-code', '{{%api_game_outcomes}}', 'code_api');

		$this->addForeignKey('fk-api_game_outcomes-outcome', '{{%api_game_outcomes}}', 'outcome_id', '{{%game_outcomes}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%api_event_game}}', [
			'id' => $this->primaryKey()->unsigned(),
			'game_id' => $this->integer()->unsigned()->notNull(),
			'service_api' => $this->integer()->unsigned()->notNull(),
			'code_api' => $this->string(100)->notNull(),
		], $tableOptions);

		$this->createIndex('idx-api_event_game-game', '{{%api_event_game}}', 'game_id');
		$this->createIndex('idx-api_event_game-service', '{{%api_event_game}}', 'service_api');
		$this->createIndex('idx-api_event_game-code', '{{%api_event_game}}', 'code_api');

		$this->addForeignKey('fk-api_event_game-game', '{{%api_event_game}}', 'game_id', '{{%event_game}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%api_event_tournament}}', [
			'id' => $this->primaryKey()->unsigned(),
			'tournament_id' => $this->integer()->unsigned()->notNull(),
			'service_api' => $this->integer()->unsigned()->notNull(),
			'code_api' => $this->string(100)->notNull(),
		], $tableOptions);

		$this->createIndex('idx-api_event_tournament-tournament', '{{%api_event_tournament}}', 'tournament_id');
		$this->createIndex('idx-api_event_tournament-service', '{{%api_event_tournament}}', 'service_api');
		$this->createIndex('idx-api_event_tournament-code', '{{%api_event_tournament}}', 'code_api');

		$this->addForeignKey('fk-api_event_tournament-tournament', '{{%api_event_tournament}}', 'tournament_id', '{{%event_tournament}}', 'id', 'CASCADE', 'CASCADE');

	}

	/**
	 * Drop tables
	 */
	public function down()
	{
		$this->dropTable('{{%api_game_sports}}');
		$this->dropTable('{{%api_game_participants}}');
		$this->dropTable('{{%api_game_markets}}');
		$this->dropTable('{{%api_game_outcomes}}');

		$this->dropTable('{{%api_event_game}}');
		$this->dropTable('{{%api_event_tournament}}');
	}
}

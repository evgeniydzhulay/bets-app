<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190808_142223_event_game_outcomes_count extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%event_game}}', 'outcomes_count', $this->integer());

		$time = $this->beginCommand("add {{%event_game}} outcomes count");
		Yii::$app->db->createCommand('UPDATE {{%event_game}} SET `outcomes_count`= (SELECT COUNT(*) AS `count` FROM {{%event_outcomes}} INNER JOIN {{%game_outcomes}} ON {{%event_outcomes}}.`outcome_id`={{%game_outcomes}}.`id` INNER JOIN {{%game_markets_groups}} ON {{%event_outcomes}}.`group_id`={{%game_markets_groups}}.`id` INNER JOIN {{%game_markets}} ON {{%event_outcomes}}.`market_id`={{%game_markets}}.`id` WHERE ({{%event_outcomes}}.`game_id`=`event_game`.`id`) AND (({{%event_outcomes}}.`is_banned`=FALSE) AND ({{%event_outcomes}}.`is_hidden`=FALSE)) AND (({{%event_outcomes}}.`is_enabled`=TRUE) AND ({{%event_outcomes}}.`is_finished`=FALSE)) AND ({{%game_outcomes}}.`is_enabled`=TRUE) AND ({{%game_markets_groups}}.`is_enabled`=TRUE) AND ({{%game_markets}}.`is_enabled`=TRUE))')->execute();
		$this->endCommand($time);

		$this->createIndex('idx-event_game-outcomes-count', '{{%event_game}}', 'outcomes_count');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%event_game}}', 'outcomes_count');
	}
}

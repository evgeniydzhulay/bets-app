<?php

namespace common\modules\games\migrations;

use common\modules\user\rbac\RbacMigration;

//TODO add processing to controllers
class m190829_163302_games_rbac extends RbacMigration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
	public function safeUp() {
		$gameCreate = $this->createPermission('games-create', 'Create games.');
		$gameEdit = $this->createPermission('games-edit', 'Edit games.');
		$gameView = $this->createPermission('games-view', 'View games.');
		$this->assignChild($gameCreate, $gameEdit);
		$this->assignChild($gameEdit, $gameView);

		$betsEdit = $this->createPermission('bets-edit', 'Edit bets.');
		$betsView = $this->createPermission('bets-view', 'View bets.');
		$this->assignChild($betsEdit, $betsView);

		$admin = $this->authManager->getRole('admin');
		$this->assignChild($admin, $gameCreate);
		$this->assignChild($admin, $betsEdit);
	}

	public function safeDown() {}

}
<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m191007_075314_api_event_game_is_main_provider extends Migration {

	/** @inheritdoc */
	public function safeUp () {
		$this->addColumn('{{%api_event_game}}', 'is_main_provider', $this->integer(1)->after('api_event_id')->defaultValue(0));
	}

	/** @inheritdoc */
	public function down () {
		$this->dropColumn('{{%api_event_game}}', 'is_main_provider');
	}
}

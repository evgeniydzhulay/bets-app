<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190502_070021_api_event_game_sports_id extends Migration {

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%api_event_game}}', 'sports_id', $this->integer()->after('game_id')->unsigned());
		$this->createIndex('idx-api_event_game-sports', '{{%api_event_game}}', 'sports_id');
		$this->addForeignKey('fk-api_event_game-sports', '{{%api_event_game}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%api_event_game}}', 'sports_id');
	}
}

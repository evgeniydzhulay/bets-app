<?php

namespace common\modules\games\migrations;

use common\traits\MigrationTypesTextTrait;
use Yii;

class m190226_090000_games_init extends \yii\db\Migration {

	use MigrationTypesTextTrait;

	/**
	 * Create tables.
	 */
	public function up () {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%game_sports}}', [
			'id' => $this->primaryKey()->unsigned(),
			'image' => $this->string(255),
			'sort_order' => $this->integer()->unsigned(),
			'is_enabled' => $this->integer(1)->notNull()->defaultValue(1),
		], $tableOptions);
		$this->createTable('{{%game_sports_locale}}', [
			'id' => $this->primaryKey(),
			'sports_id' => $this->integer()->unsigned(),
			'lang_code' => $this->string(7),
			'title' => $this->string(),
			'description' => $this->longText(),
			'meta_keywords' => $this->longText(),
			'meta_description' => $this->longText(),
		], $tableOptions);
		$this->createIndex('idx-game_sports_locale-sports', '{{%game_sports_locale}}', 'sports_id');
		$this->createIndex('idx-game_sports_locale-lang', '{{%game_sports_locale}}', 'lang_code');
		$this->createIndex('idx-game_sports_locale-unique', '{{%game_sports_locale}}', ['sports_id', 'lang_code'], true);
		$this->addForeignKey('fk-game_sports_locale-sports', '{{%game_sports_locale}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%game_markets_groups}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer()->unsigned(),
			'is_enabled' => $this->integer(1)->defaultValue(1),
		], $tableOptions);
		$this->createIndex('idx-game_markets_groups-sports', '{{%game_markets_groups}}', 'sports_id');
		$this->addForeignKey('fk-game_markets_groups-sports', '{{%game_markets_groups}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%game_markets_groups_locale}}', [
			'id' => $this->primaryKey(),
			'sports_id' => $this->integer()->unsigned(),
			'group_id' => $this->integer()->unsigned(),
			'lang_code' => $this->string(7),
			'title' => $this->string(),
		], $tableOptions);
		$this->createIndex('idx-game_markets_groups_locale-sports', '{{%game_markets_groups_locale}}', 'sports_id');
		$this->createIndex('idx-game_markets_groups_locale-group', '{{%game_markets_groups_locale}}', 'group_id');
		$this->createIndex('idx-game_markets_groups_locale-lang', '{{%game_markets_groups_locale}}', 'lang_code');
		$this->createIndex('idx-game_markets_groups_locale-unique', '{{%game_markets_groups_locale}}', ['group_id', 'lang_code'], true);
		$this->addForeignKey('fk-game_markets_groups_locale-sports', '{{%game_markets_groups_locale}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_markets_groups_locale-group', '{{%game_markets_groups_locale}}', 'group_id', '{{%game_markets_groups}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%game_markets}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer()->unsigned()->notNull(),
			'group_id' => $this->integer()->unsigned()->notNull(),
			'is_main' => $this->integer(1)->defaultValue(0),
			'sort_order' => $this->integer()->unsigned(),
			'is_enabled' => $this->integer(1)->notNull()->defaultValue(1),
		], $tableOptions);
		$this->createIndex('idx-game_markets-sports', '{{%game_markets}}', 'sports_id');
		$this->createIndex('idx-game_markets-group', '{{%game_markets}}', 'group_id');
		$this->addForeignKey('fk-game_markets-sports', '{{%game_markets}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_markets-group', '{{%game_markets}}', 'group_id', '{{%game_markets_groups}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%game_markets_locale}}', [
			'id' => $this->primaryKey(),
			'sports_id' => $this->integer()->unsigned(),
			'market_id' => $this->integer()->unsigned(),
			'group_id' => $this->integer()->unsigned(),
			'lang_code' => $this->string(7),
			'title' => $this->string(),
		], $tableOptions);
		$this->createIndex('idx-game_markets_locale-sports', '{{%game_markets_locale}}', 'sports_id');
		$this->createIndex('idx-game_markets_locale-market', '{{%game_markets_locale}}', 'market_id');
		$this->createIndex('idx-game_markets_locale-group', '{{%game_markets_locale}}', 'group_id');
		$this->createIndex('idx-game_markets_locale-lang', '{{%game_markets_locale}}', 'lang_code');
		$this->createIndex('idx-game_markets_locale-unique', '{{%game_markets_locale}}', ['market_id', 'lang_code'], true);
		$this->addForeignKey('fk-game_markets_locale-sports', '{{%game_markets_locale}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_markets_locale-market', '{{%game_markets_locale}}', 'market_id', '{{%game_markets}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_markets_locale-group', '{{%game_markets_locale}}', 'group_id', '{{%game_markets_groups}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%game_outcomes}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer()->unsigned(),
			'group_id' => $this->integer()->unsigned(),
			'market_id' => $this->integer()->unsigned(),
			'is_enabled' => $this->integer(1)->defaultValue(1),
		], $tableOptions);
		$this->createIndex('idx-game_outcomes-sports', '{{%game_outcomes}}', 'sports_id');
		$this->createIndex('idx-game_outcomes-market_group', '{{%game_outcomes}}', 'group_id');
		$this->createIndex('idx-game_outcomes-market', '{{%game_outcomes}}', 'market_id');
		$this->addForeignKey('fk-game_outcomes-sports', '{{%game_outcomes}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_outcomes-market', '{{%game_outcomes}}', 'market_id', '{{%game_markets}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_outcomes-group', '{{%game_outcomes}}', 'group_id', '{{%game_markets_groups}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%game_outcomes_locale}}', [
			'id' => $this->primaryKey(),
			'sports_id' => $this->integer()->unsigned(),
			'group_id' => $this->integer()->unsigned(),
			'market_id' => $this->integer()->unsigned(),
			'outcome_id' => $this->integer()->unsigned(),
			'lang_code' => $this->string(7),
			'title' => $this->string(),
		], $tableOptions);
		$this->createIndex('idx-game_outcomes_locale-sports', '{{%game_outcomes_locale}}', 'sports_id');
		$this->createIndex('idx-game_outcomes_locale-group', '{{%game_outcomes_locale}}', 'group_id');
		$this->createIndex('idx-game_outcomes_locale-market', '{{%game_outcomes_locale}}', 'market_id');
		$this->createIndex('idx-game_outcomes_locale-outcome', '{{%game_outcomes_locale}}', 'outcome_id');
		$this->createIndex('idx-game_outcomes_locale-unique', '{{%game_outcomes_locale}}', ['outcome_id', 'lang_code'], true);

		$this->addForeignKey('fk-game_outcomes_locale-sports', '{{%game_outcomes_locale}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_outcomes_locale-group', '{{%game_outcomes_locale}}', 'group_id', '{{%game_markets_groups}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_outcomes_locale-market', '{{%game_outcomes_locale}}', 'market_id', '{{%game_markets}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_outcomes_locale-outcome', '{{%game_outcomes_locale}}', 'outcome_id', '{{%game_outcomes}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%game_participants}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer()->unsigned(),
		], $tableOptions);
		$this->createIndex('idx-game_participants-sports', '{{%game_participants}}', 'sports_id');
		$this->addForeignKey('fk-game_participants-sports', '{{%game_participants}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%game_participants_locale}}', [
			'id' => $this->primaryKey(),
			'sports_id' => $this->integer()->unsigned(),
			'participant_id' => $this->integer()->unsigned(),
			'lang_code' => $this->string(7),
			'name' => $this->string(),
		], $tableOptions);
		$this->createIndex('idx-game_participants_locale-sports', '{{%game_participants_locale}}', 'sports_id');
		$this->createIndex('idx-game_participants_locale-participant', '{{%game_participants_locale}}', 'participant_id');
		$this->createIndex('idx-game_participants_locale-lang', '{{%game_participants_locale}}', 'lang_code');
		$this->createIndex('idx-game_participants_locale-unique', '{{%game_participants_locale}}', ['participant_id', 'lang_code'], true);

		$this->addForeignKey('fk-game_participants_locale-sports', '{{%game_participants_locale}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_participants_locale-participant', '{{%game_participants_locale}}', 'participant_id', '{{%game_participants}}', 'id', 'CASCADE', 'CASCADE');
	}

	/**
	 * Drop tables.
	 */
	public function down () {
		$this->dropTable('{{%game_participants_locale}}');
		$this->dropTable('{{%game_participants}}');
		$this->dropTable('{{%game_outcomes_locale}}');
		$this->dropTable('{{%game_outcomes}}');
		$this->dropTable('{{%game_markets_locale}}');
		$this->dropTable('{{%game_markets}}');
		$this->dropTable('{{%game_markets_groups_locale}}');
		$this->dropTable('{{%game_markets_groups}}');
		$this->dropTable('{{%game_sports_locale}}');
		$this->dropTable('{{%game_sports}}');
	}
}
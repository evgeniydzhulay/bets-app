<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190518_171441_event_game_is_result extends Migration {

	/** @inheritdoc */
	public function safeUp () {
		$this->addColumn('{{%event_game}}', 'is_resulted', $this->integer(1)->after('is_finished')->defaultValue(0));
	}

	/** @inheritdoc */
	public function down () {
		$this->dropColumn('{{%event_game}}', 'is_resulted');
	}
}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190504_163906_api_event_game_tournament_id extends Migration {

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%api_event_game}}', 'tournament_id', $this->integer()->after('sports_id')->unsigned());
		$this->createIndex('idx-api_event_game-tournament', '{{%api_event_game}}', 'tournament_id');
		$this->addForeignKey('fk-api_event_game-tournament', '{{%api_event_game}}', 'tournament_id', '{{%event_tournament}}', 'id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%api_event_game}}', 'tournament_id');
	}
}

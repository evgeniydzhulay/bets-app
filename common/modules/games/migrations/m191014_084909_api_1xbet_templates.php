<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;
use common\traits\MigrationTypesTextTrait;

class m191014_084909_api_1xbet_templates extends Migration {

	use MigrationTypesTextTrait;

	/** @inheritdoc */
	public function up() {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%api_1xbet_template_markets}}', [
			'id' => $this->primaryKey()->unsigned(),
			'market_id' => $this->integer()->unsigned()->notNull(),
			'name' => $this->string(255)->notNull(),
			'count_fields' => $this->integer()->unsigned()->notNull(),
		], $tableOptions);

		$this->createIndex('idx-api_1xbet_template_markets-market', '{{%api_1xbet_template_markets}}', 'market_id');

		$this->createTable('{{%api_1xbet_template_outcomes}}', [
			'id' => $this->primaryKey()->unsigned(),
			'outcome_id' => $this->integer()->unsigned()->notNull(),
			'market_id' => $this->integer()->unsigned()->notNull(),
			'name' => $this->string(255)->notNull(),
			'params' => $this->string(255)->notNull(),
		], $tableOptions);

		$this->createIndex('idx-api_1xbet_template_outcomes-market', '{{%api_1xbet_template_outcomes}}', 'market_id');
		$this->createIndex('idx-api_1xbet_template_outcomes-outcome', '{{%api_1xbet_template_outcomes}}', 'outcome_id');

		$this->addForeignKey('fk-api_1xbet_template_outcomes-market', '{{%api_1xbet_template_outcomes}}', 'market_id', '{{%api_1xbet_template_markets}}', 'market_id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropTable('{{%api_1xbet_template_markets}}');
		$this->dropTable('{{%api_1xbet_template_outcomes}}');
	}
}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190830_073817_event_game_result_sports_id extends Migration {

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%event_game_result}}', 'sports_id', $this->integer()->after('game_id')->unsigned());
		$this->createIndex('idx-event_game_result-sports', '{{%event_game_result}}', 'sports_id');
		$this->addForeignKey('fk-event_game_result-sports', '{{%event_game_result}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%event_game_result}}', 'sports_id');
	}
}

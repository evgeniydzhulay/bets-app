<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;
use yii\helpers\ArrayHelper;

class m191116_104218_added_flag_indexes extends Migration {

	/** @inheritdoc */
	public function safeUp () {
		$this->createIndex('idx-event_game-is_enabled', '{{%event_game}}','is_enabled');
		$this->createIndex('idx-event_game-is_banned', '{{%event_game}}','is_banned');
		$this->createIndex('idx-event_game-is_hidden', '{{%event_game}}','is_hidden');
		$this->createIndex('idx-event_game-is_finished', '{{%event_game}}','is_finished');
		$this->createIndex('idx-event_game-is_resulted', '{{%event_game}}','is_resulted');
		$this->createIndex('idx-event_game-is_live', '{{%event_game}}','is_live');

		$this->createIndex('idx-event_outcomes-is_enabled', '{{%event_outcomes}}','is_enabled');
		$this->createIndex('idx-event_outcomes-is_banned', '{{%event_outcomes}}','is_banned');
		$this->createIndex('idx-event_outcomes-is_hidden', '{{%event_outcomes}}','is_hidden');
		$this->createIndex('idx-event_outcomes-is_finished', '{{%event_outcomes}}','is_finished');

		$this->createIndex('idx-event_tournament-is_enabled', '{{%event_tournament}}','is_enabled');
		$this->createIndex('idx-event_tournament-is_banned', '{{%event_tournament}}','is_banned');
		$this->createIndex('idx-event_tournament-is_hidden', '{{%event_tournament}}','is_hidden');
		$this->createIndex('idx-event_tournament-is_finished', '{{%event_tournament}}','is_finished');

		$this->createIndex('idx-game_markets-is_enabled', '{{%game_markets}}','is_enabled');
		$this->createIndex('idx-game_markets_groups-is_enabled', '{{%game_markets_groups}}','is_enabled');
		$this->createIndex('idx-game_outcomes-is_enabled', '{{%game_outcomes}}','is_enabled');
	}

	/** @inheritdoc */
	public function down () {
		$this->dropIndex('idx-event_game-is_enabled', '{{%event_game}}');
		$this->dropIndex('idx-event_game-is_banned', '{{%event_game}}');
		$this->dropIndex('idx-event_game-is_hidden', '{{%event_game}}');
		$this->dropIndex('idx-event_game-is_finished', '{{%event_game}}');
		$this->dropIndex('idx-event_game-is_resulted', '{{%event_game}}');
		$this->dropIndex('idx-event_game-is_live', '{{%event_game}}');

		$this->dropIndex('idx-event_outcomes-is_enabled', '{{%event_outcomes}}');
		$this->dropIndex('idx-event_outcomes-is_banned', '{{%event_outcomes}}');
		$this->dropIndex('idx-event_outcomes-is_hidden', '{{%event_outcomes}}');
		$this->dropIndex('idx-event_outcomes-is_finished', '{{%event_outcomes}}');

		$this->dropIndex('idx-event_tournament-is_enabled', '{{%event_tournament}}');
		$this->dropIndex('idx-event_tournament-is_banned', '{{%event_tournament}}');
		$this->dropIndex('idx-event_tournament-is_hidden', '{{%event_tournament}}');
		$this->dropIndex('idx-event_tournament-is_finished', '{{%event_tournament}}');

		$this->dropIndex('idx-game_markets-is_enabled', '{{%game_markets}}');
		$this->dropIndex('idx-game_markets_groups-is_enabled', '{{%game_markets_groups}}');
		$this->dropIndex('idx-game_outcomes-is_enabled', '{{%game_outcomes}}');
	}
}

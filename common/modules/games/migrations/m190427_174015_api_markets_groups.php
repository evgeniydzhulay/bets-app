<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190427_174015_api_markets_groups extends Migration
{

	/**
	 * Create tables
	 */
	public function up()
	{
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%api_game_markets_groups}}', [
			'id' => $this->primaryKey()->unsigned(),
			'group_id' => $this->integer()->unsigned()->notNull(),
			'service_api' => $this->integer()->unsigned()->notNull(),
			'code_api' => $this->string(100)->notNull(),
		], $tableOptions);

		$this->createIndex('idx-api_game_markets_groups-group', '{{%api_game_markets_groups}}', 'group_id');
		$this->createIndex('idx-api_game_markets_groups-service', '{{%api_game_markets_groups}}', 'service_api');
		$this->createIndex('idx-api_game_markets_groups-code', '{{%api_game_markets_groups}}', 'code_api');

		$this->addForeignKey('fk-api_game_markets_groups-group', '{{%api_game_markets_groups}}', 'group_id', '{{%game_markets_groups}}', 'id', 'CASCADE', 'CASCADE');
	}

	/**
	 * Drop tables
	 */
	public function down()
	{
		$this->dropTable('{{%api_game_markets_groups}}');
	}
}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190606_064051_game_outcomes_sort_order extends Migration {

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%game_outcomes}}', 'sort_order', $this->integer()->after('is_enabled')->unsigned());
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%game_outcomes}}', 'sort_order');
	}

}

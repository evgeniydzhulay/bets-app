<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190812_102223_game_outcome_groups_additional extends Migration{

	/** @inheritdoc */
	public function up() {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%game_markets_groups_additional}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer(10)->unsigned(),
			'group_id' => $this->integer(10)->unsigned(),
			'market_id' => $this->integer(10)->unsigned(),
			'sort_order' => $this->integer()->defaultValue(0)->unsigned(),
			'is_enabled' => $this->integer(1)->notNull()->defaultValue(1),
		], $tableOptions);

		$this->createIndex('idx-game_markets_groups_additional-sports', '{{%game_markets_groups_additional}}', 'sports_id');
		$this->createIndex('idx-game_markets_groups_additional-group', '{{%game_markets_groups_additional}}', 'group_id');
		$this->createIndex('idx-game_markets_groups_additional-market', '{{%game_markets_groups_additional}}', 'market_id');

		$this->addForeignKey('fk-game_markets_groups_additional-sports', '{{%game_markets_groups_additional}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_markets_groups_additional-group', '{{%game_markets_groups_additional}}', 'group_id', '{{%game_markets_groups}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-game_markets_groups_additional-market', '{{%game_markets_groups_additional}}', 'market_id', '{{%game_markets}}', 'id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropTable('{{%game_markets_groups_additional}}');
	}
}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190718_173401_video_link_for_event_game extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%event_game}}', 'video_link', $this->string(255)->after('is_live')->defaultValue(Null));
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%event_game}}', 'video_link');
	}
}

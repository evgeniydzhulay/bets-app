<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190315_103719_bets_init extends Migration {

	public function safeUp () {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%carts}}', [
			'id' => $this->primaryKey()->unsigned(),
			'key' => $this->string(64)->notNull(),
			'amount' => $this->float(2)->notNull(),
			'on_change' => $this->integer(1),
			'created_at' => $this->integer()
		], $tableOptions);
		$this->createIndex('idx-carts-key', '{{%carts}}', 'key');
		$this->createIndex('idx-carts-created_at', '{{%carts}}', 'created_at');

		$this->createTable('{{%carts_items}}', [
			'id' => $this->primaryKey()->unsigned(),
			'cart_id' => $this->integer()->unsigned()->notNull(),
			'game_id' => $this->integer()->unsigned()->notNull(),
			'sport_id' => $this->integer()->unsigned()->notNull(),
			'market_id' => $this->integer()->unsigned()->notNull(),
			'outcome_id' => $this->integer()->unsigned()->notNull(),
			'rate' => $this->float(2)->notNull(),
		], $tableOptions);

		$this->createIndex('idx-carts_items-cart', '{{%carts_items}}', 'cart_id');
		$this->createIndex('idx-carts_items-sport', '{{%carts_items}}', 'sport_id');
		$this->createIndex('idx-carts_items-game', '{{%carts_items}}', 'game_id');
		$this->createIndex('idx-carts_items-market', '{{%carts_items}}', 'market_id');
		$this->createIndex('idx-carts_items-outcome', '{{%carts_items}}', 'outcome_id');

		$this->addForeignKey('fk-carts_items-game', '{{%carts_items}}', 'game_id', '{{%event_game}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-carts_items-sport', '{{%carts_items}}', 'sport_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-carts_items-market', '{{%carts_items}}', 'market_id', '{{%game_markets}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-carts_items-cart', '{{%carts_items}}', 'cart_id', '{{%carts}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-carts_items-outcome', '{{%carts_items}}', 'outcome_id', '{{%event_outcomes}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%bets}}', [
			'id' => $this->primaryKey()->unsigned(),
			'user_id' => $this->integer()->unsigned()->notNull(),
			'withdraw_id' => $this->integer()->unsigned()->notNull(),
			'refill_id' => $this->integer()->unsigned(),
			'amount_bet' => $this->float(2)->notNull(),
			'amount_win' => $this->float(2)->notNull(),
			'status' => $this->integer(2)->notNull(),
			'created_at' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createIndex('idx-bets-user', '{{%bets}}', 'user_id');
		$this->createIndex('idx-bets-withdraw', '{{%bets}}', 'withdraw_id');
		$this->createIndex('idx-bets-refill', '{{%bets}}', 'refill_id');
		$this->createIndex('idx-bets-amount_bet', '{{%bets}}', 'amount_bet');
		$this->createIndex('idx-bets-amount_win', '{{%bets}}', 'amount_win');

		$this->addForeignKey('fk-bets-user', '{{%bets}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-bets-withdraw', '{{%bets}}', 'withdraw_id', '{{%user_purse_history}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-bets-refill', '{{%bets}}', 'refill_id', '{{%user_purse_history}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%bets_items}}', [
			'id' => $this->primaryKey()->unsigned(),
			'bet_id' => $this->integer()->unsigned()->notNull(),
			'outcome_id' => $this->integer()->unsigned()->notNull(),
			'user_id' => $this->integer()->unsigned()->notNull(),
			'rate' => $this->float(2)->notNull(),
			'status' => $this->integer(2)->notNull(),
		], $tableOptions);

		$this->createIndex('idx-bets_items-bet', '{{%bets_items}}', 'bet_id');
		$this->createIndex('idx-bets_items-outcome', '{{%bets_items}}', 'outcome_id');
		$this->createIndex('idx-bets_items-user', '{{%bets_items}}', 'user_id');
		$this->createIndex('idx-bets_items-status', '{{%bets_items}}', 'status');

		$this->addForeignKey('fk-bets_items-bet', '{{%bets_items}}', 'bet_id', '{{%bets}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-bets_items-outcome', '{{%bets_items}}', 'outcome_id', '{{%event_outcomes}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-bets_items-user', '{{%bets_items}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
	}

	/**
	 * Drop tables.
	 */
	public function down () {
		$this->dropTable('{{%bets_items}}');
		$this->dropTable('{{%bets}}');

		$this->dropTable('{{%carts_items}}');
		$this->dropTable('{{%carts}}');
	}
}
<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190709_162022_game_outcomes_is_main_and_short_title extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%game_outcomes}}', 'is_main', $this->integer(1)->after('is_enabled')->defaultValue(0));
		$this->addColumn('{{%game_outcomes}}', 'short_title', $this->string(10)->after('condition')->defaultValue(null));
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%game_outcomes}}', 'is_main');
		$this->dropColumn('{{%game_outcomes}}', 'short_title');
	}
}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190930_073719_api_event_game_hash extends Migration {

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%api_event_game}}', 'hash', $this->string(32)->after('is_checked'));
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%api_event_game}}', 'hash');
	}
}

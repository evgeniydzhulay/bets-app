<?php

namespace common\modules\games\migrations;

use common\traits\MigrationTypesTextTrait;
use Yii;

class m190226_100000_events_init extends \yii\db\Migration {

	use MigrationTypesTextTrait;

	/**
	 * Create tables.
	 */
	public function up () {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%event_tournament}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer()->unsigned(),
			'starts_at' => $this->integer()->unsigned(),
			'sort_order' => $this->integer()->defaultValue(0),
			'is_enabled' => $this->integer(1)->defaultValue(1),
		], $tableOptions);
		$this->createIndex('idx-event_tournament-sports', '{{%event_tournament}}', 'sports_id');
		$this->addForeignKey('fk-event_tournament-sports', '{{%event_tournament}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%event_tournament_locale}}', [
			'id' => $this->primaryKey(),
			'sports_id' => $this->integer()->unsigned(),
			'tournament_id' => $this->integer()->unsigned(),
			'lang_code' => $this->string(7),
			'title' => $this->string(),
			'description' => $this->longText(),
			'meta_keywords' => $this->longText(),
			'meta_description' => $this->longText(),
		], $tableOptions);
		$this->createIndex('idx-event_tournament_locale-sports', '{{%event_tournament_locale}}', 'sports_id');
		$this->createIndex('idx-event_tournament_locale-tournament', '{{%event_tournament_locale}}', 'tournament_id');
		$this->createIndex('idx-event_tournament_locale-lang', '{{%event_tournament_locale}}', 'lang_code');
		$this->createIndex('idx-event_tournament_locale-unique', '{{%event_tournament_locale}}', ['tournament_id', 'lang_code'], true);
		$this->addForeignKey('fk-event_tournament_locale-tournament', '{{%event_tournament_locale}}', 'tournament_id', '{{%event_tournament}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_tournament_locale-sports', '{{%event_tournament_locale}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');


		$this->createTable('{{%event_game}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer()->unsigned(),
			'tournament_id' => $this->integer()->unsigned(),
			'starts_at' => $this->integer()->unsigned(),
			'sort_order' => $this->integer()->defaultValue(0),
			'is_enabled' => $this->integer(1)->defaultValue(1),
		], $tableOptions);
		$this->createIndex('idx-event_game-sports', '{{%event_game}}', 'sports_id');
		$this->createIndex('idx-event_game-tournament', '{{%event_game}}', 'tournament_id');
		$this->createIndex('idx-event_game-starts_at', '{{%event_game}}', 'starts_at');
		$this->addForeignKey('fk-event_game-sports', '{{%event_game}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_game-tournament', '{{%event_game}}', 'tournament_id', '{{%event_tournament}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%event_game_locale}}', [
			'id' => $this->primaryKey(),
			'sports_id' => $this->integer()->unsigned(),
			'tournament_id' => $this->integer()->unsigned(),
			'game_id' => $this->integer()->unsigned(),
			'lang_code' => $this->string(7),
			'title' => $this->string(),
			'description' => $this->longText(),
			'meta_keywords' => $this->longText(),
			'meta_description' => $this->longText(),
		], $tableOptions);
		$this->createIndex('idx-event_game_locale-sports', '{{%event_game_locale}}', 'sports_id');
		$this->createIndex('idx-event_game_locale-tournament', '{{%event_game_locale}}', 'tournament_id');
		$this->createIndex('idx-event_game_locale-game', '{{%event_game_locale}}', 'game_id');
		$this->createIndex('idx-event_game_locale-lang', '{{%event_game_locale}}', 'lang_code');
		$this->createIndex('idx-event_game_locale-unique', '{{%event_game_locale}}', ['game_id', 'lang_code'], true);
		$this->addForeignKey('fk-event_game_locale-tournament', '{{%event_game_locale}}', 'tournament_id', '{{%event_tournament}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_game_locale-sports', '{{%event_game_locale}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_game_locale-game', '{{%event_game_locale}}', 'game_id', '{{%event_game}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%event_outcomes}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer()->unsigned(),
			'tournament_id' => $this->integer()->unsigned(),
			'game_id' => $this->integer()->unsigned(),
			'market_id' => $this->integer()->unsigned(),
			'group_id' => $this->integer()->unsigned(),
			'outcome_id' => $this->integer()->unsigned(),
			'is_enabled' => $this->integer(1)->defaultValue(1),
			'rate' => $this->float(2)->defaultValue(1),
		], $tableOptions);
		$this->createIndex('idx-event_outcomes-sports', '{{%event_outcomes}}', 'sports_id');
		$this->createIndex('idx-event_outcomes-tournament', '{{%event_outcomes}}', 'tournament_id');
		$this->createIndex('idx-event_outcomes-game', '{{%event_outcomes}}', 'game_id');
		$this->createIndex('idx-event_outcomes-market', '{{%event_outcomes}}', 'market_id');
		$this->createIndex('idx-event_outcomes-group', '{{%event_outcomes}}', 'group_id');
		$this->createIndex('idx-event_outcomes-outcome', '{{%event_outcomes}}', 'outcome_id');

		$this->addForeignKey('fk-event_outcomes-sports', '{{%event_outcomes}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_outcomes-tournament', '{{%event_outcomes}}', 'tournament_id', '{{%event_tournament}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_outcomes-game', '{{%event_outcomes}}', 'game_id', '{{%event_game}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_outcomes-market', '{{%event_outcomes}}', 'market_id', '{{%game_markets}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_outcomes-group', '{{%event_outcomes}}', 'group_id', '{{%game_markets_groups}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_outcomes-outcome', '{{%event_outcomes}}', 'outcome_id', '{{%game_outcomes}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%event_participants}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer()->unsigned(),
			'tournament_id' => $this->integer()->unsigned(),
			'game_id' => $this->integer()->unsigned(),
			'participant_id' => $this->integer()->unsigned(),
		], $tableOptions);

		$this->createIndex('idx-event_participants-sports', '{{%event_participants}}', 'sports_id');
		$this->createIndex('idx-event_participants-tournament', '{{%event_participants}}', 'tournament_id');
		$this->createIndex('idx-event_participants-game', '{{%event_participants}}', 'game_id');
		$this->createIndex('idx-event_participants-participant', '{{%event_participants}}', 'participant_id');

		$this->addForeignKey('fk-event_participants-sports', '{{%event_participants}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_participants-tournament', '{{%event_participants}}', 'tournament_id', '{{%event_tournament}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_participants-game', '{{%event_participants}}', 'game_id', '{{%event_game}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_participants-participant', '{{%event_participants}}', 'participant_id', '{{%game_markets}}', 'id', 'CASCADE', 'CASCADE');

	}

	/**
	 * Drop tables.
	 */
	public function down () {
		$this->dropTable('{{%event_participants}}');
		$this->dropTable('{{%event_outcomes}}');
		$this->dropTable('{{%event_game_locale}}');
		$this->dropTable('{{%event_game}}');
		$this->dropTable('{{%event_tournament_locale}}');
		$this->dropTable('{{%event_tournament}}');
	}
}
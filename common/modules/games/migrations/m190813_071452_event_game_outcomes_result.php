<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190813_071452_event_game_outcomes_result extends Migration {

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%event_game}}', 'result', $this->string(255)->after('outcomes_count')->defaultValue(null));
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%event_game}}', 'result');
	}
}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190609_122554_game_markets_result_type extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%game_markets}}', 'result_type', $this->integer(1)->defaultValue(0)->unsigned());
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%game_markets}}', 'result_type');
	}

}

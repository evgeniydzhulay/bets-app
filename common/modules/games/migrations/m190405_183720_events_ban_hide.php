<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190405_183720_events_ban_hide extends Migration {

	/** @inheritdoc */
	public function safeUp () {
		$this->addColumn('{{%event_game}}', 'is_banned', $this->integer(1)->after('is_enabled')->defaultValue(0));
		$this->addColumn('{{%event_game}}', 'is_hidden', $this->integer(1)->after('is_banned')->defaultValue(0));
		$this->addColumn('{{%event_game}}', 'is_finished', $this->integer(1)->after('is_hidden')->defaultValue(0));

		$this->addColumn('{{%event_outcomes}}', 'is_banned', $this->integer(1)->after('is_enabled')->defaultValue(0));
		$this->addColumn('{{%event_outcomes}}', 'is_hidden', $this->integer(1)->after('is_banned')->defaultValue(0));
		$this->addColumn('{{%event_outcomes}}', 'is_finished', $this->integer(1)->after('is_hidden')->defaultValue(0));

		$this->addColumn('{{%event_tournament}}', 'is_banned', $this->integer(1)->after('is_enabled')->defaultValue(0));
		$this->addColumn('{{%event_tournament}}', 'is_hidden', $this->integer(1)->after('is_banned')->defaultValue(0));
		$this->addColumn('{{%event_tournament}}', 'is_finished', $this->integer(1)->after('is_hidden')->defaultValue(0));
	}

	/** @inheritdoc */
	public function down () {
		$this->dropColumn('{{%event_game}}', 'is_banned');
		$this->dropColumn('{{%event_game}}', 'is_hidden');
		$this->dropColumn('{{%event_game}}', 'is_finished');

		$this->dropColumn('{{%event_outcomes}}', 'is_banned');
		$this->dropColumn('{{%event_outcomes}}', 'is_hidden');
		$this->dropColumn('{{%event_outcomes}}', 'is_finished');

		$this->dropColumn('{{%event_tournament}}', 'is_banned');
		$this->dropColumn('{{%event_tournament}}', 'is_hidden');
		$this->dropColumn('{{%event_tournament}}', 'is_finished');
	}
}
<?php

namespace common\modules\games\migrations;

use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventTournamentLocaleModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use common\modules\games\models\GameParticipantsLocaleModel;
use common\modules\games\models\GameSportsLocaleModel;
use Yii;
use yii\db\Migration;

class m190626_115331_game_locales_inactive extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%event_game_locale}}', 'is_active', $this->integer(1)->after('lang_code')->defaultValue(0));
		EventGameLocaleModel::updateAll(['is_active' => 1], ['lang_code' => 'en']);

		$this->addColumn('{{%event_tournament_locale}}', 'is_active', $this->integer(1)->after('lang_code')->defaultValue(0));
		EventTournamentLocaleModel::updateAll(['is_active' => 1], ['lang_code' => 'en']);

		$this->addColumn('{{%game_markets_groups_locale}}', 'is_active', $this->integer(1)->after('lang_code')->defaultValue(0));
		GameMarketsGroupsLocaleModel::updateAll(['is_active' => 1], ['lang_code' => 'en']);

		$this->addColumn('{{%game_markets_locale}}', 'is_active', $this->integer(1)->after('lang_code')->defaultValue(0));
		GameMarketsLocaleModel::updateAll(['is_active' => 1], ['lang_code' => 'en']);

		$this->addColumn('{{%game_outcomes_locale}}', 'is_active', $this->integer(1)->after('lang_code')->defaultValue(0));
		GameOutcomesLocaleModel::updateAll(['is_active' => 1], ['lang_code' => 'en']);

		$this->addColumn('{{%game_participants_locale}}', 'is_active', $this->integer(1)->after('lang_code')->defaultValue(0));
		GameParticipantsLocaleModel::updateAll(['is_active' => 1], ['lang_code' => 'en']);

		$this->addColumn('{{%game_sports_locale}}', 'is_active', $this->integer(1)->after('lang_code')->defaultValue(0));
		GameSportsLocaleModel::updateAll(['is_active' => 1], ['lang_code' => 'en']);
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%event_game_locale}}', 'is_active');
		$this->dropColumn('{{%event_tournament_locale}}', 'is_active');
		$this->dropColumn('{{%game_markets_groups_locale}}', 'is_active');
		$this->dropColumn('{{%game_markets_locale}}', 'is_active');
		$this->dropColumn('{{%game_outcomes_locale}}', 'is_active');
		$this->dropColumn('{{%game_participants_locale}}', 'is_active');
		$this->dropColumn('{{%game_sports_locale}}', 'is_active');
	}
}

<?php

namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

class m190617_115331_api_game_markets_groups_sports_id extends Migration{

	/** @inheritdoc */
	public function up() {
		$this->addColumn('{{%api_game_markets_groups}}', 'sports_id', $this->integer()->after('group_id')->unsigned());
		$this->createIndex('idx-api_game_markets_groups-sports', '{{%api_game_markets_groups}}', 'sports_id');
		$this->addForeignKey('fk-api_game_markets_groups-sports', '{{%api_game_markets_groups}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
	}

	/** @inheritdoc */
	public function down() {
		$this->dropColumn('{{%api_game_markets_groups}}', 'sports_id');
	}
}

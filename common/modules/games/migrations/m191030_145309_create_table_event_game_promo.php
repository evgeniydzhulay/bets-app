<?php
namespace common\modules\games\migrations;

use Yii;
use yii\db\Migration;

/**
 * Class m191030_145309_create_table_event_game_promo
 */
class m191030_145309_create_table_event_game_promo extends Migration
{
	/**
	 * {@inheritdoc}
	 */
	public function up() {
		$tableOptions = null;
		if(Yii::$app->db->driverName === 'mysql'){
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%event_game_promo}}', [
			'id' => $this->primaryKey()->unsigned(),
			'sports_id' => $this->integer()->unsigned(),
			'game_id' => $this->integer()->unsigned(),
			'outcome_1st_id' => $this->integer()->unsigned(),
			'outcome_2nd_id' => $this->integer()->unsigned(),
		], $tableOptions);

		$this->createIndex('idx-event_game_promo-sports', '{{%event_game_promo}}', 'sports_id');
		$this->createIndex('idx-event_game_promo-game', '{{%event_game_promo}}', 'game_id');
		$this->createIndex('idx-event_game_promo-outcome_1st', '{{%event_game_promo}}', 'outcome_1st_id');
		$this->createIndex('idx-event_game_promo-outcome_2nd', '{{%event_game_promo}}', 'outcome_2nd_id');

		$this->addForeignKey('fk-event_game_promo-sports', '{{%event_game_promo}}', 'sports_id', '{{%game_sports}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_game_promo-game', '{{%event_game_promo}}', 'game_id', '{{%event_game}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_game_promo-outcome_1st', '{{%event_game_promo}}', 'outcome_1st_id', '{{%event_outcomes}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-event_game_promo-outcome_2nd', '{{%event_game_promo}}', 'outcome_2nd_id', '{{%event_outcomes}}', 'id', 'CASCADE', 'CASCADE');
	}

	/**
	 * {@inheritdoc}
	 */
	public function down() {
		$this->dropTable('{{%event_game_promo}}');
	}

}

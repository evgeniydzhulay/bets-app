<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\BetsItemsModel]].
 *
 * @see \common\modules\games\models\BetsItemsModel
 */
class BetsItemsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\BetsItemsModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\BetsItemsModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

<?php

namespace common\modules\games\queries;

use common\modules\games\models\GameMarketsLocaleModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\GameMarketsLocaleModel]].
 *
 * @see \common\modules\games\models\GameMarketsLocaleModel
 */
class GameMarketsLocaleQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return GameMarketsLocaleModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return GameMarketsLocaleModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	public function current() {
		return $this->andWhere([
			GameMarketsLocaleModel::tableName() . '.lang_code' => array_unique([\Yii::$app->language, \Yii::$app->sourceLanguage]),
			'is_active' => 1,
		]);
	}
}

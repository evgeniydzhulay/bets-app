<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\ApiGameParticipantsModel]].
 *
 * @see \common\modules\games\models\ApiGameParticipantsModel
 */
class ApiGameParticipantsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiGameParticipantsModel[]|array
	 */
	public function all($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiGameParticipantsModel|array|null
	 */
	public function one($db = null) {
		return parent::one($db);
	}
}

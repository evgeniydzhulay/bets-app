<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\BetsModel]].
 *
 * @see \common\modules\games\models\BetsModel
 */
class BetsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\BetsModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\BetsModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

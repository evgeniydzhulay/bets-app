<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\CartsModel]].
 *
 * @see \common\modules\games\models\CartsModel
 */
class CartsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\CartsModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\CartsModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

<?php

namespace common\modules\games\queries;

use common\modules\games\models\GameOutcomesModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\GameOutcomesModel]].
 *
 * @see \common\modules\games\models\GameOutcomesModel
 */
class GameOutcomesQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return GameOutcomesModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return GameOutcomesModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	/** @return GameOutcomesQuery */
	public function active() {
		return $this->andWhere([
			GameOutcomesModel::tableName() . '.is_enabled' => 1
		]);
	}
}

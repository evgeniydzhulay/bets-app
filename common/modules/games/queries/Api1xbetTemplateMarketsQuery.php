<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\Api1xbetTemplateMarketsModel]].
 *
 * @see \common\modules\games\models\Api1xbetTemplateMarketsModel
 */
class Api1xbetTemplateMarketsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\Api1xbetTemplateMarketsModel[]|array
	 */
	public function all($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\Api1xbetTemplateMarketsModel|array|null
	 */
	public function one($db = null) {
		return parent::one($db);
	}
}

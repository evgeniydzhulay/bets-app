<?php

namespace common\modules\games\queries;

use common\modules\games\models\EventTournamentModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\EventTournamentModel]].
 *
 * @see \common\modules\games\models\EventTournamentModel
 */
class EventTournamentQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return EventTournamentModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return EventTournamentModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	/** @return EventTournamentQuery */
	public function visible() {
		return $this->andWhere([
			EventTournamentModel::tableName() . '.is_banned' => 0,
			EventTournamentModel::tableName() . '.is_hidden' => 0,
		]);
	}

	/** @return EventTournamentQuery */
	public function active() {
		return $this->visible()->andWhere([
			EventTournamentModel::tableName() . '.is_enabled' => 1,
			EventTournamentModel::tableName() . '.is_finished' => 0,
		]);
	}
}

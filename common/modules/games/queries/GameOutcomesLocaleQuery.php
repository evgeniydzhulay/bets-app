<?php

namespace common\modules\games\queries;

use common\modules\games\models\GameOutcomesLocaleModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\GameOutcomesLocaleModel]].
 *
 * @see \common\modules\games\models\GameOutcomesLocaleModel
 */
class GameOutcomesLocaleQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return GameOutcomesLocaleModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return GameOutcomesLocaleModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	public function current() {
		return $this->andWhere([
			GameOutcomesLocaleModel::tableName() . '.lang_code' => array_unique([\Yii::$app->language, \Yii::$app->sourceLanguage]),
			'is_active' => 1,
		]);
	}
}

<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\ApiGameMarketsGroupsModel]].
 *
 * @see \common\modules\games\models\ApiGameMarketsGroupsModel
 */
class ApiGameMarketsGroupsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiGameMarketsGroupsModel[]|array
	 */
	public function all($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiGameMarketsGroupsModel|array|null
	 */
	public function one($db = null) {
		return parent::one($db);
	}
}

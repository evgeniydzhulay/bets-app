<?php

namespace common\modules\games\queries;
use common\modules\games\models\EventOutcomesModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\EventOutcomesModel]].
 *
 * @see \common\modules\games\models\EventOutcomesModel
 */
class EventOutcomesQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return EventOutcomesModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return EventOutcomesModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	/** @return EventOutcomesQuery */
	public function visible() {
		return $this->andWhere([
			EventOutcomesModel::tableName() . '.is_banned' => 0,
			EventOutcomesModel::tableName() . '.is_hidden' => 0,
		]);
	}

	/** @return EventOutcomesQuery */
	public function active() {
		return $this->visible()->andWhere([
			EventOutcomesModel::tableName() . '.is_enabled' => 1,
			EventOutcomesModel::tableName() . '.is_finished' => 0,
		]);
	}
}

<?php

namespace common\modules\games\queries;

use common\modules\games\models\EventTournamentLocaleModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\EventTournamentLocaleModel]].
 *
 * @see \common\modules\games\models\EventTournamentLocaleModel
 */
class EventTournamentLocaleQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return EventTournamentLocaleModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return EventTournamentLocaleModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	public function current() {
		return $this->andWhere([
			EventTournamentLocaleModel::tableName() . '.lang_code' => array_unique([\Yii::$app->language, \Yii::$app->sourceLanguage]),
			'is_active' => 1,
		]);
	}
}

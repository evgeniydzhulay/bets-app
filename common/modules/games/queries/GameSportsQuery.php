<?php

namespace common\modules\games\queries;

use common\modules\games\models\GameSportsModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\GameSportsModel]].
 *
 * @see \common\modules\games\models\GameSportsModel
 */
class GameSportsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return GameSportsModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return GameSportsModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	/** @return GameSportsQuery */
	public function active() {
		return $this->andWhere([
			GameSportsModel::tableName() . '.is_enabled' => 1
		]);
	}
}

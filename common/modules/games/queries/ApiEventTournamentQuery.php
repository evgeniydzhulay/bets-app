<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\ApiEventTournamentModel]].
 *
 * @see \common\modules\games\models\ApiEventTournamentModel
 */
class ApiEventTournamentQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiEventTournamentModel[]|array
	 */
	public function all($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiEventTournamentModel|array|null
	 */
	public function one($db = null) {
		return parent::one($db);
	}
}

<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\BetsRevertedModel]].
 *
 * @see \common\modules\games\models\BetsRevertedModel
 */
class BetsRevertedQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\BetsRevertedModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\BetsRevertedModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

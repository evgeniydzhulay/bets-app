<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\Api1xbetTemplateOutcomesModel]].
 *
 * @see \common\modules\games\models\Api1xbetTemplateOutcomesModel
 */
class Api1xbetTemplateOutcomesQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\Api1xbetTemplateOutcomesModel[]|array
	 */
	public function all($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\Api1xbetTemplateOutcomesModel|array|null
	 */
	public function one($db = null) {
		return parent::one($db);
	}
}

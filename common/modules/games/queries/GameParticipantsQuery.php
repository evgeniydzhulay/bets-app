<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\GameParticipantsModel]].
 *
 * @see \common\modules\games\models\GameParticipantsModel
 */
class GameParticipantsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\GameParticipantsModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\GameParticipantsModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\ApiGameMarketsModel]].
 *
 * @see \common\modules\games\models\ApiGameMarketsModel
 */
class ApiGameMarketsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiGameMarketsModel[]|array
	 */
	public function all($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiGameMarketsModel|array|null
	 */
	public function one($db = null) {
		return parent::one($db);
	}
}

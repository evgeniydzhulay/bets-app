<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\CartsItemsModel]].
 *
 * @see \common\modules\games\models\CartsItemsModel
 */
class CartsItemsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\CartsItemsModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\CartsItemsModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

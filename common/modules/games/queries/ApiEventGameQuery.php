<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\ApiEventGameModel]].
 *
 * @see \common\modules\games\models\ApiEventGameModel
 */
class ApiEventGameQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiEventGameModel[]|array
	 */
	public function all($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiEventGameModel|array|null
	 */
	public function one($db = null) {
		return parent::one($db);
	}
}

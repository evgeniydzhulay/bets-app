<?php

namespace common\modules\games\queries;
use common\modules\games\models\EventGameModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\EventGameModel]].
 *
 * @see \common\modules\games\models\EventGameModel
 */
class EventGameQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return EventGameModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return EventGameModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	/** @return EventGameQuery */
	public function visible() {
		return $this->andWhere([
			EventGameModel::tableName() . '.is_banned' => 0,
			EventGameModel::tableName() . '.is_hidden' => 0,
		]);
	}

	/** @return EventGameQuery */
	public function active() {
		return $this->visible()->andWhere([
			EventGameModel::tableName() . '.is_enabled' => 1,
			EventGameModel::tableName() . '.is_finished' => 0,
		]);
	}

	public function live(int $isLive) {
		$this->andWhere([EventGameModel::tableName() . '.is_live' => $isLive]);
		if(!$isLive) {
			$this->andWhere(['>=', EventGameModel::tableName() . '.starts_at', time()]);
		}
		return $this;
	}
}

<?php

namespace common\modules\games\queries;

use common\modules\games\models\GameMarketsModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\GameMarketsModel]].
 *
 * @see \common\modules\games\models\GameMarketsModel
 */
class GameMarketsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return GameMarketsModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return GameMarketsModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	/** @return GameMarketsQuery */
	public function active() {
		return $this->andWhere([
			GameMarketsModel::tableName() . '.is_enabled' => 1
		]);
	}
}

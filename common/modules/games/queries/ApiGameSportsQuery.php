<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\ApiGameSportsModel]].
 *
 * @see \common\modules\games\models\ApiGameSportsModel
 */
class ApiGameSportsQuery extends \yii\db\ActiveQuery{

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiGameSportsModel[]|array
	 */
	public function all($db = null){
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\ApiGameSportsModel|array|null
	 */
	public function one($db = null){
		return parent::one($db);
	}
}

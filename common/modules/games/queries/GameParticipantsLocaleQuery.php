<?php

namespace common\modules\games\queries;

use common\modules\games\models\GameParticipantsLocaleModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\GameParticipantsLocaleModel]].
 *
 * @see \common\modules\games\models\GameParticipantsLocaleModel
 */
class GameParticipantsLocaleQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return GameParticipantsLocaleModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return GameParticipantsLocaleModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	public function current() {
		return $this->andWhere([
			GameParticipantsLocaleModel::tableName() . '.lang_code' => array_unique([\Yii::$app->language, \Yii::$app->sourceLanguage]),
			'is_active' => 1,
		]);
	}
}

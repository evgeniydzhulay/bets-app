<?php

namespace common\modules\games\queries;

use common\modules\games\models\GameMarketsGroupsAdditionalModel;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\GameMarketsGroupsAdditionalModel]].
 *
 * @see \common\modules\games\models\GameMarketsGroupsAdditionalModel
 */
class GameMarketsGroupsAdditionalQuery extends ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return GameMarketsGroupsAdditionalModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return GameMarketsGroupsAdditionalModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	/** @return GameMarketsGroupsAdditionalQuery */
	public function active() {
		return $this->andWhere([
			GameMarketsGroupsAdditionalModel::tableName() . '.is_enabled' => 1
		]);
	}
}

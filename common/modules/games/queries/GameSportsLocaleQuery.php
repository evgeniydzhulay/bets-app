<?php

namespace common\modules\games\queries;

use common\modules\games\models\GameSportsLocaleModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\GameSportsLocaleModel]].
 *
 * @see \common\modules\games\models\GameSportsLocaleModel
 */
class GameSportsLocaleQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return GameSportsLocaleModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return GameSportsLocaleModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	public function current() {
		return $this->andWhere([
			GameSportsLocaleModel::tableName() . '.lang_code' => array_unique([\Yii::$app->language, \Yii::$app->sourceLanguage]),
			'is_active' => 1,
		]);
	}
}

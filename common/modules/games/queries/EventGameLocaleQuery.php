<?php

namespace common\modules\games\queries;

use common\modules\games\models\EventGameLocaleModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\EventGameLocaleModel]].
 *
 * @see \common\modules\games\models\EventGameLocaleModel
 */
class EventGameLocaleQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return EventGameLocaleModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return EventGameLocaleModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	public function current() {
		return $this->andWhere([
			EventGameLocaleModel::tableName() . '.lang_code' => array_unique([\Yii::$app->language, \Yii::$app->sourceLanguage]),
			'is_active' => 1,
		]);
	}
}

<?php

namespace common\modules\games\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\EventGameResultModel]].
 *
 * @see \common\modules\games\models\EventGameResultModel
 */
class EventGameResultQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\EventGameResultModel[]|array
	 */
	public function all($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\games\models\EventGameResultModel|array|null
	 */
	public function one($db = null) {
		return parent::one($db);
	}
}

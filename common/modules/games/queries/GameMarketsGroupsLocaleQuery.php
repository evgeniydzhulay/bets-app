<?php

namespace common\modules\games\queries;

use common\modules\games\models\GameMarketsGroupsLocaleModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\GameMarketsGroupsLocaleModel]].
 *
 * @see \common\modules\games\models\GameMarketsGroupsLocaleModel
 */
class GameMarketsGroupsLocaleQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return GameMarketsGroupsLocaleModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return GameMarketsGroupsLocaleModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	public function current() {
		return $this->andWhere([
			GameMarketsGroupsLocaleModel::tableName() . '.lang_code' => array_unique([\Yii::$app->language, \Yii::$app->sourceLanguage]),
			'is_active' => 1,
		]);
	}
}

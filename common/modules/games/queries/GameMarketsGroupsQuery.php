<?php

namespace common\modules\games\queries;

use common\modules\games\models\GameMarketsGroupsModel;

/**
 * This is the ActiveQuery class for [[\common\modules\games\models\GameMarketsGroupsModel]].
 *
 * @see \common\modules\games\models\GameMarketsGroupsModel
 */
class GameMarketsGroupsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return GameMarketsGroupsModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return GameMarketsGroupsModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}

	/** @return GameMarketsGroupsQuery */
	public function active() {
		return $this->andWhere([
			GameMarketsGroupsModel::tableName() . '.is_enabled' => 1
		]);
	}
}

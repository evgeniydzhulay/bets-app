<?php

namespace common\modules\games;

use common\modules\games\components\MiniGamesComponent;
use yii\base\InvalidConfigException;
use yii\base\Module as BaseModule;
use yii\redis\Connection;

/**
 * @property Connection $emitter
 * @property-read MiniGamesComponent $miniGames
 */
class Module extends BaseModule {

	public function getCartExpiration() {
		return 2592000; //30 days
	}

	/**
	 * @return Connection
	 * @throws InvalidConfigException
	 */
	public function getEmitter() : Connection {
		return \Yii::$app->get('redis');
	}

	/**
	 * @param string $channel
	 * @param $params
	 * @return bool
	 */
	public function send(string $channel, array $params) {
		if(!$this->emitter) {
			return false;
		}
		try {
			if (is_array($params)) {
				$params = json_encode($params);
			}
			$this->emitter->publish($channel, $params);
		} catch (\Throwable $e) {
			return false;
		}
		return true;
	}

	/**
	 * @return \yii\queue\Queue
	 */
	public function getOutcomeStatusQueue() {
		return $this->eventQueueOutcomeStatus;
	}

	/**
	 * @return \yii\queue\Queue
	 */
	public function getOutcomeBanQueue() {
		return $this->eventQueueOutcomeBan;
	}
}
<?php

namespace common\modules\blog\controllers;

use common\modules\blog\models\PostModel;
use Yii;
use common\modules\blog\models\PostSearch;

class FrontController extends \yii\web\Controller {

    public function actionPosts() {
        $filterModel = Yii::createObject([
            'class' => PostSearch::class,
            'scenario' => 'search'
        ]);
        $request = Yii::$app->request->get();
        $request['active_from'] = time();
        return $this->render('posts', [
            'filterModel' => $filterModel,
            'dataProvider' => $filterModel->search($request),
        ]);
    }

    public function actionPost($key) {
        $post = $this->getPost($key);
        return $this->render('post', [
            'post' => $post
        ]);
    }

    protected function getPost($key) {
        $item = PostModel::findOne(['key' => $key]);
        if ($item) {
            return $item;
        } else {
            throw new \yii\web\NotFoundHttpException(Yii::t('blog', 'The requested post does not exist'));
        }
    }

}

<?php

namespace common\modules\blog\models;

use common\modules\blog\traits\StorageTrait;
use Yii;
use yii\base\Model;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 *
 * @property string $fileName
 * @property \common\modules\Storage $module
 * @property \creocoder\flysystem\Filesystem $storage
 * @property array $response
 */
class FileUploadModel extends Model {

	use StorageTrait;

    public $uploadedFileName = 'upload';

    /**
     * @var UploadedFile
     */
    public $file;
    private $_fileName;

    public function rules() {
        return [
            ['file', 'file', 'extensions' => NULL]
        ];
    }

    public function getFileName() {
        if (!$this->_fileName) {
            $fileName = substr(uniqid(md5(rand()), true), 0, 10);
            $fileName .= md5(Inflector::slug($this->file->baseName));
            $fileName .= '.' . $this->file->extension;
            $this->_fileName = "{$fileName[0]}/{$fileName[1]}/{$fileName}";
        }
        return $this->_fileName;
    }

    public function upload() {
        if ($this->validate()) {
            return $this->getStorage()->writeStream($this->getFileName(), fopen($this->file->tempName, 'r+'));
        }
        return false;
    }

    public function beforeValidate() {
        if (parent::beforeValidate()) {
            if (!$this->file) {
                $this->file = UploadedFile::getInstanceByName($this->uploadedFileName);
            }
            return true;
        }
        return false;
    }

}

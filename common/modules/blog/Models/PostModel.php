<?php

namespace common\modules\blog\models;

use common\modules\blog\traits\ShortContentTrait;
use common\modules\blog\traits\StorageTrait;
use MongoDB\BSON\Timestamp;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class PostModel
 * @package common\modules\blog\models
 *
 * @property int $id [int(11)]
 * @property string $key [varchar(255)]
 * @property string $title [varchar(255)]
 * @property string $image [varchar(255)]
 * @property string $content
 * @property int $active_from [int(11)]
 * @property int $created_at [int(11)]
 * @property int $created_by [int(11)]
 * @property bool $imageUrl
 * @property string $lang_code [varchar(10)]
 *
 * @property TagModel[] $tags
 * @property TagPostModel[] $tagRelation
 */
class PostModel extends ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	use StorageTrait;
    use ShortContentTrait;

    /** @inheritdoc */
    public static function tableName() {
        return '{{%blog_post}}';
    }

	/** @inheritdoc */
    public function scenarios() {
        return [
            self::SCENARIO_CREATE => ['key', 'lang_code', 'title', 'content', 'active_from', 'image'],
	        self::SCENARIO_UPDATE => ['key', 'lang_code', 'title', 'content', 'active_from', 'image'],
	        self::SCENARIO_SEARCH => ['key', 'lang_code', 'title', 'content', 'active_from']
        ];
    }

    public function behaviors () {
	    return [
	    	'timestamp' => [
	    		'class' => TimestampBehavior::class,
			    'updatedAtAttribute' => false,
		    ],
		    'owner' => [
		    	'class' => BlameableBehavior::class,
			    'updatedByAttribute' => false,
		    ]
	    ];
    }

	public function getImageUrl() {
        return $this->image ? $this->getStorageModule()->getUrl($this->image) : false;
    }

	/** @inheritdoc */
    public function rules() {
        return [
            'required' => [['lang_code', 'key', 'title', 'content'], 'required', 'on' => [
	            self::SCENARIO_CREATE, self::SCENARIO_UPDATE
            ]],
            'safeSearch' => [['lang_code', 'key', 'title', 'content', 'active_from'], 'safe', 'on' => [self::SCENARIO_SEARCH]],
            'keyExists' => ['key', 'unique', 'targetAttribute' => ['key', 'lang_code'], 'when' => function(PostModel $model) {
                return $model->key != $model->getOldAttribute('key');
            }],
            [['active_from', 'image'], 'safe']
        ];
    }

	/** @inheritdoc */
	public function attributeLabels() {
		return [
			'active_from'  => Yii::t('blog', 'Active from'),
			'key'  => Yii::t('blog', 'Key'),
			'title'  => Yii::t('blog', 'Title'),
			'content'  => Yii::t('blog', 'Content'),
			'image'  => Yii::t('blog', 'Image'),
			'lang_code'  => Yii::t('blog', 'Language'),
		];
	}
    
    public function getTags() {
        return $this->hasMany(TagModel::class, ['id' => 'tag_id'])->via('tagRelation');
    }

    public function getTagRelation() {
        return $this->hasMany(TagPostModel::class, ['post_id' => 'id']);
    }
    
    public function getShortContent($length = 400){
        return $this->shorten($this->content, $length);
    }

    public function search($params) {
        $query = self::find();
        $dataProvider = new \yii\data\ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['like', 'key', $this->key]);
	        $query->andFilterWhere(['like', 'lang_code', $this->lang_code]);
            $query->andFilterWhere(['like', 'title', $this->title]);
            if(($time = strtotime($this->active_from)) > 0) {
                $query->andFilterWhere(['<', 'active_from', $time]);
            }
        }
        return $dataProvider;
    }

    public function addTag($tag) {
        $tagItem = new TagPostModel();
        $tagItem->setAttributes([
            'post_id' => $this->id,
            'tag_id' => $tag
        ]);
        return $tagItem->save();
    }

    public function saveImage($image, $override = true) {
        $model = Yii::createObject([
            'class' => FileUploadModel::class,
            'file' => $image
        ]);
        if ($model->upload()) {
            if ($override) {
                $this->deleteImage();
            }
            return $model->getFileName();
        } else {
        	Yii::error($model->errors);
            return false;
        }
    }

	/** @inheritdoc */
    public function deleteImage() {
        if($this->image && $this->getStorage()->has($this->image)) {
            return $this->getStorage()->delete($this->image);
        }
        return false;
    }

	/** @inheritdoc */
    public function delete() {
        foreach($this->tagRelation AS $tag) {
            $tag->delete();
        }
        $this->deleteImage();
        return parent::delete();
    }

}

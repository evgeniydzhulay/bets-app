<?php

namespace common\modules\blog\models;

class ImageUploadModel extends FileUploadModel {

    public function rules() {
        return [
            ['file', 'file', 'extensions' => [
                'jpg', 'jpeg', 'png', 'gif'
            ]]
        ];
    }

}

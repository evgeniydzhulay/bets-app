<?php

namespace common\modules\blog\models;

use yii\db\ActiveRecord;

class TagPostModel extends ActiveRecord {

    /** @inheritdoc */
    public static function tableName() {
        return '{{%blog_tag_post}}';
    }

    public function rules(){
        return [
            'required' => [['tag_id', 'post_id'], 'required']
        ];
    }
}

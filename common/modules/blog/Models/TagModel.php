<?php

namespace common\modules\blog\models;

use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * Class TagModel
 * @package common\modules\blog\models
 *
 * @property int $id [int(11)]
 * @property string $key [varchar(255)]
 * @property string $title [varchar(255)]
 * @property string $content
 * @property string $lang_code [varchar(10)]
 */
class TagModel extends ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

    /** @inheritdoc */
    public static function tableName() {
        return '{{%blog_tag}}';
    }

	/** @inheritdoc */
    public function scenarios() {
        return [
	        self::SCENARIO_CREATE => ['key', 'lang_code', 'title', 'content'],
	        self::SCENARIO_UPDATE => ['key', 'lang_code', 'title', 'content'],
	        self::SCENARIO_SEARCH => ['key', 'lang_code', 'title']
        ];
    }

	/** @inheritdoc */
    public function rules() {
        return [
            'required' => [['key', 'lang_code', 'title', 'content'], 'required', 'on' => [self::SCENARIO_CREATE, self::SCENARIO_UPDATE]],
            'safeSearch' => [['key', 'lang_code', 'title', 'content'], 'safe', 'on' => [self::SCENARIO_SEARCH]]
        ];
    }

    public function search($params) {
        $query = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['like', 'key', $this->key]);
	        $query->andFilterWhere(['like', 'lang_code', $this->lang_code]);
            $query->andFilterWhere(['like', 'title', $this->title]);
        }
        return $dataProvider;
    }

}

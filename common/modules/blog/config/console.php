<?php

return [
	'controllerMap' => [
		'migrate' => [
			'migrationNamespaces' => [
				'common\modules\blog\migrations',
			],
		],
	],
];

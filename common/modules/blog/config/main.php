<?php

use common\modules\blog\Module;
use common\modules\Storage;
use creocoder\flysystem\LocalFilesystem;

return [
	'modules' => [
		'blog' => [
			'class' => Module::class,
		],
		'blogStorage' => [
			'class' => Storage::class,
			'uploadUrl' => '@web/uploads/blog',
			'internalUrl' => '@web/uploads/blog',
			'uploadDir' => '@uploads/public/blog',
			'components' => [
				'storage' => [
					'class' => LocalFilesystem::class,
					'path' => '@uploads/public/blog'
				]
			]
		],
	],
];
<?php

namespace common\modules\blog\traits;

use common\modules\Storage;
use creocoder\flysystem\Filesystem;

trait StorageTrait {

	public function getStorageModule() : Storage {
		return \Yii::$app->getModule('blogStorage');
	}
	public function getStorage() : Filesystem {
		return $this->getStorageModule()->get('storage');
	}
}
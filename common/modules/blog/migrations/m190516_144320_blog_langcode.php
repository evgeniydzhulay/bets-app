<?php

namespace common\modules\blog\migrations;

class m190516_144320_blog_langcode extends \yii\db\Migration {

    /**
     * Create tables.
     */
    public function up() {
    	$this->addColumn('{{%blog_post}}', 'lang_code', $this->string(10)->notNull());
	    $this->addColumn('{{%blog_tag}}', 'lang_code', $this->string(10)->notNull());
    }

    /**
     * Drop tables.
     */
    public function down() {
    	$this->dropColumn('{{%blog_post}}', 'lang_code');
	    $this->dropColumn('{{%blog_tag}}', 'lang_code');
    }

}

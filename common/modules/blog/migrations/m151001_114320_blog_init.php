<?php

namespace common\modules\blog\migrations;

use Yii;

class m151001_114320_blog_init extends \yii\db\Migration {

    /**
     * Create tables.
     */
    public function up() {

        $tableOptions = null;
        if (Yii::$app->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%blog_post}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNull(),
            'title' => $this->string(255)->notNull(),
            'image' => $this->string(255),
            'content' => $this->text()->notNull(),
            'active_from' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->createIndex('idx-blog_post-key', '{{%blog_post}}', 'key');
        $this->createIndex('idx-blog_post-title', '{{%blog_post}}', 'title');
        $this->createIndex('idx-blog_post-active_from', '{{%blog_post}}', 'active_from');
        $this->createIndex('idx-blog_post-created_by', '{{%blog_post}}', 'created_by');

        $this->createTable('{{%blog_tag}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNull(),
            'title' => $this->string(255)->notNull(),
            'content' => $this->text()->notNull()
        ], $tableOptions);
        $this->createIndex('idx-blog_tag-key', '{{%blog_tag}}', 'key');
        
        $this->createTable('{{%blog_tag_post}}', [
            'post_id' => $this->integer()->notNull(),
            'tag_id' => $this->integer()->notNull(),
        ], $tableOptions);
        $this->addPrimaryKey('blog_tag', '{{%blog_tag_post}}', ['tag_id', 'post_id']);
        $this->addForeignKey('fk-blog_tag_post-tag', '{{%blog_tag_post}}', 'tag_id', '{{%blog_tag}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('fk-blog_tag_post-post', '{{%blog_tag_post}}', 'post_id', '{{%blog_post}}', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * Drop tables.
     */
    public function down() {
        $this->dropTable('{{%blog_tag_post}}');
        $this->dropTable('{{%blog_post}}');
        $this->dropTable('{{%blog_tag}}');
    }

}

<?php

namespace common\modules\blog\migrations;

use common\modules\user\rbac\RbacMigration;

class m190829_163301_blog_rbac extends RbacMigration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
	public function safeUp() {
		$admin = $this->authManager->getRole('admin');

		$edit = $this->createPermission('blog-edit', 'Edit blog posts.');
		$view = $this->createPermission('blog-view', 'View blog posts.');
		$this->assignChild($edit, $view);

		$this->assignChild($admin, $edit);
	}

	public function safeDown() {}

}
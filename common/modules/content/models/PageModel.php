<?php

namespace common\modules\content\models;

use common\modules\content\traits\KeyLangValidateTrait;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * Page data model
 *
 * @property int $id Page item primary key
 * @property string $key Page key
 * @property string $title Page title
 * @property string $content Page content
 * @property string $lang_code Page language
 *
 * @property string $meta_keywords Page Meta keywords
 * @property string $meta_description Page Meta description
 * @property string $category_key [varchar(255)]
 *
 * @package common\modules\content\models
 */
class PageModel extends ActiveRecord {

    use KeyLangValidateTrait;

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

    /** @inheritdoc */
    public static function tableName() {
        return '{{%content_page}}';
    }

    /** @inheritdoc */
    public function scenarios() {
        return [
	        self::SCENARIO_CREATE => ['key', 'title', 'content', 'lang_code', 'category_key', 'meta_keywords', 'meta_description'],
	        self::SCENARIO_UPDATE => ['key', 'title', 'content', 'lang_code', 'category_key', 'meta_keywords', 'meta_description'],
	        self::SCENARIO_SEARCH => ['key', 'title', 'lang_code', 'category_key'],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels() {
        return [
            'key' => Yii::t('content', 'Key'),
            'title' => Yii::t('content', 'Title'),
            'content' => Yii::t('content', 'Content'),
            'lang_code' => Yii::t('content', 'Language'),
	        'category_key' => Yii::t('content', 'Category'),
            'meta_keywords' => Yii::t('content', 'Meta keywords'),
            'meta_description' => Yii::t('content', 'Meta description'),
        ];
    }

    /**
     * Validation rules
     * @return array
     */
    public function rules() {
        return [
            'required' => [['key', 'title', 'content', 'lang_code'], 'required', 'on' => [
	            self::SCENARIO_CREATE, self::SCENARIO_UPDATE
            ]],
            'keyValid' => ['key', 'validateKeyLangPair', 'on' => [
	            self::SCENARIO_CREATE, self::SCENARIO_UPDATE
            ]],
            'safeSearch' => [['key', 'title', 'lang_code'], 'safe', 'on' => [
            	self::SCENARIO_SEARCH
            ]],
            'categoryExists' => ['category_key', 'exist', 'targetClass' => CategoryModel::class, 'targetAttribute' => 'key', 'on' => [
	            self::SCENARIO_CREATE, self::SCENARIO_UPDATE
            ]],
            'metaSafe' => [['meta_keywords', 'meta_description'], 'safe', 'on' => [
	            self::SCENARIO_CREATE, self::SCENARIO_UPDATE
            ]],
        ];
    }

    /**
     * Search categories list
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['id' => $this->id]);
            $query->andFilterWhere(['lang_code' => $this->lang_code]);
            $query->andFilterWhere(['like', 'key', $this->key]);
            $query->andFilterWhere(['like', 'title', $this->title]);
            $query->andFilterWhere(['like', 'content', $this->content]);
        }
        return $dataProvider;
    }

    /** @inheritdoc */
    public function formName () {
	    return $this->scenario != self::SCENARIO_SEARCH ? parent::formName() : '';
    }

}

<?php

namespace common\modules\content\models;

use common\modules\content\traits\KeyLangValidateTrait;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * Block data model
 *
 * @property int $id Block item primary key
 * @property string $key Block key
 * @property string $title Block title
 * @property string $content Block content
 * @property string $lang_code Block language
 *
 * @package common\modules\content\models
 */
class BlockModel extends ActiveRecord {

    use KeyLangValidateTrait;

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

    /** @inheritdoc */
    public static function tableName() {
        return '{{%content_block}}';
    }
    
    /** @inheritdoc */
    public function scenarios() {
        return [
            self::SCENARIO_CREATE => ['key', 'title', 'content', 'lang_code'],
	        self::SCENARIO_UPDATE => ['key', 'title', 'content', 'lang_code'],
	        self::SCENARIO_SEARCH => ['key', 'title', 'lang_code'],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels(){
        return [
            'key' => Yii::t('content', 'Key'),
            'title' => Yii::t('content', 'Title'),
            'content' => Yii::t('content', 'Content'),
            'lang_code' => Yii::t('content', 'Language'),
        ];
    }

    /**
     * Validation rules
     * @return array
     */
    public function rules() {
        return [
            'required' => [['key', 'title', 'content', 'lang_code'], 'required', 'on' => [
	            self::SCENARIO_CREATE, self::SCENARIO_UPDATE
            ]],
            'keyValid' => ['key', 'validateKeyLangPair', 'on' => [
	            self::SCENARIO_CREATE, self::SCENARIO_UPDATE
            ]],
            'safeSearch' => [['key', 'title', 'lang_code'], 'safe', 'on' => [
            	self::SCENARIO_SEARCH
            ]],
        ];
    }

    /**
     * Search blocks list
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['id' => $this->id]);
            $query->andFilterWhere(['lang_code' => $this->lang_code]);
            $query->andFilterWhere(['like', 'key', $this->key]);
            $query->andFilterWhere(['like', 'title', $this->title]);
            $query->andFilterWhere(['like', 'content', $this->content]);
        }
        return $dataProvider;
    }

	/** @inheritdoc */
	public function formName () {
		return $this->scenario != self::SCENARIO_SEARCH ? parent::formName() : '';
	}

	public static function getItem(string $code, string $lang, bool $checkSource = true)  {
		if(!!($item = self::find()->andWhere(['key' => $code, 'lang_code' => $lang])->limit(1)->one())) {
			return $item;
		}
		if($checkSource && !!($item = self::find()->andWhere(['key' => $code, 'lang_code' => Yii::$app->sourceLanguage])->limit(1)->one())) {
			return $item;
		}
		return null;
	}

	public static function showItem(string $code, string $lang, bool $checkSource = true) {
		if(!!($item = self::getItem($code, $lang, $checkSource))) {
			return "<h4 class='cblock-title'>{$item->title}</h4><div class='cblock-content'>{$item->content}</div>";
		}
		return '';
	}
}

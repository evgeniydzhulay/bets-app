<?php

namespace common\modules\content\models;

use common\modules\content\traits\KeyLangValidateTrait;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;

/**
 * Category data model
 *
 * @property int $id Category item primary key
 * @property string $key Category key
 * @property string $title Category title
 * @property string $description Category description
 * @property string $lang_code Category language
 *
 * @package common\modules\content\models
 */
class CategoryModel extends ActiveRecord {

    use KeyLangValidateTrait;

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

    /** @inheritdoc */
    public static function tableName() {
        return '{{%content_category}}';
    }

    /** @inheritdoc */
    public function scenarios() {
        return [
	        self::SCENARIO_CREATE => ['key', 'title', 'description', 'lang_code'],
	        self::SCENARIO_UPDATE => ['key', 'title', 'description', 'lang_code'],
	        self::SCENARIO_SEARCH => ['key', 'title', 'lang_code'],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels() {
        return [
            'key' => Yii::t('content', 'Key'),
            'title' => Yii::t('content', 'Title'),
            'description' => Yii::t('content', 'Description'),
            'lang_code' => Yii::t('content', 'Language'),
        ];
    }

    /**
     * Validation rules
     * @return array
     */
    public function rules() {
        return [
            'required' => [['key', 'title', 'description', 'lang_code'], 'required', 'on' => [
	            self::SCENARIO_CREATE, self::SCENARIO_UPDATE
            ]],
            'keyValid' => ['key', 'validateKeyLangPair', 'on' => [
	            self::SCENARIO_CREATE, self::SCENARIO_UPDATE
            ]],
            'safeSearch' => [['key', 'title', 'lang_code'], 'safe', 'on' => [
	            self::SCENARIO_SEARCH
            ]],
        ];
    }

    /**
     * Search categories list
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = self::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'defaultOrder' => [
                    'id' => SORT_DESC
                ]
            ]
        ]);
        if ($this->load($params) && $this->validate()) {
            $query->andFilterWhere(['id' => $this->id]);
            $query->andFilterWhere(['lang_code' => $this->lang_code]);
            $query->andFilterWhere(['like', 'key', $this->key]);
            $query->andFilterWhere(['like', 'title', $this->title]);
            $query->andFilterWhere(['like', 'description', $this->description]);
        }
        return $dataProvider;
    }

    /** @inheritdoc */
    public function delete() {
        if (parent::delete()) {
            PageModel::updateAll(['category_key' => null], [
                'category_key' => $this->key,
                'lang_code' => $this->lang_code
            ]);
            return true;
        }
        return false;
    }

	/** @inheritdoc */
	public function formName () {
		return $this->scenario != self::SCENARIO_SEARCH ? parent::formName() : '';
	}

}

<?php

namespace common\modules\content\traits;

/**
 * Trait KeyCodeValidateTrait
 * @package common\modules\content
 */
trait KeyLangValidateTrait {

    public function validateKeyLangPair($attribute) {
        if (($testItem = $this->findOne([
            'key' => $this->key,
            'lang_code' => $this->lang_code,
        ])) && $testItem->id != $this->id) {
            $this->addError($attribute, \Yii::t('content', 'Key must be unique for selected language'));
        }
    }

}

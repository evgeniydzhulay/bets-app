<?php

namespace common\modules\content\migrations;

use common\modules\user\rbac\RbacMigration;

class m160629_083722_content_rbac_init extends RbacMigration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
    public function safeUp() {
	    $admin = $this->authManager->getRole('admin');

	    $create = $this->createPermission('content-create', 'Create content.');
	    $createBlock = $this->createPermission('content-block-edit', 'Create content blocks.');
	    $createCategory = $this->createPermission('content-category-edit', 'Create content category.');
	    $createPage = $this->createPermission('content-page-edit', 'Create content page.');

	    $this->assignChild($create, $createBlock);
	    $this->assignChild($create, $createCategory);
	    $this->assignChild($create, $createPage);

	    $this->assignChild($admin, $createBlock);
	    $this->assignChild($admin, $createCategory);
	    $this->assignChild($admin, $createPage);

    }

    public function safeDown() {
	    $this->removePermission('content-create');
	    $this->removePermission('content-block-edit');
	    $this->removePermission('content-category-edit');
	    $this->removePermission('content-page-edit');
    }

}

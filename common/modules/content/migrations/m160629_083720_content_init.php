<?php

namespace common\modules\content\migrations;

use common\traits\MigrationTypesTextTrait;
use Yii;

class m160629_083720_content_init extends \yii\db\Migration {

	use MigrationTypesTextTrait;

    /**
     * Create tables.
     */
    public function up() {
        $tableOptions = null;
        if (Yii::$app->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%content_page}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNull(),
            'lang_code' => $this->string(10)->notNull(),
            'title' => $this->string(255)->notNull(),
            'content' => $this->longText()->notNull(),
	        'category_key' => $this->string(255),
	        'meta_keywords' => $this->longText(),
	        'meta_description' => $this->longText(),
        ], $tableOptions);
        $this->createIndex('idx-content_page-unique', '{{%content_page}}', ['key', 'lang_code'], true);

        $this->createTable('{{%content_category}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNull(),
            'lang_code' => $this->string(10)->notNull(),
            'title' => $this->string(255)->notNull(),
            'description' => $this->longText()->notNull(),
        ], $tableOptions);
        $this->createIndex('idx-content_category-unique', '{{%content_category}}', ['key', 'lang_code'], true);
        
        $this->createTable('{{%content_block}}', [
            'id' => $this->primaryKey(),
            'key' => $this->string(255)->notNull(),
            'lang_code' => $this->string(10)->notNull(),
            'title' => $this->string(255)->notNull(),
            'content' => $this->longText()->notNull(),
        ], $tableOptions);
        $this->createIndex('idx-content_block-unique', '{{%content_block}}', ['key', 'lang_code'], true);
    }

    /**
     * Drop tables.
     */
    public function down() {
        $this->dropTable('{{%content_block}}');
        $this->dropTable('{{%content_category}}');
        $this->dropTable('{{%content_page}}');
    }

}

<?php

namespace common\modules\content\controllers;

use common\modules\support\forms\RequestCreateForm;
use common\modules\user\models\UserModel;
use common\traits\AjaxValidationTrait;
use Yii;
use common\modules\content\models\PageModel;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;

class ViewController extends Controller {

	use AjaxValidationTrait;

	public function actionContacts() {
		\Yii::$app->view->title = 'Contacts';
		$model = Yii::createObject(RequestCreateForm::class);
		if(!Yii::$app->user->isGuest) {
			/* @var $user UserModel */
			$user = Yii::$app->user->identity;
			$model->user_id = $user->id;
			$model->email = $user->email;
			$model->name = $user->profile->name . ' ' . $user->profile->surname;
		}
		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post()) && ($request = $model->save())) {
			return $this->redirect(['/support/request/chat', 'id' => $request->id, 'key' => $request->key]);
		}
		return $this->render('contacts',[
			'model' => $model
		]);
	}

	public function actionTos() {
		return $this->render('tos');
	}

	public function actionBonuses($key = null) {
		$menu = ArrayHelper::map(PageModel::find()->andWhere([
			'lang_code' => Yii::$app->language,
			'category_key' => 'bonuses'
		])->select([
			'id', 'key', 'title'
		])->orderBy(['key' => SORT_ASC])->asArray()->all(), 'key', function($item) use ($key) {
			return [
				'id' => $item['key'],
				'url' => Url::toRoute(['bonuses', 'key' => $item['key']]),
				'label' => $item['title'],
				'active' => $key == $item['key'],
				'options' => [
					'class' => 'bonuses-element'
				],
				'dropDownOptions' => [
					'id' => "dropdown-{$item['key']}",
				],
			];
		});
		return $this->render('bonuses', [
			'page' => (!is_null($key) ? PageModel::findOne([
				'lang_code' => Yii::$app->language,
				'category_key' => 'bonuses',
				'key' => $key,
			]) : null),
			'menu' => $menu
		]);
	}

	public function actionRules($key = null) {
		$list = PageModel::find()->andWhere([
			'lang_code' => Yii::$app->language,
			'category_key' => 'rules'
		])->select([
			'id', 'key', 'title'
		])->orderBy(['key' => SORT_ASC])->asArray()->all();
		$menu = [];
		foreach($list AS $item) {
			@list(, $main, $sub) = \explode('_', $item['key']);
			if(\is_null($sub)) {
				$menu[$main] = [
					'id' => $item['key'],
					'url' => Url::toRoute(['rules', 'key' => $main]),
					'label' => $item['title'],
					'items' => $menu[$main]['items'] ?? [],
					'active' => $key == $main,
					'options' => [
						'class' => 'rules-element'
					],
					'dropDownOptions' => [
						'id' => "dropdown-{$item['key']}",
					]
				];
			} else {
				if(!\array_key_exists($main, $menu)) {
					$menu[$main] = ['items' => []];
				}
				$menu[$main]['items'][$sub] = [
					'id' => $item['key'],
					'url' => Url::toRoute([
						'rules', 'key' => $main, '#' => $item['key']
					]),
					'label' => $item['title'],
				];
			}
		}
		foreach ($menu AS &$item) {
			if(count($item['items']) === 0) {
				unset($item['items']);
			}
		}

		ksort($menu);
		foreach($menu as &$item){
			if(!empty($item['items'])){
				ksort($item['items']);
			}
		}
		if(!is_null($key)) {
			$pages = PageModel::find()->andWhere([
				'lang_code' => Yii::$app->language,
				'category_key' => 'rules',
			])->andWhere([
				'or',
				['key' => "rules_{$key}"],
				['like', 'key', "rules_{$key}_%", false]
			])->orderBy(['key' => SORT_ASC])->all();

			if($pages != [] && count($pages) > 1){
				$forSort = [];
				foreach($pages as $page){
					@list(,$f,$s) = explode('_',$page->key);
					$forSort[$f.$s] = $page;
				}
				ksort($forSort);
				$pages = $forSort;
			}
		}

		return $this->render('rules', [
			'pages' => $pages ?? [],
			'menu' => $menu
		]);
	}

	public function actionAboutUs() {
		return $this->render('about-us');
	}
}
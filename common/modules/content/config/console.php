<?php

return [
	'controllerMap' => [
		'migrate' => [
			'migrationNamespaces' => [
				'common\modules\content\migrations',
			],
		],
	],
];

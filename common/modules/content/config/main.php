<?php

use common\modules\content\Module;
use common\modules\Storage;
use creocoder\flysystem\LocalFilesystem;

return [
	'modules' => [
		'content' => [
			'class' => Module::class,
		],
		'contentStorage' => [
			'class' => Storage::class,
			'uploadUrl' => '@web/uploads/content',
			'internalUrl' => '@web/uploads/content',
			'uploadDir' => '@uploads/public/content',
			'components' => [
				'storage' => [
					'class' => LocalFilesystem::class,
					'path' => '@uploads/public/content'
				]
			]
		],
	],
];
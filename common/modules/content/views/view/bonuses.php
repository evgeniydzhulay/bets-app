<?php

use common\modules\content\models\PageModel;
use yii\widgets\Pjax;

/**
 * @var $this \yii\web\View
 * @var $page PageModel|null
 * @var $menu array
 */
?>
<div class="container-fluid rules-container">
    <div class="col-md-3 panel-group rules-menu" id="rules-menu">
        <?= \yii\bootstrap\Nav::widget([
            'id' => 'rules-menu',
            'options' => [
                'class' => 'panel-default',
            ],
            'items' => $menu,
        ]); ?>
    </div>
	<?php
	Pjax::begin([
		'id' => 'rules-list',
		'linkSelector' => '#rules-menu a',
		'scrollTo' => true,
		'options' => [
			'class' => 'col-md-9'
		]
	]);
    if(!is_null($page)) { ?>
    <div id="<?= $page->key; ?>" class="panel-group collapse in" aria-expanded="true" style="">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a class="collapse-toggle" href="#<?= $page->key; ?>-collapse" data-toggle="collapse" data-parent="#<?= $page->key; ?>">
                        <?= $page->title; ?>
                    </a>
                </h4>
            </div>
            <div id="<?= $page->key; ?>-collapse" class="in content-rules panel-collapse collapse">
                <div class="panel-body">
                    <?= preg_replace("/\s|&nbsp;/",' ', $page->content); ?>
                </div>
            </div>
        </div>
    </div>
    <?php } ?>
    <script>
        if(!!window.location.hash.length) {
            setTimeout(function() {
                document.getElementById(window.location.hash.substring(1)).scrollIntoView();
            });
        }
    </script>
	<?php
	Pjax::end();
	?>
</div>

<?php
$js = <<< JS
$('#rules-menu .dropdown-toggle').attr('data-toggle', 'off');
$('#rules-menu .dropdown-toggle').on('click', function (event) {
    $(this).parent().toggleClass('open');
    event.stopPropagation();
    return false;
});
JS;
$this->registerJs($js);
<?php

/* @var $this \yii\web\View */

use kartik\form\ActiveForm;

?>

<div class="static_wrap contact_wrap">
    <div class="static_container contact_container">
        <div class="contact_left">
            <div class="contact_left_item">
                <h3>ЭЛЕКТРОННЫЕ АДРЕСА:</h3>
                <ul class="contact_list">
                    <li>
                        <p>Общие вопросы:</p>
                        <a href="#">info@luxebet.com</a>
                    </li>
                    <li>
                        <p>Техническая поддержка:</p>
                        <a href="#">support@luxebet.com</a>
                    </li>
                    <li>
                        <p>Служба безопасности:</p>
                        <a href="#">security@luxebet.com</a>
                    </li>
                    <li>
                        <p>Связи с общественностью и реклама:</p>
                        <a href="#">marketing@luxebet.com</a>
                    </li>
                    <li>
                        <p>Вопросы партнёрства (онлайн):</p>
                        <a href="#">b2b@luxebet.com</a>
                    </li>
                    <li>
                        <p>Вопросы партнёрства (ППС):</p>
                        <a href="#">dealer@luxebet.com</a>
                    </li>
                    <li>
                        <p>Финансовый отдел:</p>
                        <a href="#">accounting@luxebet.com</a>
                    </li>
                </ul>
            </div>
            <div class="contact_left_item">
                <h3>ТЕЛЕФОН:</h3>
                <ul class="contact_list">
                    <li>
                        <p>Звонок бесплатный</p>
                        <a href="tel:+79273273212" class="contact_tell_link">+79273273212</a>
                    </li>
                </ul>
            </div>
        </div>

        <div class="contact_right">
            <h3>ПОЯВИЛИСЬ ВОПРОСЫ ИЛИ ПРЕДЛОЖЕНИЯ?</h3>
            <p>Напишите нам и наши специалисты свяжутся с Вами в течение суток.</p>
            <div class="contact_form">
	            <?php
	            $form = ActiveForm::begin([
		            'type' => ActiveForm::TYPE_VERTICAL,
		            'id' => 'content-block',
		            'class' => 'contact_form',
		            'enableAjaxValidation' => true,
		            'enableClientValidation' => true,
	            ])
	            ?>
                <div class="form_row">
		            <?= $form->field($model, 'email', [
			            'template' => '{error}{input}',
                        'options' => [
                            'class' => 'form_input'
                        ]
                    ])->textInput(['placeholder' => $model->getAttributeLabel('email')]); ?>
		            <?= $form->field($model, 'name', [
			            'template' => '{error}{input}',
			            'options' => [
				            'class' => 'form_input'
			            ]
		            ])->textInput(['placeholder' => $model->getAttributeLabel('name')]) ?>
                </div>
                <div class="form_row">
		            <?= $form->field($model, 'theme', [
			            'template' => '{error}{input}',
			            'options' => [
				            'class' => 'form_input textarea_input'
			            ]
		            ])->textarea(['placeholder' => $model->getAttributeLabel('theme')]) ?>
                </div>
                <div class="form_row">
		            <?= $form->field($model, 'message', [
			            'template' => '{error}{input}',
			            'options' => [
				            'class' => 'form_input textarea_input'
			            ]
		            ])->textarea(['placeholder' => $model->getAttributeLabel('message')]) ?>
                </div>
                <div class="form_btn">
		            <?= \yii\helpers\Html::submitButton(Yii::t('support', 'Send query'), ['class' => 'btn btn-success btn-red']) ?>
                </div>
	            <?php ActiveForm::end() ?>
            </div>
        </div>
    </div>
</div>
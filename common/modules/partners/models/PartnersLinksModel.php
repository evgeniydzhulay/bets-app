<?php

namespace common\modules\partners\models;

use common\modules\partners\queries\PartnersLeadsQuery;
use common\modules\partners\queries\PartnersLinksQuery;
use common\modules\partners\queries\PartnersQuery;
use common\modules\user\helpers\UserPassword;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "{{%partners_links}}".
 *
 * @property int $id
 * @property int $partner_id
 * @property string $link
 * @property string $description
 * @property int $created_at
 * @property int $site_id [int(11) unsigned]
 * @property int $campaign_id [int(11) unsigned]
 * @property string $page_url [varchar(255)]
 * @property string $page_sid [varchar(255)]
 *
 * @property-read PartnersLeadsModel[] $partnersLeads
 * @property-read string $fullLink
 * @property-read PartnersModel $partner
 * @property PartnersSiteModel $site
 * @property PromoCampaignsModel $campaign
 */
class PartnersLinksModel extends ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	/** {@inheritdoc} */
	public function behaviors () {
		return [
			[
				'class' => TimestampBehavior::class,
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
			],
		];
	}

	/** {@inheritdoc} */
	public static function tableName () {
		return '{{%partners_links}}';
	}

	/** @inheritdoc */
	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['partner_id', 'site_id', 'campaign_id', 'description', 'link', 'page_url', 'page_sid'],
			self::SCENARIO_UPDATE => ['description'],
			self::SCENARIO_SEARCH => ['partner_id', 'site_id', 'campaign_id', 'description', 'link', 'page_url', 'page_sid', 'created_at'],
		];
	}

	/**
	 * {@inheritdoc}
	 * @return PartnersLinksQuery the active query used by this AR class.
	 */
	public static function find () {
		return new PartnersLinksQuery(get_called_class());
	}

	/**{@inheritdoc} */
	public function rules () {
		return [
			[['partner_id', 'site_id', 'campaign_id'], 'integer', 'on' => [
				self::SCENARIO_CREATE, self::SCENARIO_UPDATE
			]],
			[['description'], 'string'],
			[['link', 'page_url', 'page_sid'], 'string', 'max' => 255],
			[['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnersModel::class, 'targetAttribute' => ['partner_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE
			]],
			[['site_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnersSiteModel::class, 'targetAttribute' => ['site_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE
			]],
		];
	}

	/**{@inheritdoc} */
	public function attributeLabels () {
		return [
			'id' => Yii::t('partners', 'ID'),
			'partner_id' => Yii::t('partners', 'Partner'),
			'site_id' => Yii::t('partners', 'Site'),
			'campaign_id' => Yii::t('partners', 'Campaign'),
			'link' => Yii::t('partners', 'Link'),
			'page_url' => Yii::t('partners', 'Landing page'),
			'page_sid' => Yii::t('partners', 'SubID'),
			'description' => Yii::t('partners', 'Description'),
			'created_at' => Yii::t('partners', 'Created at'),
		];
	}

	/**
	 * @return PartnersLeadsQuery
	 */
	public function getPartnersLeads () {
		return $this->hasMany(PartnersLeadsModel::class, ['link_id' => 'id'])->inverseOf('link');
	}

	/**
	 * @return PartnersQuery
	 */
	public function getPartner () {
		return $this->hasOne(PartnersModel::class, ['id' => 'partner_id']);
	}

	/**{@inheritdoc} */
	public function beforeSave ($insert) {
		if(parent::beforeSave($insert)) {
			do {
				$link = UserPassword::generate(8);
			} while (self::find()->andWhere(['link' => $link])->count() > 0);
			$this->link = $link;
			return true;
		}
		return false;
	}

	public function getFullLink() {
		return Yii::getAlias('@web/frontend') . Url::toRoute([
			'/promo/link', 'pl' => $this->link
		]);
	}

	public function getSite () {
		return $this->hasOne(PartnersSiteModel::class, ['id' => 'site_id']);
	}

	public function getCampaign () {
		return $this->hasOne(PromoCampaignsModel::class, ['id' => 'campaign_id']);
	}
}

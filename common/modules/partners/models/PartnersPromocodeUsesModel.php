<?php

namespace common\modules\partners\models;

use common\modules\partners\queries\PartnersPromocodesQuery;
use common\modules\partners\queries\PartnersPromocodeUsesQuery;
use common\modules\partners\queries\PartnersQuery;
use common\modules\user\models\UserModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%partners_promocode_uses}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $promocode_id
 * @property int $partner_id
 * @property int $campaign_id
 * @property int $created_at
 *
 * @property PartnersModel $partner
 * @property PartnersPromocodesModel $promocode
 * @property UserModel $user
 */
class PartnersPromocodeUsesModel extends \yii\db\ActiveRecord {

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%partners_promocode_uses}}';
    }

	/** {@inheritdoc} */
	public function behaviors () {
		return [
			[
				'class' => TimestampBehavior::class,
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
			],
		];
	}

    /**
     * {@inheritdoc}
     * @return PartnersPromocodeUsesQuery the active query used by this AR class.
     */
    public static function find() {
        return new PartnersPromocodeUsesQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['user_id', 'promocode_id', 'partner_id', 'campaign_id'], 'integer'],
            [['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnersModel::class, 'targetAttribute' => ['partner_id' => 'id']],
            [['promocode_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnersPromocodesModel::class, 'targetAttribute' => ['promocode_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id']],
	        [['user_id', 'promocode_id'], 'unique', 'targetAttribute' => ['user_id', 'promocode_id'], 'message' => Yii::t('partners', 'You have already used this promocode')],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('partners', 'ID'),
            'user_id' => Yii::t('partners', 'User'),
            'promocode_id' => Yii::t('partners', 'Promocode'),
            'partner_id' => Yii::t('partners', 'Partner ID'),
            'campaign_id' => Yii::t('partners', 'Campaign'),
            'created_at' => Yii::t('partners', 'Created At'),
        ];
    }

    /**
     * @return PartnersQuery
     */
    public function getPartner() {
        return $this->hasOne(PartnersModel::class, ['id' => 'partner_id']);
    }

    /**
     * @return PartnersPromocodesQuery
     */
    public function getPromocode() {
        return $this->hasOne(PartnersPromocodesModel::class, ['id' => 'promocode_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(UserModel::class, ['id' => 'user_id']);
    }
}

<?php

namespace common\modules\partners\models;

use Yii;
use common\modules\partners\queries\PartnersSiteQuery;
use yii\db\ActiveRecord;

/**
 * Class PartnersSiteModel
 * @package common\modules\partners\models
 *
 * @property int $id [int(10) unsigned]
 * @property string $address [varchar(255)]
 * @property int $partner_id [int(11) unsigned]
 * @property int $category_id [int(11)]
 * @property string $lang_code [varchar(10)]
 * @property int $is_active [int(1)]
 */
class PartnersSiteModel extends ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	/** {@inheritdoc} */
	public static function tableName () {
		return '{{%partners_site}}';
	}

	public function formName () {
		return 'partners-site';
	}

	public static function categories() {
		return [
			0 => 'General'
		];
	}

	/** {@inheritdoc} */
	function scenarios () {
		return [
			self::SCENARIO_CREATE => ['address', 'partner_id', 'category_id', 'lang_code'],
			self::SCENARIO_UPDATE => ['is_active'],
			self::SCENARIO_SEARCH => ['address', 'partner_id', 'category_id', 'lang_code', 'is_active'],
		];
	}

	/** {@inheritdoc} */
	public function rules() {
		return [
			'required' => [['address', 'partner_id', 'category_id', 'lang_code'], 'required', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			'safeSearch' => [['address', 'partner_id', 'category_id', 'lang_code'], 'safe', 'on' => [
				self::SCENARIO_SEARCH,
			]],
			'unique' => ['address', 'unique', 'on' => [
				self::SCENARIO_CREATE, self::SCENARIO_UPDATE,
			], 'when' => function(){
				return $this->isAttributeChanged('address');
			}, 'message' => Yii::t('partners', 'Website is already added')],
			'integer' => [['partner_id', 'category_id', 'is_active'], 'integer'],
			'addressString' => [['address'], 'string', 'max' => 255],
			'addressUrl' =>  ['address', 'url', 'defaultScheme' => 'http', 'enableIDN' => true, 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			'language' => [['lang_code'], 'string', 'max' => 10],
		];
	}

	/**
	 * {@inheritdoc}
	 * @return PartnersSiteQuery the active query used by this AR class.
	 */
	public static function find () {
		return new PartnersSiteQuery(get_called_class());
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'id' => Yii::t('partners', 'ID'),
			'partner_id' => Yii::t('partners', 'Partner'),
			'category_id' => Yii::t('partners', 'Website category'),
			'lang_code' => Yii::t('partners', 'Website language'),
			'address' => Yii::t('partners', 'Address'),
			'is_active' => Yii::t('partners','Status')
		];
	}

	public static function statuses (): array {
		return [
			self::STATUS_ACTIVE => Yii::t('partners', 'Active'),
			self::STATUS_INACTIVE => Yii::t('partners', 'Inactive'),
		];
	}
}
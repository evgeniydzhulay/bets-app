<?php

namespace common\modules\partners\models;

use common\modules\partners\queries\PartnersPromocodesQuery;
use common\modules\user\helpers\UserPassword;
use common\modules\user\models\UserModel;
use common\modules\user\models\UserPurseDepositModel;
use common\modules\user\models\UserPurseHistoryModel;
use yii\db\ActiveRecord;
use Yii;

/**
 * Class PartnersPromocodesModel
 * @package common\modules\partners\models
 *
 * @property int $id [int(10) unsigned]
 * @property int $partner_id [int(11) unsigned]
 * @property int $campaign_id [int(11)]
 * @property int $site_id [int(11) unsigned]
 * @property int $is_active [int(1)]
 * @property string $code
 * @property int $amount [int(11)]
 *
 * @property PromoCampaignsModel $campaign
 * @property PartnersSiteModel $site
 */
class PartnersPromocodesModel extends ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	/** {@inheritdoc} */
	public static function tableName () {
		return '{{%partners_promocodes}}';
	}

	/** {@inheritdoc} */
	function scenarios () {
		return [
			self::SCENARIO_CREATE => ['partner_id', 'campaign_id', 'site_id', 'code', 'amount'],
			self::SCENARIO_UPDATE => ['is_active', 'code'],
			self::SCENARIO_SEARCH => ['partner_id', 'campaign_id', 'site_id', 'is_active', 'code', 'amount'],
		];
	}

	/** {@inheritdoc} */
	public function rules() {
		return [
			'required' => [['partner_id', 'campaign_id', 'site_id', 'amount'], 'required', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			'integer' => [['partner_id', 'campaign_id', 'site_id', 'is_active', 'amount'], 'integer'],
		];
	}

	/**
	 * {@inheritdoc}
	 * @return PartnersPromocodesQuery the active query used by this AR class.
	 */
	public static function find () {
		return new PartnersPromocodesQuery(get_called_class());
	}

	/**{@inheritdoc} */
	public function beforeSave ($insert) {
		if(parent::beforeSave($insert)) {
			do {
				$code = UserPassword::generate(8);
			} while (self::find()->andWhere(['code' => $code])->count() > 0);
			$this->code = $code;
			return true;
		}
		return false;
	}

	public function attributeLabels () {
		return [
			'site_id' => Yii::t('partners', 'Website'),
			'partner_id' => Yii::t('partners', 'Partner'),
			'campaign_id' => Yii::t('partners', 'Campaign'),
			'code' => Yii::t('partners', 'Code'),
			'is_active' =>  Yii::t('partners', 'Status'),
			'amount' =>  Yii::t('partners', 'Amount'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getCampaign() {
		return $this->hasOne(PromoCampaignsModel::class, ['id' => 'campaign_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getSite() {
		return $this->hasOne(PartnersSiteModel::class, ['id' => 'site_id']);
	}

	public static function statuses (): array {
		return [
			self::STATUS_ACTIVE => Yii::t('partners', 'Active'),
			self::STATUS_INACTIVE => Yii::t('partners', 'Inactive'),
		];
	}

	public static function isUsed($code, $user) {
		$item = self::findOne(['code'=> $code]);
		if(!$item) {
			return false;
		}
		$usage = Yii::createObject(PartnersPromocodeUsesModel::class);
		$usage->setAttributes([
			'user_id' => $user,
			'promocode_id' => $item->id,
			'partner_id' => $item->partner_id,
			'campaign_id' => $item->campaign_id,
		]);
		$history = Yii::createObject(UserPurseHistoryModel::class);
		$history->setAttributes([
			'user_id' => $user,
			'amount' => $item->amount,
			'type' => UserPurseHistoryModel::TYPE_COUPON,
			'status' => UserPurseHistoryModel::STATUS_SUCCESS,
			'description' => $item->code,
		]);
		$tr = Yii::$app->db->beginTransaction();
		try {
			if($usage->save() && $history->save()) {
				UserModel::updatePurse($user, $item->amount, $history);
				$tr->commit();
			}
			Yii::info($usage->errors);
			return $usage;
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		$tr->rollBack();
		return false;
	}

}
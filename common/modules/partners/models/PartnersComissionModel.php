<?php

namespace common\modules\partners\models;

use common\modules\partners\queries\PartnersComissionQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%partners_comission}}".
 *
 * @property int $id
 * @property int $partner_id
 * @property string $currency_code
 * @property int $deposit_profit
 * @property int $deposit_min
 * @property int $profit_percent
 * @property int $created_at
 *
 * @property PartnersModel $partner
 */
class PartnersComissionModel extends ActiveRecord {


	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['partner_id', 'deposit_profit', 'deposit_min', 'profit_percent', 'created_at','currency_code'],
			self::SCENARIO_UPDATE => ['deposit_profit', 'deposit_min', 'profit_percent'],
			self::SCENARIO_SEARCH => ['id','partner_id', 'deposit_profit', 'deposit_min', 'profit_percent', 'created_at','currency_code'],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%partners_comission}}';
	}

	/**
	 * {@inheritdoc}
	 * @return PartnersComissionQuery the active query used by this AR class.
	 */
	public static function find () {
		return new PartnersComissionQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['partner_id', 'deposit_profit', 'deposit_min', 'profit_percent', 'created_at'], 'integer'],
			[['currency_code'], 'string', 'max' => 255],
			[['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnersModel::class, 'targetAttribute' => ['partner_id' => 'id']],
			[['id','partner_id', 'deposit_profit', 'deposit_min', 'profit_percent', 'created_at','currency_code'],'safe','on' => self::SCENARIO_SEARCH]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('partners', 'ID'),
			'partner_id' => Yii::t('partners', 'Partner ID'),
			'currency_code' => Yii::t('partners', 'Currency Code'),
			'deposit_profit' => Yii::t('partners', 'Deposit Profit'),
			'deposit_min' => Yii::t('partners', 'Deposit Min'),
			'profit_percent' => Yii::t('partners', 'Profit Percent'),
			'created_at' => Yii::t('partners', 'Created At'),
		];
	}

	public function behaviors()
	{
		return [
			[
				'class' => TimestampBehavior::class,
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at'],
				],
			],
		];
	}



	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPartner () {
		return $this->hasOne(PartnersModel::class, ['id' => 'partner_id']);
	}
}

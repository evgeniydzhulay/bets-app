<?php

namespace common\modules\partners\models;

use common\modules\games\models\BetsModel;
use common\modules\partners\queries\PartnersLeadsQuery;
use common\modules\partners\queries\PartnersLinksQuery;
use common\modules\partners\queries\PartnersQuery;
use common\modules\partners\queries\PromoCampaignsQuery;
use common\modules\partners\queries\PromoMaterialsQuery;
use common\modules\user\models\ProfileModel;
use common\modules\user\models\UserModel;
use common\modules\user\models\UserPurseDepositModel;
use common\modules\user\models\UserPurseHistoryModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "{{%partners_leads}}".
 *
 * @property int $id
 * @property int $partner_id
 * @property int $link_id
 * @property int $material_id [int(11) unsigned]
 * @property int $campaign_id [int(11) unsigned]
 * @property string $sid [varchar(255)]
 * @property int $user_id
 * @property float $amount_deposit [float]
 * @property float $amount_profit [float]
 * @property string $amount_currency [varchar(7)]
 * @property int $created_at
 *
 * @property PartnersLinksModel $link
 * @property PartnersModel $partner
 * @property PromoCampaignsModel $campaign
 * @property PromoMaterialsModel $material
 * @property UserModel $user
 * @property ProfileModel $profile
 */
class PartnersLeadsModel extends \yii\db\ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	/**{@inheritdoc} */
	public static function tableName () {
		return '{{%partners_leads}}';
	}

	/** {@inheritdoc} */
	public function behaviors () {
		return [
			[
				'class' => TimestampBehavior::class,
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
			],
		];
	}

	/** @inheritdoc */
	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['partner_id', 'link_id', 'material_id', 'campaign_id', 'sid', 'user_id', 'amount_deposit', 'amount_profit', 'amount_currency'],
			self::SCENARIO_UPDATE => ['amount_deposit', 'amount_profit', 'amount_currency'],
			self::SCENARIO_SEARCH => ['partner_id', 'link_id', 'material_id', 'campaign_id', 'sid', 'user_id', 'amount_deposit', 'amount_profit', 'amount_currency', 'created_at'],
		];
	}

	/**
	 * {@inheritdoc}
	 * @return PartnersLeadsQuery the active query used by this AR class.
	 */
	public static function find () {
		return new PartnersLeadsQuery(get_called_class());
	}

	/**{@inheritdoc} */
	public function rules () {
		return [
			[['partner_id', 'link_id', 'material_id', 'campaign_id', 'user_id', 'created_at'], 'integer'],
			[['amount_deposit', 'amount_profit'], 'number'],
			[['sid', 'amount_currency'], 'string'],
			[['link_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnersLinksModel::class, 'targetAttribute' => ['link_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE
			]],
			[['material_id'], 'exist', 'skipOnError' => true, 'targetClass' => PromoMaterialsModel::class, 'targetAttribute' => ['material_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE
			]],
			[['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => PromoCampaignsModel::class, 'targetAttribute' => ['campaign_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE
			]],
			[['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnersModel::class, 'targetAttribute' => ['partner_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE
			]],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE
			]],
		];
	}

	/**{@inheritdoc} */
	public function attributeLabels () {
		return [
			'id' => Yii::t('partners', 'ID'),
			'partner_id' => Yii::t('partners', 'Partner'),
			'material_id' => Yii::t('partners', 'Material'),
			'campaign_id' => Yii::t('partners', 'Campaign'),
			'link_id' => Yii::t('partners', 'Link'),
			'sid' => Yii::t('partners', 'SubID'),
			'user_id' => Yii::t('partners', 'User'),
			'created_at' => Yii::t('partners', 'Created at'),
			'amount_currency' => Yii::t('user', 'Currency'),
			'amount_deposit' => Yii::t('partners', 'Deposit amount'),
			'amount_profit' => Yii::t('partners', 'Profit'),
		];
	}

	/**
	 * @return PartnersLinksQuery
	 */
	public function getLink () {
		return $this->hasOne(PartnersLinksModel::class, ['id' => 'link_id']);
	}

	/**
	 * @return PromoMaterialsQuery
	 */
	public function getMaterial () {
		return $this->hasOne(PromoMaterialsModel::class, ['id' => 'material_id']);
	}

	/**
	 * @return PromoCampaignsQuery
	 */
	public function getCampaign () {
		return $this->hasOne(PromoCampaignsModel::class, ['id' => 'campaign_id']);
	}

	/**
	 * @return PartnersQuery
	 */
	public function getPartner () {
		return $this->hasOne(PartnersModel::class, ['id' => 'partner_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getUser () {
		return $this->hasOne(UserModel::class, ['id' => 'user_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getProfile () {
		return $this->hasOne(ProfileModel::class, ['user_id' => 'user_id']);
	}

	public static function leadAddTracking(UserModel $user, $partnerLink, $partnerMaterial, $partnerID, $sid) {
		$model = Yii::createObject(PartnersLeadsModel::class);
		$model->scenario = PartnersLeadsModel::SCENARIO_CREATE;
		$model->user_id = $user->id;
		$model->amount_currency = $user->purse_currency;
		$model->sid = $sid;
		if(!empty($partnerLink)) {
			$link = PartnersLinksModel::findOne(['link' => $partnerLink]);
			$model->link_id = $link->id;
			$model->campaign_id = $link->campaign_id;
			$partnerID = $link->partner_id;
		}
		if(!empty($partnerMaterial) && $material = PromoMaterialsModel::findOne(['id' => $partnerMaterial])) {
			$model->material_id = $material->id;
		}
		if(!empty($partnerID) && $partner = PartnersModel::findOne(['id' => $partnerID])) {
			$model->partner_id = $partner->id;
		}
		$model->save(false);
	}

	public static function leadDeposited(UserPurseDepositModel $deposit) {
		if(
			$deposit->status != UserPurseHistoryModel::STATUS_SUCCESS ||
			!($partner = PartnersModel::findOne([
				'id' => self::find()->andWhere(['user_id' => $deposit->user_id])->select(['partner_id'])
			])) || !($comission = $partner->getComissionByCurrency($deposit->user->purse_currency)) ||
			$comission->deposit_profit == 0 ||
			$deposit->amount < self::getRequiredDepositAmount($partner, $deposit->user->purse_currency) ||
			UserPurseDepositModel::find()->andWhere('`user_id` = :user AND `status` = :status AND `id` < :deposit', [
				'user' => $deposit->user_id,
				'status' => UserPurseHistoryModel::STATUS_SUCCESS,
				'deposit' => $deposit->id
			])->exists()
		) {
			return false;
		}
		$profit = self::getDepositProfit($partner, $deposit->amount, $deposit->user->purse_currency);
		$model = Yii::createObject(PartnersEarningsModel::class);
		$model->setAttributes([
			'partner_id' => $partner->id,
			'lead_id' => self::find()->andWhere(['user_id' => $deposit->user_id])->select(['id'])->column()[0] ?? null,
			'amount' => $profit,
			'currency_code' => $deposit->user->purse_currency,
		]);
		return $model->save() && !!self::updateAll([
			'amount_deposit' => $deposit->amount,
			'amount_profit' => $profit,
		], [
			'user_id' => $deposit->user_id
		]);
	}

	public static function leadBetResulted(BetsModel $bet) {
		if(!($partner = PartnersModel::findOne([
			'id' => self::find()->andWhere(['user_id' => $bet->user_id])->select(['partner_id'])
		])) || $partner->profit_percent == 0) {
			return false;
		}
		$model = Yii::createObject(PartnersEarningsModel::class);
		$model->setAttributes([
			'partner_id' => $partner->id,
			'lead_id' => self::find()->andWhere(['user_id' => $bet->user_id])->select(['id'])->column()[0] ?? null,
			'amount' => ($bet->amount_bet - $bet->amount_win) * $partner->profit_percent / 100,
			'currency_code' => $bet->user->purse_currency,
		]);
		return $model->save(false);
	}

	/**
	 * @param PartnersModel $partner
	 * @param $currency
	 * @return int
	 *
	 */
	public static function getProfitPercent(PartnersModel $partner, $currency) {
		$comission = $partner->getComissionByCurrency($currency);
		return $comission ? $comission->profit_percent : 0;
	}

	/**
	 * @param PartnersModel $partner
	 * @param $currency
	 * @return int
	 */
	public static function getRequiredDepositAmount(PartnersModel $partner, $currency) {
		$comission = $partner->getComissionByCurrency($currency);
		return $comission ? $comission->deposit_min : 0;
	}

	/**
	 * @param PartnersModel $partner
	 * @param $amount
	 * @param $currency
	 * @return int
	 */
	public static function getDepositProfit(PartnersModel $partner, $amount, $currency) {
		$comission = $partner->getComissionByCurrency($currency);
		return ($comission && $amount > $comission->deposit_min) ? $comission->deposit_profit : 0;
	}
}

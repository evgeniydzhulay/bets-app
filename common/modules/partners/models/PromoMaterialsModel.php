<?php

namespace common\modules\partners\models;

use backend\modules\partners\actions\FileUploadModel;
use League\Flysystem\Filesystem;
use Yii;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%promo_materials}}".
 *
 * @property int $id
 * @property string $path
 * @property string $title
 * @property int $type
 * @property string $lang_code
 * @property int $size_h
 * @property int $size_w
 * @property string $currency_code
 * @property int $status
 */
class PromoMaterialsModel extends \yii\db\ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const TYPE_GIF = 1;
	const TYPE_PNG = 2;
	const TYPE_JPEG = 3;

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	public static function statuses (): array {
		return [
			self::STATUS_ACTIVE => Yii::t('partners', 'Active'),
			self::STATUS_INACTIVE => Yii::t('partners', 'Inactive'),
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%promo_materials}}';
	}

	public static function types() {
		return [
			self::TYPE_GIF => 'Gif',
			self::TYPE_PNG => 'PNG',
			self::TYPE_JPEG => 'jpeg',
		];
	}
	/** @inheritdoc */
	public function scenarios () {
		return [
			self::SCENARIO_CREATE => ['path', 'title', 'type', 'lang_code', 'currency_code', 'size_h', 'size_w', 'status'],
			self::SCENARIO_UPDATE => ['path', 'title', 'status'],
			self::SCENARIO_SEARCH => ['id', 'path', 'title', 'type', 'lang_code', 'currency_code', 'size_h', 'size_w', 'status'],
		];
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\queries\PromoMaterialsQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\partners\queries\PromoMaterialsQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['type', 'size_h', 'size_w', 'status'], 'integer'],
			[['path', 'title', 'lang_code', 'currency_code'], 'string', 'max' => 255],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => 'ID',
			'path' => Yii::t('partners', 'Path'),
			'title' => Yii::t('partners', 'Title'),
			'type' => Yii::t('partners', 'Type'),
			'lang_code' => Yii::t('app', 'Language'),
			'size' => Yii::t('partners', 'Size'),
			'size_h' => Yii::t('partners', 'Height'),
			'size_w' => Yii::t('partners', 'Width'),
			'currency_code' => Yii::t('app', 'Currency'),
			'status' => Yii::t('partners', 'Status'),
		];
	}

	public function save ($runValidation = true, $attributeNames = null) {
		if(($image = $this->getImage())) {
			$model = Yii::createObject([
				'class' => FileUploadModel::class,
				'file' => $image
			]);
			if ($model->upload()) {
				$this->deleteImage();
				$result = $model->getResponse();
				$this->path = $result['fileName'];
			}
		}
		return parent::save($runValidation, $attributeNames);
	}

	/** {@inheritdoc} */
	protected function getImage() {
		return UploadedFile::getInstance($this, 'path');
	}

	/** {@inheritdoc} */
	public function deleteImage() {
		if(!empty($this->path) && $this->getStorage()->has($this->path)) {
			return $this->getStorage()->delete($this->path);
		}
		return false;
	}

	/** {@inheritdoc} */
	public function delete() {
		if(($res = parent::delete())) {
			$this->deleteImage();
			return $res;
		}
		return false;
	}

	public function getStorage() : Filesystem {
		return Yii::$app->getModule('partnersStorage')->get('storage');
	}
}

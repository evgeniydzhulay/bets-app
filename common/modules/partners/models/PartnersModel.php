<?php

namespace common\modules\partners\models;

use common\modules\partners\queries\PartnersComissionQuery;
use common\modules\partners\queries\PartnersLeadsQuery;
use common\modules\partners\queries\PartnersLinksQuery;
use common\modules\partners\queries\PartnersQuery;
use common\modules\user\helpers\UserPassword;
use common\modules\user\models\ProfileModel;
use common\modules\user\models\UserModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%partners}}".
 *
 * @property int $id
 * @property int $owner_id
 * @property string $api_key_public
 * @property string $api_key_private
 * @property int $status [int(1)]
 * @property int $created_at
 * @property int $payment_method [int(11)]
 * @property string $payment_number [varchar(255)]
 *
 * @property PartnersLinksModel[] $links
 * @property PartnersLeadsModel[] $leads
 * @property PartnersComissionModel[] $comissions
 * @property ProfileModel $ownerProfile
 * @property UserModel $owner
 */
class PartnersModel extends ActiveRecord {

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 9;

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_APIKEY = 'apikey';
	const SCENARIO_SEARCH = 'search';

	public $deposit_profit = 500;
	public $deposit_min = 500;
	public $profit_percent = 15;

	/**
	 * Default partners statuses
	 *
	 * @return array
	 */
	public static function statuses (): array {
		return [
			self::STATUS_ACTIVE => Yii::t('partners', 'Active'),
			self::STATUS_INACTIVE => Yii::t('partners', 'Inactive'),
		];
	}

	/** {@inheritdoc} */
	public static function tableName () {
		return '{{%partners}}';
	}

	/**
	 * {@inheritdoc}
	 * @return PartnersQuery the active query used by this AR class.
	 */
	public static function find () {
		return new PartnersQuery(get_called_class());
	}

	/** {@inheritdoc} */
	public function behaviors () {
		return [
			[
				'class' => TimestampBehavior::class,
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
			],
		];
	}

	/** @inheritdoc */
	public function scenarios () {
		return [
			self::SCENARIO_CREATE => ['owner_id', 'payment_method', 'payment_number', 'api_key_public', 'api_key_private', 'status'],
			self::SCENARIO_UPDATE => ['status'],
			self::SCENARIO_APIKEY => ['api_key_public', 'api_key_private'],
			self::SCENARIO_SEARCH => ['id', 'owner_id', 'payment_method', 'payment_number', 'api_key_public', 'api_key_private', 'status'],
		];
	}

	/** {@inheritdoc} */
	public function rules () {
		return [
			[['payment_method', 'owner_id'], 'integer', 'on' => [
				self::SCENARIO_CREATE, self::SCENARIO_UPDATE,
			]],
			'serviceExists' => ['payment_method',  'in', 'range' => \array_keys(PartnersWithdrawModel::paymentMethods())],
			[['payment_number'], 'string', 'max' => 255, 'on' => [
				self::SCENARIO_CREATE, self::SCENARIO_UPDATE,
			]],
			[['api_key_public'], 'string', 'max' => 32],
			[['api_key_private'], 'string', 'max' => 64],
			[['owner_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['owner_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE, self::SCENARIO_UPDATE,
			]],
			'safeSearch' => [['key', 'title', 'lang_code'], 'safe', 'on' => [
				self::SCENARIO_SEARCH,
			]],
		];
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'id' => Yii::t('partners', 'ID'),
			'company' => Yii::t('partners', 'Company'),
			'owner_id' => Yii::t('partners', 'Owner'),
			'description' => Yii::t('partners', 'Description'),
			'api_key_public' => Yii::t('partners', 'Public Api key'),
			'api_key_private' => Yii::t('partners', 'Private Api Key'),
			'status' => Yii::t('partners', 'Status'),
			'created_at' => Yii::t('partners', 'Created at'),
		];
	}

	/**
	 * @return ActiveQuery
	 */
	public function getOwner () {
		return $this->hasOne(UserModel::class, ['id' => 'owner_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getOwnerProfile () {
		return $this->hasOne(ProfileModel::class, ['user_id' => 'owner_id']);
	}

	/**
	 * @return PartnersLeadsQuery
	 */
	public function getLeads () {
		return $this->hasMany(PartnersLeadsModel::class, ['partner_id' => 'id'])->inverseOf('partner');
	}

	/** @return PartnersComissionQuery */
	public function getComissions () {
		return $this->hasMany(PartnersComissionModel::class, ['partner_id' => 'id'])->inverseOf('partner');
	}

	/**
	 * @param $currency
	 * @return PartnersComissionModel
	 */
	public function getComissionByCurrency ($currency) {
		foreach ($this->comissions AS $comission) {
			if($comission->currency_code === $currency) {
				return $comission;
			}
		}
	}

	/**
	 * @return PartnersLinksQuery
	 */
	public function getLinks () {
		return $this->hasMany(PartnersLinksModel::class, ['partner_id' => 'id'])->inverseOf('partner');
	}

	/** {@inheritdoc} */
	public function beforeSave ($insert) {
		if (parent::beforeSave($insert)) {
			$this->api_key_public = UserPassword::generate(32);
			$this->api_key_private = UserPassword::generate(64);
			return true;
		}
		return false;
	}
}

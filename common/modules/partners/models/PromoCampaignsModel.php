<?php

namespace common\modules\partners\models;

use Yii;

/**
 * This is the model class for table "{{%promo_campaigns}}".
 *
 * @property int $id
 * @property string $title
 * @property string $lang_code
 * @property string $currency_code
 * @property int $status
 *
 * @property PartnersLinksModel[] $partnersLinks
 * @property PartnersPromocodesModel[] $partnersPromocodes
 */
class PromoCampaignsModel extends \yii\db\ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const STATUS_ACTIVE = 1;
	const STATUS_INACTIVE = 0;

	public static function statuses (): array {
		return [
			self::STATUS_ACTIVE => Yii::t('partners', 'Active'),
			self::STATUS_INACTIVE => Yii::t('partners', 'Inactive'),
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%promo_campaigns}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\queries\PromoCampaignsQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\partners\queries\PromoCampaignsQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['status'], 'integer'],
			[['status','title', 'lang_code', 'currency_code'],'required','on' => [self::SCENARIO_UPDATE,self::SCENARIO_CREATE]],
			[['title', 'lang_code', 'currency_code'], 'string', 'max' => 255],
			[['status','title', 'lang_code', 'currency_code','id'],'safe','on' => self::SCENARIO_SEARCH]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => 'ID',
			'title' => Yii::t('partners', 'Title'),
			'lang_code' => Yii::t('app', 'Language'),
			'currency_code' => Yii::t('app', 'Currency'),
			'status' => Yii::t('partners', 'Status'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPartnersLinks () {
		return $this->hasMany(PartnersLinksModel::class, ['campaign_id' => 'id'])->inverseOf('campaign');
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPartnersPromocodes () {
		return $this->hasMany(PartnersPromocodesModel::class, ['campaign_id' => 'id'])->inverseOf('campaign');
	}
}

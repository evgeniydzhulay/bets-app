<?php

namespace common\modules\partners\models;

use Yii;

/**
 * This is the model class for table "{{%partners_log_links}}".
 *
 * @property int $id
 * @property int $partner_id
 * @property int $link_id
 * @property int $created_at
 * @property string $user_ip
 * @property string $user_agent
 * @property string $user_referrer
 */
class PartnersLogLinksModel extends \yii\db\ActiveRecord {

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%partners_log_links}}';
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\queries\PartnersLogLinksQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\partners\queries\PartnersLogLinksQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['partner_id', 'link_id', 'created_at'], 'integer'],
			[['user_ip', 'user_agent', 'user_referrer'], 'string', 'max' => 65000],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('partners', 'ID'),
			'partner_id' => Yii::t('partners', 'Partner ID'),
			'link_id' => Yii::t('partners', 'Link ID'),
			'created_at' => Yii::t('partners', 'Created At'),
			'user_ip' => Yii::t('partners', 'User IP'),
			'user_agent' => Yii::t('partners', 'User Agent'),
			'user_referrer' => Yii::t('partners', 'User Referrer'),
		];
	}
}

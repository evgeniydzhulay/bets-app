<?php

namespace common\modules\partners\models;

use common\modules\partners\queries\PartnersEarningsQuery;
use common\modules\partners\queries\PartnersLeadsQuery;
use common\modules\partners\queries\PartnersQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%partners_earnings}}".
 *
 * @property int $id
 * @property int $partner_id
 * @property int $lead_id
 * @property double $amount
 * @property string $currency_code
 * @property int $created_at
 *
 * @property PartnersLeadsModel $lead
 * @property PartnersModel $partner
 */
class PartnersEarningsModel extends \yii\db\ActiveRecord {

	/** {@inheritdoc} */
	public function behaviors () {
		return [
			[
				'class' => TimestampBehavior::class,
				'createdAtAttribute' => 'created_at',
				'updatedAtAttribute' => false,
			],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%partners_earnings}}';
	}

	/**
	 * {@inheritdoc}
	 * @return PartnersEarningsQuery the active query used by this AR class.
	 */
	public static function find () {
		return new PartnersEarningsQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['partner_id', 'lead_id'], 'required'],
			[['partner_id', 'lead_id', 'created_at'], 'integer'],
			[['amount'], 'number'],
			[['currency_code'], 'string', 'max' => 255],
			[['lead_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnersLeadsModel::class, 'targetAttribute' => ['lead_id' => 'id']],
			[['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnersModel::class, 'targetAttribute' => ['partner_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('partners', 'ID'),
			'partner_id' => Yii::t('partners', 'Partner'),
			'lead_id' => Yii::t('partners', 'Lead'),
			'amount' => Yii::t('partners', 'Amount'),
			'currency_code' => Yii::t('purse', 'Currency'),
			'created_at' => Yii::t('partners', 'Created at'),
		];
	}

	/**
	 * @return PartnersLeadsQuery
	 */
	public function getLead () {
		return $this->hasOne(PartnersLeadsModel::class, ['id' => 'lead_id']);
	}

	/**
	 * @return PartnersQuery
	 */
	public function getPartner () {
		return $this->hasOne(PartnersModel::class, ['id' => 'partner_id']);
	}
}

<?php

namespace common\modules\partners\models;

use common\modules\partners\queries\PartnersWithdrawQuery;
use Yii;

/**
 * This is the model class for table "{{%partners_withdraw}}".
 *
 * @property int $id
 * @property int $partner_id
 * @property double $amount
 * @property string $currency_code
 * @property int $service
 * @property string $target
 * @property string $target_additional
 * @property string $name
 * @property string $surname
 * @property string $comment
 * @property int $status
 * @property int $created_at
 * @property int $sent_at
 *
 * @property PartnersModel $partner
 */
class PartnersWithdrawModel extends \yii\db\ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';

	const STATUS_NEW = 0;
	const STATUS_PROCESSING = 1;
	const STATUS_SUCCESS = 6;
	const STATUS_FAIL = 7;
	const STATUS_REJECTED = 8;
	const STATUS_REVERTED = 9;

	public static function paymentMethods() {
		return [
			0 => Yii::t('partners', 'User account'),
			1 => Yii::t('partners', 'Credit card'),
			2 => 'Webmoney',
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%partners_withdraw}}';
	}

	/** {@inheritdoc} */
	function scenarios () {
		return [
			self::SCENARIO_CREATE => ['partner_id', 'amount', 'currency_code', 'service', 'target', 'target_additional', 'name', 'surname', 'status', 'created_at', 'sent_at','comment'],
			self::SCENARIO_UPDATE => ['status', 'sent_at','comment'],
			self::SCENARIO_SEARCH => ['id', 'amount', 'currency_code', 'partner_id', 'service', 'target', 'target_additional', 'name', 'surname', 'status', 'created_at', 'sent_at'],
		];
	}

	public static function statuses() {
		return [
			self::STATUS_NEW => Yii::t('purse', 'New'),
			self::STATUS_PROCESSING => Yii::t('purse', 'Processing'),
			self::STATUS_SUCCESS => Yii::t('purse', 'Success'),
			self::STATUS_FAIL => Yii::t('purse', 'Fail'),
			self::STATUS_REJECTED => Yii::t('purse', 'Rejected'),
			self::STATUS_REVERTED => Yii::t('purse', 'Reverted'),
		];
	}

	/**
	 * {@inheritdoc}
	 * @return PartnersWithdrawQuery the active query used by this AR class.
	 */
	public static function find () {
		return new PartnersWithdrawQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['partner_id', 'service', 'status', 'created_at', 'sent_at'], 'integer'],
			[['amount', 'created_at', 'sent_at', 'service','currency_code'], 'required', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['amount'], 'number'],
			[['comment'], 'string', 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			'serviceExists' => ['service',  'in', 'range' => \array_keys(PartnersWithdrawModel::paymentMethods())],
			['currency_code', 'in', 'range' => \array_keys(Yii::$app->get('currencies')->getlist()), 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			['status', 'in', 'range' => \array_keys(self::statuses()), 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
			[['target', 'target_additional', 'name', 'surname', 'currency_code'], 'string', 'max' => 255],
			[['partner_id'], 'exist', 'skipOnError' => true, 'targetClass' => PartnersModel::class, 'targetAttribute' => ['partner_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE,
				self::SCENARIO_UPDATE,
			]],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('partners', 'ID'),
			'partner_id' => Yii::t('partners', 'Partner ID'),
			'amount' => Yii::t('purse', 'Amount'),
			'service' => Yii::t('purse', 'Service'),
			'target' => Yii::t('purse', 'Target'),
			'target_additional' => Yii::t('purse', 'Additional target'),
			'name' => Yii::t('user', 'Name'),
			'surname' => Yii::t('user', 'Surname'),
			'comment' => Yii::t('purse', 'Comment'),
			'status' => Yii::t('partners', 'Status'),
			'created_at' => Yii::t('partners', 'Created at'),
			'sent_at' => Yii::t('purse', 'Sent at'),
			'currency_code' => Yii::t('purse', 'Currency'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getPartner () {
		return $this->hasOne(PartnersModel::class, ['id' => 'partner_id']);
	}
}

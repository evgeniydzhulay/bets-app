<?php

namespace common\modules\partners\migrations;

use common\traits\MigrationTypesTextTrait;
use Yii;

class m190228_114219_partners_init extends \yii\db\Migration {

	use MigrationTypesTextTrait;

	/**
	 * Create tables.
	 */
	public function up () {

		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%partners}}', [
			'id' => $this->primaryKey()->unsigned(),
			'company' => $this->string(),
			'owner_id' => $this->integer()->unsigned(),
			'description' => $this->longText(),
			'api_key_public' => $this->string(32),
			'api_key_private' => $this->string(64),
			'status' => $this->integer(1)->defaultValue(1),
			'created_at' => $this->integer(),
		], $tableOptions);
		$this->createIndex('idx-partners-owner', '{{%partners}}', 'owner_id');
		$this->createIndex('idx-partners-status', '{{%partners}}', 'status');
		$this->execute("ALTER TABLE {{%partners}} ADD FULLTEXT INDEX `idx-partners-company` (`company` ASC)");
		$this->execute("ALTER TABLE {{%partners}} ADD FULLTEXT INDEX `idx-partners-description` (`description` ASC)");
		$this->addForeignKey('fk-partners-owner', '{{%partners}}', 'owner_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%partners_workers}}', [
			'id' => $this->primaryKey()->unsigned(),
			'user_id' => $this->integer()->unsigned(),
			'partner_id' => $this->integer()->unsigned(),
			'role' => $this->integer()->unsigned(),
			'created_at' => $this->integer(),
		]);
		$this->createIndex('idx-partners_workers-user', '{{%partners_workers}}', 'user_id');
		$this->createIndex('idx-partners_workers-partner', '{{%partners_workers}}', 'partner_id');
		$this->createIndex('idx-partners_workers-role', '{{%partners_workers}}', 'role');
		$this->createIndex('idx-partners_workers-unique', '{{%partners_workers}}', ['user_id', 'partner_id', 'role'], true);
		$this->addForeignKey('fk-partners_workers-owner', '{{%partners_workers}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-partners_workers-partner', '{{%partners_workers}}', 'partner_id', '{{%partners}}', 'id', 'CASCADE', 'CASCADE');
	}

	/**
	 * Drop tables.
	 */
	public function down () {
		$this->dropTable('{{%partners_workers}}');
		$this->dropTable('{{%partners}}');
	}
}
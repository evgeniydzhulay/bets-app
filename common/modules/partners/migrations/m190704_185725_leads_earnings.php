<?php

namespace common\modules\partners\migrations;

use common\modules\partners\models\PromoCampaignsModel;
use yii\db\Migration;

class m190704_185725_leads_earnings extends Migration {

	public function up () {
		$this->addColumn('{{%partners_leads}}', 'material_id', $this->integer()->unsigned()->after('link_id'));
		$this->createIndex('idx-partners_leads-material', '{{%partners_leads}}', 'material_id');
		$this->addForeignKey('fk-partners_leads-material', '{{%partners_leads}}', 'material_id', '{{%promo_materials}}', 'id', 'CASCADE', 'CASCADE');

		$this->addColumn('{{%partners_leads}}', 'campaign_id', $this->integer()->unsigned()->after('material_id'));
		$this->createIndex('idx-partners_leads-campaign', '{{%partners_leads}}', 'campaign_id');
		$this->addForeignKey('fk-partners_leads-campaign', '{{%partners_leads}}', 'campaign_id', '{{%promo_campaigns}}', 'id', 'CASCADE', 'CASCADE');

		$this->addColumn('{{%partners_leads}}', 'amount_currency', $this->string(7)->after('user_id'));
		$this->createIndex('idx-partners_leads-currency', '{{%partners_leads}}', 'amount_currency');

		$this->addColumn('{{%partners_leads}}', 'amount_profit', $this->float(2)->after('user_id'));
		$this->createIndex('idx-partners_leads-profit', '{{%partners_leads}}', 'amount_profit');

		$this->addColumn('{{%partners_leads}}', 'amount_deposit', $this->float(2)->after('user_id'));
		$this->createIndex('idx-partners_leads-deposit', '{{%partners_leads}}', 'amount_deposit');
	}

	public function down () {
		$this->dropForeignKey('fk-partners_leads-material', '{{%partners_leads}}');
		$this->dropColumn('{{%partners_leads}}', 'material_id');

		$this->dropForeignKey('fk-partners_leads-campaign', '{{%partners_leads}}');
		$this->dropColumn('{{%partners_leads}}', 'campaign_id');

		$this->dropColumn('{{%partners_leads}}', 'amount_currency');
		$this->dropColumn('{{%partners_leads}}', 'amount_profit');
		$this->dropColumn('{{%partners_leads}}', 'amount_deposit');
	}

}

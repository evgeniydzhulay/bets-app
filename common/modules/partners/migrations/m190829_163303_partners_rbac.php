<?php

namespace common\modules\partners\migrations;

use common\modules\user\rbac\RbacMigration;

//TODO add processing to controllers
class m190829_163303_partners_rbac extends RbacMigration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
	public function safeUp() {
		$admin = $this->authManager->getRole('admin');
		$partnersEdit = $this->createPermission('partners-edit', 'Edit partners.');
		$partnersView = $this->createPermission('partners-view', 'View partners.');
		$this->assignChild($partnersEdit, $partnersView);

		$promoEdit = $this->createPermission('promo-edit', 'Edit promo materials.');
		$promoView = $this->createPermission('promo-view', 'View promo materials.');
		$this->assignChild($promoEdit, $promoView);

		$this->assignChild($admin, $partnersEdit);
		$this->assignChild($admin, $promoEdit);
	}

	public function safeDown() {}

}
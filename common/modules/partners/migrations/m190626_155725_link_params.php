<?php

namespace common\modules\partners\migrations;

use yii\db\Migration;

class m190626_155725_link_params extends Migration {

	public function up () {
		$this->addColumn('{{%partners_links}}', 'page_url', $this->string());
		$this->addColumn('{{%partners_links}}', 'page_sid', $this->string());
	}

	public function down () {
		$this->dropColumn('{{%partners_links}}', 'page_url');
		$this->dropColumn('{{%partners_links}}', 'page_sid');
	}

}

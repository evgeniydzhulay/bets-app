<?php

namespace common\modules\partners\migrations;

use common\modules\partners\models\PromoCampaignsModel;
use yii\db\Migration;

class m190709_115725_partner_lead_sid extends Migration {

	public function up () {
		$this->addColumn('{{%partners_leads}}', 'sid', $this->string()->after('campaign_id'));
		$this->createIndex('idx-partners_leads-sid', '{{%partners_leads}}', 'campaign_id');
	}

	public function down () {
		$this->dropIndex('idx-partners_leads-sid', '{{%partners_leads}}');
		$this->dropColumn('{{%partners_leads}}', 'sid');
	}

}

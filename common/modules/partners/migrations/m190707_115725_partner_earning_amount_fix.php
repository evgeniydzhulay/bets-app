<?php

namespace common\modules\partners\migrations;

use common\modules\partners\models\PromoCampaignsModel;
use yii\db\Migration;

class m190707_115725_partner_earning_amount_fix extends Migration {

	public function up () {
		$this->dropIndex('idx-partners_earnings-amonut', '{{%partners_earnings}}');
		$this->renameColumn('{{%partners_earnings}}', 'amonut', 'amount');
		$this->createIndex('idx-partners_earnings-amount', '{{%partners_earnings}}', 'amount');
	}

	public function down () {
		$this->dropIndex('idx-partners_earnings-amount', '{{%partners_earnings}}');
		$this->renameColumn('{{%partners_earnings}}', 'amount', 'amonut');
		$this->createIndex('idx-partners_earnings-amonut', '{{%partners_earnings}}', 'amonut');
	}

}

<?php

namespace common\modules\partners\migrations;

use common\modules\partners\models\PromoCampaignsModel;
use yii\db\Migration;

class m190827_115725_partner_comissions extends Migration {

	public function up () {
		$tableOptions = null;
		if (\Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

     	$this->createTable('{{%partners_comission}}', [
			'id' => $this->primaryKey()->unsigned(),
			'partner_id' => $this->integer()->unsigned(),
			'currency_code' => $this->string(),
	        'deposit_profit' => $this->integer(),
	        'deposit_min' => $this->integer(),
	        'profit_percent' => $this->integer(),
	        'created_at' => $this->integer(),
		], $tableOptions);

		$this->createIndex('idx-partners_comission-partner', '{{%partners_comission}}', 'partner_id');
		$this->createIndex('idx-partners_comission-currency', '{{%partners_comission}}', 'currency_code');
		$this->addForeignKey('fk-partners_comission-partner', '{{%partners_comission}}', 'partner_id', '{{%partners}}', 'id', 'CASCADE', 'CASCADE');

	}

	public function down () {
		$this->dropTable('{{%partners_comission}}');
	}

}

<?php

namespace common\modules\partners\migrations;

use common\traits\MigrationTypesTextTrait;
use Yii;

class m190305_104400_partner_leads extends \yii\db\Migration {

	use MigrationTypesTextTrait;

	/**
	 * Create tables.
	 */
	public function up () {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%partners_links}}', [
			'id' => $this->primaryKey()->unsigned(),
			'partner_id' => $this->integer()->unsigned(),
			'link' => $this->string(),
			'description' => $this->longText(),
			'created_at' => $this->integer(),
		], $tableOptions);
		$this->createIndex('idx-partners_links-partner', '{{%partners_links}}', 'partner_id');
		$this->createIndex('idx-partners_links-link', '{{%partners_links}}', 'link');
		$this->addForeignKey('fk-partners_links-partner', '{{%partners_links}}', 'partner_id', '{{%partners}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%partners_leads}}', [
			'id' => $this->primaryKey()->unsigned(),
			'partner_id' => $this->integer()->unsigned(),
			'link_id' => $this->integer()->unsigned(),
			'user_id' => $this->integer()->unsigned(),
			'created_at' => $this->integer(),
		], $tableOptions);
		$this->createIndex('idx-partners_leads-partner', '{{%partners_leads}}', 'partner_id');
		$this->createIndex('idx-partners_leads-link', '{{%partners_leads}}', 'link_id');
		$this->createIndex('idx-partners_leads-user', '{{%partners_leads}}', 'user_id');
		$this->addForeignKey('fk-partners_leads-partner', '{{%partners_leads}}', 'partner_id', '{{%partners}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-partners_leads-user', '{{%partners_leads}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-partners_leads-link', '{{%partners_leads}}', 'link_id', '{{%partners_links}}', 'id', 'SET NULL', 'CASCADE');
	}

	/**
	 * Drop tables.
	 */
	public function down () {
		$this->dropTable('{{%partners_leads}}');
		$this->dropTable('{{%partners_links}}');
	}
}
<?php

namespace common\modules\partners\migrations;

use common\modules\partners\models\PromoCampaignsModel;
use yii\db\Migration;

class m191105_165725_partner_promocode_amount extends Migration {

	public function up () {
		$this->addColumn('{{%partners_promocodes}}', 'amount', $this->integer()->notNull());
	}

	public function down () {
		$this->dropColumn('{{%partners_promocodes}}', 'amount');
	}

}

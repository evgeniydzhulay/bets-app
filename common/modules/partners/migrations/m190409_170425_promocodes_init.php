<?php

namespace common\modules\partners\migrations;

use yii\db\Migration;

class m190409_170425_promocodes_init extends Migration {

    public function up() {
        $tableOptions = null;
        if (\Yii::$app->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%promocodes}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(255),
            'created_at' => $this->integer()->defaultValue(0),
            'is_used' => $this->boolean()->defaultValue(false),
            'used_at' => $this->integer(),
            'used_by' => $this->integer()->unsigned(),
	        'amount' => $this->float(2)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk-promocodes-user', '{{%promocodes}}', 'used_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down() {
        $this->dropTable('{{%promocodes}}');
    }

}

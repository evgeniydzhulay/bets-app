<?php

namespace common\modules\partners\migrations;

use yii\db\Migration;

class m190626_165725_promocode_fix_partner_codes extends Migration {

	public function up () {
		$this->addColumn('{{%statistics_promocodes}}', 'partner_id', $this->integer()->unsigned()->after('id'));
		$this->createIndex('idx-statistics_promocodes-partner', '{{%statistics_promocodes}}', 'partner_id');
		$this->addForeignKey('fk-statistics_promocodes-partner', '{{%statistics_promocodes}}', 'partner_id', '{{%partners}}', 'id', 'CASCADE', 'CASCADE');
		$this->dropColumn('{{%partners_promocodes}}', 'lang_code');
		$this->dropColumn('{{%partners_promocodes}}', 'currency_code');
	}

	public function down () {
		$this->dropForeignKey('fk-statistics_promocodes-partner', '{{%statistics_promocodes}}');
		$this->dropColumn('{{%statistics_promocodes}}', 'partner_id');
		$this->addColumn('{{%partners_promocodes}}', 'lang_code', $this->string(10));
		$this->addColumn('{{%partners_promocodes}}', 'currency_code', $this->string());
	}

}

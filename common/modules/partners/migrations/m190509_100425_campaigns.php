<?php

namespace common\modules\partners\migrations;

use common\traits\MigrationTypesTextTrait;
use yii\db\Migration;

class m190509_100425_campaigns extends Migration {

	use MigrationTypesTextTrait;

	public function up () {
		$tableOptions = null;
		if (\Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%partners_site}}', [
			'id' => $this->primaryKey()->unsigned(),
			'address' => $this->string(255),
			'partner_id' => $this->integer()->unsigned(),
			'category_id' => $this->integer()->unsigned()->notNull(),
			'lang_code' => $this->string(10),
			'is_active' => $this->integer(1),
		], $tableOptions);

		$this->createIndex('idx-partners_site-partner', '{{%partners_site}}', 'partner_id');
		$this->createIndex('idx-partners_site-category', '{{%partners_site}}', 'category_id');
		$this->createIndex('idx-partners_site-lang', '{{%partners_site}}', 'lang_code');

		$this->addForeignKey('fk-partners_site-partner', '{{%partners_site}}', 'partner_id', '{{%partners}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%partners_promocodes}}', [
			'id' => $this->primaryKey()->unsigned(),
			'partner_id' => $this->integer()->notNull()->unsigned(),
			'campaign_id' => $this->integer()->notNull(),
			'site_id' => $this->integer()->unsigned()->notNull(),
			'lang_code' => $this->string(10),
			'currency_code' => $this->string(),
			'is_active' => $this->integer(1),
		], $tableOptions);

		$this->createIndex('idx-partners_promocodes-partner', '{{%partners_promocodes}}', 'partner_id');
		$this->createIndex('idx-partners_promocodes-site', '{{%partners_promocodes}}', 'site_id');
		$this->createIndex('idx-partners_promocodes-lang', '{{%partners_promocodes}}', 'lang_code');
		$this->createIndex('idx-partners_promocodes-currency', '{{%partners_promocodes}}', 'currency_code');

		$this->addForeignKey('fk-partners_promocodes-partner', '{{%partners_promocodes}}', 'partner_id', '{{%partners}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-partners_promocodes-site', '{{%partners_promocodes}}', 'site_id', '{{%partners_site}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%statistics_promocodes}}', [
			'id' => $this->primaryKey(),
			'site_id' => $this->integer()->unsigned(),
			'campaign_id' => $this->integer()->unsigned(),
			'promocode_id' => $this->integer()->unsigned(),
			'used_at' => $this->integer(),
			'used_by' => $this->integer()->unsigned(),
		], $tableOptions);

		$this->createIndex('idx-statistics_promocodes-site', '{{%statistics_promocodes}}', 'site_id');
		$this->createIndex('idx-statistics_promocodes-campaign', '{{%statistics_promocodes}}', 'campaign_id');
		$this->createIndex('idx-statistics_promocodes-promocode', '{{%statistics_promocodes}}', 'promocode_id');
		$this->createIndex('idx-statistics_promocodes-user', '{{%statistics_promocodes}}', 'used_by');

		$this->addForeignKey('fk-statistics_promocodes-site', '{{%statistics_promocodes}}', 'site_id', '{{%partners_site}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-statistics_promocodes-promocode', '{{%statistics_promocodes}}', 'promocode_id', '{{%partners_promocodes}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-statistics_promocodes-user', '{{%statistics_promocodes}}', 'used_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

		$this->addColumn('{{%partners_links}}', 'site_id', $this->integer()->unsigned());
		$this->addColumn('{{%partners_links}}', 'campaign_id', $this->integer()->unsigned());

		$this->createIndex('idx-partners_links-site', '{{%partners_links}}', 'site_id');
		$this->createIndex('idx-partners_links-campaign', '{{%partners_links}}', 'campaign_id');

		$this->addForeignKey('fk-partners_links-site', '{{%partners_links}}', 'site_id', '{{%partners_site}}', 'id', 'CASCADE', 'CASCADE');

		$this->dropTable('{{%promocodes}}');
		$this->dropTable('{{%partners_workers}}');

		$this->dropColumn('{{%partners}}', 'company');
		$this->dropColumn('{{%partners}}', 'description');

		$this->addColumn('{{%partners}}', 'payment_method', $this->integer());
		$this->addColumn('{{%partners}}', 'payment_number', $this->string());
	}

	public function down () {
		$tableOptions = null;
		if (\Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->addColumn('{{%partners}}', 'company', $this->string());
		$this->addColumn('{{%partners}}', 'description', $this->longText());

		$this->dropColumn('{{%partners}}', 'payment_method');
		$this->dropColumn('{{%partners}}', 'payment_number');

		$this->dropForeignKey('fk-partners_links-site', '{{%partners_links}}');

		$this->dropColumn('{{%partners_links}}', 'site_id');
		$this->dropColumn('{{%partners_links}}', 'campaign_id');

		$this->dropTable('{{%statistics_promocodes}}');
		$this->dropTable('{{%partners_promocodes}}');
		$this->dropTable('{{%partners_site}}');

		$this->createTable('{{%promocodes}}', [
			'id' => $this->primaryKey(),
			'code' => $this->string(255),
			'created_at' => $this->integer()->defaultValue(0),
			'is_used' => $this->boolean()->defaultValue(false),
			'used_at' => $this->integer(),
			'used_by' => $this->integer()->unsigned(),
			'amount' => $this->float(2)->notNull(),
		], $tableOptions);

		$this->addForeignKey('fk-promocodes-user', '{{%promocodes}}', 'used_by', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%partners_workers}}', [
			'id' => $this->primaryKey()->unsigned(),
			'user_id' => $this->integer()->unsigned(),
			'partner_id' => $this->integer()->unsigned(),
			'role' => $this->integer()->unsigned(),
			'created_at' => $this->integer(),
		], $tableOptions);

		$this->createIndex('idx-partners_workers-user', '{{%partners_workers}}', 'user_id');
		$this->createIndex('idx-partners_workers-partner', '{{%partners_workers}}', 'partner_id');
		$this->createIndex('idx-partners_workers-role', '{{%partners_workers}}', 'role');
		$this->createIndex('idx-partners_workers-unique', '{{%partners_workers}}', ['user_id', 'partner_id', 'role'], true);

		$this->addForeignKey('fk-partners_workers-owner', '{{%partners_workers}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-partners_workers-partner', '{{%partners_workers}}', 'partner_id', '{{%partners}}', 'id', 'CASCADE', 'CASCADE');
	}

}

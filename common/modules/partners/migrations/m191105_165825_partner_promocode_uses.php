<?php

namespace common\modules\partners\migrations;

use common\modules\partners\models\PromoCampaignsModel;
use yii\db\Migration;

class m191105_165825_partner_promocode_uses extends Migration {

	public function up () {
		$tableOptions = null;
		if (\Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		try {
			$this->dropTable('{{%statistics_promocodes}}');
		} catch (\Throwable $exception) {}

		$this->createTable('{{%partners_promocode_uses}}', [
			'id' => $this->primaryKey()->unsigned(),
			'user_id' => $this->integer()->unsigned(),
			'promocode_id' => $this->integer()->unsigned(),
			'partner_id' => $this->integer()->unsigned(),
			'campaign_id' => $this->integer()->unsigned(),
			'created_at' => $this->integer(),
		], $tableOptions);

		$this->createIndex('idx-partners_promocode_uses-user', '{{%partners_promocode_uses}}', 'user_id');
		$this->createIndex('idx-partners_promocode_uses-promocode', '{{%partners_promocode_uses}}', 'promocode_id');
		$this->createIndex('idx-partners_promocode_uses-partner', '{{%partners_promocode_uses}}', 'partner_id');
		$this->createIndex('idx-partners_promocode_uses-campaign', '{{%partners_promocode_uses}}', 'campaign_id');
		$this->createIndex('idx-partners_promocode_uses-unique', '{{%partners_promocode_uses}}', [
			'user_id', 'promocode_id'
		], true);

		$this->addForeignKey('fk-partners_promocode_uses-user', '{{%partners_promocode_uses}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-partners_promocode_uses-promocode', '{{%partners_promocode_uses}}', 'promocode_id', '{{%partners_promocodes}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-partners_promocode_uses-partner', '{{%partners_promocode_uses}}', 'partner_id', '{{%partners}}', 'id', 'CASCADE', 'CASCADE');
	}

	public function down () {
		$this->dropTable('{{%partners_promocode_uses}}');
	}

}

<?php

namespace common\modules\partners\migrations;

use common\modules\partners\models\PromoCampaignsModel;
use yii\db\Migration;

class m190706_115725_partner_logs extends Migration {

	public function up () {
		$tableOptions = null;
		if (\Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%partners_log_views}}', [
			'id' => $this->primaryKey()->unsigned(),
			'partner_id' => $this->integer()->unsigned(),
			'material_id' => $this->integer()->unsigned(),
			'created_at' => $this->integer(),
			'user_ip' => $this->text(),
			'user_agent' => $this->text(),
			'user_referrer' => $this->text(),
		], $tableOptions);
		$this->createIndex('idx-partners_log_views-partner', '{{%partners_log_views}}', 'partner_id');
		$this->createIndex('idx-partners_log_views-material', '{{%partners_log_views}}', 'material_id');
		$this->createIndex('idx-partners_log_views-created_at', '{{%partners_log_views}}', 'created_at');

		$this->createTable('{{%partners_log_clicks}}', [
			'id' => $this->primaryKey()->unsigned(),
			'partner_id' => $this->integer()->unsigned(),
			'material_id' => $this->integer()->unsigned(),
			'created_at' => $this->integer(),
			'user_ip' => $this->text(),
			'user_agent' => $this->text(),
			'user_referrer' => $this->text(),
		], $tableOptions);
		$this->createIndex('idx-partners_log_clicks-partner', '{{%partners_log_clicks}}', 'partner_id');
		$this->createIndex('idx-partners_log_clicks-material', '{{%partners_log_clicks}}', 'material_id');
		$this->createIndex('idx-partners_log_clicks-created_at', '{{%partners_log_clicks}}', 'created_at');

		$this->createTable('{{%partners_log_links}}', [
			'id' => $this->primaryKey()->unsigned(),
			'partner_id' => $this->integer()->unsigned(),
			'link_id' => $this->integer()->unsigned(),
			'created_at' => $this->integer(),
			'user_ip' => $this->text(),
			'user_agent' => $this->text(),
			'user_referrer' => $this->text(),
		], $tableOptions);
		$this->createIndex('idx-partners_log_links-partner', '{{%partners_log_links}}', 'partner_id');
		$this->createIndex('idx-partners_log_links-link', '{{%partners_log_links}}', 'link_id');
		$this->createIndex('idx-partners_log_links-created_at', '{{%partners_log_links}}', 'created_at');
	}

	public function down () {
		$this->dropTable('{{%partners_log_links}}');
		$this->dropTable('{{%partners_log_clicks}}');
		$this->dropTable('{{%partners_log_views}}');

	}

}

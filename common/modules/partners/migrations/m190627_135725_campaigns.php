<?php

namespace common\modules\partners\migrations;

use common\modules\partners\models\PromoCampaignsModel;
use yii\db\Migration;

class m190627_135725_campaigns extends Migration {

	public function up () {
		$tableOptions = null;
		if (\Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->addColumn('{{%partners_promocodes}}', 'code', $this->string()->after('id'));
		$this->createIndex('idx-partners_promocodes-code', '{{%partners_promocodes}}', 'code');

		$this->createTable('{{%promo_campaigns}}', [
			'id' => $this->primaryKey()->unsigned(),
			'title' => $this->string(),
			'lang_code' => $this->string(),
			'currency_code' => $this->string(),
			'status' => $this->integer(1)->defaultValue(1),
		], $tableOptions);

		$this->alterColumn('{{%partners_promocodes}}', 'campaign_id', $this->integer()->unsigned());
		$this->createIndex('idx-partners_promocodes-campaign', '{{%partners_promocodes}}', 'campaign_id');
		$this->addForeignKey('fk-partners_promocodes-campaign', '{{%partners_promocodes}}', 'campaign_id', '{{%promo_campaigns}}', 'id', 'CASCADE', 'CASCADE');

		$this->addForeignKey('fk-partners_links-campaign', '{{%partners_links}}', 'campaign_id', '{{%promo_campaigns}}', 'id', 'CASCADE', 'CASCADE');


		$this->createTable('{{%promo_materials}}', [
			'id' => $this->primaryKey()->unsigned(),
			'path' => $this->string(),
			'title' => $this->string(),
			'type' => $this->integer(2),
			'lang_code' => $this->string(),
			'size_h' => $this->integer(),
			'size_w' => $this->integer(),
			'currency_code' => $this->string(),
			'status' => $this->integer(1)->defaultValue(1),
		], $tableOptions);
		$this->createIndex('idx-promo_materials-type', '{{%promo_materials}}', 'type');
		$this->createIndex('idx-promo_materials-size_h', '{{%promo_materials}}', 'size_h');
		$this->createIndex('idx-promo_materials-size_w', '{{%promo_materials}}', 'size_w');
		$this->createIndex('idx-promo_materials-lang', '{{%promo_materials}}', 'lang_code');
		$this->createIndex('idx-promo_materials-currency', '{{%promo_materials}}', 'currency_code');
	}

	public function down () {
		$this->dropForeignKey('fk-partners_links-campaign', '{{%partners_links}}');

		$this->dropColumn('{{%partners_promocodes}}', 'code');

		$this->dropForeignKey('fk-partners_promocodes-campaign', '{{%partners_promocodes}}');
		$this->dropIndex('idx-partners_promocodes-campaign', '{{%partners_promocodes}}');

		PromoCampaignsModel::deleteAll();
		$this->dropTable('{{%promo_campaigns}}');
		$this->dropTable('{{%promo_materials}}');
	}

}

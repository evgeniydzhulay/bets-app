<?php

namespace common\modules\partners\migrations;

use common\modules\partners\models\PromoCampaignsModel;
use yii\db\Migration;

class m190702_115725_partners_earnings extends Migration {

	public function up () {
		$tableOptions = null;
		if (\Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}

		$this->createTable('{{%partners_earnings}}', [
			'id' => $this->primaryKey()->unsigned(),
			'partner_id' => $this->integer()->unsigned(),
			'lead_id' => $this->integer()->unsigned(),
			'amonut' => $this->float(),
			'currency_code' => $this->string(),
			'created_at' => $this->integer(),
		], $tableOptions);

		$this->createIndex('idx-partners_earnings-amonut', '{{%partners_earnings}}', 'amonut');
		$this->createIndex('idx-partners_earnings-currency', '{{%partners_earnings}}', 'currency_code');
		$this->createIndex('idx-partners_earnings-created_at', '{{%partners_earnings}}', 'created_at');

		$this->createIndex('idx-partners_earnings-partner', '{{%partners_earnings}}', 'partner_id');
		$this->addForeignKey('fk-partners_earnings-partner', '{{%partners_earnings}}', 'partner_id', '{{%partners}}', 'id', 'CASCADE', 'CASCADE');

		$this->createIndex('idx-partners_earnings-lead', '{{%partners_earnings}}', 'lead_id');
		$this->addForeignKey('fk-partners_earnings-lead', '{{%partners_earnings}}', 'lead_id', '{{%partners_leads}}', 'id', 'CASCADE', 'CASCADE');


		$this->createTable('{{%partners_withdraw}}', [
			'id' => $this->primaryKey()->unsigned(),
			'partner_id' => $this->integer()->unsigned(),
			'amount' => $this->float(2)->notNull(),
			'currency_code' => $this->string(),
			'service' => $this->integer(3),
			'target' => $this->string(),
			'target_additional' => $this->string(),
			'name' => $this->string(),
			'surname' => $this->string(),
			'comment' => $this->text(),
			'status' => $this->integer(2),
			'created_at' => $this->integer()->notNull(),
			'sent_at' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createIndex('idx-partners_withdraw-user', '{{%partners_withdraw}}', 'partner_id');
		$this->createIndex('idx-partners_withdraw-amount', '{{%partners_withdraw}}', 'amount');
		$this->createIndex('idx-partners_withdraw-type', '{{%partners_withdraw}}', 'service');
		$this->createIndex('idx-partners_withdraw-status', '{{%partners_withdraw}}', 'status');
		$this->createIndex('idx-partners_withdraw-target', '{{%partners_withdraw}}', 'target');
		$this->createIndex('idx-partners_withdraw-created_at', '{{%partners_withdraw}}', 'created_at');
		$this->createIndex('idx-partners_withdraw-sent_at', '{{%partners_withdraw}}', 'sent_at');

		$this->addForeignKey('fk-partners_withdraw-partner', '{{%partners_withdraw}}', 'partner_id', '{{%partners}}', 'id', 'CASCADE', 'CASCADE');
	}

	public function down () {
		$this->dropTable('{{%partners_withdraw}}');
		$this->dropTable('{{%partners_earnings}}');
	}

}

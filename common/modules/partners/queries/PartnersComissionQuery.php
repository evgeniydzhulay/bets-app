<?php

namespace common\modules\partners\queries;

use common\modules\partners\models\PartnersComissionModel;

/**
 * This is the ActiveQuery class for [[\common\modules\partners\models\PartnersComissionModel]].
 *
 * @see \common\modules\partners\models\PartnersComissionModel
 */
class PartnersComissionQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return PartnersComissionModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return PartnersComissionModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

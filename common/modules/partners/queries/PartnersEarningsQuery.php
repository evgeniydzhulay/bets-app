<?php

namespace common\modules\partners\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\partners\models\PartnersEarningsModel]].
 *
 * @see \common\modules\partners\models\PartnersEarningsModel
 */
class PartnersEarningsQuery extends \yii\db\ActiveQuery
{
    /*public function active()
    {
        return $this->andWhere('[[status]]=1');
    }*/

    /**
     * {@inheritdoc}
     * @return \common\modules\partners\models\PartnersEarningsModel[]|array
     */
    public function all($db = null)
    {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return \common\modules\partners\models\PartnersEarningsModel|array|null
     */
    public function one($db = null)
    {
        return parent::one($db);
    }
}

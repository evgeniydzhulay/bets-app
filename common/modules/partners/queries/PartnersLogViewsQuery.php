<?php

namespace common\modules\partners\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\partners\models\PartnersLogViews]].
 *
 * @see \common\modules\partners\models\PartnersLogViewsModel
 */
class PartnersLogViewsQuery extends \yii\db\ActiveQuery {

	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\models\PartnersLogViewsModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\models\PartnersLogViewsModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

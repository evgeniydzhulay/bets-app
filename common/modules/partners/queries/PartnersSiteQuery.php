<?php

namespace common\modules\partners\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\partners\models\PartnersSiteModel]].
 *
 * @see \common\modules\partners\models\PartnersSiteModel
 */
class PartnersSiteQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\models\PartnersSiteModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\models\PartnersSiteModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

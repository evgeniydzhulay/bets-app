<?php

namespace common\modules\partners\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\partners\models\PartnersPromocodesModel]].
 *
 * @see \common\modules\partners\models\PartnersPromocodesModel
 */
class PartnersPromocodesQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\models\PartnersPromocodesModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\models\PartnersPromocodesModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

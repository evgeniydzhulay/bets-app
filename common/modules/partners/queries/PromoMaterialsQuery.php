<?php

namespace common\modules\partners\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\partners\models\PromoMaterialsModel]].
 *
 * @see \common\modules\partners\models\PromoMaterialsModel
 */
class PromoMaterialsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\models\PromoMaterialsModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\models\PromoMaterialsModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

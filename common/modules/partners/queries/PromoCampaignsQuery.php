<?php

namespace common\modules\partners\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\partners\models\PromoCampaignsModel]].
 *
 * @see \common\modules\partners\models\PromoCampaignsModel
 */
class PromoCampaignsQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\models\PromoCampaignsModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\partners\models\PromoCampaignsModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

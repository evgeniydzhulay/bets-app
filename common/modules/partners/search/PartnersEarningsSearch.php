<?php

namespace common\modules\partners\search;

use common\modules\partners\models\PartnersEarningsModel;
use yii\data\ActiveDataProvider;

class PartnersEarningsSearch extends PartnersEarningsModel {

	public function formName () {
		return '';
	}

	public function search($params) {
		$query = self::find();
		if($this->load($params) && $this->validate()) {
			$query->andFilterWhere([
				'partner_id' => $this->partner_id,
				'currency_code' => $this->currency_code,
			]);
		} else if(array_key_exists('partner_id', $params)) {
			$query->andWhere([
				'partner_id' => $params['partner_id']
			]);
		}
		return new ActiveDataProvider([
			'query' => $query
		]);
	}
}
<?php

namespace common\modules\partners\search;

use common\modules\partners\models\PartnersSiteModel;
use yii\data\ActiveDataProvider;

class PartnersSiteSearch extends PartnersSiteModel {

	public function formName () {
		return '';
	}

	public function search($params) {
		$query = self::find();
		if($this->load($params) && $this->validate()) {
			$query->andFilterWhere([
				'partner_id' => $this->partner_id,
				'category_id' => $this->category_id,
				'lang_code' => $this->lang_code,
				'is_active' => $this->is_active,
			]);
		} else if(array_key_exists('partner_id', $params)) {
			$query->andWhere([
				'partner_id' => +$params['partner_id']
			]);
		}
		return new ActiveDataProvider([
			'query' => $query,
		]);
	}
}
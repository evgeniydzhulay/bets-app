<?php

namespace common\modules\partners\search;

use common\modules\partners\models\PartnersPromocodesModel;
use yii\data\ActiveDataProvider;

class PartnersPromocodesSearch extends PartnersPromocodesModel {

	public function formName () {
		return '';
	}

	public function search($params) {
		$query = self::find();
		if($this->load($params) && $this->validate()) {
			$query->andFilterWhere([
				'partner_id' => $this->partner_id,
				'campaign_id' => $this->campaign_id,
				'site_id' => $this->site_id,
				'is_active' => $this->is_active,
			]);
		} else if(array_key_exists('partner_id', $params)) {
			$query->andWhere([
				'partner_id' => +$params['partner_id']
			]);
		}
		return new ActiveDataProvider([
			'query' => $query
		]);
	}
}
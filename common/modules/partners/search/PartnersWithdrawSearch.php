<?php

namespace common\modules\partners\search;

use common\modules\partners\models\PartnersWithdrawModel;
use common\traits\DateToTimeTrait;
use yii\data\ActiveDataProvider;

class PartnersWithdrawSearch extends PartnersWithdrawModel {

	use DateToTimeTrait;

	public function formName () {
		return '';
	}

	public function load ($data, $formName = null) {
		if(parent::load($data, $formName)) {
			$this->created_at = self::getTimeFromDate($this->created_at);
			return true;
		}
		return false;
	}

	public function search($params) {
		$query = self::find();
		if($this->load($params) && $this->validate()) {
			$query->andFilterWhere([
				'partner_id' => $this->partner_id,
				'service' => $this->service,
				'currency_code' => $this->currency_code,
				'status' => $this->status,
			]);
		} else if(array_key_exists('partner_id', $params)) {
			$query->andWhere([
				'partner_id' => $params['partner_id']
			]);
		}
		return new ActiveDataProvider([
			'query' => $query
		]);
	}
}
<?php

namespace common\modules\partners\search;

use common\modules\partners\models\PartnersLinksModel;
use yii\data\ActiveDataProvider;

class PartnersLinksSearch extends PartnersLinksModel {


	/** {@inheritdoc} */
	public function rules () {
		return parent::rules() + [
			'safeSearch' => [['partner_id', 'description', 'link', 'created_at'], 'safe']
		];
	}

	public function search($params = []) {
		$query = static::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
				'pageSizeLimit' => false
			],
			'sort' => [
				'defaultOrder' => [
					'id' => SORT_DESC
				]
			]
		]);
		if ($this->load($params) && $this->validate()) {
			$query->andFilterWhere([
				self::tableName() . '.partner_id' => $this->partner_id,
				self::tableName() . '.site_id' => $this->site_id,
				self::tableName() . '.campaign_id' => $this->campaign_id,
			]);
			$query->andFilterWhere(['like', self::tableName() . '.link', $this->link]);
			$query->andFilterWhere(['like', self::tableName() . '.description', $this->description]);
		} else if(array_key_exists('partner_id', $params)) {
			$query->andWhere([
				'partner_id' => +$params['partner_id']
			]);
		}
		return $dataProvider;
	}

	/** {@inheritdoc} */
	public function formName () {
		return '';
	}
}
<?php

namespace common\modules\partners\search;

use common\modules\partners\models\PromoMaterialsModel;
use yii\data\ActiveDataProvider;

class PromoMaterialsSearch extends PromoMaterialsModel {

	public function formName () {
		return '';
	}

	public function search($params) {
		$query = self::find();
		if($this->load($params) && $this->validate()) {
			$query->andFilterWhere([
				'type' => $this->type,
				'lang_code' => $this->lang_code,
				'currency_code' => $this->currency_code,
				'size_h' => $this->size_h,
				'size_w' => $this->size_w,
			]);
		}
		return new ActiveDataProvider([
			'query' => $query
		]);
	}
}
<?php

namespace common\modules\partners;

use common\modules\games\models\BetsModel;
use common\modules\partners\models\PartnersLeadsModel;
use common\modules\user\models\UserModel;
use common\modules\user\models\UserPurseDepositModel;
use Yii;
use yii\base\Event;
use yii\base\Module as BaseModule;

class Module extends BaseModule {

	public function init() {
		parent::init();
		$partnerLink = Yii::$app->session->get('partner_link', null);
		$partnerMaterial = Yii::$app->session->get('partner_material', null);
		$partnerID = Yii::$app->session->get('partner_id', null);

		if(!is_null($partnerLink) || (!is_null($partnerMaterial) && !is_null($partnerID))) {
			Event::on(UserModel::class, UserModel::AFTER_REGISTER, function(Event $event) use (
				$partnerLink, $partnerMaterial, $partnerID
			) {
				PartnersLeadsModel::leadAddTracking($event->sender, $partnerLink, $partnerMaterial, $partnerID, Yii::$app->session->get('partner_sid', null));
			});
		}
		Event::on(UserPurseDepositModel::class, UserPurseDepositModel::EVENT_DEPOSIT_SUCCESS, function(Event $event) {
			PartnersLeadsModel::leadDeposited($event->sender);
		});

		Event::on(BetsModel::class, BetsModel::EVENT_DEFEAT, function(Event $event) {
			PartnersLeadsModel::leadBetResulted($event->sender);
		});
		Event::on(BetsModel::class, BetsModel::EVENT_VICTORY, function(Event $event) {
			PartnersLeadsModel::leadBetResulted($event->sender);
		});
		Event::on(BetsModel::class, BetsModel::EVENT_BANNED, function(Event $event) {
			PartnersLeadsModel::leadBetResulted($event->sender);
		});
		Event::on(BetsModel::class, BetsModel::EVENT_REVERT, function(Event $event) {
			PartnersLeadsModel::leadBetResulted($event->sender);
		});
	}
}
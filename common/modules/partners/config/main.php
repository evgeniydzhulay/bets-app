<?php

use common\modules\Storage;
use creocoder\flysystem\LocalFilesystem;

return [
	'bootstrap' => [
		'partners'
	],
	'modules' => [
		'partners' => [
			'class' => \common\modules\partners\Module::class,
		],
		'partnersStorage' => [
			'class' => Storage::class,
			'uploadUrl' => '@web/uploads/partners',
			'internalUrl' => '@web/uploads/partners',
			'uploadDir' => '@uploads/public/partners',
			'components' => [
				'storage' => [
					'class' => LocalFilesystem::class,
					'path' => '@uploads/public/partners'
				]
			]
		],
	],
	'params' => [
		'partners.comission.bets' => 50
	]
];
<?php

namespace common\modules\user\rbac;

use Yii;
use yii\base\InvalidConfigException;
use yii\db\Migration;
use yii\rbac\DbManager;
use yii\rbac\Item;
use yii\rbac\Permission;
use yii\rbac\Role;
use yii\rbac\Rule;
use const PHP_EOL;

/**
 * Migration is the base class for representing a database migration.
 * Migration is designed to be used together with the "yii migrate" command.
 * Basically added functions to create and assign roles from migrations
 */
class RbacMigration extends Migration {

    /**
     * @var DbManager Auth manager application component.
     */
    protected $authManager;

    /**
     * Initialize migrations.
     * Calls parent init method, then loads current authManager instance.
     *
     * @return void
     * @throws InvalidConfigException
     */
    public function init() {
        parent::init();
        $this->authManager = Yii::$app->getAuthManager();
        if (!$this->authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
    }

    /**
     * Create role.
     * If role already exists doesn nothing.
     *
     * @param string $name Role name
     * @param string $description Role description
     * @param string $rule Rule name
     * @return Role the new Role object
     * @throws \Exception
     */
    function createRole($name, $description, $rule = NULL) {
        if (($role = $this->authManager->getRole($name))) {
            echo "{$name} role already exists" . PHP_EOL;
        } else {
            $role = $this->authManager->createRole($name);
            $role->description = $description;
            $role->ruleName = $rule;
            $this->authManager->add($role);
            echo "{$name} role created" . PHP_EOL;
        }
        return $role;
    }

    /**
     * @param string $name Role name
     * @return boolean
     */
    function removeRole($name) {
        $role = new Role();
        $role->name = $name;
        if($this->authManager->remove($role)) {
            echo "{$name} role removed" . PHP_EOL;
            return true;
        }
        return false;
    }

    /**
     * Create permission.
     * If permission already exists doesn nothing.
     *
     * @param string $name Permission name
     * @param string $description Permission description
     * @param string $rule Rule name
     * @return Permission the new Permission object
     * @throws \Exception
     */
    function createPermission($name, $description, $rule = NULL) {
        if (($permission = $this->authManager->getPermission($name))) {
            echo "{$name} permission already exists" . PHP_EOL;
        } else {
            $permission = $this->authManager->createPermission($name);
            $permission->description = $description;
            $permission->ruleName = $rule;
            $this->authManager->add($permission);
            echo "{$name} permission created" . PHP_EOL;
        }
        return $permission;
    }

    /**
     * @param string $name Permission name
     * @return boolean
     */
    function removePermission($name) {
        $permission = new Permission();
        $permission->name = $name;
        if($this->authManager->remove($permission)) {
            echo "{$name} permission removed" . PHP_EOL;
            return true;
        }
        return false;
    }

    /**
     * Create rule.
     * If permission already exists does nothing.
     *
     * @param Rule $item Rule item
     * @return Rule the new Rule object
     * @throws \Exception
     */
    function createRule($item) {
        if (($this->authManager->getRule($item->name))) {
            echo "{$item->name} rule already exists" . PHP_EOL;
        } else {
            $this->authManager->add($item);
            echo "{$item->name} rule created" . PHP_EOL;
        }
        return $item;
    }

    /**
     * Assigns child role to parent
     *
     * @param Item $parent Parent role name. Identifies what permission will be expanded with new rights
     * @param Item $child Child role name. Identifies what permission to add to parent
     * @throws \yii\base\Exception
     */
    function assignChild($parent, $child) {
        if (!$this->authManager->hasChild($parent, $child)) {
            $this->authManager->addChild($parent, $child);
            echo "New child '{$child->name}' added to '{$parent->name}'" . PHP_EOL;
        } else {
            echo "Role '{$child->name}' was already added to '{$parent->name}'" . PHP_EOL;
        }
    }

    /**
     * Assigns child role to parent
     *
     * @param Item $parent Parent role name. Identifies what permission will be expanded with new rights
     * @param Item $child Child role name. Identifies what permission to add to parent
     */
    function removeChild($parent, $child) {
        if ($this->authManager->hasChild($parent, $child)) {
            $this->authManager->removeChild($parent, $child);
            echo "Child '{$child->name}' removed from '{$parent->name}'" . PHP_EOL;
        } else {
            echo "Child '{$child->name}' was already removed from '{$parent->name}'" . PHP_EOL;
        }
    }

}

<?php

namespace common\modules\user\rbac;

use common\modules\user\components\DbManager;
use common\modules\user\validators\RbacValidator;
use Yii;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @property array $availableItems
 * @property string $item_name [varchar(64)]
 * @property int $created_at [int(11)]
 */
class Assignment extends ActiveRecord {

    /** @var array */
    public $items = [];

    /** @var integer */
    public $user_id;

    /** @var boolean */
    public $updated = false;

    /** @var DbManager */
    protected $manager;


    public static function tableName() {
        return '{{%user_rbac_assignment}}';
    }

    /**
     * @inheritdoc
     *
     * @throws InvalidConfigException
     */
    public function init() {
        parent::init();
        $this->manager = Yii::$app->authManager;
        if ($this->user_id === null) {
            throw new InvalidConfigException('user_id must be set');
        }
        $this->items = array_keys($this->manager->getItemsByUser($this->user_id));
    }

    /** @inheritdoc */
    public function attributeLabels() {
        return [
            'items' => \Yii::t('rbac', 'Items'),
        ];
    }

    /** @inheritdoc */
    public function rules() {
        return [
            ['user_id', 'required'],
            ['items', RbacValidator::class],
            ['user_id', 'integer']
        ];
    }

    /**
     * Updates auth assignments for user.
     * @return boolean
     * @throws \Exception
     */
    public function updateAssignments() {
        if (!$this->validate()) {
            return false;
        }
        if (!is_array($this->items)) {
            $this->items = [];
        }
        $assignedItems = $this->manager->getItemsByUser($this->user_id);
        $assignedItemsNames = array_keys($assignedItems);

        foreach (array_diff($assignedItemsNames, $this->items) as $item) {
            $this->manager->revoke($assignedItems[$item], $this->user_id);
        }
        foreach (array_diff($this->items, $assignedItemsNames) as $item) {
            $this->manager->assign($this->manager->getItem($item), $this->user_id);
        }
        $this->updated = true;
        return true;
    }

    /**
     * Returns all available auth items to be attached to user.
     * @return array
     */
    public function getAvailableItems() {
        return ArrayHelper::map($this->manager->getItems(), 'name', function ($item) {
            return empty($item->description) ? $item->name : $item->name . ' (' . $item->description . ')';
        });
    }

}

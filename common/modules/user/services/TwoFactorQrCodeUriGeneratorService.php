<?php

namespace common\modules\user\services;

use common\modules\user\models\UserModel;
use Da\TwoFA\Manager;
use Da\TwoFA\Service\QrCodeDataUriGeneratorService;
use Da\TwoFA\Service\TOTPSecretKeyUriGeneratorService;
use Yii;

class TwoFactorQrCodeUriGeneratorService {

    /**
     * @var UserModel
     */
    protected $user;

    /**
     * TwoFactorQrCodeUriGeneratorService constructor.
     *
     * @param UserModel $user
     */
    public function __construct(UserModel $user) {
        $this->user = $user;
    }

    /** @inheritdoc */
    public function run() {
        $user = $this->user;
        if (!$user->auth_tf_key) {
            $user->auth_tf_key = (new Manager())->generateSecretKey();
            $user->updateAttributes(['auth_tf_key']);
        }
        $totpUri = (new TOTPSecretKeyUriGeneratorService(Yii::$app->name, $user->email, $user->auth_tf_key))->run();
        return (new QrCodeDataUriGeneratorService($totpUri))->run();
    }
}

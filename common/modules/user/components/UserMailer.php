<?php

namespace common\modules\user\components;

use common\components\MailerComponent;
use common\modules\user\models\TokenModel;
use common\modules\user\models\UserModel;
use Yii;

/**
 * Mailer.
 */
class UserMailer extends MailerComponent {

    /** @var string */
    public $viewPath = '@common/modules/user/views/mail';

    /**
     * Sends an email to a user after registration.
     *
     * @param UserModel  $user
     * @param TokenModel $token
     * @param bool  $showPassword
     *
     * @return bool
     */
    public function sendWelcomeMessage(UserModel $user, TokenModel $token = null, $showPassword = false) {
        return $this->sendMessage($user->email, Yii::t('user', 'Welcome to {0}', Yii::$app->name), 'welcome', [
            'user' => $user,
            'token' => $token,
            'showPassword' => $showPassword
        ]);
    }

    /**
     * Sends an email to a user after registration.
     *
     * @param UserModel $user
     * @param string $password
     *
     * @return bool
     */
    public function sendResetPasswordMessage(UserModel $user, $password) {
        return $this->sendMessage($user->email, Yii::t('user', 'Welcome to {0}', Yii::$app->name), 'reset-password', [
            'user' => $user,
            'password' => $password
        ]);
    }

    /**
     * Sends an email to a user with confirmation link.
     *
     * @param UserModel  $user
     * @param TokenModel $token
     *
     * @return bool
     */
    public function sendConfirmationMessage(UserModel $user, TokenModel $token) {
        return $this->sendMessage($user->email, Yii::t('user', 'Confirm account on {0}', Yii::$app->name), 'confirmation', [
            'user' => $user,
            'token' => $token
        ]);
    }

    /**
     * Sends an email to a user with reconfirmation link.
     *
     * @param UserModel  $user
     * @param TokenModel $token
     *
     * @return bool
     */
    public function sendReconfirmationMessage(UserModel $user, TokenModel $token) {
        if ($token->type == TokenModel::TYPE_CONFIRM_NEW_EMAIL) {
            $email = $user->unconfirmed_email;
        } else {
            $email = $user->email;
        }
        return $this->sendMessage($email, Yii::t('user', 'Confirm email change on {0}', Yii::$app->name), 'reconfirmation', [
            'user' => $user,
            'token' => $token
        ]);
    }

    /**
     * Sends an email to a user with recovery link.
     *
     * @param UserModel  $user
     * @param TokenModel $token
     *
     * @return bool
     */
    public function sendRecoveryMessage(UserModel $user, TokenModel $token) {
        return $this->sendMessage($user->email, Yii::t('user', 'Complete password reset on {0}', Yii::$app->name), 'recovery', [
            'user' => $user,
            'token' => $token
        ]);
    }

    /**
     * Sends an email to a user with recovery link.
     *
     * @param UserModel $user
     * @return bool
     */
    public function sendFailedLoginsMessage(UserModel $user) {
        return $this->sendMessage($user->email, Yii::t('user', 'Too many failed logins on {0}', Yii::$app->name), 'failed-logins', [
            'user' => $user
        ]);
    }

}

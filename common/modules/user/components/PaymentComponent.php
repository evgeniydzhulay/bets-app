<?php

namespace common\modules\user\components;

use common\modules\user\components\deposit\AbstractDeposit;
use common\modules\user\components\deposit\PiastrixDepositComponent;
use common\modules\user\components\withdraw\AbstractWithdraw;
use common\modules\user\components\withdraw\WebmoneyWithdrawComponent;
use Yii;
use yii\base\Component;

/**
 * @property-read AbstractDeposit[] $depositServices
 * @property-read AbstractWithdraw[] $withdrawalServices
 */
class PaymentComponent extends Component {

	protected $_withdrawalServices;
	protected $_depositServices;

	/**
	 * @return AbstractWithdraw[]
	 */
	public function getWithdrawalServices() {
		if(empty($this->_withdrawalServices)) {
			$this->_withdrawalServices = [
				'webmoney' => Yii::createObject(WebmoneyWithdrawComponent::class),
			];
		}
		return $this->_withdrawalServices;
	}

	/**
	 * @return AbstractDeposit[]
	 */
	public function getDepositServices() {
		if(empty($this->_depositServices)) {
			$this->_depositServices = [
				'piastrix' => Yii::createObject(PiastrixDepositComponent::class),
			];
		}
		return $this->_depositServices;
	}
}
<?php

namespace common\modules\user\components;

use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\rbac\DbManager as BaseDbManager;
use yii\rbac\ManagerInterface as BaseManagerInterface;

/**
 * This Auth manager changes visibility and signature of some methods from \yii\rbac\DbManager.
 *
 * @author Dmitry Erofeev <dmeroff@gmail.com>
 */
class DbManager extends BaseDbManager implements BaseManagerInterface {

    /**
     * @var string the name of the table storing user_rbacorization items. Defaults to "user_rbac_item".
     */
    public $itemTable = '{{%user_rbac_item}}';

    /**
     * @var string the name of the table storing user_rbacorization item hierarchy. Defaults to "user_rbac_item_child".
     */
    public $itemChildTable = '{{%user_rbac_item_child}}';

    /**
     * @var string the name of the table storing user_rbacorization item assignments. Defaults to "user_rbac_assignment".
     */
    public $assignmentTable = '{{%user_rbac_assignment}}';

    /**
     * @var string the name of the table storing rules. Defaults to "user_rbac_rule".
     */
    public $ruleTable = '{{%user_rbac_rule}}';

    /** @inheritdoc */
    public function getItem($name) {
        return parent::getItem($name);
    }

    /**
     * @param  int|null $type         If null will return all auth items.
     * @param  array    $excludeItems Items that should be excluded from result array.
     * @return array
     */
    public function getItems($type = null, $excludeItems = []) {
        $query = (new Query())->from($this->itemTable);
        if ($type !== null) {
            $query->where(['type' => $type]);
        } else {
            $query->orderBy('type');
        }
        foreach ($excludeItems as $name) {
            $query->andWhere('name != :item', ['item' => $name]);
        }
        $items = [];
        foreach ($query->all($this->db) as $row) {
            $items[$row['name']] = $this->populateItem($row);
        }
        return $items;
    }

    /**
     * Returns both roles and permissions assigned to user.
     *
     * @param  integer $userId
     * @param bool $onlyDirect
     * @return array
     */
    public function getItemsByUser($userId, $onlyDirect = true) {
        if (empty($userId)) {
            return [];
        }
        return ArrayHelper::merge($this->getRolesByUser($userId), $onlyDirect ? $this->getDirectPermissionsByUser($userId) : $this->getPermissionsByUser($userId));
    }

    protected $assignedRoles = [];
    protected $assignedPermissions = [];
    protected $assignedDirectPermissions = [];

    public function getRolesByUser($userId, $refresh = false) {
        if (!ISSET($this->assignedRoles[$userId]) || $refresh) {
            $this->assignedRoles[$userId] = parent::getRolesByUser($userId);
        }
        return $this->assignedRoles[$userId];
    }

    public function getDirectPermissionsByUser($userId, $refresh = false) {
        if (!ISSET($this->assignedDirectPermissions[$userId]) || $refresh) {
            $this->assignedDirectPermissions[$userId] = parent::getDirectPermissionsByUser($userId);
        }
        return $this->assignedDirectPermissions[$userId];
    }

    public function getPermissionsByUser($userId, $refresh = false) {
        if (!ISSET($this->assignedPermissions[$userId]) || $refresh) {
            $this->assignedPermissions[$userId] = parent::getPermissionsByUser($userId);
        }
        return $this->assignedPermissions[$userId];
    }

    public function hasAssignment($user, $item) {
        return (
            ISSET($this->getRolesByUser($user)[$item]) ||
            ISSET($this->getPermissionsByUser($user)[$item])
        );
    }

}

<?php

namespace common\modules\user\components\deposit;

use common\modules\user\helpers\PiastrixHelper;
use common\modules\user\models\UserModel;
use common\modules\user\models\UserPurseDepositModel;
use common\modules\user\models\UserPurseHistoryModel;
use yii\web\NotFoundHttpException;
use yii\web\UnauthorizedHttpException;

class PiastrixDepositComponent extends AbstractDeposit {

	public static $source = UserPurseHistoryModel::SOURCE_PIASTRIX;

	/**
	 * Get visible service title
	 * @return string
	 */
	public static function getTitle () {
		return 'Piastrix';
	}

	public $service = 'piastrix';

	/**
	 * @param UserModel $user
	 * @return bool
	 */
	public function canDeposit(UserModel $user = null) : bool {
		return !$user || in_array($user->purse_currency, array_keys(PiastrixHelper::CURRENCIES));
	}

	/**
	 * Initialise deposit process
	 *
	 * @param UserModel $user
	 * @param float $amount
	 * @return mixed
	 */
	public function deposit (UserModel $user, float $amount) {
		if(!$this->canDeposit($user) || !($deposit = $this->createDeposit($user->id, $amount))) {
			return false;
		}
		$amountFormatted = number_format($amount, 2, '.', '');
		$currency = PiastrixHelper::CURRENCIES[$user->purse_currency];
		$sign = PiastrixHelper::createSign([
			'amount' => $amountFormatted,
			'shop_order_id' => $deposit->id,
			'currency' => $currency,
		]);
		$lang = $user->lang === 'ru' ? 'ru' : 'en';
		$shopID = PiastrixHelper::getParams()['shop'];
		\Yii::$app->response->redirect("https://pay.piastrix.com/{$lang}/pay?amount={$amountFormatted}&currency={$currency}&shop_id={$shopID}&shop_order_id={$deposit->id}&sign={$sign}");
		return \Yii::$app->response->send();
	}

	/**
	 * Deposit result from gateway processing
	 *
	 * @return mixed
	 */
	public function result () {
		$request = \Yii::$app->request->post();
		$receivedSign = $request['sign'] ?? null;
		unset($request['sign']);
		$sign = PiastrixHelper::createSign(array_filter($request, function($value) {
			return $value !== null && $value !== '';
		}));
		if($receivedSign === $sign) {
			$deposit = UserPurseDepositModel::findOne(['id' => $request['shop_order_id']]);
			if($deposit !== null) {
				if($request['status'] === 'success') {
					$deposit->accept();
				} else if($request['status'] === 'rejected') {
					$deposit->decline();
				}
				\Yii::$app->response->content = 'OK';
				\Yii::$app->response->setStatusCode(200);
				return \Yii::$app->response->send();
			}
			throw new NotFoundHttpException();
		}
		throw new UnauthorizedHttpException();
	}
}
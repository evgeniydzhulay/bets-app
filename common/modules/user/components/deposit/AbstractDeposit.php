<?php

namespace common\modules\user\components\deposit;

use common\modules\user\models\UserModel;
use common\modules\user\models\UserPurseDepositModel;
use common\modules\user\models\UserPurseHistoryModel;
use yii\base\Component;

abstract class AbstractDeposit extends Component {

	public static $source;

	/**
	 * Get visible service title
	 * @return string
	 */
	public static abstract function getTitle ();

	/**
	 * Get available deposit fields
	 *
	 * @return array
	 */
	public static function getFields () {
		return [];
	}

	/**
	 * @param UserModel|null $user
	 * @return bool
	 */
	public function canDeposit(UserModel $user = null) : bool {
		return true;
	}

	/**
	 * Initialise deposit process
	 *
	 * @param UserModel $user
	 * @param float $amount
	 * @return mixed
	 */
	public abstract function deposit (UserModel $user, float $amount);

	/**
	 * Deposit result processing from gateway
	 *
	 * @return mixed
	 */
	public abstract function result ();

	/**
	 * @param int $user
	 * @param float $amount
	 * @return bool|UserPurseDepositModel
	 */
	protected function createDeposit(int $user, float $amount) {
		$model = \Yii::createObject(UserPurseDepositModel::class);
		$model->setAttributes([
			'user_id' => $user,
			'amount' => $amount,
			'service' => static::$source,
			'status' => UserPurseHistoryModel::STATUS_NEW,
		]);
		if ($model->save()) {
			return $model;
		}
		return false;
	}

	/**
	 * @param int $uid
	 * @return bool|UserModel
	 */
	protected static function getUserModel(int $uid) {
		$user = UserModel::findOne(['id' => $uid]);
		if(!$user) {
			return false;
		}
		return $user;
	}
}
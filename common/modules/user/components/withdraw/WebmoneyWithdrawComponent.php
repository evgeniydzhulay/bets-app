<?php


namespace common\modules\user\components\withdraw;

class WebmoneyWithdrawComponent extends AbstractWithdraw {

	public $service = 'webmoney';

	const SOURCE = self::SOURCE_WEBMONEY;

	/**
	 * Get visible service title
	 * @return string
	 */
	public static function getTitle () {
		return 'Webmoney';
	}

	/**
	 * Get available withdraw fields
	 *
	 * @return array
	 */
	public static function getFields () {
		return [
			'target' => [
				'name' => 'target',
				'label' => 'Purse',
				'required' => true,
			],
			'target_additional' => [
				'name' => 'target_additional',
				'label' => 'WMid',
				'required' => true,
			],

		];
	}

	/**
	 * Initialise withdraw process
	 *
	 * @param int $user
	 * @param float $amount
	 * @param string $target
	 * @param string $additional
	 * @return mixed
	 */
	public function withdraw (int $user, float $amount, $target = '', $additional = '') {

	}
}
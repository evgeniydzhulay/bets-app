<?php

namespace common\modules\user\components\withdraw;

use yii\base\Component;

abstract class AbstractWithdraw extends Component {

	const SOURCE_WEBMONEY = 1;

	const SOURCES = [
		self::SOURCE_WEBMONEY
	];
	public $service = null;

	/**
	 * Get visible service title
	 * @return string
	 */
	public static abstract function getTitle ();

	/**
	 * Get available withdraw fields
	 *
	 * @return array
	 */
	public static function getFields () {
		return [];
	}

	/**
	 * Initialise withdraw process
	 *
	 * @param int $user
	 * @param float $amount
	 * @param string $target
	 * @param string $additional
	 * @return mixed
	 */
	public abstract function withdraw (int $user, float $amount, $target = '', $additional = '');
}
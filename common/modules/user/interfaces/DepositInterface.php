<?php

namespace common\modules\user\interfaces;

use common\modules\user\models\UserModel;

interface DepositInterface {

	/**
	 * @param float $amount
	 * @param UserModel $user
	 * @return boolean
	 */
    public function create($amount, UserModel $user);
}

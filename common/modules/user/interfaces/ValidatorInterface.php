<?php

namespace common\modules\user\interfaces;

interface ValidatorInterface {

    /**
     * @return bool
     */
    public function validate();
}

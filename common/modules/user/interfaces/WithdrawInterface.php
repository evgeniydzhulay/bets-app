<?php

namespace common\modules\user\interfaces;

use common\modules\user\models\UserModel;

interface WithdrawInterface {

	/**
	 * @param float $amount
	 * @param UserModel $user
	 * @param string $target
	 * @param string $comment
	 * @return boolean
	 */
    public function create($amount, UserModel $user, $target, $comment = '');
}

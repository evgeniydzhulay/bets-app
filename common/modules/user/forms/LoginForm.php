<?php

namespace common\modules\user\forms;

use common\modules\user\components\UserMailer;
use common\modules\user\helpers\UserPassword;
use common\modules\user\models\UserApplicationModel;
use common\modules\user\models\UserModel;
use common\modules\user\Module;
use common\modules\user\validators\TwoFactorCodeValidator;
use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * LoginForm get user's login and password, validates them and logs the user in. If user has been blocked, it adds
 * an error to login form.
 */
class LoginForm extends Model {

    /** @var string User's email */
    public $login;

    /** @var string User's plain password */
    public $password;

	/** @var string User's 2fa code */
	public $twofacode;

    /** @var string Whether to remember the user */
    public $rememberMe = false;

    public $showTwoFactorInput = false;

    /** @var UserMailer */
    protected $mailer;

    /** @var UserModel */
    protected $user;

    /**
     * @param UserMailer $mailer
     * @param array $config
     */
    public function __construct(UserMailer $mailer, $config = []) {
        $this->mailer = $mailer;
        parent::__construct($config);
    }

    /** @inheritdoc */
    public function attributeLabels() {
        return [
            'login' => Yii::t('user', 'Login'),
            'password' => Yii::t('user', 'Password'),
	        'twofacode' => Yii::t('user', 'Two factor authentication code'),
            'rememberMe' => Yii::t('user', 'Remember me next time'),
        ];
    }

    /** @inheritdoc */
    public function rules() {
        return [
            'requiredFields' => [['login', 'password'], 'required'],
            'loginTrim' => ['login', 'trim'],
            'passwordValidate' => [
                'password',
                function ($attribute) {
                    $userExist = ($this->user != null);
                    $failedLimitReached = $userExist && ($this->user->login_fail_count > 4) && ($this->user->login_fail_last > (time() - 300));
                    $bypassFailsLimit = (($session = Yii::$app->get('session', false)) && $session->get('bypassFailsLimit', false));
	                if (!$userExist) {
		                $this->addError($attribute, Yii::t('user', 'Invalid login or password'));
	                } else if ($userExist && $failedLimitReached && !$bypassFailsLimit) {
	                    $this->addError($attribute, Yii::t('user', 'Too many failed attempts. Please wait for 5 minutes'));
	                } else if(!UserPassword::validate($this->password, $this->user->password_hash)) {
	                    UserModel::updateAll([
		                    'login_fail_count' => new Expression('`login_fail_count` + 1'),
		                    'login_fail_last' => time()
	                    ], ['id' => $this->user->id]);
	                    if (($this->user->login_fail_count++) == 5) {
		                    $this->mailer->sendFailedLoginsMessage($this->user);
	                    }
		                $this->addError($attribute, Yii::t('user', 'Invalid login or password'));
                    }
                }
            ],
            'rememberMe' => ['rememberMe', 'boolean'],
	        'twoFACodeTrim' => ['twofacode', 'trim'],
	        'twoFACodeValidate' => ['twofacode', function ($attribute) {
		        if ($this->user === null || !(new TwoFactorCodeValidator (
			        $this->user,
			        $this->twofacode,
			        $this->getModule()->twoFactorAuthenticationCycles
		        ))->validate()) {
			        $this->addError($attribute, Yii::t('user', 'Invalid two factor authentication code'));
		        }
	        }],
        ];
    }

    /**
     * Validates form and logs the user in.
     *
     * @return bool whether the user is logged in successfully
     * @throws \yii\base\InvalidConfigException
     */
    public function login() {
        if ($this->validate()) {
            $this->user->login_fail_count = 0;
            $this->user->save(true, ['login_fail_count']);
            if (($session = Yii::$app->get('session', false))) {
                $session->remove('bypassFailsLimit');
            }
            return Yii::$app->getUser()->login($this->user, 3600 * 24 * ($this->rememberMe ? 365 : 3));
        } else {
            return false;
        }
    }

	/**
	 * Validates form and logs the user in.
	 *
	 * @param string $app Application name
	 * @param integer $type Application type
	 * @return string|bool New key
	 */
	public function loginApplication($app = null, $type = null) {
		if ($this->validate()) {
			$this->user->login_fail_count = 0;
			$this->user->save(true, ['login_fail_count']);
			if($keyModel = $this->user->createAccessToken($app, $type)) {
				return $keyModel->token;
			}
		}
		return false;
	}

	public function getUserModel() {
		return $this->user;
	}

    /** @inheritdoc */
    public function formName() {
        return 'login-form';
    }

	/** @inheritdoc */
    public function beforeValidate() {
        if (parent::beforeValidate()) {
        	$field = 'id';
	        if (filter_var($this->login, FILTER_VALIDATE_EMAIL)) {
		        $field = 'email';
	        } else if(!empty($this->login) && $this->login[0] == '+') {
	        	$field = 'phone';
	        }
	        $this->user = UserModel::find()->andWhere([$field => trim($this->login)])->one();
	        if(!!$this->user && !!$this->user->auth_tf_enabled && empty($this->twofacode)) {
		        $this->showTwoFactorInput = true;
		        $this->addErrors([
			        'twofacode' => Yii::t('user', 'Need two factor authentication code')
		        ]);
	        }
            return true;
        } else {
            return false;
        }
    }

	/**
	 * @return Module
	 */
	public function getModule() {
		return Yii::$app->getModule('user');
	}

}

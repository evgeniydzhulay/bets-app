<?php

namespace common\modules\user\forms;

use common\modules\user\components\UserMailer;
use common\modules\user\models\TokenModel;
use common\modules\user\models\UserModel;
use Yii;
use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Model for collecting data on password recovery.
 */
class RecoveryForm extends Model {

    /** @var string */
    public $email;

    /** @var string */
    public $password;

    /** @var UserModel */
    protected $user;

    /** @var UserMailer */
    protected $mailer;

    /**
     * @param UserMailer $mailer
     * @param array  $config
     */
    public function __construct(UserMailer $mailer, $config = []) {
        $this->mailer = $mailer;
        parent::__construct($config);
    }

    /** @inheritdoc */
    public function attributeLabels() {
        return [
            'email' => Yii::t('user', 'Email'),
            'password' => Yii::t('user', 'Password'),
        ];
    }

    /** @inheritdoc */
    public function scenarios() {
        $scenarios = parent::scenarios();
        return ArrayHelper::merge($scenarios, [
            'request' => ['email'],
            'reset' => ['password'],
        ]);
    }

    /** @inheritdoc */
    public function rules() {
        return [
            'emailTrim' => ['email', 'filter', 'filter' => 'trim'],
            'emailRequired' => ['email', 'required'],
            'emailPattern' => ['email', 'email'],
            'emailExist' => ['email', 'exist', 'targetClass' => UserModel::class, 'message' => Yii::t('user', 'There is no user with this email address')],
            'emailUnconfirmed' => ['email', function ($attribute) {
                if ($this->user->isBlocked) {
                    $this->addError($attribute, Yii::t('user', 'Your account has been blocked'));
                }
            }],
            'passwordRequired' => ['password', 'required'],
            'passwordLength' => ['password', 'string', 'min' => 6],
        ];
    }

    public function beforeValidate () {
    	if(!empty($this->email)) {
    		$this->user = UserModel::find()->andWhere(['email' => $this->email])->one();
	    }
	    return parent::beforeValidate();
    }

	/**
     * Sends recovery message.
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function sendRecoveryMessage() {
        if ($this->validate()) {
            /** @var TokenModel $token */
            $token = Yii::createObject([
	            'class' => TokenModel::class,
	            'user_id' => $this->user->id,
	            'type' => TokenModel::TYPE_RECOVERY,
            ]);
            if (!$token->save(false) || !$this->mailer->sendRecoveryMessage($this->user, $token)) {
                return false;
            }
            return true;
        }
        return false;
    }

    /**
     * Resets user's password.
     *
     * @param TokenModel $token
     *
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function resetPassword(TokenModel $token) {
        if (!$this->validate() || $token->user === null) {
            return false;
        }
        if ($token->user->resetPassword($this->password)) {
            Yii::$app->session->addFlash('success', Yii::t('user', 'Your password has been changed successfully.'));
            $token->delete();
            Yii::$app->session->set('bypassFailsLimit', true);
        } else {
            Yii::$app->session->addFlash('danger', Yii::t('user', 'An error occurred and your password has not been changed. Please try again later.'));
        }
        return true;
    }

    /**
     * @inheritdoc
     */
    public function formName() {
        return 'recovery-form';
    }

}

<?php

namespace common\modules\user\forms;

use common\modules\partners\models\PartnersPromocodesModel;
use common\modules\user\models\UserBonusesModel;
use common\modules\user\models\UserModel;
use common\modules\user\models\UserPurseHistoryModel;
use common\modules\user\validators\PhoneNumberValidation;
use Yii;
use yii\base\Model;

/**
 * Registration form collects user input on registration process, validates it and creates new User model.
 */
class RegistrationForm extends Model {

    /** @var string User email address */
    public $email;

	/** @var string User phone number */
	public $phone;

    /** @var string Password */
    public $password;

	/** @var string User country */
	public $country;

	/** @var string User currency */
	public $currency;

	/** @var string Name of user */
	public $name;

	/** @var string Surname of user */
	public $surname;

	/** @var boolean Does user accepted agreement */
	public $agree = false;

	/** @var string Promocode to use after registration */
	public $promocode;

	public $bonus;

	/** @var UserModel */
	protected $_user;

	/**
	 * @return UserModel
	 */
	public function getUser() {
		return $this->_user;
	}

	/** @inheritdoc */
    public function rules() {
        return [
            // email rules
            'emailTrim' => ['email', 'filter', 'filter' => 'trim', 'when' => function() {
	            return !empty($this->email);
            }],
            'emailPattern' => ['email', 'email'],
            'emailUnique' => ['email', 'unique', 'targetClass' => UserModel::class,
                'message' => Yii::t('user', 'This email address has already been taken'),
	            'when' => function() {
        	        return !empty($this->email);
	            }
            ],
	        // phone rules
	        'phoneLength' => ['phone', 'string', 'max' => 255],
	        'phoneUnique' => ['phone', 'unique', 'targetClass' => UserModel::class,
		        'message' => Yii::t('user', 'This phone has already been taken'), 'when' => function() {
			        return !empty($this->phone);
		        }
	        ],
	        'phoneTrim' => ['phone', 'trim', 'when' => function() {
		        return !empty($this->phone);
	        }],
	        'phoneValid' => ['phone', PhoneNumberValidation::class, 'when' => function() {
		        return !empty($this->phone);
	        }],
            // password rules
            'passwordRequired' => ['password', 'required'],
            'passwordLength' => ['password', 'string', 'min' => 6],
	        // profile data
	        'namesTrim' => [['name','surname'], 'filter', 'filter' => 'trim'],
	        'namesLength' => [['name','surname'], 'string', 'max' => 255],
	        // currency rules
	        'currencyRequired' => ['currency', 'required'],
	        'currencyExists' => ['currency', 'in', 'range' => array_keys(Yii::$app->get('currencies')->getList())],
	        // country rules
	        'countryExists' => ['country', 'in', 'range' => Yii::$app->get('locations')->getCountryKeys()],
	        // promocode rules
	        'promocodeTrim' => ['promocode', 'filter', 'filter' => 'trim'],
	        'promocodeLength' => ['promocode', 'string', 'max' => 250],
	        'bonus' => ['bonus', 'in', 'range' => UserPurseHistoryModel::BONUSES_REGISTRATION],
	        'agreeRequired' => ['agree', 'required', 'requiredValue' => 1, 'message' => Yii::t('user', 'You must accept terms and conditions')],
        ];
    }

	/** @inheritdoc */
    public function attributeLabels() {
        return [
            'email' => Yii::t('user', 'Email'),
	        'phone' => Yii::t('user', 'Phone'),
	        'bonus' => Yii::t('user', 'Bonus'),
	        'promocode' => Yii::t('user', 'Promocode'),
            'password' => Yii::t('user', 'Password'),
	        'name' => Yii::t('user', 'Name'),
	        'surname' => Yii::t('user', 'Surname'),
	        'country' => Yii::t('user', 'Country'),
	        'currency' => Yii::t('user', 'Currency'),
	        'agree' => Yii::t('user', 'I accept terms and conditions')
        ];
    }

    public function attributeHints () {
	    return parent::attributeHints() + [
		    'email' => Yii::t('user', 'Enter a valid e-mail address. A confirmation e-mail will be sent to this address. This address will be used in future for communicating with you.'),
		    'phone' => Yii::t('user', 'Leave your phone and we will send you a personal password.'),
		    'bonus' => Yii::t('user', 'The selected bonus cannot be changed since you\'ve been registered.'),
		    'promocode' => Yii::t('user', 'Enter your promocode if you have it.'),
		    'password' => Yii::t('user', "At least {digits} letters.", [
		    	'digits' => $this->rules()['passwordLength']['min']
		    ]),
		    'name' => Yii::t('user', 'Your name as stated in your passport.'),
		    'surname' => Yii::t('user', 'Your surname as stated in your passport.'),
		    'country' => Yii::t('user', 'Enter the country of your permanent residence.'),
		    'currency' => Yii::t('user', 'The currency you select will affect your deposit and withdrawal transaction methods. Once set, account currency cannot be changed.'),
		    'agree' => Yii::t('user', 'I accept terms and conditions')
	    ];
    }

	/** @inheritdoc */
    public function formName() {
        return 'register-form';
    }

	public function bonusesList() {
		return array_intersect_key(UserPurseHistoryModel::bonuses(), array_flip(UserPurseHistoryModel::BONUSES_REGISTRATION));
	}

    /**
     * Registers a new user account. If registration was successful it will set flash message.
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function register() {
        if (!$this->validate()) {
            return false;
        }
        /** @var UserModel $user */
        $user = Yii::createObject(UserModel::class);
	    $user->scenario = UserModel::SCENARIO_REGISTER;
	    $user->setAttributes([
		    'email' => !empty($this->email) ? $this->email : null,
		    'phone' => !empty($this->phone) ? $this->phone : null,
		    'password' => $this->password,
		    'purse_currency' => $this->currency,
	    ]);
        if (!$user->register()) {
        	Yii::error($user->errors);
            return false;
        }
        unset($user->profile);
        $user->profile->setAttributes([
		    'name' => $this->name,
		    'surname' => $this->surname,
	        'country' => $this->country,
	    ]);
	    $user->profile->save();
	    $this->_user = $user;
	    if(!empty($this->promocode) && PartnersPromocodesModel::isUsed($this->promocode, $user->id)) {
	    	$bonusPromocode = Yii::createObject(UserBonusesModel::class);
	    	$bonusPromocode->setAttributes([
	    		'user_id' => $user->id,
			    'bonus' => UserBonusesModel::TYPE_REGISTRATION_PROMOCODE,
		    ]);
		    $bonusPromocode->save(false);
	    }

	    if(!empty($this->bonus) && in_array($this->bonus,UserPurseHistoryModel::BONUSES_REGISTRATION)) {
		    $bonusSelected = Yii::createObject(UserBonusesModel::class);
		    $bonusSelected->setAttributes([
			    'user_id' => $user->id,
			    'bonus' => $this->bonus,
		    ]);
		    $bonusSelected->save(false);
	    }
        return true;
    }

}

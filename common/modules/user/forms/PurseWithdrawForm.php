<?php

namespace common\modules\user\forms;

use common\modules\user\components\withdraw\AbstractWithdraw;
use common\modules\user\components\PaymentComponent;
use common\modules\user\models\UserModel;
use common\modules\user\models\UserPurseHistoryModel;
use common\modules\user\models\UserPurseWithdrawModel;
use Yii;
use yii\base\Model;
use yii\validators\NumberValidator;

/**
 * @property int $minimalAmount
 * @property PaymentComponent $component
 * @property array $services
 */
class PurseWithdrawForm extends Model {

	/**
	 * @var UserModel
	 */
	public $user;

	public $service;
	public $amount;
	public $target;
	public $target_additional;
	public $name;
	public $surname;

	public $min = 10;

	public function rules () {
		return [
			'amountNumber' => [
				'amount', function() {
					$min = $this->getMinimalAmount();
					$max = $this->user->purse_amount;
					$validator = Yii::createObject([
						'class' => NumberValidator::class,
						'min' => $min, 'tooSmall' => Yii::t('user', 'You can\'t withdraw less than {amount}', [
							'amount' => Yii::$app->formatter->asCurrency($min, $this->user->purse_currency),
						]),
						'max' => $max, 'tooBig' => Yii::t('user', 'You don\'t have enough money. You can withdraw only {amount}', [
							'amount' => Yii::$app->formatter->asCurrency($max, $this->user->purse_currency),
						])
					]);
					$validator->validateAttribute($this, 'amount');
				}
			],
			'required' => [['amount', 'service'], 'required'],
			'serviceList' => ['service', 'in', 'range' => \array_keys($this->getServices())],
			'targetRequired' => [['target', 'target_additional', 'name', 'surname'], function($attr) {
				if(!is_array($this->$attr)) {
					$this->$attr = [];
				}
				foreach($this->$attr AS $key => $value) {
					if(empty($this->service) || $this->service != $key) {
						continue;
					}
					$service = $this->getServices()[$key];
					$field = $service::getFields()[$attr] ?? false;
					if($field && ($field['required'] ?? false) && empty($this->$attr[$key])) {
						$this->addError("{$attr}[{$key}]", Yii::t('yii', '{attribute} cannot be blank.', [
							'attribute' => $field['label']
						]));
						return false;
					}
				}
				return true;
			}, 'skipOnEmpty' => false]
		];
	}

	protected function getMinimalAmount() {
		if(!empty($this->service)) {
			$service = $this->getServices()[$this->service];
			if(!empty($service->withdraw_min)) {
				return $service->withdraw_min;
			}
		}
		return $this->min;
	}

	public function getComponent() : PaymentComponent {
		return Yii::$app->getModule('user')->get('payment');
	}

	/**
	 * @return AbstractWithdraw[]
	 */
	public function getServices() {
		return $this->getComponent()->getWithdrawalServices();
	}

	/**
	 * @return bool|UserPurseWithdrawModel
	 */
	public function save() {
		if(!$this->validate()) {
			return false;
		}
		$model = Yii::createObject(UserPurseWithdrawModel::class);
		$model->setAttributes([
			'user_id' => Yii::$app->user->id,
			'amount' => $this->amount,
			'service' => $this->getServices()[$this->service]::SOURCE,
			'status' => UserPurseHistoryModel::STATUS_NEW,
			'target' => $this->target[$this->service] ?? null,
			'target_additional' => $this->target_additional[$this->service] ?? null,
			'name' => $this->name[$this->service] ?? null,
			'surname' => $this->surname[$this->service] ?? null,
		]);
		return $model->save() ? $model : false;
	}

	public function attributeLabels () {
		return [
			'service' => Yii::t('purse', 'Service'),
			'amount' => Yii::t('purse', 'Amount'),
			'name' => Yii::t('user', 'Name'),
			'surname' => Yii::t('user', 'Surname'),
		];
	}
}
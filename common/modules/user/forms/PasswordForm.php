<?php

namespace common\modules\user\forms;

use common\modules\user\helpers\UserPassword;
use Yii;
use common\modules\user\components\UserMailer;
use common\modules\user\models\UserModel;
use yii\base\InvalidConfigException;
use yii\base\Model;

/**
 * SettingsForm gets user's email and password and changes them.
 *
 * @property UserModel $user
 */
class PasswordForm extends Model {

	/** @var string */
	public $new_password;

	/** @var string */
	public $repeat_password;

	/** @var string */
	public $current_password;

	/** @var UserMailer */
	protected $mailer;

	/** @var UserModel */
	private $_user;

	/** @return UserModel */
	public function getUser() {
		if ($this->_user == null) {
			$this->_user = Yii::$app->user->identity;
		}
		return $this->_user;
	}

	/** @inheritdoc */
	public function __construct(UserMailer $mailer, $config = []) {
		$this->mailer = $mailer;
		parent::__construct($config);
	}

	/** @inheritdoc */
	public function rules() {
		return [
			[['new_password', 'repeat_password', 'current_password'], 'required'],
			['repeat_password', 'compare', 'compareAttribute' => 'new_password', 'message'=> Yii::t('user',"Passwords don't match") ],
			'newPasswordLength' => ['new_password', 'string', 'min' => 6],
			'currentPasswordValidate' => ['current_password', function ($attr) {
				if (!UserPassword::validate($this->$attr, $this->user->password_hash)) {
					$this->addError($attr, Yii::t('user', 'Current password is not valid'));
				}
			}],
		];
	}

	/** @inheritdoc */
	public function attributeLabels() {
		return [
			'new_password' => Yii::t('user', 'New password'),
			'repeat_password' => Yii::t('user', 'Repeat password'),
			'current_password' => Yii::t('user', 'Current password'),
		];
	}

	/** @inheritdoc */
	public function formName() {
		return 'password-form';
	}

	/**
	 * Saves new account settings.
	 *
	 * @return bool
	 * @throws InvalidConfigException
	 */
	public function save() {
		if ($this->validate()) {
			$this->user->scenario = 'settings';
			$this->user->password = $this->new_password;
			return $this->user->save();
		}
		return false;
	}

}

<?php

namespace common\modules\user\forms;

use common\modules\user\components\UserMailer;
use common\modules\user\models\TokenModel;
use common\modules\user\models\UserModel;
use Yii;
use yii\base\Model;
use yii\swiftmailer\Mailer;

/**
 * ResendForm gets user email address and validates if user has already confirmed his account. If so, it shows error
 * message, otherwise it generates and sends new confirmation token to user.
 *
 * @property UserModel $user
 */
class ResendForm extends Model {

    /** @var string */
    public $email;

    /** @var UserModel */
    private $_user;

    /** @var Mailer */
    protected $mailer;

    /**
     * @param UserMailer $mailer
     * @param array  $config
     */
    public function __construct(UserMailer $mailer, $config = []) {
        $this->mailer = $mailer;
        parent::__construct($config);
    }

    /**
     * @return UserModel
     */
    public function getUser() {
        if ($this->_user === null) {
	        $this->_user = UserModel::find()->andWhere(['email' => $this->email])->one();
        }
        return $this->_user;
    }

    /** @inheritdoc */
    public function rules() {
        return [
            'emailRequired' => ['email', 'required'],
            'emailPattern' => ['email', 'email'],
            'emailExist' => ['email', 'exist', 'targetClass' => UserModel::class],
            'emailConfirmed' => [
                'email',
                function () {
                    if ($this->user != null && $this->user->getIsConfirmed()) {
                        $this->addError('email', Yii::t('user', 'This account has already been confirmed'));
                    }
                }
            ],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels() {
        return [
            'email' => Yii::t('user', 'Email'),
        ];
    }

    /** @inheritdoc */
    public function formName() {
        return 'resend-form';
    }

    /**
     * Creates new confirmation token and sends it to the user.
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function resend() {
        if (!$this->validate()) {
            return false;
        }
        /** @var TokenModel $token */
        $token = Yii::createObject([
            'class' => TokenModel::class,
            'user_id' => $this->user->id,
            'type' => TokenModel::TYPE_APPROVE,
        ]);
        $token->save(false);
        $this->mailer->sendConfirmationMessage($this->user, $token);
        return true;
    }

}

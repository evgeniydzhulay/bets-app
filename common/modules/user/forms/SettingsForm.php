<?php

namespace common\modules\user\forms;

use common\modules\user\components\UserMailer;
use common\modules\user\helpers\UserPassword;
use common\modules\user\models\TokenModel;
use common\modules\user\models\UserModel;
use common\modules\user\validators\PhoneNumberValidation;
use Yii;
use yii\base\Model;
use yii\swiftmailer\Mailer;
use yii\validators\DateValidator;

/**
 * SettingsForm gets user's email and password and changes them.
 *
 * @property UserModel $user
 */
class SettingsForm extends Model {

	/** @var integer */
	public $id;

    /** @var string */
    public $email;

	/** @var string */
	public $phone;

    /** @var string */
    public $name;

    /** @var string */
    public $surname;

	/** @var string */
	public $patrynomic;

	/** @var string */
	public $country;

	/** @var string */
	public $city;

	/** @var string */
	public $address;

	/** @var string */
	public $dob;

	/** @var string */
	public $passport_series;

	/** @var string */
	public $passport_issuer;

	/** @var string */
	public $passport_date;

    /** @var string */
    public $current_password;

    /** @var Mailer */
    protected $mailer;

    /** @var UserModel */
    private $_user;

    /** @return UserModel */
    public function getUser() {
        if ($this->_user == null) {
            $this->_user = Yii::$app->user->identity;
        }

        return $this->_user;
    }

    public function scenarios () {
	    $list = [
	    	self::SCENARIO_DEFAULT => [
	    		'name', 'surname', 'patrynomic',
			    'city', 'address', 'dob',
			    'passport_series', 'passport_issuer', 'passport_date'
		    ]
	    ];
	    if(empty($this->user->profile->country)) {
	    	$list[self::SCENARIO_DEFAULT][] = 'country';
	    }
	    if(empty($this->user->email)) {
		    $list[self::SCENARIO_DEFAULT][] = 'email';
	    }
	    if(empty($this->user->phone)) {
		    $list[self::SCENARIO_DEFAULT][] = 'phone';
	    }
	    return $list;
    }

	/** @inheritdoc */
    public function __construct(UserMailer $mailer, $config = []) {
        $this->mailer = $mailer;
        $this->setAttributes([
	        'id' => $this->user->id,
            'email' => $this->user->unconfirmed_email ?: $this->user->email,
	        'phone' => $this->user->phone,
	        'name' => $this->user->profile->name,
	        'surname' => $this->user->profile->surname,
	        'patrynomic' => $this->user->profile->patrynomic,
	        'country' => $this->user->profile->country,
	        'city' => $this->user->profile->city,
	        'address' => $this->user->profile->address,
	        'dob' => $this->user->profile->dob,
	        'passport_series' => $this->user->profile->passport_series,
	        'passport_issuer' => $this->user->profile->passport_issuer,
	        'passport_date' => $this->user->profile->passport_date,
        ], false);
        if(empty($this->country)) {
        	$this->setAttributes([
        		'country' => Yii::$app->get('locations')->getUserCountryCode()
	        ]);
        }
        parent::__construct($config);
    }

    /** @inheritdoc */
    public function rules() {
        return [
            'profileRequired' => [[
            	'email', 'name', 'surname', 'dob',
	            'passport_series', 'passport_date'
            ], 'required'],
            'profileTrim' => [[
            	'email', 'name', 'surname', 'patrynomic', 'city', 'address',
	            'passport_series', 'passport_issuer', 'passport_date'
            ], 'filter', 'filter' => 'trim'],
	        'profileStringLength' => [['name', 'surname', 'patrynomic'], 'string', 'max' => 255],
	        'profileTextLength' => [['address', 'passport_series', 'passport_issuer'], 'string', 'max' => 65000],
	        'date' => [['dob', 'passport_date'], 'date', 'type' => DateValidator::TYPE_DATE, 'max' => date('Y-m-d'), 'format' => 'php:Y-m-d', 'tooBig' => Yii::t('yii', '{attribute} is invalid.')],
	        'countryRequired' => ['country', 'required'],
	        'countryList' => ['country', 'in', 'range' => Yii::$app->get('locations')->getCountryKeys()],
            'emailPattern' => ['email', 'email'],
            'emailUnique' => ['email', 'unique', 'targetClass' => UserModel::class, 'when' => function ($model) {
                return ($this->user->email != $model->email) || !empty($this->email);
            }, 'message' => Yii::t('user', 'This email address has already been taken')],

	        // phone rules
	        'phoneLength' => ['phone', 'string', 'max' => 255],
	        'phoneUnique' => ['phone', 'unique', 'targetClass' => UserModel::class,
		        'message' => Yii::t('user', 'This phone has already been taken'), 'when' => function() {
			        return !empty($this->phone);
		        }
	        ],
	        'phoneValid' => ['phone', PhoneNumberValidation::class],

            'currentPasswordRequired' => ['current_password', 'required'],
            'currentPasswordValidate' => ['current_password', function ($attr) {
                if (!UserPassword::validate($this->$attr, $this->user->password_hash)) {
                    $this->addError($attr, Yii::t('user', 'Current password is not valid'));
                }
            }],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels() {
        return [
	        'id' => Yii::t('user', 'Account number'),
            'email' => Yii::t('user', 'Email'),
	        'phone' => Yii::t('user', 'Phone'),
            'current_password' => Yii::t('user', 'Current password'),
	        'name' => Yii::t('user', 'Name'),
	        'surname' => Yii::t('user', 'Surname'),
	        'patrynomic' => Yii::t('user', 'Patrynomic'),
	        'dob' => Yii::t('user', 'Date of birth'),
	        'country' => Yii::t('user', 'Country'),
	        'city' => Yii::t('user', 'City'),
	        'address' => Yii::t('user', 'Address'),
	        'passport_series' => Yii::t('user', 'Passport series'),
	        'passport_issuer' => Yii::t('user', 'Passport issuer'),
	        'passport_date' => Yii::t('user', 'Passport issue date'),
        ];
    }

    /** @inheritdoc */
    public function formName() {
        return 'settings-form';
    }

    /**
     * Saves new account settings.
     *
     * @return bool
     * @throws \yii\base\InvalidConfigException
     */
    public function save() {
        if ($this->validate()) {
            $this->user->scenario = 'settings';
            $profile = $this->user->profile;
            $profile->setAttributes([
	            'name' => $this->name,
	            'surname' => $this->surname,
	            'patrynomic' => $this->patrynomic,
	            'country' => $this->country,
	            'city' => $this->city,
	            'address' => $this->address,
	            'dob' => $this->dob,
	            'passport_series' => $this->passport_series,
	            'passport_issuer' => $this->passport_issuer,
	            'passport_date' => $this->passport_date,
            ]);
            $tr = Yii::$app->db->beginTransaction();
            try {
	            if ($this->email == $this->user->email && $this->user->unconfirmed_email != null) {
		            $this->user->unconfirmed_email = null;
	            } elseif ($this->email != $this->user->email) {
		            $this->user->unconfirmed_email = $this->email;
	            }
	            $this->user->phone = !empty($this->phone) ? $this->phone : null;
	            $this->user->withdraw_enabled = (
	            	!empty($this->name) &&
		            !empty($this->surname) &&
		            !empty($this->country) &&
		            !empty($this->dob) &&
		            !empty($this->passport_series) &&
		            !empty($this->passport_date)
	            );
	            if($this->user->save() && $profile->save()) {
		            if ($this->email != $this->user->email) {
			            $this->user->unconfirmed_email = $this->email;
			            /** @var TokenModel $token */
			            $token = Yii::createObject([
				            'class' => TokenModel::class,
				            'user_id' => $this->user->id,
				            'type' => TokenModel::TYPE_CONFIRM_NEW_EMAIL,
			            ]);
			            $token->save(false);
			            $this->mailer->sendReconfirmationMessage($this->user, $token);
			            Yii::$app->session->addFlash('info', Yii::t('user', 'A confirmation message has been sent to your new email address'));
		            }
	            	$tr->commit();
		            return true;
	            }
            } catch (\Throwable $e) {
            	Yii::error($e);
            }
			$tr->rollBack();
        }
        return false;
    }

    public function getCountriesList() {
	    return Yii::$app->get('locations')->getCountries();
    }

}

<?php

namespace common\modules\user\forms;

use common\modules\user\components\deposit\AbstractDeposit;
use common\modules\user\components\PaymentComponent;
use common\modules\user\models\UserModel;
use common\modules\user\models\UserPurseDepositModel;
use Yii;
use yii\base\Model;
use yii\validators\NumberValidator;

/**
 * @property int $minimalAmount
 * @property PaymentComponent $component
 * @property AbstractDeposit[] $services
 */
class PurseDepositForm extends Model {

	/**
	 * @var UserModel
	 */
	public $user;

	public $service;
	public $amount;

	public $min = 10;

	public function rules () {
		return [
			'amountNumber' => [
				'amount', function() {
					$min = $this->getMinimalAmount();
					$validator = Yii::createObject([
						'class' => NumberValidator::class, 'min' => $min,
						'tooSmall' => Yii::t('user', 'You can\'t deposit less than {amount}', [
							'amount' => Yii::$app->formatter->asCurrency($min, $this->user->purse_currency),
						]),
					]);
					$validator->validateAttribute($this, 'amount');
				}
			],
			'required' => [['amount', 'service'], 'required'],
			'serviceList' => ['service', 'in', 'range' => \array_keys($this->getServices())],
		];
	}

	protected function getMinimalAmount() {
		if(!empty($this->service)) {
			$service = $this->getServices()[$this->service];
			if(!empty($service->deposit_min)) {
				return $service->deposit_min;
			}
		}
		return $this->min;
	}

	public function getComponent() : PaymentComponent {
		return Yii::$app->getModule('user')->get('payment');
	}

	/**
	 * @return AbstractDeposit[]
	 */
	public function getServices() {
		return $this->getComponent()->getDepositServices();
	}

	/**
	 * @return bool|UserPurseDepositModel
	 */
	public function save() {
		if(!$this->validate()) {
			return false;
		}
		return $this->getServices()[$this->service]->deposit(Yii::$app->user->identity, $this->amount);
	}

	public function attributeLabels () {
		return [
			'service' => Yii::t('purse', 'Service'),
			'amount' => Yii::t('purse', 'Amount'),
		];
	}
}
<?php

namespace common\modules\user\validators;

use common\modules\user\interfaces\ValidatorInterface;
use common\modules\user\models\UserModel;
use Da\TwoFA\Exception\InvalidSecretKeyException;
use Da\TwoFA\Manager;

class TwoFactorCodeValidator implements ValidatorInterface {

	protected $user;
	protected $code;
	protected $cycles;

	/**
	 * TwoFactorCodeValidator constructor.
	 *
	 * @param UserModel $user
	 * @param $code
	 * @param int $cycles
	 */
	public function __construct (UserModel $user, $code, $cycles = 0) {
		$this->user = $user;
		$this->code = $code;
		$this->cycles = $cycles;
	}

	/**
	 * @throws InvalidSecretKeyException
	 * @return bool|int
	 */
	public function validate () {
		$manager = new Manager();
		return $manager->setCycles($this->cycles)->verify($this->code, $this->user->auth_tf_key);
	}
}

<?php

namespace common\modules\user\validators;

use yii\validators\RegularExpressionValidator;

class PhoneNumberValidation extends RegularExpressionValidator {

	public $pattern = '/^([+]?[\s0-9]+)?(\d{3}|[(]?[0-9]+[)])?([-]?[\s]?[0-9])+$/';
}
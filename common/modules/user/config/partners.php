<?php

use common\modules\user\models\UserModel;

return [
	'components' => [
		'user' => [
			'enableAutoLogin' => true,
			'loginUrl' => ['/user/auth/login'],
			'identityClass' => UserModel::class,
		],
	],
];

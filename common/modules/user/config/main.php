<?php

use common\modules\user\components\PaymentComponent;
use common\modules\user\Module;

return [
	'modules' => [
		'user' => [
			'class' => Module::class,
			'components' => [
				'payment' => [
					'class' => PaymentComponent::class
				]
			]
		],
	],
	'params' => [
		'user.registrationBonus.min' => 50,
		'user.registrationBonus.max' => 10000,
	]
];

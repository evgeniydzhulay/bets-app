<?php

return [
	'controllerMap' => [
		'migrate' => [
			'migrationNamespaces' => [
				'common\modules\user\migrations',
			],
		],
	],
];

<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model \common\modules\user\forms\PasswordForm */

$this->title = Yii::t('user', 'Account password');
$this->params['breadcrumbs'][] = $this->title;
$this->beginContent('@common/modules/user/views/shared/profile.php');
$form = ActiveForm::begin([
    'id' => 'account-form',
    'type' => ActiveForm::TYPE_VERTICAL,
    'options' => [
        'class' => 'box box-default profile-password',
    ],
    'formConfig' => [
        'labelSpan' => 3,
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
]);
?>
<div class="box-header">
    <span>
        <?= Html::encode($this->title) ?>
    </span>
</div>
<div class="box-body">
    <?= $form->field($model, 'new_password')->passwordInput() ?>
    <?= $form->field($model, 'repeat_password')->passwordInput() ?>
    <?= $form->field($model, 'current_password')->passwordInput() ?>
</div>
<div class="box-footer">
    <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-red']) ?>
</div>
<?php ActiveForm::end(); ?>
<?php
$this->endContent();
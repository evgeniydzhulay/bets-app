<?php

use common\modules\user\forms\SettingsForm;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/**
 * @var $this  yii\web\View
 * @var $form  yii\widgets\ActiveForm
 * @var $model SettingsForm
 * @var $twofaenabled bool
 */

$this->title = Yii::t('user', 'Account settings');
$this->params['breadcrumbs'][] = $this->title;
$this->beginContent('@common/modules/user/views/shared/profile.php');
?>
<div class="box box-default profile-account">
    <div class="box-header">
        <span><?= Yii::t('user', 'My info'); ?></span>
    </div>
    <div class="box-body notice-important">
        <span><b><?= Yii::t('user', 'Important'); ?></b><?= Yii::t('user', ': Fill in your personal details to be able to withdraw money! It is mandatory to fill in all fields marked with an asterisk (*). You can use Latin characters only.'); ?></span>
    </div>
    <div class="box-body">
        <div class="profile-account-main">
            <?php
            $form = ActiveForm::begin([
                'id' => 'account-form',
                'type' => ActiveForm::TYPE_VERTICAL,
                'formConfig' => [
                    'labelSpan' => 3,
                ],
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
            ]);
            ?>
            <?= $form->field($model, 'id')->staticInput() ?>
            <?= empty($model->email) ? $form->field($model, 'email') : $form->field($model, 'email')->staticInput(); ?>
            <?= empty($model->phone) ? $form->field($model, 'phone') : $form->field($model, 'phone')->staticInput(); ?>
            <?= empty($model->name) ? $form->field($model, 'name') : $form->field($model, 'name')->staticInput(); ?>
            <?= empty($model->surname) ? $form->field($model, 'surname') : $form->field($model, 'surname')->staticInput(); ?>
            <?= empty($model->country) ? '' :
                $form->field($model, 'country', ['staticValue' => $model->getCountriesList()[$model->country] ])->staticInput()
            ?>
            <?= $form->field($model, 'patrynomic') ?>
            <?= empty($model->country) ?
                $form->field($model, 'country')->widget(Select2::class, [
                    'data' => $model->getCountriesList(),
                ]) : ''
            ?>
            <?= $form->field($model, 'city') ?>
            <?= $form->field($model, 'address')->textarea() ?>
            <?= $form->field($model, 'dob')->widget(DatePicker::class, [
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                ]
            ]) ?>
            <?= $form->field($model, 'passport_series') ?>
            <?= $form->field($model, 'passport_issuer')->textarea() ?>
            <?= $form->field($model, 'passport_date')->widget(DatePicker::class, [
                'options' => [
                    'placeholder' => Yii::t('user', 'Passport issue date'),
                ],
                'pluginOptions' => [
                    'format' => 'yyyy-mm-dd',
                ]
            ])->label(Yii::t('user', 'Issue date')) ?>
            <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-red']) ?>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="profile-account-right">
            <?=
            Html::a(Yii::t('user', 'Two factor authentication'), ['two-factor/index'], [
                'class' => 'btn btn-red',
                'data' => [
                    'toggle' => 'modal',
                    'target' => '#modal2fa'
                ]
            ]);
            ?>
        </div>
    </div>
</div>
<div id="modal2fa" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content modal2fa-form__container">
        </div>
    </div>
</div>
<?php
$this->endContent();
<?php

use common\modules\user\forms\LoginForm;
use yii\helpers\Html;
use kartik\form\ActiveForm;

/**
 * @var yii\web\View $this
 * @var LoginForm $model
 */
?>
<?php
$form = ActiveForm::begin([
	'id' => 'login-form',
	'type' => ActiveForm::TYPE_HORIZONTAL,
	'formConfig' => [
		'labelSpan' => 4,
	],
	'options' => [
		'class' => 'panel login-form'
	],
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'validateOnBlur' => false,
	'validateOnType' => false,
	'validateOnChange' => false,
])
?>
	<div class="panel-heading">
		<?= Html::encode($this->title) ?>
	</div>
	<div class="panel-body login-form-fields">
		<?= $form->field($model, 'login', [
			'inputOptions' => [
				'autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1'
			],
			'showRequiredIndicator' => false,
		])->label("<i class='fas fa-envelope'></i>" . Yii::t('user', 'Email or ID')) ?>
		<?= $form->field($model, 'password', [
			'inputOptions' => [
				'class' => 'form-control', 'tabindex' => '2'
			],
			'showRequiredIndicator' => false,
		])->passwordInput()->label("<i class='fas fa-lock'></i>" . Yii::t('user', 'Password')) ?>
		<?php
		if($model->showTwoFactorInput) {
			echo $form->field($model, 'twofacode', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '3']]);
		}
		?>
	</div>
	<div class="panel-body login-form-additionals">
		<div class="text-center login-form-remember checkbox-branded">
			<?= Html::activeCheckbox($model, 'rememberMe', ['label' => false]); ?>
			<label for="login-form-rememberme">
				<?= Yii::t('user', 'Remember me next time'); ?>
			</label>
		</div>
		<div class="login-form-links">
			<?= Html::a(Yii::t('user', 'Forgot password?'), ['auth/request']) ?><br />
			<?= Html::submitButton(Yii::t('user', 'Sign in'), ['class' => 'btn btn-red']) ?><br />
			<?= Html::a(Yii::t('user', 'Don\'t have an account? Sign up!'), ['auth/register']) ?><br />
			<?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['auth/resend']) ?>
		</div>
	</div>
<?php ActiveForm::end(); ?>
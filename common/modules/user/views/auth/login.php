<?php

use common\modules\user\forms\LoginForm;

/**
 * @var yii\web\View $this
 * @var LoginForm $model
 */
$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 login-form-page">
        <?= $this->render('loginForm', [
	        'model' => $model,
        ]); ?>
    </div>
</div>

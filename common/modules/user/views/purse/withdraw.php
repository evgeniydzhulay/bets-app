<?php

use common\modules\user\components\withdraw\AbstractWithdraw;
use common\modules\user\forms\PurseWithdrawForm;
use kartik\form\ActiveForm;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Html;

/**
 * @var $this View
 * @var $model PurseWithdrawForm
 * @var $services AbstractWithdraw[]
 * @var $enabled bool
 */

$this->beginContent('@common/modules/user/views/shared/profile.php');
if(!$enabled) {
	?>
    <div class="alert alert-danger text-center" role="alert">
		<?= Yii::t('user', 'You need to fill your profile data to request withdrawal.'); ?><br>
        <a href="<?= \yii\helpers\Url::toRoute(['/user/auth/account']); ?>" class="btn btn-danger">
			<?= Yii::t('user', 'Go to profile.'); ?>
        </a>
    </div>
	<?php
}
$form = ActiveForm::begin([
    'id' => 'account-withdraw-form',
    'type' => ActiveForm::TYPE_HORIZONTAL,
    'options' => [
        'class' => 'box box-default form-horizontal',
    ],
    'formConfig' => [
        'labelSpan' => 3,
    ],
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
]);
// var_dump(ArrayHelper::map($services, 'service', function($item) {
//     return "img src='{$item->service}'";
// }));
?>
<?= $form->field($model, 'service')->radioButtonGroup(ArrayHelper::map($services, 'service', 'title')); ?>
<div class="form_with_wrap">
    <div class="form_with_close">x</div>
    <?= $form->field($model, 'amount')->textInput(['type' => 'number', 'step' => '0.01']) ?>
    <?php foreach ($services AS $key => $service) { ?>
        <div class="payment-service-block payment-service-block-<?= $key ?> <?= $model->service != $key ? 'hidden' : ''; ?>">
            <?php foreach ($service::getFields() AS $field ){
                echo $form->field($model, "{$field['name']}[{$key}]", [
                    'options' => [
                       'class' => [(!! ($field['required'] ?? false ) ? $form->requiredCssClass : ''), 'form-group']
                    ]
                ])->label($field['label']);
            } ?>
        </div>
    <?php } ?>
    <div class="box-footer">
        <div class="form-group">
            <div class="col-lg-offset-3 col-lg-9">
                <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
            </div>
        </div>
    </div>
</div>
<?php
ActiveForm::end();
$js = <<< JS
$(document).on('change', '#pursewithdrawform-service input', function(e) {
    e.preventDefault();
    var selected = jQuery('#pursewithdrawform-service input:checked').val();
    jQuery('.payment-service-block').addClass('hidden');
    jQuery('.payment-service-block-' + selected).removeClass('hidden');
});
JS;
	$this->registerJs($js);
$this->endContent();
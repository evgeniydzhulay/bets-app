<?php

use common\modules\user\components\withdraw\AbstractWithdraw;
use common\modules\user\forms\PurseDepositForm;
use kartik\form\ActiveForm;
use yii\web\View;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;

/**
 * @var $this View
 * @var $model PurseDepositForm
 * @var $services AbstractWithdraw[]
 */
$currency = Yii::$app->user->identity->purse_currency;
$this->beginContent('@common/modules/user/views/shared/profile.php');

$form = ActiveForm::begin([
	'id' => 'account-deposit-form',
	'type' => ActiveForm::TYPE_HORIZONTAL,
	'options' => [
		'class' => 'box box-default form-horizontal',
	],
	'formConfig' => [
		'labelSpan' => 3,
	],
	'enableAjaxValidation' => true,
	'enableClientValidation' => false,
]);
?>
<div class="box-body">
    <div class="auto_row">
        <div class="col-md-3"></div>
        <div class="auto_item" data-value="100">100</div>
        <div class="auto_item" data-value="500">500</div>
        <div class="auto_item" data-value="1000">1000</div>
        <div class="auto_item" data-value="2000">2000</div>
    </div>
    <?php
    if(count($services) > 1) {
        echo $form->field($model, 'service')->radioButtonGroup(ArrayHelper::map($services, 'service', 'title'));
    } else {
        echo $form->field($model, 'service', ['template' => '{input}'])->hiddenInput([
            'readonly' => true,
            'value' => $services[array_key_first($services)]->service
        ]);
    }
    echo $form->field($model, 'amount', [
        'addon' => [ 'prepend' => [ 'content'=> $currency ] ]
    ])->textInput(['type' => 'number', 'step' => '0.01']);
    ?>
</div>
<div class="col-lg-offset-3 col-lg-9 f_n">
    <div class="pm_images_row">
        <div class="pm_image">
            <img src="/static/img/payment-icons/3.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/4.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/5.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/8.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/20.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/64.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/1.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/7.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/9.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/14.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/15.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/16.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/17.png" alt="">
        </div>
        <div class="pm_image">
            <img src="/static/img/payment-icons/19.png" alt="">
        </div>
    </div>
</div>
<div class="box-footer">
    <div class="col-lg-offset-3 col-lg-9">
        <?= Html::submitButton(Yii::t('user', 'Deposit'), ['class' => 'btn btn-block btn-success']) ?>
    </div>
</div>
<?php
ActiveForm::end();
$this->endContent();
<?php

use common\modules\user\models\UserPurseHistoryModel;
use common\modules\user\search\PurseHistorySearch;
use common\widgets\GridView;
use kartik\date\DatePicker;
use kartik\field\FieldRange;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var PurseHistorySearch $searchModel
 * @var array $types
 * @var array $statuses
 * @var array $sources
 */

$this->beginContent('@common/modules/user/views/shared/profile.php');

$this->title = Yii::t('purse', 'Purse history');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="purse-history">
<div class="box no-body">
	<div class="box-header">
		<span><?= Yii::t('purse', 'Purse history'); ?></span>
	</div>
</div>
<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'id' => 'purse-history-search',
	'class' => 'purse-history-search',
	'pjax' => false,
	'toolbar' => [
		['content' =>
			Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::toRoute(['index']), [
				'class' => 'btn btn-default btn-sm',
				'title' => Yii::t('user', 'Reset filter')]
			)
		],
	],
	'columns' => [
		[
			'attribute' => 'type',
			'filter' => $types,
			'headerOptions' => ['width' => '130px'],
			'value' => function ($model) use ($types) {
				return $types[$model->type] ?? '';
			},
		],
		[
			'attribute' => 'created_at',
			'value' => function ($model) {
				return date('Y-m-d H:i:s', $model->created_at);
			},
			'filter' => DatePicker::widget([
				'model' => $searchModel,
				'id' => 'purse-history-search-created_at',
				'attribute' => 'created_at',
				'type' => DatePicker::TYPE_COMPONENT_PREPEND,
				'pickerButton' => false,
				'pluginOptions' => [
					'autoclose' => true,
					'format' => 'yyyy-mm-dd'
				]
			]),
			'headerOptions' => ['width' => '150px'],
		],
		[
			'attribute' => 'amount',
			'filter' => FieldRange::widget([
				'model' => $searchModel,
				'id' => 'purse-history-search-amount',
				'attribute1' => 'amount_from',
				'attribute2' => 'amount_to',
				'type' => FieldRange::INPUT_TEXT,
				'template' => '{widget}{error}',
				'separator' => '-',
			]),
			'headerOptions' => ['width' => '150px'],
			'content' => function($model) {
				return Yii::$app->formatter->asCurrency($model->amount, Yii::$app->user->identity->purse_currency);
			}
		],
		[
			'attribute' => 'source',
			'filter' => $sources,
			'format' => 'raw',
			'value' => function ($model) {
				if($model['type'] == UserPurseHistoryModel::TYPE_WITHDRAW) {
					$source = (UserPurseHistoryModel::sources()[$model['source']] ?? '');
					$source .= !empty($model->withdraw->target) ? ", {$model->withdraw->target}" : '';
					$source .= !empty($model->withdraw->target_additional) ? ", {$model->withdraw->target_additional}" : '';
					return $source;
				}
				if($model['type'] == UserPurseHistoryModel::TYPE_DEPOSIT) {
					return UserPurseHistoryModel::sources()[$model['source']] ?? '';
				}
				if($model['type'] == UserPurseHistoryModel::TYPE_BONUS) {
				    if($model['source'] == UserPurseHistoryModel::BONUS_EXPRESS_RETURN) {
					    return Yii::t('purse', 'Return for lost express') . Html::a(" #{$model['description']}", [
                            '/games/bets/index', 'id' => $model['description']
                        ]);
                    }
				}
				return $model['description'] ?? '';
			},
		],
		[
			'attribute' => 'status',
			'filter' => $statuses,
			'headerOptions' => ['width' => '130px'],
			'value' => function ($model) use ($statuses) {
				return $statuses[$model->status] ?? '';
			},
		],
	],
]);
?>
</div>
<?php $this->endContent();
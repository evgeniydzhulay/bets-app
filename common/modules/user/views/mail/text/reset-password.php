<?php

use common\modules\user\models\UserModel;

/**
 * @var UserModel   $user
 * @var string $password
 * @var bool   $showPassword
 */
?>
<?= Yii::t('user', 'Hello') ?>,

<?= Yii::t('user', 'Your account on {0} has been updated', Yii::$app->name) ?>.

<?= Yii::t('user', 'Current password') . ' ' . $password ?>
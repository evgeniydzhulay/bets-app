<?= Yii::t('user', 'Warning!'); ?>

<?= Yii::t('user', 'Someone tries to login to your account at {website}', [
    'website' => Yii::$app->name
]); ?>
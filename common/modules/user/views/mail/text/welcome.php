<?php

use common\modules\user\models\TokenModel;
use common\modules\user\models\UserModel;

/**
 * @var UserModel   $user
 * @var TokenModel  $token
 * @var bool   $showPassword
 */
?>
<?= Yii::t('user', 'Hello') ?>,

<?= Yii::t('user', 'Your account on {0} has been created', Yii::$app->name) ?>.

<?php if ($token !== null): ?>
    <?= Yii::t('user', 'In order to complete your registration, please click the link below') ?>.

    <?= $token->url ?>

    <?= Yii::t('user', 'If you cannot click the link, please try pasting the text into your browser') ?>.
<?php endif ?>

<?php if ($showPassword): ?>
    <?= Yii::t('user', 'Current password') . ' ' . $user->password ?>
<?php endif ?>

<?= Yii::t('user', 'If you did not make this request you can ignore this email') ?>.

<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 16px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0; color: #F00">
    <?= Yii::t('user', 'Warning!'); ?>
</p>
<p style="font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif; font-size: 14px; line-height: 1.6; font-weight: normal; margin: 0 0 10px; padding: 0;">
    <?= Yii::t('user', 'Someone tries to login to your account at {website}', [
        'website' => \yii\helpers\Html::a(Yii::$app->name, Yii::getAlias('@web/frontend'))
    ]); ?>
</p>
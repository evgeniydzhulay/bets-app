<?php

/** @var string $uri */

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
?>
<div class="container-fluid twofa-form">
    <div class="alert alert-info row" id="tfmessage">
        <?= Yii::t('user', 'Scan the QrCode with Google Authenticator App, then insert its temporary code on the box and submit.') ?>
    </div>
    <div class="text-center">
        <img id="qrCode" src="<?= $uri ?>"/>
    </div>
    <?php
    $form = ActiveForm::begin([
        'id' => 'login-form',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
        'validateOnBlur' => false,
        'validateOnType' => false,
        'validateOnChange' => false,
        'options' => [
            'class' => 'row'
        ]
    ])
    ?>
    <div class="well well-sm">
        <div class="input-group">
            <input type="text" class="form-control" id="tfcode" name="code" placeholder="<?= Yii::t('user', 'Two factor authentication code') ?>"/>
            <span class="input-group-btn">
                <?= Html::submitButton(Yii::t('user', 'Enable'), ['class' => 'btn btn-red btn-block btn-submit-code']) ?>
            </span>
        </div>
    </div>
    <div class="twofa-apps text-center alert">
        <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2" class="twofa-badge twofa-android" target="_blank">
            <img src="/static/img/badge-google-play.png" />
        </a>
        <a href="https://itunes.apple.com/ru/app/id388497605" class="twofa-badge twofa-apple" target="_blank">
            <img src="/static/img/badge-app-store.png" />
        </a>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php
$verify = Url::to(['enable']);
$js = <<< JS
$(document).on('click', '.btn-submit-code', function(e) {
       e.preventDefault();
       var btn = $(this);
       btn.prop('disabled', true);
       $.getJSON('{$verify}', {code: $('#tfcode').val()}, function(data) {
          btn.prop('disabled', false);
          if(data.success) {
              $('#tfmessage').removeClass('alert-danger').addClass('alert-success').text(data.message);
              setTimeout(function() { 
                  location.reload(true);
              }, 2000);
          } else {
              $('input#tfcode').val('');
              $('#tfmessage').removeClass('alert-info').addClass('alert-danger').text(data.message);
          }
       }).fail(function(){ 
           btn.prop('disabled', false);
       });
    });
JS;

$this->registerJs($js);
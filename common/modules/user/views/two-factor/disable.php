<?php

use yii\helpers\Html;
?>

<div class="container-fluid text-center">
	<div class="alert alert-info row">
		<?= Yii::t('user', 'Two factor authentication is enabled') ?>
	</div>
	<?= Html::a(Yii::t('user', 'Disable'), ['disable'], [
		'class' => 'btn btn-red',
    ]); ?>
</div>
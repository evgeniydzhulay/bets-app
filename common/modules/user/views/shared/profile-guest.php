<div class="profile-heading">
    <span class="profile-heading-label"><?= Yii::t('user', 'My account'); ?></span>
</div>
<div class="profile-content">
	<?= $content; ?>
</div>
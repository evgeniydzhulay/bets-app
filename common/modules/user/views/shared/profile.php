<div class="container-fluid my_acc_wrap">
    <div class="profile-heading">
        <span class="profile-heading-label"><?= Yii::t('user', 'My account'); ?></span>
    </div>
	<div class="col-md-2">
        <div class="panel-group" id="user-menu">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading-deposit">
                    <a href="<?= \yii\helpers\Url::toRoute(['/user/purse/deposit']); ?>">
                        <?= Yii::t('user', 'Deposit'); ?>
                    </a>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading-profile">
                    <a role="button" data-toggle="collapse" data-parent="#user-menu" aria-expanded="false" href="#content-profile" aria-controls="content-profile">
                        <?= Yii::t('user', 'Profile'); ?>
                    </a>
                </div>
                <div id="content-profile" class="panel-collapse collapse"  role="tabpanel" aria-labelledby="heading-profile">
                    <div class="panel-body">
                        <?= \yii\widgets\Menu::widget([
                            'encodeLabels' => false,
                            'items' => [
                                [
	                                'label' => "<span class='fas fa-info-circle fa-fw'></span>" . Yii::t('user', 'My info'),
                                    'url' => ['/user/auth/account']
                                ],
	                            [
		                            'label' => "<span class='fas fa-unlock fa-fw'></span>" . Yii::t('user', 'Change password'),
		                            'url' => ['/user/auth/password']
	                            ],
	                            [
		                            'label' => "<span class='fas fa-envelope fa-fw'></span>" . Yii::t('support', 'Requests to support'),
		                            'url' => ['/support/request/index']
	                            ],
	                            [
		                            'label' => "<span class='fas fa-address-card fa-fw'></span>" .Yii::t('support', 'Request to administrator'),
		                            'url' => ['/support/request/create']
	                            ],
                            ]
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading-profile">
                    <a href="<?= \yii\helpers\Url::toRoute(['/games/bets/index']); ?>">
                        <?= Yii::t('games', 'Bets history'); ?>
                    </a>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading-profile">
                    <a href="<?= \yii\helpers\Url::toRoute(['/user/purse/index']); ?>">
                        <?= Yii::t('user', 'Transaction history'); ?>
                    </a>
                </div>
            </div>
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="heading-profile">
                    <a href="<?= \yii\helpers\Url::toRoute(['/user/purse/withdraw']); ?>">
                        <?= Yii::t('user', 'Withdraw funds'); ?>
                    </a>
                </div>
            </div>
        </div>
	</div>
	<div class="col-md-10 profile-content">
		<?= $content; ?>
	</div>
</div>

<script>
    if(window.location.pathname.includes('auth') || window.location.pathname.includes('request')){
        document.getElementById('content-profile').classList.add('in');
    }
</script>
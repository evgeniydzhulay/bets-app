<?php

namespace common\modules\user\search;

use common\modules\user\models\ProfileModel;
use common\modules\user\models\UserModel;
use Yii;
use yii\data\ActiveDataProvider;

/**
 * UserSearch represents the model behind the search form about User.
 */
class UserSearch extends UserModel {

    /** @var string */
    public $name;
    
    /** @var string */
    public $surname;

    /** @inheritdoc */
    public function rules() {
        return [
            'fieldsSafe' => [['email', 'created_at', 'name', 'confirmed_at', 'blocked_at'], 'safe'],
            'createdDefault' => ['created_at', 'default', 'value' => null],
        ];
    }

    /** @inheritdoc */
    public function attributeLabels() {
        return [
            'email' => Yii::t('user', 'Email'),
            'created_at' => Yii::t('user', 'Registration time'),
            'registration_ip' => Yii::t('user', 'Registration ip'),
        ];
    }

    /**
     * @param $params
     *
     * @return ActiveDataProvider
     */
    public function search($params) {
        $query = self::find()->addSelect([
        	UserModel::tableName() . '.*',
	        ProfileModel::tableName() . '.name',
	        ProfileModel::tableName() . '.surname'
        ])->joinWith('profile');
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        $dataProvider->sort->attributes['name'] = [
            'asc' => [self::tableName() . '.id' => SORT_ASC],
            'desc' => [self::tableName() . '.id' => SORT_DESC],
        ];
        if (($this->load($params) && $this->validate())) {
            if ($this->created_at !== null) {
                $date = strtotime($this->created_at);
                $query->andFilterWhere(['between', 'created_at', $date, $date + 3600 * 24]);
            }
            if(!is_null($this->blocked_at) && strlen($this->blocked_at)) {
                if($this->blocked_at == '0') {
                    $query->andWhere(['blocked_at' => null]);
                } else if($this->blocked_at == '1') {
                    $query->andWhere(['NOT', ['blocked_at' => null]]);
                }
            }
            $query->andFilterWhere(['like', 'email', $this->email]);
            if (!is_null($this->name) && $this->name != '') {
                $clientName[] = explode(' ', $this->name);
                foreach ($clientName AS $item) {
                    $query->andFilterWhere(['or',
                        ['like', ProfileModel::tableName() . '.name', $item],
                        ['like', ProfileModel::tableName() . '.surname', $item]
                    ]);
                }
            }
        }
        return $dataProvider;
    }

    public function formName() {
        return '';
    }

}

<?php

namespace common\modules\user\search;

use common\modules\user\models\UserModel;
use common\modules\user\models\UserPurseWithdrawModel;
use yii\data\ActiveDataProvider;

class UserPurseWithdrawalSearch extends UserPurseWithdrawModel {

	public $purse_currency;

	public function search($params) {
		$query = static::find()->select([
			self::tableName() . '.*',
			UserModel::tableName() . '.purse_currency'
		])->joinWith(['user'], false);
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
			],
		]);
		return $dataProvider;
	}

	public function formName () {
		return '';
	}
}
<?php

namespace common\modules\user\search;

use common\modules\user\models\UserPurseHistoryModel;
use common\traits\DateToTimeTrait;
use yii\data\ActiveDataProvider;

class PurseHistorySearch extends UserPurseHistoryModel {

	use DateToTimeTrait;

	public $amount_from;
	public $amount_to;

	public function rules() {
		return [
			[['amount_to', 'amount_from', 'type', 'user_id', 'source', 'status', 'created_at'], 'safe'],
		];
	}

	public function search($params) {
		$query = self::find();
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
			],
			'sort' => [
				'defaultOrder' => [
					'created_at' => SORT_DESC
				]
			]
		]);
		if ($this->load($params) && $this->validate()) {
			if(!empty($this->created_at)) {
				$time = self::getTimeFromDate($this->created_at, 'Y-m-d');
				$query->andWhere([
					'>=', 'created_at', $time
				])->andWhere([
					'<=', 'created_at', $time + 86399
				]);
			}
			$query->andFilterWhere(['>=', 'amount', (!empty($this->amount_from) ? $this->amount_from : null)]);
			$query->andFilterWhere(['<=', 'amount', (!empty($this->amount_to) ? $this->amount_to : null)]);
			$query->andFilterWhere(['LIKE', 'source', trim($this->source)]);
			$query->andFilterWhere([
				'type' => $this->type,
				'user_id' => $this->user_id,
				'status' => $this->status,
			]);
		}
		return $dataProvider;
	}

	public function formName () {
		return '';
	}

}
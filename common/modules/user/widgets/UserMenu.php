<?php

namespace common\modules\user\widgets;

use common\modules\user\models\ProfileModel;
use Yii;
use yii\bootstrap\Nav;
use yii\web\User;

/**
 * @property mixed $user
 */
class UserMenu extends Nav {

    public $activateItems = false;
    public $activateParents = false;

    public $options = [
        'id' => 'nav-user-website',
        'class' => 'navbar-nav'
    ];
    
    /**
     * Render widget
     * @return string
     */
    public function run() {
	    $this->items = [
		    [
			    'label' => Yii::t('user', 'My account'),
			    'visible' => !$this->getUser()->isGuest,
			    'items' => [
				    [
					    'label' => Yii::t('user', 'Profile'),
						'url' => ['/user/auth/account'],
						'linkOptions' => ['class' => 'link-profile'],
				    ],
				    [
					    'label' => Yii::t('games', 'Bets history'),
					    'url' => ['/games/bets/index'],
						'linkOptions' => ['class' => 'link-bets'],
				    ],
				    [
					    'label' => Yii::t('user', 'Transaction history'),
					    'url' => ['/user/purse/index'],
						'linkOptions' => ['class' => 'link-transactions'],
				    ],
				    [
					    'label' => Yii::t('user', 'Withdraw funds'),
					    'url' => ['/user/purse/withdraw'],
						'linkOptions' => ['class' => 'link-withdraws'],
				    ],
				    [
					    'label' => Yii::t('user', 'Logout'),
					    'url' => ['/user/auth/logout'],
						'linkOptions' => ['class' => 'link-logout'],
				    ]
			    ]
		    ],
	    ];
        return parent::run();
    }

    protected function getUser() : User {
        return Yii::$app->get('user');
    }

}

<?php

namespace common\modules\user\traits;

use common\modules\user\components\UserMailer;
use Yii;
use common\modules\user\Module;
use yii\base\InvalidConfigException;

trait ModuleTrait {

	/** @return Module */
	public function getModule() {
		return Yii::$app->getModule('user');
	}

	/**
	 * @return UserMailer
	 * @throws InvalidConfigException
	 */
	protected function getMailer() {
		return Yii::$container->get(UserMailer::class);
	}
}
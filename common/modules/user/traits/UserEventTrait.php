<?php

namespace common\modules\user\traits;

use common\modules\user\events\FormEvent;
use common\modules\user\events\ProfileEvent;
use common\modules\user\events\ResetPasswordEvent;
use common\modules\user\events\UserEvent;
use common\modules\user\forms\RecoveryForm;
use common\modules\user\models\ProfileModel;
use common\modules\user\models\TokenModel;
use common\modules\user\models\UserModel;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;


trait UserEventTrait {

    /**
     * @param  Model     $form
     * @return FormEvent
     * @throws InvalidConfigException
     */
    protected function getFormEvent(Model $form) {
        return Yii::createObject(['class' => FormEvent::class, 'form' => $form]);
    }

    /**
     * @param  UserModel      $user
     * @return UserEvent
     * @throws InvalidConfigException
     */
    protected function getUserEvent(UserModel $user) {
        return Yii::createObject(['class' => UserEvent::class, 'user' => $user]);
    }

    /**
     * @param  ProfileModel $profile
     * @return ProfileEvent
     * @throws InvalidConfigException
     */
    protected function getProfileEvent(ProfileModel $profile) {
        return Yii::createObject(['class' => ProfileEvent::class, 'profile' => $profile]);
    }

    /**
     * @param  TokenModel        $token
     * @param  RecoveryForm $form
     * @return ResetPasswordEvent
     * @throws InvalidConfigException
     */
    protected function getResetPasswordEvent(TokenModel $token = null, RecoveryForm $form = null) {
        return Yii::createObject(['class' => ResetPasswordEvent::class, 'token' => $token, 'form' => $form]);
    }

}

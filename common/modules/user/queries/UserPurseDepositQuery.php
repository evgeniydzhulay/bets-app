<?php

namespace common\modules\user\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\user\models\UserPurseDepositModel]].
 *
 * @see \common\modules\user\models\UserPurseDepositModel
 */
class UserPurseDepositQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\user\models\UserPurseDepositModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\user\models\UserPurseDepositModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

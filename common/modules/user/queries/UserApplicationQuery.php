<?php

namespace common\modules\user\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\user\models\UserApplicationModel]].
 *
 * @see \common\modules\user\models\UserApplicationModel
 */
class UserApplicationQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\user\models\UserApplicationModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\user\models\UserApplicationModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

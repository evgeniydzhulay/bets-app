<?php

namespace common\modules\user\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\user\models\UserPurseWithdrawModel]].
 *
 * @see \common\modules\user\models\UserPurseWithdrawModel
 */
class UserPurseWithdrawQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\user\models\UserPurseWithdrawModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\user\models\UserPurseWithdrawModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

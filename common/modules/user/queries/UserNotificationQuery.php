<?php

namespace common\modules\user\queries;

use common\modules\user\models\UserNotificationModel;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[\common\modules\user\models\UserNotificationModel]].
 *
 * @see \common\modules\user\models\UserNotificationModel
 */
class UserNotificationQuery extends ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return UserNotificationModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return UserNotificationModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

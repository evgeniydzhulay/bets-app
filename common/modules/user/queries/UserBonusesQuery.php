<?php

namespace common\modules\user\queries;

use common\modules\user\models\UserBonusesModel;

/**
 * This is the ActiveQuery class for [[\common\modules\user\models\UserBonusesModel]].
 *
 * @see \common\modules\user\models\UserBonusesModel
 */
class UserBonusesQuery extends \yii\db\ActiveQuery {

    /**
     * {@inheritdoc}
     * @return UserBonusesModel[]|array
     */
    public function all($db = null) {
        return parent::all($db);
    }

    /**
     * {@inheritdoc}
     * @return UserBonusesModel|array|null
     */
    public function one($db = null) {
        return parent::one($db);
    }
}

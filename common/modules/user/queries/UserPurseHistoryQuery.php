<?php

namespace common\modules\user\queries;

use common\modules\user\models\UserPurseHistoryModel;
use yii\db\ActiveQuery;

/**
 * This is the ActiveQuery class for [[UserPurseHistoryModel]].
 *
 * @see \common\modules\user\models\UserPurseHistoryModel
 */
class UserPurseHistoryQuery extends ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return UserPurseHistoryModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return UserPurseHistoryModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

<?php

namespace common\modules\user\migrations;

use yii\db\Migration;

class m190316_083719_add_twofa extends Migration {

	public function safeUp () {
		$this->addColumn('{{%user}}', 'auth_tf_key', $this->string(16));
		$this->addColumn('{{%user}}', 'auth_tf_enabled', $this->boolean()->defaultValue(0));
	}

	public function safeDown () {
		$this->dropColumn('{{%user}}', 'auth_tf_key');
		$this->dropColumn('{{%user}}', 'auth_tf_enabled');
	}
}
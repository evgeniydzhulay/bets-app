<?php

namespace common\modules\user\migrations;

use Yii;
use yii\db\Migration;

class m190315_083719_user_purse extends Migration {

	public function safeUp() {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->addColumn('{{%user}}', 'purse_amount', $this->float(2)->notNull());
		$this->addColumn('{{%user}}', 'purse_currency', $this->string(255)->defaultValue('USD'));
		$this->createIndex('idx-user-purse_amount', '{{%user}}', 'purse_amount');
		$this->createIndex('idx-user-purse_currency', '{{%user}}', 'purse_currency');

		$this->createTable('{{%user_purse_history}}', [
			'id' => $this->primaryKey()->unsigned(),
			'user_id' => $this->integer()->unsigned(),
			'amount' => $this->float(2)->notNull(),
			'type' => $this->integer(2),
			'source' => $this->integer(3),
			'status' => $this->integer(2),
			'created_at' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createIndex('idx-user_purse_history-user', '{{%user_purse_history}}', 'user_id');
		$this->createIndex('idx-user_purse_history-amount', '{{%user_purse_history}}', 'amount');
		$this->createIndex('idx-user_purse_history-type', '{{%user_purse_history}}', 'type');
		$this->createIndex('idx-user_purse_history-status', '{{%user_purse_history}}', 'status');
		$this->addForeignKey('fk-user_purse_history-user', '{{%user_purse_history}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%user_purse_withdraw}}', [
			'id' => $this->primaryKey()->unsigned(),
			'user_id' => $this->integer()->unsigned(),
			'history_id' => $this->integer()->unsigned(),
			'amount' => $this->float(2)->notNull(),
			'service' => $this->integer(3),
			'target' => $this->string(),
			'target_additional' => $this->string(),
			'comment' => $this->text(),
			'status' => $this->integer(2),
			'created_at' => $this->integer()->notNull(),
			'sent_at' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createIndex('idx-user_purse_withdraw-user', '{{%user_purse_withdraw}}', 'user_id');
		$this->createIndex('idx-user_purse_withdraw-history', '{{%user_purse_withdraw}}', 'history_id');
		$this->createIndex('idx-user_purse_withdraw-amount', '{{%user_purse_withdraw}}', 'amount');
		$this->createIndex('idx-user_purse_withdraw-type', '{{%user_purse_withdraw}}', 'service');
		$this->createIndex('idx-user_purse_withdraw-status', '{{%user_purse_withdraw}}', 'status');
		$this->createIndex('idx-user_purse_withdraw-target', '{{%user_purse_withdraw}}', 'target');
		$this->createIndex('idx-user_purse_withdraw-created_at', '{{%user_purse_withdraw}}', 'created_at');
		$this->createIndex('idx-user_purse_withdraw-sent_at', '{{%user_purse_withdraw}}', 'sent_at');

		$this->addForeignKey('fk-user_purse_withdraw-history', '{{%user_purse_withdraw}}', 'history_id', '{{%user_purse_history}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-user_purse_withdraw-user', '{{%user_purse_withdraw}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%user_purse_deposit}}', [
			'id' => $this->primaryKey()->unsigned(),
			'user_id' => $this->integer()->unsigned(),
			'history_id' => $this->integer()->unsigned(),
			'amount' => $this->float(2)->notNull(),
			'service' => $this->integer(3)->notNull(),
			'external_key' => $this->string(),
			'status' => $this->integer(2),
			'created_at' => $this->integer()->notNull(),
			'received_at' => $this->integer()->notNull(),
		], $tableOptions);

		$this->createIndex('idx-user_purse_deposit-user', '{{%user_purse_deposit}}', 'user_id');
		$this->createIndex('idx-user_purse_deposit-history', '{{%user_purse_deposit}}', 'history_id');
		$this->createIndex('idx-user_purse_deposit-amount', '{{%user_purse_deposit}}', 'amount');
		$this->createIndex('idx-user_purse_deposit-service', '{{%user_purse_deposit}}', 'service');
		$this->createIndex('idx-user_purse_deposit-status', '{{%user_purse_deposit}}', 'status');
		$this->createIndex('idx-user_purse_deposit-external_key', '{{%user_purse_deposit}}', 'external_key');

		$this->addForeignKey('fk-user_purse_deposit-history', '{{%user_purse_deposit}}', 'history_id', '{{%user_purse_history}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-user_purse_deposit-user', '{{%user_purse_deposit}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
	}

	/**
	 * Drop tables.
	 */
	public function down () {
		$this->dropColumn('{{%user}}', 'purse_amount');
		$this->dropColumn('{{%user}}', 'purse_currency');
		$this->dropTable('{{%user_purse_withdraw}}');
		$this->dropTable('{{%user_purse_deposit}}');
		$this->dropTable('{{%user_purse_history}}');
	}
}
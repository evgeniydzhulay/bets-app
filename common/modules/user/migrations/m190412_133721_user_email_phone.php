<?php

namespace common\modules\user\migrations;

use yii\db\Migration;

class m190412_133721_user_email_phone extends Migration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
    public function safeUp() {
		$this->alterColumn('{{%user}}', 'email', $this->string(255)->null());
		$this->addColumn('{{%user}}', 'phone', $this->string(255)->after('email'));
    }

    public function safeDown() {
    	$this->dropColumn('{{%user}}', 'phone');
    }

}

<?php

namespace common\modules\user\migrations;

use yii\db\Migration;

class m190412_165521_withdraw_name_surname extends Migration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
    public function safeUp() {
		$this->addColumn('{{%user_purse_withdraw}}', 'name', $this->string(255)->after('target_additional'));
	    $this->addColumn('{{%user_purse_withdraw}}', 'surname', $this->string(255)->after('name'));

	    $this->createIndex('idx-user_purse_withdraw-name', '{{%user_purse_withdraw}}', 'name');
	    $this->createIndex('idx-user_purse_withdraw-surname', '{{%user_purse_withdraw}}', 'surname');
    }

    public function safeDown() {
    	$this->dropColumn('{{%user_purse_withdraw}}', 'name');
	    $this->dropColumn('{{%user_purse_withdraw}}', 'surname');
    }

}

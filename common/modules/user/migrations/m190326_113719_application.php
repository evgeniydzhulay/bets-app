<?php

namespace common\modules\user\migrations;

use Yii;
use yii\db\Migration;

class m190326_113719_application extends Migration {

	public function safeUp () {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%user_application}}', [
			'id' => $this->primaryKey()->unsigned(),
			'user_id' => $this->integer()->unsigned(),
			'token' => $this->string(64)->notNull(),
			'app_name' => $this->string(),
			'app_type' => $this->integer(3),
			'created_at' => $this->integer(),
			'ended_at' => $this->integer(),
		], $tableOptions);

		$this->createIndex('idx-user_application-created_at', '{{%user_application}}', 'created_at');
		$this->createIndex('idx-user_application-ended_at', '{{%user_application}}', 'ended_at');

		$this->createIndex('idx-user_application-user', '{{%user_application}}', 'user_id');
		$this->createIndex('idx-user_application-token', '{{%user_application}}', 'token');
		$this->addForeignKey('fk-user_application-user', '{{%user_application}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown () {
		$this->dropTable('{{%user_application}}');
	}
}
<?php

namespace common\modules\user\migrations;

use yii\db\Migration;

class m190412_163721_withdraw_enable extends Migration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
    public function safeUp() {
		$this->addColumn('{{%user}}', 'withdraw_enabled', $this->integer(1)->defaultValue(0)->after('purse_currency'));
    }

    public function safeDown() {
    	$this->dropColumn('{{%user}}', 'withdraw_enabled');
    }

}

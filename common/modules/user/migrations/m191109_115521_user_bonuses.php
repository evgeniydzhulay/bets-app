<?php

namespace common\modules\user\migrations;

use yii\db\Migration;

class m191109_115521_user_bonuses extends Migration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
	public function safeUp() {
		$tableOptions = null;
		if (\Yii::$app->db->driverName === 'mysql') {
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%user_bonuses}}', [
			'id' => $this->primaryKey()->unsigned(),
			'user_id' => $this->integer()->unsigned(),
			'bonus' => $this->integer(),
			'additional' => $this->text(),
			'created_at' => $this->integer(),
		], $tableOptions);
		$this->createIndex('idx-user_bonuses-created_at', '{{%user_bonuses}}', 'created_at');
		$this->createIndex('idx-user_bonuses-user', '{{%user_bonuses}}', 'user_id');
		$this->createIndex('idx-user_bonuses-bonus', '{{%user_bonuses}}', 'bonus');
		$this->addForeignKey('fk-user_bonuses-user', '{{%user_bonuses}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
	}

	public function safeDown() {
		$this->dropTable('{{%user_bonuses}}');
	}


}

<?php

namespace common\modules\user\migrations;

use common\modules\user\rbac\RbacMigration;

class m160629_083721_user_rbac_init extends RbacMigration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
    public function safeUp() {
        $usersView = $this->createPermission('users-view', 'Views users data');
        $usersEdit = $this->createPermission('users-edit', 'Edit users data');
        $usersAssignments = $this->createPermission('users-assignments', 'Edit users assignments');
        $rbacRoles = $this->createPermission('rbac-roles', 'Edit RBAC roles');
        $rbacPermissions = $this->createPermission('rbac-permissions', 'Edit RBAC permissions');

        $this->assignChild($usersEdit, $usersView);
        $this->assignChild($usersAssignments, $usersView);
        
        $admin = $this->createRole('admin', 'Administrator role');
        $this->assignChild($admin, $usersView);
        $this->assignChild($admin, $usersEdit);
        $this->assignChild($admin, $usersAssignments);
        $this->assignChild($admin, $rbacRoles);
        $this->assignChild($admin, $rbacPermissions);
    }

    public function safeDown() {

    }

}

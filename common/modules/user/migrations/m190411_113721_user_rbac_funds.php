<?php

namespace common\modules\user\migrations;

use common\modules\user\rbac\RbacMigration;

class m190411_113721_user_rbac_funds extends RbacMigration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
    public function safeUp() {
	    $admin = $this->authManager->getRole('admin');

        $funds = $this->createPermission('users-funds', 'View user funds');
        $this->assignChild($admin, $funds);

	    $withdraw = $this->createPermission('users-withdraw', 'Accept withdrawal');
	    $this->assignChild($admin, $withdraw);
    }

    public function safeDown() {

    }

}

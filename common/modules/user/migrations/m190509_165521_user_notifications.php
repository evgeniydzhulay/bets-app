<?php

namespace common\modules\user\migrations;

use yii\db\Migration;

class m190509_165521_user_notifications extends Migration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
    public function safeUp() {
	    $tableOptions = null;
	    if (\Yii::$app->db->driverName === 'mysql') {
		    $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
	    }
	    $this->createTable('{{%user_notification}}', [
		    'id' => $this->primaryKey()->unsigned(),
		    'user_id' => $this->integer()->unsigned(),
		    'message' => $this->text(),
		    'target' => $this->text(),
		    'created_at' => $this->integer(),
	    ], $tableOptions);
	    $this->createIndex('idx-user_notification-created_at', '{{%user_notification}}', 'created_at');
	    $this->createIndex('idx-user_notification-user', '{{%user_notification}}', 'user_id');
	    $this->addForeignKey('fk-user_notification-user', '{{%user_notification}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown() {
	    $this->dropTable('{{%user_notification}}');
    }

}

<?php

namespace common\modules\user\migrations;

use Yii;
use yii\db\Migration;

class m190409_133719_remove_username extends Migration {

	public function safeUp () {
		$this->dropColumn('{{%user}}', 'username');
	}

	public function safeDown () {
		$this->addColumn('{{%user}}', 'username', $this->string(255));
	}
}
<?php

namespace common\modules\user\migrations;

use yii\db\Migration;

class m191107_165521_user_purse_history_description extends Migration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
    public function safeUp() {
    	$this->addColumn('{{%user_purse_history}}', 'description', $this->text());
    }

    public function safeDown() {
	    $this->dropColumn('{{%user_purse_history}}', 'description');
    }

}

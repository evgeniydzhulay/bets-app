<?php

namespace common\modules\user\migrations;

use Yii;
use yii\db\Migration;

class m190403_113719_extend_profile extends Migration {

	public function safeUp () {
		$this->renameColumn('{{%user_profile}}', 'lastname', 'patrynomic');
		$this->addColumn('{{%user_profile}}', 'country', $this->string());
		$this->addColumn('{{%user_profile}}', 'city', $this->string());
		$this->addColumn('{{%user_profile}}', 'address', $this->text());
		$this->addColumn('{{%user_profile}}', 'dob', $this->date());
		$this->addColumn('{{%user_profile}}', 'passport_series', $this->text());
		$this->addColumn('{{%user_profile}}', 'passport_issuer', $this->text());
		$this->addColumn('{{%user_profile}}', 'passport_date', $this->date());
	}

	public function safeDown () {
		$this->renameColumn('{{%user_profile}}', 'patrynomic', 'lastname');
		$this->dropColumn('{{%user_profile}}', 'country');
		$this->dropColumn('{{%user_profile}}', 'city');
		$this->dropColumn('{{%user_profile}}', 'address');
		$this->dropColumn('{{%user_profile}}', 'dob');
		$this->dropColumn('{{%user_profile}}', 'passport_series');
		$this->dropColumn('{{%user_profile}}', 'passport_issuer');
		$this->dropColumn('{{%user_profile}}', 'passport_date');
	}
}
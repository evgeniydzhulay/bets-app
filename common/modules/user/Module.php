<?php

namespace common\modules\user;

use common\modules\user\components\PaymentComponent;
use yii\base\Module as BaseModule;

/**
 * Class Module
 * @property-read PaymentComponent $payment;
 * @package common\modules\user
 */
class Module extends BaseModule {

	/**
	 * @var int cycles of key generation are set on 30 sec. To avoid sync issues, increased validity up to 60 sec.
	 * @see http://2fa-library.readthedocs.io/en/latest/
	 */
	public $twoFactorAuthenticationCycles = 1;

}
<?php

namespace common\modules\user\controllers;

use common\modules\user\forms\LoginForm;
use common\modules\user\forms\PasswordForm;
use common\modules\user\forms\RecoveryForm;
use common\modules\user\forms\RegistrationForm;
use common\modules\user\forms\ResendForm;
use common\modules\user\forms\SettingsForm;
use common\modules\user\models\TokenModel;
use common\modules\user\models\UserModel;
use common\modules\user\traits\UserEventTrait;
use common\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * RegistrationController is responsible for all registration process, which includes registration of a new account,
 * resending confirmation tokens, email confirmation and registration via social networks.
 */
class AuthController extends Controller {

    use AjaxValidationTrait;
    use UserEventTrait;

    /**
     * Event is triggered before confirming user.
     * Triggered with \common\modules\user\events\FormEvent.
     */
    const EVENT_BEFORE_APPROVE = 'beforeApprove';

    /**
     * Event is triggered before confirming user.
     * Triggered with \common\modules\user\events\FormEvent.
     */
    const EVENT_AFTER_APPROVE = 'afterApprove';

    /**
     * Event is triggered after creating UserResendForm class.
     * Triggered with \common\modules\user\events\FormEvent.
     */
    const EVENT_BEFORE_RESEND = 'beforeResend';

    /**
     * Event is triggered after successful resending of confirmation email.
     * Triggered with \common\modules\user\events\FormEvent.
     */
    const EVENT_AFTER_RESEND = 'afterResend';

    /**
     * Event is triggered before updating user's account settings.
     * Triggered with \common\modules\user\events\FormEvent.
     */
    const EVENT_BEFORE_ACCOUNT_UPDATE = 'beforeAccountUpdate';

    /**
     * Event is triggered after updating user's account settings.
     * Triggered with \common\modules\user\events\FormEvent.
     */
    const EVENT_AFTER_ACCOUNT_UPDATE = 'afterAccountUpdate';

    /**
     * Event is triggered before changing users' email address.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_BEFORE_CONFIRM = 'beforeConfirm';

    /**
     * Event is triggered after changing users' email address.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_AFTER_CONFIRM = 'afterConfirm';

    /**
     * Event is triggered before logging user out.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_BEFORE_LOGIN = 'beforeLogin';

    /**
     * Event is triggered after logging user out.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_AFTER_LOGIN = 'afterLogin';

    /**
     * Event is triggered before logging user out.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_BEFORE_LOGOUT = 'beforeLogout';

    /**
     * Event is triggered after logging user out.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_AFTER_LOGOUT = 'afterLogout';

    /**
     * Event is triggered after creating RegistrationForm class.
     * Triggered with \common\modules\user\events\FormEvent.
     */
    const EVENT_BEFORE_REGISTER = 'beforeRegister';

    /**
     * Event is triggered after successful registration.
     * Triggered with \common\modules\user\events\FormEvent.
     */
    const EVENT_AFTER_REGISTER = 'afterRegister';

    /**
     * Event is triggered before requesting password reset.
     * Triggered with \common\modules\user\events\FormEvent.
     */
    const EVENT_BEFORE_REQUEST = 'beforeRequest';

    /**
     * Event is triggered after requesting password reset.
     * Triggered with \common\modules\user\events\FormEvent.
     */
    const EVENT_AFTER_REQUEST = 'afterRequest';

    /**
     * Event is triggered before validating recovery token.
     * Triggered with \common\modules\user\events\ResetPasswordEvent. May not have $form property set.
     */
    const EVENT_BEFORE_TOKEN_VALIDATE = 'beforeTokenValidate';

    /**
     * Event is triggered after validating recovery token.
     * Triggered with \common\modules\user\events\ResetPasswordEvent. May not have $form property set.
     */
    const EVENT_AFTER_TOKEN_VALIDATE = 'afterTokenValidate';

    /**
     * Event is triggered before resetting password.
     * Triggered with \common\modules\user\events\ResetPasswordEvent.
     */
    const EVENT_BEFORE_RESET = 'beforeReset';

    /**
     * Event is triggered after resetting password.
     * Triggered with \common\modules\user\events\ResetPasswordEvent.
     */
    const EVENT_AFTER_RESET = 'afterReset';

    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'only' => ['account', 'password', 'secondary'],
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['account', 'password'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['secondary'],
                        'roles' => ['admin'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays the registration page.
     * After successful registration if enableConfirmation is enabled shows info message otherwise redirects to home page.
     *
     * @return string
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRegister() {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }
        /* @var $model RegistrationForm */
        $model = Yii::createObject(RegistrationForm::class);
        $event = $this->getFormEvent($model);
        $this->trigger(self::EVENT_BEFORE_REGISTER, $event);
        $this->performAjaxValidation($model);
        if ($model->load(Yii::$app->request->post()) && $model->register()) {
            $this->trigger(self::EVENT_AFTER_REGISTER, $event);
	        Yii::$app->session->addFlash('info', Yii::t('user', 'Your account has been created and a message with further instructions has been sent to your email'));
            return $this->redirect(Url::toRoute(['/']));
        }
        return $this->render('register', [
            'model' => $model
        ]);
    }

    /**
     * Displays the login page.
     *
     * @return string|Response
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogin() {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }
        /** @var LoginForm $model */
        $model = Yii::createObject(LoginForm::class);
        $event = $this->getFormEvent($model);

        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_LOGIN, $event);
        if ($model->load(Yii::$app->getRequest()->post()) && $model->login()) {
            $this->trigger(self::EVENT_AFTER_LOGIN, $event);
            return $this->goBack();
        }
        return $this->render('login', [
                    'model' => $model,
        ]);
    }

	/**
	 * @param int $id
	 * @return Response
	 * @throws NotFoundHttpException
	 */
    public function actionSecondary(int $id) {
        $current = Yii::$app->session->get('__id');
	    $user = $this->getUserModel($id);
        if($user) {
            Yii::$app->session->set('__master', $current);
            Yii::$app->session->set('__id', $id);
        }
        return $this->goHome();
    }

    /**
     * Logs the user out and then redirects to the homepage.
     *
     * @return Response
     * @throws \yii\base\InvalidConfigException
     */
    public function actionLogout() {
        $masterID = Yii::$app->session->get('__master');
        $event = $this->getUserEvent(Yii::$app->user->identity);
        $this->trigger(self::EVENT_BEFORE_LOGOUT, $event);
        Yii::$app->getUser()->logout();
        $this->trigger(self::EVENT_AFTER_LOGOUT, $event);
        if($masterID) {
            Yii::$app->session->set('__id', $masterID);
        }
        return $this->goHome();
    }

    /**
     * Attempts changing user's email address.
     *
     * @param int $id
     * @param string $code
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionConfirm($id, $code) {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }
        $user = $this->getUserModel($id);
        $event = $this->getUserEvent($user);
        $this->trigger(self::EVENT_BEFORE_CONFIRM, $event);
        $user->attemptEmailChange($code);
        $this->trigger(self::EVENT_AFTER_CONFIRM, $event);
        return $this->redirect(['account']);
    }

    /**
     * Confirms user's account. If confirmation was successful logs the user and shows success message. Otherwise
     * shows error message.
     *
     * @param int $id
     * @param string $code
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws \Throwable
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionApprove($id, $code) {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }
	    $user = $this->getUserModel($id);
        if ($user === null) {
            throw new NotFoundHttpException();
        }
        $event = $this->getUserEvent($user);
        $this->trigger(self::EVENT_BEFORE_APPROVE, $event);
        $user->attemptConfirmation($code);
        $this->trigger(self::EVENT_AFTER_APPROVE, $event);
        return $this->redirect(['account']);
    }

    /**
     * Displays page where user can update account settings (email or password).
     *
     * @return string|Response
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionAccount() {
        /** @var SettingsForm $model */
        $model = Yii::createObject(SettingsForm::class);
        $event = $this->getFormEvent($model);
        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_ACCOUNT_UPDATE, $event);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->addFlash('success', Yii::t('user', 'Your account details have been updated'));
            $this->trigger(self::EVENT_AFTER_ACCOUNT_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('account', [
            'model' => $model,
        ]);
    }

	/**
	 * Displays page where user can update account settings (email or password).
	 *
	 * @return string|Response
	 * @throws \yii\base\ExitException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function actionPassword() {
		/** @var PasswordForm $model */
		$model = Yii::createObject(PasswordForm::class);
		$event = $this->getFormEvent($model);
		$this->performAjaxValidation($model);
		$this->trigger(self::EVENT_BEFORE_ACCOUNT_UPDATE, $event);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			Yii::$app->session->addFlash('success', Yii::t('user', 'Your account password have been updated'));
			$this->trigger(self::EVENT_AFTER_ACCOUNT_UPDATE, $event);
			return $this->refresh();
		}

		return $this->render('password', [
			'model' => $model,
		]);
	}

    /**
     * Shows page where user can request password recovery.
     *
     * @return string
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionRequest() {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }
        /** @var RecoveryForm $model */
        $model = Yii::createObject([
            'class' => RecoveryForm::class,
            'scenario' => 'request',
        ]);
        $event = $this->getFormEvent($model);
        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_REQUEST, $event);
        if ($model->load(Yii::$app->request->post()) && $model->sendRecoveryMessage()) {
            $this->trigger(self::EVENT_AFTER_REQUEST, $event);
            Yii::$app->session->addFlash('success', Yii::t('user', 'Recovery message sent'));
        }
        return $this->render('request', [
            'model' => $model,
        ]);
    }

    /**
     * Displays page where user can reset password.
     *
     * @param int $id
     * @param string $code
     *
     * @return string
     * @throws \Throwable
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     * @throws \yii\db\StaleObjectException
     */
    public function actionReset($id, $code) {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }
        /** @var TokenModel $token */
        $token = $this->getTokenModel(['user_id' => $id, 'code' => $code, 'type' => TokenModel::TYPE_RECOVERY]);
        $event = $this->getResetPasswordEvent($token);
        $this->trigger(self::EVENT_BEFORE_TOKEN_VALIDATE, $event);
        if ($token === null || $token->isExpired || $token->user === null) {
            $this->trigger(self::EVENT_AFTER_TOKEN_VALIDATE, $event);
            Yii::$app->session->addFlash('danger', Yii::t('user', 'Recovery link is invalid or expired. Please try requesting a new one.'));
        }
        /** @var RecoveryForm $model */
        $model = Yii::createObject([
            'class' => RecoveryForm::class,
            'scenario' => 'reset',
        ]);
        $event->setForm($model);
        $this->performAjaxValidation($model);
        $this->trigger(self::EVENT_BEFORE_RESET, $event);
        if ($model->load(Yii::$app->getRequest()->post()) && $model->resetPassword($token)) {
            $this->trigger(self::EVENT_AFTER_RESET, $event);
            return $this->redirect(['login']);
        }
        return $this->render('reset', [
            'model' => $model,
        ]);
    }

    /**
     * Displays page where user can request new confirmation token. If resending was successful, displays message.
     *
     * @return string
     * @throws \yii\base\ExitException
     * @throws \yii\base\InvalidConfigException
     */
    public function actionResend() {
        if (!Yii::$app->user->isGuest) {
            $this->goHome();
        }
        /** @var ResendForm $model */
        $model = Yii::createObject(ResendForm::class);
        $event = $this->getFormEvent($model);
        $this->trigger(self::EVENT_BEFORE_RESEND, $event);
        $this->performAjaxValidation($model);
        if ($model->load(Yii::$app->request->post()) && $model->resend()) {
            $this->trigger(self::EVENT_AFTER_RESEND, $event);
            Yii::$app->session->addFlash('success', Yii::t('user', 'A message has been sent to your email address. It contains a confirmation link that you must click to complete registration.'));
        }
        return $this->render('resend', [
            'model' => $model,
        ]);
    }

	/**
	 * @param $params
	 * @return UserModel
	 * @throws NotFoundHttpException
	 */
    protected function getUserModel($params) : UserModel {
	    $user = UserModel::find()->andWhere($params)->one();
	    if(!$user) {
	    	throw new NotFoundHttpException();
	    }
	    return $user;
    }

	/**
	 * @param $params
	 * @return TokenModel
	 * @throws NotFoundHttpException
	 */
	protected function getTokenModel($params) : TokenModel {
		$token = TokenModel::find()->andWhere($params)->one();
		if(!$token) {
			throw new NotFoundHttpException();
		}
		return $token;
	}

}

<?php

namespace common\modules\user\controllers;

use common\modules\user\models\UserModel;
use common\modules\user\services\TwoFactorQrCodeUriGeneratorService;
use common\modules\user\traits\ModuleTrait;
use common\modules\user\validators\TwoFactorCodeValidator;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use Yii;

class TwoFactorController extends Controller {

	use ModuleTrait;

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionIndex() {
		/* @var $user UserModel */
		$user = Yii::$app->user->identity;
		if($user->auth_tf_enabled) {
			if(Yii::$app->request->isAjax) {
				return $this->renderAjax('disable');
			}
			return $this->render('/shared/profile', [
				'content' => $this->renderAjax('disable')
			]);
		} else {
			$uri = (new TwoFactorQrCodeUriGeneratorService($user))->run();
			if(Yii::$app->request->isAjax) {
				return $this->renderAjax('enable', [
					'uri' => $uri
				]);
			}
			return $this->render('/shared/profile', [
				'content' => $this->renderAjax('enable', [
					'uri' => $uri
				])
			]);
		}
	}

	public function actionEnable() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		/* @var $user UserModel */
		$user = Yii::$app->user->identity;
		$code = Yii::$app->request->get('code');
		$success = (new TwoFactorCodeValidator($user, $code, $this->getModule()->twoFactorAuthenticationCycles))->validate();
		$success = $success && $user->updateAttributes(['auth_tf_enabled' => 1]);
		return [
			'success' => $success,
			'message' => $success
				? Yii::t('user', 'Two factor authentication successfully enabled.')
				: Yii::t('user', 'Verification failed. Please, enter new code.')
		];
	}

	public function actionDisable() {
		/* @var $user UserModel */
		$user = Yii::$app->user->identity;
		if ($user->updateAttributes(['auth_tf_enabled' => 0])) {
			Yii::$app->getSession()->setFlash('success', Yii::t('user', 'Two factor authentication has been disabled.'));
		} else {
			Yii::$app->getSession()->setFlash('danger', Yii::t('user', 'Unable to disable Two factor authentication.'));
		}
		$this->redirect(['auth/account']);
	}
}
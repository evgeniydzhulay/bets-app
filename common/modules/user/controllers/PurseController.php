<?php

namespace common\modules\user\controllers;

use common\modules\user\forms\PurseDepositForm;
use common\modules\user\forms\PurseWithdrawForm;
use common\modules\user\Module;
use common\modules\user\search\PurseHistorySearch;
use common\modules\user\traits\ModuleTrait;
use common\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class PurseController
 * @property-read Module $module
 * @package common\modules\user\controllers
 */
class PurseController extends Controller {

	use ModuleTrait;
	use AjaxValidationTrait;

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'only' => ['index', 'deposit', 'withdraw'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
						'actions' => ['index', 'deposit', 'withdraw'],
					],
				],
			],
		];
	}

	/**
	 * @inheritdoc
	 */
	public function beforeAction($action) {
		if ($action->id == 'result') {
			$this->enableCsrfValidation = false;
		}

		return parent::beforeAction($action);
	}

	public function actionIndex() {
		$searchModel = Yii::createObject(PurseHistorySearch::class);
		$dataProvider = $searchModel->search([
			'user_id' => Yii::$app->user->id
		] + Yii::$app->request->get());
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
			'statuses' => PurseHistorySearch::statuses(),
			'types' => PurseHistorySearch::types(),
			'sources' => PurseHistorySearch::sources(),
		]);
	}

	public function actionDeposit() {
		/** @var PurseDepositForm $model */
		$model = Yii::createObject(PurseDepositForm::class);
		$model->user = Yii::$app->user->identity;
		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->refresh();
		}
		return $this->render('deposit', [
			'model' => $model,
			'services' => $model->getServices(),
		]);
	}

	public function actionWithdraw() {
		/** @var PurseWithdrawForm $model */
		$model = Yii::createObject(PurseWithdrawForm::class);
		$model->user = Yii::$app->user->identity;
		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['purse/index']);
		}
		return $this->render('withdraw', [
			'model' => $model,
			'services' => $model->getServices(),
			'enabled' => Yii::$app->user->identity->withdraw_enabled,
		]);
	}

	public function actionResult(string $merchant) {
		$services = $this->module->payment->getDepositServices();
		if(!array_key_exists($merchant, $services)) {
			throw new NotFoundHttpException();
		}
		$services[$merchant]->result();
	}
}
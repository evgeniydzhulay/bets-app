<?php

namespace common\modules\user\helpers;

class PiastrixHelper {

	const CODE_USD = 840;
	const CODE_RUR = 643;
	const CODE_EUR = 978;

	const CURRENCIES = [
		'RUR' => self::CODE_RUR,
		'USD' => self::CODE_USD,
		'EUR' => self::CODE_EUR,
	];

	public static function getParams() {
		return \Yii::$app->params['piastrix'] ?? [];
	}

	public static function createSign($params) {
		$list = ['shop_id' => self::getParams()['shop']] + $params;
		ksort($list, SORT_STRING);
		$hash = hash ('sha256', implode($list, ':') . self::getParams()['key']);
		return $hash;
	}
}
<?php

namespace common\modules\user\models;

use common\modules\user\queries\UserPurseDepositQuery;
use common\modules\user\queries\UserPurseHistoryQuery;
use common\modules\user\queries\UserPurseWithdrawQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%user_purse_history}}".
 *
 * @property int $id
 * @property int $user_id
 * @property float $amount
 * @property int $type
 * @property string $description
 * @property int $source
 * @property int $status
 * @property int $created_at
 *
 * @property-read UserPurseWithdrawModel $withdraw
 * @property-read UserPurseDepositModel $deposit
 * @property-read UserModel $user
 */
class UserPurseHistoryModel extends \yii\db\ActiveRecord {

	const STATUS_NEW = 0;
	const STATUS_PROCESSING = 1;
	const STATUS_SUCCESS = 6;
	const STATUS_FAIL = 7;
	const STATUS_REJECTED = 8;
	const STATUS_REVERTED = 9;

	const TYPE_DEPOSIT = 1;
	const TYPE_WITHDRAW = 2;
	const TYPE_BET = 3;
	const TYPE_WIN = 4;
	const TYPE_COUPON = 5;
	const TYPE_BONUS = 6;
	const TYPE_REFERRAL = 7;

	const SOURCE_WEBMONEY = 1;
	const SOURCE_PAYPAL = 2;
	const SOURCE_CC = 3;
	const SOURCE_PIASTRIX = 4;

	const BONUS_NONE = 0;
	const BONUS_DEPOSIT = UserBonusesModel::TYPE_REGISTRATION_DEPOSIT;
	const BONUS_EXPRESS_RETURN = 10;

	const BONUSES_REGISTRATION = [
		self::BONUS_NONE,
		self::BONUS_DEPOSIT,
	];

	public static function statuses() {
		return [
			self::STATUS_NEW => Yii::t('purse', 'New'),
			self::STATUS_PROCESSING => Yii::t('purse', 'Processing'),
			self::STATUS_SUCCESS => Yii::t('purse', 'Success'),
			self::STATUS_FAIL => Yii::t('purse', 'Fail'),
			self::STATUS_REJECTED => Yii::t('purse', 'Rejected'),
			self::STATUS_REVERTED => Yii::t('purse', 'Reverted'),
		];
	}

	public static function types() {
		return [
			self::TYPE_DEPOSIT => Yii::t('purse', 'Refill'),
			self::TYPE_WITHDRAW => Yii::t('purse', 'Withdraw'),
			self::TYPE_BET => Yii::t('purse', 'Bet'),
			self::TYPE_WIN => Yii::t('purse', 'Victory'),
			self::TYPE_COUPON => Yii::t('purse', 'Coupon'),
			self::TYPE_BONUS => Yii::t('purse', 'Bonus'),
			self::TYPE_REFERRAL => Yii::t('purse', 'Referral'),
		];
	}

	public static function sources() {
		return [
			self::SOURCE_WEBMONEY => Yii::t('purse', 'WebMoney'),
			self::SOURCE_PAYPAL => Yii::t('purse', 'PayPal'),
			self::SOURCE_CC => Yii::t('purse', 'Credit card'),
		];
	}

	public static function bonuses() {
		return [
			self::BONUS_NONE => Yii::t('purse', 'No bonus'),
			self::BONUS_DEPOSIT => Yii::t('purse', '100% Bonus for first deposit up to {amount}', [
				'amount' => Yii::$app->params['user.registrationBonus.max'] . ' RUR'
			]),
			self::BONUS_EXPRESS_RETURN => Yii::t('purse', 'Return for lost express'),
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%user_purse_history}}';
	}

	/**
	 * {@inheritdoc}
	 * @return UserPurseHistoryQuery the active query used by this AR class.
	 */
	public static function find () {
		return new UserPurseHistoryQuery(get_called_class());
	}

	/** {@inheritdoc} */
	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['amount'], 'number'],
			[['user_id', 'type', 'source', 'status'], 'integer'],
			[['amount'], 'required'],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id']],
			['description', 'string', 'max' => 65000]
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('user', 'ID'),
			'user_id' => Yii::t('user', 'User'),
			'amount' => Yii::t('purse', 'Amount'),
			'type' => Yii::t('user', 'Type'),
			'source' => Yii::t('purse', 'Source'),
			'status' => Yii::t('user', 'Status'),
			'created_at' => Yii::t('user', 'Created at'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser () {
		return $this->hasOne(UserModel::class, ['id' => 'user_id']);
	}

	/**
	 * @return UserPurseDepositQuery
	 */
	public function getDeposit () {
		return $this->hasOne(UserPurseDepositModel::class, ['history_id' => 'id']);
	}

	/**
	 * @return UserPurseWithdrawQuery
	 */
	public function getWithdraw () {
		return $this->hasOne(UserPurseWithdrawModel::class, ['history_id' => 'id']);
	}
}

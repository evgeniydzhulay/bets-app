<?php

namespace common\modules\user\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%user_application}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $token
 * @property string $app_name
 * @property int $app_type
 * @property int $created_at
 * @property int $ended_at
 *
 * @property-read UserModel $user
 */
class UserApplicationModel extends \yii\db\ActiveRecord {

	const TYPE_ANDROID = 1;
	const TYPE_IOS = 2;
	const TYPE_WINPHONE = 3;

	const TYPE_WEB = 20;

	const TYPES_MOBILE = [
		self::TYPE_ANDROID,
		self::TYPE_IOS,
		self::TYPE_WINPHONE,
	];

	const TYPES_DESKTOP = [];

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%user_application}}';
	}

	/** {@inheritdoc} */
	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			]
		];
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\user\queries\UserApplicationQuery the active query used by this AR class.
	 */
	public static function find () {
		return new \common\modules\user\queries\UserApplicationQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['user_id', 'app_type', 'ended_at'], 'integer'],
			[['token'], 'string', 'max' => 64],
			[['app_name'], 'string', 'max' => 255],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('user', 'ID'),
			'user_id' => Yii::t('user', 'User'),
			'token' => Yii::t('user', 'Token'),
			'app_name' => Yii::t('user', 'Application name'),
			'app_type' => Yii::t('user', 'Application type'),
			'created_at' => Yii::t('user', 'Created at'),
			'ended_at' => Yii::t('user', 'Ended at'),
		];
	}

	/** @inheritdoc */
	public function beforeSave($insert) {
		if ($insert) {
			$this->setAttribute('token', Yii::$app->security->generateRandomString(64));
			$this->setAttribute('ended_at', time() + (Yii::$app->params['user.apptoken.lifetime'] ?? 604800));
		}
		return parent::beforeSave($insert);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser () {
		return $this->hasOne(UserModel::class, ['id' => 'user_id']);
	}

	public function renewTokenTime() {
		return !empty($this->updateAttributes([
			'ended_at' => time() + (Yii::$app->params['user.apptoken.lifetime'] ?? 604800)
		]));
	}

	public static function getMobileToken($token) {
		return UserApplicationModel::find()->andWhere([
			'token' => $token,
		])->andWhere([
			'or',
			['app_type' => null],
			['app_type' => UserApplicationModel::TYPES_MOBILE],
			['app_type' => UserApplicationModel::TYPES_DESKTOP],
		])->andWhere([
			'>', 'ended_at', time()
		])->limit(1)->one();
	}
}

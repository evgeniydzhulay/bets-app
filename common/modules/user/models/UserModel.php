<?php

namespace common\modules\user\models;

use common\modules\user\components\UserMailer;
use common\modules\user\events\PurseEvent;
use common\modules\user\events\UserEvent;
use common\modules\user\helpers\UserPassword;
use common\modules\user\queries\UserNotificationQuery;
use common\modules\user\rbac\Assignment;
use common\modules\user\traits\ModuleTrait;
use RuntimeException;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\Application as WebApplication;
use yii\web\IdentityInterface;
use yii\web\User;

/**
 * User ActiveRecord model.
 *
 * @property bool $isBlocked
 * @property bool $isConfirmed
 *
 * Database fields:
 * @property int $id
 * @property string $email
 * @property string $phone
 * @property string $password_hash
 * @property string $auth_key
 * @property int $confirmed_at
 * @property string $unconfirmed_email
 * @property int $blocked_at
 * @property string $registration_ip
 * @property int $created_at
 * @property int $updated_at
 * @property int $flags
 * @property string $lang
 * @property int $last_login
 * @property int $login_fail_last
 * @property int $login_fail_count
 * @property int $purse_amount [int(11)]
 * @property string $purse_currency [varchar(255)]
 * @property int $withdraw_enabled [int(1)]
 * @property string $auth_tf_key [varchar(16)]
 * @property bool $auth_tf_enabled [tinyint(1)]
 *
 * Defined relations:
 * @property ProfileModel $profile
 * @property Assignment[] $rbacAssignments
 *
 * Dependencies:
 * @property mixed $accessToken
 * @property string $displayName
 * @property mixed $authKey
 */
class UserModel extends ActiveRecord implements IdentityInterface {

	use ModuleTrait;

    const BEFORE_CREATE = 'beforeCreate';
    const AFTER_CREATE = 'afterCreate';
    const BEFORE_REGISTER = 'beforeRegister';
    const AFTER_REGISTER = 'afterRegister';

    /**
     * Event is triggered before blocking existing user.
     * @see \common\modules\user\events\UserEvent
     */
    const EVENT_BEFORE_BLOCK = 'beforeBlock';

    /**
     * Event is triggered after blocking existing user.
     * @see \common\modules\user\events\UserEvent
     */
    const EVENT_AFTER_BLOCK = 'afterBlock';

    /**
     * Event is triggered before unblocking existing user.
     * @see \common\modules\user\events\UserEvent
     */
    const EVENT_BEFORE_UNBLOCK = 'beforeUnblock';

    /**
     * Event is triggered after unblocking existing user.
     * @see \common\modules\user\events\UserEvent
     */
    const EVENT_AFTER_UNBLOCK = 'afterUnblock';

	/**
	 * Event is triggered after change purse amount.
	 * @see \common\modules\user\events\UserEvent
	 */
    const EVENT_AFTER_PURSE_CHANGE = 'afterPurseChange';
    
    // following constants are used on secured email changing process
    const OLD_EMAIL_CONFIRMED = 0b1;
    const NEW_EMAIL_CONFIRMED = 0b10;

    /** @var string Plain password. Used for model validation. */
    public $password;

    /** @var ProfileModel|null */
    private $_profile;

    /**
     * @return bool Whether the user is confirmed or not.
     */
    public function getIsConfirmed() {
        return $this->confirmed_at != null;
    }

    /**
     * @return bool Whether the user is blocked or not.
     */
    public function getIsBlocked() {
        return $this->blocked_at != null;
    }

    /**
     * @return ActiveQuery
     */
    public function getProfile() {
        return $this->hasOne(ProfileModel::class, ['user_id' => 'id']);
    }

    /**
     * @return UserNotificationQuery
     */
    public function getNotifications() {
    	return $this->hasMany(UserNotificationModel::class, ['user_id' => 'id']);
    }

    /**
     * @param ProfileModel $profile
     */
    public function setProfile(ProfileModel $profile) {
        $this->_profile = $profile;
    }

    /**
     * @return ActiveQuery
     */
    public function getRbacAssignments() {
        return $this->hasMany(Assignment::class, ['user_id' => 'id'])->inverseOf('user');
    }

    /** @inheritdoc */
    public function getId() {
        return $this->getAttribute('id');
    }

    /** @inheritdoc */
    public function getAuthKey() {
        return $this->getAttribute('auth_key');
    }

    public function getDisplayName() {
        return $this->profile->name . ' ' . $this->profile->surname;
    }

    /** @inheritdoc */
    public function attributeLabels() {
        return [
	        'email' => Yii::t('user', 'Email'),
	        'phone' => Yii::t('user', 'Phone'),
            'registration_ip' => Yii::t('user', 'Registration ip'),
            'unconfirmed_email' => Yii::t('user', 'New email'),
            'password' => Yii::t('user', 'Password'),
            'created_at' => Yii::t('user', 'Registration time'),
            'confirmed_at' => Yii::t('user', 'Confirmation time'),
	        'purse_amount' => Yii::t('user', 'Purse amount'),
	        'purse_currency' => Yii::t('user', 'Currency'),
        ];
    }

    /** @inheritdoc */
    public function behaviors() {
        return [
            TimestampBehavior::class
        ];
    }

    const SCENARIO_REGISTER = 'register';
    const SCENARIO_CONNECT = 'connect';
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';
    const SCENARIO_SETTINGS = 'settings';
	const SCENARIO_LANGUAGE = 'language';
	const SCENARIO_TFA = 'twofa';

    /** @inheritdoc */
    public function scenarios() {
        $scenarios = parent::scenarios();
        return ArrayHelper::merge($scenarios, [
            self::SCENARIO_REGISTER => ['email', 'phone', 'password', 'purse_currency'],
            self::SCENARIO_CONNECT => ['email'],
            self::SCENARIO_CREATE => ['email', 'phone', 'password', 'purse_currency'],
            self::SCENARIO_UPDATE => ['email', 'phone', 'password', 'purse_currency'],
	        self::SCENARIO_TFA => ['auth_tf_key', 'auth_tf_enabled'],
            self::SCENARIO_SETTINGS => ['email', 'password'],
            self::SCENARIO_LANGUAGE => ['lang'],
        ]);
    }

    /** @inheritdoc */
    public function rules() {
        return [
            // email rules
            'emailPattern' => ['email', 'email'],
            'emailLength' => ['email', 'string', 'max' => 255],
            'emailUnique' => ['email', 'unique', 'message' => Yii::t('user', 'This email address has already been taken'), 'when' => function() {
	            return !empty($this->email);
            }],
            'emailTrim' => ['email', 'trim', 'when' => function() {
	            return !empty($this->email);
            }],
	        // phone rules
	        'phoneLength' => ['phone', 'string', 'max' => 255],
	        'phoneUnique' => ['phone', 'unique', 'message' => Yii::t('user', 'This phone has already been taken'), 'when' => function() {
		        return !empty($this->phone);
	        }],
	        'phoneTrim' => ['phone', 'trim', 'when' => function() {
		        return !empty($this->phone);
	        }],
            // password rules
            'passwordRequired' => ['password', 'required', 'on' => [
                self::SCENARIO_REGISTER,
            ]],
            'passwordLength' => ['password', 'string', 'min' => 6, 'on' => [
                self::SCENARIO_REGISTER,
                self::SCENARIO_CREATE
            ]],

	        'twoFactorSecretTrim' => ['auth_tf_key', 'trim'],
	        'twoFactorSecretLength' => ['auth_tf_key', 'string', 'max' => 16],
	        'twoFactorEnabledNumber' => ['auth_tf_enabled', 'integer']
        ];
    }

    /** @inheritdoc */
    public function validateAuthKey($authKey) {
        return $this->getAttribute('auth_key') === $authKey;
    }

	/**
	 * Creates new user account. It generates password if it is not provided by user.
	 *
	 * @param bool $sendWelcome
	 * @return bool
	 * @throws InvalidConfigException
	 */
    public function create($sendWelcome = true) {
        if ($this->getIsNewRecord() == false) {
            throw new RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }
        $this->confirmed_at = time();
        $this->password = $this->password == null ? UserPassword::generate(16) : $this->password;
        $this->trigger(self::BEFORE_CREATE);
        if (!$this->save()) {
            return false;
        }
        if ($sendWelcome && !empty($this->email)) {
            $this->getMailer()->sendWelcomeMessage($this, null, true);
        }
        $this->trigger(self::AFTER_CREATE);
        return true;
    }

    /**
     * This method is used to register new user account. This method
     * will generate new confirmation token and use mailer to send it to the user.
     *
     * @return bool
     * @throws InvalidConfigException
     */
    public function register() {
        if ($this->getIsNewRecord() == false) {
            throw new RuntimeException('Calling "' . __CLASS__ . '::' . __METHOD__ . '" on existing user');
        }
        $this->confirmed_at = null;
        $this->trigger(self::BEFORE_REGISTER);
        if (!$this->save()) {
            return false;
        }
        /** @var TokenModel $token */
        if(!empty($this->email)) {
	        $token = Yii::createObject(['class' => TokenModel::class, 'type' => TokenModel::TYPE_APPROVE]);
	        $token->link('user', $this);
	        $this->getMailer()->sendWelcomeMessage($this, isset($token) ? $token : null);
        }
        Yii::$app->user->login($this);
        $this->trigger(self::AFTER_REGISTER);
        return true;
    }

    /**
     * Attempts user confirmation.
     *
     * @param string $code Confirmation code.
     *
     * @return boolean
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function attemptConfirmation($code) {
        $token = $token = TokenModel::find()->andWhere([
	        'user_id' => $this->id,
	        'code' => $code,
	        'type' => TokenModel::TYPE_APPROVE
        ])->one();
        if ($token instanceof TokenModel && !$token->isExpired) {
            $token->delete();
            if (($success = $this->confirm())) {
                Yii::$app->user->login($this);
                $message = Yii::t('user', 'Thank you, registration is now complete.');
            } else {
                $message = Yii::t('user', 'Something went wrong and your account has not been confirmed.');
            }
        } else {
            $success = false;
            $message = Yii::t('user', 'The confirmation link is invalid or expired. Please try requesting a new one.');
        }
        Yii::$app->session->addFlash($success ? 'success' : 'danger', $message);
        return $success;
    }

    /**
     * This method attempts changing user email. If user's "unconfirmed_email" field is empty is returns false, else if
     * somebody already has email that equals user's "unconfirmed_email" it returns false, otherwise returns true and
     * updates user's password.
     *
     * @param string $code
     *
     * @return void
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function attemptEmailChange($code) {
        // TODO refactor method

        /** @var TokenModel $token */
        $token = TokenModel::find()->andWhere([
            'user_id' => $this->id,
            'code' => $code,
	        'type' => [TokenModel::TYPE_CONFIRM_NEW_EMAIL, TokenModel::TYPE_CONFIRM_OLD_EMAIL]
        ])->one();
        if (empty($this->unconfirmed_email) || $token === null || $token->isExpired) {
            Yii::$app->session->addFlash('danger', Yii::t('user', 'Your confirmation token is invalid or expired'));
        } else {
            $token->delete();
            if (empty($this->unconfirmed_email)) {
                Yii::$app->session->addFlash('danger', Yii::t('user', 'An error occurred processing your request'));
            } elseif (self::find()->andWhere(['email' => $this->unconfirmed_email])->exists() == false) {
                $this->flags |= self::OLD_EMAIL_CONFIRMED;
                Yii::$app->session->addFlash('success', Yii::t('user', 'Awesome, almost there. Now you need to click the confirmation link sent to your new email address'));
                $this->save(false);
            }
        }
    }

    /**
     * Confirms the user by setting 'confirmed_at' field to current time.
     */
    public function confirm() {
        return (bool) $this->updateAttributes(['confirmed_at' => time()]);
    }

    /**
     * Resets password.
     *
     * @param string $password
     * @param bool $notifyEmail
     *
     * @return bool
     * @throws \yii\base\Exception
     */
    public function resetPassword($password = null, $notifyEmail = false) {
        if(!$password) {
            $password = UserPassword::generate(16);
        }
        if((bool) $this->updateAttributes(['password_hash' => UserPassword::hash($password)])) {
            if($notifyEmail) {
                $this->getMailer()->sendResetPasswordMessage($this, $password);
            }
            Yii::info("New password: {$password}");
            return true;
        }
        return false;
    }

    /**
     * Blocks the user by setting 'blocked_at' field to current time and regenerates auth_key.
     */
    public function block() {
        $event = $this->getUserEvent($this);
        $this->trigger(self::EVENT_BEFORE_BLOCK, $event);
        if((bool) $this->updateAttributes([
            'blocked_at' => time(),
            'auth_key' => Yii::$app->security->generateRandomString(),
        ])) {
            $this->trigger(self::EVENT_AFTER_BLOCK, $event);
            return true;
        }
        return false;
    }

    /**
     * UnBlocks the user by setting 'blocked_at' field to null.
     */
    public function unblock() {
        $event = $this->getUserEvent($this);
        $this->trigger(self::EVENT_BEFORE_UNBLOCK, $event);
        if((bool) $this->updateAttributes(['blocked_at' => null])) {
            $this->trigger(self::EVENT_AFTER_UNBLOCK, $event);
            return true;
        }
        return false;
    }

    /** @inheritdoc */
    public function beforeSave($insert) {
        if ($insert) {
            $this->setAttribute('auth_key', Yii::$app->security->generateRandomString());
            if (Yii::$app instanceof WebApplication) {
                $this->setAttribute('registration_ip', Yii::$app->request->userIP);
            }
        }
        if (!empty($this->password)) {
            $this->setAttribute('password_hash', UserPassword::hash($this->password));
        }
        if (empty($this->lang)) {
            $this->setAttribute('lang', Yii::$app->language);
        }
	    if (empty($this->purse_amount)) {
		    $this->setAttribute('purse_amount', 0);
	    }
        return parent::beforeSave($insert);
    }

    /** @inheritdoc */
    public function afterSave($insert, $changedAttributes) {
        parent::afterSave($insert, $changedAttributes);
        if ($insert && empty($this->profile)) {
            $profile = Yii::createObject(ProfileModel::class);
            $profile->setAttribute('public_email', $this->email);
            $profile->link('user', $this);
        }
    }

    public function getAccessToken() {
        $key = base64_encode(Yii::$app->security->encryptByKey($this->auth_key, $this->password_hash . $this->last_login));
        return base64_encode("{$this->id}+{$key}");
    }

    public static function updatePurse(int $id, float $amount, UserPurseHistoryModel $record) {
    	if(self::updateAll([
    		'purse_amount' => new Expression("`purse_amount` + {$amount}")
	    ], '`id` = :id AND `purse_amount` >= :amount ', [
	    	'id' => $id,
		    'amount' => ($amount < 0 ? -$amount : 0),
	    ])) {
			$user = self::findOne(['id' => $id]);
			$event = Yii::createObject([
				'class' => PurseEvent::class,
				'user' => $user,
				'record' => $record,
			]);
		    $user->trigger(self::EVENT_AFTER_PURSE_CHANGE, $event);
		    return true;
	    }
	    return false;
    }

    /** @inheritdoc */
    public static function tableName() {
        return '{{%user}}';
    }

    /** @inheritdoc */
    public static function findIdentityByAccessToken($token, $type = null) {
	    $tokenModel = UserApplicationModel::findOne(['token' => $token]);
	    if($tokenModel && ($user = self::findOne(['id' => $tokenModel->user_id]))) {
		    $tokenModel->trigger(User::EVENT_AFTER_LOGIN);
	    	return $user;
	    }
        return false;
    }

    public function createAccessToken($app = null, $type = null) {
	    $key = Yii::createObject(UserApplicationModel::class);
	    $key->setAttributes([
		    'user_id' => $this->id,
		    'app_name' => $app,
		    'app_type' => $type,
	    ]);
	    if($key->save()) {
	    	return $key;
	    }
	    return false;
    }

    /** @inheritdoc */
    public static function findIdentity($id) {
        return static::findOne($id);
    }

    /**
     * @param  UserModel      $user
     * @return UserEvent
     * @throws InvalidConfigException
     */
    protected function getUserEvent(UserModel $user) {
        return Yii::createObject(['class' => UserEvent::class, 'user' => $user]);
    }

	/**
	 * @return false|int|void
	 * @throws NotSupportedException
	 */
    public function delete() {
        throw new NotSupportedException('Method "' . __CLASS__ . '::' . __METHOD__ . '" is not supported. Deletion of users disabled.');
    }

	/**
	 * @param null $condition
	 * @param array $params
	 * @return int|void
	 * @throws NotSupportedException
	 */
    public static function deleteAll($condition = null, $params = array()) {
        throw new NotSupportedException('Method "' . __CLASS__ . '::' . __METHOD__ . '" is not supported. Deletion of users disabled.');
    }

	/**
	 * @return false|int|void
	 * @throws NotSupportedException
	 */
    protected function deleteInternal() {
        throw new NotSupportedException('Method "' . __CLASS__ . '::' . __METHOD__ . '" is not supported. Deletion of users disabled.');
    }

}

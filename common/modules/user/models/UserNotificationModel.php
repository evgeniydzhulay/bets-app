<?php

namespace common\modules\user\models;

use common\modules\user\queries\UserNotificationQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%user_notification}}".
 *
 * @property int $id
 * @property int $user_id
 * @property string $message
 * @property string $target
 * @property int $created_at
 *
 * @property UserModel $user
 */
class UserNotificationModel extends ActiveRecord {

	/** {@inheritdoc} */
	public static function tableName () {
		return '{{%user_notification}}';
	}

	/**
	 * {@inheritdoc}
	 * @return UserNotificationQuery the active query used by this AR class.
	 */
	public static function find () {
		return new UserNotificationQuery(get_called_class());
	}

	/** {@inheritdoc} */
	public function rules () {
		return [
			[['user_id', 'created_at'], 'integer'],
			[['message'], 'string'],
			[['app_name'], 'string', 'max' => 255],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'id' => Yii::t('user', 'ID'),
			'user_id' => Yii::t('user', 'User'),
			'message' => Yii::t('user', 'Message'),
			'target' => Yii::t('user', 'Target'),
			'created_at' => Yii::t('user', 'Created at'),
		];
	}

	/** {@inheritdoc} */
	public function getUser () {
		return $this->hasOne(UserModel::class, ['id' => 'user_id']);
	}
}

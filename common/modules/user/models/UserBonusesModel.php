<?php

namespace common\modules\user\models;

use common\modules\user\queries\UserBonusesQuery;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%user_bonuses}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $bonus
 * @property string $additional
 * @property int $created_at
 *
 * @property UserModel $user
 */
class UserBonusesModel extends \yii\db\ActiveRecord {

	const TYPE_REGISTRATION_PROMOCODE = 1;
	const TYPE_REGISTRATION_DEPOSIT = 2;

	/** {@inheritdoc} */
	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			]
		];
	}

    /**
     * {@inheritdoc}
     */
    public static function tableName() {
        return '{{%user_bonuses}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules() {
        return [
            [['user_id', 'bonus'], 'integer'],
            [['additional'], 'string'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels() {
        return [
            'id' => Yii::t('user', 'ID'),
            'user_id' => Yii::t('user', 'User ID'),
            'bonus' => Yii::t('user', 'Bonus'),
            'additional' => Yii::t('user', 'Additional'),
            'created_at' => Yii::t('user', 'Created at'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser() {
        return $this->hasOne(UserModel::class, ['id' => 'user_id']);
    }

    /**
     * {@inheritdoc}
     * @return UserBonusesQuery the active query used by this AR class.
     */
    public static function find() {
        return new UserBonusesQuery(get_called_class());
    }
}

<?php

namespace common\modules\user\models;

use Yii;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecord;
use yii\validators\DateValidator;

/**
 * This is the model class for table "profile".
 *
 * @property integer $user_id
 * @property string $name
 * @property string $surname
 * @property string $patrynomic [varchar(255)]
 * @property string $public_email
 * @property string $bio
 * @property UserModel $user
 * @property int $id [int(10) unsigned]
 * @property string $avatar [varchar(255)]
 * @property string $gravatar_id [varchar(32)]
 * @property string $country [varchar(255)]
 * @property string $city [varchar(255)]
 * @property string $address
 * @property string $dob [date]
 * @property string $passport_series
 * @property string $passport_issuer
 * @property string $passport_date [date]
 */
class ProfileModel extends ActiveRecord {

    /**
     * @return ActiveQueryInterface
     */
    public function getUser() {
        return $this->hasOne(UserModel::class, ['id' => 'user_id']);
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            'publicEmailPattern' => ['public_email', 'email'],
	        'stringLength' => [['public_email', 'name', 'surname', 'patrynomic'], 'string', 'max' => 255],
	        'textLength' => [['bio', 'address', 'passport_series', 'passport_issuer'], 'string', 'max' => 65000],
	        'profileTrim' => [[
		        'name', 'surname', 'patrynomic', 'address',
		        'passport_series', 'passport_issuer', 'passport_date'
	        ], 'filter', 'filter' => 'trim'],
	        'countryList' => ['country', 'in', 'range' => Yii::$app->get('locations')->getCountryKeys()],
	        'date' => [['dob', 'passport_date'], 'date', 'type' => DateValidator::TYPE_DATE, 'format' => 'php:Y-m-d'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
	        'name' => Yii::t('user', 'Name'),
	        'surname' => Yii::t('user', 'Surname'),
	        'patrynomic' => Yii::t('user', 'Patrynomic'),
	        'dob' => Yii::t('user', 'Date of birth'),
	        'country' => Yii::t('user', 'Country'),
	        'city' => Yii::t('user', 'City'),
	        'address' => Yii::t('user', 'Address'),
	        'passport_series' => Yii::t('user', 'Passport series'),
	        'passport_issuer' => Yii::t('user', 'Passport issuer'),
	        'passport_date' => Yii::t('user', 'Passport issue date'),
            'public_email' => Yii::t('user', 'Email (public)'),
            'bio' => Yii::t('user', 'Bio'),
        ];
    }

    public function getAvatarUrl($size = false, $default = false) {
        return "//www.gravatar.com/avatar/{$this->gravatar_id}?" . http_build_query([
            's' => ($size ?: 200),
            'd' => ($default ?: 'blank'),
            'r' => 'g',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert) {
        if ($this->isAttributeChanged('public_email')) {
            $this->setAttribute('gravatar_id', md5(strtolower(trim($this->getAttribute('public_email')))));
        }
        return parent::beforeSave($insert);
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return '{{%user_profile}}';
    }

}

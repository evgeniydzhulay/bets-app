<?php

namespace common\modules\user\models;

use common\modules\user\queries\UserPurseDepositQuery;
use common\modules\user\queries\UserPurseHistoryQuery;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%user_purse_deposit}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $history_id
 * @property double $amount
 * @property int $service
 * @property string $external_key
 * @property int $status
 * @property int $created_at
 * @property int $received_at
 *
 * @property-read UserPurseHistoryModel $history
 * @property-read UserModel $user
 */
class UserPurseDepositModel extends \yii\db\ActiveRecord {

	const EVENT_DEPOSIT_SUCCESS = 'eventDepositSuccess';
	const EVENT_DEPOSIT_REJECTED = 'eventDepositRejected';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%user_purse_deposit}}';
	}

	/**
	 * {@inheritdoc}
	 * @return UserPurseDepositQuery the active query used by this AR class.
	 */
	public static function find () {
		return new UserPurseDepositQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			[['user_id', 'service', 'status', 'created_at', 'received_at'], 'integer'],
			[['amount'], 'required'],
			['amount', 'number', 'min' => 1],
			[['external_key'], 'string', 'max' => 255],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('user', 'ID'),
			'user_id' => Yii::t('user', 'User'),
			'amount' => Yii::t('purse', 'Amount'),
			'service' => Yii::t('purse', 'Service'),
			'external_key' => Yii::t('purse', 'External key'),
			'status' => Yii::t('user', 'Status'),
			'created_at' => Yii::t('user', 'Created at'),
			'received_at' => Yii::t('purse', 'Received at'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser () {
		return $this->hasOne(UserModel::class, ['id' => 'user_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProfile () {
		return $this->hasOne(ProfileModel::class, ['user_id' => 'user_id']);
	}

	/**
	 * @return UserPurseHistoryQuery
	 */
	public function getHistory () {
		return $this->hasOne(UserPurseHistoryModel::class, [
			'id' => 'history_id',
		]);
	}

	/** {@inheritdoc} */
	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			]
		];
	}

	public function insert ($runValidation = true, $attributes = null) {
		$tr = Yii::$app->db->beginTransaction();
		try {
			$history = Yii::createObject(UserPurseHistoryModel::class);
			$history->setAttributes([
				'user_id' => $this->user_id,
				'amount' => $this->amount,
				'type' => UserPurseHistoryModel::TYPE_DEPOSIT,
				'source' => $this->service,
				'status' => UserPurseHistoryModel::STATUS_NEW,
			]);
			if(!$history->save()) {
				throw new Exception(json_encode($history->errors));
			}
			$this->history_id = $history->id;
			$this->received_at = $this->received_at ?? 1;
			if(parent::insert($runValidation, $attributes)) {
				$tr->commit();
				return true;
			}
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		$tr->rollBack();
		return false;
	}

	public function accept() {
		if($this->updateStatus(UserPurseHistoryModel::STATUS_SUCCESS)) {
			$this->trigger(self::EVENT_DEPOSIT_SUCCESS);
			if(UserModel::updatePurse($this->user_id, $this->amount, $this->history)) {
				$this->processFirstDeposit();
				return true;
			}
		}
		return false;
	}

	protected function processFirstDeposit() {
		if($this->amount < Yii::$app->params['user.registrationBonus.min'] ||
			!UserBonusesModel::find()->andWhere([
				'user_id' => $this->user_id,
				'bonus' => UserBonusesModel::TYPE_REGISTRATION_DEPOSIT
			])->exists() ||
			self::find()->andWhere('`user_id` = :user_id AND `status` = :status AND `id` != :id', [
				'user_id' => $this->user_id,
				'status' => UserPurseHistoryModel::STATUS_SUCCESS,
				'id' => $this->id,
			])->exists()
		) {
			return false;
		}
		$history = Yii::createObject(UserPurseHistoryModel::class);
		$history->setAttributes([
			'user_id' => $this->user_id,
			'amount' => min($this->amount, Yii::$app->params['user.registrationBonus.max']),
			'type' => UserPurseHistoryModel::TYPE_BONUS,
			'status' => UserPurseHistoryModel::STATUS_SUCCESS,
			'description' => Yii::t('purse', 'Bonus for first deposit', [], $this->user->lang),
		]);
		return $history->save(false) && UserModel::updatePurse($history->user_id, $history->amount, $history);
	}

	public function decline($status = UserPurseHistoryModel::STATUS_REJECTED) {
		$this->trigger(self::EVENT_DEPOSIT_REJECTED);
		return $this->updateStatus($status);
	}

	protected function updateStatus($status) {
		if(($history = $this->history) && $history->status == UserPurseHistoryModel::STATUS_NEW) {
			$tr = Yii::$app->db->beginTransaction();
			try {
				$this->status = $status;
				$history->status = $status;
				if($history->save(false, ['status']) && $this->save(false, ['status'])) {
					$tr->commit();
					return true;
				}
			} catch (\Throwable $e) {
				Yii::error($e);
			}
			$tr->rollBack();
		}
		return false;
	}
}

<?php

namespace common\modules\user\models;

use common\modules\user\queries\UserPurseHistoryQuery;
use common\modules\user\queries\UserPurseWithdrawQuery;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%user_purse_withdraw}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $history_id
 * @property float $amount
 * @property int $service
 * @property string $target
 * @property string $target_additional
 * @property string $name [varchar(255)]
 * @property string $surname [varchar(255)]
 * @property string $comment
 * @property int $status
 * @property int $created_at
 * @property int $sent_at
 *
 * @property-read UserPurseHistoryModel $history
 * @property-read UserModel $user
 */
class UserPurseWithdrawModel extends \yii\db\ActiveRecord {

	const EVENT_WITHDRAW_SUCCESS = 'eventDepositSuccess';
	const EVENT_WITHDRAW_REJECTED = 'eventDepositRejected';

	/**
	 * {@inheritdoc}
	 */
	public static function tableName () {
		return '{{%user_purse_withdraw}}';
	}

	/**
	 * {@inheritdoc}
	 * @return UserPurseWithdrawQuery the active query used by this AR class.
	 */
	public static function find () {
		return new UserPurseWithdrawQuery(get_called_class());
	}

	/**
	 * {@inheritdoc}
	 */
	public function rules () {
		return [
			['amount', 'number', 'min' => 1],
			[['user_id', 'service', 'status', 'created_at', 'sent_at'], 'integer'],
			[['amount', 'user_id'], 'required'],
			[['comment'], 'string'],
			[['target', 'target_additional', 'name', 'surname'], 'string', 'max' => 255],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id']],
		];
	}

	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels () {
		return [
			'id' => Yii::t('user', 'ID'),
			'user_id' => Yii::t('user', 'User'),
			'amount' => Yii::t('purse', 'Amount'),
			'service' => Yii::t('purse', 'Service'),
			'target' => Yii::t('purse', 'Target'),
			'target_additional' => Yii::t('purse', 'Additional target'),
			'name' => Yii::t('user', 'Name'),
			'surname' => Yii::t('user', 'Surname'),
			'comment' => Yii::t('user', 'Comment'),
			'status' => Yii::t('user', 'Status'),
			'created_at' => Yii::t('user', 'Created at'),
			'sent_at' => Yii::t('purse', 'Sent at'),
		];
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getUser () {
		return $this->hasOne(UserModel::class, ['id' => 'user_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getProfile () {
		return $this->hasOne(ProfileModel::class, ['user_id' => 'user_id']);
	}

	/**
	 * @return UserPurseHistoryQuery
	 */
	public function getHistory () {
		return $this->hasOne(UserPurseHistoryModel::class, [
			'id' => 'history_id',
		]);
	}

	/** {@inheritdoc} */
	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			]
		];
	}

	/** {@inheritdoc} */
	public function insert ($runValidation = true, $attributes = null) {
		$tr = Yii::$app->db->beginTransaction();
		try {
			$history = Yii::createObject(UserPurseHistoryModel::class);
			$history->setAttributes([
				'user_id' => $this->user_id,
				'amount' => -$this->amount,
				'type' => UserPurseHistoryModel::TYPE_WITHDRAW,
				'source' => $this->service,
				'status' => UserPurseHistoryModel::STATUS_NEW,
			]);
			if(!$history->save()) {
				Yii::error($history->errors);
				throw new Exception();
			}
			$this->history_id = $history->id;
			$this->sent_at = $this->sent_at ?? 1;
			if(parent::insert($runValidation, $attributes)) {
				UserModel::updatePurse($this->user_id, -$this->amount, $history);
				$tr->commit();
				return true;
			}
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		$tr->rollBack();
		return false;
	}

	public function accept() {
		$this->trigger(self::EVENT_WITHDRAW_SUCCESS);
		return $this->updateStatus(UserPurseHistoryModel::STATUS_SUCCESS);
	}

	public function decline($status = UserPurseHistoryModel::STATUS_REJECTED) {
		if($this->updateStatus($status)) {
			$this->trigger(self::EVENT_WITHDRAW_REJECTED);
			UserModel::updatePurse($this->user_id, $this->amount, $this->history);
			return true;
		}
		return false;
	}

	protected function updateStatus($status) {
		if(($history = $this->history) && $history->status == UserPurseHistoryModel::STATUS_NEW) {
			$tr = Yii::$app->db->beginTransaction();
			try {
				$this->status = $status;
				$history->status = $status;
				if($history->save(false, ['status']) && $this->save(false, ['status'])) {
					$tr->commit();
					return true;
				}
			} catch (\Throwable $e) {
				Yii::error($e);
			}
			$tr->rollBack();
		}
		return false;
	}
}

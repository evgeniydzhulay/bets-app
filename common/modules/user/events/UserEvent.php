<?php

namespace common\modules\user\events;

use common\modules\user\models\UserModel;
use yii\base\Event;

/**
 * @property \common\modules\user\models\UserModel $user
 * @property UserModel $model
 */
class UserEvent extends Event {

    /**
     * @var UserModel
     */
    private $_user;

    /**
     * @return UserModel
     */
    public function getUser() {
        return $this->_user;
    }

    /**
     * @param UserModel $form
     */
    public function setUser(UserModel $form) {
        $this->_user = $form;
    }

}

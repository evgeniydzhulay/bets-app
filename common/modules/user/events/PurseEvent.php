<?php

namespace common\modules\user\events;

use common\modules\user\models\UserModel;
use common\modules\user\models\UserPurseHistoryModel;
use yii\base\Event;

/**
 * @property UserModel $user
 * @property UserPurseHistoryModel $record
 */
class PurseEvent extends Event {

    /**
     * @var UserModel
     */
    private $_user;

	/**
	 * @var UserPurseHistoryModel
	 */
	private $_record;

    /**
     * @return UserModel
     */
    public function getUser() {
        return $this->_user;
    }

    /**
     * @param UserModel $form
     */
    public function setUser(UserModel $form) {
        $this->_user = $form;
    }

	/**
	 * @return UserPurseHistoryModel
	 */
	public function getRecord() {
		return $this->_record;
	}

	/**
	 * @param UserPurseHistoryModel $record
	 */
	public function setRecord(UserPurseHistoryModel $record) {
		$this->_record = $record;
	}

}

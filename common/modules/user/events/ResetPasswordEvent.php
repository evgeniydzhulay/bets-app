<?php

namespace common\modules\user\events;

use common\modules\user\forms\RecoveryForm;
use common\modules\user\models\TokenModel;
use yii\base\Event;

/**
 * @property TokenModel        $token
 * @property RecoveryForm $form
 */
class ResetPasswordEvent extends Event {

    /**
     * @var RecoveryForm
     */
    private $_form;

    /**
     * @var TokenModel
     */
    private $_token;

    /**
     * @return TokenModel
     */
    public function getToken() {
        return $this->_token;
    }

    /**
     * @param TokenModel $token
     */
    public function setToken(TokenModel $token = null) {
        $this->_token = $token;
    }

    /**
     * @return RecoveryForm
     */
    public function getForm() {
        return $this->_form;
    }

    /**
     * @param RecoveryForm $form
     */
    public function setForm(RecoveryForm $form = null) {
        $this->_form = $form;
    }

}

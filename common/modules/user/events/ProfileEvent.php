<?php

namespace common\modules\user\events;

use common\modules\user\models\ProfileModel;
use yii\base\Event;

/**
 * @property ProfileModel $profile
 */
class ProfileEvent extends Event {

    /**
     * @var ProfileModel
     */
    private $_profile;

    /**
     * @return ProfileModel
     */
    public function getProfile() {
        return $this->_profile;
    }

    /**
     * @param ProfileModel $form
     */
    public function setProfile(ProfileModel $form) {
        $this->_profile = $form;
    }

}

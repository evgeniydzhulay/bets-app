<?php

namespace common\modules\user\actions;

use common\modules\user\models\UserModel;
use Yii;
use yii\base\Action;
use yii\helpers\Url;

class TimezoneAction extends Action {

	/**
	 * @param string $timezone
	 * @return \yii\web\Response
	 */
	public function run(string $timezone) {
		$session = Yii::$app->get('session', false);
		if($session && in_array($timezone, \DateTimeZone::listIdentifiers(\DateTimeZone::ALL))) {
			$session->set('timezone', $timezone);
			setcookie('tzo', $timezone, time() + 365 * 24 * 60 * 60, '/', '', false, false);
		}
		$redirectUrl = Yii::$app->request->get('redirect', false);
		if (!$redirectUrl) {
			$redirectUrl = Yii::$app->request->referrer;
		}
		if (!$redirectUrl) {
			$redirectUrl = Url::home(true);
		}
		return Yii::$app->response->redirect($redirectUrl);
	}

}
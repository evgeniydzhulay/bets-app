<?php

namespace common\modules\user\actions;

use common\modules\user\models\UserModel;
use Yii;
use yii\base\Action;
use yii\helpers\Url;

class LanguageAction extends Action {

	/**
	 * @param string $language
	 * @return \yii\web\Response
	 */
	public function run(string $language) {
		if (isset(Yii::$app->params['languages'][$language])) {
			if (!Yii::$app->user->isGuest) {
				/* @var $user UserModel */
				$user = Yii::$app->user->identity;
				$user->scenario = 'language';
				$user->lang = $language;
				$user->save();
			}
			Yii::$app->session->set('lang', $language);
		}
		$redirectUrl = Yii::$app->request->get('redirect', false);
		if (!$redirectUrl) {
			$redirectUrl = Yii::$app->request->referrer;
		}
		if (!$redirectUrl) {
			$redirectUrl = Url::home(true);
		}
		return Yii::$app->response->redirect($redirectUrl);
	}

}
<?php

namespace common\modules;

use Yii;
use yii\base\Module as BaseModule;

/**
 * @property string $uploadUrl
 * @property string $uploadDir
 * @property string $internalUrl
 */
class Storage extends BaseModule {

    protected $_filesUploadUrl = '@web/uploads';
    protected $_filesInternalUrl = '@web/uploads/protected';
    protected $_filesUploadDir = '@uploads';
    
    public function setUploadDir($path) {
        $this->_filesUploadDir = $path;
    }

    public function getUploadDir() {
        return $this->_filesUploadDir;
    }

    public function setInternalUrl($path) {
        $this->_filesInternalUrl = $path;
    }

    public function getInternalUrl() {
        return $this->_filesInternalUrl;
    }

    public function setUploadUrl($path) {
        $this->_filesUploadUrl = $path;
    }

    public function getUploadUrl() {
        return $this->_filesUploadUrl;
    }

    /**
     * @param $fileName
     * @param bool $internal
     * @return string
     */
    public function getUrl($fileName, $internal = false) {
        return Yii::getAlias(($internal ? $this->_filesInternalUrl : $this->_filesUploadUrl) . '/' . $fileName);
    }

    /**
     * @param $fileName
     * @return string
     */
    public function getPath($fileName) {
        return Yii::getAlias($this->_filesUploadDir . '/' . $fileName);
    }

}

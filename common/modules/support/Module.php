<?php

namespace common\modules\support;

use common\modules\support\models\SupportRequestModel;
use common\modules\user\helpers\UserPassword;
use common\modules\user\models\UserModel;
use Yii;
use yii\base\Module as BaseModule;
use yii\redis\Connection;

class Module extends BaseModule {

	const TYPE_OPERATOR = 1;
	const TYPE_USER = 2;

	public static function createNodeSession($name, $mail, $phone, int $type, $user = null) {
		/* @var $user UserModel */
		if(!empty($user)) {
			$mail = !empty($mail) ? $mail : $user->email;
			$name = !empty($name) ? $name : ($user->profile->name . ' ' . $user->profile->surname);
		}
		/* @var $redis Connection */
		if($redis = Yii::$app->get('redis', false)) {
			$key = 'supporttoken_' . UserPassword::generate(64);
			$redis->set($key, json_encode([
				'name' => $name,
				'email' => $mail,
				'phone' => $phone,
				'id' => (!empty($user)) ? +$user->id : null,
				'type' => $type
			]), 'EX', 24 * 60 * 60);
			return $key;
		}
		return false;
	}

	public function sendNewRequest(SupportRequestModel $request) {
		$this->get('bus')->send('support', 'request', [
			'id' => +$request->id,
			'key' => $request->key,
			'user_id' => $request->user_id,
			'email' => $request->email,
			'phone' => $request->phone,
			'name' => $request->name,
			'theme' => $request->theme,
			'message' => $request->lastMessage->message,
			'from_id' => $request->user_id,
			'from_email' => $request->email,
			'from_phone' => $request->phone,
			'from_name' => $request->name,
			'created_at' => date('Y-m-d H:i:s', $request->created_at),
			'message_at' => date('Y-m-d H:i:s', $request->created_at),
		]);
	}
}
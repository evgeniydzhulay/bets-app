<?php

namespace common\modules\support\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\support\models\SupportRequestModel]].
 *
 * @see \common\modules\support\models\SupportRequestModel
 */
class SupportRequestQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\support\models\SupportRequestModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\support\models\SupportRequestModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

<?php

namespace common\modules\support\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\support\models\SupportMessageModel]].
 *
 * @see \common\modules\support\models\SupportMessageModel
 */
class SupportMessageQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\support\models\SupportMessageModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\support\models\SupportMessageModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

<?php

namespace common\modules\support\queries;

/**
 * This is the ActiveQuery class for [[\common\modules\support\models\SupportAttachmentModel]].
 *
 * @see \common\modules\support\models\SupportAttachmentModel
 */
class SupportAttachmentQuery extends \yii\db\ActiveQuery {

	/**
	 * {@inheritdoc}
	 * @return \common\modules\support\models\SupportAttachmentModel[]|array
	 */
	public function all ($db = null) {
		return parent::all($db);
	}

	/**
	 * {@inheritdoc}
	 * @return \common\modules\support\models\SupportAttachmentModel|array|null
	 */
	public function one ($db = null) {
		return parent::one($db);
	}
}

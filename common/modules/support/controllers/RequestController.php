<?php

namespace common\modules\support\controllers;

use common\modules\support\models\SupportAttachmentModel;
use common\modules\support\models\SupportMessageModel;
use common\modules\support\Module;
use common\modules\support\search\FrontendSearch;
use common\modules\user\models\UserModel;
use common\modules\support\forms\RequestCreateForm;
use common\modules\support\models\SupportRequestModel;
use common\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\View;

class RequestController extends Controller {

	use AjaxValidationTrait;

	/** @inheritdoc */
	public function behaviors () {
		return [
			'access' => [
				'class' => AccessControl::class,
				'only' => ['index'],
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index'],
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionIndex($is_closed = 0) {
		$searchModel = Yii::createObject(FrontendSearch::class);
		$searchModel->scenario = FrontendSearch::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search([
			'is_closed' => $is_closed
		] + Yii::$app->request->get());
		return $this->render('index', [
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate () {
		$model = Yii::createObject(RequestCreateForm::class);
		if(!Yii::$app->user->isGuest) {
			/* @var $user UserModel */
			$user = Yii::$app->user->identity;
			$model->user_id = $user->id;
			$model->email = $user->email;
			$model->name = $user->profile->name . ' ' . $user->profile->surname;
		}
		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post()) && ($request = $model->save())) {
			return $this->redirect(['chat', 'id' => $request->id, 'key' => $request->key]);
		}
		return $this->render('create', [
			'model' => $model
		]);
	}

	public function actionChat (int $id, $key) {
		$request = $this->getRequest([
			'id' => $id,
			'key' => $key,
		]);
		if(!$request->is_closed) {
			$key = Module::createNodeSession($request->name, $request->email, $request->phone, Module::TYPE_USER, (Yii::$app->user->isGuest ? null : Yii::$app->user->identity));
			$this->view->registerJs("window.paramsSocket = " . json_encode([
				'key' => $key,
				'host' => Url::base(true),
			]), View::POS_HEAD);
		}
		$this->view->registerJs("window.paramsSupport = " . json_encode([
			'path' => Yii::$app->params['socketSupport.path'],
			'request' => $request->id,
			'theme' => $request->theme,
			'key' => $request->key,
			'closed' => $request->is_closed,
			'csrf' => [
				'param' => Yii::$app->getRequest()->csrfParam,
				'token' => Yii::$app->getRequest()->getCsrfToken()
			],
		]), View::POS_HEAD);
		return $this->render('chat');
	}

	public function actionMessages (int $id, $key) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$this->getRequest(['id' => $id, 'key' => $key]);
		$messages = SupportMessageModel::find()->andWhere([
			'request_id' => $id
		])->andFilterWhere([
			'<', 'id', Yii::$app->request->get('last', null)
		])->with('attachments')->orderBy(['id' => SORT_DESC])->limit(20)->asArray()->all();
		$attachmentModel = new SupportAttachmentModel();
		foreach($messages AS &$message) {
			$message['id'] = +$message['id'];
			$message['created_at'] = date('Y-m-d H:i:s', $message['created_at']);
			$attachments = [];
			foreach($message['attachments'] AS $attachment) {
				$attachmentModel->path = $attachment['path'];
				$attachments[] = [
					'name' => $attachment['name'],
					'url' => $attachmentModel->getUrl(),
				];
			}
			$message['files'] = $attachments;
			unset($message['attachments']);
		}
		return $messages;
	}

	public function actionUpload(int $id, $key) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$this->getRequest(['id' => $id, 'key' => $key]);
		$file = Yii::createObject(SupportAttachmentModel::class);
		$file->setAttributes([
			'request_id' => $id
		]);
		if($file->save()) {
			return [
				'id' => $file->id,
				'path' => $file->getUrl(),
				'name' => $file->getFile()->name
			];
		}
		return false;
	}

	/**
	 * @param $params
	 * @return SupportRequestModel
	 */
	protected function getRequest($params) {
		$request = SupportRequestModel::find()->andWhere($params)->one();
		if(!$request) {
			throw new NotFoundHttpException();
		}
		return $request;
	}
}
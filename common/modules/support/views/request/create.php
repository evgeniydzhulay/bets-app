<?php


use common\modules\support\forms\RequestCreateForm;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use yii\web\View;

/* @var $this View */
/* @var $model RequestCreateForm */

$this->params['breadcrumbs'][] = $this->title;

if(!Yii::$app->user->isGuest) {
    $this->beginContent('@common/modules/user/views/shared/profile.php');
} else {
	$this->beginContent('@common/modules/user/views/shared/profile-guest.php');
}
?>
<div class="box">
    <?php
    $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_VERTICAL,
        'id' => 'content-block',
        'enableAjaxValidation' => true,
        'enableClientValidation' => true,
    ])
    ?>
    <div class="box-body">
		<?= $form->field($model, 'email') ?>
		<?= $form->field($model, 'phone') ?>
		<?= $form->field($model, 'name') ?>
		<?= $form->field($model, 'theme')->textarea([
			'rows' => 2
		]) ?>
		<?= $form->field($model, 'message')->textarea([
			'rows' => 10
		]) ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('support', 'Send query'), ['class' => 'btn btn-success btn-red']) ?>
    </div>
    <?php ActiveForm::end() ?>
    </div>
<?php
$this->endContent();
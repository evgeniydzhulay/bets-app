<?php

use dosamigos\fileupload\FileUploadAsset;
use common\assets\SupportAsset;
use yii\web\View;

/**
 * @var $this View
 */

FileUploadAsset::register($this);
SupportAsset::register($this);

if(!Yii::$app->user->isGuest) {
	$this->beginContent('@common/modules/user/views/shared/profile.php');
} else {
	$this->beginContent('@common/modules/user/views/shared/profile-guest.php');
}
?>
<script>
    window.$locale = '<?= Yii::$app->language; ?>';
    window.$messages = <?= json_encode([
        Yii::$app->language => [
            'Requests list' => Yii::t('support', 'Requests list'),
            'Last update by {name} at {time}' => Yii::t('support', 'Last update by {name} at {time}'),
            'Is closed' => Yii::t('support', 'Is closed'),
            'Add files' => Yii::t('support', 'Add files'),
            'Message' => Yii::t('support', 'Message'),
            'Submit' => Yii::t('support', 'Submit'),
        ]
    ]); ?>;
</script>
<div class="container">
	<div id="support-chat"></div>
</div>
<?php
$this->endContent();
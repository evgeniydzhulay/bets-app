<?php

use common\assets\SupportAsset;
use common\modules\support\search\BackendSearch;
use common\widgets\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\Html;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var BackendSearch $searchModel
 * @var array $statuses
 */
$this->title = Yii::t('support', 'Requests list');
$this->params['breadcrumbs'][] = $this->title;
SupportAsset::register($this);
$this->beginContent('@common/modules/user/views/shared/profile.php');
Pjax::begin([
	'linkSelector' => '#support-list .pagination a, #support-search .search-reset, .requests-header a',
	'formSelector' => '#support-search',
	'id' => 'support-list_container',
	'options' => [
		'class' => 'container-fluid support-requests-list',
	],
	'timeout' => 5000,
	'clientOptions' => [
		'maxCacheLength' => 0
	]
]);
?>
<div class="box no-body requests-header">
	<div class="box-header">
		<span><?= Yii::t('support', 'Requests list'); ?></span>
	</div>
	<div class="box-body">
		<?php
		echo Html::a(Yii::t('support','Opened'), ['index', 'is_closed' => 0], [
			'class' => (Yii::$app->request->get('is_closed', false) ? '' : ' active')
		]);
		echo Html::a(Yii::t('support','Closed'), ['index', 'is_closed' => 1], [
			'class' => (Yii::$app->request->get('is_closed', false) ? ' active' : '')
		]);
		?>
	</div>
</div>

<?= GridView::widget([
	'dataProvider' => $dataProvider,
	'id' => 'support-tickets-search',
	'toolbar' => [
		['content' =>
			Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::toRoute(['index']), [
				'class' => 'btn btn-default btn-sm',
				'title' => Yii::t('app', 'Reset filter')]
			)
		],
	],
	'columns' => [
		[
			'attribute' => 'theme',
			'headerOptions' => ['width' => '150px'],
			'content' => function($model) {
				return Html::a($model->theme, [
					'request/chat', 'id' => $model['id'], 'key' => $model['key']
				], [
					'data' => [
						'pjax' => 0
					]
				]);
			}
		],
		[
			'attribute' => 'id',
			'headerOptions' => ['width' => '80px'],
		],
		[
			'attribute' => 'created_at',
			'headerOptions' => ['width' => '120px'],
			'content' => function($model) {
				return date('Y-m-d H:i:s', $model['created_at']);
			},
		],
		[
			'attribute' => 'message',
		],
		[
			'attribute' => 'updated_by',
			'headerOptions' => ['width' => '150px'],
			'content' => function($model) {
				if($model->lastMessage->from) {
					return $model->lastMessage->from->profile->name . ' ' . $model->lastMessage->from->profile->surname;

				}
			},
		],
		[
			'attribute' => 'updated_at',
			'headerOptions' => ['width' => '120px'],
			'content' => function($model) {
				return date('Y-m-d H:i:s', $model['created_at']);
			},
		],
	],
]);
Pjax::end();
$this->endContent();
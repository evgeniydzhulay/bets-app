<?php

namespace common\modules\support\models;

use common\modules\support\models\SupportMessageModel;
use common\modules\support\models\SupportRequestModel;
use yii\data\ActiveDataProvider;

class SearchModel extends SupportRequestModel {

	public $message;
	public $updated_by;
	public $messageSearch;

	/** {@inheritdoc} */
	public function scenarios () {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'messageSearch';
		return $scenarios;
	}

	/** {@inheritdoc} */
	public function rules () {
		return parent::rules() + [
			'messageSearch' => ['messageSearch', 'safe']
		];
	}

	public function search($params) {
		$query = static::find()->joinWith(['lastMessage'])->with(['lastMessage.from', 'lastMessage.from.profile'])->select([
			SupportRequestModel::tableName() . '.*',
			SupportMessageModel::tableName() . '.message',
			'updated_by' => SupportMessageModel::tableName() . '.from_id',
		]);
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
				'pageSizeLimit' => false
			],
			'sort' => [
				'defaultOrder' => [
					'id' => SORT_DESC
				]
			]
		]);
		if ($this->load($params) && $this->validate()) {
			$query->andFilterWhere(['like', self::tableName() . '.name', $this->name]);
			$query->andFilterWhere(['like', self::tableName() . '.theme', $this->theme]);
			$query->andFilterWhere(['like', self::tableName() . '.email', $this->email]);
			$query->andFilterWhere(['like', self::tableName() . '.phone', $this->phone]);
			$query->andFilterWhere(['like', self::tableName() . '.messageSearch', $this->messageSearch]);
			$query->andFilterWhere([
				self::tableName() . '.is_closed' => $this->is_closed,
				self::tableName() . '.is_new' => $this->is_new,
			]);
		}
		return $dataProvider;
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'id' => \Yii::t('support', 'Request ID'),
			'updated_by' => \Yii::t('support', 'Updated by'),
			'message' => \Yii::t('support', 'Message'),
			'messageSearch' => \Yii::t('support', 'Message'),
			'user' => \Yii::t('support', 'User'),
		] + parent::attributeLabels();
	}

	/** {@inheritdoc} */
	public function formName () {
		return '';
	}
}
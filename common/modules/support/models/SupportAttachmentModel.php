<?php

namespace common\modules\support\models;

use common\modules\Storage;
use common\modules\support\queries\SupportAttachmentQuery;
use common\modules\support\queries\SupportMessageQuery;
use common\modules\support\queries\SupportRequestQuery;
use League\Flysystem\Filesystem;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\web\UploadedFile;

/**
 * This is the model class for table "{{%support_attachment}}".
 *
 * @property int $id
 * @property int $request_id
 * @property int $message_id
 * @property string $path
 * @property string $name
 * @property int $created_at
 *
 * @property SupportMessageModel $message
 * @property Filesystem $storage
 * @property SupportRequestModel $request
 */
class SupportAttachmentModel extends \yii\db\ActiveRecord {

	/** {@inheritdoc} */
	public static function tableName () {
		return '{{%support_attachment}}';
	}

	/**
	 * {@inheritdoc}
	 * @return SupportAttachmentQuery the active query used by this AR class.
	 */
	public static function find () {
		return new SupportAttachmentQuery(get_called_class());
	}

	/** {@inheritdoc} */
	public function rules () {
		return [
			[['request_id', 'message_id', 'created_at'], 'integer'],
			[['path'], 'string', 'max' => 255],
			[['message_id'], 'exist', 'skipOnError' => true, 'targetClass' => SupportMessageModel::class, 'targetAttribute' => ['message_id' => 'id']],
			[['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => SupportRequestModel::class, 'targetAttribute' => ['request_id' => 'id']],
		];
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'id' => Yii::t('support', 'ID'),
			'request_id' => Yii::t('support', 'Request'),
			'message_id' => Yii::t('support', 'Message'),
			'path' => Yii::t('support', 'Path'),
			'created_at' => Yii::t('support', 'Created at'),
		];
	}

	/**
	 * @return SupportMessageQuery
	 */
	public function getMessage () {
		return $this->hasOne(SupportMessageModel::class, ['id' => 'message_id']);
	}

	/**
	 * @return SupportRequestQuery
	 */
	public function getRequest () {
		return $this->hasOne(SupportRequestModel::class, ['id' => 'request_id']);
	}

	/** {@inheritdoc} */
	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			]
		];
	}

	/** {@inheritdoc} */
	public function save ($runValidation = true, $attributeNames = null) {
		if($this->isNewRecord) {
			if(!($file = $this->getFile())) {
				return false;
			}
			$model = Yii::createObject([
				'class' => FileUploadModel::class,
				'file' => $file
			]);
			if ($model->upload()) {
				$result = $model->getResponse();
				$this->path = $result['fileName'];
				$this->name = !empty($this->name) ? $this->name : $file->name;
			}
		}
		return parent::save($runValidation, $attributeNames);
	}

	/**
	 * @return null|UploadedFile
	 */
	public function getFile() {
		return UploadedFile::getInstanceByName( 'file');
	}

	/**
	 * @return bool
	 */
	public function deleteFile() {
		if(!empty($this->path) && $this->getStorage()->has($this->path)) {
			return $this->getStorage()->delete($this->path);
		}
		return false;
	}

	/**
	 * @return bool
	 * @throws \Throwable
	 */
	public function delete() {
		if(($res = parent::delete())) {
			$this->deleteFile();
			return $res;
		}
		return false;
	}

	public function getStorageModule() : Storage {
		return Yii::$app->getModule('supportStorage');
	}

	public function getStorage() : Filesystem {
		return $this->getStorageModule()->get('storage');
	}

	public function getUrl() {
		return Yii::getAlias($this->getStorageModule()->uploadUrl . '/' . $this->path);
	}
}

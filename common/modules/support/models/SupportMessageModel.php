<?php

namespace common\modules\support\models;

use common\modules\support\queries\SupportAttachmentQuery;
use common\modules\support\queries\SupportMessageQuery;
use common\modules\support\queries\SupportRequestQuery;
use common\modules\user\models\UserModel;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "{{%support_message}}".
 *
 * @property int $id
 * @property int $from_id
 * @property string $from_email
 * @property string $from_phone
 * @property string $from_name
 * @property int $request_id
 * @property string $message
 * @property int $attachments_count
 * @property int $is_new
 * @property int $created_at
 *
 * @property SupportAttachmentModel[] $attachments
 * @property SupportRequestModel $request
 * @property UserModel $from
 */
class SupportMessageModel extends \yii\db\ActiveRecord {

	/** {@inheritdoc} */
	public static function tableName () {
		return '{{%support_message}}';
	}

	/**
	 * {@inheritdoc}
	 * @return SupportMessageQuery the active query used by this AR class.
	 */
	public static function find () {
		return new SupportMessageQuery(get_called_class());
	}

	/** {@inheritdoc} */
	public function rules () {
		return [
			[['from_id', 'request_id', 'attachments_count'], 'integer'],
			[['is_new'], 'boolean'],
			[['message'], 'string'],
			[['from_email', 'from_phone', 'from_name'], 'string', 'max' => 255],
			[['request_id'], 'exist', 'skipOnError' => true, 'targetClass' => SupportRequestModel::class, 'targetAttribute' => ['request_id' => 'id']],
			[['from_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['from_id' => 'id']],
		];
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'id' => Yii::t('support', 'ID'),
			'from_id' => Yii::t('support', 'From ID'),
			'from_email' => Yii::t('support', 'From email'),
			'from_phone' => Yii::t('support', 'From phone'),
			'from_name' => Yii::t('support', 'From name'),
			'request_id' => Yii::t('support', 'Request'),
			'message' => Yii::t('support', 'Message'),
			'attachments_count' => Yii::t('support', 'Attachments count'),
			'is_new' => Yii::t('support', 'Is new'),
			'created_at' => Yii::t('support', 'Created at'),
		];
	}

	/**
	 * @return SupportAttachmentQuery
	 */
	public function getAttachments () {
		return $this->hasMany(SupportAttachmentModel::class, ['message_id' => 'id'])->inverseOf('message');
	}

	/**
	 * @return SupportRequestQuery
	 */
	public function getRequest () {
		return $this->hasOne(SupportRequestModel::class, ['id' => 'request_id']);
	}

	/**
	 * @return \yii\db\ActiveQuery
	 */
	public function getFrom () {
		return $this->hasOne(UserModel::class, ['id' => 'from_id']);
	}

	/** {@inheritdoc} */
	public function behaviors() {
		return [
			[
				'class' => TimestampBehavior::class,
				'updatedAtAttribute' => false,
			]
		];
	}

	public function beforeSave ($insert) {
		if(parent::beforeSave($insert)) {
			$this->message = htmlspecialchars(preg_replace('#\R+#', "\n", $this->message));
			return true;
		}
		return false;
	}

	public function afterSave ($insert, $changedAttributes) {
		parent::afterSave($insert, $changedAttributes);
		if($insert) {
			SupportRequestModel::updateAll([
				'last_message_id' => $this->id,
			], ['id' => $this->request_id]);
			SupportMessageModel::updateAll([
				'is_new' => false
			], '`from_email` != :email AND `request_id` = :request AND `is_new` = 1', [
				'email' => $this->from_email,
				'request' => $this->request_id,
			]);
		}
	}
}

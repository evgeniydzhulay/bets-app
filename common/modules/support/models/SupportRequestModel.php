<?php

namespace common\modules\support\models;

use common\modules\support\queries\SupportAttachmentQuery;
use common\modules\support\queries\SupportMessageQuery;
use common\modules\support\queries\SupportRequestQuery;
use common\modules\user\helpers\UserPassword;
use common\modules\user\models\UserModel;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "{{%support_request}}".
 *
 * @property int $id
 * @property int $user_id
 * @property int $operator_id
 * @property string $email
 * @property string $phone
 * @property string $name
 * @property string $theme
 * @property string $key
 * @property int $last_message_id [int(11) unsigned]
 * @property int $is_closed
 * @property int $is_new
 * @property int $created_at
 * @property int $updated_at
 *
 * @property SupportAttachmentModel[] $attachments
 * @property SupportMessageModel[] $messages
 * @property SupportMessageModel $lastMessage
 * @property UserModel $operator
 * @property UserModel $user
 */
class SupportRequestModel extends ActiveRecord {

	const SCENARIO_CREATE = 'create';
	const SCENARIO_UPDATE = 'update';
	const SCENARIO_SEARCH = 'search';
	const SCENARIO_CLOSE = 'close';

	/** {@inheritdoc} */
	public static function tableName () {
		return '{{%support_request}}';
	}

	/**
	 * {@inheritdoc}
	 * @return SupportRequestQuery the active query used by this AR class.
	 */
	public static function find () {
		return new SupportRequestQuery(get_called_class());
	}

	/** @inheritdoc */
	public function scenarios() {
		return [
			self::SCENARIO_CREATE => ['user_id', 'operator_id', 'is_closed', 'is_new', 'created_at', 'updated_at', 'theme', 'email', 'phone', 'name'],
			self::SCENARIO_UPDATE => ['operator_id', 'is_closed', 'is_new', 'created_at', 'updated_at'],
			self::SCENARIO_CLOSE => ['is_closed'],
			self::SCENARIO_SEARCH => ['user_id', 'operator_id', 'is_closed', 'is_new', 'created_at', 'updated_at', 'theme', 'email', 'phone', 'name', 'key'],
		];
	}

	/** {@inheritdoc} */
	public function rules () {
		return [
			[['user_id', 'operator_id', 'created_at', 'updated_at'], 'integer', 'on' => [
				self::SCENARIO_CREATE, self::SCENARIO_UPDATE
			]],
			[['is_closed', 'is_new'], 'boolean', 'on' => [
				self::SCENARIO_CREATE, self::SCENARIO_UPDATE
			]],
			[['theme'], 'string'],
			[['email', 'phone', 'name'], 'string', 'max' => 255],
			[['operator_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['operator_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE, self::SCENARIO_UPDATE
			]],
			[['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => UserModel::class, 'targetAttribute' => ['user_id' => 'id'], 'on' => [
				self::SCENARIO_CREATE, self::SCENARIO_UPDATE
			]],
			'safeSearch' => [['user_id', 'operator_id', 'is_closed', 'is_new', 'created_at', 'updated_at', 'theme', 'email', 'phone', 'name', 'key'], 'safe', 'on' => [
				self::SCENARIO_SEARCH
			]],
		];
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'id' => Yii::t('support', 'ID'),
			'user_id' => Yii::t('support', 'User'),
			'operator_id' => Yii::t('support', 'Operator'),
			'email' => Yii::t('support', 'Email'),
			'phone' => Yii::t('support', 'Phone'),
			'name' => Yii::t('support', 'Name'),
			'theme' => Yii::t('support', 'Theme'),
			'key' => Yii::t('support', 'Key'),
			'is_closed' => Yii::t('support', 'Is closed'),
			'is_new' => Yii::t('support', 'Is new'),
			'created_at' => Yii::t('support', 'Created at'),
			'updated_at' => Yii::t('support', 'Updated at'),
		];
	}

	/**
	 * @return SupportAttachmentQuery
	 */
	public function getAttachments () {
		return $this->hasMany(SupportAttachmentModel::class, ['request_id' => 'id'])->inverseOf('request');
	}

	/**
	 * @return SupportMessageQuery
	 */
	public function getMessages () {
		return $this->hasMany(SupportMessageModel::class, ['request_id' => 'id'])->inverseOf('request');
	}

	/**
	 * @return SupportMessageQuery
	 */
	public function getLastMessage () {
		return $this->hasOne(SupportMessageModel::class, ['id' => 'last_message_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getOperator () {
		return $this->hasOne(UserModel::class, ['id' => 'operator_id']);
	}

	/**
	 * @return ActiveQuery
	 */
	public function getUser () {
		return $this->hasOne(UserModel::class, ['id' => 'user_id']);
	}

	/** {@inheritdoc} */
	public function beforeSave ($insert) {
		if(parent::beforeSave($insert)) {
			if($insert) {
				$this->key = UserPassword::generate(64);
			}
			return true;
		}
		return false;
	}

	/** {@inheritdoc} */
	public function behaviors() {
		return [
			TimestampBehavior::class,
		];
	}
}

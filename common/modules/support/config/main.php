<?php

use common\modules\Storage;
use creocoder\flysystem\LocalFilesystem;

return [
	'modules' => [
		'support' => [
			'class' => \common\modules\support\Module::class,
		],
		'supportStorage' => [
			'class' => Storage::class,
			'uploadUrl' => '@web/uploads/support',
			'internalUrl' => '@web/uploads/protected/support',
			'uploadDir' => '@uploads/protected/support',
			'components' => [
				'storage' => [
					'class' => LocalFilesystem::class,
					'path' => '@uploads/protected/support'
				]
			]
		],
	],
	'params' => [
		'socketSupport.path' => '/socket-support',
	],
];
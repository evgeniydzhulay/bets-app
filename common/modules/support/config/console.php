<?php

return [
	'controllerMap' => [
		'migrate' => [
			'migrationNamespaces' => [
				'common\modules\support\migrations',
			],
		],
	],
];

<?php

namespace common\modules\support\migrations;

use yii\db\Migration;

class m190413_113721_last_message_null extends Migration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
    public function safeUp() {
		$this->alterColumn('{{%support_request}}', 'last_message_id', $this->integer()->unsigned()->null());
    }

    public function safeDown() {
    }

}

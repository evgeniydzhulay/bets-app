<?php

namespace common\modules\support\migrations;

use common\traits\MigrationTypesTextTrait;
use Yii;

class m190226_130000_support_init extends \yii\db\Migration {

	use MigrationTypesTextTrait;

	/**
	 * Create tables.
	 */
	public function up () {
		$tableOptions = null;
		if (Yii::$app->db->driverName === 'mysql') {
			// http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
			$tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
		}
		$this->createTable('{{%support_request}}', [
			'id' => $this->primaryKey()->unsigned(),
			'user_id' => $this->integer()->unsigned(),
			'operator_id' => $this->integer()->unsigned(),
			'last_message_id' => $this->integer()->unsigned()->notNull(),
			'key' => $this->string(64)->notNull(),
			'email' => $this->string()->notNull(),
			'phone' => $this->string(),
			'name' => $this->string(),
			'theme' => $this->text(),
			'is_closed' => $this->integer(1)->defaultValue(0),
			'is_new' => $this->integer(1)->defaultValue(1),
			'created_at' => $this->integer(),
			'updated_at' => $this->integer(),
		], $tableOptions);
		$this->createIndex('idx-support_request-user', '{{%support_request}}', 'user_id');
		$this->createIndex('idx-support_request-operator', '{{%support_request}}', 'operator_id');
		$this->createIndex('idx-support_request-message', '{{%support_request}}', 'last_message_id');
		$this->createIndex('idx-support_request-key', '{{%support_request}}', 'key');
		$this->execute("ALTER TABLE {{%support_request}} ADD FULLTEXT INDEX `idx-support_request-email` (`email` ASC)");
		$this->execute("ALTER TABLE {{%support_request}} ADD FULLTEXT INDEX `idx-support_request-phone` (`phone` ASC)");
		$this->execute("ALTER TABLE {{%support_request}} ADD FULLTEXT INDEX `idx-support_request-theme` (`theme` ASC)");
		$this->createIndex('idx-support_request-created_at', '{{%support_request}}', 'created_at');
		$this->createIndex('idx-support_request-updated_at', '{{%support_request}}', 'updated_at');

		$this->addForeignKey('fk-support_request-user', '{{%support_request}}', 'user_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-support_request-operator', '{{%support_request}}', 'operator_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%support_message}}', [
			'id' => $this->primaryKey()->unsigned(),
			'from_id' => $this->integer()->unsigned(),
			'from_email' => $this->string(),
			'from_phone' => $this->string(),
			'from_name' => $this->string(),
			'request_id' => $this->integer()->unsigned(),
			'message' => $this->longText(),
			'attachments_count' => $this->integer()->unsigned()->defaultValue(0),
			'is_new' => $this->integer(1)->defaultValue(1),
			'created_at' => $this->integer(),
		], $tableOptions);
		$this->createIndex('idx-support_message-user', '{{%support_message}}', 'from_id');
		$this->createIndex('idx-support_message-request', '{{%support_message}}', 'request_id');
		$this->execute("ALTER TABLE {{%support_message}} ADD FULLTEXT INDEX `idx-support_message-message` (`message` ASC)");
		$this->addForeignKey('fk-support_message-user', '{{%support_message}}', 'from_id', '{{%user}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-support_message-request', '{{%support_message}}', 'request_id', '{{%support_request}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-support_request-message', '{{%support_request}}', 'last_message_id', '{{%support_message}}', 'id', 'CASCADE', 'CASCADE');

		$this->createTable('{{%support_attachment}}', [
			'id' => $this->primaryKey()->unsigned(),
			'request_id' => $this->integer()->unsigned(),
			'message_id' => $this->integer()->unsigned(),
			'name' => $this->string()->notNull(),
			'path' => $this->string(),
			'created_at' => $this->integer(),
		], $tableOptions);
		$this->createIndex('idx-support_attachment-message', '{{%support_attachment}}', 'message_id');
		$this->createIndex('idx-support_attachment-request', '{{%support_attachment}}', 'request_id');

		$this->addForeignKey('fk-support_attachment-message', '{{%support_attachment}}', 'message_id', '{{%support_message}}', 'id', 'CASCADE', 'CASCADE');
		$this->addForeignKey('fk-support_attachment-request', '{{%support_attachment}}', 'request_id', '{{%support_request}}', 'id', 'CASCADE', 'CASCADE');

	}

	/**
	 * Drop tables.
	 */
	public function down () {
		$this->dropForeignKey('fk-support_request-message', '{{%support_request}}');
		$this->dropTable('{{%support_attachment}}');
		$this->dropTable('{{%support_message}}');
		$this->dropTable('{{%support_request}}');
	}
}
<?php

namespace common\modules\support\migrations;

use common\modules\user\rbac\RbacMigration;

class m190226_140000_support_rbac_init extends RbacMigration {

	/**
	 * @return void
	 * @throws \yii\base\Exception
	 * @throws \Exception
	 */
    public function safeUp() {
	    $admin = $this->authManager->getRole('admin');

	    $create = $this->createPermission('support-view', 'View support info.');
	    $edit = $this->createPermission('support-reply', 'Support worker.');

	    $this->assignChild($create, $edit);
	    $this->assignChild($admin, $create);
    }

    public function safeDown() {
		$this->removePermission('support-reply');
	    $this->removePermission('support-view');
    }

}

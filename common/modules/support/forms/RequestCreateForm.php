<?php

namespace common\modules\support\forms;

use common\modules\support\models\SupportMessageModel;
use Yii;
use common\modules\support\models\SupportRequestModel;
use yii\base\Model;

class RequestCreateForm extends Model {

	public $theme;
	public $message;
	public $files;

	public $name;
	public $email;
	public $phone;
	public $user_id;

	/** {@inheritdoc} */
	public function rules () {
		return [
			[['theme', 'message'], 'string', 'min' => 10, 'max' => 65000],
			[['email', 'phone', 'name'], 'string', 'max' => 255],
			[['email'], 'email'],
			[['email', 'theme', 'message'], 'required'],
		];
	}

	/**
	 * @return bool|SupportRequestModel
	 */
	public function save() {
		if (!$this->validate()) {
			return false;
		}
		$tr = Yii::$app->db->beginTransaction();
		$request = Yii::createObject(SupportRequestModel::class);
		$request->scenario = SupportRequestModel::SCENARIO_CREATE;
		$request->setAttributes([
			'user_id' => $this->user_id,
			'email' => $this->email,
			'phone' => $this->phone,
			'name' => $this->name,
			'theme' => $this->theme,
			'is_closed' => false,
			'is_new' => true,
		]);
		if(!$request->save()) {
			$tr->rollBack();
			Yii::error($request->errors);
			return false;
		}
		$message = Yii::createObject(SupportMessageModel::class);
		$message->setAttributes([
			'from_id' => $this->user_id,
			'from_email' => $this->email,
			'from_phone' => $this->phone,
			'from_name' => $this->name,
			'request_id' => $request->id,
			'message' => $this->message,
			'is_new' => true,
		]);
		if(!$message->save()) {
			$tr->rollBack();
			Yii::error($message->errors);
			return false;
		}
		$request->last_message_id = $message->id;
		Yii::$app->getModule('support')->sendNewRequest($request);
		$tr->commit();
		return $request;
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'email' => Yii::t('support', 'Email'),
			'phone' => Yii::t('support', 'Phone'),
			'name' => Yii::t('support', 'Name'),
			'theme' => Yii::t('support', 'Theme'),
			'message' => Yii::t('support', 'Message'),
		];
	}
}
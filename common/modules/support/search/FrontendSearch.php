<?php

namespace common\modules\support\search;

use common\modules\support\models\SupportRequestModel;
use Yii;
use common\modules\support\models\SearchModel;

class FrontendSearch extends SearchModel {

	public static function find() {
		$query = parent::find();
		if(!Yii::$app->user->isGuest) {
			$query->andWhere([
				'or',
				[SupportRequestModel::tableName(). '.user_id' => Yii::$app->user->id],
				[SupportRequestModel::tableName(). '.email' => Yii::$app->user->identity->email],
			]);
		}
		return $query;
	}
}
<?php

namespace common\assets;

use yii\web\AssetBundle;

class FontAmesomeAsset extends AssetBundle {

	/** @inheritdoc */
	public $sourcePath = '@npm/components-font-awesome';

	/** @inheritdoc */
	public $css = [
		'css/all.min.css'
	];

	public $publishOptions = [
		'only' => [
			'webfonts/*',
			'css/*',
		],
	];
}
<?php

namespace common\assets;

use yii\web\AssetBundle;

class VueRouterAsset extends AssetBundle {

	/**
	 * Default path
	 * @var string
	 */
	public $sourcePath = '@npm/vue-router/dist';

	public $js = [
		'vue-router.min.js'
	];
}
<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * AdminAsset
 *
 * Basic admin asset
 */
class SocketIoAsset extends AssetBundle {

    /** @inheritdoc */
    public $sourcePath = '@npm/socket.io-client/dist';

    /** @inheritdoc */
    public $js = [
        'socket.io.js'
    ];

}
import Vue from 'vue';
import VueI18n from 'vue-i18n';
import display from './content.vue';

(function () {
    Vue.use(VueI18n);
    const i18n = new VueI18n({
        locale: window.$locale,
        fallbackLocale: 'en',
        formatFallbackMessages: true,
        messages: window.$messages,
    });
    new Vue({
        el: '#support-chat',
        i18n: i18n,
        render: function(createElement) {
            return createElement(display);
        },
    });
})();
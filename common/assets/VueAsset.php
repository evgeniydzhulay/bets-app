<?php

namespace common\assets;

use yii\web\AssetBundle;

class VueAsset extends AssetBundle {

	/**
	 * Default path
	 * @var string
	 */
	public $sourcePath = '@npm/vue/dist';

	public $js = [
		'vue.min.js'
	];
}
<?php

namespace common\assets;

use yii\web\AssetBundle;

class ChartJsAsset extends AssetBundle {

	/**
	 * Default path
	 * @var string
	 */
	public $sourcePath = '@npm/chart.js/dist';

	public $js = [
		'Chart.js'
	];
}
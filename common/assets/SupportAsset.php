<?php

namespace common\assets;

use common\assets\SocketIoAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class SupportAsset  extends AssetBundle {

	/** {@inheritdoc} */
	public $sourcePath = null;

	/** {@inheritdoc} */
	public $basePath = '@webroot/static';

	/** {@inheritdoc} */
	public $baseUrl = '@web/static';

	/** {@inheritdoc} */
	public $css = [
		'css/support.css'
	];

	/** {@inheritdoc} */
	public $js = [
		'js/vue.min.js',
		'js/support.min.js',
	];

	/** {@inheritdoc} */
	public $depends = [
		JqueryAsset::class,
		SocketIoAsset::class,
	];
}
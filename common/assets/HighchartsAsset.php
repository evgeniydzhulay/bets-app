<?php

namespace common\assets;

use yii\web\AssetBundle;

class HighchartsAsset extends AssetBundle {

	/**
	 * Default path
	 * @var string
	 */
	public $sourcePath = '@npm/highcharts';

	public $js = [
		'highstock.js'
	];

	public $publishOptions = [
		'only' => [
			'highstock.js',
			'modules/series-label.js',
		],
	];
}
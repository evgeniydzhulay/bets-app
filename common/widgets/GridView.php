<?php

namespace common\widgets;

use kartik\grid\GridView as BaseGrid;
use yii\base\InvalidConfigException;

class GridView extends BaseGrid {


	public $panelHeading = '';
	public $pjax = true;
	public $pjaxSettings = [
		'loadingCssClass' => false,
	];
	public $hover = false;
	public $export = false;
	public $toolbar = false;
	public $resizableColumns = false;
	public $floatHeader = false;
	public $showFiltersCollapse = false;
	public $striped = false;
	public $panelPrefix = 'box box-grid box-';
	public $panel = [
		'type' => self::TYPE_DEFAULT,
		'headingOptions' => [
			'class' => 'box-header',
		],
		'footer' => false,
	];
	public $pager = [
		'options' => [
			'class' => 'pagination pagination-sm no-margin',
		],
	];
	public $containerOptions = [
		'class' => 'box-body',
	];
	public $panelTemplate = '{panelHeading}{items}{panelFooter}';
	public $panelHeadingTemplate = '
	    <div class="pull-left">
	        {pager}
	        {summary}
	    </div>
	    <div class="pull-right">
	        <div class="btn-toolbar kv-grid-toolbar" role="toolbar">
	            {toolbar}
	        </div>
	    </div>
	    <div class="clearfix"></div>
    ';

	/** @inheritdoc */
	public function init () {
		$this->panel['heading'] = $this->panelHeading;
		parent::init();
	}

	/**
	 * @inheritdoc
	 * @throws InvalidConfigException
	 */
	public function renderTableBody () {
		$content = parent::renderTableBody();
		if ($this->showPageSummary) {
			return $this->renderPageSummary() . $content;
		}
		return $content;
	}
}

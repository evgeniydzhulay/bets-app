<?php

namespace common\widgets;

use kartik\select2\Select2;
use Yii;
use yii\bootstrap\Nav;
use yii\helpers\ArrayHelper;
use yii\web\JsExpression;

class TimeZoneMenu extends Nav {

	public $encodeLabels = false;

	public $options = [
		'id' => 'nav-timezones',
		'class' => 'navbar-nav'
	];

	public function run() {
		$timezone = Yii::$app->get('locations')->getUserTimezone();
		if(empty($timezone)) {
			$timezone = 'UTC';
		}
		$this->items = [
			Select2::widget([
				'id' => 'select-timezone',
				'theme' => Select2::THEME_DEFAULT,
				'name' => 'timezone',
				'data' => ArrayHelper::map(Yii::$app->get('locations')->getTimeZones(), 'timezone', 'name'),
				'value' => $timezone,
				'pluginOptions' => [
					'dropdownAutoWidth' => true,
					'templateSelection' => new JsExpression("function(state){ return $('');}")
				],
				'pluginEvents' => [
					'change' => "function(event) {window.location.href = '/core/timezone?timezone=' + $(this).val();}"
				]
			])
		];
		return parent::run();
	}

}
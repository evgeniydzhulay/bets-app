<?php

namespace common\widgets;

use kartik\icons\Icon;
use Yii;
use yii\bootstrap\Nav;

class LanguageMenu extends Nav {

	public $encodeLabels = false;

	public $options = [
		'id' => 'nav-languages-website',
		'class' => 'navbar-nav'
	];

	public function run() {
		$languages = [];
		foreach (Yii::$app->params['languages'] AS $key => $item) {
			if($key == Yii::$app->language) {
				continue;
			}
			$languages[] = [
				'label' => Icon::show($item['flag'], []) . ' ' . $item['label'],
				'url' => ['/core/language', 'language' => $key]
			];
		}
		$this->items = [
			[
				'label' => Icon::show(Yii::$app->params['languages'][Yii::$app->language]['flag']),
				'items' => $languages
			],
		];
		return parent::run();
	}

}
<?php

namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\web\Application;

/**
 * @property-read string $timezone
 */
class TimezoneRequestBehavior extends Behavior {

	public function events () {
		return [
			Application::EVENT_BEFORE_REQUEST => function() {
				Yii::$app->setTimeZone($this->getTimezone());
			},
		];
	}

	/**
	 * Get current timezone
	 * @return string
	 */
	protected function getTimezone () {
		if (($request = Yii::$app->get('request', false)) && $request->get('timezone')) {
			return $request->get('timezone');
		} else if (($session = Yii::$app->get('session', false)) && $session->get('timezone')) {
			return $session->get('timezone');
		} else if (\array_key_exists('tzo', $_COOKIE)) {
			return $_COOKIE['tzo'];
		}
		return Yii::$app->getTimeZone();
	}

}

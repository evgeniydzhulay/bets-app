<?php

namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\web\Application;

/**
 *
 * @property string $currentLang
 */
class LanguageRequestBehavior extends Behavior {

	public function events () {
		return [
			Application::EVENT_BEFORE_REQUEST => 'setLanguage',
		];
	}

	public function setLanguage () {
		Yii::$app->language = $this->getCurrentLang();
	}

	/**
	 * Get current language
	 * If user is logged in gets its language
	 * Otherwise from session
	 * Otherwise from request
	 * @return string
	 */
	protected function getCurrentLang () {
		if (!Yii::$app->user->isGuest && !empty(Yii::$app->user->identity->lang)) {
			return Yii::$app->user->identity->lang;
		} else if (($request = Yii::$app->get('request', false)) && $request->get('lang')) {
			return $request->get('lang');
		} else if (($session = Yii::$app->get('session', false)) && $session->get('lang')) {
			return $session->get('lang');
		}
		return Yii::$app->getRequest()->getPreferredLanguage(\array_keys(Yii::$app->params['languages']));
	}

}

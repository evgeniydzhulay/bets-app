<?php

namespace frontend\controllers;

use common\modules\partners\models\PartnersLinksModel;
use common\modules\partners\models\PartnersLogClicksModel;
use common\modules\partners\models\PartnersLogLinksModel;
use common\modules\partners\models\PartnersLogViewsModel;
use common\modules\partners\models\PartnersModel;
use common\modules\partners\models\PromoMaterialsModel;
use Yii;
use yii\web\NotFoundHttpException;

class PromoController extends \yii\web\Controller {

	/**
	 * @param string $pl Partner link
	 * @param string $pm Partner material
	 * @param int $pid Partner id
	 * @param string $sid Partner SID
	 * @return \yii\web\Response
	 */
	public function actionLink ($pl = '', $pm = null, int $pid = null, $sid = null) {
		if (!\is_null($sid)) {
			\Yii::$app->session->set('partner_sid', $sid);
		}
		if (!empty($pl) && ($link = PartnersLinksModel::find()->andFilterWhere(['link' => $pl, 'partner_id' => $pid])->asArray()->one())) {
			Yii::$app->session->set('partner_link', $pl);
			$pid = $pid ?? +$link['partner_id'];
			$logLink = \Yii::createObject(PartnersLogLinksModel::class);
			$logLink->setAttributes([
				'partner_id' => $link['partner_id'],
				'link_id' => $link['id'],
				'created_at' => time(),
				'user_ip' => Yii::$app->request->userIP,
				'user_agent' => Yii::$app->request->userAgent,
				'user_referrer' => Yii::$app->request->referrer,
			]);
			$logLink->save(false);
		}
		if (!\is_null($pm) && ($material = PromoMaterialsModel::find()->andFilterWhere(['id' => $pm, 'partner_id' => $pid])->asArray()->one())) {
			\Yii::$app->session->set('partner_material', $pm);
			$pid = $pid ?? +$material['partner_id'];
			if (!empty($partner)) {
				$logClick = \Yii::createObject(PartnersLogClicksModel::class);
				$logClick->setAttributes([
					'partner_id' => $partner['id'],
					'material_id' => $material['id'],
					'created_at' => time(),
					'user_ip' => Yii::$app->request->userIP,
					'user_agent' => Yii::$app->request->userAgent,
					'user_referrer' => Yii::$app->request->referrer,
				]);
				$logClick->save(false);
			}
		}
		if (!\is_null($pid) && ($partner = PartnersModel::find()->andWhere(['id' => $pid])->asArray()->one())) {
			\Yii::$app->session->set('partner_id', $pid);
		}

		return $this->goHome();
	}

	public function actionMaterial ($id, $pid) {
		$partner = PartnersModel::find()->andWhere(['id' => $pid])->asArray()->one();
		$material = PromoMaterialsModel::find()->andWhere(['id' => $id])->one();
		if ($partner && $material) {
			$logView = \Yii::createObject(PartnersLogViewsModel::class);
			$logView->setAttributes([
				'partner_id' => $partner['id'],
				'material_id' => $material['id'],
				'created_at' => time(),
				'user_ip' => Yii::$app->request->userIP,
				'user_agent' => Yii::$app->request->userAgent,
				'user_referrer' => Yii::$app->request->referrer,
			]);
			$logView->save(false);
			Yii::$app->response->content = "Partner {$pid} material {$id}";
			Yii::$app->response->send();
		}
		throw new NotFoundHttpException();
	}

	public function actionReferral ($code = '') {
		if (!empty($code)) {
			\Yii::$app->session->set('referral_id', 1);
		}
		return $this->goHome();
	}

}
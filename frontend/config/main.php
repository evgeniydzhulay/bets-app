<?php

use common\behaviors\LanguageRequestBehavior;
use common\behaviors\TimezoneRequestBehavior;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\ArrayHelper;
use yii\web\JqueryAsset;
use yii\web\UrlManager;
use yii\web\UrlNormalizer;
use yii\web\YiiAsset;

return ArrayHelper::merge([
	'id' => 'frontend',
	'as LanguageRequestBehavior' => LanguageRequestBehavior::class,
	'as TimezoneRequestBehavior' => TimezoneRequestBehavior::class,
	'controllerNamespace' => 'frontend\controllers',
	'defaultRoute' => 'games/games/index',
	'basePath' => dirname(__DIR__),
	'components' => [
		'errorHandler' => [
			'errorAction' => 'core/error',
		],
		'view' => [
			'theme' => [
				'basePath' => '@common/views',
			],
		],
		'assetManager' => [
			'appendTimestamp' => true,
			'bundles' => [
				JqueryAsset::class => [
					'js' => ['jquery.min.js',],
				],
				BootstrapAsset::class => [
					'sourcePath' => null,
					'basePath' => '@webroot',
					'baseUrl' => '@web',
					'css' => ['static/css/bootstrap.min.css'],
				],
				BootstrapPluginAsset::class => [
					'js' => ['js/bootstrap.min.js'],
				],
			],
		],
		'urlManager' => [
			'class' => UrlManager::class,
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'normalizer' => [
				'class' => UrlNormalizer::class,
				'collapseSlashes' => true,
				'normalizeTrailingSlash' => true,
			],
			'rules' => [
				'' => '/games/games/index',
				'payment/result/<merchant:\w+>' => '/user/purse/result',
				'content/rules/<key:[A-Za-z0-9 -_.]+>' => '/content/view/rules',
				'content/rules' => '/content/view/rules',
				'content/bonuses/<key:[A-Za-z0-9 -_.]+>' => '/content/view/bonuses',
				'content/bonuses' => '/content/view/bonuses',
			],
		],
	],
],
	require_once(BASE_PATH . '/frontend/modules/user/config/frontend.php'),
	require_once(BASE_PATH . '/frontend/modules/games/config/frontend.php'),
	require_once(BASE_PATH . '/frontend/modules/blog/config/frontend.php'),
	require_once(BASE_PATH . '/frontend/modules/casino/config/frontend.php'),
	(is_file(__DIR__ . '/main-local.php') ? require_once(__DIR__ . '/main-local.php') : [])
);
<?php

use yii\helpers\ArrayHelper;

return ArrayHelper::merge([
	'components' => [
		'view' => [
			'theme' => [
				'pathMap' => [
					'@frontend/views/layouts' => '@frontend/views/layoutsMobile',
				]
			],
		],
	]
], (is_file(__DIR__ . '/mobile-local.php') ? require_once(__DIR__ . '/mobile-local.php') : []));
<?php

use frontend\assets\casino\rockpaperscissors\RockPaperScissorsAsset;

/**
 * @var $this yii\web\View
 */

RockPaperScissorsAsset::register($this);
$this->title = Yii::t('casino.rps', 'Rock Paper Scissors');
?>
<script>
    window.$locale = '<?= Yii::$app->language; ?>';
    window.$messages = <?= json_encode([
		Yii::$app->language => [
			'Place a bet' => Yii::t('casino','Place a bet'),
			'Place your bet' => Yii::t('casino','Place your bet'),
			'Your stake: {sum}' => Yii::t('casino','Your stake: {sum}'),
			'Minimum stake: {sum}' => Yii::t('casino','Minimum stake: {sum}'),
		]
	]); ?>;
</script>
<div id="rockpaperscissors-app"></div>

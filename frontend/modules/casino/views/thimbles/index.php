<?php

use frontend\assets\casino\thimbles\ThimblesAsset;

ThimblesAsset::register($this);

/**
 * @var $this yii\web\View
 */

$this->title = Yii::t('casino.thimbles','Thimbles');
?>

<script>
    window.$locale = '<?= Yii::$app->language; ?>';
    window.$messages = <?= json_encode([
		Yii::$app->language => [
			'1 ball' => Yii::t('casino.thimbles','1 ball'),
			'2 balls' => Yii::t('casino.thimbles','2 balls'),
			'Place a bet' => Yii::t('casino','Place a bet'),
			'Place your bet' => Yii::t('casino','Place your bet'),
			'Your stake: {sum}' => Yii::t('casino','Your stake: {sum}'),
			'Minimum stake: {sum}' => Yii::t('casino','Minimum stake: {sum}'),
			'Guess where the ball is' => Yii::t('casino.thimbles','Guess where the ball is'),
		]
	]); ?>;
</script>
<div id="thimbles-app"></div>

<?php

/**
 * @var $this yii\web\View
 */

use frontend\assets\casino\roulette\RouletteAsset;

RouletteAsset::register($this);
$this->title = Yii::t('casino.roulette', 'Roulette');
?>
<div class="roulette" id="roulette-app">
	<div class="roulette__head">
		<p class="roulette__title"><?= Yii::t('casino.roulette','Roulette'); ?></p>
	</div>
	<div class="roulette__body">
		<div id="rouletteg" class="roulette__game">
			<div class="roulette__roulette idle">
				<div class="roulette-inner">
					<div class="roulette-inner__segment sector0"></div>
					<div class="roulette-inner__segment sector32"></div>
					<div class="roulette-inner__segment sector15"></div>
					<div class="roulette-inner__segment sector19"></div>
					<div class="roulette-inner__segment sector4"></div>
					<div class="roulette-inner__segment sector21"></div>
					<div class="roulette-inner__segment sector2"></div>
					<div class="roulette-inner__segment sector25"></div>
					<div class="roulette-inner__segment sector17"></div>
					<div class="roulette-inner__segment sector34"></div>
					<div class="roulette-inner__segment sector6"></div>
					<div class="roulette-inner__segment sector27"></div>
					<div class="roulette-inner__segment sector13"></div>
					<div class="roulette-inner__segment sector36"></div>
					<div class="roulette-inner__segment sector11"></div>
					<div class="roulette-inner__segment sector30"></div>
					<div class="roulette-inner__segment sector8"></div>
					<div class="roulette-inner__segment sector23"></div>
					<div class="roulette-inner__segment sector10"></div>
					<div class="roulette-inner__segment sector5"></div>
					<div class="roulette-inner__segment sector24"></div>
					<div class="roulette-inner__segment sector16"></div>
					<div class="roulette-inner__segment sector33"></div>
					<div class="roulette-inner__segment sector1"></div>
					<div class="roulette-inner__segment sector20"></div>
					<div class="roulette-inner__segment sector14"></div>
					<div class="roulette-inner__segment sector31"></div>
					<div class="roulette-inner__segment sector9"></div>
					<div class="roulette-inner__segment sector22"></div>
					<div class="roulette-inner__segment sector18"></div>
					<div class="roulette-inner__segment sector29"></div>
					<div class="roulette-inner__segment sector7"></div>
					<div class="roulette-inner__segment sector28"></div>
					<div class="roulette-inner__segment sector12"></div>
					<div class="roulette-inner__segment sector35"></div>
					<div class="roulette-inner__segment sector3"></div>
					<div class="roulette-inner__segment sector26"></div>
				</div>
				<div class="roulette-borders">
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
					<div class="roulette-borders__segment"></div>
				</div>
				<div class="roulette-outer">
					<div class="roulette-outer__segment sector0">0</div>
					<div class="roulette-outer__segment sector32">32</div>
					<div class="roulette-outer__segment sector15">15</div>
					<div class="roulette-outer__segment sector19">19</div>
					<div class="roulette-outer__segment sector4">4</div>
					<div class="roulette-outer__segment sector21">21</div>
					<div class="roulette-outer__segment sector2">2</div>
					<div class="roulette-outer__segment sector25">25</div>
					<div class="roulette-outer__segment sector17">17</div>
					<div class="roulette-outer__segment sector34">34</div>
					<div class="roulette-outer__segment sector6">6</div>
					<div class="roulette-outer__segment sector27">27</div>
					<div class="roulette-outer__segment sector13">13</div>
					<div class="roulette-outer__segment sector36">36</div>
					<div class="roulette-outer__segment sector11">11</div>
					<div class="roulette-outer__segment sector30">30</div>
					<div class="roulette-outer__segment sector8">8</div>
					<div class="roulette-outer__segment sector23">23</div>
					<div class="roulette-outer__segment sector10">10</div>
					<div class="roulette-outer__segment sector5">5</div>
					<div class="roulette-outer__segment sector24">24</div>
					<div class="roulette-outer__segment sector16">16</div>
					<div class="roulette-outer__segment sector33">33</div>
					<div class="roulette-outer__segment sector1">1</div>
					<div class="roulette-outer__segment sector20">20</div>
					<div class="roulette-outer__segment sector14">14</div>
					<div class="roulette-outer__segment sector31">31</div>
					<div class="roulette-outer__segment sector9">9</div>
					<div class="roulette-outer__segment sector22">22</div>
					<div class="roulette-outer__segment sector18">18</div>
					<div class="roulette-outer__segment sector29">29</div>
					<div class="roulette-outer__segment sector7">7</div>
					<div class="roulette-outer__segment sector28">28</div>
					<div class="roulette-outer__segment sector12">12</div>
					<div class="roulette-outer__segment sector35">35</div>
					<div class="roulette-outer__segment sector3">3</div>
					<div class="roulette-outer__segment sector26">26</div>
				</div>
				<div class="roulette__ring roulette__ring_inner"></div>
				<div class="roulette__ring roulette__ring_outer"></div>
				<div class="roulette__base"></div>
				<div id="rouletteb" class="roulette__ball"></div>
				<div class="roulette__handle"></div>
				<div class="roulette__border"></div>
			</div>
			<div id="roulettew" class="roulette-window">
				<div id="roulettew-close" class="roulette-window__close"></div>
				<div class="roulette__chips">
					<div class="roulette__chip chip1" data-cost="20"></div>
					<div class="roulette__chip chip2" data-cost="30"></div>
					<div class="roulette__chip chip3" data-cost="90"></div>
					<div class="roulette__chip chip4" data-cost="100"></div>
					<div class="roulette__chip chip5" data-cost="300"></div>
				</div>
				<button class="roulette-window__btn">Очистить</button>
			</div>
			<div class="roulette__grids">
				<div class="roulette-grid roulette-grid_numbers">
					<div class="roulette-grid__cell js_cell cell_0" data-num="0">
						<div class="roulette-grid__item">
							0
							<div class="roulette-grid__drop roulette-grid__drop_center">
							</div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_3" data-num="3">
						<div class="roulette-grid__item">
							3
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_2" data-num="2">
						<div class="roulette-grid__item">
							2
							<div class="roulette-grid__drop roulette-grid__drop_quad">
							</div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_1" data-num="1">
						<div class="roulette-grid__item">1
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_6" data-num="6">
						<div class="roulette-grid__item">6
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_5" data-num="5">
						<div class="roulette-grid__item">5
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_4" data-num="4">
						<div class="roulette-grid__item">4
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_9" data-num="9">
						<div class="roulette-grid__item">9
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_8" data-num="8">
						<div class="roulette-grid__item">8
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_7" data-num="7">
						<div class="roulette-grid__item">7
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_12" data-num="12">
						<div class="roulette-grid__item">12
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_11" data-num="11">
						<div class="roulette-grid__item">11
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_10" data-num="10">
						<div class="roulette-grid__item">10
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_15" data-num="15">
						<div class="roulette-grid__item">15
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_14" data-num="14">
						<div class="roulette-grid__item">14
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_13" data-num="13">
						<div class="roulette-grid__item">13
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_18" data-num="18">
						<div class="roulette-grid__item">18
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_17" data-num="17">
						<div class="roulette-grid__item">17
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_16" data-num="16">
						<div class="roulette-grid__item">16
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_21" data-num="21">
						<div class="roulette-grid__item">21
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_20" data-num="20">
						<div class="roulette-grid__item">20
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_19" data-num="19">
						<div class="roulette-grid__item">19
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_24" data-num="24">
						<div class="roulette-grid__item">24
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_23" data-num="23">
						<div class="roulette-grid__item">23
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_22" data-num="22">
						<div class="roulette-grid__item">22
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_27" data-num="27">
						<div class="roulette-grid__item">27
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_26" data-num="26">
						<div class="roulette-grid__item">26
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_25" data-num="25">
						<div class="roulette-grid__item">25
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_30" data-num="30">
						<div class="roulette-grid__item">30
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_29" data-num="29">
						<div class="roulette-grid__item">29
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_28" data-num="28">
						<div class="roulette-grid__item">28
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_33" data-num="33">
						<div class="roulette-grid__item">33
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_32" data-num="32">
						<div class="roulette-grid__item">32
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_31" data-num="31">
						<div class="roulette-grid__item">31
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_36" data-num="36">
						<div class="roulette-grid__item">36
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell black cell_35" data-num="35">
						<div class="roulette-grid__item">35
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div class="roulette-grid__cell js_cell red cell_34" data-num="34">
						<div class="roulette-grid__item">34
							<div class="roulette-grid__drop roulette-grid__drop_quad"></div>
							<div class="roulette-grid__drop roulette-grid__drop_vpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_hpair"></div>
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div id="third-row" class="roulette-grid__cell">
						<div class="roulette-grid__item">2 <?= Yii::t('casino.roulette','to'); ?> 1
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div id="second-row" class="roulette-grid__cell">
						<div class="roulette-grid__item">2 <?= Yii::t('casino.roulette','to'); ?> 1
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
					<div id="first-row" class="roulette-grid__cell">
						<div class="roulette-grid__item">2 <?= Yii::t('casino.roulette','to'); ?> 1
							<div class="roulette-grid__drop roulette-grid__drop_center"></div>
						</div>
					</div>
				</div>
				<div class="roulette-grid roulette-grid_groups">
					<div class="roulette-grid__row">
						<div id="first-dozen" class="roulette-grid__cell">
							<div class="roulette-grid__item"><?= Yii::t('casino.roulette','First'); ?> 12
								<div class="roulette-grid__drop roulette-grid__drop_center"></div>
							</div>
						</div>
						<div id="second-dozen" class="roulette-grid__cell">
							<div class="roulette-grid__item"><?= Yii::t('casino.roulette','Second'); ?> 12
								<div class="roulette-grid__drop roulette-grid__drop_center"></div>
							</div>
						</div>
						<div id="third-dozen" class="roulette-grid__cell">
							<div class="roulette-grid__item"><?= Yii::t('casino.roulette','Third'); ?> 12
								<div class="roulette-grid__drop roulette-grid__drop_center"></div>
							</div>
						</div>
					</div>
					<div class="roulette-grid__row">
						<div id="first-half" class="roulette-grid__cell">
							<div class="roulette-grid__item">1 <?= Yii::t('casino.roulette','to'); ?> 18
								<div class="roulette-grid__drop roulette-grid__drop_center"></div>
							</div>
						</div>
						<div id="even" class="roulette-grid__cell">
							<div class="roulette-grid__item"><?= Yii::t('casino.roulette','Even'); ?>
								<div class="roulette-grid__drop roulette-grid__drop_center"></div>
							</div>
						</div>
						<div id="red" class="roulette-grid__cell">
							<div class="roulette-grid__item roulette-grid__item_red">
								<div class="roulette-grid__drop roulette-grid__drop_center"></div>
							</div>
						</div>
						<div id="black" class="roulette-grid__cell">
							<div class="roulette-grid__item roulette-grid__item_black">
								<div class="roulette-grid__drop roulette-grid__drop_center"></div>
							</div>
						</div>
						<div id="odd" class="roulette-grid__cell">
							<div class="roulette-grid__item"><?= Yii::t('casino.roulette','Odd'); ?>
								<div class="roulette-grid__drop roulette-grid__drop_center"></div>
							</div>
						</div>
						<div id="second-half" class="roulette-grid__cell">
							<div class="roulette-grid__item">19 <?= Yii::t('casino.roulette','to'); ?> 36
								<div class="roulette-grid__drop roulette-grid__drop_center"></div>
							</div>
						</div>
					</div>
					<span class="roulette-grid__hint">
                        <i>Перетащите фишки</i>
                        <i>или кликните по полю</i>
                    </span>
				</div>
			</div>
			<div class="roulette__chips">
				<div class="roulette__chip chip1" data-cost="20" draggable="true"></div>
				<div class="roulette__chip chip2" data-cost="30" draggable="true"></div>
				<div class="roulette__chip chip3" data-cost="90" draggable="true"></div>
				<div class="roulette__chip chip4" data-cost="100" draggable="true"></div>
				<div class="roulette__chip chip5" data-cost="300" draggable="true"></div>
			</div>
			<div class="roulette-result">
				<div class="roulette-result__text" id="roulette_iso_res_msg">Делайте Ваши ставки!</div>
			</div>
			<div class="roulette-controls">
				<div class="roulette-controls__item roulette-controls__item_play">Запустить рулетку</div>
				<div class="roulette-controls__item roulette-controls__item_clear js_clear_bet">Убрать все фишки</div>
			</div>
		</div>
		<div class="roulette-bet">
			<p class="roulette-bet__heading">Номинал ставок (<span class="js_roulette_currency">UAH</span>)</p>
			<div class="roulette-bet__list">
				<div class="roulette-bet__item roulette-bet__item_1">20</div><div class="roulette-bet__item roulette-bet__item_2">30</div><div class="roulette-bet__item roulette-bet__item_3">90</div><div class="roulette-bet__item roulette-bet__item_4">100</div><div class="roulette-bet__item roulette-bet__item_5">300</div>            </div>
			<p class="roulette-bet__heading">Ваша ставка</p>
			<div class="roulette-bet__amount"><span class="js_sum_bet_container">0</span> <span class="js_roulette_currency">UAH</span></div>
			<div class="roulette-bet__btns">
				<button class="roulette-bet__btn roulette-bet__btn_play"></button>
				<button class="roulette-bet__btn roulette-bet__btn_clear js_clear_bet"></button>
			</div>
			<div class="roulette-bet__rules">Правила</div>
		</div>
	</div>
	<div id="roulette-win" class="roulette-popup hidden">
		<div class="roulette-popup__close modal-close">x</div>
		<div class="roulette-popup__body">
			<p class="roulette-popup__heading" id="roulette_popup_sumwin"></p>
			<button class="roulette-popup__btn modal-close">Играть еще</button>
		</div>
	</div>
	<div id="roulette-lose" class="roulette-popup hidden">
		<div class="roulette-popup__close modal-close">x</div>
		<div class="roulette-popup__body">
			<p class="roulette-popup__heading">Проигрыш</p>
			<button class="roulette-popup__btn modal-close">Попробовать снова</button>
		</div>
	</div>
	<div id="roulette-auth" class="roulette-popup hidden">
		<div class="roulette-popup__close modal-close">x</div>
		<div class="roulette-popup__body">
			<p class="roulette-popup__subheading">Чтобы начать игру необходимо</p>
			<div class="roulette-popup__wrap">
				<a href="/registration" class="roulette-popup__btn roulette-popup__btn_reg">Зарегистрироваться</a>
				<button id="loginTrigger" class="roulette-popup__btn roulette-popup__btn_login but login">Войти</button>
			</div>
		</div>
	</div>
	<div id="roulette-error" class="roulette-popup hidden">
		<div class="roulette-popup__close modal-close">x</div>
		<div class="roulette-popup__body">
			<p class="roulette-popup__heading" style="font-size: 24px; margin: 0;"></p>
		</div>
	</div>
	<div id="roulette-rules" class="roulette-popup fade modal">
		<div class="roulette-popup__close modal-close" data-dismiss="modal">x</div>
		<div class="roulette-popup__body roulette-popup__body_rules">
			<p class="roulette-popup__subheading"><?= Yii::t('casino.roulette','Roulette'); ?></p>
			<p class="roulette-popup__h3"><?= Yii::t('casino.roulette','Predict where the ball will land!'); ?></p>
			<p class="roulette-popup__caption"><?= Yii::t('casino.roulette','How to play'); ?></p>
			<ol class="roulette-popup__list">
				<li><?= Yii::t('casino.roulette','The game is played at a roulette table, which has a wheel and a betting area with numbers. The wheel contains 37 pockets numbered from 0 to 36 in a set sequence, typical of European roulette. The zero pocket is green and the others are red or black.'); ?></li>
				<li><?= Yii::t('casino.roulette','The initial stake is determined before the start of the game.'); ?></li>
				<li><?= Yii::t('casino.roulette','The maximum stake is 3683.28 UAH, the minimum stake is 20 UAH.'); ?></li>
				<li><?= Yii::t('casino.roulette','To place a bet, drag a chip of the desired value into the betting area or click on the betting area and select a chip.'); ?></li>
				<li><?= Yii::t('casino.roulette','To cancel a bet, press "Remove all chips".'); ?></li>
				<li><?= Yii::t('casino.roulette','When you have finished placing your chips, press "Spin". The wheel and the ball will start spinning in opposite directions. The ball will eventually land in one of the numbered pockets on the wheel.'); ?></li>
				<li><?= Yii::t('casino.roulette','Your potential winnings depend on the type of bet you make.'); ?></li>
			</ol>
			<p class="roulette-popup__caption"><?= Yii::t('casino.roulette','Bets and odds'); ?></p>
			<table class="roulette-popup__table">
				<tr>
					<th><?= Yii::t('casino.roulette','Name'); ?></th>
					<th><?= Yii::t('casino.roulette','Description'); ?></th>
					<th><?= Yii::t('casino.roulette','Odds'); ?></th>
				</tr>

				<tr>
					<td><?= Yii::t('casino.roulette','Straight-up Bet'); ?></td>
					<td><?= Yii::t('casino.roulette','Bet on any single number.'); ?></td>
					<td>36</td>
				</tr>
				<tr>
					<td><?= Yii::t('casino.roulette','Split Bet'); ?></td>
					<td><?= Yii::t('casino.roulette','Bet on two adjacent numbers by placing a chip on the line that divides the two numbers.'); ?></td>
					<td>18</td>
				</tr>
				<tr>
					<td><?= Yii::t('casino.roulette','Corner Bet'); ?></td>
					<td><?= Yii::t('casino.roulette','Bet on four numbers by placing a chip on the common corner of all four numbers.'); ?></td>
					<td>9</td>
				</tr>
				<tr>
					<td><?= Yii::t('casino.roulette','Dozen Bet'); ?></td>
					<td><?= Yii::t('casino.roulette','Bet on twelve numbers by placing your chip on one of the three boxes marked "1st 12", "2nd 12", or "3rd 12".'); ?></td>
					<td>3</td>
				</tr>
				<tr>
					<td><?= Yii::t('casino.roulette','Column Bet'); ?></td>
					<td><?= Yii::t('casino.roulette','Bet on which column of twelve numbers will contain the winning number by placing a chip in one of the boxes marked "2 to 1".'); ?></td>
					<td>3</td>
				</tr>
				<tr>
					<td><?= Yii::t('casino.roulette','Red/Black, Even/Odd, Low/High (1-18)/(19-36)'); ?></td>
					<td><?= Yii::t('casino.roulette','This bet is placed on one of the six boxes on the side of the betting area. Half of the roulette numbers are included in each bet, according to the description in the box. 0 is not included in any of them. Each box encompasses 18 numbers.'); ?></td>
					<td>2</td>
				</tr>
			</table>
			<p class="roulette-popup__text_common"><?= Yii::t('casino.roulette','If you experience any problems, please contact us within 3 days of finishing the game.'); ?></p>
			<p class="roulette-popup__msg"><?= Yii::t('casino.roulette','Looking for a thrill? Then place your bets Ladies and Gentlemen!'); ?></p>
		</div>
	</div>
</div>
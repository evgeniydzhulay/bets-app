<?php

use frontend\assets\casino\luckycard\LuckyCardAsset;

/**
 * @var $this yii\web\View
 */

LuckyCardAsset::register($this);
$this->title = Yii::t('casino.luckycard','Lucky card');
?>
<script>
    window.$locale = '<?= Yii::$app->language; ?>';
    window.$messages = <?= json_encode([
		Yii::$app->language => [
			'Red' => Yii::t('casino.luckycard','Red'),
			'Black' => Yii::t('casino.luckycard','Black'),
			'Place a bet' => Yii::t('casino','Place a bet'),
			'Place your bet' => Yii::t('casino','Place your bet'),
			'Your stake: {sum}' => Yii::t('casino','Your stake: {sum}'),
			'Minimum stake: {sum}' => Yii::t('casino','Minimum stake: {sum}'),
		]
	]); ?>;
</script>
<div id="luckycard-app"></div>

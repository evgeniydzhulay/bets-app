<?php
/**
 * @var $this yii\web\View
 */

use frontend\assets\casino\headsortails\HeadsOrTailsAsset;

HeadsOrTailsAsset::register($this);

$this->title = Yii::t('casino.hot', 'Heads or Tails');
?>
<script>
    window.$locale = '<?= Yii::$app->language; ?>';
    window.$messages = <?= json_encode([
		Yii::$app->language => [
			'Eagle' => Yii::t('casino.hot','Eagle'),
			'Tails' => Yii::t('casino.hot','Tails'),
			'Place a bet' => Yii::t('casino','Place a bet'),
			'Place your bet' => Yii::t('casino','Place your bet'),
			'Your stake: {sum}' => Yii::t('casino','Your stake: {sum}'),
			'Minimum stake: {sum}' => Yii::t('casino','Minimum stake: {sum}'),
		]
	]); ?>;
</script>
<div id="headsortails-app"></div>


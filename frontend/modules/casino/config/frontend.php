<?php

return [
	'modules' => [
		'casino' => [
			'controllerNamespace' => '\frontend\modules\casino\controllers',
			'viewPath' => '@frontend/modules/casino/views',
		],
	],
];

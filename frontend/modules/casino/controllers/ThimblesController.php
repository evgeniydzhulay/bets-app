<?php

namespace frontend\modules\casino\controllers;

use common\modules\casino\components\BetsComponent;
use common\modules\casino\models\ThimblesModel;
use Yii;
use yii\filters\ContentNegotiator;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;
use yii\web\UnauthorizedHttpException;

class ThimblesController extends Controller {

	/** @var BetsComponent */
	protected $_betsComponent;

	public function __construct($id, $module, $config = array()) {
		$this->_betsComponent = Yii::createObject(BetsComponent::class);
		return parent::__construct($id, $module, $config);
	}

	public function actionIndex () {
		return $this->render('index');
	}

	public function behaviors () {
		return [
			'contentNegotiator' => [
				'class' => ContentNegotiator::class,
				'only' => [ 'game' ],
				'formats' => [
					'application/json' => Response::FORMAT_JSON
				],
			],
		];
	}

	public function actionGame () {
		$amount = abs(Yii::$app->request->post('amount', 0));
		try {
			if ($amount < ThimblesModel::MIN_AMOUNT) {
				throw new BadRequestHttpException(Yii::t('casino', 'Minimum stake: {sum}', ['sum' => ThimblesModel::MIN_AMOUNT]));
			}
			if (Yii::$app->user->getIsGuest()) {
				throw new UnauthorizedHttpException(Yii::t('casino', 'You must login first'));
			}
			if((Yii::$app->user->identity->purse_amount < $amount)) {
				throw new BadRequestHttpException(Yii::t('casino', 'Not enough funds'));
			}
			if (!($bet = $this->_betsComponent->place(Yii::$app->user->id, $amount, 0, 'thimbles'))) {
				throw new ServerErrorHttpException(Yii::t('casino', 'Bet failed'));
			}
			$model = new ThimblesModel([
				'user_id' => Yii::$app->user->id,
				'user_bet_type' => (int)Yii::$app->request->post('bet_type'),
				'amount_bet' => (float)$amount,
				'game_type' => (int)Yii::$app->request->post('game_type'),
			]);
			if ($model->play()) {
				if ($model->status == ThimblesModel::STATUS_VICTORY) {
					$this->_betsComponent->victory($bet, $model->amount_win);
				} else {
					$this->_betsComponent->defeat($bet);
				}
				return [
					'success' => true,
					'result' => +$model->status,
					'amount_win' => $model->amount_win,
					'bet_type' => $model->bet_type,
					'message' => ($model->status == ThimblesModel::STATUS_VICTORY) ? Yii::t('casino', 'You have won: {sum}', ['sum' => $model->amount_win]) : Yii::t('casino', 'Loss. Try again!'),
					'show' => $this->showResult($model),
				];
			} else {
				throw new ServerErrorHttpException($model->getFirstErrors()[0] ?? Yii::t('casino', 'Bet failed'));
			}
		} catch (\Throwable $ex) {
			if($ex instanceof HttpException) {
				Yii::$app->response->statusCode = $ex->statusCode;
			} else {
				Yii::error($ex);
			}
			if(isset($bet)) {
				$this->_betsComponent->revert($bet);
			}
			Yii::$app->response->content = json_encode([
				'success' => false,
				'message' => $ex->getMessage()
			]);
			return Yii::$app->response->send();
		}
	}

	private function showResult (ThimblesModel $game) {
		$show = [];
		/* one ball */
		if ($game->game_type == ThimblesModel::GAME_TYPE_ONE_BALL) {
			$show['left'] = ($game->bet_type == 1) ? 'open_ball_1' : 'open_1';
			$show['center'] = ($game->bet_type == 2) ? 'open_ball_2' : 'open_2';
			$show['right'] = ($game->bet_type == 3) ? 'open_ball_3' : 'open_3';
		} else {
			/* two balls */
			$show['left'] = ($game->bet_type != 1) ? 'open_ball_1' : 'open_1';
			$show['center'] = ($game->bet_type != 2) ? 'open_ball_2' : 'open_2';
			$show['right'] = ($game->bet_type != 3) ? 'open_ball_3' : 'open_3';
		}
		return $show;
	}

}

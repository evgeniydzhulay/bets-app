<?php

namespace frontend\modules\casino\controllers;

use common\modules\casino\Module;
use yii\web\Controller;

/**
 * Class IndexController
 * @package frontend\modules\casino\controllers
 *
 * @property-read Module $module
 */
class IndexController extends Controller {

	public function actionIndex() {
		return $this->render('index', [
			'games' => $this->module->getGamesList()
		]);
	}
}
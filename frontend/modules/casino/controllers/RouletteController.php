<?php

namespace frontend\modules\casino\controllers;

use common\modules\casino\models\RouletteModel;
use Yii;
use common\modules\casino\components\BetsComponent;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\web\HttpException;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;

class RouletteController extends Controller {

	/** @var BetsComponent */
	protected $_betsComponent;

	public function __construct ($id, $module, $config = array()) {
		$this->_betsComponent = Yii::createObject(BetsComponent::class);
		return parent::__construct($id, $module, $config);
	}

	public function behaviors () {
		return [
			'contentNegotiator' => [
				'class' => ContentNegotiator::class,
				'only' => [ 'game' ],
				'formats' => [
					'application/json' => Response::FORMAT_JSON
				],
			],
		];
	}

	public function actionIndex () {
		return $this->render('index');
	}

	public function actionGame() {
		try {
			if (Yii::$app->user->getIsGuest()) {
				throw new UnauthorizedHttpException(Yii::t('casino', 'You must login first'));
			}
			$model = new RouletteModel([
				'user_id' => Yii::$app->user->id,
				'bets' => Yii::$app->request->post('bets', []),
			]);
			if ($model->play()) {
				if($model->amount_win > 0) {
					$this->_betsComponent->victory($model->bet, $model->amount_win);
				} else {
					$this->_betsComponent->defeat($model->bet);
				}
				return [
					'result' => +$model->bet_result,
					'amount' => $model->amount_win,
				];
			} else {
				throw new BadRequestHttpException($model->getFirstErrors()[0] ?? Yii::t('casino', 'Bet failed'));
			}
		} catch (\Throwable $ex) {
			if($ex instanceof HttpException) {
				Yii::$app->response->statusCode = $ex->statusCode;
			} else {
				Yii::error($ex);
			}
			if(isset($bet)) {
				$this->_betsComponent->revert($bet);
			}
			Yii::$app->response->content = json_encode([
				'success' => false,
				'message' => $ex->getMessage()
			]);
			return Yii::$app->response->send();
		}
	}
}
<?php

namespace frontend\modules\user\forms;

use common\modules\user\forms\RegistrationForm;
use common\modules\user\helpers\UserPassword;

class RegistrationPhoneForm extends RegistrationForm {

	public $phoneCode;

	public function formName () {
		return 'reg-phone';
	}

	public function rules () {
		return parent::rules() + [
			'currencyRequired' => ['currency', 'required'],
			'phoneRequired' => ['phone', 'required'],
		];
	}

	public function scenarios () {
		return [
			self::SCENARIO_DEFAULT => [
				'phone', 'currency', 'bonus', 'promocode', 'agree'
			]
		];
	}

	public function register () {
		$this->password = UserPassword::generate(8);
		if($result = parent::register()) {
			return $result;
		}
		return false;
	}
}
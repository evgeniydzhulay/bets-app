<?php

namespace frontend\modules\user\forms;

use common\modules\user\forms\RegistrationForm;

class RegistrationEmailForm extends RegistrationForm {

	public $password_repeat;

	public function formName () {
		return 'reg-email';
	}

	public function rules () {
		return parent::rules() + [
			'emailRequired' => ['email', 'required'],
			'currencyRequired' => ['currency', 'required'],
			'namesRequired' => [['name', 'surname'], 'required'],
			['password_repeat', 'required'],
			['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => \Yii::t('user', "Passwords don't match")],
		];
	}

	public function attributeLabels () {
		return parent::attributeLabels() + [
			'password_repeat' => \Yii::t('user', 'Repeat password')
		];
	}
}
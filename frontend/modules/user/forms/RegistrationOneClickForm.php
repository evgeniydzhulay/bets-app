<?php

namespace frontend\modules\user\forms;

use common\modules\user\forms\RegistrationForm;
use common\modules\user\helpers\UserPassword;

class RegistrationOneClickForm extends RegistrationForm {

	public function formName () {
		return 'reg-oneclick';
	}

	public function rules () {
		return parent::rules() + [
			'currencyRequired' => ['currency', 'required'],
		];
	}

	public function scenarios () {
		return [
			self::SCENARIO_DEFAULT => [
				'country', 'currency', 'bonus', 'promocode', 'agree'
			]
		];
	}

	public function register () {
		$this->password = UserPassword::generate(8);
		if($result = parent::register()) {
			return $result;
		}
		return false;
	}
}
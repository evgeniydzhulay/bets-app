<?php

namespace frontend\modules\user\controllers;

use common\modules\user\forms\LoginForm;
use Yii;
use yii\helpers\Url;
use frontend\modules\user\forms\RegistrationEmailForm;
use frontend\modules\user\forms\RegistrationOneClickForm;
use frontend\modules\user\forms\RegistrationPhoneForm;
use yii\web\Response;

class AuthController extends \common\modules\user\controllers\AuthController {


	/**
	 * Displays the login page.
	 *
	 * @return string|Response
	 * @throws \yii\base\ExitException
	 * @throws \yii\base\InvalidConfigException
	 */
	public function actionLogin() {
		if (Yii::$app->request->isAjax && Yii::$app->request->isGet) {
			$model = Yii::createObject(LoginForm::class);
			return $this->renderAjax('loginPopup', [
				'model' => $model,
			]);
		}
		return parent::actionLogin();
	}

	public function actionRegister () {
		if (!Yii::$app->user->isGuest) {
			$this->goHome();
		}
		$modelPhone = new RegistrationPhoneForm();
		$this->performAjaxValidation($modelPhone);
		if ($modelPhone->load(Yii::$app->request->post()) && $modelPhone->register()) {
			$eventPhone = $this->getFormEvent($modelPhone);
			$this->trigger(self::EVENT_AFTER_REGISTER, $eventPhone);
			Yii::$app->session->addFlash('info', Yii::t('user', 'Your account has been created.'));
			return $this->render('register-phone', [
				'id' => $modelPhone->getUser()->id,
				'phone' => $modelPhone->getUser()->phone,
				'password' => $modelPhone->password,
			]);
		}

		$modelOneClick = new RegistrationOneClickForm();
		$this->performAjaxValidation($modelOneClick);
		if ($modelOneClick->load(Yii::$app->request->post()) && $modelOneClick->register()) {
			$eventOneClick = $this->getFormEvent($modelOneClick);
			$this->trigger(self::EVENT_AFTER_REGISTER, $eventOneClick);
			Yii::$app->session->addFlash('info', Yii::t('user', 'Your account has been created.'));
			return $this->render('register-oneclick', [
				'id' => $modelOneClick->getUser()->id,
				'password' => $modelOneClick->password,
			]);
		}

		$modelEmail = new RegistrationEmailForm();
		$this->performAjaxValidation($modelEmail);
		if ($modelEmail->load(Yii::$app->request->post()) && $modelEmail->register()) {
			$eventEmail = $this->getFormEvent($modelEmail);
			$this->trigger(self::EVENT_AFTER_REGISTER, $eventEmail);
			Yii::$app->session->addFlash('info', Yii::t('user', 'Your account has been created and a message with further instructions has been sent to your email'));
			return $this->render('register-email');
		}

		return $this->render('register', [
			'modelPhone' => $modelPhone,
			'modelOneClick' => $modelOneClick,
			'modelEmail' => $modelEmail,
			'countries' => Yii::$app->get('locations')->getCountries(),
			'currencies' => Yii::$app->get('currencies')->getlist(),
			'phonecodes' => Yii::$app->get('locations')->getCountryPhoneCodes(),
		]);
	}
}
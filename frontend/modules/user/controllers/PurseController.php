<?php

namespace frontend\modules\user\controllers;

use common\modules\user\models\UserApplicationModel;
use common\modules\user\models\UserModel;
use Yii;
use yii\web\Response;
use yii\web\UnauthorizedHttpException;
use yii\web\User;

class PurseController extends \common\modules\user\controllers\PurseController {

	public function actionDepositRedirect($key) {
		$token = UserApplicationModel::getMobileToken($key);
		if($token) {
			$identity =  UserModel::find()->andWhere(['id' => $token['user_id']])->limit(1)->one();
			if ($identity !== null) {
				Yii::$app->user->login($identity);
				$token->trigger(User::EVENT_AFTER_LOGIN);
				return $this->redirect(['purse/deposit']);
			}
		}
		throw new UnauthorizedHttpException();
	}

	public function actionGetAmount() {
		Yii::$app->response->format = Response::FORMAT_JSON;
		/** @var UserModel $user */
		$user = Yii::$app->user->identity;
		return [
			'amount' => $user->purse_amount
		];
	}

	public function actionSuccess() {
		return $this->render('success');
	}

	public function actionFailed() {
		return $this->render('failed');
	}
}
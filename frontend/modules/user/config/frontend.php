<?php

use frontend\modules\user\controllers\AuthController;
use common\modules\user\models\UserModel;
use frontend\modules\user\controllers\PurseController;

return [
	'modules' => [
		'user' => [
			'controllerMap' => [
				'auth' => AuthController::class,
				'purse' => PurseController::class,
			],
		],
	],
	'components' => [
		'user' => [
			'enableAutoLogin' => true,
			'loginUrl' => ['/user/auth/login'],
			'identityClass' => UserModel::class,
		],
		'view' => [
			'theme' => [
				'pathMap' => [
					'@common/modules/user/views' => '@frontend/modules/user/views',
				],
			],
		],
	]
];

<?php
$this->title = Yii::t('user', 'Sign in');
echo '<div class="popup-close" data-dismiss="modal"><i class="fas fa-times"></i></div>';
echo $this->render('loginForm', [
	'model' => $model,
]);
<?php

use frontend\modules\user\forms\RegistrationEmailForm;
use frontend\modules\user\forms\RegistrationOneClickForm;
use frontend\modules\user\forms\RegistrationPhoneForm;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/**
 * @var $countries array
 * @var $currencies array
 * @var $phonecodes array
 * @var $modelPhone RegistrationPhoneForm
 * @var $modelOneClick RegistrationOneClickForm
 * @var $modelEmail RegistrationEmailForm
 */
?>
<div class="profile-heading text-center">
    <?= Yii::t('user', 'Welcome'); ?>
</div>
<div class="profile-content registration-tabs container-fluid">
    <div class="col-md-8 col-md-offset-2">
	<ul class="nav nav-tabs registration-tabs-links" role="tablist">
		<li role="presentation" class="active">
			<a href="#phone" aria-controls="phone" role="tab" data-toggle="tab">
                <?= Yii::t('user','By phone number'); ?>
            </a>
		</li>
		<li role="presentation">
			<a href="#oneclick" aria-controls="oneclick" role="tab" data-toggle="tab">
				<?= Yii::t('user','One click'); ?>
            </a>
		</li>
		<li role="presentation">
			<a href="#email" aria-controls="email" role="tab" data-toggle="tab">
				<?= Yii::t('user','By email'); ?>
            </a>
		</li>
	</ul>
	<div class="tab-content registration-tabs-list">
		<div role="tabpanel" class="tab-pane active registration-tabs-phone" id="phone">
			<?php
			$formPhone = ActiveForm::begin([
				'id' => 'registration-form-byphone',
				'enableAjaxValidation' => true,
				'enableClientValidation' => true,
                'fieldConfig' => [
                    'template' => "{label}\n{error}\n{beginWrapper}\n{input}\n{hint}\n{endWrapper}"
                ]
			]);
			?>
            <div class="row">
                <div class="col-md-6">
                    <?= $formPhone->field($modelPhone, 'phone') ?>
                </div>
                <div class="col-md-6">
	                <?= $formPhone->field($modelPhone, 'currency')->widget(Select2::class, [
		                'data' => $currencies,
	                ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
	                <?= $formPhone->field($modelPhone, 'bonus')->dropDownList($modelPhone->bonusesList()) ?>
                </div>
                <div class="col-md-6">
	                <?= $formPhone->field($modelPhone, 'promocode') ?>
                </div>
            </div>
            <div class="text-center login-form-remember checkbox-branded">
                <?= $formPhone->field($modelPhone, 'agree', [
	                'showRequiredIndicator' => false,
                ])->checkbox([
                    'template' => '{input}{label}{error}'
                ]); ?>
            </div>
            <div class="text-center">
				<?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-red']) ?>
            </div>
			<?php ActiveForm::end(); ?>
		</div>
		<div role="tabpanel" class="tab-pane registration-tabs-oneclick" id="oneclick">
			<?php
			$formOneClick = ActiveForm::begin([
				'id' => 'registration-form-byoneclick',
				'enableAjaxValidation' => true,
				'enableClientValidation' => true,
			]);
			?>
            <div class="row">
                <div class="col-md-6">
	                <?= $formOneClick->field($modelOneClick, 'country')->widget(Select2::class, [
		                'data' => $countries,
	                ]) ?>
                </div>
                <div class="col-md-6">
	                <?= $formOneClick->field($modelOneClick, 'currency')->widget(Select2::class, [
		                'data' => $currencies,
	                ]) ?>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <?= $formOneClick->field($modelOneClick, 'bonus')->dropDownList($modelPhone->bonusesList()) ?>
                </div>
                <div class="col-md-6">
	                <?= $formOneClick->field($modelOneClick, 'promocode') ?>
                </div>
            </div>
            <div class="text-center login-form-remember checkbox-branded">
	            <?= $formOneClick->field($modelOneClick, 'agree', [
		            'showRequiredIndicator' => false,
	            ])->checkbox([
		            'template' => '{input}{label}{error}'
	            ]); ?>
            </div>
            <div class="text-center">
			    <?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-red']) ?>
            </div>
			<?php ActiveForm::end(); ?>
		</div>
		<div role="tabpanel" class="tab-pane registration-tabs-email" id="email">
			<?php
			$formEmail = ActiveForm::begin([
				'id' => 'registration-form-byemail',
				'enableAjaxValidation' => true,
				'enableClientValidation' => true,
				'fieldConfig' => [
					'template' => "{label}\n{error}\n{beginWrapper}\n{input}\n{hint}\n{endWrapper}"
				]
			]);
			?>
            <ul class="registration-tabs-steps">
                <li id="email-step1" class="active">1</li>
                <li id="email-step2">2</li>
                <li id="email-step3">3</li>
            </ul>
            <div class="email-screen email-screen1 active">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
	                    <?= $formEmail->field($modelEmail, 'name') ?>
	                    <?= $formEmail->field($modelEmail, 'surname') ?>
	                    <?= $formEmail->field($modelEmail, 'currency')->widget(Select2::class, [
		                    'data' => $currencies,
	                    ]) ?>
                        <div class="text-center">
		                    <?= Html::button(Yii::t('user', 'Next'), [
                                'class' => 'btn btn-red',
                                'onclick' => 'next()'
                            ]) ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="email-screen email-screen2">
                <div class="row">
                    <div class="col-md-6">
			            <?= $formEmail->field($modelEmail, 'password')->passwordInput() ?>
                    </div>
                    <div class="col-md-6">
		                <?= $formEmail->field($modelEmail, 'password_repeat')->passwordInput() ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
			            <?= $formEmail->field($modelEmail, 'email') ?>
                    </div>
                    <div class="col-md-6">
			            <?= $formEmail->field($modelEmail, 'country')->widget(Select2::class, [
				            'data' => $countries,
			            ]) ?>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
			            <?= $formEmail->field($modelEmail, 'bonus')->dropDownList($modelPhone->bonusesList()) ?>
                    </div>
                    <div class="col-md-6">
			            <?= $formEmail->field($modelEmail, 'promocode') ?>
                    </div>
                </div>
                <div class="text-center login-form-remember checkbox-branded">
		            <?= $formEmail->field($modelEmail, 'agree', [
			            'showRequiredIndicator' => false,
		            ])->checkbox([
			            'template' => '{input}{label}{error}'
		            ]); ?>
                </div>
                <div class="text-center">
		            <?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-red']) ?>
                </div>
            </div>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
    </div>
</div>
<script>
    function next() {
        var form = jQuery('#registration-form-byemail');
        form.one('afterValidate', (res) => {
            console.log(res);
        });
        form.yiiActiveForm('validateAttribute', 'reg-email-name');
        form.yiiActiveForm('validateAttribute', 'reg-email-surname');
        form.yiiActiveForm('validateAttribute', 'reg-email-currency');
        setTimeout(function() {
            if (form.find('.email-screen1 .has-error').length === 0 ) {
                form.find('#email-step2').addClass('active');
                form.find('.email-screen1').removeClass('active');
                form.find('.email-screen2').addClass('active');
            }
        }, 200);
    }
</script>
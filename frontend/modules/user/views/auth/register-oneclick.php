<?php

/* @var $this \yii\web\View */
/* @var $id int */
/* @var $password string */

?>

<div class="profile-heading text-center">
	<?= Yii::t('user', 'Welcome'); ?>
</div>
<div class="profile-content registration-tabs-result">
    <div class="box">
        <div class="box-header">
            <?= Yii::t('user', 'Please save this credentials to login next time, or change account settings.'); ?>
        </div>
        <div class="box-body">
            <div class="registration-tabs-result-id">
                <span>id</span> <?= $id; ?>
            </div>
            <div class="registration-tabs-result-password">
                <span><?= Yii::t('user', 'password'); ?></span> <?= $password; ?>
            </div>
        </div>
    </div>
</div>

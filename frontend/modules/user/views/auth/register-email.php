<?php

use frontend\modules\user\forms\RegistrationEmailForm;
use frontend\modules\user\forms\RegistrationOneClickForm;
use frontend\modules\user\forms\RegistrationPhoneForm;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/**
 * @var $countries array
 * @var $currencies array
 * @var $phonecodes array
 * @var $modelPhone RegistrationPhoneForm
 * @var $modelOneClick RegistrationOneClickForm
 * @var $modelEmail RegistrationEmailForm
 */
?>
<div class="profile-heading text-center">
	<?= Yii::t('user', 'Welcome'); ?>
</div>
<div class="profile-content registration-tabs container-fluid">
    <div class="col-md-8 col-md-offset-2">
        <ul class="nav nav-tabs registration-tabs-links">
            <li role="presentation">
                <a aria-controls="phone" role="tab" data-toggle="tab">
					<?= Yii::t('user','By phone number'); ?>
                </a>
            </li>
            <li role="presentation">
                <a aria-controls="oneclick" role="tab" data-toggle="tab">
					<?= Yii::t('user','One click'); ?>
                </a>
            </li>
            <li role="presentation" class="active">
                <a aria-controls="email" role="tab" data-toggle="tab">
					<?= Yii::t('user','By email'); ?>
                </a>
            </li>
        </ul>
        <div class="tab-content registration-tabs-list">
            <div role="tabpanel" class="tab-pane registration-tabs-email active" id="email">
                <ul class="registration-tabs-steps">
                    <li id="email-step1" class="active">1</li>
                    <li id="email-step2" class="active">2</li>
                    <li id="email-step3" class="active">3</li>
                </ul>
                <div class="email-screen active">
                    <div class="registration-tabs-email-success">
                        <?= Yii::t('user', 'Registration successful!'); ?>
                    </div>
                    <div class="registration-tabs-email-sent">
		                <?= Yii::t('user', 'Awesome, almost there. Now you need to click the confirmation link sent to your new email address'); ?>
                    </div>
                    <div class="registration-tabs-email-deposit text-center">
		                <?= Html::a(Yii::t('user', 'Make a deposit'), [
                            '/user/purse/deposit'
                        ], ['class' => 'btn btn-red']); ?>
                    </div>
                    <div class="text-center">
		                <?= Html::a(Yii::t('user', 'Back to home page'), '/', [
			                'class' => 'go-home'
		                ]); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php

return [
	'modules' => [
		'games' => [
			'controllerNamespace' => '\frontend\modules\games\controllers',
			'viewPath' => '@frontend/modules/games/views'
		],
	],

	'components' => [
		'urlManager' => [
			'rules' => [
				'/' => '/games/games/index',
				'/<type:(live|line)>' => '/games/games/index',
				'/<type:(live|line)>/sports/<id:\d+>' => '/games/games/index',
				'/<type:(live|line)>/tournaments/<id:\d+>' => '/games/games/index',
				'/game/<id:\d+>' => '/games/games/index',
			]
		]
	]
];

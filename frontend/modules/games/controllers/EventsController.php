<?php

namespace frontend\modules\games\controllers;

use common\modules\games\helpers\GameHelpers;
use common\modules\games\models\BetsItemsModel;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\EventParticipantsModel;
use common\modules\games\models\EventTournamentModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesModel;
use common\modules\games\models\GameParticipantsModel;
use common\modules\games\models\GameSportsModel;
use common\modules\games\queries\EventGameLocaleQuery;
use common\modules\games\queries\EventGameQuery;
use common\modules\games\queries\EventOutcomesQuery;
use common\modules\games\queries\EventParticipantsQuery;
use common\modules\games\queries\EventTournamentLocaleQuery;
use common\modules\games\queries\GameMarketsGroupsAdditionalQuery;
use common\modules\games\queries\GameMarketsGroupsLocaleQuery;
use common\modules\games\queries\GameMarketsGroupsQuery;
use common\modules\games\queries\GameMarketsLocaleQuery;
use common\modules\games\queries\GameMarketsQuery;
use common\modules\games\queries\GameOutcomesLocaleQuery;
use common\modules\games\queries\GameOutcomesQuery;
use common\modules\games\queries\GameParticipantsLocaleQuery;
use common\modules\games\queries\GameSportsLocaleQuery;
use common\modules\games\queries\GameSportsQuery;
use common\modules\games\search\EventGameSearch;
use Yii;
use yii\db\Expression;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class EventsController extends Controller {

	/** @inheritdoc */
	public function behaviors() {
		return [
			'contentNegotiator' => [
				'class' => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON
				],
			],
		];
	}

	public function init () {
		parent::init();
		Yii::$app->response->on(Response::EVENT_BEFORE_SEND, function($event) {
			GameHelpers::formatResponse($event);
		});
	}


	public function actionList(bool $live = false, array $ids = [], int $page = 1) {
		$query = Yii::createObject([
			'class' => EventGameSearch::class,
			'scenario' => EventGameSearch::SCENARIO_SEARCH
		])->search(Yii::$app->request->get())->query;
		/** @var $query EventGameQuery */
		$query->active()->live($live)->andFilterWhere([
			EventGameModel::tableName() . '.id' => $ids,
		])->andWhere([
			'>', 'outcomes_count', 0
		])->innerJoinWith([
			'sports' => function(GameSportsQuery $query) {
				return $query->active();
			},
			'sportsLocales sports_locales_en' => function(GameSportsLocaleQuery $query) {
				return $query->andOnCondition(['sports_locales_en.lang_code' => 'en']);
			},
			'eventGameLocales event_locales_en' => function(EventGameLocaleQuery $query) {
				return $query->andOnCondition(['event_locales_en.lang_code' => 'en']);
			},
			'tournamentLocales tournament_locales_en' => function(EventTournamentLocaleQuery $query) {
				return $query->andOnCondition(['tournament_locales_en.lang_code' => 'en']);
			},
		], false)->with([
			'eventParticipants' => function(EventParticipantsQuery $query) {
				$query->innerJoinWith([
					'participantLocale locale_en' => function(GameParticipantsLocaleQuery $query) {
						return $query->andOnCondition([
							'locale_en.lang_code' => 'en',
						]);
					},
				], false)->addSelect([
					EventParticipantsModel::tableName() . '.*',
					'name' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.name, locale_en.name)') : 'locale_en.name',
				]);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'participantLocale locale_tr' => function(GameParticipantsLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				}
			},
			'eventOutcomes' => function(EventOutcomesQuery $query) {
				return $query->innerJoinWith([
					'outcome' => function (GameOutcomesQuery $query) {
						return $query->active();
					},
					'market' => function (GameMarketsQuery $query) {
						return $query->active();
					}
				], false)->andWhere([
					GameOutcomesModel::tableName() . '.is_main' => 1,
					EventOutcomesModel::tableName() . '.is_hidden' => 0,
				])->select([
					EventOutcomesModel::tableName() . '.id',
					EventOutcomesModel::tableName() . '.game_id',
					EventOutcomesModel::tableName() . '.outcome_id',
					GameOutcomesModel::tableName() . '.short_title',
					EventOutcomesModel::tableName() . '.rate',

					EventOutcomesModel::tableName() . '.is_enabled',
					EventOutcomesModel::tableName() . '.is_hidden',
					EventOutcomesModel::tableName() . '.is_finished',
					EventOutcomesModel::tableName() . '.is_banned',
					'outcome_enabled' => GameOutcomesModel::tableName() . '.is_enabled',
				])->orderBy([
					GameMarketsModel::tableName() . '.sort_order' => SORT_DESC,
					GameOutcomesModel::tableName() . '.market_id' => SORT_ASC,
					GameOutcomesModel::tableName() . '.sort_order' => SORT_DESC,
					GameOutcomesModel::tableName() . '.id' => SORT_ASC,
				]);
			}
		])->addSelect([
			EventGameModel::tableName() . '.*',
			'bets_count' => BetsItemsModel::find()->select([
				'count' => new Expression('COUNT(*)')
			])->andWhere('{{bets_items}}.`game_id` = {{event_game}}.`id`'),
			'sport_image' => GameSportsModel::tableName() . '.image',
			'sport_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(sports_locales_tr.title, sports_locales_en.title)') : 'sports_locales_en.title',
			'event_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(event_locales_tr.title, event_locales_en.title)') : 'event_locales_en.title',
			'tournament_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(tournament_locales_tr.title, tournament_locales_en.title)') : 'tournament_locales_en.title',
			'tournament_description' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(tournament_locales_tr.description, tournament_locales_en.description)') : 'tournament_locales_en.description',
		])->orderBy(['sort_order' => SORT_DESC])->limit(50)->offset(($page - 1) * 50);
		if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$query->joinWith([
				'sportsLocales sports_locales_tr' => function(GameSportsLocaleQuery $query) {
					return $query->andOnCondition([
						'sports_locales_tr.lang_code' => Yii::$app->language,
						'sports_locales_tr.is_active' => 1,
					]);
				},
				'eventGameLocales event_locales_tr' => function(EventGameLocaleQuery $query) {
					return $query->andOnCondition([
						'event_locales_tr.lang_code' => Yii::$app->language,
						'event_locales_tr.is_active' => 1,
					]);
				},
				'tournamentLocales tournament_locales_tr' => function(EventTournamentLocaleQuery $query) {
					return $query->andOnCondition([
						'tournament_locales_tr.lang_code' => Yii::$app->language,
						'tournament_locales_tr.is_active' => 1,
					]);
				},
			], false);
		}
		$items = [];
		foreach($query->asArray()->all() AS $model) {
			if(!isset($items[$model['tournament_id']])) {
				$items[$model['tournament_id']] = [
					'sports_id' => +$model['sports_id'],
					'sports_title' => $model['sport_title'] ?? '',
					'sports_image' => $model['sport_image'] ?? '',
					'tournament_id' => +$model['tournament_id'],
					'tournament_title' => $model['tournament_title'] ?? '',
					'tournament_events' => [],
				];
			}
			$items[$model['tournament_id']]['tournament_events'][] = [
				'id' => +$model['id'],
				'title' => $model['event_title'] ?? '',
				'start_date' => \date('Y-m-d', $model['starts_at']),
				'start_time' => \date('H:i', $model['starts_at']),
				'result' => $model['result'] ?? '',
				'outcomes_count' => +$model['outcomes_count'],
				'bets_count' => +$model['bets_count'],
				'is_finished' => !!$model['is_finished'],
				'participants' => ArrayHelper::map($model['eventParticipants'], 'participant_id', 'name'),
				'video_link' => $model['video_link'] ?? false,
				'outcomes' => ArrayHelper::getColumn($model['eventOutcomes'], function($outcome) {
					return [
						'id' => +$outcome['id'],
						'l' => $outcome['short_title'] ?? '',
						'r' => ($outcome['is_enabled'] && !$outcome['is_hidden']  && !$outcome['is_finished'] && !$outcome['is_banned']  && $outcome['outcome_enabled']) ? +$outcome['rate'] : 0,
					];
				}, false),
			];
		}
		return ['events' => \array_values($items)];
	}

	public function actionUpcoming(int $from = 0, int $to = 24) {
		/** @var $query EventGameQuery */
		$query = Yii::createObject([
			'class' => EventGameSearch::class,
			'scenario' => EventGameSearch::SCENARIO_SEARCH
		])->search([
			'starts_from' => time() + $from * 3600,
			'starts_to' => time() + $to * 3600,
		])->query;
		$query->active()->with([
			'eventParticipants' => function(EventParticipantsQuery $query) {
				$query->innerJoinWith([
					'participantLocale locale_en' => function(GameParticipantsLocaleQuery $query) {
						return $query->andOnCondition([
							'locale_en.lang_code' => 'en',
						]);
					},
				], false)->addSelect([
					EventParticipantsModel::tableName() . '.*',
					'name' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.name, locale_en.name)') : 'locale_en.name',
				]);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'participantLocale locale_tr' => function(GameParticipantsLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				}
			},
		])->addSelect([
			EventGameModel::tableName() . '.*',
			'sport_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ?
				new Expression('COALESCE(sports_locales_tr.title, sports_locales_en.title)') : 'sports_locales_en.title',
			'event_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ?
				new Expression('COALESCE(event_locales_tr.title, event_locales_en.title)') : 'event_locales_en.title',
		])->innerJoinWith([
			'sportsLocales sports_locales_en' => function(GameSportsLocaleQuery $query) {
				return $query->andOnCondition([
					'sports_locales_en.lang_code' => 'en',
				]);
			},
			'eventGameLocales event_locales_en' => function(EventGameLocaleQuery $query) {
				return $query->andOnCondition([
					'event_locales_en.lang_code' => 'en',
				]);
			},
		], false)->orderBy(['sort_order' => SORT_DESC])->andWhere(['>', 'outcomes_count', 0])->asArray()->limit(50);
		if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$query->joinWith([
				'sportsLocales sports_locales_tr' => function(GameSportsLocaleQuery $query) {
					return $query->andOnCondition([
						'sports_locales_tr.lang_code' => Yii::$app->language,
						'sports_locales_tr.is_active' => 1,
					]);
				},
				'eventGameLocales event_locales_tr' => function(EventGameLocaleQuery $query) {
					return $query->andOnCondition([
						'event_locales_tr.lang_code' => Yii::$app->language,
						'event_locales_tr.is_active' => 1,
					]);
				},
			], false);
		}
		return ['events' => \array_values(ArrayHelper::map($query->all(), 'tournament_id', function($model) {
			return [
				'id' => +$model['id'],
				'sports_id' => +$model['sports_id'],
				'sports_title' => $model['sport_title'] ?? '',
				'title' => $model['event_title'] ?? '',
				'start_date' => \date('Y-m-d', $model['starts_at']),
				'start_time' => \date('H:i', $model['starts_at']),
				'participants' => ArrayHelper::map($model['eventParticipants'], 'participant_id', 'name'),
			];
		}))];
	}

	public function actionTournaments () {
		$query = EventTournamentModel::find()->addSelect([
			EventTournamentModel::tableName() . '.*',
			'tournament_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ?
				new Expression('COALESCE(locale_tr.title, locale_en.title)') : 'locale_en.title',
			'tournament_description' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ?
				new Expression('COALESCE(locale_tr.description, locale_en.description)') : 'locale_en.description',
		])->visible()->andFilterWhere([
			'sports_id' => Yii::$app->request->get('sport', null)
		])->innerJoinWith([
			'eventTournamentLocales locale_en' => function (EventTournamentLocaleQuery $query) {
				$query->andFilterWhere([
					'sports_id' => Yii::$app->request->get('sport', null)
				])->andOnCondition([
					'locale_en.lang_code' => 'en',
				]);
			},
		], false)->asArray();
		if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$query->joinWith([
				'eventTournamentLocales locale_tr' => function(EventTournamentLocaleQuery $query) {
					return $query->andOnCondition([
						'locale_tr.lang_code' => Yii::$app->language,
						'locale_tr.is_active' => 1,
					]);
				},
			], false);
		}
		return [
			'tournaments' => array_values(ArrayHelper::map($query->all(), 'id', function($item) {
				return [
					'id' => +$item['id'],
					'sports_id' => +$item['sports_id'],
					'starts_at' => +$item['starts_at'],
					'sort_order' => +$item['sort_order'],
					'title' => $item['tournament_title'] ?? '',
					'description' => $item['tournament_description'] ?? '',
				];
			}))
		];
	}

	public function actionEvent (int $id) {
		$event = EventGameModel::find()->andWhere(['id' => $id])->visible()->with([
			'eventGameLocales' => function (EventGameLocaleQuery $query) {
				return $query->current();
			},
			'tournamentLocales' => function (EventTournamentLocaleQuery $query) {
				return $query->current();
			},
			'marketsGroups' => function (GameMarketsGroupsQuery $query) {
				$query->active()->orderBy([
					GameMarketsGroupsModel::tableName() . '.sort_order' => SORT_DESC,
					GameMarketsGroupsModel::tableName() . '.id' => SORT_ASC
				])->innerJoinWith([
					'gameMarketsGroupsLocales locale_en' => function(GameMarketsGroupsLocaleQuery $query) {
						return $query->andOnCondition([
							'locale_en.lang_code' => 'en',
						]);
					},
				], false)->addSelect([
					GameMarketsGroupsModel::tableName() . '.*',
					'title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.title, locale_en.title)') : 'locale_en.title',
				]);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'gameMarketsGroupsLocales locale_tr' => function(GameMarketsGroupsLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				}
			},
			'marketsGroups.additionalMarkets' => function (GameMarketsGroupsAdditionalQuery $query) {
				return $query->active();
			},
			'markets' => function (GameMarketsQuery $query) {
				$query->active()->orderBy([
					GameMarketsModel::tableName() . '.sort_order' => SORT_DESC,
					GameMarketsModel::tableName() . '.id' => SORT_ASC
				])->innerJoinWith([
					'gameMarketsLocales locale_en' => function(GameMarketsLocaleQuery $query) {
						return $query->andOnCondition([
							'locale_en.lang_code' => 'en',
						]);
					},
				], false)->addSelect([
					GameMarketsModel::tableName() . '.*',
					'title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.title, locale_en.title)') : 'locale_en.title',
				]);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'gameMarketsLocales locale_tr' => function(GameMarketsLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				};
			},
			'eventOutcomes' => function(EventOutcomesQuery $query) {
				$query->visible()->innerJoinWith(['outcome'], false)->select([
					'id' => EventOutcomesModel::tableName() . '.id',
					'game_id' => EventOutcomesModel::tableName() . '.game_id',
					'market_id' => EventOutcomesModel::tableName() . '.market_id',
					'group_id' => EventOutcomesModel::tableName() . '.group_id',
					'outcome_id' => GameOutcomesModel::tableName() . '.id',
					'is_enabled' =>  EventOutcomesModel::tableName() . '.is_enabled',
					'is_finished' =>  EventOutcomesModel::tableName() . '.is_finished',
					'sort_order' =>  GameOutcomesModel::tableName() . '.sort_order',
					'rate' => EventOutcomesModel::tableName() . '.rate',
					'title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.title, locale_en.title)') : 'locale_en.title',
				])->orderBy([
					GameOutcomesModel::tableName() . '.sort_order' => SORT_DESC,
					GameOutcomesModel::tableName() . '.id' => SORT_ASC
				])->andWhere([
					GameOutcomesModel::tableName() . '.is_enabled' => true,
					EventOutcomesModel::tableName() . '.is_finished' => false,
				])->innerJoinWith([
					'outcomeLocale locale_en' => function(GameOutcomesLocaleQuery $query) {
						return $query->andOnCondition([
							'locale_en.lang_code' => 'en',
						]);
					},
				], false);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'outcomeLocale locale_tr' => function(GameOutcomesLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				}
			},
			'eventParticipants' => function(EventParticipantsQuery $query) {
				$query->innerJoinWith([
					'participant',
					'participantLocale locale_en' => function(GameParticipantsLocaleQuery $query) {
						return $query->andOnCondition([
							'locale_en.lang_code' => 'en',
						]);
					},
				], false)->addSelect([
					GameParticipantsModel::tableName() . '.image',
					EventParticipantsModel::tableName() . '.*',
					'name' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.name, locale_en.name)') : 'locale_en.name',
				])->orderBy([
					EventParticipantsModel::tableName() . '.sort_order' => SORT_ASC,
					EventParticipantsModel::tableName() . '.id' => SORT_ASC,
				]);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'participantLocale locale_tr' => function(GameParticipantsLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				}
			},
			'result',
		])->asArray()->limit(1)->one();
		if(!$event) {
			throw new NotFoundHttpException();
		}
		$eventLocale = GameHelpers::getLocalization($event['eventGameLocales']);
		$participants = ArrayHelper::getColumn($event['eventParticipants'], function($item) {
			return [
				'id' => +$item['participant_id'],
				'sort_order' => +$item['sort_order'],
				'name' => $item['name'] ?? '',
				'image' => !empty($item['image']) ? Yii::getAlias("@web/frontend/uploads/games/{$item['image']}") : '',
			];
		}, false);
		$participantReplacements = [];
		foreach($participants AS $k => $v) {
			$participantReplacements[$k + 1] = $v['name'];
		}
		$timer = time() - $event['starts_at'];
		$resultsExtended = json_decode($event['result']['result_extended'], true) ?? [];
		if(count($resultsExtended) === 0 ) {
			$resultsExtended = new \stdClass();
		}
		return [
			'id' => +$event['id'],
			'sports_id' => +$event['sports_id'],
			'tournament_id' => +$event['tournament_id'],
			'tournament_title' => GameHelpers::getLocalization($event['tournamentLocales'])['title'] ?? '',
			'starts_at' => +$event['starts_at'],
			'starts_date' => \date('Y-m-d', $event['starts_at']),
			'starts_time' => \date('H:i', $event['starts_at']),
			'is_live' => !!$event['is_live'],
			'is_finished' => !!$event['is_finished'],
			'is_enabled' => !!$event['is_enabled'],
			'is_hidden' => !!$event['is_hidden'],
			'is_banned' => !!$event['is_banned'],
			'title' => $eventLocale['title'] ?? '',
			'description' => $eventLocale['description'] ?? '',
			'meta_keywords' => $eventLocale['meta_keywords'] ?? '',
			'meta_description' => $eventLocale['meta_description'] ?? '',
			'participants' => $participants,
			'video_link' => $event['video_link'] ?? false,
			'game_period' => Yii::t('results', $event['result']['game_period'] ?? ''),
			'result' => $event['result']['result_simple'] ?? '0:0',
			'result_extended' => $resultsExtended,
			'timer' => !!$event['is_live'] ? (intval($timer / 60) . gmdate(":s", $timer % 60)) : '00:00',
			'markets' => ArrayHelper::getColumn($event['markets'], function($item) {
				return [
					'id' => +$item['id'],
					'is_main' => !!$item['is_main'],
					'group_id' => +$item['group_id'],
					'sort_order' => +$item['sort_order'],
					'title' => $item['title'] ?? '',
				];
			}, false),
			'groups' => ArrayHelper::getColumn($event['marketsGroups'], function($item) {
				return [
					'id' => +$item['id'],
					'sort_order' => +$item['sort_order'],
					'title' => $item['title'] ?? '',
					'additional' => ArrayHelper::getColumn($item['additionalMarkets'], function($item) {
						return [
							'market_id' => +$item['market_id'],
							'sort_order' => +$item['sort_order']
						];
					}, false),
				];
			}, false),
			'outcomes' => ArrayHelper::getColumn($event['eventOutcomes'], function($item) use ($participantReplacements) {
				return [
					'id' => +$item['id'],
					'market_id' => +$item['market_id'],
					'group_id' => +$item['group_id'],
					'is_enabled' => !!$item['is_enabled'],
					'is_finished' => !!$item['is_finished'],
					'sort_order' => +$item['sort_order'],
					'rate' => +$item['rate'],
					'title' => GameHelpers::replacingCodeWithName($item['title'] ?? '', $participantReplacements),
				];
			}, false),
		];
	}
}
<?php

namespace frontend\modules\games\controllers;

use common\modules\games\helpers\GameHelpers;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventTournamentModel;
use common\modules\games\models\GameSportsModel;
use common\modules\games\queries\EventGameLocaleQuery;
use common\modules\games\queries\EventGameQuery;
use common\modules\games\queries\EventTournamentLocaleQuery;
use common\modules\games\queries\GameSportsLocaleQuery;
use common\modules\games\queries\GameSportsQuery;
use common\modules\games\search\EventGameSearch;
use Yii;
use yii\db\Expression;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\Response;
use yii\web\View;

class GamesController extends Controller {

	/** @inheritdoc */
	public function behaviors () {
		return [
			'contentNegotiator' => [
				'class' => ContentNegotiator::class,
				'except' => ['index', 'results'],
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
		];
	}

	public function init () {
		parent::init();
		Yii::$app->response->on(Response::EVENT_BEFORE_SEND, function ($event) {
			GameHelpers::formatResponse($event);
		});
	}

	public function actionIndex () {
		$this->view->registerJs("window.socketGameParams = " . json_encode([
			'host' => Url::base(true),
			'path' => Yii::$app->params['socketGames.path'],
			'loggedIn' => !Yii::$app->user->isGuest,
			'currency' => !Yii::$app->user->isGuest ? Yii::$app->user->identity->purse_currency : 'RUR',
		]), View::POS_HEAD);
		return $this->render('index');
	}

	public function actionResults() {
		return $this->render('results');
	}

	public function actionResultsList(int $live = null) {
		$query = Yii::createObject([
			'class' => EventGameSearch::class,
			'scenario' => EventGameSearch::SCENARIO_SEARCH
		])->search(Yii::$app->request->get())->query;
		/** @var $query EventGameQuery */
		$query->active()->andWhere(['not', ['result' => null]])->andFilterWhere(['is_live' => $live])->andWhere([
			'>', 'outcomes_count', 0
		])->innerJoinWith([
			'sports' => function(GameSportsQuery $query) {
				return $query->active();
			},
			'sportsLocales sports_locales_en' => function(GameSportsLocaleQuery $query) {
				return $query->andOnCondition(['sports_locales_en.lang_code' => 'en']);
			},
			'eventGameLocales event_locales_en' => function(EventGameLocaleQuery $query) {
				return $query->andOnCondition(['event_locales_en.lang_code' => 'en']);
			},
			'tournamentLocales tournament_locales_en' => function(EventTournamentLocaleQuery $query) {
				return $query->andOnCondition(['tournament_locales_en.lang_code' => 'en']);
			},
		], false)->addSelect([
			EventGameModel::tableName() . '.*',
			'sport_image' => GameSportsModel::tableName() . '.image',
			'sport_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(sports_locales_tr.title, sports_locales_en.title)') : 'sports_locales_en.title',
			'event_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(event_locales_tr.title, event_locales_en.title)') : 'event_locales_en.title',
			'tournament_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(tournament_locales_tr.title, tournament_locales_en.title)') : 'tournament_locales_en.title',

		])->orderBy(['sort_order' => SORT_ASC])->asArray();
		if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$query->joinWith([
				'sportsLocales sports_locales_tr' => function(GameSportsLocaleQuery $query) {
					return $query->andOnCondition([
						'sports_locales_tr.lang_code' => Yii::$app->language,
						'sports_locales_tr.is_active' => 1,
					]);
				},
				'eventGameLocales event_locales_tr' => function(EventGameLocaleQuery $query) {
					return $query->andOnCondition([
						'event_locales_tr.lang_code' => Yii::$app->language,
						'event_locales_tr.is_active' => 1,
					]);
				},
				'tournamentLocales tournament_locales_tr' => function(EventTournamentLocaleQuery $query) {
					return $query->andOnCondition([
						'tournament_locales_tr.lang_code' => Yii::$app->language,
						'tournament_locales_tr.is_active' => 1,
					]);
				},
			], false);
		}
		$items = [];
		$sports = [];
		foreach($query->all() AS $model) {
			if(!isset($items[$model['tournament_id']])) {
				$items[$model['tournament_id']] = [
					'sports_id' => +$model['sports_id'],
					'sports_title' => $model['sport_title'] ?? '',
					'sports_image' => $model['sport_image'] ?? '',
					'tournament_id' => +$model['tournament_id'],
					'tournament_title' => $model['tournament_title'] ?? '',
					'tournament_events' => [],
				];
			}
			if(!isset($sports[$model['sports_id']])) {
				$sports[$model['sports_id']] = $model['sport_title'];
			}
			$items[$model['tournament_id']]['tournament_events'][] = [
				'id' => +$model['id'],
				'title' => $model['event_title'] ?? '',
				'start_date' => \date('Y-m-d', $model['starts_at']),
				'start_time' => \date('H:i', $model['starts_at']),
				'result' => $model['result'] ?? '',
				'live' => +$model['is_live'],
			];
		}
		return [
			'events' => \array_values($items),
			'sports' => $sports,
		];
	}

	public function actionSports (int $live = 1) {
		$query = EventTournamentModel::find()->addSelect([
			EventTournamentModel::tableName() . '.id',
			EventTournamentModel::tableName() . '.sports_id',
			EventTournamentModel::tableName() . '.flag',
			'sort_tournament' => EventTournamentModel::tableName() . '.sort_order',
			'sort_sport' => GameSportsModel::tableName() . '.sort_order',
			'image' => GameSportsModel::tableName() . '.image',
			'starts_at' => EventTournamentModel::tableName() . '.starts_at',
			'events_count' => new Expression('COUNT(*)'),
			'videos_count' => new Expression('COUNT(`event_game`.`video_link`)'),
			'outcomes_count' => EventGameModel::tableName() . '.outcomes_count',
			'sport_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(sport_locale_tr.title, sport_locale_en.title)') : 'sport_locale_en.title',
			'sport_description' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(sport_locale_tr.description, sport_locale_en.description)') : 'sport_locale_en.description',
			'tournament_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(tournament_locale_tr.title, tournament_locale_en.title)') : 'tournament_locale_en.title',
			'tournament_description' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(tournament_locale_tr.description, tournament_locale_en.description)') : 'tournament_locale_en.description',
		])->innerJoinWith([
			'eventGames' => function (EventGameQuery $query) use ($live) {
				return $query->active()->live($live);
			},
			'sports' => function (GameSportsQuery $query) {
				return $query->active();
			},
			'gameSportsLocales sport_locale_en' => function (GameSportsLocaleQuery $query) {
				return $query->andOnCondition([
					'sport_locale_en.lang_code' => 'en',
				]);
			},
			'eventTournamentLocales tournament_locale_en' => function (EventTournamentLocaleQuery $query) {
				return $query->andOnCondition([
					'tournament_locale_en.lang_code' => 'en',
				]);
			},
		], false)->groupBy([EventTournamentModel::tableName() . '.id'])->andWhere(['>', 'outcomes_count', 0]);
		if (\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$query->joinWith([
				'gameSportsLocales sport_locale_tr' => function (GameSportsLocaleQuery $query) {
					return $query->andOnCondition([
						'sport_locale_tr.lang_code' => Yii::$app->language,
						'sport_locale_tr.is_active' => 1,
					]);
				},
				'eventTournamentLocales tournament_locale_tr' => function (EventTournamentLocaleQuery $query) {
					return $query->andOnCondition([
						'tournament_locale_tr.lang_code' => Yii::$app->language,
						'tournament_locale_tr.is_active' => 1,
					]);
				},
			], false);
		}
		$sports = [];
		foreach ($query->active()->asArray()->all() AS $item) {
			if (!isset($sports[$item['sports_id']])) {
				$sports[$item['sports_id']] = [
					'id' => +$item['sports_id'],
					'image' => $item['image'],
					'title' => $item['sport_title'] ?? '',
					'sort_order' => +$item['sort_sport'],
					'description' => $item['sport_description'] ?? '',
					'tournaments' => [],
					'videos_count' => 0,
				];
			}
			$sports[$item['sports_id']]['videos_count'] += $item['videos_count'];
			$sports[$item['sports_id']]['tournaments'][] = [
				'id' => +$item['id'],
				'sports_id' => +$item['sports_id'],
				'starts_at' => +$item['starts_at'],
				'sort_order' => +$item['sort_tournament'],
				'title' => $item['tournament_title'] ?? '',
				'description' => $item['tournament_description'] ?? '',
				'events_count' => +$item['events_count'],
				'videos_count' => +$item['videos_count'],
				'flag' => $item['flag'],
			];
		}
		return [
			'sports' => \array_values($sports),
		];
	}
}
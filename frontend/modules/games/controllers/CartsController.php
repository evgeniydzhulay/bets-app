<?php

namespace frontend\modules\games\controllers;

use common\modules\games\helpers\GameHelpers;
use common\modules\games\models\CartsItemsModel;
use common\modules\games\models\CartsModel;
use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\EventParticipantsModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use common\modules\games\models\GameSportsModel;
use common\modules\games\queries\EventGameLocaleQuery;
use common\modules\games\queries\EventOutcomesQuery;
use common\modules\games\queries\EventParticipantsQuery;
use common\modules\games\queries\GameMarketsLocaleQuery;
use common\modules\games\queries\GameOutcomesLocaleQuery;
use common\modules\games\queries\GameParticipantsLocaleQuery;
use common\modules\user\models\UserModel;
use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\ContentNegotiator;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\Cookie;
use yii\web\ForbiddenHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\ServerErrorHttpException;

class CartsController extends Controller {

	/** @inheritdoc */
	public function behaviors () {
		return [
			'contentNegotiator' => [
				'class' => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON,
				],
			],
			'access' => [
				'class' => AccessControl::class,
				'denyCallback' => function () {
					throw new ForbiddenHttpException(Yii::t('yii', 'You are not allowed to perform this action.'));
				},
				'only' => ['place-bet'],
				'rules' => [
					[
						'allow' => true,
						'actions' => ['place-bet'],
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function init () {
		parent::init();
		Yii::$app->response->on(Response::EVENT_BEFORE_SEND, function ($event) {
			GameHelpers::formatResponse($event);
		});
	}

	/**
	 * View Cart
	 *
	 * @return array
	 */
	public function actionView () {
		if (!($cart = self::getCurrentCart(false))) {
			return [
				'success' => false,
				'items' => [],
			];
		}
		$query = CartsItemsModel::find()->addSelect([
			CartsItemsModel::tableName() . '.*',
			'game_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(game_locale_tr.title, game_locale_en.title)') : 'game_locale_en.title',
			'market_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(market_locale_tr.title, market_locale_en.title)') : 'market_locale_en.title',
			'outcome_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(outcome_locale_tr.title, outcome_locale_en.title)') : 'outcome_locale_en.title',

			'outcome_rate' => EventOutcomesModel::tableName() . '.rate',
			'outcome_enabled' => EventOutcomesModel::tableName() . '.is_enabled',
			'outcome_finished' => EventOutcomesModel::tableName() . '.is_finished',
			'outcome_banned' => EventOutcomesModel::tableName() . '.is_banned',
			'outcome_hidden' => EventOutcomesModel::tableName() . '.is_hidden',
			'game_finished' => EventGameModel::tableName() . '.is_finished',
			'game_resulted' => EventGameModel::tableName() . '.is_resulted',
			'sport_image' => GameSportsModel::tableName() . '.image'
		])->andWhere(['cart_id' => $cart->id])->with([
			'eventParticipants' => function(EventParticipantsQuery $query) {
				$query->innerJoinWith([
					'participantLocale locale_en' => function(GameParticipantsLocaleQuery $query) {
						return $query->andOnCondition(['locale_en.lang_code' => 'en']);
					},
				], false)->addSelect([
					EventParticipantsModel::tableName() . '.*',
					'name' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.name, locale_en.name)') : 'locale_en.name',
				]);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'participantLocale locale_tr' => function(GameParticipantsLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				}
			},
		])->innerJoinWith([
			'gameLocales game_locale_en' => function(EventGameLocaleQuery $query) {
				return $query->andOnCondition(['game_locale_en.lang_code' => 'en']);
			},
			'marketLocales market_locale_en' => function(GameMarketsLocaleQuery $query) {
				return $query->andOnCondition(['market_locale_en.lang_code' => 'en']);
			},
			'outcomeLocales outcome_locale_en' => function(GameOutcomesLocaleQuery $query) {
				return $query->andOnCondition(['outcome_locale_en.lang_code' => 'en']);
			},
			'sport', 'game', 'outcome',
		], false)->asArray();
		if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$query->joinWith([
				'gameLocales game_locale_tr' => function(EventGameLocaleQuery $query) {
					return $query->andOnCondition([
						'game_locale_tr.lang_code' => Yii::$app->language,
						'game_locale_tr.is_active' => 1,
					]);
				},
				'marketLocales market_locale_tr' => function(GameMarketsLocaleQuery $query) {
					return $query->andOnCondition([
						'market_locale_tr.lang_code' => Yii::$app->language,
						'market_locale_tr.is_active' => 1,
					]);
				},
				'outcomeLocales outcome_locale_tr' => function(GameOutcomesLocaleQuery $query) {
					return $query->andOnCondition([
						'outcome_locale_tr.lang_code' => Yii::$app->language,
						'outcome_locale_tr.is_active' => 1,
					])->via('outcome', function(EventOutcomesQuery $query) {
						$query->alias('outcome_tr');
					});
				},
			], false);
		}
		return [
			'id' => $cart->id,
			'bet' => $cart->amount,
			'on_change' => $cart->on_change,
			'minimal' => CartsModel::getMinimalBet(),
			'maximal' => CartsModel::getMaximalBet(),
			'items' => \array_values(ArrayHelper::map($query->all(), 'id', function ($item) {
				$participants = \array_values(ArrayHelper::map($item['eventParticipants'], 'id', function($item) {
					return [
						'id' => +$item['participant_id'],
						'name' => $item['name'] ?? '',
					];
				}));
				$participantReplacements = [];
				foreach($participants AS $k => $v) {
					$participantReplacements[$k + 1] = $v['name'];
				}
				return [
					'id' => +$item['id'],
					'game_id' => +$item['game_id'],
					'game_title' => $item['game_title'] ?? '',
					'outcome_id' => +$item['outcome_id'],
					'outcome_title' => GameHelpers::replacingCodeWithName($item['outcome_title'], $participantReplacements),
					'market_id' => +$item['market_id'],
					'market_title' => $item['market_title'] ?? '',
					'rate' => +$item['rate'],
					'rate_now' => +$item['outcome_rate'],
					'is_enabled' => +$item['outcome_enabled'],
					'is_finished' => max(+$item['outcome_finished'], +$item['game_finished'], +$item['game_resulted']),
					'is_banned' => +$item['outcome_banned'],
					'is_hidden' => +$item['outcome_hidden'],
					'sport_image' => $item['sport_image'],
				];
			})),
		];
	}

	/**
	 * Get current cart item based on cookies
	 *
	 * @param bool $throwException
	 * @return CartsModel
	 */
	public static function getCurrentCart ($throwException = true) {
		$cart_id = Yii::$app->request->cookies->getValue('cart_id', false);
		$key = Yii::$app->request->cookies->getValue('key', false);
		if (empty($cart_id) || empty($key)) {
			if ($throwException) {
				throw new NotFoundHttpException(Yii::t('games', 'Your cart is empty'));
			}
			return null;
		}
		return self::findCart(['id' => $cart_id, 'key' => $key], $throwException);
	}

	/**
	 * @param array $params
	 * @param bool $throwException
	 * @return CartsModel
	 */
	public static function findCart ($params, $throwException = true) {
		$cart = CartsModel::find()->andWhere($params)->limit(1)->one();
		if (!$cart && $throwException) {
			throw new NotFoundHttpException(Yii::t('games', 'Your cart is empty'));
		}
		return $cart;
	}

	/**
	 * Edit cart
	 * @return array
	 */
	public function actionEdit () {
		$cart = self::getCurrentCart(false) ?? $this->createCart();
		$cart->scenario = CartsModel::SCENARIO_UPDATE;
		$params = Yii::$app->request->get();
		$success = ($cart->load($params, '') && $cart->save());
		return [
			'success' => $success,
			'errors' => $cart->errors,
		] + ($success ? [
			'bet' => +$cart->amount,
			'on_change' => +$cart->on_change,
		] : []);
	}

	protected function createCart () {
		$cart = new CartsModel();
		$cart->scenario = CartsModel::SCENARIO_CREATE;
		$cart->amount = CartsModel::getMinimalBet();
		$cart->on_change = CartsModel::ON_CHANGE_AGREE;
		$cart->key = Yii::$app->security->generateRandomString(64);
		if ($cart->save()) {
			$expires = time() + Yii::$app->getModule('games')->getCartExpiration();
			Yii::$app->response->cookies->add(new Cookie([
				'name' => 'cart_id', 'value' => $cart->id,
				'expire' => $expires,
			]));
			Yii::$app->response->cookies->add(new Cookie([
				'name' => 'key', 'value' => $cart->key,
				'expire' => $expires,
			]));
			return $cart;
		}
		throw new ServerErrorHttpException(Yii::t('bets', 'Error while cart initialization'));
	}

	/**
	 * Add item to cart
	 *
	 * @param int $outcome
	 * @return array
	 */
	public function actionItemAdd (int $outcome) {
		$cart = self::getCurrentCart(false) ?? $this->createCart();
		$item = $cart->addItem($outcome);
		if (!!$item && empty($item->getErrors())) {
			CartsItemsModel::deleteAll('`game_id` = :game AND `id` < :bet AND `cart_id` = :cart', [
				'game' => $item->game_id,
				'bet' => $item->id,
				'cart' => $item->cart_id
			]);
			$gameLocales = EventGameLocaleModel::find()->andWhere(['game_id' => $item->game_id])->current()->limit(2)->all();
			$outcomeLocales = GameOutcomesLocaleModel::find()->andWhere([
				'outcome_id' => EventOutcomesModel::find()->select('outcome_id')->andWhere([
					'id' => $item->outcome_id,
				]),
			])->current()->limit(2)->all();
			$marketLocales = GameMarketsLocaleModel::find()->andWhere(['market_id' => $item->market_id])->current()->limit(2)->all();

			$participants = \array_values(ArrayHelper::map(EventParticipantsModel::find()->andWhere([
				'game_id' => $item->game_id,
			])->with([
				'participantLocale' => function (GameParticipantsLocaleQuery $query) {
					return $query->current();
				},
			])->asArray()->all(), 'id', function($item) {
				return [
					'id' => +$item['participant_id'],
					'name' => GameHelpers::getLocalization($item['participantLocale'])['name'] ?? '',
				];
			}));
			$participantReplacements = [];
			foreach($participants AS $k => $v) {
				$participantReplacements[$k + 1] = $v['name'];
			}

			$sport = GameSportsModel::find()->select('image')->where(['id' => $item->sport_id])->one();
			return [
				'success' => true,
				'item' => [
					'id' => +$item['id'],
					'game_id' => +$item['game_id'],
					'game_title' => GameHelpers::getLocalization($gameLocales)['title'] ?? '',
					'outcome_id' => +$item['outcome_id'],
					'outcome_title' => GameHelpers::replacingCodeWithName(GameHelpers::getLocalization($outcomeLocales)['title'] ?? '', $participantReplacements),
					'market_id' => +$item['market_id'],
					'market_title' => GameHelpers::getLocalization($marketLocales)['title'] ?? '',
					'rate' => +$item['rate'],
					'rate_now' => +$item['rate'],
					'is_enabled' => 1,
					'is_finished' => 0,
					'is_banned' => 0,
					'is_hidden' => 0,
					'sport_image' => $sport['image']
				],
				'amount' => $cart->amount,
				'on_change' => $cart->on_change,
				'minimal' => CartsModel::getMinimalBet(),
				'maximal' => CartsModel::getMaximalBet(),
			];
		}
		return [ 'success' => false ] + (empty($item->getErrors()) ? [
			'message' => Yii::t('bets', 'Error while adding item to cart')
		] : [ 'errors' => $item->getErrors() ]);
	}

	/**
	 * Remove cart item
	 *
	 * @param int $id
	 * @return array
	 * @throws \Throwable
	 */
	public function actionItemRemove (int $id) {
		$cart = self::getCurrentCart(false);
		if (!$cart) {
			return ['success' => false];
		}
		if($item = CartsItemsModel::find()->andWhere(['id' => $id, 'cart_id' => $cart->id])->one()) {
			$item->delete();
			if($cart->getItems()->count() == 0) {
				Yii::$app->response->cookies->remove('cart_id');
				Yii::$app->response->cookies->remove('key');
				$cart->delete();
			}
		}
		return [
			'success' => true,
		];
	}

	/**
	 * Remove cart item
	 *
	 * @param int $id
	 * @return array
	 * @throws \Throwable
	 */
	public function actionItemConfirm (int $id) {
		$cart = self::getCurrentCart(false);
		if (!$cart) {
			return ['success' => false];
		}
		$item = CartsItemsModel::findOne(['id' => $id, 'cart_id' => $cart->id]);
		$item->rate = EventOutcomesModel::findOne(['id' => $item->outcome_id])->rate;
		return [
			'success' => !!$item->save(false, ['rate']),
			'rate' => +$item->rate
		];
	}

	/**
	 * Place bet
	 *
	 * @param int $amount
	 * @param int $clean
	 * @param string $promocode
	 * @param int $type
	 * @return array
	 *
	 * @throws \Throwable
	 */
	public function actionPlaceBet (int $amount, int $clean = 1, $promocode = '', int $type = 1) {
		try {
			$cart = self::getCurrentCart(true);
			$cart->scenario = CartsModel::SCENARIO_UPDATE;
			$cart->amount = $amount;
			if (!$cart->validate('amount')) {
				throw new BadRequestHttpException(Yii::t('games', $cart->getFirstError('amount')));
			}
			$user = Yii::$app->user->identity;
			/** @var $user UserModel */
			if ($user->purse_amount < $cart->amount) {
				throw new BadRequestHttpException(Yii::t('user', 'There is not enough money in your account'));
			}
			$cart->placeBet($user, $type);
		} catch (HttpException $httpEX) {
			Yii::$app->response->setStatusCode($httpEX->statusCode);
			return [
				'success' => false,
				'message' => $httpEX->getMessage(),
			];
		} catch (\Throwable $ex) {
			Yii::$app->response->setStatusCode(400);
			return [
				'success' => false,
				'message' => $ex->getMessage(),
			];
		}
		if($clean) {
			$cart->delete();
			Yii::$app->response->cookies->remove('cart_id');
			Yii::$app->response->cookies->remove('key');
		}
		return ['success' => true];
	}

	/**
	 * Delete cart
	 *
	 * @return array
	 * @throws \Throwable
	 */
	public function actionDelete () {
		$cart = self::getCurrentCart(false);
		if (!!$cart && $cart->delete()) {
			/** @var $response Response */
			if (($response = Yii::$app->get('response', false))) {
				$response->cookies->remove('cart_id');
				$response->cookies->remove('key');
			}
			return ['success' => true];
		}
		return ['success' => false];
	}

}

<?php

namespace frontend\modules\games\controllers;

use common\modules\games\Module;
use common\modules\user\models\UserApplicationModel;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * Class MiniGamesController
 * @package frontend\modules\games\controllers
 * @property-read array $token
 * @property-read Module $module
 */
class MiniGamesController extends Controller {

	/**
	 * @return string
	 */
	public function actionList () {
		return $this->render('list', [
			'games' => $this->module->miniGames->getList(),
		]);
	}

	public function actionGame (string $name) {
		$list = $this->module->miniGames->getList();
		if (!array_key_exists($name, $list)) {
			throw new NotFoundHttpException();
		}
		return $this->render('game', [
			'game' => $list[$name],
			'user' => $this->getToken(),
		]);
	}

	protected function getToken () {
		if (Yii::$app->user->getIsGuest()) {
			return ['uid' => null, 'key' => null];
		}
		$uid = Yii::$app->user->id;
		if (!($tokenModel = UserApplicationModel::find()->andWhere([
			'user_id' => $uid, 'app_type' => UserApplicationModel::TYPE_WEB,
		])->andWhere(['>', 'ended_at', time()])->limit(1)->one())) {
			$tokenModel = Yii::createObject(UserApplicationModel::class);
			$tokenModel->setAttributes([
				'user_id' => $uid,
				'app_type' => UserApplicationModel::TYPE_WEB,
			]);
			$tokenModel->save();
		}
		$key = $tokenModel->token;
		return [
			'uid' => $uid,
			'key' => $key,
		];
	}
}
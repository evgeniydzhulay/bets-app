<?php

namespace frontend\modules\games\controllers;

use common\modules\games\queries\EventTournamentLocaleQuery;
use common\modules\games\queries\GameMarketsGroupsLocaleQuery;
use common\modules\games\queries\GameMarketsLocaleQuery;
use common\modules\games\queries\GameOutcomesLocaleQuery;
use common\modules\games\queries\GameParticipantsLocaleQuery;
use Yii;
use common\modules\games\search\BetsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;

class BetsController extends Controller {

	/** @inheritdoc */
	public function behaviors () {
		return [
			'access' => [
				'class' => AccessControl::class,
				'only' => ['index'],
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index'],
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * User bets history
	 *
	 * @return string
	 */
	public function actionIndex() {
		$searchModel = Yii::createObject(BetsSearch::class);
		$searchModel->scenario = BetsSearch::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search([
			'user_id' => Yii::$app->user->id
		] + Yii::$app->request->get());
		$dataProvider->query->with([
			'items', 'items.game', 'items.game.eventParticipants',
			'items.game.tournamentLocales' => function (EventTournamentLocaleQuery $query) {
				return $query->current()->select(['tournament_id', 'lang_code', 'title']);
			},
			'items.game.eventParticipants.participantLocale' => function(GameParticipantsLocaleQuery $query) {
				return $query->current();
			},
			'items.market', 'items.market.gameMarketsGroupsLocales' => function (GameMarketsGroupsLocaleQuery $query) {
				return $query->current();
			},
			'items.marketLocales' => function (GameMarketsLocaleQuery $query) {
				return $query->current();
			},
			'items.outcomeLocales' => function (GameOutcomesLocaleQuery $query) {
				return $query->current();
			},
		])->orderBy(['id' => SORT_DESC]);
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
			'statuses' => BetsSearch::statuses(),
		]);
	}
}
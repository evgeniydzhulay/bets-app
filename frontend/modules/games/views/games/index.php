<?php

use common\modules\content\models\BlockModel;
use frontend\assets\GamesAsset;
use yii\web\View;
use \common\modules\games\models\BetsModel;

/**
 * @var $this View
 */

GamesAsset::register($this);
?>
<script>
    window.$locale = '<?= Yii::$app->language; ?>';
    window.$messages = <?= json_encode([
	    Yii::$app->language => [
            'All' => Yii::t('bets', 'All'),
            'Line' => Yii::t('bets', 'Line'),
		    'Live' => Yii::t('bets', 'Live'),
            'Upcoming' => Yii::t('bets', 'Upcoming'),
            'Favourites' => Yii::t('bets', 'Favourites'),
            'event starts in under 2 hours' => Yii::t('bets', 'event starts in under 2 hours'),
            'event starts in 2—4 hours' => Yii::t('bets', 'event starts in 2—4 hours'),
            'event starts in over 4 hours' => Yii::t('bets', 'event starts in over 4 hours'),
		    'PLACE A BET' => Yii::t('bets', 'PLACE A BET'),
		    'Log In' => Yii::t('user', 'Log In'),
		    'My bets' => Yii::t('bets', 'My bets'),
		    'Bet slip' => Yii::t('bets', 'Bet slip'),
		    'Promo code:' => Yii::t('bets', 'Promo code:'),
		    'Accept when increase' => Yii::t('bets', 'Accept when increase'),
		    'Agree' => Yii::t('bets', 'Agree'),
		    'Confirm' => Yii::t('bets', 'Confirm'),
		    'When odds change:' => Yii::t('bets', 'When odds change:'),
		    'Clear bet slip after placing a bet' => Yii::t('bets', 'Clear bet slip after placing a bet'),
		    'ACCUMULATOR' => Yii::t('bets', 'ACCUMULATOR'),
		    'SINGLE' => Yii::t('bets', 'SINGLE'),
		    'Stake:' => Yii::t('bets', 'Stake:'),
		    'Potential winnings:' => Yii::t('bets', 'Potential winnings:'),
		    'Max. stake amount:' => Yii::t('bets', 'Max. stake amount:'),
		    'REMOVE ALL' => Yii::t('bets', 'REMOVE ALL'),
		    'Overall odds:' => Yii::t('bets', 'Overall odds:'),
		    'Select sports' => Yii::t('bets', 'Select sports'),
		    'Select outcome' => Yii::t('bets', 'Select outcome'),
		    'Select bet amount and type' => Yii::t('bets', 'Select bet amount and type'),
            'Game' => Yii::t('bets', 'Game'),
            'Accumulator Of The Day' => Yii::t('bets', 'Accumulator Of The Day'),
            'Bonus from BET:' => Yii::t('bets', 'Bonus from BET:'),
            'Overall ODDS:' => Yii::t('bets', 'Overall ODDS:'),
            'Add to bet slip' => Yii::t('bets', 'Add to bet slip'),
		    'Game is finished' => Yii::t('games', 'Game is finished'),
		    'The game will begin' => Yii::t('games', 'The game will begin'),
		    'Match time' => Yii::t('games', 'Match time'),
		    'Video' => Yii::t('games', 'Video'),
		    'Pause' => Yii::t('games', 'Pause'),
		    'Registration' => Yii::t('user', 'Registration'),

		    '1 Set' => Yii::t('results', '1 Set'),
		    '2 Set' => Yii::t('results','2 Set'),
		    '3 Set' => Yii::t('results','3 Set'),
		    '4 Set' => Yii::t('results','4 Set'),
		    '5 Set' => Yii::t('results','5 Set'),
		    '6 Set' => Yii::t('results','6 Set'),
		    '7 Set' => Yii::t('results','7 Set'),
		    '8 Set' => Yii::t('results','8 Set'),
		    '1 Half' => Yii::t('results','1 Half'),
		    '2 Half' => Yii::t('results','2 Half'),
        ]
    ]); ?>;
    window.timezone = '<?= Yii::$app->getTimeZone(); ?>';
    window.timezones = <?= json_encode(\yii\helpers\ArrayHelper::map(Yii::$app->get('locations')->getTimeZones(), 'name', 'timezone')); ?>;
</script>

<div id="games-console" class="home"></div>
<div class="footer-blocks">
    <div class="info-footer-content">
        <div class="info-footer-text">
            <div class="info-footer-header">
                Luxe.bet - Online Sports Betting
            </div>
		    <?php
		    if($blockBetting = BlockModel::getItem('footer-betting', Yii::$app->language)) {
			    echo "<h5>{$blockBetting->title}</h5>{$blockBetting->content}";
		    }
	        if($blockHowToPlay = BlockModel::getItem('footer-how-play', Yii::$app->language)) {
		        echo "<h5>{$blockHowToPlay->title}</h5>{$blockHowToPlay->content}";
	        }
	        ?>
        </div>
        <div class="info-footer-text">
		    <?php
		    if($blockAdvantages = BlockModel::getItem('footer-advantages', Yii::$app->language)) {
			    echo "<h5>{$blockAdvantages->title}</h5>{$blockAdvantages->content}";
		    }
		    ?>
        </div>
        <div class="info-footer-text">
		    <?php
		    if($blockLiveBetting = BlockModel::getItem('footer-live-betting', Yii::$app->language)) {
			    echo "<h5>{$blockLiveBetting->title}</h5>{$blockLiveBetting->content}";
		    }
		    ?>
        </div>
    </div>
</div>
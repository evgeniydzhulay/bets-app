<?php

/**
 * @var $this \yii\web\View
 * @var $live bool
 */

use frontend\assets\ResultsAsset;

ResultsAsset::register($this);
?>
<div id="games-results">
</div>

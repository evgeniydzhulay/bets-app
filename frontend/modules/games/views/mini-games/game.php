<?php

/**
 * @var $this \yii\web\View
 * @var $game mixed
 * @var $user array
 */

$iframeUrl = $game['url'];
if(!is_null($user['uid']) && !is_null($user['key'])) {
	$iframeUrl .= "?uid={$user['uid']}&key={$user['key']}";
}
?>

<iframe src="<?= $iframeUrl ?>" id="game-frame" frameborder="0" scrolling="no" width="100%" height="1000" onload="calcHeight()"></iframe>
<script type="text/javascript">
    document.domain = "<?= $_SERVER['SERVER_NAME']; ?>";
    function calcHeight() {
        document.getElementById('game-frame').height = document.getElementById('game-frame').contentWindow.document.body.scrollHeight;
    }
    window.addEventListener("message", function(event) {
        if(event.data === 'need-auth') {
            jQuery('#authRequiredPopup').modal();
        }
    }, false);
</script>
<div id="authRequiredPopup" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content auth-popup">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="col-md-6">
                        <a href="<?= \yii\helpers\Url::toRoute(['/user/auth/login']); ?>" class="btn btn-red btn-block">
                            <?= Yii::t('user', 'Log In'); ?>
                        </a>
                    </div>
                    <div class="col-md-6">
                        <a href="<?= \yii\helpers\Url::toRoute(['/user/auth/register']); ?>" class="btn btn-red btn-block">
                            <?= Yii::t('user', 'Registration'); ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
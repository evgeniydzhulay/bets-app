<?php

/**
 * @var $this \yii\web\View
 * @var $games array
 */
?>
<div class="mini_wrap">
    <div class="mini_left">
        <div class="mini_search">
            <input type="text" placeholder="Поиск">
            <button class="mini_search_btn"></button>
        </div>

        <div class="ui-button-side-bar mini_cat">
            <div class="button active">
                <div class="arrow-button drop_togle"><i class="fas fa-chevron-down"></i></div>
                <div class="text">
                    <span>Категории</span>
                </div>
            </div>
            <div class="content left_content active mini_cat_content">
                <div class="ui-button-content">
                    <div class="button-content">
                        <div class="content-text">
                            Популярные
                        </div>
                    </div>
                </div>
                <div class="ui-button-content">
                    <div class="button-content">
                        <div class="content-text">
                            Карточные
                        </div>
                    </div>
                </div>
                <div class="ui-button-content">
                    <div class="button-content">
                        <div class="content-text">
                            Простые
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="min_popular">
            <div class="min_popular_title">
                <span>Популярные игры</span>
            </div>
            <ul class="popular_list">
                <li>
                    <a href="#">
                        <div class="popular_img">
                            <img src="/static/img/mini-games/pop1.png" alt="">
                        </div>
                        <div class="popular_content">
                            <p class="popular_price">
                                Jackpot:
                                <span>$95</span>
                            </p>
                            <p class="popular_name">
                                BlackJack
                            </p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="popular_img">
                            <img src="/static/img/mini-games/pop2.png" alt="">
                        </div>
                        <div class="popular_content">
                            <p class="popular_price">
                                Jackpot:
                                <span>$78</span>
                            </p>
                            <p class="popular_name">
                                Mines
                            </p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="popular_img">
                            <img src="/static/img/mini-games/pop3.png" alt="">
                        </div>
                        <div class="popular_content">
                            <p class="popular_price">
                                Jackpot:
                                <span>$115</span>
                            </p>
                            <p class="popular_name">
                                Wheel
                            </p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="popular_img">
                            <img src="/static/img/mini-games/pop4.png" alt="">
                        </div>
                        <div class="popular_content">
                            <p class="popular_price">
                                Jackpot:
                                <span>$66</span>
                            </p>
                            <p class="popular_name">
                                Baccarat
                            </p>
                        </div>
                    </a>
                </li>
                <li>
                    <a href="#">
                        <div class="popular_img">
                            <img src="/static/img/mini-games/pop5.png" alt="">
                        </div>
                        <div class="popular_content">
                            <p class="popular_price">
                                Jackpot:
                                <span>$52</span>
                            </p>
                            <p class="popular_name">
                                Keno
                            </p>
                        </div>
                    </a>
                </li>
            </ul>
        </div>

	</div>    
    <div class="mini_center">
        <div class="mini_center_title">
            <p>Игры</p>
        </div>
        <div class="mini_center_content">
            <?php foreach($games AS $gkey => $game) { ?>
            <div class="mini_item <?=$gkey ?>">
                <a href="<?= \yii\helpers\Url::toRoute(['/games/mini-games/game', 'name' => $gkey]); ?>">
                    <div class="mini_item_bg"></div>
                    <div class="mini_item_txt">
                        <?= $game['name']; ?>
                    </div>
                </a>
            </div>
            <?php } ?>
        </div>
    </div>
    <div class="mini_right">
        <div class="mini_banner">
            <a href="#" class="mini_banner_btn">Принять участие</a>
        </div>
        <div class="mini_winner">
            <div class="mini_winner_title">
                <p>
                    Последние победители
                </p>
            </div>
            <ul class="mini_winner_list">
                <li>
                    <div class="winner_list_left">
                        <div class="winner_list_img">
                            <img src="/static/img/mini-games/win1.png" alt="">
                        </div>
                        <p>Анна Дмитриева</p>
                    </div>
                    <div class="winner_list_right">
                        <p>$329</p>
                    </div>
                </li>
                <li>
                    <div class="winner_list_left">
                        <div class="winner_list_img">
                            <img src="/static/img/mini-games/win2.png" alt="">
                        </div>
                        <p>Павел Воля</p>
                    </div>
                    <div class="winner_list_right">
                        <p>$278</p>
                    </div>
                </li>
                <li>
                    <div class="winner_list_left">
                        <div class="winner_list_img">
                            <img src="/static/img/mini-games/win3.png" alt="">
                        </div>
                        <p>Дмитрий Кошкин</p>
                    </div>
                    <div class="winner_list_right">
                        <p>$226</p>
                    </div>
                </li>
                <li>
                    <div class="winner_list_left">
                        <div class="winner_list_img">
                            <img src="/static/img/mini-games/win4.png" alt="">
                        </div>
                        <p>Виталий Смелый</p>
                    </div>
                    <div class="winner_list_right">
                        <p>$198</p>
                    </div>
                </li>
                <li>
                    <div class="winner_list_left">
                        <div class="winner_list_img">
                            <img src="/static/img/mini-games/win5.png" alt="">
                        </div>
                        <p>Ирина Ражик</p>
                    </div>
                    <div class="winner_list_right">
                        <p>$128</p>
                    </div>
                </li>
            </div>
        </div>
    </div>
</div>

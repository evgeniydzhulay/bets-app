<?php

use common\modules\games\models\BetsItemsModel;
use common\modules\games\models\BetsModel;
use common\modules\games\helpers\GameHelpers;
use yii\helpers\ArrayHelper;

/**
 * @var $model BetsModel
 * @var $user \common\modules\user\models\UserModel
 */
$badgeColors = [
	BetsModel::STATUS_VICTORY => 'win',
	BetsModel::STATUS_DEFEAT => 'lose',
	BetsModel::STATUS_ACTIVE => 'active',
	BetsModel::STATUS_BANNED => 'ban',
];
$statusColors = [
	BetsItemsModel::STATUS_VICTORY => 'win',
	BetsItemsModel::STATUS_DEFEAT => 'lose',
	BetsItemsModel::STATUS_ACTIVE => 'active',
	BetsItemsModel::STATUS_REVERTED => 'reverted',
	BetsItemsModel::STATUS_REFUND => 'refund',
	BetsItemsModel::STATUS_HALF_VICTORY => 'half-win',
	BetsItemsModel::STATUS_HALF_DEFEAT => 'half-lose',
];
$user = Yii::$app->user->identity;
if(count($model->items) === 1) {
	$item = $model->items[0];
	?>
	<div class="bets-history-item bets-history-item-single">
		<div class="bets-history-item-title">
			<div class="history-item-field history-item-id">
				#<?= $model->id ?>
			</div>
			<div class="history-item-field history-item-label">
				<?= GameHelpers::getLocalization($item->game->tournamentLocales)['title'] ?? '' ?>
			</div>
			<div class="history-item-field history-item-bet">
				<?= Yii::t('games', 'Bet'); ?>
			</div>
			<div class="history-item-field history-item-win">
				<?= Yii::t('games', 'Winning'); ?>
			</div>
			<div class="history-item-field history-item-type">
				<?= Yii::t('games', 'Type'); ?>
			</div>
			<div class="history-item-field history-item-status">
				<?= Yii::t('games', 'Status'); ?>
			</div>
		</div>
		<div class="bets-history-item-content">
			<div class="history-item-field opener">
				<div class="show-button show-button-default expand-block" data-target="bet-items-list-<?= $model->id; ?>">
                    <i class="fas fa-caret-right"></i>
                </div>
			</div>
			<div class="history-item-field history-item-created_at">
				<span class="created_at-date">
					<?= date('Y-m-d', $model->created_at); ?>
				</span>
				<span class="created_at-time">
					<?= date('H:i', $model->created_at); ?>
				</span>
			</div>
			<div class="history-item-field history-item-label">
				<?= implode(' - ', ArrayHelper::map($item->game->eventParticipants, 'participant_id', function ($participant) {
					return GameHelpers::getLocalization($participant['participantLocale'])['name'] ?? '';
				})); ?>
			</div>
			<div class="history-item-field history-item-bet">
				<?= Yii::$app->formatter->asCurrency($model->amount_bet, $user->purse_currency); ?>
			</div>
			<div class="history-item-field history-item-win">
				<?=
                $model->status == BetsModel::STATUS_ACTIVE ? ' - ' :
                    Yii::$app->formatter->asCurrency($model->amount_win, $user->purse_currency);
                ?>
			</div>
			<div class="history-item-field history-item-type">
                <?= BetsModel::types()[BetsModel::TYPE_SINGLE]; ?>
			</div>
			<div class="history-item-field history-item-status">
                <div class="history-item-status-badge item-badge-<?= $badgeColors[$model->status]; ?>">
				    <?= BetsModel::statuses()[$model->status] ?>
                </div>
			</div>
		</div>
	</div>
<?php } else { ?>
	<div class="bets-history-item  bets-history-item-multi">
		<div class="bets-history-item-content">
			<div class="history-item-field opener">
				<div class="show-button show-button-default expand-block" data-target="bet-items-list-<?= $model->id; ?>">
                    <i class="fas fa-caret-right"></i>
                </div>
			</div>
			<div class="history-item-field history-item-created_at">
				<span class="created_at-date">
					<?= date('Y-m-d', $model->created_at); ?>
				</span>
				<span class="created_at-time">
					<?= date('H:i', $model->created_at); ?>
				</span>
			</div>
			<div class="history-item-field history-item-label">
				<?= Yii::t('games', 'Multi ticket'); ?>
			</div>
			<div class="history-item-field history-item-bet">
				<?= Yii::t('games', 'Bet'); ?>
				<?= Yii::$app->formatter->asCurrency($model->amount_bet, $user->purse_currency); ?>
			</div>
			<div class="history-item-field history-item-win">
				<?= Yii::t('games', 'Winning'); ?>
				<?=
				$model->status == BetsModel::STATUS_ACTIVE ? ' - ' :
                    Yii::$app->formatter->asCurrency($model->amount_win, $user->purse_currency);
				?>
			</div>
			<div class="history-item-field history-item-status">
				<?= Yii::t('games', 'Status'); ?>
			</div>
            <div class="history-item-field history-item-status-label">
                <div class="history-item-status-badge item-badge-<?= $badgeColors[$model->status] ?? ''; ?>">
				    <?= BetsModel::statuses()[$model->status] ?>
                </div>
            </div>
		</div>
	</div>
<?php } ?>
<div class="bets-history-item-items" id="bet-items-list-<?= $model->id; ?>">
	<?php
	$count = 0;
	foreach($model->items AS $item) {
		?>
        <div class="bets-history-item-content">
            <div class="history-item-field history-item-created_at">
				<?php if($count == 0) { ?>
                    <span class="history-item-block-label">
                        <?= Yii::t('games', 'Time'); ?>
                    </span>
				<?php } ?>
                <span class="created_at-date">
                    <?= date('Y-m-d', $item->game->starts_at); ?>
                </span>
                <span class="created_at-time">
                    <?= date('H:i', $item->game->starts_at); ?>
                </span>
            </div>
            <div class="history-item-field history-item-label">
                <span class="history-item-tournament">
                    <?= GameHelpers::getLocalization($item->game->tournamentLocales)['title'] ?? '' ?>
                </span>
				<?= implode(' - ', ArrayHelper::map($item->game->eventParticipants, 'participant_id', function ($participant) {
					return GameHelpers::getLocalization($participant['participantLocale'])['name'] ?? '';
				})); ?>
            </div>
            <div class="history-item-field history-item-outcome">
                <span class="history-item-outcome-group">
                    <?= GameHelpers::getLocalization($item->market->gameMarketsGroupsLocales)['title'] ?? '' ?>
                </span>
                <span class="history-item-outcome-market">
                    <?= GameHelpers::getLocalization($item->marketLocales)['title'] ?? '' ?>
                </span>
                <span class="history-item-outcome-outcome">
                    <?= GameHelpers::getLocalization($item->outcomeLocales)['title'] ?? '' ?>
                </span>
            </div>
            <div class="history-item-field history-item-coefficient">
				<?php if($count == 0) { ?>
                    <span class="history-item-block-label">
                        <?= Yii::t('games', 'Rate'); ?>
                    </span>
				<?php } ?>
				<?= $item->rate; ?>
            </div>
            <div class="history-item-field history-item-status">
				<?php if($count == 0) { ?>
                    <span class="history-item-block-label">
                        <?= Yii::t('games', 'Status'); ?>
                    </span>
				<?php } ?>
                <div class="history-item-status-info">
					<?= BetsItemsModel::statuses()[$item->status] ?>
                    <span class="item-status-point item-point-<?= $statusColors[$item->status] ?? ''; ?>"></span>
                </div>
            </div>
        </div>
		<?php $count++;
	} ?>
</div>

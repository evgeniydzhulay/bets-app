<?php

use common\modules\user\search\PurseHistorySearch;
use kartik\form\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ListView;
use yii\helpers\Html;
use kartik\date\DatePicker;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var PurseHistorySearch $searchModel
 * @var array $types
 * @var array $statuses
 */

$this->beginContent('@common/modules/user/views/shared/profile.php');

$this->title = Yii::t('games', 'Bets history');
$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin([
	'id' => 'bets-history-search-form',
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'options' => [
		'class' => 'box bets-history-search-form'
	],
	'method' => 'GET',
	'action' => Url::toRoute(['index'])
]);
?>
<div class="box-header">
	<span><?= Yii::t('games', 'Yout bet history'); ?></span>
</div>
<div class="box-body">
	<div class="bets-history-date">
		<i class="fas fa-calendar"></i>
		<?= Yii::t('games', 'Period from'); ?>
		<?= $form->field($searchModel, 'created_from')->widget(DatePicker::class, [
			'pluginOptions' => [
				'format' => 'yyyy-mm-dd',
			],
			'type' => DatePicker::TYPE_INPUT,
		])->label(false); ?>
		<?= Yii::t('app', 'to'); ?>
		<?= $form->field($searchModel, 'created_to')->widget(DatePicker::class, [
			'pluginOptions' => [
				'format' => 'yyyy-mm-dd',
			],
			'type' => DatePicker::TYPE_INPUT,
		])->label(false); ?>
	</div>
	<?= Html::submitButton(Yii::t('games', 'Show'), ['class' => 'btn btn-red']) ?>
</div>
<?php
ActiveForm::end();
echo ListView::widget([
	'dataProvider' => $dataProvider,
	'id' => 'bets-history-search',
	'options' => [
		'class' => 'bets-history',
	],
	'itemView' => 'indexItem',
	'layout' => "{items}\n{pager}"
]);
$this->endContent();
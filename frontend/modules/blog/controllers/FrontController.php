<?php

namespace frontend\modules\blog\controllers;

use common\modules\blog\models\PostModel;
use common\modules\blog\models\PostSearch;
use Yii;

class FrontController extends \yii\web\Controller {

    public function actionIndex() {
        $filterModel = Yii::createObject([
            'class' => PostSearch::class,
            'scenario' => PostSearch::SCENARIO_SEARCH,
        ]);
        $request = Yii::$app->request->get();
	    $request['active_from'] = time();
	    $request['lang_code'] = Yii::$app->language;
        return $this->render('posts', [
            'dataProvider' => $filterModel->search($request),
        ]);
    }

    public function actionPost($key) {
        $post = $this->getPost($key);
        return $this->render('post', [
            'post' => $post
        ]);
    }

    protected function getPost($key) {
        $item = PostModel::findOne(['key' => $key]);
        if ($item) {
            return $item;
        }
        throw new \yii\web\NotFoundHttpException(Yii::t('blog', 'The requested post does not exist'));
    }

}

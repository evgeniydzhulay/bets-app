<?php

return [
	'modules' => [
		'blog' => [
			'controllerNamespace' => '\frontend\modules\blog\controllers',
			'viewPath' => '@frontend/modules/blog/views'
		],
	],

	'components' => [
		'urlManager' => [
			'rules' => [
				'/blog' => '/blog/front/index',
				'/blog/<key:\w+>' => '/blog/front/post',
			]
		]
	]
];

<?php

use yii\widgets\ListView;
$this->title = Yii::t('blog', 'Posts');

echo ListView::widget([
    'dataProvider' => $dataProvider,
    'itemView' => '_post',
    'id' => 'posts-list',
    'layout' => "{items}{pager}",
]);
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./frontend/assets/main/js/main.js":
/***/ (function(module, exports) {

function reloadUserPurse() {
  fetch("/user/purse/get-amount").then(function (result) {
    return result.json();
  }).then(function (result) {
    jQuery('#nav-user-website-purse_amount').html(result.amount);
  }).catch(function (err) {
    console.error(err);
  });
}

jQuery(function () {
  jQuery('body').on('click', '.expand-block', function (ev) {
    var button = jQuery(ev.currentTarget);
    var target = button.data('target');

    if (!target) {
      return;
    }

    var targetElement = jQuery('#' + target);
    button.toggleClass('show-button-active');
    targetElement.toggleClass('opened');
  });
  jQuery('#nav-user-website-purse').click(function () {
    reloadUserPurse();
  });
  setInterval(function () {
    var date = new Date(+Date.now() + window.timeDiff);
    jQuery('#select2-select-timezone-container').html(("0" + date.getHours()).slice(-2) + ':' + ("0" + date.getMinutes()).slice(-2));
  }, 1000);
  var accountButton = document.getElementsByClassName('ui-account-information-drop-menu')[0];

  if (!!accountButton) {
    var content = document.getElementsByClassName('account-content')[0];
    window.addEventListener('click', function (_ref) {
      var target = _ref.target;

      if (content.className.includes('account-content-active') && !target.parentNode.className.includes('ui-account-information-drop-menu')) {
        content.classList.remove('account-content-active');
        document.getElementById('rotateArrow').style.transform = "rotate(0deg) translateY(0%)";
      }
    });
    accountButton.addEventListener('click', function () {
      if (content.className.includes('account-content-active')) {
        content.classList.remove('account-content-active');
        document.getElementById('rotateArrow').style.transform = "rotate(0deg) translateY(0%)";
      } else {
        content.classList.add('account-content-active');
        document.getElementById('rotateArrow').style.transform = "rotate(180deg) translateY(-36%)";
      }
    });
  }
}); // Withdraw page

$(document).ready(function () {
  // console.log(window.location.pathname.includes('user') || window.location.pathname.includes('results'));    
  $('#account-withdraw-form .btn-group').each(function (id, el) {
    // console.log($(el).find('input').val()); 
    $(el).addClass($(el).find('input').val());
    $(el).click(function () {
      $('.form_with_wrap').show();
      $('.overlay_modal').show();
    });
  });
  $('.overlay_modal').click(function () {
    $('.form_with_wrap').hide();
    $(this).hide();
  });
  $('.form_with_close').click(function () {
    $('.form_with_wrap').hide();
    $('.overlay_modal').hide();
  });
});
$('.auto_item').click(function () {
  // console.log($(this).data('value'));  
  $('#pursedepositform-amount').val(+$('#pursedepositform-amount').val() + +$(this).data('value'));
}); // $(document).ready(function(){

$('body').on('click', '.show_video_sidebar', function () {
  // $('#video_sidebar').show();        
  // $('#urlParam').attr('value', $(this).data('val')); 
  var val = $(this).data('val');
  var idx = $(this).data('id');
  var html = document.documentElement.getAttribute("lang");
  var text = {};

  if (html == 'ru') {
    text = {
      video: "Видео",
      login: "Войти",
      reg: "Регистрация"
    };
  } else {
    text = {
      video: "Video",
      login: "Log In",
      reg: "Registration"
    };
  }

  ;

  if (!$("#".concat(idx)).length) {
    $('.video_sidebar_wrap').prepend("\n                <div class=\"video_sidebar\" id=\"".concat(idx, "\">\n                    <div class=\"ui-button\">\n                        <div class=\"text\">\n                            <span>").concat(text.video, "</span>\n                        </div>\n                        <div class=\"ui-button-icon icon-close\"></div>                \n                    </div>\n                    <div class=\"video_sidebar_content\">\n                        <object type=\"application/x-shockwave-flash\" data=\"/static/player.swf\">\n                            <param name=\"menu\" value=\"false\">\n                            <param name=\"wmode\" value=\"opaque\">\n                            <param name=\"allowFullScreen\" value=\"true\">\n                            <param name=\"AllowScriptAccess\" value=\"always\">\n                            <param name=\"flashvars\" class=\"id_video_obj\" value=\"").concat(val, "\">\n                        </object>\n    \n                        <div class=\"without_reg without_reg_side hide\">\n                            <div class=\"btns_wrap\">\n                                <a href=\"/user/auth/login\" class=\"ui-radius-button log-in\" data-toggle=\"modal\" data-target=\"#loginPopup\">").concat(text.login, "</a>\n                                <a href=\"/user/auth/register\" class=\"ui-radius-button\">").concat(text.reg, "</a>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            "));
  }

  $('.icon-close').click(function () {
    $(this).parent('.ui-button').parent('.video_sidebar').remove();
  });
}); // });
// Mini games category

$('.drop_togle').click(function () {
  $(this).parent('.button').toggleClass('active');
  $('.mini_cat_content').stop().slideToggle();
}); // Burger btn

$('#mobileMenuButton').click(function () {
  if ($('.navigation-menu').css('right') === '0%') {
    $('.navigation-menu').css('right', '-150%');
  } else {
    $('.navigation-menu').css('right', '0');
  }
});

/***/ }),

/***/ "./frontend/assets/main/scss/main.sass":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ 1:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./frontend/assets/main/js/main.js");
module.exports = __webpack_require__("./frontend/assets/main/scss/main.sass");


/***/ })

/******/ });
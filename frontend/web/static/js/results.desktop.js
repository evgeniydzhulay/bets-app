/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"results.desktop": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([4,"vue"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./frontend/assets/results/js/results-filter.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _results_filter_vue_vue_type_template_id_2be7498f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/results/js/results-filter.vue?vue&type=template&id=2be7498f&");
/* harmony import */ var _results_filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/results/js/results-filter.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _results_filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _results_filter_vue_vue_type_template_id_2be7498f___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _results_filter_vue_vue_type_template_id_2be7498f___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/results/js/results-filter.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/results/js/results-filter.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_results_filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results-filter.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_results_filter_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/results/js/results-filter.vue?vue&type=template&id=2be7498f&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_results_filter_vue_vue_type_template_id_2be7498f___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results-filter.vue?vue&type=template&id=2be7498f&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_results_filter_vue_vue_type_template_id_2be7498f___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_results_filter_vue_vue_type_template_id_2be7498f___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/results/js/results-side-bar.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _results_side_bar_vue_vue_type_template_id_2ffbca74___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/results/js/results-side-bar.vue?vue&type=template&id=2ffbca74&");
/* harmony import */ var _results_side_bar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/results/js/results-side-bar.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _results_side_bar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _results_side_bar_vue_vue_type_template_id_2ffbca74___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _results_side_bar_vue_vue_type_template_id_2ffbca74___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/results/js/results-side-bar.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/results/js/results-side-bar.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_results_side_bar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results-side-bar.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_results_side_bar_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/results/js/results-side-bar.vue?vue&type=template&id=2ffbca74&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_results_side_bar_vue_vue_type_template_id_2ffbca74___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results-side-bar.vue?vue&type=template&id=2ffbca74&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_results_side_bar_vue_vue_type_template_id_2ffbca74___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_results_side_bar_vue_vue_type_template_id_2ffbca74___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/results/js/results.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue/dist/vue.runtime.esm.js");
/* harmony import */ var _results_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/results/js/results.vue");


vue__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].config.devtools = true;
new vue__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]({
  el: '#games-results',
  render: function render(h) {
    return h(_results_vue__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"]);
  }
});

/***/ }),

/***/ "./frontend/assets/results/js/results.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _results_vue_vue_type_template_id_05085174___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/results/js/results.vue?vue&type=template&id=05085174&");
/* harmony import */ var _results_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/results/js/results.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _results_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _results_vue_vue_type_template_id_05085174___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _results_vue_vue_type_template_id_05085174___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/results/js/results.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/results/js/results.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_results_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_results_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/results/js/results.vue?vue&type=template&id=05085174&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_results_vue_vue_type_template_id_05085174___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results.vue?vue&type=template&id=05085174&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_results_vue_vue_type_template_id_05085174___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_results_vue_vue_type_template_id_05085174___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/results/js/tournament.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _tournament_vue_vue_type_template_id_5bf1e149___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/results/js/tournament.vue?vue&type=template&id=5bf1e149&");
/* harmony import */ var _tournament_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/results/js/tournament.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _tournament_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _tournament_vue_vue_type_template_id_5bf1e149___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _tournament_vue_vue_type_template_id_5bf1e149___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/results/js/tournament.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/results/js/tournament.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_tournament_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/tournament.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_tournament_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/results/js/tournament.vue?vue&type=template&id=5bf1e149&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_tournament_vue_vue_type_template_id_5bf1e149___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/tournament.vue?vue&type=template&id=5bf1e149&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_tournament_vue_vue_type_template_id_5bf1e149___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_tournament_vue_vue_type_template_id_5bf1e149___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/results/scss/results.sass":
/***/ (function(module, exports) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results-filter.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vue2_datepicker__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue2-datepicker/lib/index.js");
/* harmony import */ var vue2_datepicker__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_datepicker__WEBPACK_IMPORTED_MODULE_0__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  props: ['isExpand', 'translateText', 'allExpand', 'searchVal', 'time', 'live', 'lang'],
  data: function data() {
    return {
      timeLocal: this.time,
      liveLocal: this.live
    };
  },
  components: {
    DatePicker: vue2_datepicker__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  methods: {
    updateTime: function updateTime(value) {
      this.$emit('update:time', value); //  $('.results_wrap').each((id, el) => {                
      //     if($(el).find('.game-results-tournament').length) {
      //         $(el).hide();                    
      //     } else {
      //         $(el).show();                    
      //     }                            
      // });
    },
    updateLive: function updateLive(value) {
      this.$emit('update:live', value);
    },
    showChecked: function showChecked() {
      $('.results_wrap').each(function (id, el) {
        if ($(el).find('.game-results-tournament').not(".checked").length === $(el).find('.game-results-tournament').length && $('#marked').prop("checked")) {
          $(el).hide();
        } else {
          $(el).show();
        }
      });

      if ($('#marked').prop("checked")) {
        $('.game-results-tournament').not(".checked").hide();
      } else {
        $('.game-results-tournament').not(".checked").show();
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results-side-bar.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["a"] = ({
  data: function data() {
    return {
      isSearchShowed: false
    };
  },
  props: ['filters', 'selectSport', 'setSidebarIcon', 'translateText', 'allSport', 'sports', 'sideSearchVal', 'sort'],
  methods: {
    sidebarSearch: function sidebarSearch() {
      this.isSearchShowed = !this.isSearchShowed;
      this.$refs.sidebarInput.focus();
    }
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var tournament_vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/results/js/tournament.vue");
/* harmony import */ var results_side_bar_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/results/js/results-side-bar.vue");
/* harmony import */ var results_filter_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./frontend/assets/results/js/results-filter.vue");
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance"); }

function _iterableToArray(iter) { if (Symbol.iterator in Object(iter) || Object.prototype.toString.call(iter) === "[object Arguments]") return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = new Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } }

//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var timeout = null;
/* harmony default export */ __webpack_exports__["a"] = ({
  data: function data() {
    return {
      live: +window.location.search.includes('live=1'),
      sport: '',
      filters: [],
      results: [],
      sports: {},
      translateText: {},
      searchVal: '',
      isSorted: 1,
      isExpand: false,
      time: '',
      lang: 'en',
      sideSearchVal: ''
    };
  },
  components: {
    'tournament': tournament_vue__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"],
    'side-bar': results_side_bar_vue__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
    'filter-block': results_filter_vue__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"]
  },
  computed: {
    gameGroups: function gameGroups() {
      var _this = this;

      return this.results.reduce(function (r, a) {
        var list = [].concat(_toConsumableArray(r[a.sports_title] || []), [a]).filter(function (item) {
          return (_this.filters.length === 0 || _this.filters.includes(item.sports_id)) && (_this.searchVal.length === 0 || item.tournament_title.toLowerCase().includes(_this.searchVal.toLowerCase())); // && item.tournament_events[0].start_date.includes(this.time) 
        });

        if (list.length !== 0) {
          r[a.sports_title] = list;
        }

        return r;
      }, {});
    },
    sportsList: function sportsList() {
      var _this2 = this;

      var sports = this.sports;
      var sortable = [];

      for (var title in this.sports) {
        sortable.push([title, this.sports[title]]);
      }

      if (this.isSorted === 2) {
        sortable.sort(function (a, b) {
          if (a[1] < b[1]) return -1;
          if (a[1] > b[1]) return 1;
          return 0;
        });
      } else if (this.isSorted === 3) {
        sortable.sort(function (a, b) {
          if (a[1] < b[1]) return 1;
          if (a[1] > b[1]) return -1;
          return 0;
        });
      }

      var result = sortable.filter(function (item) {
        return item[1].toLowerCase().includes(_this2.sideSearchVal.toLowerCase());
      });

      if (this.sideSearchVal.length !== 0) {
        return result;
      } else {
        return sortable;
      }
    }
  },
  created: function created() {
    var html = document.documentElement.getAttribute("lang");

    if (html == 'ru') {
      return this.translateText = {
        all: "Все",
        type: "ВИДЫ СПОРТА",
        search: "Поиск",
        title: "Результаты",
        hr: "Харрисон результаты",
        score: "Счет матча",
        video: "Показать с видео",
        marked: "Показать отмеченные",
        popular: "Популярные",
        expand: "Показать все",
        collapse: "Свернуть все"
      }, this.lang = "ru";
    } else {
      return this.translateText = {
        all: "All",
        type: "TYPE OF SPORTS",
        search: "Search",
        title: "Results",
        hr: "Harrison results",
        score: "Match score",
        video: "Show with video",
        marked: "Show marked",
        popular: "Popular",
        expand: "Expand All",
        collapse: "Collapse All"
      }, this.lang = "en";
    }
  },
  watch: {
    live: function live() {
      this.loadEvents();
    }
  },
  methods: {
    loadEvents: function loadEvents() {
      var _this3 = this;

      clearTimeout(timeout);
      this.$refs.results.classList.add('loading');
      fetch('/games/games/results-list' + (this.live ? '?live=1' : '')).then(function (result) {
        return result.json();
      }).then(function (data) {
        _this3.$refs.results.classList.remove('loading');

        if (!data.success) {
          throw new Error(data.message);
        }

        _this3.results = data.events;
        _this3.sports = data.sports;
        timeout = setTimeout(function () {
          _this3.loadEvents();
        }, 30000);
      });
    },
    selectSport: function selectSport(key) {
      var id = parseInt(key);

      if (this.filters.includes(id)) {
        this.filters = this.filters.filter(function (filterId) {
          return filterId !== id;
        });
      } else {
        this.filters.push(id);
      }
    },
    allSport: function allSport() {
      return this.filters = [];
    },
    setIcon: function setIcon(link) {
      return link !== null ? {
        'background-image': "url('/uploads/games/".concat(link, "')")
      } : {};
    },
    setSidebarIcon: function setSidebarIcon(title) {
      var imgUrl = this.results.filter(function (item) {
        return item.sports_title.includes(title);
      });
      return {
        'background-image': "url('/uploads/games/".concat(imgUrl[0].sports_image, "')")
      };
    },
    allExpand: function allExpand() {
      this.isExpand = !this.isExpand;
    },
    sort: function sort() {
      if (this.isSorted === 1 || this.isSorted === 3) {
        return this.isSorted = 2;
      } else if (this.isSorted === 2) {
        return this.isSorted = 3;
      }
    }
  },
  mounted: function mounted() {
    this.loadEvents();
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/tournament.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["a"] = ({
  name: "result-tournament",
  props: ['result', 'isExpand', 'time', 'timeFilter'],
  data: function data() {
    return {
      isActive: false,
      selected: false
    };
  },
  watch: {
    isExpand: function isExpand(val) {
      this.isActive = val;
    }
  },
  computed: {
    events: function events() {
      var _this = this;

      if (this._props.timeFilter === "") {
        return this.result.tournament_events;
      } else {
        return this.result.tournament_events.filter(function (item) {
          return item.start_date == _this._props.timeFilter;
        });
      }
    }
  },
  methods: {
    toggleResults: function toggleResults() {
      this.isActive = !this.isActive;
    },
    toggleCheck: function toggleCheck() {
      this.selected = !this.selected;
    }
  }
});

/***/ }),

/***/ "./node_modules/process/browser.js":
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./node_modules/setimmediate/setImmediate.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("./node_modules/webpack/buildin/global.js"), __webpack_require__("./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/timers-browserify/main.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var scope = (typeof global !== "undefined" && global) ||
            (typeof self !== "undefined" && self) ||
            window;
var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, scope, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, scope, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(scope, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__("./node_modules/setimmediate/setImmediate.js");
// On some exotic environments, it's not clear which object `setimmediate` was
// able to install onto.  Search each possibility in the same order as the
// `setimmediate` library.
exports.setImmediate = (typeof self !== "undefined" && self.setImmediate) ||
                       (typeof global !== "undefined" && global.setImmediate) ||
                       (this && this.setImmediate);
exports.clearImmediate = (typeof self !== "undefined" && self.clearImmediate) ||
                         (typeof global !== "undefined" && global.clearImmediate) ||
                         (this && this.clearImmediate);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results-filter.vue?vue&type=template&id=2be7498f&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "filters" }, [
    _c("div", { staticClass: "filters_top" }, [
      _c("div", { staticClass: "filters-live" }, [
        _c("input", {
          directives: [
            {
              name: "model",
              rawName: "v-model",
              value: _vm.liveLocal,
              expression: "liveLocal"
            }
          ],
          attrs: { type: "checkbox", name: "live", id: "live" },
          domProps: {
            checked: Array.isArray(_vm.liveLocal)
              ? _vm._i(_vm.liveLocal, null) > -1
              : _vm.liveLocal
          },
          on: {
            change: [
              function($event) {
                var $$a = _vm.liveLocal,
                  $$el = $event.target,
                  $$c = $$el.checked ? true : false
                if (Array.isArray($$a)) {
                  var $$v = null,
                    $$i = _vm._i($$a, $$v)
                  if ($$el.checked) {
                    $$i < 0 && (_vm.liveLocal = $$a.concat([$$v]))
                  } else {
                    $$i > -1 &&
                      (_vm.liveLocal = $$a
                        .slice(0, $$i)
                        .concat($$a.slice($$i + 1)))
                  }
                } else {
                  _vm.liveLocal = $$c
                }
              },
              function($event) {
                return _vm.updateLive(_vm.liveLocal)
              }
            ]
          }
        }),
        _vm._v(" "),
        _c("label", { attrs: { for: "live" } }, [_vm._v("Live")])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "filters-live" }, [
        _c("input", {
          attrs: { type: "checkbox", name: "result", id: "result" }
        }),
        _vm._v(" "),
        _c("label", { attrs: { for: "result" } }, [
          _vm._v(_vm._s(_vm.translateText.title))
        ])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "filters-live" }, [
        _c("input", {
          attrs: { type: "checkbox", name: "hResult", id: "hResult" }
        }),
        _vm._v(" "),
        _c("label", { attrs: { for: "hResult" } }, [
          _vm._v(_vm._s(_vm.translateText.hr))
        ])
      ])
    ]),
    _vm._v(" "),
    _c("div", { staticClass: "filters_bot" }, [
      _c(
        "div",
        { staticClass: "filters_bot_left" },
        [
          _c("date-picker", {
            attrs: {
              valueType: "format",
              "first-day-of-week": 1,
              lang: _vm.lang
            },
            on: {
              change: function($event) {
                return _vm.updateTime(_vm.timeLocal)
              }
            },
            model: {
              value: _vm.timeLocal,
              callback: function($$v) {
                _vm.timeLocal = $$v
              },
              expression: "timeLocal"
            }
          }),
          _vm._v(" "),
          _c("div", { staticClass: "filters-live" }, [
            _c("input", {
              attrs: { type: "checkbox", name: "score", id: "score" }
            }),
            _vm._v(" "),
            _c("label", { attrs: { for: "score" } }, [
              _vm._v(_vm._s(_vm.translateText.score))
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "filters-live" }, [
            _c("input", {
              attrs: { type: "checkbox", name: "video", id: "video" }
            }),
            _vm._v(" "),
            _c("label", { attrs: { for: "video" } }, [
              _vm._v(_vm._s(_vm.translateText.video))
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "filters-live" }, [
            _c("input", {
              attrs: { type: "checkbox", name: "marked", id: "marked" },
              on: { click: _vm.showChecked }
            }),
            _vm._v(" "),
            _c("label", { attrs: { for: "marked" } }, [
              _vm._v(_vm._s(_vm.translateText.marked))
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "filters-live" }, [
            _c("input", {
              attrs: { type: "checkbox", name: "popular", id: "popular" }
            }),
            _vm._v(" "),
            _c("label", { attrs: { for: "popular" } }, [
              _vm._v(_vm._s(_vm.translateText.popular))
            ])
          ]),
          _vm._v(" "),
          _c("div", { staticClass: "result_search" }, [
            _c("input", {
              directives: [
                {
                  name: "model",
                  rawName: "v-model",
                  value: _vm.searchVal,
                  expression: "searchVal"
                }
              ],
              attrs: { type: "text", placeholder: _vm.translateText.search },
              domProps: { value: _vm.searchVal },
              on: {
                keyup: function($event) {
                  return _vm.$emit("update:searchVal", _vm.searchVal)
                },
                input: function($event) {
                  if ($event.target.composing) {
                    return
                  }
                  _vm.searchVal = $event.target.value
                }
              }
            }),
            _vm._v(" "),
            _c("button")
          ])
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "filters_bot_right", on: { click: _vm.allExpand } },
        [
          _c("button", { class: { expanded: _vm.isExpand } }, [
            !_vm.isExpand
              ? _c("span", [_vm._v(_vm._s(_vm.translateText.expand))])
              : _vm._e(),
            _vm._v(" "),
            _vm.isExpand
              ? _c("span", [_vm._v(_vm._s(_vm.translateText.collapse))])
              : _vm._e()
          ])
        ]
      )
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results-side-bar.vue?vue&type=template&id=2ffbca74&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "result_sidebar" },
    [
      _c(
        "div",
        { staticClass: "sidebar_title", class: { showed: _vm.isSearchShowed } },
        [
          _c("span", [_vm._v(" " + _vm._s(_vm.translateText.type) + " ")]),
          _vm._v(" "),
          _c("div", { staticClass: "sidebar_buttons" }, [
            _c("div", { staticClass: "sidebar_search_wrap" }, [
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.sideSearchVal,
                    expression: "sideSearchVal"
                  }
                ],
                ref: "sidebarInput",
                attrs: { type: "text" },
                domProps: { value: _vm.sideSearchVal },
                on: {
                  keyup: function($event) {
                    return _vm.$emit("update:sideSearchVal", _vm.sideSearchVal)
                  },
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.sideSearchVal = $event.target.value
                  }
                }
              }),
              _vm._v(" "),
              _c("button", {
                staticClass: "sidebar_search",
                on: { click: _vm.sidebarSearch }
              })
            ]),
            _vm._v(" "),
            _c("button", {
              staticClass: "sidebar_sort",
              on: { click: _vm.sort }
            })
          ])
        ]
      ),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "ui-button-content", on: { click: _vm.allSport } },
        [
          _c("div", { staticClass: "button-content" }, [
            _c("div", { staticClass: "content-icon" }),
            _vm._v(" "),
            _c("div", { staticClass: "content-text" }, [
              _vm._v(_vm._s(_vm.translateText.all))
            ])
          ])
        ]
      ),
      _vm._v(" "),
      _vm._l(_vm.sports, function(item) {
        return _c(
          "div",
          {
            key: item[0],
            staticClass: "ui-button-content",
            class: { active: _vm.filters.includes(+item[0]) },
            attrs: { value: item[0] },
            on: {
              click: function($event) {
                return _vm.selectSport(item[0])
              }
            }
          },
          [
            _c(
              "div",
              { staticClass: "button-content", attrs: { id: item[0] } },
              [
                _c("div", {
                  staticClass: "content-icon",
                  class: "icon" || false,
                  style: _vm.setSidebarIcon(item[1])
                }),
                _vm._v(" "),
                _c("div", { staticClass: "content-text" }, [
                  _vm._v(_vm._s(item[1]))
                ])
              ]
            )
          ]
        )
      })
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/results.vue?vue&type=template&id=05085174&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "game-results-container container" },
    [
      _c("div", { staticClass: "results_main_title" }, [
        _c("h1", [_vm._v(_vm._s(_vm.translateText.title))])
      ]),
      _vm._v(" "),
      _c("side-bar", {
        attrs: {
          filters: _vm.filters,
          selectSport: _vm.selectSport,
          setSidebarIcon: _vm.setSidebarIcon,
          translateText: _vm.translateText,
          allSport: _vm.allSport,
          sports: _vm.sportsList,
          sideSearchVal: _vm.sideSearchVal,
          sort: _vm.sort
        },
        on: {
          "update:sideSearchVal": function($event) {
            _vm.sideSearchVal = $event
          },
          "update:side-search-val": function($event) {
            _vm.sideSearchVal = $event
          }
        }
      }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "result_content" },
        [
          _c("filter-block", {
            attrs: {
              isExpand: _vm.isExpand,
              translateText: _vm.translateText,
              allExpand: _vm.allExpand,
              searchVal: _vm.searchVal,
              time: _vm.time,
              live: _vm.live,
              lang: _vm.lang
            },
            on: {
              "update:searchVal": function($event) {
                _vm.searchVal = $event
              },
              "update:search-val": function($event) {
                _vm.searchVal = $event
              },
              "update:time": function($event) {
                _vm.time = $event
              },
              "update:live": function($event) {
                _vm.live = $event
              }
            }
          }),
          _vm._v(" "),
          _c(
            "div",
            { ref: "results", staticClass: "results" },
            _vm._l(_vm.gameGroups, function(list, key) {
              return _c(
                "div",
                { key: key, staticClass: "results_wrap" },
                [
                  _c("div", { staticClass: "result_sport" }, [
                    _c("div", {
                      staticClass: "result_sport_icon",
                      class: "icon" || false,
                      style: _vm.setIcon(list[0].sports_image)
                    }),
                    _vm._v(" "),
                    _c("p", [_vm._v(_vm._s(key))])
                  ]),
                  _vm._v(" "),
                  _vm._l(list, function(result) {
                    return _c("tournament", {
                      key: result.tournament_id,
                      attrs: {
                        isExpand: _vm.isExpand,
                        result: result,
                        timeFilter: _vm.time
                      }
                    })
                  })
                ],
                2
              )
            }),
            0
          )
        ],
        1
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/results/js/tournament.vue?vue&type=template&id=5bf1e149&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { class: { tournament_not_found: !_vm.events.length } }, [
    _vm.events.length
      ? _c(
          "div",
          {
            staticClass: "game-results-tournament",
            class: { checked: _vm.selected }
          },
          [
            _c("div", { staticClass: "result-tournament-title" }, [
              _c("div", { staticClass: "check_wrap" }, [
                _c("input", {
                  attrs: { type: "checkbox", id: _vm.result.tournament_id },
                  on: { click: _vm.toggleCheck }
                }),
                _vm._v(" "),
                _c("label", { attrs: { for: _vm.result.tournament_id } })
              ]),
              _vm._v(
                "\n            " +
                  _vm._s(_vm.result.tournament_title || "") +
                  "\n            "
              ),
              _c("button", {
                staticClass: "open_result",
                class: { active: _vm.isActive },
                on: { click: _vm.toggleResults }
              })
            ]),
            _vm._v(" "),
            _c("transition", { attrs: { name: "slide" } }, [
              _vm.isActive
                ? _c(
                    "div",
                    { staticClass: "result-games-list" },
                    [
                      _c(
                        "transition-group",
                        { attrs: { name: "slide" } },
                        _vm._l(_vm.events, function(game) {
                          return _c(
                            "div",
                            { key: game.id, staticClass: "result-games-item" },
                            [
                              _c("div", {
                                staticClass: "live",
                                class: { active: game.live }
                              }),
                              _vm._v(" "),
                              _c("div", { staticClass: "time" }, [
                                _vm._v(
                                  "\n                            " +
                                    _vm._s(game.live ? "" : game.start_date) +
                                    " \n                            " +
                                    _vm._s(game.start_time) +
                                    "\n                        "
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "title" }, [
                                _vm._v(
                                  "\n                            " +
                                    _vm._s(game.title) +
                                    "\n                        "
                                )
                              ]),
                              _vm._v(" "),
                              _c("div", { staticClass: "result" }, [
                                _vm._v(
                                  "\n                            " +
                                    _vm._s(game.result) +
                                    "\n                        "
                                )
                              ])
                            ]
                          )
                        }),
                        0
                      )
                    ],
                    1
                  )
                : _vm._e()
            ])
          ],
          1
        )
      : _vm._e(),
    _vm._v(" "),
    !_vm.events.length
      ? _c("div", { staticClass: "game-results-tournament" }, [
          _c("div", { staticClass: "result-tournament-title" }, [
            _vm._v("\n            Ничего не найдено\n        ")
          ])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ 4:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./frontend/assets/results/js/results.js");
module.exports = __webpack_require__("./frontend/assets/results/scss/results.sass");


/***/ })

/******/ });
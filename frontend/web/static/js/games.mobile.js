/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"games.mobile": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push([3,"vue"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./frontend/assets/games/js/backend/bets.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return cart; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return cartOnChange; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return cartAddMixin; });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue/dist/vue.runtime.esm.js");
/* harmony import */ var backend_socket__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/backend/socket.js");


var CartInstance = new vue__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]({
  data: {
    items: [],
    bet: 0,
    on_change: 0,
    messageError: '',
    minimal: 0,
    maximal: 0
  },
  created: function created() {
    var _this = this;

    this.load();
    backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"].on('or', function (data) {
      var item = _this.items.find(function (outcomeItem) {
        return outcomeItem.outcome_id === data.o;
      });

      if (item) {
        item.rate_now = data.r;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"].on('oisb', function (data) {
      var item = _this.items.find(function (outcomeItem) {
        return outcomeItem.outcome_id === data.o;
      });

      if (item) {
        item.is_banned = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"].on('oise', function (data) {
      var item = _this.items.find(function (outcomeItem) {
        return outcomeItem.outcome_id === data.o;
      });

      if (item) {
        item.is_enabled = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"].on('oish', function (data) {
      var item = _this.items.find(function (outcomeItem) {
        return outcomeItem.outcome_id === data.o;
      });

      if (item) {
        item.is_hidden = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"].on('oisf', function (data) {
      var item = _this.items.find(function (outcomeItem) {
        return outcomeItem.outcome_id === data.o;
      });

      if (item) {
        item.is_finished = !!data.is;
      }
    });
  },
  methods: {
    processErrors: function processErrors(_ref) {
      var _ref$success = _ref.success,
          success = _ref$success === void 0 ? false : _ref$success,
          _ref$errors = _ref.errors,
          errors = _ref$errors === void 0 ? {} : _ref$errors,
          _ref$message = _ref.message,
          message = _ref$message === void 0 ? '' : _ref$message;
      this.messageError = !success ? message.length > 0 ? message : Object.values(errors).map(function (fieldError) {
        return fieldError.join(', ');
      }).join(', ') : '';
      return success;
    },
    add: function add(id) {
      var _this2 = this;

      if (!this.items.find(function (item) {
        return item.outcome_id === id;
      })) {
        fetch("/games/carts/item-add?outcome=".concat(id)).then(function (result) {
          return result.json();
        }).then(function (result) {
          _this2.processErrors(result);

          _this2.minimal = result.minimal;
          _this2.maximal = result.maximal;
          _this2.bet = result.amount;
          _this2.on_change = result.on_change;

          var prev = _this2.items.findIndex(function (item) {
            return item.game_id === result.item.game_id;
          });

          if (prev !== -1) {
            _this2.items.splice(prev, 1, result.item);
          } else {
            Object(backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* subscribe */ "b"])(result.item.game_id);

            _this2.items.push(result.item);
          }
        });
      }
    },
    confirm: function confirm(id) {
      var _this3 = this;

      fetch("/games/carts/item-confirm?id=".concat(id)).then(function (result) {
        return result.json();
      }).then(function (result) {
        if (!_this3.processErrors(result)) {
          return false;
        }

        _this3.items.find(function (item) {
          return item.id === id;
        }).rate = result.rate;
      }).catch(function (err) {
        console.error(err);
      });
    },
    remove: function remove(id) {
      var _this4 = this;

      fetch("/games/carts/item-remove?id=".concat(id)).then(function (result) {
        return result.json();
      }).then(function (result) {
        if (!_this4.processErrors(result)) {
          return false;
        }

        var idx = _this4.items.findIndex(function (item) {
          return item.id === id;
        });

        if (idx !== -1) {
          var old = _this4.items.splice(idx, 1)[0];

          Object(backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* unsubscribe */ "c"])(old.game_id);
        }
      }).catch(function (err) {
        console.error(err);

        if (!!err.message) {
          _this4.messageError = err.message;
        }
      });
    },
    clean: function clean() {
      var _this5 = this;

      fetch("/games/carts/delete").then(function (result) {
        return result.json();
      }).then(function (result) {
        if (!_this5.processErrors(result)) {
          return false;
        }

        _this5.items = [];
      }).catch(function (err) {
        console.error(err);

        if (!!err.message) {
          _this5.messageError = err.message;
        }
      });
    },
    load: function load() {
      var _this6 = this;

      fetch("/games/carts/view").then(function (result) {
        return result.json();
      }).then(function (result) {
        _this6.processErrors(result);

        (result.items || []).forEach(function (newItem) {
          return Object(backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* subscribe */ "b"])(newItem.game_id);
        });

        _this6.items.forEach(function (oldItem) {
          return Object(backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* unsubscribe */ "c"])(oldItem.game_id);
        });

        _this6.items = result.items || [];
        _this6.bet = result.bet || 0;
        _this6.on_change = result.on_change || 0;
        _this6.minimal = result.minimal || 0;
        _this6.maximal = result.maximal || Number.MAX_SAFE_INTEGER;
      }).catch(function (err) {
        console.error(err);

        if (!!err.message) {
          _this6.messageError = err.message;
        }
      });
    },
    edit: function edit(amount, change) {
      var _this7 = this;

      fetch("/games/carts/edit?amount=".concat(amount || 0, "&on_change=").concat(+change)).then(function (result) {
        return result.json();
      }).then(function (result) {
        _this7.processErrors(result);

        _this7.bet = result.bet || +amount;
        _this7.on_change = result.on_change || +change;
      }).catch(function (err) {
        console.error(err);

        if (!!err.message) {
          _this7.messageError = err.message;
        }
      });
    },
    place: function place(clean, promocode, type) {
      var _this8 = this;

      fetch("/games/carts/place-bet?amount=".concat(+this.bet, "&clean=").concat(+clean, "&promocode=").concat(promocode, "&type=").concat(type)).then(function (result) {
        return result.json();
      }).then(function (result) {
        if (!_this8.processErrors(result)) {
          return false;
        }

        _this8.load();

        reloadUserPurse();
      }).catch(function (err) {
        if (!!err.message) {
          _this8.messageError = err.message;
        }
      });
    }
  }
});
var cart = CartInstance;
var cartOnChange = {
  ON_CHANGE_AGREE: 0,
  ON_CHANGE_CONFIRM: 1,
  ON_CHANGE_ACCEPT_INCREASE: 2
};
var cartAddMixin = {
  methods: {
    addToCart: function addToCart(id) {
      CartInstance.add(id); // $('body').addClass('show_coef_popup'); 
      // setTimeout(() => {
      //     $('body').removeClass('show_coef_popup'); 
      // }, 3000); 
      // const btn = document.getElementsByClassName("scroll_coef");
      // const el = document.getElementsByClassName("side-bar-right");
      // btn[0].addEventListener('click', () => {
      //     el[0].scrollIntoView({block: "start", behavior: "smooth"});
      // })
      // $('.scroll_coef').click(function(){
      //     $('body').removeClass('show_coef_popup');
      //     $('html, body').animate({
      //         scrollTop: $(".side-bar-right").offset().top
      //     }, 2000);
      // });     
    }
  }
};

/***/ }),

/***/ "./frontend/assets/games/js/backend/games.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return games; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return gamesMixin; });
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue/dist/vue.runtime.esm.js");

vue__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].config.devtools = true;

function urlEncode(params) {
  if (!params) {
    return '';
  }

  return Object.keys(params).map(function (k) {
    if (Array.isArray(params[k])) {
      return params[k].map(function (value) {
        return encodeURIComponent(k) + '=' + encodeURIComponent(value);
      }).join('&');
    } else {
      return encodeURIComponent(k) + '=' + encodeURIComponent(params[k]);
    }
  }).join('&');
}

var GamesListClass = vue__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].extend({
  data: function data() {
    return {
      isLive: null,
      sports: []
    };
  },
  created: function created() {
    var _this = this;

    setInterval(function () {
      _this.loadSports();
    }, 60000);
  },
  watch: {
    isLive: function isLive() {
      this.loadSports();
    }
  },
  methods: {
    loadGames: function loadGames(params) {
      params.live = +(params.hasOwnProperty('live') ? params.live : this.isLive);
      var query = urlEncode(params);
      return fetch("/games/events/list?".concat(query)).then(function (result) {
        return result.json();
      }).then(function (data) {
        if (!data.success) {
          throw new Error(data.message);
        }

        return data.events;
      });
    },
    loadSports: function loadSports() {
      var _this2 = this;

      fetch("/games/games/sports?live=".concat(+this.isLive)).then(function (result) {
        return result.json();
      }).then(function (data) {
        if (!data.success) {
          throw new Error(data.message);
        }

        _this2.sports = data.sports.sort(function (a, b) {
          return a.sort_order - b.sort_order;
        }).map(function (sport) {
          sport.tournaments = sport.tournaments.sort(function (a, b) {
            return a.sort_order - b.sort_order;
          });
          return sport;
        });
      }).catch(function (err) {
        console.error(err);
      });
    },
    loadGame: function loadGame(id) {
      return fetch("/games/events/event?id=".concat(id)).then(function (result) {
        return result.json();
      }).then(function (data) {
        if (!data.success) {
          throw new Error(data.message);
        }

        return data;
      });
    },
    loadUpcoming: function loadUpcoming(from, to) {
      return fetch("/games/events/upcoming?from=".concat(from || 0, "&to=").concat(to || 24)).then(function (result) {
        return result.json();
      }).then(function (data) {
        if (!data.success) {
          throw new Error(data.message);
        }

        return data.events;
      });
    }
  }
});
var instance = new GamesListClass();
var games = instance;
var gamesMixin = {
  data: function data() {
    return {
      upcoming: [],
      favourites: []
    };
  },
  computed: {
    isLive: function isLive() {
      return !!instance.isLive;
    },
    sports: function sports() {
      return instance.sports;
    }
  }
};

/***/ }),

/***/ "./frontend/assets/games/js/backend/i18n.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vue_i18n__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-i18n/dist/vue-i18n.esm.js");
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/vue/dist/vue.runtime.esm.js");


vue__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"].use(vue_i18n__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]);
var i18n = new vue_i18n__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]({
  locale: window.$locale,
  fallbackLocale: 'en',
  formatFallbackMessages: true,
  messages: window.$messages
});
/* harmony default export */ __webpack_exports__["a"] = (i18n);

/***/ }),

/***/ "./frontend/assets/games/js/backend/routerMobile.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue/dist/vue.runtime.esm.js");
/* harmony import */ var vue_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./node_modules/vue-router/dist/vue-router.esm.js");
/* harmony import */ var _games__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./frontend/assets/games/js/backend/games.js");
/* harmony import */ var components_mobile_game_listHome_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/listHome.vue");
/* harmony import */ var components_mobile_game_list_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list.vue");
/* harmony import */ var components_mobile_game_game_vue__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/game.vue");
/* harmony import */ var components_container_router_vue__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./frontend/assets/games/js/components/container-router.vue");







vue__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].use(vue_router__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"]);
var router = new vue_router__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"]({
  mode: 'history',
  routes: [{
    path: '/',
    component: components_mobile_game_listHome_vue__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"],
    name: 'live/home',
    beforeEnter: function beforeEnter(to, from, next) {
      next();
    }
  }, {
    path: '/live',
    name: 'live',
    component: components_mobile_game_list_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"],
    beforeEnter: function beforeEnter(to, from, next) {
      _games__WEBPACK_IMPORTED_MODULE_2__[/* games */ "a"].isLive = 1;
      next();
    },
    children: [{
      path: '/live',
      component: components_mobile_game_list_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"],
      name: 'live/home'
    }, {
      path: '/live/sports/:sports_id',
      component: components_mobile_game_list_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"],
      name: 'live/sport'
    }, {
      path: '/live/tournaments/:tournament_id',
      component: components_mobile_game_list_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"],
      name: 'live/tournament'
    }]
  }, {
    path: '/line',
    name: 'line',
    component: components_mobile_game_list_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"],
    beforeEnter: function beforeEnter(to, from, next) {
      _games__WEBPACK_IMPORTED_MODULE_2__[/* games */ "a"].isLive = 0;
      next();
    },
    children: [{
      path: '/line',
      component: components_mobile_game_list_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"],
      name: 'line/home'
    }, {
      path: '/line/sports/:sports_id',
      component: components_mobile_game_list_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"],
      name: 'line/sport'
    }, {
      path: '/line/tournaments/:tournament_id',
      component: components_mobile_game_list_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"],
      name: 'line/tournament'
    }]
  }, {
    path: '/game/:id',
    component: components_mobile_game_game_vue__WEBPACK_IMPORTED_MODULE_5__[/* default */ "a"],
    name: 'game',
    beforeEnter: function beforeEnter(to, from, next) {
      if (_games__WEBPACK_IMPORTED_MODULE_2__[/* games */ "a"].isLive === undefined || _games__WEBPACK_IMPORTED_MODULE_2__[/* games */ "a"].isLive === null) {
        _games__WEBPACK_IMPORTED_MODULE_2__[/* games */ "a"].isLive = 1;
      }

      next();
    }
  }]
});
/* harmony default export */ __webpack_exports__["a"] = (router);

/***/ }),

/***/ "./frontend/assets/games/js/backend/socket.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return connection; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return subscribe; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return unsubscribe; });
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("socket.io-client");
/* harmony import */ var socket_io_client__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(socket_io_client__WEBPACK_IMPORTED_MODULE_0__);

var socket = socket_io_client__WEBPACK_IMPORTED_MODULE_0___default.a.connect(socketGameParams.host + (socketGameParams.port ? ':' + socketGameParams.port : ''), {
  path: socketGameParams.path,
  timeout: 5000,
  reconnectionDelay: 1000,
  reconnectionDelayMax: 1500,
  randomizationFactor: 0
});
var subscriptions = [];
socket.on('connect', function () {
  subscriptions.filter(function (value, index) {
    return subscriptions.indexOf(value) === index;
  }).forEach(function (event) {
    return socket.emit('sub', {
      g: event
    });
  });
});
var connection = socket;
function subscribe(event) {
  if (!subscriptions.includes(event)) {
    socket.emit('sub', {
      g: event
    });
  }

  socket.emit('reload', {
    g: event
  });
  subscriptions.push(event);
}
function unsubscribe(event) {
  var idx = subscriptions.indexOf(event);

  if (idx !== -1) {
    subscriptions.splice(idx, 1);
  }

  if (!subscriptions.includes(event)) {
    socket.emit('unsub', {
      g: event
    });
  }
}

/***/ }),

/***/ "./frontend/assets/games/js/components/container-router.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _container_router_vue_vue_type_template_id_092dddd3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/container-router.vue?vue&type=template&id=092dddd3&");
/* harmony import */ var _container_router_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/container-router.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _container_router_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _container_router_vue_vue_type_template_id_092dddd3___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _container_router_vue_vue_type_template_id_092dddd3___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/container-router.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/container-router.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_container_router_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/container-router.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_container_router_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/container-router.vue?vue&type=template&id=092dddd3&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_container_router_vue_vue_type_template_id_092dddd3___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/container-router.vue?vue&type=template&id=092dddd3&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_container_router_vue_vue_type_template_id_092dddd3___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_container_router_vue_vue_type_template_id_092dddd3___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/game/game-market.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _game_market_vue_vue_type_template_id_6aa6a5d0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/game/game-market.vue?vue&type=template&id=6aa6a5d0&");
/* harmony import */ var _game_market_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/game/game-market.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _game_market_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _game_market_vue_vue_type_template_id_6aa6a5d0___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _game_market_vue_vue_type_template_id_6aa6a5d0___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/game/game-market.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/game/game-market.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_game_market_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/game/game-market.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_game_market_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/game/game-market.vue?vue&type=template&id=6aa6a5d0&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_game_market_vue_vue_type_template_id_6aa6a5d0___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/game/game-market.vue?vue&type=template&id=6aa6a5d0&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_game_market_vue_vue_type_template_id_6aa6a5d0___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_game_market_vue_vue_type_template_id_6aa6a5d0___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/game/ui-one-click-radio.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _ui_one_click_radio_vue_vue_type_template_id_24745145___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/game/ui-one-click-radio.vue?vue&type=template&id=24745145&");
/* harmony import */ var _ui_one_click_radio_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/game/ui-one-click-radio.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _ui_one_click_radio_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _ui_one_click_radio_vue_vue_type_template_id_24745145___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _ui_one_click_radio_vue_vue_type_template_id_24745145___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/game/ui-one-click-radio.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/game/ui-one-click-radio.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ui_one_click_radio_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/game/ui-one-click-radio.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ui_one_click_radio_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/game/ui-one-click-radio.vue?vue&type=template&id=24745145&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ui_one_click_radio_vue_vue_type_template_id_24745145___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/game/ui-one-click-radio.vue?vue&type=template&id=24745145&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ui_one_click_radio_vue_vue_type_template_id_24745145___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ui_one_click_radio_vue_vue_type_template_id_24745145___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/display-mobile.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _display_mobile_vue_vue_type_template_id_6e53becc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/display-mobile.vue?vue&type=template&id=6e53becc&");
/* harmony import */ var _display_mobile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/display-mobile.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _display_mobile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _display_mobile_vue_vue_type_template_id_6e53becc___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _display_mobile_vue_vue_type_template_id_6e53becc___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/display-mobile.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/display-mobile.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_display_mobile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/display-mobile.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_display_mobile_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/display-mobile.vue?vue&type=template&id=6e53becc&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_display_mobile_vue_vue_type_template_id_6e53becc___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/display-mobile.vue?vue&type=template&id=6e53becc&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_display_mobile_vue_vue_type_template_id_6e53becc___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_display_mobile_vue_vue_type_template_id_6e53becc___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/carusel.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _carusel_vue_vue_type_template_id_0cd73d0a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/carusel.vue?vue&type=template&id=0cd73d0a&");
/* harmony import */ var _carusel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/carusel.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _carusel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _carusel_vue_vue_type_template_id_0cd73d0a___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _carusel_vue_vue_type_template_id_0cd73d0a___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/game/carusel.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/carusel.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_carusel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/carusel.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_carusel_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/carusel.vue?vue&type=template&id=0cd73d0a&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_carusel_vue_vue_type_template_id_0cd73d0a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/carusel.vue?vue&type=template&id=0cd73d0a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_carusel_vue_vue_type_template_id_0cd73d0a___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_carusel_vue_vue_type_template_id_0cd73d0a___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/game-market.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _game_market_vue_vue_type_template_id_755f2a59___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/game-market.vue?vue&type=template&id=755f2a59&");
/* harmony import */ var _game_market_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/game-market.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _game_market_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _game_market_vue_vue_type_template_id_755f2a59___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _game_market_vue_vue_type_template_id_755f2a59___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/game/game-market.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/game-market.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_game_market_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/game-market.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_game_market_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/game-market.vue?vue&type=template&id=755f2a59&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_game_market_vue_vue_type_template_id_755f2a59___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/game-market.vue?vue&type=template&id=755f2a59&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_game_market_vue_vue_type_template_id_755f2a59___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_game_market_vue_vue_type_template_id_755f2a59___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/game.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _game_vue_vue_type_template_id_b08f5000___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/game.vue?vue&type=template&id=b08f5000&");
/* harmony import */ var _game_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/game.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _game_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _game_vue_vue_type_template_id_b08f5000___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _game_vue_vue_type_template_id_b08f5000___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/game/game.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/game.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_game_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/game.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_game_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/game.vue?vue&type=template&id=b08f5000&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_game_vue_vue_type_template_id_b08f5000___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/game.vue?vue&type=template&id=b08f5000&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_game_vue_vue_type_template_id_b08f5000___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_game_vue_vue_type_template_id_b08f5000___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list-item.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _list_item_vue_vue_type_template_id_400187c4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list-item.vue?vue&type=template&id=400187c4&");
/* harmony import */ var _list_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list-item.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _list_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _list_item_vue_vue_type_template_id_400187c4___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _list_item_vue_vue_type_template_id_400187c4___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/game/list-item.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list-item.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_list_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-item.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_list_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list-item.vue?vue&type=template&id=400187c4&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_item_vue_vue_type_template_id_400187c4___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-item.vue?vue&type=template&id=400187c4&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_item_vue_vue_type_template_id_400187c4___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_item_vue_vue_type_template_id_400187c4___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list-itemHome.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _list_itemHome_vue_vue_type_template_id_449a397a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list-itemHome.vue?vue&type=template&id=449a397a&");
/* harmony import */ var _list_itemHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list-itemHome.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _list_itemHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _list_itemHome_vue_vue_type_template_id_449a397a___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _list_itemHome_vue_vue_type_template_id_449a397a___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/game/list-itemHome.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list-itemHome.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_list_itemHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-itemHome.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_list_itemHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list-itemHome.vue?vue&type=template&id=449a397a&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_itemHome_vue_vue_type_template_id_449a397a___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-itemHome.vue?vue&type=template&id=449a397a&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_itemHome_vue_vue_type_template_id_449a397a___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_itemHome_vue_vue_type_template_id_449a397a___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list-itemSearch.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _list_itemSearch_vue_vue_type_template_id_1bb1248c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list-itemSearch.vue?vue&type=template&id=1bb1248c&");
/* harmony import */ var _list_itemSearch_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list-itemSearch.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _list_itemSearch_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _list_itemSearch_vue_vue_type_template_id_1bb1248c___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _list_itemSearch_vue_vue_type_template_id_1bb1248c___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/game/list-itemSearch.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list-itemSearch.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_list_itemSearch_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-itemSearch.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_list_itemSearch_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list-itemSearch.vue?vue&type=template&id=1bb1248c&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_itemSearch_vue_vue_type_template_id_1bb1248c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-itemSearch.vue?vue&type=template&id=1bb1248c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_itemSearch_vue_vue_type_template_id_1bb1248c___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_itemSearch_vue_vue_type_template_id_1bb1248c___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _list_vue_vue_type_template_id_616086ac___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list.vue?vue&type=template&id=616086ac&");
/* harmony import */ var _list_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _list_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _list_vue_vue_type_template_id_616086ac___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _list_vue_vue_type_template_id_616086ac___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/game/list.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/list.vue?vue&type=template&id=616086ac&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_616086ac___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list.vue?vue&type=template&id=616086ac&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_616086ac___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_list_vue_vue_type_template_id_616086ac___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/listHome.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _listHome_vue_vue_type_template_id_28282e2b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/listHome.vue?vue&type=template&id=28282e2b&");
/* harmony import */ var _listHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/listHome.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _listHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _listHome_vue_vue_type_template_id_28282e2b___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _listHome_vue_vue_type_template_id_28282e2b___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/game/listHome.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/listHome.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_listHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/listHome.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_listHome_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/listHome.vue?vue&type=template&id=28282e2b&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_listHome_vue_vue_type_template_id_28282e2b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/listHome.vue?vue&type=template&id=28282e2b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_listHome_vue_vue_type_template_id_28282e2b___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_listHome_vue_vue_type_template_id_28282e2b___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _ui_one_click_set_vue_vue_type_template_id_7c5e760b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue?vue&type=template&id=7c5e760b&");
/* harmony import */ var _ui_one_click_set_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _ui_one_click_set_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _ui_one_click_set_vue_vue_type_template_id_7c5e760b___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _ui_one_click_set_vue_vue_type_template_id_7c5e760b___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ui_one_click_set_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_ui_one_click_set_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue?vue&type=template&id=7c5e760b&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ui_one_click_set_vue_vue_type_template_id_7c5e760b___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue?vue&type=template&id=7c5e760b&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ui_one_click_set_vue_vue_type_template_id_7c5e760b___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_ui_one_click_set_vue_vue_type_template_id_7c5e760b___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/right/cart.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _cart_vue_vue_type_template_id_3fab831c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/right/cart.vue?vue&type=template&id=3fab831c&");
/* harmony import */ var _cart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/right/cart.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _cart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _cart_vue_vue_type_template_id_3fab831c___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _cart_vue_vue_type_template_id_3fab831c___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/right/cart.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/right/cart.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_cart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/right/cart.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_cart_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/right/cart.vue?vue&type=template&id=3fab831c&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_cart_vue_vue_type_template_id_3fab831c___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/right/cart.vue?vue&type=template&id=3fab831c&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_cart_vue_vue_type_template_id_3fab831c___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_cart_vue_vue_type_template_id_3fab831c___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/right/casino.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _casino_vue_vue_type_template_id_0d449647___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/right/casino.vue?vue&type=template&id=0d449647&");
/* harmony import */ var _casino_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/mobile/right/casino.vue?vue&type=script&lang=js&");
/* unused harmony reexport * */
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _casino_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _casino_vue_vue_type_template_id_0d449647___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _casino_vue_vue_type_template_id_0d449647___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/mobile/right/casino.vue"
/* unused harmony default export */ var _unused_webpack_default_export = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/right/casino.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_casino_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/right/casino.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_casino_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/mobile/right/casino.vue?vue&type=template&id=0d449647&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_casino_vue_vue_type_template_id_0d449647___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/right/casino.vue?vue&type=template&id=0d449647&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_casino_vue_vue_type_template_id_0d449647___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_casino_vue_vue_type_template_id_0d449647___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/components/right/cart-item.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _cart_item_vue_vue_type_template_id_618d35c2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/right/cart-item.vue?vue&type=template&id=618d35c2&");
/* harmony import */ var _cart_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/right/cart-item.vue?vue&type=script&lang=js&");
/* harmony import */ var _node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-loader/lib/runtime/componentNormalizer.js");





/* normalize component */

var component = Object(_node_modules_vue_loader_lib_runtime_componentNormalizer_js__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"])(
  _cart_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
  _cart_item_vue_vue_type_template_id_618d35c2___WEBPACK_IMPORTED_MODULE_0__[/* render */ "a"],
  _cart_item_vue_vue_type_template_id_618d35c2___WEBPACK_IMPORTED_MODULE_0__[/* staticRenderFns */ "b"],
  false,
  null,
  null,
  null
  
)

/* hot reload */
if (false) { var api; }
component.options.__file = "frontend/assets/games/js/components/right/cart-item.vue"
/* harmony default export */ __webpack_exports__["a"] = (component.exports);

/***/ }),

/***/ "./frontend/assets/games/js/components/right/cart-item.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_cart_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/right/cart-item.vue?vue&type=script&lang=js&");
 /* harmony default export */ __webpack_exports__["a"] = (_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_babel_loader_lib_index_js_ref_1_2_node_modules_vue_loader_lib_index_js_vue_loader_options_cart_item_vue_vue_type_script_lang_js___WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]); 

/***/ }),

/***/ "./frontend/assets/games/js/components/right/cart-item.vue?vue&type=template&id=618d35c2&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_cart_item_vue_vue_type_template_id_618d35c2___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/right/cart-item.vue?vue&type=template&id=618d35c2&");
/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "a", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_cart_item_vue_vue_type_template_id_618d35c2___WEBPACK_IMPORTED_MODULE_0__["a"]; });

/* harmony reexport (safe) */ __webpack_require__.d(__webpack_exports__, "b", function() { return _node_modules_vue_loader_lib_loaders_templateLoader_js_vue_loader_options_node_modules_cache_loader_dist_cjs_js_node_modules_thread_loader_dist_cjs_js_node_modules_vue_loader_lib_index_js_vue_loader_options_cart_item_vue_vue_type_template_id_618d35c2___WEBPACK_IMPORTED_MODULE_0__["b"]; });



/***/ }),

/***/ "./frontend/assets/games/js/games-mobile.js":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue/dist/vue.runtime.esm.js");
/* harmony import */ var backend_i18n__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/backend/i18n.js");
/* harmony import */ var _backend_routerMobile__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./frontend/assets/games/js/backend/routerMobile.js");
/* harmony import */ var components_mobile_display_mobile__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./frontend/assets/games/js/components/mobile/display-mobile.vue");





(function () {
  var instance = new vue__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]({
    el: '#games-console',
    i18n: backend_i18n__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
    router: _backend_routerMobile__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"],
    render: function render(createElement) {
      return createElement(components_mobile_display_mobile__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"]);
    }
  });
  _backend_routerMobile__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"].beforeEach(function (to, from, next) {
    jQuery(instance.$el).addClass('loading');
    next();
  });
  _backend_routerMobile__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"].afterEach(function () {
    jQuery(instance.$el).removeClass('loading');
  });
})();

/***/ }),

/***/ "./frontend/assets/games/scss/games.sass":
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/container-router.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
/* harmony default export */ __webpack_exports__["a"] = ({
  name: "container-router"
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/game/game-market.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var backend_bets__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/backend/bets.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  mixins: [backend_bets__WEBPACK_IMPORTED_MODULE_0__[/* cartAddMixin */ "b"]],
  data: function data() {
    return {
      listOpened: true
    };
  },
  name: "game-market",
  props: ['market'],
  computed: {
    outcomes: function outcomes() {
      return this.market.outcomes.filter(function (outcome) {
        return outcome.is_enabled && !outcome.is_finished;
      });
    }
  },
  methods: {
    setShort: function setShort(item) {
      if (('' + item).length > 4) {
        return item.toFixed(3);
      }

      return item;
    },
    toggleList: function toggleList() {
      this.listOpened = !this.listOpened;
    }
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/game/ui-one-click-radio.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
/* harmony default export */ __webpack_exports__["a"] = ({
  props: ['title'],
  data: function data() {
    return {
      radio: false
    };
  },
  methods: {
    radioSet: function radioSet(result) {
      var _this = this;

      this.radio = result;

      if (this.radio === true) {
        setTimeout(function () {
          _this.$refs.input.focus();
        }, 0);
      }
    }
  },
  watch: {
    radio: function radio() {// if(this.radio){
      //   this.$refs.radio.classList.remove('radio-icon-default');
      //   this.$refs.radio.classList.add('radio-icon-active');
      // }else{
      //   this.$refs.radio.classList.remove('radio-icon-active');
      //   this.$refs.radio.classList.add('radio-icon-default');
      // }
    }
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/display-mobile.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var components_mobile_right_cart__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/components/mobile/right/cart.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  components: {
    cart: components_mobile_right_cart__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]
  },
  name: "display-mobile",
  data: function data() {
    return {
      cartShow: false,
      count: null
    };
  },
  methods: {
    openCart: function openCart() {
      this.cartShow = true;
    },
    cartHide: function cartHide(close) {
      this.cartShow = close;
    },
    cartopen: function cartopen(bool) {
      this.cartShow = bool;
    }
  },
  watch: {
    cartShow: function cartShow() {
      var _this = this;

      fetch("/games/carts/view").then(function (result) {
        return result.json();
      }).then(function (result) {
        _this.count = result.items.length;
      }).catch(function (err) {
        console.error(err);
      });
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    fetch("/games/carts/view").then(function (result) {
      return result.json();
    }).then(function (result) {
      _this2.count = result.items.length;
    }).catch(function (err) {
      console.error(err);
    });
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/carusel.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue-slick/dist/slickCarousel.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  name: "carusel",
  data: function data() {
    return {
      slickOptions: {
        slidesToShow: 1,
        infinite: false,
        arrows: false,
        dots: true
      },
      translateText: {},
      logined: false
    };
  },
  components: {
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"]
  },
  mounted: function mounted() {
    var _this = this;

    var button = document.querySelectorAll('.arrow-hide-side-bar');
    button.forEach(function (item) {
      item.addEventListener('click', function () {
        setTimeout(function () {
          _this.$refs.carusel.reSlick();
        }, 300);
      });
    }); // console.log(window.socketGameParams.loggedIn);

    this.logined = window.socketGameParams.loggedIn;
  },
  created: function created() {
    var html = document.documentElement.getAttribute("lang");

    if (html == 'ru') {
      return this.translateText = {
        btnTxt: "Сделать ставку"
      };
    } else {
      return this.translateText = {
        btnTxt: "PLACE A BET"
      };
    }
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/game-market.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var backend_bets__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/backend/bets.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  mixins: [backend_bets__WEBPACK_IMPORTED_MODULE_0__[/* cartAddMixin */ "b"]],
  name: "game-market",
  props: ['market'],
  computed: {
    outcomes: function outcomes() {
      return this.market.outcomes.filter(function (outcome) {
        return outcome.is_enabled && !outcome.is_finished;
      });
    }
  },
  methods: {
    showCart: function showCart(bool) {
      this.$emit('showcart', bool);
    },
    setShort: function setShort(item) {
      if (('' + item).length > 4) {
        return item.toFixed(3);
      }

      return item;
    }
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/game.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue/dist/vue.runtime.esm.js");
/* harmony import */ var backend_games__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/backend/games.js");
/* harmony import */ var backend_bets__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./frontend/assets/games/js/backend/bets.js");
/* harmony import */ var backend_socket__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./frontend/assets/games/js/backend/socket.js");
/* harmony import */ var components_mobile_game_game_market_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/game-market.vue");
/* harmony import */ var vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./node_modules/vue-custom-scrollbar/dist/vueScrollbar.umd.min.js");
/* harmony import */ var vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var components_mobile_game_ui_one_click_set_vue__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue");
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__("./node_modules/vue-slick/dist/slickCarousel.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//








/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].component('container-game', {
  mixins: [backend_bets__WEBPACK_IMPORTED_MODULE_2__[/* cartAddMixin */ "b"], backend_games__WEBPACK_IMPORTED_MODULE_1__[/* gamesMixin */ "b"]],
  components: {
    market: components_mobile_game_game_market_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"],
    vueCustomScrollbar: vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5___default.a,
    'ui-one-click-set': components_mobile_game_ui_one_click_set_vue__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"],
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_7__[/* default */ "a"]
  },
  data: function data() {
    return {
      loading: true,
      currentGroup: null,
      info: {},
      marketsList: [],
      groupsList: [],
      outcomesList: [],
      search: null,
      duration: 0,
      columns: 'col1',
      showVideo: false,
      extends_res: {},
      participants: [],
      nameHeight: '',
      timer: '',
      period: '',
      isRunning: true,
      index: null,
      indexPage: null,
      startDate: '',
      startTime: '',
      translateText: {},
      isLived: true,
      slickOptions: {
        infinite: false,
        variableWidth: true,
        swipeToSlide: true
      },
      settings: {
        suppressScrollY: true,
        maxScrollbarLength: 100,
        swicher: true
      },
      extendedResultsMap: {
        'Yellow cards': '<img src="/static/img/carts/yellow_cards.png" />',
        'Red cards': '<img src="/static/img/carts/red_cards.png" />',
        'Penalties': '<img src="/static/img/carts/penalties.png" />',
        'Corners': '<img src="/static/img/carts/corners.png" />'
      }
    };
  },
  computed: {
    getList: function getList() {
      var list = backend_games__WEBPACK_IMPORTED_MODULE_1__[/* games */ "a"].sports;
      return list;
    },
    setLanguage: function setLanguage() {
      var html = document.documentElement.getAttribute("lang");

      if (html == 'ru') {
        return 'Все';
      } else {
        return 'All';
      }
    },
    visible: function visible() {
      return !this.info.is_finished && this.info.is_enabled && !this.info.is_hidden && !this.info.is_banned;
    },
    durationFormatted: function durationFormatted() {
      if (this.duration < 0 || this.duration === null) {
        return '00:00';
      } else if (this.info.is_finished) {
        return '';
      }

      var left = (this.duration / 60).toString().padStart(2, '0').split(".");
      return left[0] + ':' + (this.duration % 60).toString().padStart(2, '0');
    },
    results: function results() {
      return this.info.result;
    },
    resultExtendedFootball: function resultExtendedFootball() {
      var _this = this;

      var results = {};
      Object.keys(this.extends_res).forEach(function (key) {
        var value = _this.extends_res[key];

        if (_this.extendedResultsMap.hasOwnProperty(key)) {
          results[_this.extendedResultsMap[key]] = value;
        } else if (_this.$te(key)) {
          results[_this.$t(key)] = value;
        } else {
          results[key] = value;
        }
      });
      delete results['1/2'];
      delete results['2/2'];
      return results;
    },
    resultExtended: function resultExtended() {
      var _this2 = this;

      var results = {};
      Object.keys(this.extends_res).forEach(function (key) {
        var value = _this2.extends_res[key];

        if (_this2.extendedResultsMap.hasOwnProperty(key)) {
          results[_this2.extendedResultsMap[key]] = value;
        } else if (_this2.$te(key)) {
          results[_this2.$t(key)] = value;
        } else {
          results[key] = value;
        }
      });
      delete results['<img src="/static/img/carts/corners.png" />'];
      delete results['<img src="/static/img/carts/penalties.png" />'];
      delete results['<img src="/static/img/carts/red_cards.png" />'];
      delete results['<img src="/static/img/carts/yellow_cards.png" />'];
      return results;
    },
    sport: function sport() {
      var _this3 = this;

      return this.sports.find(function (item) {
        return item.id === _this3.info.sports_id;
      }) || {};
    },
    outcomes: function outcomes() {
      return this.outcomesList.filter(function (item) {
        return item.is_enabled && !item.is_finished && !item.is_banned && !item.is_hidden;
      });
    },
    markets: function markets() {
      var _this4 = this;

      var markets = this.marketsList.filter(function (item) {
        return item.group_id === _this4.currentGroup;
      }).map(function (item) {
        return {
          id: item.id,
          title: item.title,
          outcomes: _this4.outcomes.filter(function (outcome) {
            return outcome.market_id === item.id;
          }),
          group_id: item.group_id,
          sort_order: item.sort_order
        };
      }).filter(function (item) {
        return item.outcomes.length > 0;
      });

      if (this.search && this.search.length > 0) {
        markets = markets.reduce(function (a, e, i) {
          if (e.title.toLowerCase().includes(_this4.search.toLowerCase())) {
            a.push(e);
          } else {
            var outcomes = e.outcomes.reduce(function (array, el) {
              if (el.title.toLowerCase().includes(_this4.search.toLowerCase())) {
                array.push(el);
              }

              return array;
            }, []);

            if (outcomes.length > 0) {
              a.push({
                outcomes: outcomes,
                id: e.id,
                title: e.title
              });
            }
          }

          return a;
        }, []);
      }

      if (this.columns === 'col3') {
        var array = markets;
        var half_length = Math.ceil(array.length / 3);
        var left = array.splice(0, half_length);
        var center = array.splice(0, markets.length % half_length === 0 ? half_length : half_length - 1);
        var right = array.splice(0, markets.length % half_length === 0 ? half_length : half_length - 1);
        markets = {
          left: left,
          center: center,
          right: right
        };
        return markets;
      } else if (this.columns === 'col2') {
        var _array = markets;

        var _half_length = Math.ceil(_array.length / 2);

        var leftSide = _array.slice(0, _half_length);

        var rightSide = _array.slice(_half_length);

        markets = {
          leftSide: leftSide,
          rightSide: rightSide
        };
        return markets;
      } else {
        return markets.sort(function (a, b) {
          return b.sort_order - a.sort_order;
        });
      }
    },
    groups: function groups() {
      var _this5 = this;

      return this.groupsList.filter(function (group) {
        return _this5.marketsList.some(function (item) {
          return group.id === item.group_id && _this5.outcomes.some(function (outcome) {
            return outcome.market_id === item.id;
          });
        });
      });
    },
    userLoggedIn: function userLoggedIn() {
      return !!window.socketGameParams.loggedIn;
    },
    videoExists: function videoExists() {
      return !!this.info.video_link;
    },
    playerLink: function playerLink() {
      return 'userID=null&videoID=' + this.info.video_link + '&matchName= &ref=8';
    }
  },
  watch: {
    getList: function getList() {
      var _this6 = this;

      this.loading = false;
      setTimeout(function () {
        return _this6.loading = true;
      }, 300);
    }
  },
  methods: {
    showcart: function showcart(bool) {
      this.$emit('cartopen', bool);
    },
    setIcon: function setIcon(image) {
      return image !== null ? {
        'background-image': "url('/uploads/games/".concat(image, "')")
      } : {};
    },
    setTab: function setTab(index) {
      this.currentGroup = index;
      this.columns = "col1";
    },
    clearSearch: function clearSearch() {
      this.search = null;
    },
    setData: function setData(game, target) {
      if (!!target.info.id) {
        Object(backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* unsubscribe */ "c"])(target.info.id);
      }

      target.info = {
        id: game.id,
        sports_id: game.sports_id || null,
        tournament_id: game.tournament_id || null,
        tournament_title: game.tournament_title || '',
        starts_at: game.starts_at || null,
        is_finished: game.is_finished || false,
        is_hidden: game.is_hidden || false,
        is_banned: game.is_banned || false,
        is_enabled: game.is_enabled || false,
        title: game.title || '',
        result: game.result || '',
        description: game.description || '',
        meta_keywords: game.meta_keywords || '',
        meta_description: game.meta_description || '',
        video_link: game.video_link
      };
      target.startDate = game.starts_date;
      target.startTime = game.starts_time;
      target.isLived = game.is_live;
      target.extends_res = game.result_extended;
      target.participants = game.participants;
      target.duration = parseInt(game.timer.split(":")[0]) * 60 + parseInt(game.timer.split(":")[1]) || null;
      target.period = game.game_period;
      target.index = game.tournament_id;
      target.indexPage = game.id;
      target.groupsList = game.groups;
      target.marketsList = game.markets;
      target.outcomesList = game.outcomes.map(function (event) {
        return Object.assign(event, {
          change: false
        });
      });
      target.groupsList.forEach(function (group) {
        group.additional.forEach(function (additional) {
          var market = target.marketsList.find(function (item) {
            return item.id === additional.market_id;
          });

          if (market) {
            target.marketsList.push(Object.assign({}, market, {
              group_id: group.id,
              sort_order: additional.sort_order
            }));
          }
        });
      });
      target.currentGroup = target.groups[0].id || null;

      if (game.video_link) {
        this.$nextTick(function () {
          target.videoSetAttributes();
        });
      }

      Object(backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* subscribe */ "b"])(game.id);
    },
    getOutcome: function getOutcome(game, outcome) {
      if (this.outcomesList.length === 0 || this.info.id !== +game) {
        return;
      }

      return this.outcomesList.find(function (outcomeItem) {
        return outcomeItem.id === +outcome;
      });
    },
    videoSetAttributes: function videoSetAttributes() {
      var width = this.$refs.videoContainer.offsetWidth;
      this.$refs.videoPlayer.setAttribute('height', width * .75);
      this.$refs.videoPlayer.setAttribute('width', width / 2);
      this.$refs.videoContainer.style.height = "".concat(width * .5, "px");
    },
    setBg: function setBg(image) {
      return image !== undefined ? {
        'background-image': "url('/static/img/backgrounds/".concat(image, ".jpg')")
      } : {};
    },
    setColumns: function setColumns(col) {
      return this.columns = col;
    },
    // toggleVideo() {
    //     this.recalculateNamesHeights();
    //     $('.id_video_obj').each((id, el) => {
    //         if ($(el).attr('value') === this.playerLink) {
    //             $(el).parents('.video_sidebar').hide();
    //         }
    //     });
    //     return this.showVideo = !this.showVideo;
    // },
    // recalculateNamesHeights() {
    //     if (this.$refs.nameLeft.offsetHeight > this.$refs.nameRight.offsetHeight) {
    //         this.nameHeight = `${this.$refs.nameLeft.offsetHeight}px`;
    //     } else if (this.$refs.nameLeft.offsetHeight < this.$refs.nameRight.offsetHeight) {
    //         this.nameHeight = `${this.$refs.nameRight.offsetHeight}px`;
    //     } else {
    //         this.nameHeight = 'auto';
    //     }
    // },
    videoToSidebar: function videoToSidebar() {
      // console.log(this.playerLink);
      // console.log($('#video_sidebar'));
      // $('#video_sidebar').show();
      // $('#urlParam').attr('value', this.playerLink);
      // const el = $('#BridgeMovie2').html();
      // $('#BridgeMovie2').html(el);
      this.toggleVideo(); // if(window.innerWidth <= 1000){
      //     $('.video_sidebar').show();
      //     $('.id_video_obj').attr('value', this.playerLink);
      //     const el = $('.video_sidebar_content object').html();
      //     $('.video_sidebar_content object').html(el);
      // } else{
      //     }

      $('.video_sidebar_wrap').prepend("\n                <div class=\"video_sidebar\">\n                    <div class=\"ui-button\">\n                        <div class=\"text\">\n                            <span>".concat(this.translateText.video, "</span>\n                        </div>\n                        <div class=\"ui-button-icon icon-close\"></div>                \n                    </div>\n                    <div class=\"video_sidebar_content\">\n                        <object type=\"application/x-shockwave-flash\" data=\"/static/player.swf\">\n                            <param name=\"menu\" value=\"false\">\n                            <param name=\"wmode\" value=\"opaque\">\n                            <param name=\"allowFullScreen\" value=\"true\">\n                            <param name=\"AllowScriptAccess\" value=\"always\">\n                            <param name=\"flashvars\" class=\"id_video_obj\" value=\"").concat(this.playerLink, "\">\n                        </object>\n\n                        <div class=\"without_reg without_reg_side hide\">\n                            <div class=\"btns_wrap\">\n                                <a href=\"/user/auth/login\" class=\"ui-radius-button log-in\" data-toggle=\"modal\" data-target=\"#loginPopup\">").concat(this.translateText.login, "</a>\n                                <a href=\"/user/auth/register\" class=\"ui-radius-button\">").concat(this.translateText.reg, "</a>\n                            </div>\n                        </div>\n                    </div>\n                </div>\n            "));
      $('.icon-close').click(function () {
        $(this).parent('.ui-button').parent('.video_sidebar').remove();
      });
    }
  },
  created: function created() {
    var _this7 = this;

    backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* connection */ "a"].on('or', function (data) {
      var outcome = _this7.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.rate = data.r;

        if (outcome.rate < data.r) {
          outcome.change = 'change-coeff-active-up';
        } else if (result.rate > data.r) {
          outcome.change = 'change-coeff-active-down';
        } else {
          outcome.change = '';
        }
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* connection */ "a"].on('os', function (data) {// status
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* connection */ "a"].on('oisb', function (data) {
      var outcome = _this7.getOutcome(data.g, data.o, data);

      if (outcome) {
        outcome.is_banned = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* connection */ "a"].on('oise', function (data) {
      var outcome = _this7.getOutcome(data.g, data.o, data);

      if (outcome) {
        outcome.is_enabled = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* connection */ "a"].on('oish', function (data) {
      var outcome = _this7.getOutcome(data.g, data.o, data);

      if (outcome) {
        outcome.is_hidden = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* connection */ "a"].on('oisf', function (data) {
      var outcome = _this7.getOutcome(data.g, data.o, data);

      if (outcome) {
        outcome.is_finished = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* connection */ "a"].on('gres', function (data) {
      if (data.g === _this7.info.id) {
        console.log('gres', data);
        _this7.info.result = data.rs;
        _this7.extends_res = data.re;
        if (data.gp !== undefined) _this7.info.gamePeriod = data.gp;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* connection */ "a"].on('gstats', function (data) {
      console.log('gstats', data, data.g, _this7.info.id, data.g === _this7.info.id);

      if (data.g === _this7.info.id) {
        if (data.f !== undefined) _this7.info.is_finished = !!data.f;
        if (data.e !== undefined) _this7.info.is_enabled = !!data.e;
        if (data.h !== undefined) _this7.info.is_hidden = !!data.h;
        if (data.b !== undefined) _this7.info.is_banned = !!data.b;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* connection */ "a"].on('gtimer', function (data) {
      if (data.g !== _this7.info.id) {
        return;
      }

      if (data.t !== undefined) {
        var timer = data.t;
        _this7.duration = parseInt(timer.split(":")[0]) * 60 + parseInt(timer.split(":")[1]) || null;
      }

      if (data.r !== undefined) _this7.isRunning = !!data.r;
    });
    setTimeout(function () {
      jQuery('html, body').animate({
        scrollTop: jQuery("body").offset().top
      }, 1000);
    }, 300);
    var html = document.documentElement.getAttribute("lang");

    if (html == 'ru') {
      return this.translateText = {
        started: "Игра начнется",
        login: "Войти",
        reg: "Регистрация",
        time: "Время матча",
        pause: "Пауза",
        finished: "Игра окончена",
        video: "Видео"
      };
    } else {
      return this.translateText = {
        started: "The game will begin",
        login: "LogIn",
        reg: "Registration",
        time: "Match time",
        pause: "Pause",
        finished: "Game is finished",
        video: "Video"
      };
    }
  },
  beforeDestroy: function beforeDestroy() {
    Object(backend_socket__WEBPACK_IMPORTED_MODULE_3__[/* unsubscribe */ "c"])(this.info.id);

    if (window.innerWidth <= 550) {
      document.getElementById('mob_banner').classList.add("banner_showed");
    }
  },
  mounted: function mounted() {
    var _this8 = this;

    // if(window.innerWidth <= 1000){
    //         this.settings.swicher = false;
    // } else {
    //     this.settings.swicher = true;
    // }
    // document.getElementById('mob_banner').style.display = "none";
    // document.getElementById('mob_banner').classList.remove("banner_showed");
    // window.addEventListener('resize', () => {
    //     setTimeout(() => {
    //         this.recalculateNamesHeights();
    //     }, 50);
    //     this.videoSetAttributes();
    // });
    // setTimeout(() => {
    //     this.recalculateNamesHeights();
    // }, 50);
    clearInterval(this.durationInterval);
    this.durationInterval = setInterval(function () {
      if (!_this8.isRunning) {
        return;
      }

      _this8.duration++;
    }, 1000);
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    backend_games__WEBPACK_IMPORTED_MODULE_1__[/* games */ "a"].loadGame(to.params.id).then(function (game) {
      next(function (vm) {
        vm.columns = 'col1';
        vm.setData(game, vm);
      });
    }).catch(function (err) {
      next(err);
    });

    if (jQuery(window).width() < 1000) {
      jQuery('.bacground-content').slideUp(300, function () {
        jQuery('.button-content').removeClass('button-active');
      });
    }
  },
  beforeRouteUpdate: function beforeRouteUpdate(to, from, next) {
    var _this9 = this;

    this.showVideo = false;
    backend_games__WEBPACK_IMPORTED_MODULE_1__[/* games */ "a"].loadGame(to.params.id).then(function (game) {
      _this9.setData(game, _this9);

      _this9.columns = 'col1';
      next();
    }).catch(function (err) {
      next(err);
    });

    if (jQuery(window).width() < 1000) {
      jQuery('.bacground-content').slideUp(300, function () {
        jQuery('.button-content').removeClass('button-active');
      });
    }
  }
}));

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-item.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue2-flip-countdown/dist/vue2-flip-countdown.js");
/* harmony import */ var vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backend_games__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/backend/games.js");
/* harmony import */ var backend_socket__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./frontend/assets/games/js/backend/socket.js");
/* harmony import */ var components_game_game_market_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./frontend/assets/games/js/components/game/game-market.vue");
/* harmony import */ var backend_bets__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./frontend/assets/games/js/backend/bets.js");
/* harmony import */ var vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./node_modules/vue-custom-scrollbar/dist/vueScrollbar.umd.min.js");
/* harmony import */ var vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["a"] = ({
  name: "games-list-item",
  components: {
    market: components_game_game_market_vue__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"],
    vueCustomScrollbar: vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5___default.a,
    FlipCountdown: vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  props: ['game', 'live'],
  mixins: [backend_bets__WEBPACK_IMPORTED_MODULE_4__[/* cartAddMixin */ "b"]],
  data: function data() {
    return {
      timer: false,
      timerTo: '',
      connection: backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"],
      marketsList: [],
      outcomesList: [],
      settings: {
        wheelSpeed: 0.1,
        wheelPropagation: false
      },
      videoId: false
    };
  },
  computed: {
    outcomes: function outcomes() {
      return this.outcomesList.filter(function (item) {
        return item.is_enabled && !item.is_finished && !item.is_banned && !item.is_hidden;
      });
    },
    markets: function markets() {
      var _this = this;

      return this.marketsList.map(function (item) {
        return {
          id: item.id,
          title: item.title,
          group: item.group_id,
          outcomes: _this.outcomes.filter(function (outcome) {
            return outcome.market_id === item.id;
          })
        };
      }).filter(function (item) {
        return item.outcomes.length > 0;
      });
    }
  },
  mounted: function mounted() {
    if (this.live === 0) {
      this.timer = true;
      this.timerTo = this.game.start_date + ' ' + this.game.start_time + ':00';
    }

    ;
  },
  created: function created() {
    var _this2 = this;

    // console.log(this.game.participants);
    this.videoId = this.game.video_link; // console.log(this.videoId);

    Object(backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* subscribe */ "b"])(this.game.id);
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('or', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.rate = data.r;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('os', function (data) {// status
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('oisb', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_banned = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('oise', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_enabled = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('oish', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_hidden = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('oisf', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_finished = !!data.is;
      }
    });
  },
  beforeDestroy: function beforeDestroy() {
    Object(backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* unsubscribe */ "c"])(this.game.id);
  },
  methods: {
    showCart: function showCart(bool) {
      this.$emit('showcart', bool);
    },
    getOutcome: function getOutcome(game, outcome) {
      if (this.outcomesList.length === 0 || this.game.id !== game) {
        return;
      }

      return this.outcomesList.find(function (outcomeItem) {
        return outcomeItem.id === outcome;
      });
    },
    openContent: function openContent() {
      var _this3 = this;

      var button = this.$refs.button;

      if (button.classList.contains('show-button-default')) {
        button.classList.remove('show-button-default');
        button.classList.add('show-button-active');
        this.$refs.content.classList.add('opened');

        if (this.marketsList.length === 0) {
          this.$refs.content.classList.add('loading');
          backend_games__WEBPACK_IMPORTED_MODULE_1__[/* games */ "a"].loadGame(this.game.id).then(function (game) {
            _this3.marketsList = game.markets;
            _this3.outcomesList = game.outcomes;

            _this3.$refs.content.classList.remove('loading');
          }).catch(function (err) {
            console.error(err);
          });
        }
      } else {
        button.classList.remove('show-button-active');
        button.classList.add('show-button-default');
        this.$refs.content.classList.remove('opened');
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-itemHome.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue2-flip-countdown/dist/vue2-flip-countdown.js");
/* harmony import */ var vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var backend_games__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/backend/games.js");
/* harmony import */ var backend_socket__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./frontend/assets/games/js/backend/socket.js");
/* harmony import */ var components_mobile_game_game_market_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/game-market.vue");
/* harmony import */ var backend_bets__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./frontend/assets/games/js/backend/bets.js");
/* harmony import */ var vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./node_modules/vue-custom-scrollbar/dist/vueScrollbar.umd.min.js");
/* harmony import */ var vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//






/* harmony default export */ __webpack_exports__["a"] = ({
  name: "games-list-item-home",
  components: {
    market: components_mobile_game_game_market_vue__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"],
    vueCustomScrollbar: vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_5___default.a,
    FlipCountdown: vue2_flip_countdown__WEBPACK_IMPORTED_MODULE_0___default.a
  },
  props: ['game', 'live'],
  mixins: [backend_bets__WEBPACK_IMPORTED_MODULE_4__[/* cartAddMixin */ "b"]],
  data: function data() {
    return {
      timer: false,
      timerTo: '',
      connection: backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"],
      marketsList: [],
      outcomesList: [],
      settings: {
        wheelSpeed: 0.1,
        wheelPropagation: false
      },
      videoId: false
    };
  },
  computed: {
    outcomes: function outcomes() {
      return this.outcomesList.filter(function (item) {
        return item.is_enabled && !item.is_finished && !item.is_banned && !item.is_hidden;
      });
    },
    markets: function markets() {
      var _this = this;

      return this.marketsList.map(function (item) {
        return {
          id: item.id,
          title: item.title,
          group: item.group_id,
          outcomes: _this.outcomes.filter(function (outcome) {
            return outcome.market_id === item.id;
          })
        };
      }).filter(function (item) {
        return item.outcomes.length > 0;
      });
    }
  },
  mounted: function mounted() {
    if (this.live === false) {
      this.timer = true;
      this.timerTo = this.game.tournament_events[0].start_date + ' ' + this.game.tournament_events[0].start_time + ':00';
    }

    ;
  },
  created: function created() {
    var _this2 = this;

    this.videoId = this.game.tournament_events[0].video_link;
    Object(backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* subscribe */ "b"])(this.game.tournament_events[0].id);
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('or', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.rate = data.r;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('os', function (data) {// status
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('oisb', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_banned = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('oise', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_enabled = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('oish', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_hidden = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* connection */ "a"].on('oisf', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_finished = !!data.is;
      }
    });
  },
  beforeDestroy: function beforeDestroy() {
    Object(backend_socket__WEBPACK_IMPORTED_MODULE_2__[/* unsubscribe */ "c"])(this.game.tournament_events[0].id);
  },
  methods: {
    showCart: function showCart(bool) {
      this.$emit('showcart', bool);
    },
    setIcon: function setIcon(image) {
      return image !== null ? {
        'background-image': "url('/uploads/games/".concat(image, "')")
      } : {};
    },
    getOutcome: function getOutcome(game, outcome) {
      if (this.outcomesList.length === 0 || this.game.tournament_events[0].id !== game) {
        return;
      }

      return this.outcomesList.find(function (outcomeItem) {
        return outcomeItem.id === outcome;
      });
    },
    openContent: function openContent() {
      var _this3 = this;

      var button = this.$refs.button;

      if (button.classList.contains('show-button-default')) {
        button.classList.remove('show-button-default');
        button.classList.add('show-button-active');
        this.$refs.content.classList.add('opened');

        if (this.marketsList.length === 0) {
          this.$refs.content.classList.add('loading');
          backend_games__WEBPACK_IMPORTED_MODULE_1__[/* games */ "a"].loadGame(this.game.tournament_events[0].id).then(function (game) {
            _this3.marketsList = game.markets;
            _this3.outcomesList = game.outcomes;

            _this3.$refs.content.classList.remove('loading');
          }).catch(function (err) {
            console.error(err);
          });
        }
      } else {
        button.classList.remove('show-button-active');
        button.classList.add('show-button-default');
        this.$refs.content.classList.remove('opened');
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-itemSearch.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var backend_games__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/backend/games.js");
/* harmony import */ var backend_socket__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/backend/socket.js");
/* harmony import */ var components_game_game_market_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./frontend/assets/games/js/components/game/game-market.vue");
/* harmony import */ var backend_bets__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./frontend/assets/games/js/backend/bets.js");
/* harmony import */ var vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./node_modules/vue-custom-scrollbar/dist/vueScrollbar.umd.min.js");
/* harmony import */ var vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_4__);
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//





/* harmony default export */ __webpack_exports__["a"] = ({
  name: "games-list-item-search",
  components: {
    market: components_game_game_market_vue__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"],
    vueCustomScrollbar: vue_custom_scrollbar__WEBPACK_IMPORTED_MODULE_4___default.a
  },
  props: ['game'],
  mixins: [backend_bets__WEBPACK_IMPORTED_MODULE_3__[/* cartAddMixin */ "b"]],
  data: function data() {
    return {
      connection: backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"],
      marketsList: [],
      outcomesList: [],
      settings: {
        wheelSpeed: 0.1,
        wheelPropagation: false
      },
      videoId: false
    };
  },
  computed: {
    outcomes: function outcomes() {
      return this.outcomesList.filter(function (item) {
        return item.is_enabled && !item.is_finished && !item.is_banned && !item.is_hidden;
      });
    },
    markets: function markets() {
      var _this = this;

      return this.marketsList.map(function (item) {
        return {
          id: item.id,
          title: item.title,
          group: item.group_id,
          outcomes: _this.outcomes.filter(function (outcome) {
            return outcome.market_id === item.id;
          })
        };
      }).filter(function (item) {
        return item.outcomes.length > 0;
      });
    }
  },
  created: function created() {
    var _this2 = this;

    // console.log(this.game.participants);
    this.videoId = this.game.video_link; // console.log(this.videoId);

    Object(backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* subscribe */ "b"])(this.game.id);
    backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"].on('or', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.rate = data.r;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"].on('os', function (data) {// status
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"].on('oisb', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_banned = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"].on('oise', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_enabled = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"].on('oish', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_hidden = !!data.is;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* connection */ "a"].on('oisf', function (data) {
      var outcome = _this2.getOutcome(data.g, data.o);

      if (outcome) {
        outcome.is_finished = !!data.is;
      }
    });
  },
  beforeDestroy: function beforeDestroy() {
    Object(backend_socket__WEBPACK_IMPORTED_MODULE_1__[/* unsubscribe */ "c"])(this.game.id);
  },
  methods: {
    closeSearch: function closeSearch(bool) {
      this.$emit('closeSearch', bool);
    },
    showCart: function showCart(bool) {
      this.$emit('showcart', bool);
    },
    getOutcome: function getOutcome(game, outcome) {
      if (this.outcomesList.length === 0 || this.game.id !== game) {
        return;
      }

      return this.outcomesList.find(function (outcomeItem) {
        return outcomeItem.id === outcome;
      });
    },
    setIcon: function setIcon(image) {
      return image !== null ? {
        'background-image': "url('/uploads/games/".concat(image, "')")
      } : {};
    },
    openContent: function openContent() {
      var _this3 = this;

      var button = this.$refs.button;

      if (button.classList.contains('show-button-default')) {
        button.classList.remove('show-button-default');
        button.classList.add('show-button-active');
        this.$refs.content.classList.add('opened');

        if (this.marketsList.length === 0) {
          this.$refs.content.classList.add('loading');
          backend_games__WEBPACK_IMPORTED_MODULE_0__[/* games */ "a"].loadGame(this.game.id).then(function (game) {
            _this3.marketsList = game.markets;
            _this3.outcomesList = game.outcomes;

            _this3.$refs.content.classList.remove('loading');
          }).catch(function (err) {
            console.error(err);
          });
        }
      } else {
        button.classList.remove('show-button-active');
        button.classList.add('show-button-default');
        this.$refs.content.classList.remove('opened');
      }
    }
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var backend_games__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/backend/games.js");
/* harmony import */ var backend_bets__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/backend/bets.js");
/* harmony import */ var components_mobile_game_list_item_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list-item.vue");
/* harmony import */ var components_mobile_game_carusel_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/carusel.vue");
/* harmony import */ var components_mobile_game_ui_one_click_set_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue");
/* harmony import */ var backend_socket__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./frontend/assets/games/js/backend/socket.js");
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./node_modules/vue-slick/dist/slickCarousel.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["a"] = ({
  mixins: [backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cartAddMixin */ "b"]],
  components: {
    'games-list-item': components_mobile_game_list_item_vue__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"],
    'ui-one-click-set': components_mobile_game_ui_one_click_set_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"],
    carusel: components_mobile_game_carusel_vue__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"],
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_6__[/* default */ "a"]
  },
  data: function data() {
    return {
      live: true,
      loading: true,
      list: [],
      showLive: true,
      showLiveIcon: false,
      sportsList: [],
      slickOptions: {
        infinite: false,
        variableWidth: true,
        swipeToSlide: true
      }
    };
  },
  computed: {
    getList: function getList() {
      var list = backend_games__WEBPACK_IMPORTED_MODULE_0__[/* games */ "a"].sports;
      return list;
    },
    setLanguage: function setLanguage() {
      var html = document.documentElement.getAttribute("lang");

      if (html == 'ru') {
        return 'Все';
      } else {
        return 'All';
      }
    },
    update: function update() {}
  },
  watch: {
    getList: function getList() {
      var _this = this;

      this.loading = false;
      setTimeout(function () {
        return _this.loading = true;
      }, 300);
    }
  },
  methods: {
    showcart: function showcart(bool) {
      this.$emit('cartopen', bool);
    },
    setIcon: function setIcon(image) {
      return image !== null ? {
        'background-image': "url('/uploads/games/".concat(image, "')")
      } : {};
    },
    getGameItem: function getGameItem(id) {
      for (var idx in this.list) {
        var _item = this.list[idx].tournament_events.find(function (item) {
          return item.id === id;
        });

        if (_item) {
          return _item;
        }
      }
    },
    showSlider: function showSlider() {
      if (this.$route.matched.length !== 0 && this.$route.matched[0].hasOwnProperty('path')) return this.$route.matched[0].path == "/live";else return false;
    }
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    backend_games__WEBPACK_IMPORTED_MODULE_0__[/* games */ "a"].loadGames(to.params).then(function (games) {
      next(function (vm) {
        vm.list = games;
        vm.live = to.params.live;
      });
    }).catch(function (err) {
      next(err);
    });

    if (jQuery(window).width() < 1000) {
      jQuery('.bacground-content').slideUp(300, function () {
        jQuery('.button-content').removeClass('button-active');
      });
    }
  },
  beforeRouteUpdate: function beforeRouteUpdate(to, from, next) {
    var _this2 = this;

    backend_games__WEBPACK_IMPORTED_MODULE_0__[/* games */ "a"].loadGames(to.params).then(function (games) {
      _this2.list = games;
      _this2.live = to.params.live;
      next();
    }).catch(function (err) {
      next(err);
    });

    if (jQuery(window).width() < 1000) {
      jQuery('.bacground-content').slideUp(300, function () {
        jQuery('.button-content').removeClass('button-active');
      });
    }
  },
  updated: function updated() {
    // console.log(this.$route);
    if (this.$route.name.includes('live')) {
      this.showLive = true;
      this.showLiveIcon = true;
    } else {
      this.showLive = false;
      this.showLiveIcon = false;
    }
  },
  mounted: function mounted() {
    var _this3 = this;

    if (this.$route.name.includes('live')) {
      this.showLive = true;
      this.showLiveIcon = true;
    } else {
      this.showLive = false;
      this.showLiveIcon = false;
    }

    backend_socket__WEBPACK_IMPORTED_MODULE_5__[/* connection */ "a"].on('gres', function (data) {
      var game = _this3.getGameItem(data.g);

      if (game) {
        game.result = data.r;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_5__[/* connection */ "a"].on('gstats', function (data) {
      console.log('gstats', data, data.g, _this3.info.id, data.g === _this3.info.id);

      var game = _this3.getGameItem(data.g);

      if (game) {
        if (data.f !== undefined) game.is_finished = !!data.f;
        if (data.e !== undefined) game.is_enabled = !!data.e;
        if (data.h !== undefined) game.is_hidden = !!data.h;
        if (data.b !== undefined) game.is_banned = !!data.b;
      }
    });
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/listHome.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var backend_games__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/backend/games.js");
/* harmony import */ var backend_bets__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/backend/bets.js");
/* harmony import */ var components_mobile_game_list_itemHome_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list-itemHome.vue");
/* harmony import */ var components_mobile_game_carusel_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/carusel.vue");
/* harmony import */ var components_mobile_game_ui_one_click_set_vue__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue");
/* harmony import */ var components_mobile_right_casino__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__("./frontend/assets/games/js/components/mobile/right/casino.vue");
/* harmony import */ var backend_socket__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__("./frontend/assets/games/js/backend/socket.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["a"] = ({
  mixins: [backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cartAddMixin */ "b"]],
  components: {
    'games-list-item-home': components_mobile_game_list_itemHome_vue__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"],
    'ui-one-click-set': components_mobile_game_ui_one_click_set_vue__WEBPACK_IMPORTED_MODULE_4__[/* default */ "a"],
    carusel: components_mobile_game_carusel_vue__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"]
  },
  data: function data() {
    return {
      toplive: null,
      topline: null,
      showLive: true,
      showLiveIcon: false,
      sportsList: [],
      showCart: false
    };
  },
  methods: {
    pushLine: function pushLine() {
      this.$router.push('/line');
    },
    pushLive: function pushLive() {
      this.$router.push('/live');
    },
    showcart: function showcart(bool) {
      this.$emit('cartopen', bool);
    },
    setIcon: function setIcon(image) {
      return image !== null ? {
        'background-image': "url('/uploads/games/".concat(image, "')")
      } : {};
    },
    getGameItem: function getGameItem(id) {
      for (var idx in this.list) {
        var _item = this.list[idx].tournament_events.find(function (item) {
          return item.id === id;
        });

        if (_item) {
          return _item;
        }
      }
    },
    showSlider: function showSlider() {
      if (this.$route.matched.length !== 0 && this.$route.matched[0].hasOwnProperty('path')) return this.$route.matched[0].path == "/live";else return false;
    }
  },
  beforeRouteEnter: function beforeRouteEnter(to, from, next) {
    backend_games__WEBPACK_IMPORTED_MODULE_0__[/* games */ "a"].loadGames({
      live: 1
    }).then(function (games) {
      next(function (vm) {
        vm.toplive = games.slice(0, 5);
      });
    }).catch(function (err) {
      next(err);
    });

    if (jQuery(window).width() < 1000) {
      jQuery('.bacground-content').slideUp(300, function () {
        jQuery('.button-content').removeClass('button-active');
      });
    }
  },
  beforeRouteUpdate: function beforeRouteUpdate(to, from, next) {
    var _this = this;

    backend_games__WEBPACK_IMPORTED_MODULE_0__[/* games */ "a"].loadGames({
      live: 1
    }).then(function (games) {
      _this.toplive = games.slice(0, 5);
      next();
    }).catch(function (err) {
      next(err);
    });

    if (jQuery(window).width() < 1000) {
      jQuery('.bacground-content').slideUp(300, function () {
        jQuery('.button-content').removeClass('button-active');
      });
    }
  },
  updated: function updated() {
    // console.log(this.$route);
    if (this.$route.name.includes('live')) {
      this.showLive = true;
      this.showLiveIcon = true;
    } else {
      this.showLive = false;
      this.showLiveIcon = false;
    }
  },
  mounted: function mounted() {
    var _this2 = this;

    backend_games__WEBPACK_IMPORTED_MODULE_0__[/* games */ "a"].loadGames({
      live: 0
    }).then(function (games) {
      _this2.topline = games.slice(0, 5);
    });

    if (this.$route.name.includes('live')) {
      this.showLive = true;
      this.showLiveIcon = true;
    } else {
      this.showLive = false;
      this.showLiveIcon = false;
    }

    backend_socket__WEBPACK_IMPORTED_MODULE_6__[/* connection */ "a"].on('gres', function (data) {
      var game = _this2.getGameItem(data.g);

      if (game) {
        game.result = data.r;
      }
    });
    backend_socket__WEBPACK_IMPORTED_MODULE_6__[/* connection */ "a"].on('gstats', function (data) {
      console.log('gstats', data, data.g, _this2.info.id, data.g === _this2.info.id);

      var game = _this2.getGameItem(data.g);

      if (game) {
        if (data.f !== undefined) game.is_finished = !!data.f;
        if (data.e !== undefined) game.is_enabled = !!data.e;
        if (data.h !== undefined) game.is_hidden = !!data.h;
        if (data.b !== undefined) game.is_banned = !!data.b;
      }
    });
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var backend_games__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/backend/games.js");
/* harmony import */ var components_game_ui_one_click_radio_vue__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/components/game/ui-one-click-radio.vue");
/* harmony import */ var vue_slick__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./node_modules/vue-slick/dist/slickCarousel.esm.js");
/* harmony import */ var components_mobile_game_list_itemSearch_vue__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__("./frontend/assets/games/js/components/mobile/game/list-itemSearch.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//




/* harmony default export */ __webpack_exports__["a"] = ({
  props: {
    title: {
      type: Number,
      default: ""
    },
    sportsList: {
      type: Array,
      default: []
    },
    isLived: {
      type: Boolean,
      default: false
    },
    showLiveIcon: {
      type: Boolean,
      default: true
    }
  },
  components: {
    'games-list-item-search': components_mobile_game_list_itemSearch_vue__WEBPACK_IMPORTED_MODULE_3__[/* default */ "a"],
    'ui-one-click-radio': components_game_ui_one_click_radio_vue__WEBPACK_IMPORTED_MODULE_1__[/* default */ "a"],
    Slick: vue_slick__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"]
  },
  data: function data() {
    return {
      searchModal: false,
      searchAnswer: [],
      home: false,
      live: false,
      line: false,
      loading: false,
      search: false,
      searchForm: '',
      routeId: null,
      slickOptions: {
        slidesToShow: 1,
        infinite: false,
        variableWidth: true,
        // responsive: [
        //   {breakpoint: 1600,settings:{slidesToShow: 4},breakpoint: 768,settings:{slidesToShow: 2}}
        // ],
        touchThreshold: 8
      }
    };
  },
  computed: {
    getList: function getList() {
      var list = backend_games__WEBPACK_IMPORTED_MODULE_0__[/* games */ "a"].sports;
      return list;
    },
    setLanguage: function setLanguage() {
      var html = document.documentElement.getAttribute("lang");

      if (html == 'ru') {
        return 'Все';
      } else {
        return 'All';
      }
    },
    update: function update() {}
  },
  watch: {
    getList: function getList() {
      var _this = this;

      this.loading = false;
      setTimeout(function () {
        return _this.loading = true;
      }, 300);
    },
    '$route': function $route(to, from) {
      var _this2 = this;

      setTimeout(function () {
        if (_this2.$route.fullPath.includes('/live') || _this2.isLived === true) {
          _this2.live = true;
          _this2.line = false;
          _this2.home = false;
        }

        if (_this2.$route.fullPath.includes('/line') || _this2.isLived === false && !_this2.$route.fullPath.includes('/live')) {
          _this2.live = false;
          _this2.home = false;
          _this2.line = true;
        }

        if (_this2.$route.fullPath === '/') {
          _this2.live = false;
          _this2.home = true;
          _this2.line = false;
        }
      }, 300);
    }
  },
  mounted: function mounted() {
    var _this3 = this;

    this.loading = false;
    setTimeout(function () {
      return _this3.loading = true;
    }, 300);
    setTimeout(function () {
      if (_this3.$route.fullPath.includes('/live') || _this3.isLived === true) {
        _this3.live = true;
        _this3.line = false;
        _this3.home = false;
      }

      if (_this3.$route.fullPath.includes('/line') || _this3.isLived === false && !_this3.$route.fullPath.includes('/live')) {
        _this3.live = false;
        _this3.home = false;
        _this3.line = true;
      }

      if (_this3.$route.fullPath === '/') {
        _this3.live = false;
        _this3.home = true;
        _this3.line = false;
      }
    }, 300);
  },
  // created() {
  //     if (this.$parent.isLived === false) {
  //         this.line = true
  //     }
  //     if (this.$parent.isLived) {
  //         this.live = true
  //     }
  // },
  methods: {
    startSearch: function startSearch() {
      var _this4 = this;

      var query = this.searchForm.toLowerCase();
      backend_games__WEBPACK_IMPORTED_MODULE_0__[/* games */ "a"].loadGames({
        live: 1
      }).then(function (games) {
        games.forEach(function (x) {
          if (x.sports_title.toLowerCase() === query || x.tournament_title.toLowerCase().includes(query)) {
            x.tournament_events.forEach(function (x) {
              _this4.searchAnswer.push(x);
            });
          }
        });
        games.forEach(function (x) {
          x.tournament_events.forEach(function (x) {
            if (x.title.toLowerCase().includes(query) || x.start_date === query) {
              _this4.searchAnswer.push(x);
            } else if (Object.values(x.participants).join(' ').toLowerCase().includes(query)) {
              _this4.searchAnswer.push(x);
            }
          });
        });
      });
      backend_games__WEBPACK_IMPORTED_MODULE_0__[/* games */ "a"].loadGames({
        live: 0
      }).then(function (games) {
        games.forEach(function (x) {
          if (x.sports_title.toLowerCase() === query || x.tournament_title.toLowerCase().includes(query)) {
            x.tournament_events.forEach(function (x) {
              _this4.searchAnswer.push(x);
            });
          }
        });
        games.forEach(function (x) {
          x.tournament_events.forEach(function (x) {
            if (x.title.toLowerCase().includes(query) || x.start_date === query) {
              _this4.searchAnswer.push(x);
            } else if (Object.values(x.participants).join(' ').toLowerCase().includes(query)) {
              _this4.searchAnswer.push(x);
            }
          });
        });
      });
      this.searchModal = true;
    },
    showcart: function showcart(bool) {
      this.$emit('showcart', bool);
    },
    setIcon: function setIcon(image) {
      return image !== null ? {
        'background-image': "url('/uploads/games/".concat(image, "')")
      } : {};
    },
    openSearch: function openSearch() {
      var _this5 = this;

      this.search = true;
      setTimeout(function () {
        _this5.$refs.searchInput.focus();
      }, 300);
    },
    closeSearchModal: function closeSearchModal(bool) {
      this.searchModal = bool;
      this.search = bool;
      this.searchAnswer = [];
    },
    closeSearch: function closeSearch() {
      this.searchModal = false;
      this.search = false;
      this.searchAnswer = [];
    },
    pushLine: function pushLine() {
      this.$router.push('/line');
      this.withVideo = false;
    },
    pushLive: function pushLive() {
      this.$router.push('/live');
    }
  }
});

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/right/cart.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue/dist/vue.runtime.esm.js");
/* harmony import */ var backend_bets__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__("./frontend/assets/games/js/backend/bets.js");
/* harmony import */ var components_right_cart_item_vue__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__("./frontend/assets/games/js/components/right/cart-item.vue");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//



var TYPE_SINGLE = 0;
var TYPE_MULTI = 1;
/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].component('container-cart', {
  components: {
    'cart-item': components_right_cart_item_vue__WEBPACK_IMPORTED_MODULE_2__[/* default */ "a"]
  },
  data: function data() {
    return {
      typeLocal: TYPE_MULTI
    };
  },
  mounted: function mounted() {
    jQuery(this.$el).find('#select-timezone').select2({
      themeCss: ".select2-container--default",
      dropdownAutoWidth: true,
      templateSelection: function templateSelection() {
        return $('');
      },
      theme: "default",
      width: "100%",
      language: "ru"
    });
    jQuery(this.$el).find('#select-timezone').on('change', function (event) {
      window.location.href = '/core/timezone?timezone=' + $(this).val();
    });
  },
  methods: {
    closeCart: function closeCart() {
      this.$emit('closeCart', false);
    },
    remove: function remove(id) {
      return backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].remove(id);
    },
    clean: function clean() {
      return backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].clean();
    },
    edit: function edit(amount, change) {
      return backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].edit(amount, change);
    },
    place: function place() {
      backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].place(document.getElementById('cart-clean').checked, document.getElementById('cart-promocode').value, this.type);
    },
    hideError: function hideError() {
      backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].messageError = '';
    },
    setType: function setType(type) {
      this.typeLocal = type;
    },
    slipToggle: function slipToggle() {
      var opener = this.$refs.button;
      var content = this.$refs.content;

      if (opener.className.includes('text-right-side-active')) {
        opener.classList.remove('text-right-side-active');
        content.classList.remove('active');
      } else {
        opener.classList.add('text-right-side-active');
        content.classList.add('active');
      }
    },
    timeToggle: function timeToggle() {
      var opener = this.$refs.time_button;
      var content = this.$refs.list;

      if (opener.className.includes('time_active')) {
        opener.classList.remove('time_active');
        content.classList.remove('active');
      } else {
        opener.classList.add('time_active');
        content.classList.add('active');
      }
    }
  },
  computed: {
    userLoggedIn: function userLoggedIn() {
      return !!window.socketGameParams.loggedIn;
    },
    userCurrency: function userCurrency() {
      return window.socketGameParams.currency;
    },
    timezonesList: function timezonesList() {
      return window.timezones;
    },
    type: {
      get: function get() {
        return this.typeLocal !== TYPE_SINGLE && this.items.length === 1 ? TYPE_SINGLE : this.typeLocal;
      },
      set: function set(value) {
        this.typeLocal = value;
      }
    },
    cart: function cart() {
      return backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"];
    },
    bet: {
      get: function get() {
        return backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].bet || 0;
      },
      set: function set(value) {
        backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].edit(value, this.on_change);
      }
    },
    on_change: {
      get: function get() {
        return +backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].on_change;
      },
      set: function set(value) {
        backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].edit(this.bet, +value);
      }
    },
    items: function items() {
      return backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].items;
    },
    win: function win() {
      return Math.round(this.rate * backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].bet * 1000) / 1000;
    },
    limit: function limit() {
      return backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].maximal;
    },
    rate: function rate() {
      return Math.round(backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].items.reduce(function (value, item) {
        return item.rate * value;
      }, 1) * 1000) / 1000;
    },
    messageError: function messageError() {
      return backend_bets__WEBPACK_IMPORTED_MODULE_1__[/* cart */ "a"].messageError;
    }
  }
}));

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/right/casino.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var vue__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./node_modules/vue/dist/vue.runtime.esm.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = (vue__WEBPACK_IMPORTED_MODULE_0__[/* default */ "a"].component('container-casino', {
  name: "casino"
}));

/***/ }),

/***/ "./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/babel-loader/lib/index.js?!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/right/cart-item.vue?vue&type=script&lang=js&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var backend_bets__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__("./frontend/assets/games/js/backend/bets.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

/* harmony default export */ __webpack_exports__["a"] = ({
  name: "cart-item",
  props: ['item'],
  methods: {
    confirm: function confirm() {
      backend_bets__WEBPACK_IMPORTED_MODULE_0__[/* cart */ "a"].confirm(this.item.id);
    },
    remove: function remove() {
      backend_bets__WEBPACK_IMPORTED_MODULE_0__[/* cart */ "a"].remove(this.item.id);
    },
    setIcon: function setIcon(link) {
      return link === null ? {} : {
        'background-image': "url('/uploads/games/".concat(link, "')")
      };
    }
  },
  computed: {
    classBlocked: function classBlocked() {
      // console.log(this.item);
      return {
        'blocked': !this.item.is_enabled || !!this.item.is_finished || !!this.item.is_banned || !!this.item.is_hidden,
        'price-changed': backend_bets__WEBPACK_IMPORTED_MODULE_0__[/* cart */ "a"].on_change === backend_bets__WEBPACK_IMPORTED_MODULE_0__[/* cartOnChange */ "c"].ON_CHANGE_ACCEPT_INCREASE && this.item.rate > this.item.rate_now || backend_bets__WEBPACK_IMPORTED_MODULE_0__[/* cart */ "a"].on_change === backend_bets__WEBPACK_IMPORTED_MODULE_0__[/* cartOnChange */ "c"].ON_CHANGE_CONFIRM && this.item.rate !== this.item.rate_now
      };
    }
  }
});

/***/ }),

/***/ "./node_modules/process/browser.js":
/***/ (function(module, exports) {

// shim for using process in browser
var process = module.exports = {};

// cached from whatever global is present so that test runners that stub it
// don't break things.  But we need to wrap it in a try catch in case it is
// wrapped in strict mode code which doesn't define any globals.  It's inside a
// function because try/catches deoptimize in certain engines.

var cachedSetTimeout;
var cachedClearTimeout;

function defaultSetTimout() {
    throw new Error('setTimeout has not been defined');
}
function defaultClearTimeout () {
    throw new Error('clearTimeout has not been defined');
}
(function () {
    try {
        if (typeof setTimeout === 'function') {
            cachedSetTimeout = setTimeout;
        } else {
            cachedSetTimeout = defaultSetTimout;
        }
    } catch (e) {
        cachedSetTimeout = defaultSetTimout;
    }
    try {
        if (typeof clearTimeout === 'function') {
            cachedClearTimeout = clearTimeout;
        } else {
            cachedClearTimeout = defaultClearTimeout;
        }
    } catch (e) {
        cachedClearTimeout = defaultClearTimeout;
    }
} ())
function runTimeout(fun) {
    if (cachedSetTimeout === setTimeout) {
        //normal enviroments in sane situations
        return setTimeout(fun, 0);
    }
    // if setTimeout wasn't available but was latter defined
    if ((cachedSetTimeout === defaultSetTimout || !cachedSetTimeout) && setTimeout) {
        cachedSetTimeout = setTimeout;
        return setTimeout(fun, 0);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedSetTimeout(fun, 0);
    } catch(e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't trust the global object when called normally
            return cachedSetTimeout.call(null, fun, 0);
        } catch(e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error
            return cachedSetTimeout.call(this, fun, 0);
        }
    }


}
function runClearTimeout(marker) {
    if (cachedClearTimeout === clearTimeout) {
        //normal enviroments in sane situations
        return clearTimeout(marker);
    }
    // if clearTimeout wasn't available but was latter defined
    if ((cachedClearTimeout === defaultClearTimeout || !cachedClearTimeout) && clearTimeout) {
        cachedClearTimeout = clearTimeout;
        return clearTimeout(marker);
    }
    try {
        // when when somebody has screwed with setTimeout but no I.E. maddness
        return cachedClearTimeout(marker);
    } catch (e){
        try {
            // When we are in I.E. but the script has been evaled so I.E. doesn't  trust the global object when called normally
            return cachedClearTimeout.call(null, marker);
        } catch (e){
            // same as above but when it's a version of I.E. that must have the global object for 'this', hopfully our context correct otherwise it will throw a global error.
            // Some versions of I.E. have different rules for clearTimeout vs setTimeout
            return cachedClearTimeout.call(this, marker);
        }
    }



}
var queue = [];
var draining = false;
var currentQueue;
var queueIndex = -1;

function cleanUpNextTick() {
    if (!draining || !currentQueue) {
        return;
    }
    draining = false;
    if (currentQueue.length) {
        queue = currentQueue.concat(queue);
    } else {
        queueIndex = -1;
    }
    if (queue.length) {
        drainQueue();
    }
}

function drainQueue() {
    if (draining) {
        return;
    }
    var timeout = runTimeout(cleanUpNextTick);
    draining = true;

    var len = queue.length;
    while(len) {
        currentQueue = queue;
        queue = [];
        while (++queueIndex < len) {
            if (currentQueue) {
                currentQueue[queueIndex].run();
            }
        }
        queueIndex = -1;
        len = queue.length;
    }
    currentQueue = null;
    draining = false;
    runClearTimeout(timeout);
}

process.nextTick = function (fun) {
    var args = new Array(arguments.length - 1);
    if (arguments.length > 1) {
        for (var i = 1; i < arguments.length; i++) {
            args[i - 1] = arguments[i];
        }
    }
    queue.push(new Item(fun, args));
    if (queue.length === 1 && !draining) {
        runTimeout(drainQueue);
    }
};

// v8 likes predictible objects
function Item(fun, array) {
    this.fun = fun;
    this.array = array;
}
Item.prototype.run = function () {
    this.fun.apply(null, this.array);
};
process.title = 'browser';
process.browser = true;
process.env = {};
process.argv = [];
process.version = ''; // empty string to avoid regexp issues
process.versions = {};

function noop() {}

process.on = noop;
process.addListener = noop;
process.once = noop;
process.off = noop;
process.removeListener = noop;
process.removeAllListeners = noop;
process.emit = noop;
process.prependListener = noop;
process.prependOnceListener = noop;

process.listeners = function (name) { return [] }

process.binding = function (name) {
    throw new Error('process.binding is not supported');
};

process.cwd = function () { return '/' };
process.chdir = function (dir) {
    throw new Error('process.chdir is not supported');
};
process.umask = function() { return 0; };


/***/ }),

/***/ "./node_modules/setimmediate/setImmediate.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global, process) {(function (global, undefined) {
    "use strict";

    if (global.setImmediate) {
        return;
    }

    var nextHandle = 1; // Spec says greater than zero
    var tasksByHandle = {};
    var currentlyRunningATask = false;
    var doc = global.document;
    var registerImmediate;

    function setImmediate(callback) {
      // Callback can either be a function or a string
      if (typeof callback !== "function") {
        callback = new Function("" + callback);
      }
      // Copy function arguments
      var args = new Array(arguments.length - 1);
      for (var i = 0; i < args.length; i++) {
          args[i] = arguments[i + 1];
      }
      // Store and register the task
      var task = { callback: callback, args: args };
      tasksByHandle[nextHandle] = task;
      registerImmediate(nextHandle);
      return nextHandle++;
    }

    function clearImmediate(handle) {
        delete tasksByHandle[handle];
    }

    function run(task) {
        var callback = task.callback;
        var args = task.args;
        switch (args.length) {
        case 0:
            callback();
            break;
        case 1:
            callback(args[0]);
            break;
        case 2:
            callback(args[0], args[1]);
            break;
        case 3:
            callback(args[0], args[1], args[2]);
            break;
        default:
            callback.apply(undefined, args);
            break;
        }
    }

    function runIfPresent(handle) {
        // From the spec: "Wait until any invocations of this algorithm started before this one have completed."
        // So if we're currently running a task, we'll need to delay this invocation.
        if (currentlyRunningATask) {
            // Delay by doing a setTimeout. setImmediate was tried instead, but in Firefox 7 it generated a
            // "too much recursion" error.
            setTimeout(runIfPresent, 0, handle);
        } else {
            var task = tasksByHandle[handle];
            if (task) {
                currentlyRunningATask = true;
                try {
                    run(task);
                } finally {
                    clearImmediate(handle);
                    currentlyRunningATask = false;
                }
            }
        }
    }

    function installNextTickImplementation() {
        registerImmediate = function(handle) {
            process.nextTick(function () { runIfPresent(handle); });
        };
    }

    function canUsePostMessage() {
        // The test against `importScripts` prevents this implementation from being installed inside a web worker,
        // where `global.postMessage` means something completely different and can't be used for this purpose.
        if (global.postMessage && !global.importScripts) {
            var postMessageIsAsynchronous = true;
            var oldOnMessage = global.onmessage;
            global.onmessage = function() {
                postMessageIsAsynchronous = false;
            };
            global.postMessage("", "*");
            global.onmessage = oldOnMessage;
            return postMessageIsAsynchronous;
        }
    }

    function installPostMessageImplementation() {
        // Installs an event handler on `global` for the `message` event: see
        // * https://developer.mozilla.org/en/DOM/window.postMessage
        // * http://www.whatwg.org/specs/web-apps/current-work/multipage/comms.html#crossDocumentMessages

        var messagePrefix = "setImmediate$" + Math.random() + "$";
        var onGlobalMessage = function(event) {
            if (event.source === global &&
                typeof event.data === "string" &&
                event.data.indexOf(messagePrefix) === 0) {
                runIfPresent(+event.data.slice(messagePrefix.length));
            }
        };

        if (global.addEventListener) {
            global.addEventListener("message", onGlobalMessage, false);
        } else {
            global.attachEvent("onmessage", onGlobalMessage);
        }

        registerImmediate = function(handle) {
            global.postMessage(messagePrefix + handle, "*");
        };
    }

    function installMessageChannelImplementation() {
        var channel = new MessageChannel();
        channel.port1.onmessage = function(event) {
            var handle = event.data;
            runIfPresent(handle);
        };

        registerImmediate = function(handle) {
            channel.port2.postMessage(handle);
        };
    }

    function installReadyStateChangeImplementation() {
        var html = doc.documentElement;
        registerImmediate = function(handle) {
            // Create a <script> element; its readystatechange event will be fired asynchronously once it is inserted
            // into the document. Do so, thus queuing up the task. Remember to clean up once it's been called.
            var script = doc.createElement("script");
            script.onreadystatechange = function () {
                runIfPresent(handle);
                script.onreadystatechange = null;
                html.removeChild(script);
                script = null;
            };
            html.appendChild(script);
        };
    }

    function installSetTimeoutImplementation() {
        registerImmediate = function(handle) {
            setTimeout(runIfPresent, 0, handle);
        };
    }

    // If supported, we should attach to the prototype of global, since that is where setTimeout et al. live.
    var attachTo = Object.getPrototypeOf && Object.getPrototypeOf(global);
    attachTo = attachTo && attachTo.setTimeout ? attachTo : global;

    // Don't get fooled by e.g. browserify environments.
    if ({}.toString.call(global.process) === "[object process]") {
        // For Node.js before 0.9
        installNextTickImplementation();

    } else if (canUsePostMessage()) {
        // For non-IE10 modern browsers
        installPostMessageImplementation();

    } else if (global.MessageChannel) {
        // For web workers, where supported
        installMessageChannelImplementation();

    } else if (doc && "onreadystatechange" in doc.createElement("script")) {
        // For IE 6–8
        installReadyStateChangeImplementation();

    } else {
        // For older browsers
        installSetTimeoutImplementation();
    }

    attachTo.setImmediate = setImmediate;
    attachTo.clearImmediate = clearImmediate;
}(typeof self === "undefined" ? typeof global === "undefined" ? this : global : self));

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("./node_modules/webpack/buildin/global.js"), __webpack_require__("./node_modules/process/browser.js")))

/***/ }),

/***/ "./node_modules/timers-browserify/main.js":
/***/ (function(module, exports, __webpack_require__) {

/* WEBPACK VAR INJECTION */(function(global) {var scope = (typeof global !== "undefined" && global) ||
            (typeof self !== "undefined" && self) ||
            window;
var apply = Function.prototype.apply;

// DOM APIs, for completeness

exports.setTimeout = function() {
  return new Timeout(apply.call(setTimeout, scope, arguments), clearTimeout);
};
exports.setInterval = function() {
  return new Timeout(apply.call(setInterval, scope, arguments), clearInterval);
};
exports.clearTimeout =
exports.clearInterval = function(timeout) {
  if (timeout) {
    timeout.close();
  }
};

function Timeout(id, clearFn) {
  this._id = id;
  this._clearFn = clearFn;
}
Timeout.prototype.unref = Timeout.prototype.ref = function() {};
Timeout.prototype.close = function() {
  this._clearFn.call(scope, this._id);
};

// Does not start the time, just sets up the members needed.
exports.enroll = function(item, msecs) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = msecs;
};

exports.unenroll = function(item) {
  clearTimeout(item._idleTimeoutId);
  item._idleTimeout = -1;
};

exports._unrefActive = exports.active = function(item) {
  clearTimeout(item._idleTimeoutId);

  var msecs = item._idleTimeout;
  if (msecs >= 0) {
    item._idleTimeoutId = setTimeout(function onTimeout() {
      if (item._onTimeout)
        item._onTimeout();
    }, msecs);
  }
};

// setimmediate attaches itself to the global object
__webpack_require__("./node_modules/setimmediate/setImmediate.js");
// On some exotic environments, it's not clear which object `setimmediate` was
// able to install onto.  Search each possibility in the same order as the
// `setimmediate` library.
exports.setImmediate = (typeof self !== "undefined" && self.setImmediate) ||
                       (typeof global !== "undefined" && global.setImmediate) ||
                       (this && this.setImmediate);
exports.clearImmediate = (typeof self !== "undefined" && self.clearImmediate) ||
                         (typeof global !== "undefined" && global.clearImmediate) ||
                         (this && this.clearImmediate);

/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__("./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/container-router.vue?vue&type=template&id=092dddd3&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("router-view")
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/game/game-market.vue?vue&type=template&id=6aa6a5d0&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "game-show-content-elements",
      class: { closed: !_vm.listOpened }
    },
    [
      _c(
        "div",
        {
          staticClass: "game-user-day-header",
          on: {
            click: function($event) {
              return _vm.toggleList()
            }
          }
        },
        [_c("span", [_vm._v(_vm._s(_vm.market.title))]), _vm._v(" "), _vm._m(0)]
      ),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "game-show-for-one-game-coeff",
          class: { row_3: _vm.outcomes.length === 3 }
        },
        _vm._l(_vm.outcomes, function(outcome) {
          return _c(
            "a",
            {
              key: outcome.id,
              staticClass: "coeffComponent",
              on: {
                click: function($event) {
                  return _vm.addToCart(outcome.id)
                }
              }
            },
            [
              _c("div", { staticClass: "coeffComponent-first-element" }, [
                _c("span", [_vm._v(_vm._s(outcome.title))])
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "coeffComponent-second-element" }, [
                _c("span", { class: outcome.change }, [
                  _vm._v(_vm._s(_vm.setShort(outcome.rate)))
                ])
              ])
            ]
          )
        }),
        0
      )
    ]
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "arrow" }, [
      _c("i", { staticClass: "fas fa-chevron-up" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/game/ui-one-click-radio.vue?vue&type=template&id=24745145&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "ui-one-click-radio" }, [
    _c("div", { staticClass: "radio-text" }, [_vm._v("One-click")]),
    _vm._v(" "),
    _c("div", { staticClass: "radio-content" }, [
      _c("input", {
        ref: "input",
        staticClass: "radio-content-text",
        attrs: { type: "text", disabled: !_vm.radio },
        domProps: { value: _vm.title }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "radio-content-value" }, [
        _c("div", {
          staticClass: "radio-false",
          on: {
            click: function($event) {
              return _vm.radioSet(false)
            }
          }
        }),
        _vm._v(" "),
        _c("div", {
          staticClass: "radio-true",
          on: {
            click: function($event) {
              return _vm.radioSet(true)
            }
          }
        }),
        _vm._v(" "),
        _c("div", {
          ref: "radio",
          staticClass: "radio-icon",
          class: _vm.radio ? "radio-icon-active" : "radio-icon-default"
        })
      ])
    ])
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/display-mobile.vue?vue&type=template&id=6e53becc&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "home mobile_layout", attrs: { id: "games-console" } },
    [
      _c(
        "div",
        { staticClass: "content-bar" },
        [
          _c("router-view", {
            on: {
              cartopen: function($event) {
                return _vm.cartopen($event)
              }
            }
          })
        ],
        1
      ),
      _vm._v(" "),
      _c("transition", { attrs: { name: "modal" } }, [
        _vm.cartShow
          ? _c(
              "div",
              { staticClass: "side-bar-right" },
              [
                _c("div", {
                  staticClass: "cart-popup-wpar",
                  on: {
                    click: function($event) {
                      return _vm.cartHide(false)
                    }
                  }
                }),
                _vm._v(" "),
                _c("container-cart", {
                  on: {
                    closeCart: function($event) {
                      return _vm.cartHide($event)
                    }
                  }
                })
              ],
              1
            )
          : _vm._e()
      ]),
      _vm._v(" "),
      _c("transition", { attrs: { name: "cartBtn" } }, [
        !_vm.cartShow
          ? _c(
              "div",
              { staticClass: "show-cart-btn", on: { click: _vm.openCart } },
              [
                _c("i", { staticClass: "fas fa-list-alt" }),
                _c("span", [_vm._v(_vm._s(_vm.count))])
              ]
            )
          : _vm._e()
      ])
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/carusel.vue?vue&type=template&id=0cd73d0a&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "carusel", class: !_vm.logined ? "" : "loginIn" },
    [
      _c("slick", { ref: "carusel", attrs: { options: _vm.slickOptions } }, [
        _c("div", { staticClass: "slide_item" }, [
          _c("img", {
            attrs: { src: "/static/img/carusel/slide1.jpg", alt: "" }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "slide_item" }, [
          _c("img", {
            attrs: { src: "/static/img/carusel/slide2.jpg", alt: "" }
          })
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "slide_item" }, [
          _c("img", {
            attrs: { src: "/static/img/carusel/slide3.jpg", alt: "" }
          })
        ])
      ]),
      _vm._v(" "),
      !_vm.logined
        ? _c("div", { staticClass: "button_wrap" }, [
            _c(
              "a",
              {
                staticClass: "carusel-button",
                attrs: {
                  href: "/user/auth/login",
                  "data-toggle": "modal",
                  "data-target": "#loginPopup"
                }
              },
              [
                _c("span", [
                  _vm._v(
                    "\n        " + _vm._s(_vm.translateText.btnTxt) + "\n      "
                  )
                ])
              ]
            )
          ])
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/game-market.vue?vue&type=template&id=755f2a59&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "game-show-content-elements" }, [
    _c("div", { staticClass: "game-user-day-header" }, [
      _c("span", [_vm._v(_vm._s(_vm.market.title))])
    ]),
    _vm._v(" "),
    _c(
      "div",
      { staticClass: "game-show-for-one-game-coeff" },
      _vm._l(_vm.outcomes, function(outcome) {
        return _c(
          "a",
          {
            key: outcome.id,
            staticClass: "coeffComponent",
            on: {
              click: function($event) {
                _vm.addToCart(outcome.id)
                _vm.showCart(true)
              }
            }
          },
          [
            _c("div", { staticClass: "coeffComponent-first-element" }, [
              _c("span", [_vm._v(_vm._s(outcome.title))])
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "coeffComponent-second-element" }, [
              _c("span", { class: outcome.change }, [
                _vm._v(_vm._s(_vm.setShort(outcome.rate)))
              ])
            ])
          ]
        )
      }),
      0
    )
  ])
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/game.vue?vue&type=template&id=b08f5000&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "game-view" },
    [
      _c("ui-one-click-set", {
        class: { active_line: _vm.showLive },
        attrs: {
          sportsList: _vm.list,
          showLiveIcon: _vm.showLiveIcon,
          isLived: _vm.isLived,
          title: 300
        },
        on: {
          showcart: function($event) {
            return _vm.showcart($event)
          }
        }
      }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "carousel_wrap" },
        [
          _vm.loading
            ? _c(
                "slick",
                { attrs: { options: _vm.slickOptions } },
                [
                  _c(
                    "div",
                    {
                      staticClass: "link_wrap",
                      class: {
                        active_wrap:
                          _vm.$route.path == (_vm.showLive ? "/live" : "/line")
                      }
                    },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "list_link all_link",
                          attrs: { to: _vm.showLive ? "/live" : "/line" }
                        },
                        [
                          _c("div", {
                            staticClass: "list_icon game-icon",
                            staticStyle: {
                              "background-image":
                                "url(/static/img/carusel/all_icon_white.png)"
                            }
                          }),
                          _vm._v(
                            "\n                    " +
                              _vm._s(_vm.setLanguage) +
                              "\n                "
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm._l(_vm.getList, function(item) {
                    return _c(
                      "div",
                      { key: item.id, staticClass: "link_wrap" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "list_link",
                            class: { active: item.id === _vm.sport.id },
                            attrs: {
                              to: {
                                name: _vm.isLived ? "live/sport" : "line/sport",
                                params: { sports_id: item.id }
                              }
                            }
                          },
                          [
                            _c("div", {
                              staticClass: "list_icon game-icon",
                              style: _vm.setIcon(item.image)
                            }),
                            _vm._v(
                              "\n                    " +
                                _vm._s(item.title) +
                                "\n                "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  })
                ],
                2
              )
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _vm.isLived || this.info.is_finished
        ? _c("div", { staticClass: "game-view-duration" }, [
            this.info.is_finished
              ? _c("p", [_vm._v(_vm._s(_vm.translateText.finished))])
              : _vm._e(),
            _vm._v(" "),
            _vm.info.gamePeriod && !this.info.is_finished
              ? _c("p", [_vm._v(_vm._s(_vm.info.gamePeriod))])
              : _vm._e(),
            _vm._v(" "),
            !_vm.info.gamePeriod && _vm.isLived && !this.info.is_finished
              ? _c("p", [_vm._v(_vm._s(_vm.period))])
              : _vm._e(),
            _vm._v(" "),
            _vm.isRunning || this.info.is_finished
              ? _c("span", [
                  _c("i", { staticClass: "far fa-clock" }),
                  _vm._v(" " + _vm._s(_vm.durationFormatted))
                ])
              : _vm._e(),
            _vm._v(" "),
            !_vm.isRunning
              ? _c("span", [_vm._v(_vm._s(_vm.translateText.pause))])
              : _vm._e()
          ])
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        {
          staticClass: "game_content_top",
          class: { with_video: _vm.showVideo }
        },
        [
          _c(
            "div",
            {
              ref: "videoContainer",
              staticClass: "video_container",
              staticStyle: { overflow: "hidden" }
            },
            [
              _c(
                "object",
                {
                  ref: "videoPlayer",
                  attrs: {
                    id: "BridgeMovie",
                    type: "application/x-shockwave-flash",
                    data: "/static/player.swf"
                  }
                },
                [
                  _c("param", { attrs: { name: "menu", value: "false" } }),
                  _vm._v(" "),
                  _c("param", { attrs: { name: "wmode", value: "opaque" } }),
                  _vm._v(" "),
                  _c("param", {
                    attrs: { name: "allowFullScreen", value: "true" }
                  }),
                  _vm._v(" "),
                  _c("param", {
                    attrs: { name: "AllowScriptAccess", value: "always" }
                  }),
                  _vm._v(" "),
                  _c("param", {
                    attrs: { name: "flashvars", value: _vm.playerLink }
                  })
                ]
              ),
              _vm._v(" "),
              !_vm.userLoggedIn
                ? _c("div", { staticClass: "without_reg" }, [
                    _c("div", { staticClass: "btns_wrap" }, [
                      _c(
                        "a",
                        {
                          staticClass: "ui-radius-button log-in",
                          attrs: {
                            href: "/user/auth/login",
                            "data-toggle": "modal",
                            "data-target": "#loginPopup"
                          }
                        },
                        [_vm._v(_vm._s(_vm.translateText.login))]
                      ),
                      _vm._v(" "),
                      _c(
                        "a",
                        {
                          staticClass: "ui-radius-button",
                          attrs: { href: "/user/auth/register" }
                        },
                        [_vm._v(_vm._s(_vm.translateText.reg))]
                      )
                    ])
                  ])
                : _vm._e()
            ]
          ),
          _vm._v(" "),
          _vm.visible || this.info.is_finished
            ? _c(
                "div",
                {
                  staticClass: "game-view-stats",
                  style: _vm.setBg(_vm.info.sports_id)
                },
                [
                  _c("div", { staticClass: "game_stats_left" }, [
                    _c("div", { staticClass: "left_team" }, [
                      _c("span", { staticClass: "name_left" }, [
                        _vm._v(_vm._s(_vm.participants[0].name))
                      ]),
                      _vm._v(" "),
                      _vm.isLived && _vm.sport.title === "Футбол"
                        ? _c("div", { staticClass: "result_table_wrap" }, [
                            _c("table", { staticClass: "result_table" }, [
                              _c("thead", [
                                _c(
                                  "tr",
                                  _vm._l(_vm.resultExtendedFootball, function(
                                    stat,
                                    title
                                  ) {
                                    return _c("th", {
                                      key: title,
                                      domProps: { innerHTML: _vm._s(title) }
                                    })
                                  }),
                                  0
                                )
                              ]),
                              _vm._v(" "),
                              _c("tbody", [
                                _c(
                                  "tr",
                                  _vm._l(_vm.resultExtendedFootball, function(
                                    stat,
                                    title
                                  ) {
                                    return _c("td", { key: title }, [
                                      _c("span", {
                                        staticClass: "mobile_name",
                                        domProps: { innerHTML: _vm._s(title) }
                                      }),
                                      _vm._v(
                                        "\n                                            " +
                                          _vm._s(stat[0]) +
                                          "\n                                        "
                                      )
                                    ])
                                  }),
                                  0
                                )
                              ])
                            ])
                          ])
                        : _vm._e()
                    ])
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "game_stats-center" }, [
                    !_vm.isLived
                      ? _c("p", { staticClass: "start-time" }, [
                          _vm._v(_vm._s(_vm.startTime))
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    !_vm.isLived
                      ? _c("p", { staticClass: "start-date" }, [
                          _vm._v(_vm._s(_vm.startDate))
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.isLived
                      ? _c("div", { staticClass: "game-stats-result" }, [
                          _vm._v(_vm._s(_vm.results))
                        ])
                      : _vm._e(),
                    _vm._v(" "),
                    _vm.isLived
                      ? _c("div", { staticClass: "result_table_wrap" }, [
                          _c("table", { staticClass: "result_table" }, [
                            _c("thead", [
                              _c(
                                "tr",
                                _vm._l(_vm.resultExtended, function(
                                  stat,
                                  title
                                ) {
                                  return _c("th", { key: title }, [
                                    _vm._v(_vm._s(title[0])),
                                    _c("i", { staticClass: "far fa-clock" })
                                  ])
                                }),
                                0
                              )
                            ]),
                            _vm._v(" "),
                            _c("tbody", [
                              _c(
                                "tr",
                                _vm._l(_vm.resultExtended, function(
                                  stat,
                                  title
                                ) {
                                  return _c("td", { key: title }, [
                                    _c("span", {
                                      staticClass: "mobile_name",
                                      domProps: { innerHTML: _vm._s(title) }
                                    }),
                                    _vm._v(
                                      "\n                                        " +
                                        _vm._s(stat[0]) +
                                        ":" +
                                        _vm._s(stat[1]) +
                                        "\n                                    "
                                    )
                                  ])
                                }),
                                0
                              )
                            ])
                          ])
                        ])
                      : _vm._e()
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "game_stats_right" }, [
                    _c("div", { staticClass: "right_team" }, [
                      _c("span", { staticClass: "name_right" }, [
                        _vm._v(_vm._s(_vm.participants[1].name))
                      ]),
                      _vm._v(" "),
                      _vm.isLived && _vm.sport.title === "Футбол"
                        ? _c("div", { staticClass: "result_table_wrap" }, [
                            _c("table", { staticClass: "result_table" }, [
                              _c("thead", [
                                _c(
                                  "tr",
                                  _vm._l(_vm.resultExtendedFootball, function(
                                    stat,
                                    title
                                  ) {
                                    return _c("th", {
                                      key: title,
                                      domProps: { innerHTML: _vm._s(title) }
                                    })
                                  }),
                                  0
                                )
                              ]),
                              _vm._v(" "),
                              _c("tbody", [
                                _c(
                                  "tr",
                                  _vm._l(_vm.resultExtendedFootball, function(
                                    stat,
                                    title
                                  ) {
                                    return _c("td", { key: title }, [
                                      _c("span", {
                                        staticClass: "mobile_name",
                                        domProps: { innerHTML: _vm._s(title) }
                                      }),
                                      _vm._v(
                                        "\n                                            " +
                                          _vm._s(stat[1]) +
                                          "\n                                        "
                                      )
                                    ])
                                  }),
                                  0
                                )
                              ])
                            ])
                          ])
                        : _vm._e()
                    ])
                  ])
                ]
              )
            : _vm._e()
        ]
      ),
      _vm._v(" "),
      _vm.visible
        ? _c("div", { staticClass: "game-view-groups" }, [
            _c(
              "div",
              { staticClass: "view-groups-left" },
              [
                _c(
                  "vue-custom-scrollbar",
                  {
                    staticClass: "scroll_tabs",
                    attrs: { settings: _vm.settings }
                  },
                  [
                    _c(
                      "ul",
                      { staticClass: "game-tabs-wrap" },
                      _vm._l(_vm.groups, function(item, index) {
                        return _c(
                          "li",
                          {
                            key: index,
                            staticClass: "game-tab_item",
                            class: {
                              "active-game-tab": _vm.currentGroup === item.id
                            },
                            on: {
                              click: function($event) {
                                return _vm.setTab(item.id)
                              }
                            }
                          },
                          [
                            _vm._v(
                              "\n                        " +
                                _vm._s(item.title) +
                                "\n                    "
                            )
                          ]
                        )
                      }),
                      0
                    )
                  ]
                )
              ],
              1
            ),
            _vm._v(" "),
            _c("div", { staticClass: "view-groups-right" }, [
              _c("div", { staticClass: "game-search-wrap" }, [
                _c("input", {
                  directives: [
                    {
                      name: "model",
                      rawName: "v-model",
                      value: _vm.search,
                      expression: "search"
                    }
                  ],
                  attrs: { type: "text" },
                  domProps: { value: _vm.search },
                  on: {
                    input: function($event) {
                      if ($event.target.composing) {
                        return
                      }
                      _vm.search = $event.target.value
                    }
                  }
                }),
                _vm._v(" "),
                _c("span", { staticClass: "game-search-reset" }, [
                  _c("i", {
                    staticClass: "fas fa-times",
                    on: { click: _vm.clearSearch }
                  })
                ])
              ]),
              _vm._v(" "),
              _vm.showVideo && _vm.videoExists
                ? _c("button", {
                    staticClass: "clone_video",
                    on: { click: _vm.videoToSidebar }
                  })
                : _vm._e(),
              _vm._v(" "),
              _c("div", { staticClass: "view_btn mob_hide" }),
              _vm._v(" "),
              (_vm.markets.length ? _vm.markets.length >= 2 : true) &&
              (_vm.markets.leftSide
                ? _vm.markets.leftSide.length + _vm.markets.rightSide.length >=
                  2
                : true)
                ? _c("button", {
                    staticClass: "temp_change temp_change1",
                    class: { active: _vm.columns === "col1" },
                    on: {
                      click: function($event) {
                        return _vm.setColumns("col1")
                      }
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              (_vm.markets.length ? _vm.markets.length >= 2 : true) &&
              (_vm.markets.leftSide
                ? _vm.markets.leftSide.length + _vm.markets.rightSide.length >=
                  2
                : true)
                ? _c("button", {
                    staticClass: "temp_change temp_change2",
                    class: { active: _vm.columns === "col2" },
                    on: {
                      click: function($event) {
                        return _vm.setColumns("col2")
                      }
                    }
                  })
                : _vm._e(),
              _vm._v(" "),
              (_vm.markets.length ? _vm.markets.length >= 5 : true) &&
              (_vm.markets.leftSide
                ? _vm.markets.leftSide.length + _vm.markets.rightSide.length >=
                  5
                : true) &&
              (_vm.markets.center
                ? _vm.markets.left.length +
                    _vm.markets.center.length +
                    _vm.markets.right.length >=
                  5
                : true)
                ? _c("button", {
                    staticClass: "temp_change temp_change3",
                    class: { active: _vm.columns === "col3" },
                    on: {
                      click: function($event) {
                        return _vm.setColumns("col3")
                      }
                    }
                  })
                : _vm._e()
            ])
          ])
        : _vm._e(),
      _vm._v(" "),
      _vm.visible && _vm.columns === "col1"
        ? _c(
            "div",
            { staticClass: "ui-game-show-content", class: _vm.columns },
            _vm._l(_vm.markets, function(market) {
              return _c("market", {
                key: market.id,
                attrs: { market: market },
                on: {
                  showcart: function($event) {
                    return _vm.showcart($event)
                  }
                }
              })
            }),
            1
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.visible && _vm.columns === "col2"
        ? _c(
            "div",
            { staticClass: "ui-game-show-content", class: _vm.columns },
            [
              _c(
                "div",
                { staticClass: "column" },
                _vm._l(_vm.markets.leftSide, function(market) {
                  return _c("market", {
                    key: market.id,
                    attrs: { market: market },
                    on: {
                      showcart: function($event) {
                        return _vm.showcart($event)
                      }
                    }
                  })
                }),
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "column" },
                _vm._l(_vm.markets.rightSide, function(market) {
                  return _c("market", {
                    key: market.id,
                    attrs: { market: market },
                    on: {
                      showcart: function($event) {
                        return _vm.showcart($event)
                      }
                    }
                  })
                }),
                1
              )
            ]
          )
        : _vm._e(),
      _vm._v(" "),
      _vm.visible && _vm.columns === "col3"
        ? _c(
            "div",
            { staticClass: "ui-game-show-content", class: _vm.columns },
            [
              _c(
                "div",
                { staticClass: "column" },
                _vm._l(_vm.markets.left, function(market) {
                  return _c("market", {
                    key: market.id,
                    attrs: { market: market },
                    on: {
                      showcart: function($event) {
                        return _vm.showcart($event)
                      }
                    }
                  })
                }),
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "column" },
                _vm._l(_vm.markets.center, function(market) {
                  return _c("market", {
                    key: market.id,
                    attrs: { market: market },
                    on: {
                      showcart: function($event) {
                        return _vm.showcart($event)
                      }
                    }
                  })
                }),
                1
              ),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "column" },
                _vm._l(_vm.markets.right, function(market) {
                  return _c("market", {
                    key: market.id,
                    attrs: { market: market },
                    on: {
                      showcart: function($event) {
                        return _vm.showcart($event)
                      }
                    }
                  })
                }),
                1
              )
            ]
          )
        : _vm._e()
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-item.vue?vue&type=template&id=400187c4&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "ui-game-show-coefficient" },
    [
      _c(
        "div",
        { staticClass: "game-show-coefficient" },
        [
          _c(
            "router-link",
            {
              staticClass: "show-games-teams",
              attrs: { to: { name: "game", params: { id: _vm.game.id } } }
            },
            [
              _c(
                "div",
                { staticClass: "team-name-wrap" },
                _vm._l(_vm.game.participants, function(pname, pkey) {
                  return _c(
                    "div",
                    {
                      key: pkey,
                      staticClass: "show-team-name show-team-name--home"
                    },
                    [_vm._v(" " + _vm._s(pname || "") + " ")]
                  )
                }),
                0
              ),
              _vm._v(" "),
              _c("div", { staticClass: "show-time" }, [
                _c("span", [
                  _vm._v(
                    _vm._s(_vm.game.start_time) +
                      " " +
                      _vm._s(_vm.game.start_date)
                  )
                ])
              ])
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "show-coeff-matches" }, [
            _c("div", { staticClass: "show-team-game-coeff" }, [
              _c("span", { staticClass: "show-game-second-coef" }, [
                _vm._v(_vm._s(_vm.game.bets_count || 0))
              ]),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticClass: "show-game-first-coeff",
                  on: { click: _vm.openContent }
                },
                [_vm._v(_vm._s(_vm.game.outcomes_count || 0))]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "show-team-game-coeff" }, [
              _c("span", [_vm._v(_vm._s(_vm.game.result))])
            ])
          ]),
          _vm._v(" "),
          _vm.timer
            ? _c("flip-countdown", { attrs: { deadline: _vm.timerTo } })
            : _vm._e(),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "show-all-flex-coefficient" },
            _vm._l(_vm.game.outcomes, function(outcome) {
              return _c(
                "div",
                {
                  key: outcome.id,
                  staticClass: "ui-coefficients-for-bet",
                  on: {
                    click: function($event) {
                      outcome.r > 0 && _vm.addToCart(outcome.id)
                      outcome.r > 0 && _vm.showCart(true)
                    }
                  }
                },
                [
                  _c("div", { staticClass: "top-coefficient-for-bet" }, [
                    _vm._v(
                      "\n                    " +
                        _vm._s(outcome.l) +
                        "\n                "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "bottom-coefficient-for-bet" }, [
                    _vm._v(_vm._s(outcome.r > 0 ? outcome.r : "-"))
                  ])
                ]
              )
            }),
            0
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "vue-custom-scrollbar",
        { staticClass: "scroll_markets", attrs: { settings: _vm.settings } },
        [
          _c(
            "div",
            { ref: "content", staticClass: "ui-game-show-content" },
            [
              _c("div", { staticClass: "ui-game-show-loader" }, [
                _c("span", { staticClass: "fas fa-spinner fa-spin" })
              ]),
              _vm._v(" "),
              _vm._l(_vm.markets, function(market) {
                return _c("market", {
                  key: market.id,
                  attrs: { market: market },
                  on: {
                    showcart: function($event) {
                      return _vm.showCart($event)
                    }
                  }
                })
              })
            ],
            2
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-itemHome.vue?vue&type=template&id=449a397a&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "ui-game-show-coefficient" },
    [
      _c(
        "div",
        { staticClass: "game-show-coefficient" },
        [
          _c("div", { staticClass: "top-span" }, [_vm._v("TOP")]),
          _vm._v(" "),
          _c("div", { staticClass: "game-event-title" }, [
            _c("div", {
              staticClass: "game-event-icon",
              style: _vm.setIcon(_vm.game.sports_image)
            }),
            _vm._v(" "),
            _c("span", [_vm._v(_vm._s(_vm.game.sports_title) + ". ")]),
            _vm._v(" "),
            _c("span", [_vm._v(_vm._s(_vm.game.tournament_title))])
          ]),
          _vm._v(" "),
          _c(
            "div",
            {
              ref: "button",
              staticClass: "show-button show-button-default hide",
              on: { click: _vm.openContent }
            },
            [_c("i", { staticClass: "fas fa-caret-right" })]
          ),
          _vm._v(" "),
          _c(
            "router-link",
            {
              staticClass: "show-games-teams show-games-teams--home",
              attrs: {
                to: {
                  name: "game",
                  params: { id: _vm.game.tournament_events[0].id }
                }
              }
            },
            [
              _c(
                "div",
                { staticClass: "team-name-wrap" },
                _vm._l(_vm.game.tournament_events[0].participants, function(
                  pname,
                  pkey
                ) {
                  return _c(
                    "div",
                    {
                      key: pkey,
                      staticClass: "show-team-name show-team-name--home"
                    },
                    [_vm._v(" " + _vm._s(pname || "") + " ")]
                  )
                }),
                0
              ),
              _vm._v(" "),
              _c("div", { staticClass: "show-time" }, [
                _c("span", [
                  _vm._v(
                    _vm._s(_vm.game.tournament_events[0].start_time) +
                      " " +
                      _vm._s(_vm.game.tournament_events[0].start_date)
                  )
                ])
              ])
            ]
          ),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "show-coeff-matches show-coeff-matches--home" },
            [
              _c("div", { staticClass: "show-team-game-coeff" }, [
                _c("span", { staticClass: "show-game-second-coef" }, [
                  _vm._v(_vm._s(_vm.game.tournament_events[0].bets_count || 0))
                ]),
                _vm._v(" "),
                _c(
                  "span",
                  {
                    staticClass: "show-game-first-coeff",
                    on: { click: _vm.openContent }
                  },
                  [
                    _vm._v(
                      _vm._s(_vm.game.tournament_events[0].outcomes_count || 0)
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _c("div", { staticClass: "show-team-game-coeff" }, [
                _c("span", [
                  _vm._v(_vm._s(_vm.game.tournament_events[0].result))
                ])
              ])
            ]
          ),
          _vm._v(" "),
          _vm.timer
            ? _c("flip-countdown", { attrs: { deadline: _vm.timerTo } })
            : _vm._e(),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "show-all-flex-coefficient" },
            _vm._l(_vm.game.tournament_events[0].outcomes, function(outcome) {
              return _c(
                "div",
                {
                  key: outcome.id,
                  staticClass: "ui-coefficients-for-bet",
                  on: {
                    click: function($event) {
                      outcome.r > 0 && _vm.addToCart(outcome.id)
                      outcome.r > 0 && _vm.showCart(true)
                    }
                  }
                },
                [
                  _c("div", { staticClass: "top-coefficient-for-bet" }, [
                    _vm._v(
                      "\n                    " +
                        _vm._s(outcome.l) +
                        "\n                "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "bottom-coefficient-for-bet" }, [
                    _vm._v(_vm._s(outcome.r > 0 ? outcome.r : "-"))
                  ])
                ]
              )
            }),
            0
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "vue-custom-scrollbar",
        { staticClass: "scroll_markets", attrs: { settings: _vm.settings } },
        [
          _c(
            "div",
            { ref: "content", staticClass: "ui-game-show-content" },
            [
              _c("div", { staticClass: "ui-game-show-loader" }, [
                _c("span", { staticClass: "fas fa-spinner fa-spin" })
              ]),
              _vm._v(" "),
              _vm._l(_vm.markets, function(market) {
                return _c("market", {
                  key: market.id,
                  attrs: { market: market }
                })
              })
            ],
            2
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list-itemSearch.vue?vue&type=template&id=1bb1248c&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "ui-game-show-coefficient" },
    [
      _c(
        "div",
        { staticClass: "game-show-coefficient" },
        [
          _c(
            "router-link",
            {
              staticClass: "show-games-teams",
              attrs: { to: { name: "game", params: { id: _vm.game.id } } }
            },
            [
              _c(
                "div",
                {
                  staticClass: "team-name-wrap",
                  on: {
                    click: function($event) {
                      return _vm.closeSearch(false)
                    }
                  }
                },
                _vm._l(_vm.game.participants, function(pname, pkey) {
                  return _c(
                    "div",
                    { key: pkey, staticClass: "show-team-name" },
                    [_vm._v(_vm._s(pname || ""))]
                  )
                }),
                0
              ),
              _vm._v(" "),
              _c(
                "div",
                {
                  staticClass: "show-time",
                  on: {
                    click: function($event) {
                      return _vm.closeSearch(false)
                    }
                  }
                },
                [
                  _c("span", [
                    _vm._v(
                      _vm._s(_vm.game.start_time) +
                        " " +
                        _vm._s(_vm.game.start_date)
                    )
                  ])
                ]
              )
            ]
          ),
          _vm._v(" "),
          _c("div", { staticClass: "show-coeff-matches" }, [
            _c("div", { staticClass: "show-team-game-coeff" }, [
              _c("span", { staticClass: "show-game-second-coef" }, [
                _vm._v(_vm._s(_vm.game.bets_count || 0))
              ]),
              _vm._v(" "),
              _c(
                "span",
                {
                  staticClass: "show-game-first-coeff",
                  on: { click: _vm.openContent }
                },
                [_vm._v(_vm._s(_vm.game.outcomes_count || 0))]
              )
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "show-team-game-coeff" }, [
              _c("span", [_vm._v(_vm._s(_vm.game.result))])
            ])
          ]),
          _vm._v(" "),
          _c(
            "div",
            { staticClass: "show-all-flex-coefficient" },
            _vm._l(_vm.game.outcomes, function(outcome) {
              return _c(
                "div",
                {
                  key: outcome.id,
                  staticClass: "ui-coefficients-for-bet",
                  on: {
                    click: function($event) {
                      outcome.r > 0 && _vm.addToCart(outcome.id)
                      outcome.r > 0 && _vm.showCart(true)
                    }
                  }
                },
                [
                  _c("div", { staticClass: "top-coefficient-for-bet" }, [
                    _vm._v(
                      "\n                    " +
                        _vm._s(outcome.l) +
                        "\n                "
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "bottom-coefficient-for-bet" }, [
                    _vm._v(_vm._s(outcome.r > 0 ? outcome.r : "-"))
                  ])
                ]
              )
            }),
            0
          )
        ],
        1
      ),
      _vm._v(" "),
      _c(
        "vue-custom-scrollbar",
        { staticClass: "scroll_markets", attrs: { settings: _vm.settings } },
        [
          _c(
            "div",
            { ref: "content", staticClass: "ui-game-show-content" },
            [
              _c("div", { staticClass: "ui-game-show-loader" }, [
                _c("span", { staticClass: "fas fa-spinner fa-spin" })
              ]),
              _vm._v(" "),
              _vm._l(_vm.markets, function(market) {
                return _c("market", {
                  key: market.id,
                  attrs: { market: market }
                })
              })
            ],
            2
          )
        ]
      )
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/list.vue?vue&type=template&id=616086ac&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "games-list" },
    [
      _c("carusel"),
      _vm._v(" "),
      _c("ui-one-click-set", {
        class: { active_line: _vm.showLive },
        attrs: {
          sportsList: _vm.list,
          showLiveIcon: _vm.showLiveIcon,
          showLive: _vm.showLive,
          title: 300
        },
        on: {
          showcart: function($event) {
            return _vm.showcart($event)
          }
        }
      }),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "carousel_wrap" },
        [
          _vm.loading
            ? _c(
                "slick",
                { attrs: { options: _vm.slickOptions } },
                [
                  _c(
                    "div",
                    {
                      staticClass: "link_wrap",
                      class: {
                        active_wrap:
                          _vm.$route.path == (_vm.showLive ? "/live" : "/line")
                      }
                    },
                    [
                      _c(
                        "router-link",
                        {
                          staticClass: "list_link all_link",
                          attrs: { to: _vm.showLive ? "/live" : "/line" }
                        },
                        [
                          _c("div", {
                            staticClass: "list_icon game-icon",
                            staticStyle: {
                              "background-image":
                                "url(/static/img/carusel/all_icon_white.png)"
                            }
                          }),
                          _vm._v(
                            "          \n                    " +
                              _vm._s(_vm.setLanguage) +
                              "\n                "
                          )
                        ]
                      )
                    ],
                    1
                  ),
                  _vm._v(" "),
                  _vm._l(_vm.getList, function(item) {
                    return _c(
                      "div",
                      { key: item.id, staticClass: "link_wrap" },
                      [
                        _c(
                          "router-link",
                          {
                            staticClass: "list_link",
                            class: {
                              active: item.id == _vm.$route.params.sports_id
                            },
                            attrs: {
                              to: {
                                name: _vm.showLive
                                  ? "live/sport"
                                  : "line/sport",
                                params: { sports_id: item.id }
                              }
                            }
                          },
                          [
                            _c("div", {
                              staticClass: "list_icon game-icon",
                              style: _vm.setIcon(item.image)
                            }),
                            _vm._v(
                              "\n                    " +
                                _vm._s(item.title) +
                                "\n                "
                            )
                          ]
                        )
                      ],
                      1
                    )
                  })
                ],
                2
              )
            : _vm._e()
        ],
        1
      ),
      _vm._v(" "),
      _vm._l(_vm.list, function(tournament) {
        return _c(
          "div",
          {
            key: tournament.tournament_id,
            staticClass:
              "ui-game-event-coefficient ui-game-event-coefficient--not_home"
          },
          [
            _c("div", { staticClass: "game-event-title" }, [
              _c("div", {
                staticClass: "game-event-icon",
                style: _vm.setIcon(tournament.sports_image)
              }),
              _vm._v(" "),
              _c("span", [_vm._v(_vm._s(tournament.tournament_title))])
            ]),
            _vm._v(" "),
            _vm._l(tournament.tournament_events, function(game) {
              return _c("games-list-item", {
                key: game.id,
                attrs: { game: game, live: _vm.live },
                on: {
                  showcart: function($event) {
                    return _vm.showcart($event)
                  }
                }
              })
            })
          ],
          2
        )
      })
    ],
    2
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/listHome.vue?vue&type=template&id=28282e2b&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "games-list" },
    [
      _c("carusel"),
      _vm._v(" "),
      _c("ui-one-click-set", {
        class: { active_line: _vm.showLive },
        attrs: {
          sportsList: _vm.list,
          showLiveIcon: _vm.showLiveIcon,
          showLive: _vm.showLive,
          title: 300
        },
        on: {
          showcart: function($event) {
            return _vm.showcart($event)
          }
        }
      }),
      _vm._v(" "),
      _c("div", { staticClass: "game-event-title top-title" }, [
        _c("div", { staticClass: "title-left", on: { click: _vm.pushLive } }, [
          _c("img", {
            attrs: { src: "/static/img/carusel/live.png", alt: "" }
          }),
          _vm._v(" "),
          _c("span", [_vm._v("ТОП LIVE")])
        ]),
        _vm._v(" "),
        _c("i", { staticClass: "fas fa-long-arrow-alt-right" })
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "ui-game-event-coefficient" },
        _vm._l(_vm.toplive, function(game) {
          return _c("games-list-item-home", {
            key: game.id,
            attrs: { game: game, live: true },
            on: {
              showcart: function($event) {
                return _vm.showcart($event)
              }
            }
          })
        }),
        1
      ),
      _vm._v(" "),
      _c("div", { staticClass: "game-event-title top-title" }, [
        _c("div", { staticClass: "title-left", on: { click: _vm.pushLine } }, [
          _c("img", {
            attrs: {
              src: "/static/img/nav-games/sports-icon_hover.png",
              alt: ""
            }
          }),
          _vm._v(" "),
          _c("span", [_vm._v("ТОП ЛИНИЯ")])
        ]),
        _vm._v(" "),
        _c("i", { staticClass: "fas fa-long-arrow-alt-right" })
      ]),
      _vm._v(" "),
      _c(
        "div",
        { staticClass: "ui-game-event-coefficient" },
        _vm._l(_vm.topline, function(game) {
          return _c("games-list-item-home", {
            key: game.id,
            attrs: { game: game, live: false },
            on: {
              showcart: function($event) {
                return _vm.showcart($event)
              }
            }
          })
        }),
        1
      ),
      _vm._v(" "),
      _vm._m(0),
      _vm._v(" "),
      _c("container-casino")
    ],
    1
  )
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "game-event-title top-title" }, [
      _c("div", { staticClass: "title-left" }, [
        _c("img", {
          attrs: { src: "/static/img/nav-games/console_hover.png", alt: "" }
        }),
        _vm._v(" "),
        _c("span", [_vm._v("GAMES")])
      ]),
      _vm._v(" "),
      _c("i", { staticClass: "fas fa-long-arrow-alt-right" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/game/ui-one-click-set.vue?vue&type=template&id=7c5e760b&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      { staticClass: "ui-one-click-set" },
      [
        _c(
          "div",
          { staticClass: "set-wrap" },
          [
            _c(
              "div",
              {
                staticClass: "one-click-live",
                class: { activeLink: _vm.home }
              },
              [_vm._m(0)]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "one-click-live",
                class: { activeLink: _vm.live },
                on: { click: _vm.pushLive }
              },
              [_vm._v("\n                Live\n            ")]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "one-click-live",
                class: { activeLink: _vm.line },
                on: { click: _vm.pushLine }
              },
              [_vm._v("\n                Линия\n            ")]
            ),
            _vm._v(" "),
            _c(
              "div",
              {
                staticClass: "navigation-search-button-mobile",
                on: { click: _vm.openSearch }
              },
              [_c("i", { staticClass: "fas fa-search" })]
            ),
            _vm._v(" "),
            _c("transition", { attrs: { name: "fade" } }, [
              _vm.search
                ? _c("div", { staticClass: "search_wrap" }, [
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.searchForm,
                          expression: "searchForm"
                        }
                      ],
                      ref: "searchInput",
                      attrs: { type: "text", placeholder: "Поиск" },
                      domProps: { value: _vm.searchForm },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.searchForm = $event.target.value
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "search_close",
                        on: { click: _vm.closeSearch }
                      },
                      [_c("i", { staticClass: "fas fa-times" })]
                    ),
                    _vm._v(" "),
                    _c(
                      "button",
                      {
                        staticClass: "search_btn",
                        on: { click: _vm.startSearch }
                      },
                      [_c("i", { staticClass: "fas fa-search" })]
                    )
                  ])
                : _vm._e()
            ])
          ],
          1
        ),
        _vm._v(" "),
        _c("ui-one-click-radio", { attrs: { title: _vm.title } })
      ],
      1
    ),
    _vm._v(" "),
    _vm.searchModal
      ? _c("div", { staticClass: "search_modal_wrap" }, [
          _c("div", {
            staticClass: "modal-overlay",
            on: { click: _vm.closeSearch }
          }),
          _vm._v(" "),
          _c("div", { staticClass: "search_modal" }, [
            _c("div", { staticClass: "header" }, [
              _c("div", { staticClass: "search_count" }, [
                _c("span", [_vm._v("Найдено Событий:")]),
                _vm._v(" "),
                _c("span", [_vm._v(_vm._s(_vm.searchAnswer.length))])
              ]),
              _vm._v(" "),
              _c(
                "div",
                { staticClass: "close_search", on: { click: _vm.closeSearch } },
                [_c("i", { staticClass: "fas fa-times" })]
              )
            ]),
            _vm._v(" "),
            _vm.searchAnswer.length > 0
              ? _c(
                  "div",
                  {
                    staticClass: "ui-game-event-coefficient search-list-mobile"
                  },
                  _vm._l(_vm.searchAnswer, function(game) {
                    return _c("games-list-item-search", {
                      key: game.id,
                      attrs: { game: game },
                      on: {
                        closeSearch: function($event) {
                          return _vm.closeSearchModal(_vm.bool)
                        },
                        showcart: function($event) {
                          return _vm.showcart($event)
                        }
                      }
                    })
                  }),
                  1
                )
              : _vm._e()
          ])
        ])
      : _vm._e()
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("a", { attrs: { href: "/" } }, [
      _c("i", { staticClass: "fas fa-home" })
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/right/cart.vue?vue&type=template&id=3fab831c&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "right_wrap" }, [
    _c("div", { staticClass: "ui-button-side-bar event-content" }, [
      _c("div", { staticClass: "close-cart", on: { click: _vm.closeCart } }, [
        _c("i", { staticClass: "fas fa-times" })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "button tac right_event" }, [
        _c(
          "div",
          {
            ref: "button",
            staticClass: "text text-right-side-active",
            on: {
              click: function($event) {
                return _vm.slipToggle()
              }
            }
          },
          [_c("span", [_vm._v(_vm._s(_vm.$t("Bet slip")))])]
        ),
        _vm._v(" "),
        _c("div", { staticClass: "text-right-side-bar" }, [
          _c("a", { attrs: { href: "/games/bets/index" } }, [
            _vm._v(_vm._s(_vm.$t("My bets")))
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "time_drop_wrap" }, [
          _c(
            "ul",
            { staticClass: "navbar-nav nav", attrs: { id: "nav-timezones" } },
            [
              _c(
                "select",
                {
                  staticClass: "form-control",
                  attrs: { id: "select-timezone", name: "timezone" }
                },
                _vm._l(_vm.timezonesList, function(value, key) {
                  return _c(
                    "option",
                    { key: key, domProps: { value: value } },
                    [
                      _vm._v(
                        "\n                            " +
                          _vm._s(key) +
                          "\n                        "
                      )
                    ]
                  )
                }),
                0
              )
            ]
          )
        ])
      ]),
      _vm._v(" "),
      _c("div", { ref: "content", staticClass: "content active" }, [
        _vm.items.length === 0
          ? _c("div", { staticClass: "ui-my-bets-content" }, [
              _c("ol", { staticClass: "ui-my-bets-help-list" }, [
                _c("li", [_vm._v(_vm._s(_vm.$t("Select sports")))]),
                _vm._v(" "),
                _c("li", [_vm._v(_vm._s(_vm.$t("Select outcome")))]),
                _vm._v(" "),
                _c("li", [_vm._v(_vm._s(_vm.$t("Select bet amount and type")))])
              ])
            ])
          : _vm._e(),
        _vm._v(" "),
        _vm.items.length > 0
          ? _c(
              "div",
              { staticClass: "ui-my-bets-content" },
              [
                _c("div", { staticClass: "my-bets-header" }, [
                  _c(
                    "div",
                    {
                      staticClass: "my-bets-header-menu-element",
                      class: { active: _vm.type === 0 },
                      on: {
                        click: function($event) {
                          return _vm.setType(0)
                        }
                      }
                    },
                    [_c("span", [_vm._v(_vm._s(_vm.$t("SINGLE")))])]
                  ),
                  _vm._v(" "),
                  _vm.items.length > 1
                    ? _c(
                        "div",
                        {
                          staticClass: "my-bets-header-menu-element",
                          class: { active: _vm.type === 1 },
                          on: {
                            click: function($event) {
                              return _vm.setType(1)
                            }
                          }
                        },
                        [_c("span", [_vm._v(_vm._s(_vm.$t("ACCUMULATOR")))])]
                      )
                    : _vm._e()
                ]),
                _vm._v(" "),
                !!_vm.messageError && _vm.messageError.length > 0
                  ? _c(
                      "div",
                      {
                        staticClass: "alert alert-danger fade in",
                        attrs: { role: "alert" }
                      },
                      [
                        _vm._v(
                          "\n                    " +
                            _vm._s(_vm.messageError) +
                            " "
                        ),
                        _c("span", {
                          staticClass: "glyphicon glyphicon-remove",
                          attrs: { "aria-hidden": "true" },
                          on: {
                            click: function($event) {
                              return _vm.hideError()
                            }
                          }
                        })
                      ]
                    )
                  : _vm._e(),
                _vm._v(" "),
                _c(
                  "transition-group",
                  {
                    staticClass: "my-bets-list",
                    attrs: { name: "cart-list", tag: "div" }
                  },
                  _vm._l(_vm.items, function(item) {
                    return _c("cart-item", {
                      key: item.id,
                      attrs: { item: item }
                    })
                  }),
                  1
                ),
                _vm._v(" "),
                _c("div", { staticClass: "my-bet-sumbit-bets-info" }, [
                  _vm.type === 1 || _vm.items.length === 1
                    ? _c("div", { staticClass: "my-bet-sumbit-bets-odds" }, [
                        _c(
                          "div",
                          { staticClass: "my-bet-sumbit-bets-odds-name" },
                          [_vm._v(_vm._s(_vm.$t("Overall odds:")))]
                        ),
                        _vm._v(" "),
                        _c(
                          "div",
                          { staticClass: "my-bet-sumbit-bets-odds-count" },
                          [_vm._v(_vm._s(_vm.rate))]
                        )
                      ])
                    : _vm._e(),
                  _vm._v(" "),
                  _c("div", { staticClass: "my-bet-submit-bets-stake" }, [
                    _c("span", [_vm._v(_vm._s(_vm.$t("Stake:")))]),
                    _vm._v(" "),
                    _c("input", {
                      directives: [
                        {
                          name: "model",
                          rawName: "v-model",
                          value: _vm.bet,
                          expression: "bet"
                        }
                      ],
                      staticClass: "my-bet-submit-bets-input",
                      attrs: {
                        type: "number",
                        step: "0.01",
                        min: _vm.cart.minimal - 1,
                        max: _vm.cart.maximal + 1
                      },
                      domProps: { value: _vm.bet },
                      on: {
                        input: function($event) {
                          if ($event.target.composing) {
                            return
                          }
                          _vm.bet = $event.target.value
                        }
                      }
                    }),
                    _vm._v(" "),
                    _c(
                      "div",
                      { staticClass: "my-bet-submit-bets-stake-currency" },
                      [
                        _c("span", [
                          _vm._v(
                            "\n                                " +
                              _vm._s(_vm.userCurrency) +
                              "\n                            "
                          )
                        ])
                      ]
                    )
                  ]),
                  _vm._v(" "),
                  _c("div", { staticClass: "my-bet-sybmit-money-set" }, [
                    _c(
                      "div",
                      {
                        staticClass: "money-quality-set",
                        on: {
                          click: function($event) {
                            _vm.bet += 10
                          }
                        }
                      },
                      [_c("span", [_vm._v("10")])]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "money-quality-set",
                        on: {
                          click: function($event) {
                            _vm.bet += 50
                          }
                        }
                      },
                      [_c("span", [_vm._v("50")])]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "money-quality-set",
                        on: {
                          click: function($event) {
                            _vm.bet += 100
                          }
                        }
                      },
                      [_c("span", [_vm._v("100")])]
                    ),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "money-quality-set",
                        on: {
                          click: function($event) {
                            _vm.bet += 1000
                          }
                        }
                      },
                      [_c("span", [_vm._v("1000")])]
                    ),
                    _vm._v(" "),
                    _vm._m(0),
                    _vm._v(" "),
                    _vm._m(1),
                    _vm._v(" "),
                    _c(
                      "div",
                      {
                        staticClass: "money-quality-set",
                        on: {
                          click: function($event) {
                            _vm.bet = 0
                          }
                        }
                      },
                      [_c("i", { staticClass: "fas fa-long-arrow-alt-left" })]
                    )
                  ]),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "my-bet-sumbit-bets-odds odds-in-the-button"
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "my-bet-sumbit-bets-odds-name" },
                        [_vm._v(_vm._s(_vm.$t("Max. stake amount:")))]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "my-bet-sumbit-bets-odds-count" },
                        [_vm._v(_vm._s(_vm.limit || 0))]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _vm.type === 1 || _vm.items.length === 1
                    ? _c(
                        "div",
                        {
                          staticClass:
                            "my-bet-sumbit-bets-odds odds-in-the-button"
                        },
                        [
                          _c(
                            "div",
                            { staticClass: "my-bet-sumbit-bets-odds-name" },
                            [_vm._v(_vm._s(_vm.$t("Potential winnings:")))]
                          ),
                          _vm._v(" "),
                          _c(
                            "div",
                            { staticClass: "my-bet-sumbit-bets-odds-count" },
                            [_vm._v(_vm._s(_vm.win))]
                          )
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass: "my-bet-sumbit-bets-odds odds-in-the-button"
                    },
                    [
                      _c(
                        "div",
                        { staticClass: "my-bet-sumbit-bets-odds-name" },
                        [_vm._v(_vm._s(_vm.$t("When odds change:")))]
                      ),
                      _vm._v(" "),
                      _c(
                        "div",
                        { staticClass: "my-bet-sumbit-bets-odds-drop" },
                        [
                          _c(
                            "select",
                            {
                              directives: [
                                {
                                  name: "model",
                                  rawName: "v-model",
                                  value: _vm.on_change,
                                  expression: "on_change"
                                }
                              ],
                              staticClass: "my-bet-sumbit-bets-odds-select",
                              on: {
                                change: function($event) {
                                  var $$selectedVal = Array.prototype.filter
                                    .call($event.target.options, function(o) {
                                      return o.selected
                                    })
                                    .map(function(o) {
                                      var val =
                                        "_value" in o ? o._value : o.value
                                      return val
                                    })
                                  _vm.on_change = $event.target.multiple
                                    ? $$selectedVal
                                    : $$selectedVal[0]
                                }
                              }
                            },
                            [
                              _c(
                                "option",
                                {
                                  attrs: {
                                    value:
                                      "<?= \\common\\modules\\games\\models\\CartsModel::ON_CHANGE_CONFIRM; ?>"
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                    " +
                                      _vm._s(_vm.$t("Confirm")) +
                                      "\n                                "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                {
                                  attrs: {
                                    value:
                                      "<?= \\common\\modules\\games\\models\\CartsModel::ON_CHANGE_AGREE; ?>"
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                    " +
                                      _vm._s(_vm.$t("Agree")) +
                                      "\n                                "
                                  )
                                ]
                              ),
                              _vm._v(" "),
                              _c(
                                "option",
                                {
                                  attrs: {
                                    value:
                                      "<?= \\common\\modules\\games\\models\\CartsModel::ON_CHANGE_ACCEPT_INCREASE; ?>"
                                  }
                                },
                                [
                                  _vm._v(
                                    "\n                                    " +
                                      _vm._s(_vm.$t("Accept when increase")) +
                                      "\n                                "
                                  )
                                ]
                              )
                            ]
                          )
                        ]
                      )
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    {
                      staticClass:
                        "my-bet-submit-bets-stake bets-take-whit-long-input"
                    },
                    [
                      _c("label", { attrs: { for: "cart-promocode" } }, [
                        _vm._v(_vm._s(_vm.$t("Promo code:")))
                      ]),
                      _vm._v(" "),
                      _c("input", {
                        staticClass: "my-bet-submit-bets-input",
                        attrs: { name: "cart-promocode", id: "cart-promocode" }
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _vm.userLoggedIn
                    ? _c(
                        "div",
                        {
                          staticClass: "my-bet-submit-place-bet-button",
                          on: {
                            click: function($event) {
                              return _vm.place()
                            }
                          }
                        },
                        [
                          _c("div", { staticClass: "ui-event-button" }, [
                            _c("span", [_vm._v(_vm._s(_vm.$t("PLACE A BET")))])
                          ])
                        ]
                      )
                    : _vm._e(),
                  _vm._v(" "),
                  !_vm.userLoggedIn
                    ? _c(
                        "div",
                        { staticClass: "my-bet-submit-place-bet-button" },
                        [
                          _c(
                            "a",
                            {
                              staticClass: "ui-event-button",
                              attrs: {
                                href: "/user/auth/login",
                                "data-toggle": "modal",
                                "data-target": "#loginPopup"
                              }
                            },
                            [_c("span", [_vm._v(_vm._s(_vm.$t("Log In")))])]
                          )
                        ]
                      )
                    : _vm._e()
                ]),
                _vm._v(" "),
                _c("div", { staticClass: "my-bet-submit-footer" }, [
                  _c(
                    "div",
                    {
                      staticClass: "my-bets-submit-footer-clear",
                      on: {
                        click: function($event) {
                          return _vm.clean()
                        }
                      }
                    },
                    [
                      _c("div", { staticClass: "my-bets-submit-clear-text" }, [
                        _vm._v(_vm._s(_vm.$t("REMOVE ALL")))
                      ]),
                      _vm._v(" "),
                      _c("div", {
                        staticClass: "my-bets-submit-clear-close-icon"
                      })
                    ]
                  ),
                  _vm._v(" "),
                  _c(
                    "div",
                    { staticClass: "my-bets-submit-footer-check-box" },
                    [
                      _c("input", {
                        staticClass: "my-bets-submit-footer-check-box-element",
                        attrs: {
                          type: "checkbox",
                          name: "cart-clean",
                          id: "cart-clean",
                          checked: ""
                        }
                      }),
                      _vm._v(" "),
                      _c(
                        "label",
                        {
                          staticClass: "my-bets-submit-check-box-text",
                          attrs: { for: "cart-clean" }
                        },
                        [
                          _vm._v(
                            "\n                            " +
                              _vm._s(
                                _vm.$t("Clear bet slip after placing a bet")
                              ) +
                              "\n                        "
                          )
                        ]
                      )
                    ]
                  )
                ])
              ],
              1
            )
          : _vm._e()
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "money-quality-set money_disable" }, [
      _c("span", [_vm._v("2500")])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "money-quality-set money_disable" }, [
      _c("span", [_vm._v("5000")])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/mobile/right/casino.vue?vue&type=template&id=0d449647&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _vm._m(0)
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "game_sidebar" }, [
      _c("ul", { staticClass: "game_sidebar_list" }, [
        _c("li", [
          _c("a", { attrs: { href: "/games/mini-games/game?name=roulette" } }, [
            _c("div", {
              staticClass: "game_sidebar_img",
              staticStyle: {
                "background-image": "url(/static/img/mini-games/roulet.jpg)"
              }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "game_sidebar_title" }, [
              _vm._v("\n                    Roulette\n                ")
            ])
          ])
        ]),
        _vm._v(" "),
        _c("li", [
          _c(
            "a",
            {
              attrs: { href: "/games/mini-games/game?name=rock-paper-scissors" }
            },
            [
              _c("div", {
                staticClass: "game_sidebar_img game2",
                staticStyle: {
                  "background-image": "url(/static/img/mini-games/rock.jpg)"
                }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "game_sidebar_title" }, [
                _vm._v(
                  "\n                    Rock Paper Scissors\n                "
                )
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _c(
            "a",
            { attrs: { href: "/games/mini-games/game?name=heads-or-tails" } },
            [
              _c("div", {
                staticClass: "game_sidebar_img game2",
                staticStyle: {
                  "background-image":
                    "url(/static/img/mini-games/headsortails.jpg)"
                }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "game_sidebar_title" }, [
                _vm._v("\n                    Heads of Tails\n                ")
              ])
            ]
          )
        ]),
        _vm._v(" "),
        _c("li", [
          _c(
            "a",
            { attrs: { href: "/games/mini-games/game?name=lucky-card" } },
            [
              _c("div", {
                staticClass: "game_sidebar_img game2",
                staticStyle: {
                  "background-image": "url(/static/img/mini-games/lucky.jpg)"
                }
              }),
              _vm._v(" "),
              _c("div", { staticClass: "game_sidebar_title" }, [
                _vm._v("\n                    Lucky card\n                ")
              ])
            ]
          )
        ])
      ])
    ])
  }
]
render._withStripped = true



/***/ }),

/***/ "./node_modules/vue-loader/lib/loaders/templateLoader.js?!./node_modules/cache-loader/dist/cjs.js!./node_modules/thread-loader/dist/cjs.js!./node_modules/vue-loader/lib/index.js?!./frontend/assets/games/js/components/right/cart-item.vue?vue&type=template&id=618d35c2&":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return render; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return staticRenderFns; });
var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    {
      staticClass: "ui-my-bet-game-component cart-list-item",
      class: _vm.classBlocked
    },
    [
      _c("div", { staticClass: "ui-my-bet-game-content" }, [
        _c("div", {
          staticClass: "ui-my-bet-game-content-icon",
          style: _vm.setIcon(_vm.item.sport_image)
        }),
        _vm._v(" "),
        _c("div", { staticClass: "show-games-teams" }, [
          _c("div", { staticClass: "show-team-name" }, [
            _vm._v(_vm._s(_vm.item.game_title))
          ])
        ]),
        _vm._v(" "),
        _c("div", {
          staticClass: "ui-my-bet-game-close-icon",
          on: {
            click: function($event) {
              return _vm.remove()
            }
          }
        })
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "ui-my-bet-game-quality" }, [
        _c("span", [_vm._v(_vm._s(_vm.item.market_title))])
      ]),
      _vm._v(" "),
      _c("div", { staticClass: "ui-my-bet-game-coeff" }, [
        _c("div", { staticClass: "my-bet-game-coeff-name" }, [
          _c("span", [_vm._v(_vm._s(_vm.item.outcome_title))]),
          _vm._v(" "),
          _c("div", { staticClass: "coeffComponentCount" }, [
            _vm._v("\n                 ("),
            _c("span", { staticClass: "ComponentCountCoeff" }, [
              _vm._v(_vm._s(_vm.item.rate_now))
            ]),
            _vm._v(")\n                "),
            _c(
              "span",
              {
                staticClass: "price-confirm",
                on: {
                  click: function($event) {
                    return _vm.confirm()
                  }
                }
              },
              [_c("i", { staticClass: "fas fa-check" })]
            )
          ])
        ]),
        _vm._v(" "),
        _c("div", { staticClass: "my-bet-game-coeff-quality" }, [
          _c("span", [_vm._v(_vm._s(_vm.item.rate))])
        ])
      ])
    ]
  )
}
var staticRenderFns = []
render._withStripped = true



/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ 3:
/***/ (function(module, exports, __webpack_require__) {

__webpack_require__("./frontend/assets/games/js/games-mobile.js");
module.exports = __webpack_require__("./frontend/assets/games/scss/games.sass");


/***/ }),

/***/ "jquery":
/***/ (function(module, exports) {

module.exports = jQuery;

/***/ }),

/***/ "socket.io-client":
/***/ (function(module, exports) {

module.exports = io;

/***/ })

/******/ });
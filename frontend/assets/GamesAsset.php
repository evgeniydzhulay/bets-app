<?php

namespace frontend\assets;

use common\assets\SocketIoAsset;
use kartik\select2\Select2Asset;
use kartik\select2\ThemeDefaultAsset;
use yii\web\AssetBundle;

class GamesAsset  extends AssetBundle {

	/**
	 * {@inheritdoc}
	 */
	public $sourcePath = null;

	/**
	 * {@inheritdoc}
	 */
	public $basePath = '@webroot';

	/**
	 * {@inheritdoc}
	 */
	public $baseUrl = '@web';

	/**
	 * {@inheritdoc}
	 */
	public $css = [
		IS_MOBILE ? 'static/css/games.mobile.min.css': 'static/css/games.desktop.min.css',
	];

	/**
	 * {@inheritdoc}
	 */
	public $js = [
		IS_MOBILE ? 'static/js/games.mobile.min.js': 'static/js/games.desktop.min.js',
	];

	/**
	 * {@inheritdoc}
	 */
	public $depends = [
		Select2Asset::class,
		ThemeDefaultAsset::class,
		MainAsset::class,
		VueAsset::class,
		SocketIoAsset::class,
	];
}
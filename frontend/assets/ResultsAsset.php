<?php

namespace frontend\assets;

use common\assets\SocketIoAsset;
use yii\web\AssetBundle;

class ResultsAsset  extends AssetBundle {

	/**
	 * {@inheritdoc}
	 */
	public $sourcePath = null;

	/**
	 * {@inheritdoc}
	 */
	public $basePath = '@webroot';

	/**
	 * {@inheritdoc}
	 */
	public $baseUrl = '@web';

	/**
	 * {@inheritdoc}
	 */
	public $css = [
		IS_MOBILE ? 'static/css/results.mobile.min.css': 'static/css/results.min.css',
	];

	/**
	 * {@inheritdoc}
	 */
	public $js = [
		IS_MOBILE ? 'static/js/results.mobile.min.js': 'static/js/results.desktop.min.js',
	];

	/**
	 * {@inheritdoc}
	 */
	public $depends = [
		MainAsset::class,
		VueAsset::class,
		SocketIoAsset::class,
	];
}
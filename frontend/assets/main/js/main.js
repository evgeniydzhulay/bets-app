function reloadUserPurse() {
    fetch(`/user/purse/get-amount`).then((result) => result.json()).then((result) => {
        jQuery('#nav-user-website-purse_amount').html(result.amount);
    }).catch((err) => {
        console.error(err);
    });
}
jQuery(function() {
    jQuery('body').on('click', '.expand-block', function (ev) {
        const button = jQuery(ev.currentTarget);
        const target = button.data('target');
        if(!target) {
            return;
        }
        const targetElement = jQuery('#' + target);
        button.toggleClass('show-button-active');
        targetElement.toggleClass('opened');
    });
    jQuery('#nav-user-website-purse').click(function() {
        reloadUserPurse();
    });
    setInterval(function () {
        const date = new Date(+Date.now() + window.timeDiff);
        jQuery('#select2-select-timezone-container').html(
            ("0" + date.getHours()).slice(-2) + ':' +
            ("0" + date.getMinutes()).slice(-2)
        );
    }, 1000);
    const accountButton = document.getElementsByClassName('ui-account-information-drop-menu')[0];
    if(!!accountButton) {
        const content = document.getElementsByClassName('account-content')[0];
        window.addEventListener('click', ({target}) => {
            if(content.className.includes('account-content-active') && !target.parentNode.className.includes('ui-account-information-drop-menu')){
                content.classList.remove('account-content-active');
                document.getElementById('rotateArrow').style.transform = "rotate(0deg) translateY(0%)";
            }
        });
        accountButton.addEventListener('click', () => {
            if(content.className.includes('account-content-active')){
                content.classList.remove('account-content-active');
                document.getElementById('rotateArrow').style.transform = "rotate(0deg) translateY(0%)";
            }else{
                content.classList.add('account-content-active');
                document.getElementById('rotateArrow').style.transform = "rotate(180deg) translateY(-36%)";
            }
        });
    }
});

// Withdraw page
$(document).ready(function(){
    // console.log(window.location.pathname.includes('user') || window.location.pathname.includes('results'));    
    $('#account-withdraw-form .btn-group').each(function (id, el) {
        // console.log($(el).find('input').val()); 
        $(el).addClass($(el).find('input').val());
        $(el).click(function () {
            $('.form_with_wrap').show();
            $('.overlay_modal').show();
        });
    });
    $('.overlay_modal').click(function () {
        $('.form_with_wrap').hide();
        $(this).hide();
    });
    $('.form_with_close').click(function () {
        $('.form_with_wrap').hide();
        $('.overlay_modal').hide();
    });
});

$('.auto_item').click(function(){
    // console.log($(this).data('value'));  
    $('#pursedepositform-amount').val(+$('#pursedepositform-amount').val() + +$(this).data('value'));
});
// $(document).ready(function(){
    $('body').on('click', '.show_video_sidebar', function(){       
        // $('#video_sidebar').show();        
        // $('#urlParam').attr('value', $(this).data('val')); 
        const val = $(this).data('val');
        const idx = $(this).data('id');
        const html = document.documentElement.getAttribute("lang");
        let text = {};
        if(html == 'ru'){
            text = { 
                video: "Видео",
                login: "Войти",
                reg: "Регистрация"
            }
        } else {
            text = { 
                video: "Video",
                login: "Log In",
                reg: "Registration"
            }
        };
        
        if(!$(`#${idx}`).length){
            $('.video_sidebar_wrap').prepend(`
                <div class="video_sidebar" id="${idx}">
                    <div class="ui-button">
                        <div class="text">
                            <span>${text.video}</span>
                        </div>
                        <div class="ui-button-icon icon-close"></div>                
                    </div>
                    <div class="video_sidebar_content">
                        <object type="application/x-shockwave-flash" data="/static/player.swf">
                            <param name="menu" value="false">
                            <param name="wmode" value="opaque">
                            <param name="allowFullScreen" value="true">
                            <param name="AllowScriptAccess" value="always">
                            <param name="flashvars" class="id_video_obj" value="${val}">
                        </object>
    
                        <div class="without_reg without_reg_side hide">
                            <div class="btns_wrap">
                                <a href="/user/auth/login" class="ui-radius-button log-in" data-toggle="modal" data-target="#loginPopup">${text.login}</a>
                                <a href="/user/auth/register" class="ui-radius-button">${text.reg}</a>
                            </div>
                        </div>
                    </div>
                </div>
            `);                    
        }
        
        $('.icon-close').click(function () {            
            $(this).parent('.ui-button').parent('.video_sidebar').remove();
        });
    });

// });

// Mini games category
$('.drop_togle').click(function () {
    $(this).parent('.button').toggleClass('active');
    $('.mini_cat_content').stop().slideToggle();
});

// Burger btn
$('#mobileMenuButton').click(function(){
        if($('.navigation-menu').css('right') === '0%') {
            $('.navigation-menu').css('right', '-150%')
        } else {
            $('.navigation-menu').css('right', '0')
        }
})


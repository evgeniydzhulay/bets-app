<?php

namespace frontend\assets;

use common\assets\FontAmesomeAsset;
use kartik\icons\FlagIconAsset;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class MainAsset extends AssetBundle {

	/**
	 * {@inheritdoc}
	 */
	public $sourcePath = null;

	/**
	 * {@inheritdoc}
	 */
	public $basePath = '@webroot';

	/**
	 * {@inheritdoc}
	 */
	public $baseUrl = '@web';

	/**
	 * {@inheritdoc}
	 */
	public $css = [
		'static/css/main.min.css',
	];

	/**
	 * {@inheritdoc}
	 */
	public $js = [
		'static/js/main.min.js'
	];

	/**
	 * {@inheritdoc}
	 */
	public $depends = [
		JqueryAsset::class,
		FontAmesomeAsset::class,
		BootstrapAsset::class,
		BootstrapPluginAsset::class,
		FlagIconAsset::class,
	];
}
<?php

namespace frontend\assets;

use yii\web\AssetBundle;

class VueAsset extends AssetBundle {

	/**
	 * {@inheritdoc}
	 */
	public $sourcePath = null;

	/**
	 * {@inheritdoc}
	 */
	public $basePath = '@webroot';

	/**
	 * {@inheritdoc}
	 */
	public $baseUrl = '@web/static/js';

	/**
	 * {@inheritdoc}
	 */
	public $js = [
		'vue.min.js',
	];
}
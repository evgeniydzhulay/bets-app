const baseConfig = require('./webpack.prod.js');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const CssExtractPlugin = require('mini-css-extract-plugin');

module.exports = Object.assign({}, baseConfig, {
    mode: 'development',
    name: 'casino-dev',
    devtool: false,
    output: {
        pathinfo: false,
        path: path.resolve(path.dirname(path.dirname(__dirname)), 'web/static'),
        filename: 'casino/js/[name].js',
        chunkFilename: 'casino/js/[name].js',
    },
    plugins: [
        new VueLoaderPlugin(),
        new CssExtractPlugin({
            filename: 'casino/css/[name].css',
            chunkFilename: 'casino/css/[id].css'
        }),
    ],
});
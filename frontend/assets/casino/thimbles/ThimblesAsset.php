<?php

namespace frontend\assets\casino\thimbles;

use frontend\assets\MainAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

class ThimblesAsset  extends AssetBundle {

	/**
	 * {@inheritdoc}
	 */
	public $sourcePath = null;

	/**
	 * {@inheritdoc}
	 */
	public $basePath = '@webroot/static/casino/';

	/**
	 * {@inheritdoc}
	 */
	public $baseUrl = '@web/static/casino/';

	/**
	 * {@inheritdoc}
	 */
	public $css = [
		'css/thimbles.min.css',
	];

	/**
	 * {@inheritdoc}
	 */
	public $js = [
		'js/vue.min.js',
		'js/thimbles.min.js',
	];

	public $depends = [
		YiiAsset::class,
		BootstrapAsset::class,
		JqueryAsset::class,
		MainAsset::class,
	];
}
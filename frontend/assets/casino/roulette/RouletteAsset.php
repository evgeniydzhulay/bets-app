<?php

namespace frontend\assets\casino\roulette;

use frontend\assets\MainAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

class RouletteAsset  extends AssetBundle {

	/**
	 * {@inheritdoc}
	 */
	public $sourcePath = null;

	/**
	 * {@inheritdoc}
	 */
	public $basePath = '@webroot/static/casino/';

	/**
	 * {@inheritdoc}
	 */
	public $baseUrl = '@web/static/casino/';

	/**
	 * {@inheritdoc}
	 */
	public $css = [
		'css/roulette.min.css',
	];

	/**
	 * {@inheritdoc}
	 */
	public $js = [
		'js/roulette.js',
	];

	public $depends = [
		YiiAsset::class,
		BootstrapAsset::class,
		JqueryAsset::class,
		MainAsset::class,
	];
}
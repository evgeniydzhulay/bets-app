import $ from 'jquery';

$.arcticmodal = $.fn.arcticmodal = function() {
    $(this).removeClass('hidden').on('click', '.modal-close', () => {
        $(this).addClass('hidden');
    });
};

Number.isFinite = Number.isFinite || function(value) {
    return typeof value === 'number' && isFinite(value);
};

Number.isInteger = Number.isInteger || function(value) {
    return typeof value === 'number' && Number.isFinite(value) && !(value % 1);
};
const RouletteIso = {
    gameState : false,
    get gameStarted() {
        return this.gameState;
    },
    set gameStarted(bool) {
        $('.roulette__chip').attr('draggable', !bool);
        this.gameState = bool;
    },
    $draggedChip : {},
    $dropWithPopup : {},
    sumWin : 0,
    set setBet(bet){
        $('.js_sum_bet_container').html(bet)
    },
    getItems : function (drop) {
        let $this = drop,
            $item = $('.roulette-grid__cell'),
            $parent = $this.parents('.roulette-grid__cell'),
            $index = $parent.index(),
            $id = $parent.attr('id') || false,
            $items = $parent,
            $red = $('.roulette-grid__cell.red'),
            $black = $('.roulette-grid__cell.black'),
            $cc;

        $('.roulette-grid__cell.highlight').removeClass('highlight');
        //Нужно ли подсвечивать смежные ячейки?
        if ($this.hasClass('roulette-grid__drop_quad')) {
            $items.push($item.eq($index - 1), $item.eq($index - 3), $item.eq($index - 4));
        } else if ($this.hasClass('roulette-grid__drop_vpair')) {
            $items.push($item.eq($index - 3));
        } else if ($this.hasClass('roulette-grid__drop_hpair')) {
            $items.push($item.eq($index - 1));
        }
        //Нужно ли подсвечивать группы? Не забываем про хитрый порядок ячеек
        else if ($id) {
            switch($id) {
                case 'red' :
                    for ($cc = 0; $cc < $red.length; $cc ++) {
                        $items.push($red.eq($cc));
                    }
                    break;
                case 'black' :
                    for ($cc = 0; $cc < $black.length; $cc ++) {
                        $items.push($black.eq($cc));
                    }
                    break;
                case 'even' :
                    for ($cc = 1; $cc < 37; $cc ++) {
                        if (!($cc % 2)) {
                            $items.push($item.eq($cc));
                        }
                    }
                    break;
                case 'odd' :
                    for ($cc = 1; $cc < 37; $cc++) {
                        if ($cc % 2) {
                            $items.push($item.eq($cc));
                        }
                    }
                    break;
                case 'first-half' :
                    for ($cc = 1; $cc < 19; $cc++) {
                        $items.push($item.eq($cc));
                    }
                    break;
                case 'second-half' :
                    for ($cc = 19; $cc < 37; $cc++) {
                        $items.push($item.eq($cc));
                    }
                    break;
                case 'first-dozen' :
                    for ($cc = 1; $cc < 13; $cc++) {
                        $items.push($item.eq($cc));
                    }
                    break;
                case 'second-dozen' :
                    for ($cc = 13; $cc < 25; $cc++) {
                        $items.push($item.eq($cc));
                    }
                    break;
                case 'third-dozen' :
                    for ($cc = 25; $cc < 37; $cc++) {
                        $items.push($item.eq($cc));
                    }
                    break;
                case 'first-row' :
                    for ($cc = 3; $cc < 37; $cc += 3) {
                        $items.push($item.eq($cc));
                    }
                    break;
                case 'second-row' :
                    for ($cc = 2; $cc < 37; $cc += 3) {
                        $items.push($item.eq($cc));
                    }
                    break;
                case 'third-row' :
                    for ($cc = 1; $cc < 37; $cc += 3) {
                        $items.push($item.eq($cc));
                    }
                    break;
            }
        }
        return $items;
    },
    highlightOver : function(drop) {
        this.getItems(drop).each(function() {
            $(this).addClass('highlight');
        });
    },
    getBets : function () {
        let $chips = $('.roulette__grids .roulette__chip'),
            nums_arr = [],
            nums = '',
            bet = 0,
            res = [];

        $chips.map(function (i, chip) {
            chip = $(chip);
            bet = Number(chip.attr('data-cost'));
            nums = chip.attr('data-nums');
            if (!bet) {
                window.location.reload();
                return;
            }
            if (nums_arr.indexOf(nums) + 1) {
                res[nums_arr.indexOf(nums)].amount += bet;
            } else {
                res.push({
                    selected : nums,
                    amount : bet
                });
                nums_arr.push(nums);
            }
        });

        return res;
    },
    rotateWheel : function(n) {
        let $roulette = $('.roulette__roulette'),
            $outerSector = $('.roulette-outer__segment'),
            $innerSector = $('.roulette-inner__segment'),
            $border = $('.roulette-borders__segment'),
            $ball = $('#rouletteb'),
            s = 0,
            step = 9.73,
            extra = (Math.random() < 0.5 ? -1 : 1) * (Math.ceil(Math.random() * 170) + 20);
        $('#roulette_iso_res_msg').css('opacity', 0);
        if (n >= 0 && n < 37 && Number.isInteger(n)) {
            $ball.hide().attr('style', 'transition: transform 0s; transform: rotateX(25deg) rotateZ(0deg) translate(7em, 6.5em) rotateZ(0deg) rotateX(-25deg);');
            $roulette.removeClass('idle');
            $outerSector.attr('style', 'transition: transform 0s');
            $innerSector.attr('style', 'transition: transform 0s');
            $border.attr('style', 'transition: transform 0s');
            switch(n) {
                case 7 :
                    s = 0;
                    break;
                case 29 :
                    s = 1;
                    break;
                case 18 :
                    s = 2;
                    break;
                case 22 :
                    s = 3;
                    break;
                case 9 :
                    s = 4;
                    break;
                case 31 :
                    s = 5;
                    break;
                case 14 :
                    s = 6;
                    break;
                case 20 :
                    s = 7;
                    break;
                case 1 :
                    s = 8;
                    break;
                case 33 :
                    s = 9;
                    break;
                case 16 :
                    s = 10;
                    break;
                case 24 :
                    s = 11;
                    break;
                case 5 :
                    s = 12;
                    break;
                case 10 :
                    s = 13;
                    break;
                case 23 :
                    s = 14;
                    break;
                case 8 :
                    s = 15;
                    break;
                case 30 :
                    s = 16;
                    break;
                case 11 :
                    s = 17;
                    break;
                case 36 :
                    s = 18;
                    break;
                case 13 :
                    s = 19;
                    break;
                case 27 :
                    s = 20;
                    break;
                case 6 :
                    s = 21;
                    break;
                case 34 :
                    s = 22;
                    break;
                case 17 :
                    s = 23;
                    break;
                case 25 :
                    s = 24;
                    break;
                case 2 :
                    s = 25;
                    break;
                case 21 :
                    s = 26;
                    break;
                case 4 :
                    s = 27;
                    break;
                case 19 :
                    s = 28;
                    break;
                case 15 :
                    s = 29;
                    break;
                case 32 :
                    s = 30;
                    break;
                case 0 :
                    s = 31;
                    break;
                case 26 :
                    s = 32;
                    break;
                case 3 :
                    s = 33;
                    break;
                case 35 :
                    s = 34;
                    break;
                case 12 :
                    s = 35;
                    break;
                case 28 :
                    s = 36;
                    break;
            }
            setTimeout(function() {
                for(var $k = 0; $k < 37; $k++) {
                    $outerSector.eq($k).attr('style', 'transform: rotateX(30deg) translateX(-50%) rotateZ(' + (774 + $k * 9.73 + extra + s * step) + 'deg) rotateX(-25deg);');
                    $innerSector.eq($k).attr('style', 'transform: rotateX(30deg) translateX(-50%) rotateZ(' + (774 + $k * 9.73 + extra + s * step) + 'deg) rotateX(0deg);');
                    $border.eq($k).attr('style', 'transform: rotateX(30deg) translateX(-50%) rotateZ(' + (774 + $k * 9.73 + 4.865 + extra + s * step) + 'deg) rotateX(0deg);');
                }
                $ball.attr('style', 'transition: transform 2700ms linear; transform: rotateX(30deg) rotateZ(-635deg) translate(7em, 6.5em) rotateZ(635deg) rotateX(-30deg);')
                    .show().delay(2700).queue(function(next) {
                    $ball.attr('style', 'transition: transform 240ms linear; transform: rotateX(30deg) rotateZ(-625deg) translate(5.5em, 5.5em) rotateZ(625deg) rotateX(-30deg);');
                    next();
                }).delay(240).queue(function(next) {
                    $ball.attr('style', 'transition: transform 240ms linear; transform: rotateX(30deg) rotateZ(-645deg) translate(4.4em, 4em) rotateZ(645deg) rotateX(-30deg);');
                    next();
                }).delay(240).queue(function(next) {
                    $ball.attr('style', 'transition: transform 120ms linear; transform: rotateX(30deg) rotateZ(-655deg) translate(4.2em, 4.2em) rotateZ(655deg) rotateX(-30deg);');
                    next();
                }).delay(120).queue(function(next) {
                    $ball.attr('style', 'transition: transform 60ms ease-out; transform: rotateX(30deg) rotateZ(-657deg) translate(4.2em, 4.2em) rotateZ(657deg) rotateX(-30deg); animation-timing-function: ease-out;');
                    next();
                }).delay(60).queue(function(next) {
                    $ball.attr('style', 'transition: transform 120ms ease-out; transform: rotateX(30deg) rotateZ(-667deg) translate(4.2em, 4.2em) rotateZ(667deg) rotateX(-30deg); animation-timing-function: ease-out;');
                    next();
                }).delay(120).queue(function(next) {
                    $ball.attr('style', 'transition: transform 1920ms ease-out; transform: rotateX(30deg) rotateZ(' + (-137 + extra) + 'deg) translate(4.2em, 4.2em) rotateZ(' + (137 - extra) + 'deg) rotateX(-30deg); animation-timing-function: ease-out;');
                    next();
                }).delay(1920).queue(function(next) {
                    $ball.attr('style', 'transition: transform 600ms ease-in-out; transform: rotateX(30deg) rotateZ(' + (-139 + extra) + 'deg) translate(4.2em, 4.2em) rotateZ(' + (139 - extra) + 'deg) rotateX(-30deg);');
                    next();
                });
            }, 100);
            this.highlightWinner(n);
        }
    },
    highlightWinner : function(n) {
        let $winners = [$('.sector' + n), $('.cell_' + n)],
            red = [1, 3, 5, 7, 9, 12, 14, 16, 18, 19, 21, 23, 25, 27, 30, 32, 34, 36],
            color = '',
            evenOrOdd = '';
        $('.winner').removeClass('winner');
        if (!n) {
            $winners.push('.cell_0');
        } else {
            //цвет
            if (red.indexOf(n) + 1) {
                $winners.push($('#red'));
                color = 'red';
            } else {
                $winners.push($('#black'));
                color = 'black';
            }
            //ряд
            if (!(n % 3)) {
                $winners.push($('.roulette-grid_numbers .roulette-grid__cell:nth-last-of-type(3)'));
            } else if (n % 3 === 2) {
                $winners.push($('.roulette-grid_numbers .roulette-grid__cell:nth-last-of-type(2)'));
            } else {
                $winners.push($('.roulette-grid_numbers .roulette-grid__cell:nth-last-of-type(1)'));
            }
            //чет/нечёт
            if (!(n % 2)) {
                $winners.push($('#even'));
                evenOrOdd = 'even';
            } else {
                $winners.push($('#odd'));
                evenOrOdd = 'odd';
            }
            //дюжины и половины
            if (n < 13) {
                $winners.push($('#first-dozen'), $('#first-half'));
            } else if (n > 24) {
                $winners.push($('#third-dozen'), $('#second-half'));
            } else {
                $winners.push($('#second-dozen'));
                if (n < 19) {
                    $winners.push($('#first-half'));
                } else {
                    $winners.push($('#second-half'));
                }
            }
        }
        setTimeout(function() {
            $($winners).each(function() {
                $(this).addClass('winner');
            });
        }, 300);
        setTimeout(function() {
            $('#roulette_iso_res_msg').css('opacity', 1).html(!n ? 'Result: Zero' : 'Result: {0}, {1}, {2}'.replace('{0}', n).replace('{1}', color).replace('{2}', evenOrOdd));
            RouletteIso.showResult();
        }, 5900);
    },
    setNums : function (elem, chip) {
        chip = $(chip) || null;

        let $this = $(elem),
            $cells = RouletteIso.getItems($this),
            nums = '',
            sum_bet = 0;

        $cells.map(function (i, cell) {
            let num = $(cell).attr('data-num');
            if ($.isNumeric(num)) {
                nums += num.toString();
                if (i < $cells.length - 1) {
                    nums += ', ';
                }
            }
        });
        chip && chip.attr('data-nums', nums);
        RouletteIso.getBets().map(function (bet) {
            sum_bet += bet.amount;
        });
        RouletteIso.setBet = sum_bet;
    },
    showResult : function () {
        let $popup = this.sumWin ? $('#roulette-win') : $('#roulette-lose');
        this.sumWin && $('#roulette_popup_sumwin').html('You have won: {0}'.replace('{0}', this.sumWin));
        this.gameStarted = false;
        $popup.popup();
    },
    startRotate : function () {
        this.hidePopupWithChips();
        if (window.isGuest) {
            $('#roulette-auth').arcticmodal();
            return;
        }
        const bets = this.getBets();
        this.sumWin = 0;
        bets.map(function (bet, i) {
            bets[i].selected = bets[i].selected.split(', ');
        });
        if (this.gameStarted || !bets.length) {
            return;
        }
        this.gameStarted = true;

        $.post('/casino/roulette/game', {
            bets: bets,
            [yii.getCsrfParam()]: yii.getCsrfToken()
        }).done((result) => {
            if(!result) {
                result = {
                    success: false,
                    message: 'internal error'
                };
            }
            if (!!result && !result.success && !!result.message) {
                RouletteIso.parse_error(result.message);
            } else {
                RouletteIso.sumWin = result.amount;
                RouletteIso.rotateWheel(Number(result.result));
            }
        });
    },
    clearBets : function () {
        $('.roulette__grids .roulette__chip').remove();
        RouletteIso.setBet = 0;
    },
    auth : function () {
        $('#loginPopup').modal({
            remote: '/user/auth/login'
        });
    },
    parse_error : function (data) {
        let $popup = $('#roulette-error');
        $popup.find('.roulette-popup__heading').html(data);
        $popup.arcticmodal();
        this.gameStarted = false;
    },
    hidePopupWithChips : function () {
        $('#roulettew').hide();
        RouletteIso.$dropWithPopup.length && RouletteIso.$dropWithPopup.removeClass('with-riw');
    }
};

$('#roulette-app').on('dragstart', '.roulette__chip', function(e) {
    // хак для лисы
    e.originalEvent.dataTransfer.setData('text/plain', 'anything');
    RouletteIso.$draggedChip =  $(this);
    $('.roulette-grid__hint').fadeOut();
}).on('dragend', '.roulette__chip', function() {
    $('.roulette-grid__cell.highlight').removeClass('highlight');
}).on('mouseleave', '.roulette-grid__drop', function() {
    $('.roulette-grid__cell.highlight').removeClass('highlight');
}).on('dragenter', '.roulette-grid__drop', function() {
    RouletteIso.highlightOver($(this));
}).on('mouseenter', '.roulette-grid__drop', function() {
    RouletteIso.highlightOver($(this));
}).on('dragover', '.roulette-grid__drop', function(e) {
    e.preventDefault();
}).on('click', '.roulette-window__btn', function() {
    RouletteIso.$dropWithPopup.find('.roulette__chip').remove();
    RouletteIso.setNums(RouletteIso.$dropWithPopup);
}).on('click', '.roulette-bet__rules', function() {
    $('#roulette-rules').modal();
}).on('drop', '.roulette-grid__drop', function(e) {
    e.preventDefault();
    if (RouletteIso.gameStarted) {
        return;
    }
    let $clone = RouletteIso.$draggedChip.clone(),
        $this = $(this);
    $this.append($clone);
    $('.roulette-grid__cell.highlight').removeClass('highlight');
    //Если фишку перетаскивали не со стола, а из другого поля, то убираем её оттуда
    RouletteIso.$draggedChip.parent('.roulette-grid__drop').length && RouletteIso.$draggedChip.remove();
    RouletteIso.setNums($this, $clone);
}).on('click', '.roulette', function (e) {
    let $target = $(e.target);
    if (RouletteIso.$dropWithPopup.length && !($target.is('#roulettew') || $target.closest('#roulettew').length || $target.is('.roulette-grid__drop') || $target.closest('.roulette-grid__drop').length)) {
        RouletteIso.hidePopupWithChips();
    }
}).on('click', '.roulette-grid__drop', function() {
    if (RouletteIso.gameStarted) {
        return;
    }
    let $this = $(this);
    if ($this.hasClass('with-riw')) {
        return;
    }
    let $popup = $('#roulettew'),
        $area = $('#rouletteg'),
        $drop = $('.roulette-grid__drop'),
        dimensions = this.getBoundingClientRect(),
        $left = $this.offset().left - $area.offset().left + dimensions.width / 2,
        $top = $this.offset().top - $area.offset().top + dimensions.height / 2;
    $popup.hide();
    $drop.removeClass('with-riw');
    RouletteIso.$dropWithPopup = $this;
    $this.addClass('with-riw');
    $popup.css({left : $left, top: $top}).fadeIn(300);
}).on('click', '.roulette-window__close', function() {
    RouletteIso.hidePopupWithChips();
}).on('click', '#roulettew .roulette__chip', function() {
    let $this = $(this),
        $clone = $this.clone();
    RouletteIso.$dropWithPopup.append($clone);
    $clone.attr('draggable', true);
    RouletteIso.setNums(RouletteIso.$dropWithPopup, $clone);
}).on('click', '.roulette-controls__item_play, .roulette-bet__btn_play', function() {
    RouletteIso.startRotate();
}).on('click', '.js_clear_bet', function () {
    RouletteIso.clearBets();
});

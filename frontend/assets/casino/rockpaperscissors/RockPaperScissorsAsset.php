<?php

namespace frontend\assets\casino\rockpaperscissors;

use frontend\assets\MainAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

class RockPaperScissorsAsset  extends AssetBundle {

	/**
	 * {@inheritdoc}
	 */
	public $sourcePath = null;

	/**
	 * {@inheritdoc}
	 */
	public $basePath = '@webroot/static/casino/';

	/**
	 * {@inheritdoc}
	 */
	public $baseUrl = '@web/static/casino/';

	/**
	 * {@inheritdoc}
	 */
	public $css = [
		'css/rockpaperscissors.min.css',
	];

	/**
	 * {@inheritdoc}
	 */
	public $js = [
		'js/vue.min.js',
		'js/rockpaperscissors.min.js',
	];

	public $depends = [
		YiiAsset::class,
		BootstrapAsset::class,
		JqueryAsset::class,
		MainAsset::class,
	];
}
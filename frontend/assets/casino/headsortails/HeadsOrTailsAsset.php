<?php

namespace frontend\assets\casino\headsortails;

use frontend\assets\MainAsset;
use yii\bootstrap\BootstrapAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;
use yii\web\YiiAsset;

class HeadsOrTailsAsset  extends AssetBundle {

	/**
	 * {@inheritdoc}
	 */
	public $sourcePath = null;

	/**
	 * {@inheritdoc}
	 */
	public $basePath = '@webroot/static/casino/';

	/**
	 * {@inheritdoc}
	 */
	public $baseUrl = '@web/static/casino/';

	/**
	 * {@inheritdoc}
	 */
	public $css = [
		'css/headsortails.min.css',
	];

	/**
	 * {@inheritdoc}
	 */
	public $js = [
		'js/vue.min.js',
		'js/headsortails.min.js',
	];

	public $depends = [
		YiiAsset::class,
		BootstrapAsset::class,
		JqueryAsset::class,
		MainAsset::class,
	];
}
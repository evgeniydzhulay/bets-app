import Vue from 'vue';
import {connection, subscribe, unsubscribe} from "backend/socket";

const CartInstance = new Vue({
    data: {
        items: [],
        bet: 0,
        on_change: 0,
        messageError: '',
        minimal: 0,
        maximal: 0,
    },
    created: function() {
        this.load();
        connection.on('or', (data) => {
            const item = this.items.find(outcomeItem => outcomeItem.outcome_id === data.o);            
            if(item) {
                item.rate_now = data.r;
            }
        });
        connection.on('oisb', (data) => {
            const item = this.items.find(outcomeItem => outcomeItem.outcome_id === data.o);
            if(item) {
                item.is_banned = !!data.is;
            }
        });
        connection.on('oise', (data) => {
            const item = this.items.find(outcomeItem => outcomeItem.outcome_id === data.o);
            if(item) {
                item.is_enabled = !!data.is;
            }
        });
        connection.on('oish', (data) => {
            const item = this.items.find(outcomeItem => outcomeItem.outcome_id === data.o);
            if(item) {
                item.is_hidden = !!data.is;
            }
        });
        connection.on('oisf', (data) => {
            const item = this.items.find(outcomeItem => outcomeItem.outcome_id === data.o);
            if(item) {
                item.is_finished = !!data.is;
            }
        });
    },
    methods: {
        processErrors: function({success = false, errors = {}, message = ''}) {
            this.messageError = !success ? (message.length > 0 ? message : Object.values(errors).map((fieldError) => fieldError.join(', ')).join(', ')) : '';
            return success;
        },
        add: function (id) {
            if(!this.items.find(item => item.outcome_id === id)) {
                fetch(`/games/carts/item-add?outcome=${id}`).then((result) => result.json()).then((result) => {
                    this.processErrors(result);
                    this.minimal = result.minimal;
                    this.maximal = result.maximal;
                    this.bet = result.amount;
                    this.on_change = result.on_change;
                    const prev = this.items.findIndex(item => item.game_id === result.item.game_id);
                    if(prev !== -1) {
                        this.items.splice(prev, 1, result.item);
                    } else {
                        subscribe(result.item.game_id);
                        this.items.push(result.item);
                    }
                });
            }
        },
        confirm: function (id) {
            fetch(`/games/carts/item-confirm?id=${id}`).then((result) => result.json()).then((result) => {
                if(!this.processErrors(result)) {
                    return false;
                }
                this.items.find(item => item.id === id).rate = result.rate;
            }).catch((err) => {
                console.error(err);
            });
        },
        remove: function (id) {
            fetch(`/games/carts/item-remove?id=${id}`).then((result) => result.json()).then((result) => {
                if(!this.processErrors(result)) {
                    return false;
                }
                const idx = this.items.findIndex(item => item.id === id);
                if(idx !== -1) {
                    const old = this.items.splice(idx, 1)[0];
                    unsubscribe(old.game_id);
                }
            }).catch((err) => {
                console.error(err);
                if(!!err.message) {
                    this.messageError = err.message;
                }
            });
        },
        clean: function () {
            fetch(`/games/carts/delete`).then((result) => result.json()).then((result) => {
                if(!this.processErrors(result)) {
                    return false;
                }
                this.items = [];
            }).catch((err) => {
                console.error(err);
                if(!!err.message) {
                    this.messageError = err.message;
                }
            });
        },
        load: function() {
            fetch(`/games/carts/view`).then((result) => result.json()).then((result) => {
                this.processErrors(result);
                (result.items || []).forEach(newItem => subscribe(newItem.game_id));
                this.items.forEach(oldItem => unsubscribe(oldItem.game_id));
                this.items = result.items || [];
                this.bet = result.bet || 0;
                this.on_change = result.on_change || 0;
                this.minimal = result.minimal || 0;
                this.maximal = result.maximal || Number.MAX_SAFE_INTEGER;
            }).catch((err) => {
                console.error(err);
                if(!!err.message) {
                    this.messageError = err.message;
                }
            });
        },
        edit: function(amount, change) {
            fetch(`/games/carts/edit?amount=${amount || 0}&on_change=${+change}`).then((result) => result.json()).then((result) => {
                this.processErrors(result);
                this.bet = result.bet || +amount;
                this.on_change = result.on_change || +change;
            }).catch((err) => {
                console.error(err);
                if(!!err.message) {
                    this.messageError = err.message;
                }
            });
        },
        place: function(clean, promocode, type) {
            fetch(`/games/carts/place-bet?amount=${+this.bet}&clean=${+clean}&promocode=${promocode}&type=${type}`).then((result) => result.json()).then((result) => {
                if(!this.processErrors(result)) {
                    return false;
                }
                this.load();
                reloadUserPurse();
            }).catch((err) => {
                if(!!err.message) {
                    this.messageError = err.message;
                }
            });
        }
    }
});

export const cart = CartInstance;
export const cartOnChange = {
    ON_CHANGE_AGREE: 0,
    ON_CHANGE_CONFIRM: 1,
    ON_CHANGE_ACCEPT_INCREASE: 2,
};
export const cartAddMixin = {
    methods: {
        addToCart: (id) => {
            CartInstance.add(id);
            // $('body').addClass('show_coef_popup'); 
            // setTimeout(() => {
            //     $('body').removeClass('show_coef_popup'); 
            // }, 3000); 

            // const btn = document.getElementsByClassName("scroll_coef");
            // const el = document.getElementsByClassName("side-bar-right");
            // btn[0].addEventListener('click', () => {
            //     el[0].scrollIntoView({block: "start", behavior: "smooth"});
            // })
            // $('.scroll_coef').click(function(){
            //     $('body').removeClass('show_coef_popup');
            //     $('html, body').animate({
            //         scrollTop: $(".side-bar-right").offset().top
            //     }, 2000);
            // });     
    },
    }
};
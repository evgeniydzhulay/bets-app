import Vue from 'vue';
Vue.config.devtools = true;

function urlEncode(params) {
    if(!params) {
        return '';
    }
    return Object.keys(params).map(function(k) {
        if(Array.isArray(params[k])) {
            return params[k].map(function(value) {
                return encodeURIComponent(k) + '=' + encodeURIComponent(value);
            }).join('&');
        } else {
            return encodeURIComponent(k) + '=' + encodeURIComponent(params[k]);
        }
    }).join('&');
}

const GamesListClass = Vue.extend({
    data: function () {
        return {
            isLive: null,
            sports: [],
        };
    },
    created: function() {
        setInterval(() => {
            this.loadSports();
        }, 60000);
    },
    watch: {
        isLive(){
            this.loadSports();
        }
    },
    methods: {
        loadGames: function (params) {
            params.live = +(params.hasOwnProperty('live') ? params.live : this.isLive);
            const query = urlEncode(params);
            return fetch(`/games/events/list?${query}`).then((result) => result.json()).then((data) => {
                if(!data.success) {
                    throw new Error(data.message);
                }
                return data.events;
            });
        },
        loadSports: function() {
            fetch(`/games/games/sports?live=${+this.isLive}`).then((result) => result.json()).then((data) => {
                if(!data.success) {
                    throw new Error(data.message);
                }
                this.sports = data.sports.sort(function( a, b ) {
                    return a.sort_order - b.sort_order;
                }).map(sport => {
                    sport.tournaments = sport.tournaments.sort(function( a, b ) {
                        return a.sort_order - b.sort_order;
                    });
                    return sport;
                });
            }).catch((err) => {
                console.error(err);
            });
        },
        loadGame: function(id) {
            return fetch(`/games/events/event?id=${id}`).then((result) => result.json()).then((data) => {                 
                if(!data.success) {
                    throw new Error(data.message);
                }
                return data;
            });
        },
        loadUpcoming: function(from, to) {
            return fetch(`/games/events/upcoming?from=${from || 0}&to=${to || 24}`).then((result) => result.json()).then((data) => {
                if(!data.success) {
                    throw new Error(data.message);
                }
                return data.events;
            });
        },
    }
});

const instance = new GamesListClass();

export const games = instance;

export const gamesMixin = {
    data: function() {
        return {
            upcoming: [],
            favourites: [],
        };
    },
    computed: {
        isLive: () => !!instance.isLive,
        sports: () => instance.sports,       
    }
};
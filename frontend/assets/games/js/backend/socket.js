import io from 'socket.io-client';

const socket = io.connect(socketGameParams.host + (socketGameParams.port ? (':' + socketGameParams.port) : ''), {
    path: socketGameParams.path,
    timeout: 5000,
    reconnectionDelay: 1000,
    reconnectionDelayMax: 1500,
    randomizationFactor: 0,
});
const subscriptions = [];
socket.on('connect', () => {
    subscriptions.filter((value, index) => {
        return subscriptions.indexOf(value) === index;
    }).forEach(event => socket.emit('sub', {g: event}));
});
export const connection = socket;
export function subscribe(event) {
    if(!subscriptions.includes(event)) {
        socket.emit('sub', {g: event});
    }
    socket.emit('reload', {g: event});
    subscriptions.push(event);
}
export function unsubscribe(event) {
    const idx = subscriptions.indexOf(event);
    if(idx !== -1) {
        subscriptions.splice(idx, 1)
    }
    if(!subscriptions.includes(event)) {
        socket.emit('unsub', {g: event});
    }
}
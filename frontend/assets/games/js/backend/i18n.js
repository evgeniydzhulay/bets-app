import VueI18n from 'vue-i18n';
import Vue from 'vue';

Vue.use(VueI18n);
const i18n = new VueI18n({
    locale: window.$locale,
    fallbackLocale: 'en',
    formatFallbackMessages: true,
    messages: window.$messages,
});
export default i18n;
import Vue from 'vue';
import VueRouter from 'vue-router';

import {games} from "./games";
import list from 'components/game/list.vue';
import game from 'components/game/game.vue';
import containerRouter from 'components/container-router.vue';

Vue.use(VueRouter);
const router = new VueRouter({
    mode: 'history',
    routes: [
        { path: '/', component: list, name: 'live/home', beforeEnter: (to, from, next) => {
            games.isLive = 1;
            next();
        }},
        { path: '/live', name: 'live', component: containerRouter, beforeEnter: (to, from, next) => {
            games.isLive = 1;
            next();
        }, children: [
            { path: '/live', component: list, name: 'live/home' },
            { path: '/live/sports/:sports_id', component: list, name: 'live/sport' },
            { path: '/live/tournaments/:tournament_id', component: list, name: 'live/tournament' },
        ]},
        { path: '/line', name: 'line', component: containerRouter, beforeEnter: (to, from, next) => {
            games.isLive = 0;
            next();
        }, children: [
            { path: '/line', component: list, name: 'line/home' },
            { path: '/line/sports/:sports_id', component: list, name: 'line/sport' },
            { path: '/line/tournaments/:tournament_id', component: list, name: 'line/tournament' },
        ]},
        { path: '/game/:id', component: game, name: 'game' , beforeEnter: (to, from, next) => {
            if(games.isLive === undefined || games.isLive === null) {
                games.isLive = 1;
            }
            next();
        }},
    ],
});
export default router;
import Vue from 'vue';

import i18n from 'backend/i18n';
import router from "./backend/router";
import display from 'components/display-desktop';

(function () {
    const instance = new Vue({
        el: '#games-console',
        i18n: i18n,
        router: router,
        render: function(createElement) {
            return createElement(display);
        },
    });
    router.beforeEach((to, from, next) => {
        jQuery(instance.$el).addClass('loading');
        next();
    });
    router.afterEach(() => {
        jQuery(instance.$el).removeClass('loading');
    });
})();
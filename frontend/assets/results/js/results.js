import Vue from 'vue'
import app from './results.vue'

Vue.config.devtools = true;

new Vue({
    el: '#games-results',
    render: h => h(app)
});
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const CssExtractPlugin = require('mini-css-extract-plugin');
const OptimizeCssAssetsPlugin = require('optimize-css-assets-webpack-plugin');

module.exports =  {
    mode: 'production',
    name: 'frontend-prod',
    entry: {
        bootstrap: [
            path.resolve(__dirname, 'assets/bootstrap/less/bootstrap.less'),
        ],
        main: [
            path.resolve(__dirname, 'assets/main/js/main.js'),
            path.resolve(__dirname, 'assets/main/scss/main.sass'),
        ],
        'games.desktop': [
            path.resolve(__dirname, 'assets/games/js/games-desktop.js'),
            path.resolve(__dirname, 'assets/games/scss/games.sass'),
        ],
        'games.mobile': [
            path.resolve(__dirname, 'assets/games/js/games-mobile.js'),
            path.resolve(__dirname, 'assets/games/scss/games.sass'),
        ],
        'results.desktop': [
            path.resolve(__dirname, 'assets/results/js/results.js'),
            path.resolve(__dirname, 'assets/results/scss/results.sass'),
        ],
        'results.mobile': [
            path.resolve(__dirname, 'assets/results/js/resultsMobile.js'),
            path.resolve(__dirname, 'assets/results/scss/resultsMobile.sass'),
        ],
        support: [
            path.resolve(path.dirname(__dirname), 'common/assets/support/js/support.js'),
            path.resolve(path.dirname(__dirname), 'common/assets/support/scss/support.scss'),
        ],
    },
    output: {
        pathinfo: false,
        path: path.resolve(__dirname, 'web/static'),
        filename: 'js/[name].min.js',
        chunkFilename: 'js/[name].min.js',
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                use: [
                    "cache-loader",
                    "thread-loader",
                    "vue-loader",
                ],
            },
            {
                test: /\.(js)$/,
                exclude: /(node_modules)/,
                use: [
                    "cache-loader",
                    "thread-loader",
                    {
                        loader: 'babel-loader',
                        query: {
                            presets: ['@babel/env'],
                            cacheDirectory: true,
                        }
                    }
                ],
            },
            {
                test: /\.scss$/,
                use: [
                    "cache-loader",
                    CssExtractPlugin.loader,
                    'css-loader?url=false',
                    'sass-loader',
                ],
            },
            {
                test: /\.sass$/,
                use: [
                    "cache-loader",
                    CssExtractPlugin.loader,
                    'css-loader?url=false',
                    'sass-loader',
                ],
            },
            {
                test: /\.less$/,
                use: [
                    "cache-loader",
                    CssExtractPlugin.loader,
                    'css-loader',
                    {
                        loader: 'less-loader',
                        options: {
                            minimize: true
                        }
                    }
                ],
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[name].[ext]',
                            outputPath: 'fonts/',
                            publicPath: '/static/fonts/'
                        }
                    }
                ]
            },
        ]
    },
    resolve: {
        modules: [
            path.resolve(__dirname, 'assets/games/js/'),
            path.resolve(__dirname, 'assets/results/js/'),
            path.resolve(path.dirname(__dirname), 'common/assets/support'),
            'node_modules'
        ],
        extensions: ['.vue', '.js'],
    },
    externals: {
        socketParams: 'socketParams',
        jquery: 'jQuery',
        'socket.io-client': 'io'
    },
    plugins: [
        new VueLoaderPlugin(),
        new CssExtractPlugin({
            filename: 'css/[name].min.css',
            chunkFilename: 'css/[id].min.css'
        }),
        new OptimizeCssAssetsPlugin({
            assetNameRegExp: /\.min\.css$/g,
            cssProcessor: require('cssnano'),
            cssProcessorPluginOptions: {
                preset: ['default', { discardComments: { removeAll: true } }],
            },
            canPrint: true
        })
    ],
    performance: {
        hints: false,
        maxAssetSize: 512000
    },
    optimization: {
        usedExports: true,
        namedChunks: true,
        namedModules: true,
        mergeDuplicateChunks: false,
        splitChunks: {
            name: false,
            cacheGroups: {
                vue: {
                    test: /[\\/]node_modules[\\/]vue.*[\\/]/,
                    name: 'vue',
                    chunks: 'all',
                    enforce: true,
                    reuseExistingChunk: false,
                },
            }
        }
    },
};
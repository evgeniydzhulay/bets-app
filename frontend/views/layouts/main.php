<?php

use frontend\assets\MainAsset;
use kartik\alert\AlertBlock;
use kartik\growl\Growl;
use kartik\icons\Icon;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $content string */
MainAsset::register($this);
$mainClass = str_replace('/', '-', Yii::$app->controller->route);
$bodyClass = isset($this->params['bodyClass']) ? ' ' . $this->params['bodyClass'] : '';
$this->beginPage();

$mobileMenu = [
    Yii::t('user', 'Games') => ['src' => '/static/img/nav-games/winner-icon.svg', 'link' => '#'],
    Yii::t('user', 'SPORTS') => ['src' => '/static/img/nav-games/sports-icon.png', 'link' => '/line'],
    Yii::t('user', 'Live bets') => ['src' => '/static/img/nav-games/live.png', 'link' => '/live'],
    Yii::t('user', 'eSports') => ['src' => '/static/img/nav-games/console.png', 'link' => '/live/sports/228'],
    Yii::t('user', 'Fast games') => ['src' => '/static/img/nav-games/dice.png', 'link' => '/games/mini-games/list'],
    Yii::t('user', 'Bonuses') => ['src' => '/static/img/nav-games/gift.png', 'link' => '#'],
    Yii::t('user', 'Gambling') => ['src' => '/static/img/nav-games/Gambing.png', 'link' => '#'],
    Yii::t('user', 'Results') => ['src' => '/static/img/nav-games/podium.png', 'link' => '/games/games/results']
];

$this->registerJs('isGuest = ' . ( Yii::$app->user->isGuest ? '!0;' : '!1;'), View::POS_HEAD);
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no"> -->
        <!-- <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no"> -->
        <title><?= (!empty($this->title) ? Html::encode("{$this->title} | ") : '') . Yii::$app->name; ?></title>
        <script>
            if(document.cookie.match('(?:^|;\\s*)tzo=([^;]*)') === null) {
                window.location.href = '/core/timezone?timezone=' + Intl.DateTimeFormat().resolvedOptions().timeZone;
            }
            window.timeDiff = (new Date().getTimezoneOffset() * 60 + <?= date('Z') ?>) * 1000;
        </script>
        <?php $this->head() ?>
        <?= Html::csrfMetaTags() ?>
        <link rel="icon" href="/static/favicon/favicon.ico" type="image/x-icon">
    </head>
    <body class="desktop layout-top-nav skin-black-light <?= $mainClass ?><?= $bodyClass ?><?= Yii::$app->user->getIsGuest() ? ' unlogin' : ''; ?>">
        <?php
        $this->beginBody();
        echo AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true,
            'delay' => 500,
            'id' => 'alerts',
            'alertSettings' => [
                'success' => [
                    'type' => Growl::TYPE_SUCCESS,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
                'danger' => [
                    'type' => Growl::TYPE_DANGER,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
                'warning' => [
                    'type' => Growl::TYPE_WARNING,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
                'info' => [
                    'type' => Growl::TYPE_INFO,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
            ]
        ]);
        ?>
        <div class="wrapper">
            <div class="navigation-menu" id="mobileSideMenu">
                <button class="mobile_menu_close"><i class="fas fa-long-arrow-alt-left"></i></button>
                <div class="mobile-side-header">
                    <div class="mobile_logo">
                    </div>
                    <?php if(!Yii::$app->user->isGuest) { ?>
                        <div class="user_info_wrap">
                            <div class="mobile-user-icon"></div>
                            <div class="mobile-user-info mb_info">
                                <div class="user-info-id">
                                    <span><?= Yii::t('user', 'User name') ?> (ID: <?= Yii::$app->user->id; ?>)</span>
                                </div> 
                                <div class="user-balnce">
                                    <span><?= Yii::$app->user->identity->purse_amount ?> &nbsp;<?=Yii::$app->user->identity->purse_currency; ?></span>
                                </div>  
                            </div>
                            <div class="ui-lang" style="position: relative">
                                <a class="lang-flag flag-icon flag-icon-<?= Yii::$app->params['languages'][Yii::$app->language]['flag']; ?>" data-toggle="dropdown"> 
                                </a>
                                <ul class="dropdown-menu">
                                    <?php
                                    foreach (Yii::$app->params['languages'] AS $key => $item) {
                                        if($key == Yii::$app->language) {
                                            continue;
                                        }
                                        echo '<li><a href="' . Url::toRoute(['/core/language', 'language' => $key]) . '">' . Icon::show($item['flag'], []) . ' ' . $item['label'] . '</a></li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    <?php } else { ?>
                        <div class="login_wrap">
                            <i class="fas fa-sign-in-alt"></i>
                            <a href="<?= Url::toRoute(['/user/auth/login']); ?>" class="log-in" data-toggle="modal" data-target="#loginPopup">
                               <?= Yii::t('user', 'Log In'); ?>
                            </a>
                            <a href="<?= Url::toRoute(['/user/auth/register']); ?>" class="registration">
                                / <?= Yii::t('user', 'Registration'); ?>
                            </a>
                            <div class="ui-lang" style="position: relative">
                                <a class="lang-flag flag-icon flag-icon-<?= Yii::$app->params['languages'][Yii::$app->language]['flag']; ?>" data-toggle="dropdown"> 
                                </a>
                                <ul class="dropdown-menu">
                                    <?php
                                    foreach (Yii::$app->params['languages'] AS $key => $item) {
                                        if($key == Yii::$app->language) {
                                            continue;
                                        }
                                        echo '<li><a href="' . Url::toRoute(['/core/language', 'language' => $key]) . '">' . Icon::show($item['flag'], []) . ' ' . $item['label'] . '</a></li>';
                                    }
                                    ?>
                                </ul>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <div class="menu-mobile-side-container">
                    <?php if(!Yii::$app->user->isGuest) { ?>
                        <a href="<?= Url::toRoute(['/user/auth/account']); ?>" class="menu-mobile-side-element">
                            <div class="menu-molie-element-logo" style="background-image: url('/static/img/account-menu/avatar.png')"></div>
                            <div class="menu-mobile-content">
                                <span>
                                <?= Yii::t('user', 'Profile') ?>
                                </span>
                            </div>
                        </a>
                        <a href="<?= Url::toRoute(['/games/bets/index']); ?>" class="menu-mobile-side-element">
                            <div class="menu-molie-element-logo" style="background-image: url('/static/img/account-menu/event.png')"></div>
                            <div class="menu-mobile-content">
                                <span>
                                    <?= Yii::t('games', 'Bets history') ?>
                                </span>
                            </div>
                        </a>
                        <a href="<?= Url::toRoute(['/user/purse/index']); ?>" class="menu-mobile-side-element">
                            <div class="menu-molie-element-logo" style="background-image: url('/static/img/account-menu/clock.png')"></div>
                            <div class="menu-mobile-content">
                                <span>
                                    <?= Yii::t('user', 'Transaction history') ?>
                                </span>
                            </div>
                        </a>
                    <?php } ?>
                    <?php foreach ($mobileMenu as $key => $value) { ?>
                        <a href="<?= $value['link'] ?>" class="menu-mobile-side-element">
                            <div class="menu-molie-element-logo" style="background-image: url('<?= $value['src'] ?>')"></div>
                            <div class="menu-mobile-content">
                                <span>
                                    <?= $key ?>
                                </span>
                            </div>
                        </a>
                    <?php } ?>
                    <?php if(!Yii::$app->user->isGuest) { ?>
                        <a href="<?= Url::toRoute(['/user/auth/logout']); ?>" class="menu-mobile-side-element">
                            <div class="menu-molie-element-logo" style="background-image: url('/static/img/account-menu/logout.png')"></div>
                            <div class="menu-mobile-content">
                                <span>
                                    <?= Yii::t('user', 'Logout') ?>
                                </span>
                            </div>
                        </a>
                    <?php } ?>

                </div>
            </div>
            <div class="navigation">
                <div class="nav-bar">
                    <a href="<?= \yii\helpers\Url::toRoute(['/games/games/index']); ?>" class="logo"></a>
                    <!-- <div class="buttons-left-nav">
                        <a class="ui-nav-button keyword"><?= Yii::t('user', 'Website access'); ?></a>
                        <a href="<?= Url::toRoute(['/user/purse/index']); ?>" class="ui-nav-button credit-card">
				            <?= Yii::t('purse', 'Payments'); ?>
                        </a>
                        <a class="ui-bonus-button bonus-button-icon">
                            <div class="text"><?= Yii::t('user', 'Bonus'); ?></div>
                            <div class="bonus">
                                <span class="quality">3000&nbsp;</span>
                                <span>UAH</span>
                            </div>
                        </a>
                    </div> -->
                    <div class="header_left">
                        <div class="nav-games">
                            <div class="games">
                                <!-- <div class="ui-games">
                                    <a href="#">
                                        <div class="games-icon winner"></div>
                                        <div class="game-previewText">
                                            <div class="text-preview"><?= Yii::t('user', 'Weekly tournament'); ?></div>
                                            <div class="text">«<?= Yii::t('user', 'Games'); ?>»</div>
                                        </div>
                                    </a>
                                </div> -->
                                <div class="ui-games">
                                    <a href="/line">
                                        <div class="games-icon sports"></div>
                                        <div class="text"><?= Yii::t('user', 'SPORTS'); ?></div>
                                    </a>
                                </div>
                                <div class="ui-games">
                                    <a href="/live">
                                        <div class="games-icon live"></div>
                                        <div class="text"><?= Yii::t('user', 'Live bets'); ?></div>
                                        <i class="fas fa-sort-down"></i>
                                    </a>
                                </div>
                                <div class="ui-games">
                                    <a href="/live/sports/228">
                                        <div class="games-icon console"></div>
                                        <div class="text"><?= Yii::t('user', 'eSports'); ?></div>
                                    </a>
                                </div>
                                <div class="ui-games">
                                    <a href="<?= Url::toRoute(['/games/mini-games/list']); ?>">
                                        <div class="games-icon dice"></div>
                                        <div class="text"><?= Yii::t('user', 'Fast games'); ?></div>
                                        <i class="fas fa-sort-down"></i>
                                    </a>
                                </div>
                                <div class="ui-games">
                                    <a href="<?= Url::toRoute(['/content/view/bonuses']); ?>">
                                        <div class="games-icon gift"></div>
                                        <div class="text"><?= Yii::t('user', 'Bonuses'); ?></div>
                                        <i class="fas fa-sort-down"></i>
                                    </a>
                                </div>
                                <div class="ui-games">
                                    <a href="#">
                                        <div class="games-icon Gambing"></div>
                                        <div class="text"><?= Yii::t('user', 'Gambling'); ?></div>
                                        <i class="fas fa-sort-down"></i>
                                    </a>
                                </div>
                                <div class="ui-games">
                                    <a href="<?= Url::toRoute(['/games/games/results']); ?>">
                                        <div class="games-icon podium"></div>
                                        <div class="text"><?= Yii::t('user', 'Results'); ?></div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="button-right-nav header-login">
                        <!-- <a href="<?= Url::toRoute(['/support/request/create']); ?>" class="ui-user">
                            <div class="picture"></div>
                        </a> -->
			            <?php if(!Yii::$app->user->isGuest) { ?>
                            <div class="ui-bonus-button" id="nav-user-website-purse">
                                <div class="text"><?= Yii::t('user', 'Main account'); ?></div>
                                <div class="bonus">
                                    <span class="quality" id="nav-user-website-purse_amount"><?= Yii::$app->user->identity->purse_amount ?></span>
                                    <span>&nbsp;<?=Yii::$app->user->identity->purse_currency; ?></span>
                                </div>
                            </div>
                            <div class="ui-account-information-drop-menu">
                                <span><?= Yii::t('user', 'My account') ?></span>
                                <i id="rotateArrow" class="fas fa-sort-down" style="transform: rotate(0deg) translateY(0%);"></i>
                                <div class="account-content">
                                    <div class="account-content-user-id"><span><?= Yii::t('user', 'User name') ?> (ID: <?= Yii::$app->user->id; ?>)</span></div>
                                    <div class="account-content-menu-select account-icon-avatar">
                                        <a href="<?= Url::toRoute(['/user/auth/account']); ?>"><?= Yii::t('user', 'Profile') ?></a>
                                    </div>
                                    <div class="account-content-menu-select account-icon-event">
                                        <a href="<?= Url::toRoute(['/games/bets/index']); ?>"><?= Yii::t('games', 'Bets history') ?></a>
                                    </div>
                                    <div class="account-content-menu-select account-icon-clock">
                                        <a href="<?= Url::toRoute(['/user/purse/index']); ?>"><?= Yii::t('user', 'Transaction history') ?></a>
                                    </div>
                                    <div class="account-content-menu-select account-icon-wallet">
                                        <a href="<?= Url::toRoute(['/user/purse/withdraw']); ?>"><?= Yii::t('user', 'Withdraw funds') ?></a>
                                    </div>
                                    <div class="account-content-menu-select account-icon-wallet">
                                        <a href="<?= Url::toRoute(['/user/purse/deposit']); ?>"><?= Yii::t('user', 'Deposit') ?></a>
                                    </div>
                                    <div class="account-content-menu-select account-icon-logout">
                                        <a href="<?= Url::toRoute(['/user/auth/logout']); ?>"><?= Yii::t('user', 'Logout') ?></a>
                                    </div>
                                </div>
                            </div>
                            <!-- <a class="ui-header-mail">
                                <div class="count-message">
                                    <span><?= Yii::$app->user->identity->getNotifications()->count(); ?></span>
                                </div>
                            </a> -->
                            <!-- <div class="header-button-place">
                                <div class="header-button-container">
                                    <a href="<?= Url::toRoute(['/user/purse/deposit']); ?>" class="ui-radius-button">
                                        <div class="background-header-button">
	                                        <?= Yii::t('user', 'Top-up account'); ?>
                                        </div>
                                    </a>
                                </div>
                            </div> -->
			            <?php } else { ?>
                            <a href="<?= Url::toRoute(['/user/auth/login']); ?>" class="log-in" data-toggle="modal" data-target="#loginPopup">
                                <?= Yii::t('user', 'Log In'); ?>
                            </a>
                            <a href="<?= Url::toRoute(['/user/auth/register']); ?>" class="registration">
                                <?= Yii::t('user', 'Registration'); ?>
                            </a>
			            <?php } ?>
                        <div class="ui-lang" style="position: relative">
                            <a class="lang-flag flag-icon flag-icon-<?= Yii::$app->params['languages'][Yii::$app->language]['flag']; ?>" data-toggle="dropdown">
                                <!-- <i class="fas fa-chevron-down"></i> -->
                            </a>
                            <ul class="dropdown-menu">
					            <?php
					            foreach (Yii::$app->params['languages'] AS $key => $item) {
						            if($key == Yii::$app->language) {
							            continue;
						            }
						            echo '<li><a href="' . Url::toRoute(['/core/language', 'language' => $key]) . '">' . Icon::show($item['flag'], []) . ' ' . $item['label'] . '</a></li>';
					            }
					            ?>
                            </ul>
                        </div>
                    </div>
                </div>
                <!-- <div class="nav-games">
                    <div class="games">
                        <div class="ui-games">
                            <a href="#">
                                <div class="games-icon winner"></div>
                                <div class="game-previewText">
                                    <div class="text-preview"><?= Yii::t('user', 'Weekly tournament'); ?></div>
                                    <div class="text">«<?= Yii::t('user', 'Games'); ?>»</div>
                                </div>
                            </a>
                        </div>
                        <div class="ui-games">
                            <a href="/line">
                                <div class="games-icon sports"></div>
                                <div class="text"><?= Yii::t('user', 'SPORTS'); ?></div>
                            </a>
                        </div>
                        <div class="ui-games">
                            <a href="/live">
                                <div class="games-icon live"></div>
                                <div class="text"><?= Yii::t('user', 'Live bets'); ?></div>
                                <i class="fas fa-sort-down"></i>
                            </a>
                        </div>
                        <div class="ui-games">
                            <a href="/live/sports/228">
                                <div class="games-icon console"></div>
                                <div class="text"><?= Yii::t('user', 'eSports'); ?></div>
                            </a>
                        </div>
                        <div class="ui-games">
                            <a href="#">
                                <div class="games-icon dice"></div>
                                <div class="text"><?= Yii::t('user', 'Fast games'); ?></div>
                                <i class="fas fa-sort-down"></i>
                            </a>
                        </div>
                        <div class="ui-games">
                            <a href="#">
                                <div class="games-icon gift"></div>
                                <div class="text"><?= Yii::t('user', 'Bonuses'); ?></div>
                                <i class="fas fa-sort-down"></i>
                            </a>
                        </div>
                        <div class="ui-games">
                            <a href="#">
                                <div class="games-icon Gambing"></div>
                                <div class="text"><?= Yii::t('user', 'Gambling'); ?></div>
                                <i class="fas fa-sort-down"></i>
                            </a>
                        </div>
                        <div class="ui-games">
                            <a href="<?= Url::toRoute(['/games/games/results']); ?>">
                                <div class="games-icon podium"></div>
                                <div class="text"><?= Yii::t('user', 'Results'); ?></div>
                            </a>
                        </div>
                    </div>
                </div> -->
            </div>
            <!-- <div class="navigation-mobile"> 
                <div class="mob_logo_wrap">
                    <a href="<?= \yii\helpers\Url::toRoute(['/games/games/index']); ?>" class="logo"></a>
                    <div class="navigation-search-mobile"> 
                        <input type="text">
                    </div>
                </div>
                <div class="navigation-search-button-mobile" id="mobileSearchButton"><i class="fas fa-search"></i></div>
                <?php if(!Yii::$app->user->isGuest) { ?>
                    <a href="<?= Url::toRoute(['/user/auth/account']); ?>" class="mobile-user-info">
                        <div class="user-info-id">
                            <div class="acc_icon"></div>
                            <span> (ID: <?= Yii::$app->user->id; ?>)</span>
                        </div> 
                    </a>
                <?php } else { ?>  
                    <a href="<?= Url::toRoute(['/user/auth/login']); ?>" class="log-in-mobile" data-toggle="modal" data-target="#loginPopup">
                        <i class="fas fa-sign-in-alt"></i>
                    </a>
                <?php } ?>
                <div class="navigation-menu-mobile" id="mobileMenuButton"></div> 
            </div> -->
            <div class="content-wrapper">
                <?= $content ?>
            </div>
            <footer class="footer-navigation">
                <div class="footer-navigation-count">
                    <div class="footer-navigation-context">
                        <a href="<?= Url::toRoute(['/support/request/create']); ?>" class="ui-user">
                            <div class="picture"></div>
                        </a>
                        <div class="footer-link-content">
                            <div class="footer-link-block">
                                <a href="<?= Url::toRoute(['/content/view/about-us']); ?>"><?= Yii::t('user', 'About us'); ?></a>
                                <a href="<?= Url::toRoute(['/content/view/contacts']); ?>"><?= Yii::t('user', 'Contacts'); ?></a>
                                <!-- <a href="<?= Url::toRoute(['/blog/front/index']); ?>"><?= Yii::t('user', 'Blog'); ?></a> -->
                                <a href="#"><?= Yii::t('user', 'Blog'); ?></a>
                            </div>
                            <div class="footer-link-block">
                                <a href="<?= Url::toRoute(['/content/view/rules', 'key' => 1]); ?>"><?= Yii::t('user', 'Terms and conditions'); ?></a>
                                <a href="<?= Yii::getAlias('@web/partners'); ?>"><?= Yii::t('user', 'Affiliate program'); ?></a>
                            </div>
                            <div class="footer-link-block">
                                <!-- <a href="<?= Url::toRoute(['/games/results/statistics']); ?>"><?= Yii::t('user', 'Statistics'); ?></a> -->
                                <a href="#"><?= Yii::t('user', 'Statistics'); ?></a>
                                <a href="<?= Url::toRoute(['/games/games/results', 'live' => 1]); ?>"><?= Yii::t('user', 'Results live'); ?></a>
                                <a href="<?= Url::toRoute(['/games/games/results']); ?>"><?= Yii::t('user', 'Results'); ?></a>
                            </div>
                        </div>
                        <div class="footer-social">
                            <div class="footer-social-text">
                                <span><?= Yii::t('user', 'you’ll like us:'); ?></span>
                            </div>
                            <div class="footer-social-icons">
                                <a href="https://vk.com/luxebetinc" class="ui-social" target="_blank"><i class="fab fa-vk"></i></a>
                                <!-- <div class="ui-social"><i class="fab fa-facebook-f"></i></div> -->
                                <a href="https://www.instagram.com/luxebetinc/" class="ui-social" target="_blank"><i class="fab fa-instagram"></i></a>
                            </div>
                        </div>
                    </div>
                    <div class="footer-for-mobile-app">
                        <div class="footer-for-mobile-icon"></div>
                        <div class="footer-for-mobile-text-count">
                            <span class="footer-for-mobile-text-opacity"><?= Yii::t('user', 'Luxe.bet uses cookies to enhance your website experience.'); ?> </span>
                            <br>
                            <span class="footer-for-mobile-text-opacity"><?= Yii::t('user', 'By staying on the website, you agree to the use of these cookies.'); ?></span>
                            <div class="footer-for-mobile-text-white"> <?= Yii::t('user', 'Find out more'); ?>&nbsp;&nbsp;<i class="fas fa-long-arrow-alt-right"></i></div>
                        </div>
                        <div class="footer-for-mobile-button">
                            <div class="ui-event-button">
                                <span><i class="fas fa-mobile-alt"></i> <?= Yii::t('user', 'MOBILE APP'); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="footer-copyright">
                    <span>
                        <?= Yii::t('app', 'Copyright &copy; {year} "{name}". All rights reserved and protected by law.', [
                            'year' =>  date('Y'),
                            'name' => Yii::$app->name
                        ]); ?>
                    </span>
                </div>
            </footer>
        </div>

        <div id="loginPopup" class="fade modal" role="dialog" tabindex="-1">
            <div class="modal-dialog modal-md">
                <div class="modal-content login-form-popup">
                </div>
            </div>
        </div>

        <div class="modal fade" tabindex="-1" role="dialog" id="proccesed">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p style="font-size: 14px;"><?= Yii::t('user', 'In developing'); ?></p>
                </div>
                <div class="modal-footer">                    
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
                </div>
            </div>
        </div>

        <div class="coef_popup">
            <p>Ваша ставка принята</p>
            <div class="scroll_coef">Просмотреть ставки &#8595;</div>
        </div>
        <div class="overlay_modal"></div>
        <script>
            const mobileMenuButton = document.getElementById('mobileMenuButton');
            const mobileCloseButton = document.getElementsByClassName('mobile_menu_close');
            const mobileSideMenu = document.getElementById('mobileSideMenu');
            const mobileSearchButton = document.getElementById('mobileSearchButton');
            const mobileSearchWrap = document.getElementsByClassName('mob_logo_wrap');
            mobileSearchButton.addEventListener('click', function(){ 
                if (mobileSearchWrap[0].className.includes('search_show')) {
                    mobileSearchWrap[0].classList.remove("search_show");
                } else {
                    mobileSearchWrap[0].classList.add("search_show");
                }           
            });
            mobileMenuButton.addEventListener('click', function(){
                if(mobileSideMenu.style.right === '0%') {
                    mobileSideMenu.style.right = '-100%';
                } else {
                    mobileSideMenu.style.right = '0%';
                }
            });
            
            mobileCloseButton[0].addEventListener('click', function(){                
                mobileSideMenu.style.right = '-100%';
            });
            window.addEventListener('click', function({target}){
                let contain = target;
                if(contain !== mobileSideMenu){
                    contain = contain.parentNode;
                }
                if(contain !== mobileSideMenu){
                    contain = contain.parentNode;
                }
                if(contain !== mobileSideMenu){
                    contain = contain.parentNode;
                }
                if(contain === mobileSideMenu || target === mobileMenuButton){
                }else{
                    mobileSideMenu.style.right = '-100%';
                }
            });
            window.addEventListener('popstate', function(){
                let linkToErr = window.location.href;
                if(linkToErr.includes('#') && linkToErr.indexOf('#') === linkToErr.length - 1){
                    $('#proccesed').modal('toggle');
                }
            });

            const timeZone = document.getElementsByClassName('time_drop_wrap');
            const container = document.getElementsByClassName('line_tabs_wrap');
            const container2 = document.getElementsByClassName('right_event');      
            document.addEventListener('DOMContentLoaded', function(event) {
                if (window.innerWidth <= 1000 && (window.location.pathname.includes('/line') || window.location.pathname.includes('/live') ||  window.location.pathname.includes('/game') || window.location.pathname === '/games/games/results' || window.location.pathname === '/')) {                    
                    container[0].prepend(timeZone[0]);
                }                
            });
            window.addEventListener('resize', function(){

                if (window.innerWidth <= 1000 && (window.location.pathname.includes('/line') || window.location.pathname.includes('/live') || window.location.pathname.includes('/game') || window.location.pathname === '/games/games/results' || window.location.pathname === '/')) {                    
                    container[0].prepend(timeZone[0]);
                } else if(window.innerWidth > 1000 && (window.location.pathname.includes('/line') || window.location.pathname.includes('/live') || window.location.pathname.includes('/game') || window.location.pathname === '/games/games/results' || window.location.pathname === '/')){
                    container2[0].appendChild(timeZone[0]);
                } 
            });
        </script>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

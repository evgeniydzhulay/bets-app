<?php

use yii\helpers\Html;

/**
 * @var string $message
 */
$this->title = "Error {$code}";
?>
<div class="modal-dialog modal-md error__container">
    <div class="modal-content">
        <div class="modal-header">
            <?= $this->title ?>
        </div>
        <div class="modal-body">
            <div class="error__message">
                <?= nl2br(Html::encode($message)) ?>
            </div>
        </div>
    </div>
</div>
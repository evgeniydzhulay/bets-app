<?php

use console\controllers\Bet365Controller;
use console\controllers\Bet1xController;
use console\controllers\CronController;
use yii\console\controllers\MigrateController;
use yii\helpers\ArrayHelper;

return ArrayHelper::merge([
	'id' => 'console',
	'language' => 'en',
	'components' => [
		'log' => [
			'targets' => [
				'errors' => [
					'logVars' => [],
				],
			],
		],
	],
	'controllerMap' => [
		'cron' => CronController::class,
		'bet365' => Bet365Controller::class,
		'1xbet' => Bet1xController::class,
		'migrate' => [
			'class' => MigrateController::class,
			'migrationNamespaces' => [
				'common\migrations',
			],
		],
	],
],
	require_once(BASE_PATH . '/common/modules/user/config/console.php'),
	require_once(BASE_PATH . '/common/modules/games/config/console.php'),
	require_once(BASE_PATH . '/common/modules/support/config/console.php'),
	require_once(BASE_PATH . '/common/modules/content/config/console.php'),
	require_once(BASE_PATH . '/common/modules/blog/config/console.php'),
	require_once(BASE_PATH . '/common/modules/partners/config/console.php'),
	require_once(BASE_PATH . '/common/modules/casino/config/console.php'),
	(is_file(__DIR__ . '/main-local.php') ? require_once(__DIR__ . '/main-local.php') : [])
);

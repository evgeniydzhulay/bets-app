<?php

namespace console\controllers;

use backapi\modules\games\services\Betsapi1xbetService;
use backapi\modules\games\services\BetsapiService;
use common\modules\games\models\ApiEventGameModel;
use common\modules\games\models\EventGameModel;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\log\FileTarget;
use yii\helpers\Json;
use Yii;
use yii\redis\Connection;
/**
 * Class Bet365Controller
 * @package console\controllers
 *
 * upcoming  :cron  :6h
 * prematch  :cron  :1m
 */
class Bet1xController extends Controller {

	public function init () {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-1xbet-error.log');
		}
		return parent::init();
	}

	protected static function requestAPI($method, $params = []) {
		$params['token'] = \Yii::$app->params['bet365.token'];
		$content = @file_get_contents("https://api.betsapi.com/v1/1xbet/{$method}?" . http_build_query($params));
		if(!$content) {
			$error = error_get_last();
			$error = explode(': ', $error['message']);
			$error = trim($error[2]) . ' ' . trim($error[0]) . PHP_EOL;
			fwrite(STDERR, 'Error: '. $error);
			\Yii::error($error, 'integration/1xbet');
		}
		return $content;
	}

	public function actionUpcoming() {
		echo 'upcoming 1xbet started' . PHP_EOL;
		$service = new Betsapi1xbetService();
		foreach (Betsapi1xbetService::sports() AS $sport => $label) {
			if($data = self::requestAPI('upcoming', ['sport_id' => $sport])) {
				$pager = json_decode($data, true);
				if(json_last_error() !== JSON_ERROR_NONE) {
					continue;
				}
				$service->prematch($data);
				if($pager['pager']['per_page'] >= $pager['pager']['total']) {
					continue;
				}
				for($i = 2; $i <= ceil($pager['pager']['total'] / $pager['pager']['per_page']); $i++) {
					if($data = self::requestAPI('upcoming', ['sport_id' => $sport, 'page' => $i])) {
						$service->prematch($data);
					}
				}
			}
		}
	}

	public function actionPrematch() {
		echo 'prematch 1xbet started' . PHP_EOL;
		$time = time();
		while(true) {
			$service = new Betsapi1xbetService();
			$time++;
			$event_ids = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['code_api'])->andWhere([
				'>', EventGameModel::tableName() . '.starts_at', time()
			])->andWhere([
				ApiEventGameModel::tableName() . '.service_api' => Betsapi1xbetService::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.is_main_provider' => true,
			]) ->joinWith('game', false)->asArray()->all(), 'code_api');
			while(count($slice = array_splice($event_ids, 0, 10)) > 0) {
				if($data = self::requestAPI('event', ['event_id' => implode(',', $slice)])) {
					$service->event($data);
					unset($data);
				}
				sleep(10);
			}
			unset($service);
			gc_collect_cycles();
			if($time > time()) {
				@time_sleep_until($time);
			}
		}
	}

	public function actionFilterInplay() {
		echo 'filter inplay 1xbet started' . PHP_EOL;
		$service = new Betsapi1xbetService();
		$timeStart = time() + 24*3600;
		$gamesLive = ArrayHelper::index(ApiEventGameModel::find()->select( ApiEventGameModel::tableName() .'.*,'.EventGameModel::tableName() .'.is_finished')->andWhere([
				'<', EventGameModel::tableName() . '.starts_at', $timeStart
			])->andWhere([
				ApiEventGameModel::tableName() . '.service_api'=> Betsapi1xbetService::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.is_main_provider' => true,
				//EventGameModel::tableName() . '.is_finished' => false,
				EventGameModel::tableName() . '.is_live'=> 1
			])->joinWith('game', false)->asArray()->all(),'code_api');

		if($data = self::requestAPI('inplay')) {
			$pager = json_decode($data, true);
			if(json_last_error() !== JSON_ERROR_NONE) {
				return false;
			}
			$gamesLive = $service->filterInplay($data,$gamesLive);
			if($pager['pager']['per_page'] >= $pager['pager']['total']) {
				if(is_array($gamesLive) &&count($gamesLive) > 0){
					$gamesLiveIds = ArrayHelper::getColumn($gamesLive, 'game_id');
					EventGameModel::updateAll(
						['is_finished' => true],
						['in', 'id', $gamesLiveIds]
					);
				}
				return false;
			}
			for($i = 2; $i <= ceil($pager['pager']['total'] / $pager['pager']['per_page']); $i++) {
				if($data = self::requestAPI('inplay', ['page' => $i])) {
					$gamesLive = $service->filterInplay($data, $gamesLive);
				}
			}
			if( is_array($gamesLive) && count($gamesLive) > 0){
				$gamesLiveIds = ArrayHelper::getColumn($gamesLive, 'game_id');
				EventGameModel::updateAll(
					['is_finished' => true],
					['in', 'id', $gamesLiveIds]
				);
			}
		}

		$event_ids = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['code_api'])->andWhere([
				ApiEventGameModel::tableName() . '.service_api' => Betsapi1xbetService::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.is_main_provider' => true,
				ApiEventGameModel::tableName() . '.is_checked' => false,
				//ApiEventGameModel::tableName() . '.api_event_id' => 0,
				EventGameModel::tableName() . '.is_finished' => 0,
				EventGameModel::tableName() . '.is_live' => 1,
			])->joinWith('game', false)->asArray()->all(), 'code_api');

		while(count($slice = array_splice($event_ids, 0, 10)) > 0) {
			exec('php /var/www/website/yii.php 1xbet/inplay-start-event '.Json::encode($slice).' >> /var/www/website/runtime/logs/api-1xbet-inplay-start-event.log 2>&1');
		}
	}

	public function actionInplay() {
		echo 'inplay 1xbet started' . PHP_EOL;
		$time = time();
		$eventsTime = [];
		while(true) {
			$service = new Betsapi1xbetService();
			$time++;
			/*
			$event_ids = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['code_api'])->andWhere([
				ApiEventGameModel::tableName() . '.service_api' => Betsapi1xbetService::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.is_main_provider' => true,
				'<', EventGameModel::tableName() . '.starts_at', time(),
				EventGameModel::tableName() . '.is_finished' => false,
				EventGameModel::tableName() . '.is_live' => true,
			])->joinWith('game', false)->asArray()->all(), 'code_api');
			*/
			$event_live_ids = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['code_api'])->andWhere([
				ApiEventGameModel::tableName() . '.service_api' => Betsapi1xbetService::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.is_main_provider' => true,
				//ApiEventGameModel::tableName() . '.is_checked' => true,
				//ApiEventGameModel::tableName() . '.api_event_id' => 0,
				EventGameModel::tableName() . '.is_finished' => 0,
				EventGameModel::tableName() . '.is_live' => 1,
			])->joinWith('game', false)->asArray()->all(), 'code_api');
			// set 2s sleep to get data event
			/*foreach($event_live_ids as $k => $event_id){
				if(!empty($eventsTime[$event_id]) && ($time - $eventsTime[$event_id]) < 7){
					unset($event_live_ids[$k]);
				} else {
					$eventsTime[$event_id] = $time;
				}
			}*/
			foreach($event_live_ids as $k => $event_id){
				if($redis = Yii::$app->get('redis', false)) {
					$key = '1xbet-event-live'.$event_id;
					if($redis->get($key) != null){
						unset($event_live_ids[$k]);
						continue;
					}
					$redis->set($key,1,'EX',60);
				}
			}

			while(count($slice = array_splice($event_live_ids, 0, 10)) > 0) {
				//exec('php /var/www/website/yii.php 1xbet/event-live '.Json::encode($slice).' >> /var/www/website/runtime/logs/api-1xbet-event-live.log 2>&1');
				exec('php /var/www/website/yii.php 1xbet/event-live '.Json::encode($slice).' > /var/www/website/runtime/logs/api-1xbet-event-live.log 2>/dev/null &');
			}
/*
			// add video to bet365 event from 1xbet
			$event_ids = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['code_api'])->andWhere([
				'<', EventGameModel::tableName() . '.starts_at', time(),
			])->andWhere([
				ApiEventGameModel::tableName() . '.service_api' => Betsapi1xbetService::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.is_main_provider' => false,
				ApiEventGameModel::tableName() . '.is_checked' => false,
				EventGameModel::tableName() . '.is_finished' => false,
				EventGameModel::tableName() . '.is_live' => true,
			])->joinWith('game', false)->asArray()->all(), 'code_api');

			while(count($slice = array_splice($event_ids, 0, 10)) > 0) {
				if($data = self::requestAPI('event', ['event_id' => implode(',', $slice)])) {
					$service->event($data);
					unset($data);
				}
			}*/
			unset($service);
			gc_collect_cycles();
			if($time > time()) {
				@time_sleep_until($time);
			}
		}
	}

	public function actionCheckImage() {
		echo 'check participant image 1xbet started' . PHP_EOL;
		$service = new Betsapi1xbetService();
		$service->checkParticipantImage();
	}

	public function actionResults() {
		echo 'results started' . PHP_EOL;
		$time = time();
		while(true) {
			$service = new Betsapi1xbetService();

			$fis = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['code_api'])->andWhere([
				'<', EventGameModel::tableName() . '.starts_at', time(),
			])->andWhere([
				'>', ApiEventGameModel::tableName() . '.api_event_id', 0,
			])->andWhere([
				ApiEventGameModel::tableName() . '.service_api' => Betsapi1xbetService::SOURCE_BETSAPI,
				EventGameModel::tableName() . '.is_resulted' => 0,
				ApiEventGameModel::tableName() . '.is_main_provider' => true
			])->joinWith('game', false)->asArray()->all(), 'code_api');

			while(count($slice = array_splice($fis, 0, 10)) > 0) {
				if($data = self::requestAPI('result', ['event_id' => implode(',', $slice)])) {
					$service->result($data,$slice);
				}
			}
			unset($service);
			gc_collect_cycles();
			if($time > time()) {
				@time_sleep_until($time);
			}
		}
	}

	public function actionSetMarkets() {
		echo 'set markets 1xbet started' . PHP_EOL;
		$service = new Betsapi1xbetService();
		$service->setMarkets();
	}

	public function actionSetOutcomes() {
		echo 'set outcomes 1xbet started' . PHP_EOL;
		$service = new Betsapi1xbetService();
		$service->setOutcomes();
	}

	public function actionInplayStartEvent($eventIdsJson = '') {
		echo 'inplay event start started' . PHP_EOL;
		if( $eventIdsJson == ''){
			return false;
		}
		$service = new Betsapi1xbetService();
		$event_ids = Json::decode($eventIdsJson);
		if($data = self::requestAPI('event', ['event_id' => implode(',', $event_ids)])) {
			foreach($event_ids as $event_id){
				$eventsCodeApi[(int)$event_id] = (int)$event_id;
			}
			$service->event($data, $eventsCodeApi);
			unset($data);
		}
		exit(1);
	}

	public function actionEventLive($eventIdsJson = '') {
		echo 'event live started' . PHP_EOL;
		if( $eventIdsJson == ''){
			return false;
		}
		$service = new Betsapi1xbetService();
		$event_ids = Json::decode($eventIdsJson);
		if($data = self::requestAPI('event', ['event_id' => implode(',', $event_ids)])) {
			foreach($event_ids as $event_id){
				$eventsCodeApi[(int)$event_id] = (int)$event_id;
			}

			$dataDecode = Json::decode($data);
			foreach($dataDecode['results'] as $event){
				$event_id = (int)$event['id'];
				if(!empty($eventsCodeApi[$event_id])){
					unset($eventsCodeApi[$event_id]);
				}
				/*if($redis = Yii::$app->get('redis', false)) {
					$key = '1xbet-event-live'.$event_id;
					if($redis->get($key) != null){
						continue;
					}
					$redis->set($key,1,'EX',60);
				}*/

				if($redis = Yii::$app->get('redis', false)) {
					$key = '1xbet-event-live-time-update-'.$event_id;
					if(($updated_at = $redis->get($key)) != null && (int)$updated_at == (int)$event['updated_at']){
						Yii::warning('Event id = '.$event['id'].'.updated_at:'.$event['updated_at']);
						$keyEvent = '1xbet-event-live'.$event_id;
						$redis->del($keyEvent);
						continue;
					}
					$redis->set($key,(int)$event['updated_at'],'EX',3600*3);
				}

				//$service->eventLive($event, $eventsCodeApi);
				//exec('php /var/www/website/yii.php 1xbet/set-event-live '.json_encode($event).' > /var/www/website/runtime/logs/api-1xbet-event-live.log 2>/dev/null &');
				//exec('php /var/www/website/yii.php 1xbet/set-event-live '.base64_encode(json_encode($event)).' >> /var/www/website/runtime/logs/api-1xbet-event-live.log 2>&1');
				exec('php /var/www/website/yii.php 1xbet/set-event-live '.base64_encode(json_encode($event)).' > /var/www/website/runtime/logs/api-1xbet-event-live.log 2>/dev/null &');
			}
			//$service->eventLive($data, $eventsCodeApi);
			unset($data);
			try{
				if(count($eventsCodeApi) > 0){
					$idsGame = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['game_id'])->andWhere([
						ApiEventGameModel::tableName() . '.service_api' => Betsapi1xbetService::SOURCE_BETSAPI,
						ApiEventGameModel::tableName() . '.is_main_provider' => true,
						ApiEventGameModel::tableName() . '.api_event_id' => 0,
						ApiEventGameModel::tableName() . '.code_api' => $eventsCodeApi,
					])->asArray()->all(), 'game_id');

					EventGameModel::updateAll(
						//['is_resulted' => true,'is_finished' => 1],
						['is_finished' => 1],
						['in', 'id', $idsGame]
					);
				}
			} catch (\Throwable $e) {
				Yii::error($e);
			}
		}
		exit(1);
	}

	public function actionSetEventLive($jsonData) {
		echo 'set event live started' . PHP_EOL;
		$service = new Betsapi1xbetService();
		$data = json_decode(base64_decode($jsonData),true);
		$service->eventLive($data);
		$event_id = (int)$data['id'];
		echo 'event id:' . $event_id. PHP_EOL;
		if($redis = Yii::$app->get('redis', false)) {
			$key = '1xbet-event-live'.$event_id;
			$redis->del($key);
		}
		exit(1);
	}

}

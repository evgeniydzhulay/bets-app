<?php

namespace console\controllers;

use backend\modules\games\models\Translator_1X_Model;
use backend\modules\games\models\Translator_365_Model;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesModel;
use common\modules\games\queries\EventOutcomesQuery;
use common\modules\games\queries\GameMarketsGroupsQuery;
use common\modules\games\queries\GameMarketsQuery;
use common\modules\games\queries\GameOutcomesQuery;
use yii\console\Controller;
use yii\db\Expression;

class CronController extends Controller {

	public function actionT365o () {
		(new Translator_365_Model())->translateOutcomes();
	}

	public function actionT365m () {
		(new Translator_365_Model())->translateMarkets();
	}

	public function actionT1x () {
		(new Translator_1X_Model())->translate();
	}

	public function actionRecalculate() {
		echo 'Recalculating...' . PHP_EOL;
		$query = EventGameModel::find()->select([
			'cnt' => new Expression('COUNT( `event_outcomes`.`id` )'),
			EventGameModel::tableName() . '.id',
			EventGameModel::tableName() . '.outcomes_count',
		])->active()->joinWith([
			'eventOutcomes' => function(EventOutcomesQuery $query) {
				return $query->andOnCondition([
					EventOutcomesModel::tableName() . '.is_hidden' => 0,
					EventOutcomesModel::tableName() . '.is_banned' => 0,
					EventOutcomesModel::tableName() . '.is_finished' => 0,
					EventOutcomesModel::tableName() . '.is_enabled' => 1,
				])->joinWith([
					'group' => function(GameMarketsGroupsQuery $query) {
						return $query->andOnCondition([
							GameMarketsGroupsModel::tableName() . '.is_enabled' => 1,
						]);
					},
					'market' => function(GameMarketsQuery $query) {
						return $query->andOnCondition([
							GameMarketsModel::tableName() . '.is_enabled' => 1,
						]);
					},
					'outcome' => function(GameOutcomesQuery $query) {
						return $query->andOnCondition([
							GameOutcomesModel::tableName() . '.is_enabled' => 1,
						]);
					},
				], false);
			},
		], false)->andWhere([
			EventGameModel::tableName() . '.is_resulted' => 0,
		])->groupBy([
			EventGameModel::tableName() . '.id'
		]);
		$list = $query->asArray()->all();
		foreach($list AS $item) {
			if(+$item['outcomes_count'] === +$item['cnt']) {
				continue;
			}
			echo "{$item['id']} - {$item['cnt']}" . PHP_EOL;
			EventGameModel::updateAll(['outcomes_count' => +$item['cnt']], ['id' => +$item['id']]);
		}
		echo 'Recalculating finished' . PHP_EOL;
	}
}

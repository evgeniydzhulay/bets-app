<?php

namespace console\controllers;

use backapi\modules\games\services\BetsapiService;
use common\modules\games\models\ApiEventGameModel;
use common\modules\games\models\EventGameModel;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\log\FileTarget;

/**
 * Class Bet365Controller
 * @package console\controllers
 *
 * upcoming  :cron  :6h
 * results   :cron  :5m
 * prematch  :pm2
 * live      :pm2
 * filter-inplay :cron :1m
 */
class Bet365Controller extends Controller {

	public function init () {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-bet365-error.log');
		}
		return parent::init();
	}

	protected static function requestAPI($method, $params = []) {
		$params['token'] = \Yii::$app->params['bet365.token'];
		$content = @file_get_contents("https://api.betsapi.com/v1/bet365/{$method}?" . http_build_query($params));
		if(!$content) {
			$error = error_get_last();
			$error = explode(': ', $error['message']);
			$error = trim($error[2]) . ' ' . trim($error[0]) . PHP_EOL;
			fwrite(STDERR, 'Error: '. $error);
			\Yii::error($error, 'integration/bet365');
		}
		return $content;
	}

	public function actionUpcoming() {
		echo 'upcoming started' . PHP_EOL;
		$service = new BetsapiService();
		foreach (BetsapiService::sports() AS $sport => $label) {
			if($data = self::requestAPI('upcoming', ['sport_id' => $sport])) {
				$pager = json_decode($data, true);
				if(json_last_error() !== JSON_ERROR_NONE) {
					continue;
				}
				$service->prematch($data);
				if($pager['pager']['per_page'] >= $pager['pager']['total']) {
					continue;
				}
				$iteration = 1;
				for($i = 2; $i <= ceil($pager['pager']['total'] / $pager['pager']['per_page']); $i++) {
					if($data = self::requestAPI('upcoming', ['sport_id' => $sport, 'page' => $i])) {
						$service->prematch($data);
						$iteration = 1;
					} elseif($iteration <= 5) {
						$i--;
						$iteration++;
					} else {
						$iteration = 1;
					}
				}
			}
		}
	}

	public function actionPrematch() {
		echo 'prematch started' . PHP_EOL;
		$time = time();
		while(true) {
			$service = new BetsapiService();
			$time++;
			$fis = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['code_api'])->andWhere([
				'>', EventGameModel::tableName() . '.starts_at', time()
			])->andWhere([
				ApiEventGameModel::tableName() . '.service_api' => BetsapiService::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.is_main_provider' => true,
			]) ->joinWith('game', false)->asArray()->all(), 'code_api');
			while(count($slice = array_splice($fis, 0, 10)) > 0) {
				if($data = self::requestAPI('prematch', ['FI' => implode(',', $slice)])) {
					$service->event($data);
					unset($data);
				}
			}
			unset($service);
			gc_collect_cycles();
			if($time > time()) {
				@time_sleep_until($time);
			}
		}
	}

	public function actionLive() {
		echo 'live started' . PHP_EOL;
		$time = time();
		$counter = 0;
		$eventsQueue = [];
		while(true) {
			$service = new BetsapiService();
			$time++;
			$counter++;

			$eventsBet365 = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['code_api'])->andWhere([
				ApiEventGameModel::tableName() . '.service_api' => BetsapiService::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.is_main_provider' => true,
				EventGameModel::tableName() . '.is_finished' => false,
				EventGameModel::tableName() . '.is_live' => true,
			])->joinWith('game', false)->asArray()->all(), 'code_api');

			if($data = self::requestAPI('inplay')) {
				$eventsQueue = $service->inplay($data, $eventsQueue,$eventsBet365);
				unset($data);
			}
			unset($service);
			gc_collect_cycles();
			if($time > time()) {
				@time_sleep_until($time);
			}
			if($counter > 43200) {
				exit(1);
			}
		}
	}

	public function actionResults() {
		echo 'results started' . PHP_EOL;
		$service = new BetsapiService();
		$fis = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['code_api'])->andWhere([
			'<', EventGameModel::tableName() . '.starts_at', time(),
		])->andWhere([
			ApiEventGameModel::tableName() . '.service_api' => BetsapiService::SOURCE_BETSAPI,
			ApiEventGameModel::tableName() . '.is_main_provider' => true,
			EventGameModel::tableName() . '.is_resulted' => false
		])->joinWith('game', false)->asArray()->all(), 'code_api');
		while(count($slice = array_splice($fis, 0, 10)) > 0) {
			if($data = self::requestAPI('result', ['event_id' => implode(',', $slice)])) {
				$service->result($data,$slice);
			}
		}
	}

	public function actionFilterInplay() {
		echo 'inplay filter started' . PHP_EOL;
		$service = new BetsapiService();
		if($data = self::requestAPI('inplay_filter')) {
			$service->inplay_filter($data);
		}
	}

	public function actionCheckImage() {
		echo 'check participant image started' . PHP_EOL;
		$service = new BetsapiService();
		$service->checkParticipantImage();
	}

	public function actionParticipantImage() {
		echo 'participant image started' . PHP_EOL;
		$service = new BetsapiService();
		$fis = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['code_api'])->andWhere([
				ApiEventGameModel::tableName() . '.service_api' => BetsapiService::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.is_checked' => false
			])->asArray()->all(), 'code_api');
		while(count($slice = array_splice($fis, 0, 10)) > 0) {
			if($data = self::requestAPI('result', ['event_id' => implode(',', $slice)])) {
				$service->participantImage($data,$slice);
				unset($data);
			}
		}
	}

	public function actionInplayEvent($event_fi = 0) {
		echo 'inplay event started' . PHP_EOL;
		if( (int)$event_fi == 0){
			return false;
		}
		$service = new BetsapiService();
		if($data = self::requestAPI('event', ['FI' => (int)$event_fi])) {
			$service->inplayEventAllMarkets($data,$event_fi);
		}
		exit(1);
	}
}

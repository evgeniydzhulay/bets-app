<?php

use common\behaviors\LanguageRequestBehavior;
use common\behaviors\TimezoneRequestBehavior;
use frontapi\models\UserAuthModel;
use yii\helpers\ArrayHelper;
use yii\web\Request;
use yii\web\Response;
use yii\web\UrlManager;
use yii\web\UrlNormalizer;

return ArrayHelper::merge([
	'id' => 'frontapi',
	'controllerNamespace' => 'frontapi\controllers',
	'basePath' => dirname(__DIR__),
	'as LanguageRequestBehavior' => LanguageRequestBehavior::class,
	'as TimezoneRequestBehavior' => TimezoneRequestBehavior::class,
	'language' => 'en',
	'components' => [
		'user' => [
			'enableSession' => false,
			'enableAutoLogin' => false,
			'identityClass' => UserAuthModel::class,
		],
		'urlManager' => [
			'class' => UrlManager::class,
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'normalizer' => [
				'class' => UrlNormalizer::class,
				'collapseSlashes' => true,
				'normalizeTrailingSlash' => true,
			],
		],
		'request' => [
			'class' => Request::class,
			'enableCookieValidation' => false,
			'enableCsrfValidation' => false,
			'enableCsrfCookie' => false,
		],
		'response' => [
			'class' => Response::class,
			'format' => Response::FORMAT_JSON,
			'on beforeSend' => function ($event) {
				$response = $event->sender;
				if (
					$response->format == Response::FORMAT_JSON &&
					$response->data !== null &&
					is_array($response->data)
				) {
					$response->data['success'] = $response->data['success'] ?? $response->isSuccessful;
					if (!$response->isSuccessful) {
						$response->data['status'] = $response->statusCode;
					}
				}
			},
		],
	],
],
	(is_file(__DIR__ . '/main-local.php') ? require_once(__DIR__ . '/main-local.php') : [])
);
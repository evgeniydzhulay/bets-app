<?php

use frontapi\Application;
use yii\helpers\ArrayHelper;

defined('YII_DEBUG') or define('YII_DEBUG', ISSET($_SERVER['YII_DEBUG']) ? (bool)$_SERVER['YII_DEBUG'] : false);
defined('YII_ENV') or define('YII_ENV', ISSET($_SERVER['YII_ENV']) ? $_SERVER['YII_ENV'] : 'prod');

$applicationPath = dirname(__DIR__);
$rootPath = dirname($applicationPath);
require($rootPath . '/vendor/autoload.php');
require($rootPath . '/vendor/yiisoft/yii2/Yii.php');
require($applicationPath . '/Application.php');

$config = ArrayHelper::merge(require($rootPath . '/common/config/main.php'), require($applicationPath . '/config/main.php'));
(new Application($config))->run();

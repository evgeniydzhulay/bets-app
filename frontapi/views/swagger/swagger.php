<?php

$this->beginPage();
?>
<!-- HTML for static distribution bundle build -->
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Swagger UI</title>
	<?php $this->head() ?>
	<link href="//unpkg.com/swagger-ui-dist@3/swagger-ui.css" rel="stylesheet" media="all" />
	<script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-bundle.js"></script>
	<script src="//unpkg.com/swagger-ui-dist@3/swagger-ui-standalone-preset.js"></script>
	<style>
		html {
			box-sizing: border-box;
			overflow: -moz-scrollbars-vertical;
			overflow-y: scroll;
		}
		*, *:before, *:after {
			box-sizing: inherit;
		}
		body {
			margin: 0;
			background: #fafafa;
		}
	</style>
</head>
<body>
<?php $this->beginBody(); ?>
<div id="swagger-ui"></div>
<script>
    window.onload = function () {
        window.ui = SwaggerUIBundle({
            url: "/swagger/json",
            dom_id: '#swagger-ui',
            deepLinking: true,
            enableCORS: false,
            presets: [
                SwaggerUIBundle.presets.apis,
            ],
        });
    }
</script>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>


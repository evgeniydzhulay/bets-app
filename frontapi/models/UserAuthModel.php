<?php

namespace frontapi\models;

use common\modules\user\models\UserApplicationModel;
use common\modules\user\models\UserModel;
use yii\web\User;

class UserAuthModel extends UserModel {

	/** @inheritdoc */
	public static function findIdentityByAccessToken($token, $type = null) {
		$token = UserApplicationModel::getMobileToken($token);
		if($token) {
			$token->trigger(User::EVENT_AFTER_LOGIN);
			return UserModel::find()->andWhere(['id' => $token['user_id']])->limit(1)->one();
		}
		return null;
	}
}
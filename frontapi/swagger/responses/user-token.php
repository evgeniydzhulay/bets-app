<?php

/**
 * @OA\Schema(
 *     schema="responseUserToken",
 *     description="User auth token",
 *     required={"token"},
 *     @OA\Property(
 *         property="token", type="string",
 *         description="Application token if request is successful",
 *     ),
 * ),
 */
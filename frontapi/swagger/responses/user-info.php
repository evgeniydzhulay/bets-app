<?php

/**
 * @OA\Schema(
 *     schema="responseUserInfo",
 *     description="Basic user info",
 *     required={"user", "profile"},
 *     @OA\Property(
 *         property="user", type="object", default="User info",
 *         title="Basic user info",
 *         required={"id", "email", "amount"},
 *         @OA\Property(
 *              property="id", type="number",
 *              description="User id",
 *         ),
 *         @OA\Property(
 *              property="email", type="string", format="email",
 *              description="User email",
 *         ),
 *         @OA\Property(
 *              property="currency", type="string",
 *              description="User purse currency",
 *         ),
 *         @OA\Property(
 *              property="amount", type="number", format="float",
 *              description="User purse amount",
 *         ),
 *     ),
 *     @OA\Property(
 *         property="profile", type="object", default="User profile",
 *         title="User profile info",
 *         required={"name", "surname", "dob", "country", "passport_series"},
 *         @OA\Property(
 *              property="name", type="string",
 *              description="Name",
 *         ),
 *         @OA\Property(
 *              property="surname", type="string",
 *              description="Surname",
 *         ),
 *         @OA\Property(
 *              property="patrynomic", type="string",
 *              description="Patrynomic",
 *         ),
 *         @OA\Property(
 *              property="phone", type="string",
 *              description="phone",
 *         ),
 *         @OA\Property(
 *              property="country", type="string",
 *              description="Country",
 *         ),
 *         @OA\Property(
 *              property="city", type="string",
 *              description="City",
 *         ),
 *         @OA\Property(
 *              property="address", type="string",
 *              description="Address",
 *         ),
 *         @OA\Property(
 *              property="dob", type="string", format="date",
 *              description="Date of birth",
 *         ),
 *         @OA\Property(
 *              property="passport_series", type="string",
 *              description="Passport series",
 *         ),
 *         @OA\Property(
 *              property="passport_issuer", type="string",
 *              description="Passport issuer",
 *         ),
 *         @OA\Property(
 *              property="passport_date", type="string", format="date",
 *              description="Passport issue date",
 *         ),
 *     ),
 * )
 */
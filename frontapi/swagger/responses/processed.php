<?php
/**
* @OA\Schema(
*     schema="responseProcessed",
*     description="Request is successful",
*     required={"success"},
*     @OA\Property(
*          property="success", type="boolean", default=true,
*          description="Is request successful",
*     ),
*     @OA\Property(
*          property="message", type="string",
*          description="Optional message for user",
*     ),
*     @OA\Property(
*          property="errors", format="object", type="object",
*          title="Form errors",
*          @OA\Items(
*              type="string",
*          ),
*     ),
* )
*/
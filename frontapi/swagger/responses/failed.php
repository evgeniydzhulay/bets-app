<?php

/**
 * @OA\Schema(
 *     schema="responseFailed",
 *     description="Request failed",
 *     required={"success"},
 *     @OA\Property(
 *          property="success", type="boolean", default=false,
 *          description="Is request successful",
 *     ),
 *     @OA\Property(
 *          property="name", type="string", default="Exception",
 *          description="Optional exception name",
 *     ),
 *     @OA\Property(
 *          property="type", type="string", default="Exception type",
 *          description="Optional exception type",
 *     ),
 *     @OA\Property(
 *          property="message", type="string",
 *          default="Error wile processing response",
 *          description="Optional message for user",
 *     ),
 * )
 */
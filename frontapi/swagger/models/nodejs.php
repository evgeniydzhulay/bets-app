<?php

/**
 * @OA\Schema(
 *     schema="nodeGameResults:gtimer",
 *     @OA\Property(property="g", type="number", description="Game ID"),
 *     @OA\Property(property="t", type="string", description="Game timer"),
 *     @OA\Property(property="r", type="boolean", description="Is timer running"),
 * )
 */

/**
 * @OA\Schema(
 *     schema="nodeGameResults:gres",
 *     @OA\Property(property="g", type="number", description="Game ID"),
 *     @OA\Property(property="gp", type="string", description="Game period"),
 *     @OA\Property(property="r", type="string", description="Result"),
 *     @OA\Property(property="rs", type="string", description="Result simple"),
 *     @OA\Property(property="re", type="string", description="Result extended"),
 * )
 */

/**
 * @OA\Schema(
 *     schema="nodeGameResults:gstats",
 *     @OA\Property(property="g", type="number", description="Game ID"),
 *     @OA\Property(property="f", type="integer", description="Finished"),
 *     @OA\Property(property="e", type="integer", description="Enabled"),
 *     @OA\Property(property="h", type="integer", description="Hidden"),
 *     @OA\Property(property="b", type="integer", description="Banned"),
 * )
 */

/**
 * @OA\Schema(
 *     schema="nodeOutcomeRate:or",
 *     @OA\Property(property="g", type="number", description="Game ID"),
 *     @OA\Property(property="o", type="number", description="Outcome ID"),
 *     @OA\Property(property="r", type="number", description="Outcome rate"),
 * )
 */

/**
 * @OA\Schema(
 *     schema="nodeOutcomeStatus:os",
 *     @OA\Property(property="g", type="number", description="Game ID"),
 *     @OA\Property(property="o", type="number", description="Outcome ID"),
 *     @OA\Property(property="s", type="number", description="Outcome status"),
 * )
 */

/**
 * @OA\Schema(
 *     schema="nodeOutcomeBanned:oisb",
 *     @OA\Property(property="g", type="number", description="Game ID"),
 *     @OA\Property(property="o", type="number", description="Outcome ID"),
 *     @OA\Property(property="is", type="boolean", description="Is banned"),
 * )
 */

/**
 * @OA\Schema(
 *     schema="nodeOutcomeEnabled:oise",
 *     @OA\Property(property="g", type="number", description="Game ID"),
 *     @OA\Property(property="o", type="number", description="Outcome ID"),
 *     @OA\Property(property="is", type="boolean", description="Is enabled"),
 * )
 */

/**
 * @OA\Schema(
 *     schema="nodeOutcomeHidden:oish",
 *     @OA\Property(property="g", type="number", description="Game ID"),
 *     @OA\Property(property="o", type="number", description="Outcome ID"),
 *     @OA\Property(property="is", type="boolean", description="Is hidden"),
 * )
 */

/**
 * @OA\Schema(
 *     schema="nodeOutcomeFinished:oisf",
 *     @OA\Property(property="g", type="number", description="Game ID"),
 *     @OA\Property(property="o", type="number", description="Outcome ID"),
 *     @OA\Property(property="is", type="boolean", description="Is finished"),
 * )
 */
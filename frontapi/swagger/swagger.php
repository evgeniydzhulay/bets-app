<?php
/**
 * @OA\OpenApi(
 *     @OA\Info(
 *         version="1.0.0",
 *         title="Betting mobile API",
 *         description="API for mobile devices",
 *     ),
 *     @OA\Server(
 *         description="Staging API",
 *         url="https://api.luxe.bet/"
 *     ),
 * )
 *
 * @OA\SecurityScheme(
 *     description="Bearer auth",
 *     securityScheme="bearer",
 *     scheme="bearer",
 *     type="http"
 * )
 */

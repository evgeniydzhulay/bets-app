<?php

namespace frontapi\controllers;

use Yii;
use Symfony\Component\Finder\Finder;
use yii\web\Controller;
use yii\web\Response;

class SwaggerController extends Controller {

	public function actionIndex() {
		\Yii::$app->response->format = Response::FORMAT_HTML;
		return $this->renderPartial('swagger');
	}

	public function actionJson() {
		return  \OpenApi\scan(Finder::create()->in(Yii::getAlias('@frontapi'))->name('*.php')->files())->jsonSerialize();
	}
}
<?php

namespace frontapi\controllers;

use Yii;
use yii\helpers\Json;

class LocationsController extends BaseController {

	/** @inheritdoc */
	public function behaviors () {
		$behaviors = parent::behaviors();
		$behaviors['authenticator']['optional'] = [
			'countries', 'timezones'
		];
		return $behaviors;
	}

	/**
	 * Get available countries list, returns key - value pairs of country code and localized country name
	 *
	 * @return array
	 *
	 * @OA\Get(
	 *   path="/locations/countries",
	 *   operationId="countries",
	 *   tags={"locations"},
	 *
	 *   @OA\Response(response="200", description="Countries list",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(
	 *         @OA\Property(property="results", type="object",
	 *           example="{'UK': 'United kingdom', 'CN': 'China'}"
	 *         ),
	 *       )
	 *     })
	 *   )
	 * )
	 */
	public function actionCountries() {
		return ['success' => true, 'results' => Yii::$app->get('locations')->getCountries()];
	}

	/**
	 * Get available timezones list
	 *
	 * @return array
	 *
	 * @OA\Get(
	 *   path="/locations/timezones",
	 *   operationId="timezones",
	 *   tags={"locations"},
	 *   @OA\Response(response="200", description="Timezones list",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(
	 *         @OA\Property(property="results", type="array", description="Timezones list",
	 *           @OA\Items(
	 *             description="Timezone item",
	 *             @OA\Property(property="timezone", type="string", description="ID"),
	 *             @OA\Property(property="country", type="string", description="Country"),
	 *             @OA\Property(property="name", type="string", description="Full name"),
	 *             @OA\Property(property="title", type="string", description="Title"),
	 *             @OA\Property(property="offset", type="number", format="integer", description="Offset"),
	 *           )
	 *         ),
	 *       )
	 *     })
	 *   )
	 * )
	 */
	public function actionTimezones() {
		return ['success' => true, 'results' => Yii::$app->get('locations')->getTimeZones()];
	}
}
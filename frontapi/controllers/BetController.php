<?php

namespace frontapi\controllers;

use common\modules\games\helpers\GameHelpers;
use common\modules\games\models\BetsItemsModel;
use common\modules\games\models\BetsModel;
use common\modules\games\models\CartsItemsModel;
use common\modules\games\models\CartsModel;
use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\EventParticipantsModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use common\modules\games\models\GameSportsModel;
use common\modules\games\queries\BetsItemsQuery;
use common\modules\games\queries\EventGameLocaleQuery;
use common\modules\games\queries\EventGameQuery;
use common\modules\games\queries\EventOutcomesQuery;
use common\modules\games\queries\EventParticipantsQuery;
use common\modules\games\queries\GameMarketsLocaleQuery;
use common\modules\games\queries\GameOutcomesLocaleQuery;
use common\modules\games\queries\GameParticipantsLocaleQuery;
use common\modules\user\models\UserModel;
use common\modules\games\search\BetsSearch;
use common\modules\games\queries\EventTournamentLocaleQuery;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;

class BetController extends BaseController {

	/** @inheritdoc */
	public function behaviors () {
		$behaviors = parent::behaviors();
		$behaviors['authenticator']['optional'] = [
			'cart', 'edit', 'item-add', 'item-remove', 'delete'
		];
		return $behaviors;
	}

	/**
	 * View Cart
	 *
	 * @return array
	 *
	 * @OA\Get(
	 *   path="/bet/cart",
	 *   operationId="cart",
	 *   tags={"Bet"},
	 *   @OA\Parameter(name="cart_id", in="query", required=true, description="Cart id", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="cart_key", in="query", required=true, description="Cart key", @OA\Schema(type="string")),
	 *   @OA\Response(response="404", description="Not found", @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseFailed"),
	 *     })
	 *   ),
	 *   @OA\Response(
	 *     response="200",
	 *     description="Cart item",
	 *     @OA\JsonContent(
	 *       type="array",
	 *       @OA\Items(
	 *         description="Tournament info",
	 *         @OA\Property(property="id", type="number", description="ID"),
	 *         @OA\Property(property="bet", type="number", description="Bet amount"),
	 *         @OA\Property(property="on_change", type="number", description="How act on change"),
	 *         @OA\Property(property="items", type="array", description="Items list", @OA\Items(
	 *             description="Catr item",
	 *             @OA\Property(property="id", type="number", description="ID"),
	 *             @OA\Property(property="sport_id", type="number", description="Sport ID"),
	 *             @OA\Property(property="sport_image", type="string", description="Sport image"),
	 *             @OA\Property(property="game_id", type="number", description="Game ID"),
	 *             @OA\Property(property="game_title", type="string", description="Game title"),
	 *             @OA\Property(property="outcome_id", type="number", description="Outcome ID"),
	 *             @OA\Property(property="outcome_title", type="string", description="Outcome title"),
	 *             @OA\Property(property="market_id", type="number", description="Market ID"),
	 *             @OA\Property(property="market_title", type="string", description="Market title"),
	 *             @OA\Property(property="rate", type="number", description="Rate"),
	 *             @OA\Property(property="rate_now", type="number", description="Current rate"),
	 *
	 *             @OA\Property(property="is_enabled", type="number", description="Is outcome enabled"),
	 *             @OA\Property(property="is_finished", type="number", description="Is outcome finished"),
	 *             @OA\Property(property="is_banned", type="number", description="Is outcome banned"),
	 *             @OA\Property(property="is_hidden", type="number", description="Is outcome hidden"),
	 *           )
	 *         ),
	 *       )
	 *     )
	 *   )
	 * )
	 */
	public function actionCart(){
		$cart = self::getCurrentCart();
		$query = CartsItemsModel::find()->andWhere(['cart_id' => $cart->id])->addSelect([
			CartsItemsModel::tableName() . '.*',
			'sport_image' => GameSportsModel::tableName() . '.image',
			'game_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(game_locale_tr.title, game_locale_en.title)') : 'game_locale_en.title',
			'market_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(market_locale_tr.title, market_locale_en.title)') : 'market_locale_en.title',
			'outcome_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(outcome_locale_tr.title, outcome_locale_en.title)') : 'outcome_locale_en.title',

			'outcome_rate' => EventOutcomesModel::tableName() . '.rate',
			'outcome_enabled' => EventOutcomesModel::tableName() . '.is_enabled',
			'outcome_finished' => EventOutcomesModel::tableName() . '.is_finished',
			'outcome_banned' => EventOutcomesModel::tableName() . '.is_banned',
			'outcome_hidden' => EventOutcomesModel::tableName() . '.is_hidden',
		])->with([
			'eventParticipants' => function(EventParticipantsQuery $query) {
				$query->innerJoinWith([
					'participantLocale locale_en' => function(GameParticipantsLocaleQuery $query) {
						return $query->andOnCondition(['locale_en.lang_code' => 'en']);
					},
				], false)->addSelect([
					EventParticipantsModel::tableName() . '.*',
					'name' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.name, locale_en.name)') : 'locale_en.name',
				]);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'participantLocale locale_tr' => function(GameParticipantsLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				}
			},
		])->innerJoinWith([
			'gameLocales game_locale_en' => function(EventGameLocaleQuery $query) {
				return $query->andOnCondition(['game_locale_en.lang_code' => 'en']);
			},
			'marketLocales market_locale_en' => function(GameMarketsLocaleQuery $query) {
				return $query->andOnCondition(['market_locale_en.lang_code' => 'en']);
			},
			'outcomeLocales outcome_locale_en' => function(GameOutcomesLocaleQuery $query) {
				return $query->andOnCondition(['outcome_locale_en.lang_code' => 'en']);
			},
			'sport'
		], false)->asArray();
		if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$query->joinWith([
				'gameLocales game_locale_tr' => function(EventGameLocaleQuery $query) {
					return $query->andOnCondition([
						'game_locale_tr.lang_code' => Yii::$app->language,
						'game_locale_tr.is_active' => 1,
					]);
				},
				'marketLocales market_locale_tr' => function(GameMarketsLocaleQuery $query) {
					return $query->andOnCondition([
						'market_locale_tr.lang_code' => Yii::$app->language,
						'market_locale_tr.is_active' => 1,
					]);
				},
				'outcomeLocales outcome_locale_tr' => function(GameOutcomesLocaleQuery $query) {
					return $query->andOnCondition([
						'outcome_locale_tr.lang_code' => Yii::$app->language,
						'outcome_locale_tr.is_active' => 1,
					])->via('outcome', function(EventOutcomesQuery $query) {
						$query->alias('outcome_tr');
					});
				},
			], false);
		};
		return [
			'id' => $cart->id,
			'bet' => $cart->amount,
			'on_change' => $cart->on_change,
			'items' => array_values(ArrayHelper::map($query->all(), 'id', function($item) {
				$participants = \array_values(ArrayHelper::map($item['eventParticipants'], 'id', function($item) {
					return [
						'id' => +$item['participant_id'],
						'name' => $item['name'] ?? '',
					];
				}));
				$participantReplacements = [];
				foreach($participants AS $k => $v) {
					$participantReplacements[$k + 1] = $v['name'];
				}
				return [
					'id' => +$item['id'],
					'sport_id' => +$item['sport_id'],
					'sport_image' => !empty($item['sport_image']) ? Yii::getAlias("@web/frontend/uploads/games/{$item['sport_image']}") : '',
					'game_id' => +$item['game_id'],
					'game_title' => $item['game_title'],
					'outcome_id' => +$item['outcome_id'],
					'outcome_title' => GameHelpers::replacingCodeWithName($item['outcome_title'], $participantReplacements),
					'market_id' => +$item['market_id'],
					'market_title' => $item['market_title'] ?? '',
					'rate' => +$item['rate'],
					'rate_now' => +$item['outcome_rate'],
					'is_enabled' => +$item['outcome_enabled'],
					'is_finished' => +$item['outcome_finished'],
					'is_banned' => +$item['outcome_banned'],
					'is_hidden' => +$item['outcome_hidden'],
				];
			}))
		];

	}

	/**
	 * Edit cart
	 * @param float $amount
	 * @param integer $on_change
	 * @return array
	 *
	 * @OA\Get(
	 *   path="/bet/edit",
	 *   operationId="edit",
	 *   tags={"Bet"},
	 *   @OA\Parameter(name="cart_id", in="query", required=true, description="Cart id", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="cart_key", in="query", required=true, description="Cart key", @OA\Schema(type="string")),
	 *   @OA\Parameter(name="amount", in="query", required=true, description="Cart amount", @OA\Schema(type="number")),
	 *   @OA\Parameter(
	 *     name="on_change", in="query", required=true, @OA\Schema(type="integer", enum={0,1,2}),
	 *     description="On change behavior. Accept changes: 0; need confirm any changes: 1; need confirm only if lower 2;"
	 *   ),
	 *   @OA\Response(
	 *     response="200",
	 *     description="Cart item",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(
	 *         @OA\Property(property="cart_id", type="number", description="ID"),
	 *         @OA\Property(property="cart_key", type="string", description="Key"),
	 *         @OA\Property(property="bet", type="number", description="Bet amount"),
	 *         @OA\Property(property="on_change", type="number", description="How act on change"),
	 *       )
	 *     })
	 *   )
	 * )
	 */
	public function actionEdit($amount, $on_change) {
		$cart = self::getCurrentCart(false) ?? $this->createCart();
		$cart->scenario = CartsModel::SCENARIO_UPDATE;
		$success = ($cart->load([
			'amount' => $amount,
			'on_change' => $on_change,
		],'') && $cart->save());
		return [
			'success' => $success,
			'errors' => $cart->errors,
			'bet' => +$cart->amount,
			'on_change' => $cart->on_change,
			'cart_id' => $cart->id,
			'cart_key' => $cart->key,
		];
	}

	/**
	 * Add item to cart
	 *
	 * @param int $id Outcome ID
	 * @return array
	 *
	 * @OA\Get(
	 *   path="/bet/item-add",
	 *   operationId="item-add",
	 *   tags={"Bet"},
	 *   @OA\Parameter(name="id", in="query", required=true, description="Outcome id", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="cart_id", in="query", required=true, description="Cart id", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="cart_key", in="query", required=true, description="Cart key", @OA\Schema(type="string")),
	 *   @OA\Response(response="200", description="Request sent",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(
	 *         @OA\Property(property="sport_id", type="number", description="Sport ID"),
	 *         @OA\Property(property="sport_image", type="string", description="Sport image"),
	 *         @OA\Property(property="game_id", type="number", description="Game ID"),
	 *         @OA\Property(property="game_title", type="string", description="Game title"),
	 *         @OA\Property(property="outcome_id", type="number", description="Outcome ID"),
	 *         @OA\Property(property="outcome_title", type="string", description="Outcome title"),
	 *         @OA\Property(property="market_id", type="number", description="Market ID"),
	 *         @OA\Property(property="market_title", type="string", description="Market title"),
	 *         @OA\Property(property="rate", type="number", description="Item rate"),
	 *         @OA\Property(property="cart_id", type="number", description="ID"),
	 *         @OA\Property(property="cart_key", type="string", description="Key"),
	 *       )
	 *     })
	 *   ),
	 *   @OA\Response(response="400", description="Bet item add errored",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseFailed"),
	 *     })
	 *   )
	 * )
	 */
	public function actionItemAdd(int $id) {
		$cart = self::getCurrentCart(false) ?? $this->createCart();

		$item = $cart->addItem($id);
		if (!$item || (!!$item && !empty($item->getErrors()))) {
			return [
				'success' => false,
				'message' => Yii::t('bets', 'Error while adding item to cart'),
			] + ((!!$item && !empty($item->getErrors())) ? [
				'errors' => $item->getErrors()
			] : []);
		}
		$gameLocales = EventGameLocaleModel::find()->andWhere(['game_id' => $item->game_id])->current()->limit(2)->all();
		$outcomeLocales = GameOutcomesLocaleModel::find()->andWhere(['outcome_id' => $item->outcome_id])->current()->limit(2)->all();
		$marketLocales = GameMarketsLocaleModel::find()->andWhere(['market_id' => $item->market_id])->current()->limit(2)->all();

		$participants = \array_values(ArrayHelper::map(EventParticipantsModel::find()->andWhere([
			'game_id' => $item->game_id,
		])->with([
			'participantLocale' => function (GameParticipantsLocaleQuery $query) {
				return $query->current();
			},
		])->asArray()->all(), 'id', function($item) {
			return [
				'id' => +$item['participant_id'],
				'name' => GameHelpers::getLocalization($item['participantLocale'])['name'] ?? '',
			];
		}));
		$participantReplacements = [];
		foreach($participants AS $k => $v) {
			$participantReplacements[$k + 1] = $v['name'];
		}
		return [
			'id' => +$item['id'],
			'sport_id' => +$item['sport_id'],
			'sport_image' => !empty($item->sport->image) ? Yii::getAlias("@web/frontend/uploads/games/{$item->sport->image}") : '',
			'game_id' => +$item['game_id'],
			'game_title' => GameHelpers::getLocalization($gameLocales)['title'] ?? '',
			'outcome_id' => +$item['outcome_id'],
			'outcome_title' => GameHelpers::replacingCodeWithName(GameHelpers::getLocalization($outcomeLocales)['title'] ?? '', $participantReplacements),
			'market_id' => +$item['market_id'],
			'market_title' => GameHelpers::getLocalization($marketLocales)['title'] ?? '',
			'rate' => +$item['rate'],
			'cart_id' => $cart->id,
			'cart_key' => $cart->key,
			'success' => true,
		];
	}

	/**
	 * Remove cart item
	 *
	 * @param int $id
	 * @return array
	 * @throws \Throwable
	 *
	 * @OA\Get(
	 *   path="/bet/item-remove",
	 *   operationId="item-remove",
	 *   tags={"Bet"},
	 *   @OA\Parameter(name="id", in="query", required=true, description="Cart item id", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="cart_id", in="query", required=true, description="Cart id", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="cart_key", in="query", required=true, description="Cart key", @OA\Schema(type="string")),
	 *   @OA\Response(response="200", description="Request sent",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *     })
	 *   ),
	 *   @OA\Response(response="400", description="Bet item remove errored",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseFailed"),
	 *     })
	 *   )
	 * )
	 */
	public function actionItemRemove(int $id) {
		$cart = self::getCurrentCart();
		$item = CartsItemsModel::find()->andWhere(['id' => $id, 'cart_id' => $cart->id])->one();
		if(!$item) {
			throw new NotFoundHttpException(Yii::t('games', 'Cart item not found'));
		}
		return ['success' => !!$item->delete()];
	}

	/**
	 * Place bet
	 *
	 * @param int $clean
	 * @param string $promocode
	 * @param int $type
	 * @return array
	 * @throws \Throwable
	 *
	 * @OA\Get(
	 *   path="/bet/place",
	 *   operationId="place",
	 *   tags={"Bet"},
	 *   security={{"bearer":{"authorized"}}},
	 *   @OA\Parameter(name="cart_id", in="query", required=true, description="Cart id", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="cart_key", in="query", required=true, description="Cart key", @OA\Schema(type="string")),
	 *   @OA\Parameter(name="clean", in="query", example="1", description="Clean after placing bet", @OA\Schema(type="integer", enum={0,1})),
	 *   @OA\Parameter(
	 *     name="type", in="query", example="1", @OA\Schema(type="integer", enum={0,1}),
	 *     description="Bet type. Single: 0; multi: 1;"
	 *   ),
	 *   @OA\Parameter(name="promocode", in="query", description="Additional promocode", @OA\Schema(type="string")),
	 *
	 *   @OA\Response(response="200", description="Request sent",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *     })
	 *   ),
	 *   @OA\Response(response="400", description="Bet place error",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseFailed"),
	 *     })
	 *   ),
	 *   @OA\Response(response="404", description="Cart not found",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseFailed"),
	 *     })
	 *   )
	 * )
	 */
	public function actionPlace(int $clean = 1, $promocode = '', int $type = 1) {
		try {
			$cart = self::getCurrentCart(true);
			if ($cart->amount == 0) {
				throw new BadRequestHttpException(Yii::t('games', 'Bid amount 0. Change the bid amount'));
			}
			$user = Yii::$app->user->identity;
			/** @var $user UserModel */
			if ($user->purse_amount < $cart->amount) {
				throw new BadRequestHttpException(Yii::t('user', 'There is not enough money in your account'));
			}
			$cart->placeBet($user, $type);
		} catch (HttpException $httpEX) {
			Yii::$app->response->setStatusCode($httpEX->statusCode);
			return [
				'success' => false,
				'message' => $httpEX->getMessage(),
			];
		} catch (\Throwable $ex) {
			Yii::$app->response->setStatusCode(400);
			return [
				'success' => false,
				'message' => $ex->getMessage(),
			];
		}
		if($clean) {
			$cart->delete();
			Yii::$app->response->cookies->remove('cart_id');
			Yii::$app->response->cookies->remove('key');
		}
		return ['success' => true];
	}

	/**
	 * Delete cart
	 *
	 * @return array
	 * @throws \Throwable
	 *
	 * @OA\Get(path="/bet/delete", operationId="delete", tags={"Bet"},
	 *   @OA\Parameter(name="cart_id", in="query", required=true, description="Cart id", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="cart_key", in="query", required=true, description="Cart key", @OA\Schema(type="string")),
	 *   @OA\Response(response="200", description="Request sent",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *     })
	 *   ),
	 *   @OA\Response(response="400", description="Delete errored",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseFailed"),
	 *     })
	 *   )
	 * )
	 */
	public function actionDelete() {
		$cart = self::getCurrentCart();
		return ['success' => !!$cart->delete()];
	}

	/**
	 * Get current cart item based on cookies
	 *
	 * @param bool $throwException
	 * @return CartsModel
	 */
	public static function getCurrentCart($throwException = true) {
		$cart_id = Yii::$app->request->get('cart_id', null);
		$key = Yii::$app->request->get('cart_key', null);
		if (empty($cart_id) || empty($key)) {
			if($throwException) {
				throw new NotFoundHttpException(Yii::t('games', 'Your cart is empty'));
			}
			return null;
		}
		return self::findCart(['id' => $cart_id, 'key' => $key], $throwException);
	}

	/**
	 * @param array $params
	 * @param bool $throwException
	 * @return CartsModel
	 */
	public static function findCart($params, $throwException = true) {
		$cart = CartsModel::find()->andWhere($params)->limit(1)->one();
		if(!$cart && $throwException) {
			throw new NotFoundHttpException(Yii::t('games', 'Your cart is empty'));
		}
		return $cart;
	}

	/**
	 * Create new cart
	 *
	 * @return CartsModel
	 */
	protected function createCart() {
		$cart = new CartsModel();
		$cart->scenario = CartsModel::SCENARIO_CREATE;
		$cart->amount = CartsModel::getMinimalBet();
		$cart->on_change = CartsModel::ON_CHANGE_AGREE;
		$cart->key = Yii::$app->security->generateRandomString(64);
		if($cart->save()) {
			return $cart;
		}
		throw new ServerErrorHttpException(Yii::t('bets', 'Error while cart initialization'));
	}

	/**
	 * User bets history
	 *
	 * @param int $page
	 * @return array
	 *
	 * @OA\Get(
	 *   path="/bet/index",
	 *   operationId="index",
	 *   tags={"Bet"},
	 *   @OA\Parameter(name="page", in="query", required=false, description="Page number", @OA\Schema(type="integer")),
	 *   @OA\Response(
	 *     response="200",
	 *     description="Events list",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(
	 *         @OA\Property(property="total", type="number", description="Total amount"),
	 *         @OA\Property(property="statuses", type="array", description="Statuses list",
     *           @OA\Items(type="string")
	 *         ),
	 *         @OA\Property(property="bets", type="array", description="Events list",
	 *           @OA\Items(
	 *             description="Bet info",
	 *             @OA\Property(property="id", type="number", description="ID"),
	 *             @OA\Property(property="amount_bet", type="number", description="Bet amount"),
	 *             @OA\Property(property="amount_win", type="number", description="Victory amount"),
	 *             @OA\Property(property="status", type="number", description="Status ID"),
	 *             @OA\Property(property="created_at", type="number", description="Created at"),
	 *             @OA\Property(property="market", type="string", description="Market name"),
	 *             @OA\Property(property="outcome", type="string", description="Outcome name"),
	 *             @OA\Property(property="participants", type="array", description="Participants list",
	 *               @OA\Items(
	 *                 description="Participant item",
	 *                 @OA\Property(property="name", type="string", description="Name"),
	 *               )
	 *             )
	 *           )
	 *         )
	 *       )
	 *     })
	 *   )
	 * )
	 */
	public function actionIndex($page = 1) {
		$bets = BetsModel::find()->andWhere(['user_id' => Yii::$app->user->id])->with([
			'items' => function(BetsItemsQuery $query) {
				$query->with([
					'eventParticipants' => function(EventParticipantsQuery $query) {
						$query->innerJoinWith([
							'participantLocale locale_en' => function(GameParticipantsLocaleQuery $query) {
								return $query->andOnCondition(['locale_en.lang_code' => 'en']);
							},
						], false)->addSelect([
							EventParticipantsModel::tableName() . '.*',
							'name' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.name, locale_en.name)') : 'locale_en.name',
						]);
						if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
							$query->joinWith([
								'participantLocale locale_tr' => function(GameParticipantsLocaleQuery $query) {
									return $query->andOnCondition([
										'locale_tr.lang_code' => Yii::$app->language,
										'locale_tr.is_active' => 1,
									]);
								},
							], false);
						}
					}
				])->addSelect([
					BetsItemsModel::tableName() . '.*',
					'market_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(market_locale_tr.title, market_locale_en.title)') : 'market_locale_en.title',
					'outcome_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(outcome_locale_tr.title, outcome_locale_en.title)') : 'outcome_locale_en.title',
					'tournament_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(tournament_locale_tr.title, tournament_locale_en.title)') : 'tournament_locale_en.title',
					'game_starts_at' => EventGameModel::tableName() . '.starts_at'
				])->innerJoinWith([
					'game' => function(EventGameQuery $query) {
						$query->innerJoinWith([
							'tournamentLocales tournament_locale_en' => function(EventTournamentLocaleQuery $query) {
								return $query->andOnCondition(['tournament_locale_en.lang_code' => 'en']);
							},
						]);
						if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
							$query->joinWith([
								'tournamentLocales tournament_locale_tr' => function(EventTournamentLocaleQuery $query) {
									return $query->andOnCondition([
										'tournament_locale_tr.lang_code' => Yii::$app->language,
										'tournament_locale_tr.is_active' => 1,
									]);
								},
							], false);
						}
					},
					'marketLocales market_locale_en' => function(GameMarketsLocaleQuery $query) {
						return $query->andOnCondition(['market_locale_en.lang_code' => 'en']);
					},
					'outcomeLocales outcome_locale_en' => function(GameOutcomesLocaleQuery $query) {
						return $query->andOnCondition(['outcome_locale_en.lang_code' => 'en']);
					},
				], false);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'marketLocales market_locale_tr' => function(GameMarketsLocaleQuery $query) {
							return $query->andOnCondition([
								'market_locale_tr.lang_code' => Yii::$app->language,
								'market_locale_tr.is_active' => 1,
							]);
						},
						'outcomeLocales outcome_locale_tr' => function(GameOutcomesLocaleQuery $query) {
							return $query->andOnCondition([
								'outcome_locale_tr.lang_code' => Yii::$app->language,
								'outcome_locale_tr.is_active' => 1,
							])->via('outcome', function(EventOutcomesQuery $query) {
								$query->alias('outcome_tr');
							});
						},
					], false);
				}
			},
		])->orderBy(['id' => SORT_DESC])->limit(10)->offset(($page - 1) * 10)->asArray()->all();
		return [
			'total' => BetsModel::find()->andWhere(['user_id' => Yii::$app->user->id])->count(),
			'bets' => \array_values(ArrayHelper::map($bets, 'id', function($model) {
				return [
					'id' => +$model['id'],
					'amount_bet' => +$model['amount_bet'],
					'amount_win' => +$model['amount_win'],
					'status' => +$model['status'],
					'created_at' => +$model['created_at'],
					'items' => ArrayHelper::getColumn($model['items'], function ($item) {
						$participants = \array_values(ArrayHelper::map($item['eventParticipants'], 'id', function($item) {
							return [
								'id' => +$item['participant_id'],
								'name' => $item['name'] ?? '',
							];
						}));
						$participantReplacements = [];
						foreach($participants AS $k => $v) {
							$participantReplacements[$k + 1] = $v['name'];
						}
						return [
							'id' => +$item['id'],
							'rate' => +$item['rate'],
							'status' => +$item['status'],
							'starts_at' => +$item['game_starts_at'],
							'tournament' => $item['tournament_title'] ?? '',
							'participants' => $participants,
							'market' => $item['market_title'] ?? '',
							'outcome' => GameHelpers::replacingCodeWithName($item['outcome_title'], $participantReplacements),
						];
					}, false),

				];
			})),
			'statuses' => BetsSearch::statuses(),
		];
	}
}

<?php

namespace frontapi\controllers;

use common\modules\user\controllers\AuthController as BaseAuthController;
use common\modules\user\forms\LoginForm;
use common\modules\user\forms\RecoveryForm;
use common\modules\user\forms\RegistrationForm;
use common\modules\user\forms\ResendForm;
use common\modules\user\models\TokenModel;
use common\modules\user\models\UserApplicationModel;
use common\modules\user\models\UserModel;
use common\modules\user\traits\UserEventTrait;
use Throwable;
use Yii;
use yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\db\StaleObjectException;
use yii\filters\auth\HttpBearerAuth;
use yii\web\NotFoundHttpException;

/**
 * @property-read UserApplicationModel $accessToken
 */
class AuthController extends BaseController {

	use UserEventTrait;

	public function behaviors () {
		$behaviors = parent::behaviors();
		$behaviors['authenticator']['except'] = [
			'login', 'registration', 'logout',
			'renew', 'request', 'reset', 'resend'
		];
		$behaviors['authenticator']['optional'] = [
			'approve'
		];
		return $behaviors;
	}

	/**
	 * Get application token
	 *
	 * @return array
	 *
	 * @OA\Post(
	 *   path="/auth/login",
	 *   operationId="login",
	 *   tags={"Authorization"},
	 *   @OA\RequestBody(
	 *     description="User data",
	 *     @OA\MediaType(
	 *       mediaType="application/x-www-form-urlencoded",
	 *       @OA\Schema(
	 *         required={"login", "password"},
	 *         @OA\Property(property="login", type="string", description="User email or phone or accountID"),
	 *         @OA\Property(property="password", type="string", format="password", description="User password"),
	 *         @OA\Property(property="app_type", type="string", description="Application type, 1 for android, 2 for ios"),
	 *         @OA\Property(property="app_name", type="string", description="Application name")
	 *       )
	 *     ),
	 *   ),
	 *   @OA\Response(
	 *     response="200",
	 *     description="Request sent",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(ref="#/components/schemas/responseUserInfo"),
	 *       @OA\Schema(ref="#/components/schemas/responseUserToken"),
	 *     })
	 *   )
	 * )
	 */
	public function actionLogin () {
		/* @var $model LoginForm */
		$model = Yii::createObject(LoginForm::class);
		$event = $this->getFormEvent($model);
		$type = Yii::$app->request->post('app_type', null);
		if ($model->load(Yii::$app->getRequest()->post(), '') && ($key = $model->loginApplication(
			Yii::$app->request->post('app_name', null),
			(!is_null($type) && in_array($type, UserApplicationModel::TYPES_MOBILE)) ? $type : null
		))) {
			$this->trigger(BaseAuthController::EVENT_AFTER_LOGIN, $event);
			return [
				'token' => $key,
			] + $this->getUserDisplayInfo($model->getUserModel());
		}
		return [
			'success' => false,
			'token' => false,
			'errors' => $model->errors,
		];
	}

	/**
	 * Create user account
	 *
	 * @return array
	 *
	 * @OA\Post(
	 *   path="/auth/registration",
	 *   operationId="registration",
	 *   tags={"Authorization"},
	 *   @OA\RequestBody(
	 *     description="User data",
	 *     @OA\MediaType(
	 *       mediaType="application/x-www-form-urlencoded",
	 *       @OA\Schema(
	 *         required={"email", "password", "currency", "agree"},
	 *         @OA\Property(property="email", type="string", format="email", description="Email"),
	 *         @OA\Property(property="password", type="string", format="password", description="Password"),
	 *         @OA\Property(property="currency", type="string", format="string", description="User currency", enum={"USD", "RUR"}),
	 *         @OA\Property(property="agree", type="boolean", description="Accepted agreements"),
	 *         @OA\Property(property="app_type", type="string", description="Application type, 1 for android, 2 for ios"),
	 *         @OA\Property(property="app_name", type="string", description="Application name")
	 *       )
	 *     ),
	 *   ),
	 *   @OA\Response(
	 *     response="200",
	 *     description="Request sent",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(ref="#/components/schemas/responseUserInfo"),
	 *       @OA\Schema(ref="#/components/schemas/responseUserToken"),
	 *     })
	 *   )
	 * )
	 */
	public function actionRegistration () {
		/* @var $model RegistrationForm */
		$model = Yii::createObject(RegistrationForm::class);
		$event = $this->getFormEvent($model);
		$this->trigger(BaseAuthController::EVENT_BEFORE_REGISTER, $event);
		if ($model->load(Yii::$app->request->post(), '') && $model->register()) {
			$this->trigger(BaseAuthController::EVENT_AFTER_REGISTER, $event);
			$user = $model->getUser();
			$keyModel = Yii::createObject(UserApplicationModel::class);
			$type = Yii::$app->request->post('app_type', null);
			$keyModel->setAttributes([
				'user_id' => $user->id,
				'app_name' => Yii::$app->request->post('app_name', null),
				'app_type' => (!is_null($type) && in_array($type, UserApplicationModel::TYPES_MOBILE)) ? $type : null,
			]);
			return [
				'success' => true,
				'token' => $keyModel->save() ? $keyModel->token : false
			] + $this->getUserDisplayInfo($user);
		}
		return [
			'success' => false,
			'errors' => $model->errors,
		];
	}

	/**
	 * Confirms user's account. If confirmation was successful logs the user and shows success message. Otherwise
	 * shows error message.
	 *
	 * @param int $id
	 * @param string $code
	 *
	 * @return array
	 * @throws NotFoundHttpException
	 * @throws \Throwable
	 * @throws \yii\base\InvalidConfigException
	 * @throws \yii\db\StaleObjectException
	 *
	 * @OA\Post(
	 *   path="/auth/approve?id={id}&code={code}",
	 *   operationId="approve",
	 *   tags={"Authorization"},
	 *   @OA\Parameter(
	 *     name="id", in="path", required=true,
	 *     description="Request id",
	 *     @OA\Schema(type="integer"),
	 *   ),
	 *   @OA\Parameter(
	 *     name="code", in="path", required=true,
	 *     description="Request code",
	 *     @OA\Schema(type="string"),
	 *   ),
	 *   @OA\RequestBody(
	 *     @OA\MediaType(
	 *       mediaType="application/x-www-form-urlencoded",
	 *       @OA\Schema(
	 *         @OA\Property(property="app_type", type="string", description="Application type, 1 for android, 2 for ios"),
	 *         @OA\Property(property="app_name", type="string", description="Application name")
	 *       )
	 *     ),
	 *   ),
	 *   @OA\Response(
	 *     response="200", description="Request sent",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(ref="#/components/schemas/responseUserInfo"),
	 *       @OA\Schema(ref="#/components/schemas/responseUserToken"),
	 *     })
	 *   )
	 * )
	 */
	public function actionApprove($id, $code) {
		$user = $this->getUserModel($id);
		$event = $this->getUserEvent($user);
		$this->trigger(BaseAuthController::EVENT_BEFORE_APPROVE, $event);
		$accessToken = false;
		$token = $this->getTokenModel([
			'code' => $code,
			'user_id' => $user->id,
			'type' => TokenModel::TYPE_APPROVE,
		]);
		if (!$token->isExpired) {
			$token->delete();
			if (($success = $user->confirm())) {
				if(Yii::$app->user->isGuest) {
					$type = Yii::$app->request->post('app_type', null);
					$accessToken = $user->createAccessToken (
						Yii::$app->request->post('app_name', null),
						(!is_null($type) && in_array($type, UserApplicationModel::TYPES_MOBILE)) ? $type : null
					);
				}
				$message = Yii::t('user', 'Thank you, registration is now complete.');
			} else {
				$message = Yii::t('user', 'Something went wrong and your account has not been confirmed.');
			}
		} else {
			$success = false;
			$message = Yii::t('user', 'The confirmation link is invalid or expired. Please try requesting a new one.');
		}
		$this->trigger(BaseAuthController::EVENT_AFTER_APPROVE, $event);
		return [
			'success' => $success,
			'message' => $message,
		] + ($accessToken ? ([
			'token' => $accessToken->token
		] + $this->getUserDisplayInfo($user)) : []);
	}

	/**
	 * Request password recovery.
	 *
	 * @return array
	 * @throws ExitException
	 * @throws InvalidConfigException
	 *
	 * @OA\Post(
	 *   path="/auth/request",
	 *   operationId="request",
	 *   tags={"Authorization"},
	 *   @OA\RequestBody(
	 *     description="User data",
	 *     @OA\MediaType(
	 *       mediaType="application/x-www-form-urlencoded",
	 *       @OA\Schema(
	 *         required={"email"},
	 *         @OA\Property(property="email", type="string", format="email", description="User email")
	 *       )
	 *     ),
	 *   ),
	 *   @OA\Response(
	 *     response="200", description="Request sent",
	 *     @OA\JsonContent(ref="#/components/schemas/responseProcessed")
	 *   )
	 * )
	 */
	public function actionRequest() {
		/** @var RecoveryForm $model */
		$model = Yii::createObject([
			'class' => RecoveryForm::class,
			'scenario' => 'request',
		]);
		$event = $this->getFormEvent($model);
		$this->trigger(BaseAuthController::EVENT_BEFORE_REQUEST, $event);
		if ($model->load(Yii::$app->request->post(), '') && $model->sendRecoveryMessage()) {
			$this->trigger(BaseAuthController::EVENT_AFTER_REQUEST, $event);
			return ['success' => true];
		}
		return [
			'success' => false,
		];
	}

	/**
	 * Reset password.
	 *
	 * @param int $id
	 * @param string $code
	 *
	 * @return array
	 * @throws Throwable
	 * @throws ExitException
	 * @throws InvalidConfigException
	 * @throws StaleObjectException
	 *
	 * @OA\Post(
	 *   path="/auth/reset?id={id}&code={code}",
	 *   operationId="reset",
	 *   tags={"Authorization"},
	 *   @OA\Parameter(
	 *     name="id", in="path", required=true,
	 *     description="Request id",
	 *     @OA\Schema(type="integer"),
	 *   ),
	 *   @OA\Parameter(
	 *     name="code", in="path", required=true,
	 *     description="Request code",
	 *     @OA\Schema(type="string"),
	 *   ),
	 *   @OA\RequestBody(
	 *     description="User data",
	 *     @OA\MediaType(
	 *       mediaType="application/x-www-form-urlencoded",
	 *       @OA\Schema(
	 *         required={"password"},
	 *         @OA\Property(property="password", type="string", format="password", description="New user password")
	 *       )
	 *     ),
	 *   ),
	 *   @OA\Response(
	 *     response="200", description="Request sent",
	 *     @OA\JsonContent(ref="#/components/schemas/responseProcessed")
	 *   )
	 * )
	 */
	public function actionReset(int $id, string $code) {
		/** @var TokenModel $token */
		$token = $this->getTokenModel(['user_id' => $id, 'code' => $code, 'type' => TokenModel::TYPE_RECOVERY]);
		$event = $this->getResetPasswordEvent($token);
		$this->trigger(BaseAuthController::EVENT_BEFORE_TOKEN_VALIDATE, $event);
		if ($token === null || $token->isExpired || $token->user === null) {
			$this->trigger(BaseAuthController::EVENT_AFTER_TOKEN_VALIDATE, $event);
			return [
				'success' => false,
				'message' => Yii::t('user', 'Recovery link is invalid or expired. Please try requesting a new one.')
			];
		}
		/** @var RecoveryForm $model */
		$model = Yii::createObject([
			'class' => RecoveryForm::class,
			'scenario' => 'reset',
		]);
		$event->setForm($model);
		$this->trigger(BaseAuthController::EVENT_BEFORE_RESET, $event);
		if ($model->load(Yii::$app->getRequest()->post(), '') && $model->resetPassword($token)) {
			$this->trigger(BaseAuthController::EVENT_AFTER_RESET, $event);
			return ['success' => true];
		}
		return [
			'success' => false,
			'errors' => $model->errors
		];
	}

	/**
	 * Request new confirmation token.
	 *
	 * @return array
	 * @throws ExitException
	 * @throws InvalidConfigException
	 *
	 * @OA\Post(
	 *   path="/auth/resend",
	 *   operationId="resend",
	 *   tags={"Authorization"},
	 *   @OA\RequestBody(
	 *     description="User data",
	 *     @OA\MediaType(
	 *       mediaType="application/x-www-form-urlencoded",
	 *       @OA\Schema(
	 *         required={"email"},
	 *         @OA\Property(property="email", type="string", format="email", description="User email")
	 *       )
	 *     ),
	 *   ),
	 *   @OA\Response(
	 *     response="200", description="Request sent",
	 *     @OA\JsonContent(ref="#/components/schemas/responseProcessed")
	 *   )
	 * )
	 */
	public function actionResend() {
		/** @var ResendForm $model */
		$model = Yii::createObject(ResendForm::class);
		$event = $this->getFormEvent($model);
		$this->trigger(BaseAuthController::EVENT_BEFORE_RESEND, $event);
		if ($model->load(Yii::$app->request->post(), '') && $model->resend()) {
			$this->trigger(BaseAuthController::EVENT_AFTER_RESEND, $event);
			return ['success' => true];
		}
		return [
			'success' => false,
			'errors' => $model->errors
		];
	}

	/**
	 * Renew access token expire timeout
	 *
	 * @OA\Get(
	 *   path="/auth/renew",
	 *   operationId="renew",
	 *   tags={"Authorization"},
	 *   security={{"bearer":{"authorized"}}},
	 *   @OA\Response(
	 *     response="200", description="Key renewed",
	 *     @OA\JsonContent(ref="#/components/schemas/responseProcessed")
	 *   ),
	 * )
	 */
	public function actionRenew () {
		return [
			'success' => (($token = $this->getAccessToken()) && $token->renewTokenTime())
		];
	}

	/**
	 * Destroy access token
	 *
	 * @OA\Get(
	 *   path="/auth/logout",
	 *   operationId="logout",
	 *   tags={"Authorization"},
	 *   security={{"bearer":{"authorized"}}},
	 *   @OA\Response(
	 *     response="200", description="User logged out",
	 *     @OA\JsonContent(ref="#/components/schemas/responseProcessed")
	 *   ),
	 * )
	 */
	public function actionLogout () {
		return [
			'success' => (($token = $this->getAccessToken()) && $token->delete())
		];
	}

	/**
	 * @param $params
	 * @return UserModel
	 * @throws NotFoundHttpException
	 */
	protected function getUserModel($params) : UserModel {
		$user = UserModel::find()->andWhere($params)->one();
		if(!$user) {
			throw new NotFoundHttpException();
		}
		return $user;
	}

	/**
	 * @return UserApplicationModel|null
	 */
	protected function getAccessToken () {
		$auth = new HttpBearerAuth();
		$authHeader = Yii::$app->request->getHeaders()->get($auth->header);
		if ( $authHeader !== null && $auth->pattern !== null && preg_match($auth->pattern, $authHeader, $matches) ) {
			return UserApplicationModel::find()->andWhere(['token' => $matches[1]])->limit(1)->one();
		}
		return null;
	}

	/**
	 * @param $params
	 * @return TokenModel
	 * @throws NotFoundHttpException
	 */
	protected function getTokenModel($params) : TokenModel {
		$token = TokenModel::find()->andWhere($params)->one();
		if(!$token) {
			throw new NotFoundHttpException();
		}
		return $token;
	}
}
<?php

namespace frontapi\controllers;

use common\modules\games\helpers\GameHelpers;
use common\modules\games\models\BetsItemsModel;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\EventParticipantsModel;
use common\modules\games\models\EventTournamentModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesModel;
use common\modules\games\models\GameParticipantsModel;
use common\modules\games\models\GameSportsModel;
use common\modules\games\queries\EventGameLocaleQuery;
use common\modules\games\queries\EventGameQuery;
use common\modules\games\queries\EventOutcomesQuery;
use common\modules\games\queries\EventParticipantsQuery;
use common\modules\games\queries\EventTournamentLocaleQuery;
use common\modules\games\queries\GameMarketsGroupsAdditionalQuery;
use common\modules\games\queries\GameMarketsGroupsLocaleQuery;
use common\modules\games\queries\GameMarketsGroupsQuery;
use common\modules\games\queries\GameMarketsLocaleQuery;
use common\modules\games\queries\GameMarketsQuery;
use common\modules\games\queries\GameOutcomesLocaleQuery;
use common\modules\games\queries\GameOutcomesQuery;
use common\modules\games\queries\GameParticipantsLocaleQuery;
use common\modules\games\queries\GameSportsLocaleQuery;
use common\modules\games\queries\GameSportsQuery;
use common\modules\games\search\EventGameSearch;
use Yii;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

class GameController extends BaseController {

	/** @inheritdoc */
	public function behaviors () {
		$behaviors = parent::behaviors();
		$behaviors['authenticator']['optional'] = [
			'sports', 'participants', 'events', 'tournaments', 'event'
		];
		return $behaviors;
	}

	/**
	 * Get list of sports
	 *
	 * @OA\Get(
	 *   path="/game/sports",
	 *   operationId="sports",
	 *   tags={"Games"},
	 *   @OA\Parameter(name="live", in="query", required=false, description="Select only live", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="esport", in="query", required=false, description="Select only esport", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="start_from", in="query", required=false, description="Time filter start", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="start_to", in="query", required=false, description="Time filter end", @OA\Schema(type="integer")),
	 *   @OA\Response(
	 *     response="200",
	 *     description="Sports list",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(
	 *         @OA\Property(property="results", type="array", description="Sports list",
	 *           @OA\Items(
	 *             description="Sport info",
	 *             @OA\Property(property="id", type="number", description="ID"),
	 *             @OA\Property(property="image", type="string", description="Image"),
	 *             @OA\Property(property="title", type="string", description="Title"),
	 *             @OA\Property(property="description", type="string", description="Description"),
	 *             @OA\Property(property="tournaments_count", type="number", description="Tournaments count"),
	 *           )
	 *         ),
	 *       )
	 *     })
	 *   )
	 * )
	 *
	 * @param int|null $live
	 * @param int|null $esport
	 * @param int|null $starts_from
	 * @param int|null $starts_to
	 *
	 * @return array
	 */
	public function actionSports(int $live = null, int $esport = null, int $starts_from = null, int $starts_to = null) {
		$query = GameSportsModel::find()->active()->addSelect([
			GameSportsModel::tableName() . '.*',
			'sport_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(sport_locale_tr.title, sport_locale_en.title)') : 'sport_locale_en.title',
			'sport_description' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(sport_locale_tr.description, sport_locale_en.description)') : 'sport_locale_en.description',
			'tournaments_count' => EventTournamentModel::find()->visible()->andFilterWhere([
				EventTournamentModel::tableName() . '.sports_id' => new Expression(GameSportsModel::tableName() . '.id')
			])->innerJoinWith(['eventGames' => function (EventGameQuery $query) use ($live, $starts_from, $starts_to) {
				return $query->active()->live($live)->andWhere([
					'>', 'outcomes_count', 0
				])->andFilterWhere([
					'>=', EventGameModel::tableName() . '.starts_at', $starts_from
				])->andFilterWhere([
					'<=', EventGameModel::tableName() . '.starts_at', $starts_to
				]);
			}])->select('COUNT(  DISTINCT `event_tournament`.`id` )')
		])->innerJoinWith([
			'gameSportsLocales sport_locale_en' => function (GameSportsLocaleQuery $query) {
				return $query->andOnCondition(['sport_locale_en.lang_code' => 'en']);
			},
		], false)->asArray()->orderBy([
			GameSportsModel::tableName() . '.sort_order' => SORT_ASC,
			GameSportsModel::tableName() . '.id' => SORT_ASC,
		])->andFilterWhere([GameSportsModel::tableName() . '.id' => !!$esport ? Yii::$app->params['esportID'] : null]);

		if (\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$query->joinWith([
				'gameSportsLocales sport_locale_tr' => function (GameSportsLocaleQuery $query) {
					return $query->andOnCondition([
						'sport_locale_tr.lang_code' => Yii::$app->language,
						'sport_locale_tr.is_active' => 1,
					]);
				},
			], false);
		}

		return ['success' => true, 'results' => ArrayHelper::map($query->all(), 'id', function($item) {
			return [
				'id' => +$item['id'],
				'image' => !empty($item['image']) ? Yii::getAlias("@web/frontapi/uploads/games/{$item['image']}") : '',
				'title' => $item['sport_title'] ?? '',
				'description' => $item['sport_description'] ?? '',
				'tournaments_count' => +$item['tournaments_count'],
			];
		})];
	}

	/**
	 * Get list of participants
	 *
	 * @param int|null $sport
	 *
	 * @OA\Get(
	 *   path="/game/participants",
	 *   operationId="participants",
	 *   tags={"Games"},
	 *   @OA\Parameter(name="sport", in="path", required=false, description="Sport id", @OA\Schema(type="integer")),
	 *   @OA\Response(
	 *     response="200",
	 *     description="Participants list",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(
	 *         @OA\Property(property="results", type="array", description="Participants list",
	 *           @OA\Items(
	 *             description="Participant info",
	 *             @OA\Property(property="id", type="number", description="ID"),
	 *             @OA\Property(property="sports_id", type="number", description="Sport id"),
	 *             @OA\Property(property="name", type="string", description="Name"),
	 *             @OA\Property(property="image", type="string", description="Participant logo"),
	 *           )
	 *         ),
	 *       )
	 *     })
	 *   )
	 * )
	 * @return array
	 */
	public function actionParticipants($sport = null) {
		$query = GameParticipantsModel::find()->andFilterWhere(['sports_id' => $sport])->innerJoinWith([
			'gameParticipantsLocales locale_en' => function(GameParticipantsLocaleQuery $query) {
				return $query->andOnCondition(['locale_en.lang_code' => 'en']);
			},
		], false)->addSelect([
			GameParticipantsModel::tableName() . '.*',
			'name' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.name, locale_en.name)') : 'locale_en.name',
		])->asArray();
		if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$query->joinWith([
				'gameParticipantsLocales locale_tr' => function(GameParticipantsLocaleQuery $query) {
					return $query->andOnCondition([
						'locale_tr.lang_code' => Yii::$app->language,
						'locale_tr.is_active' => 1,
					]);
				},
			], false);
		}
		return ['success' => true, 'results' => ArrayHelper::map($query->limit(200)->all(), 'id', function($item) {
			return [
				'id' => +$item['id'],
				'sports_id' => +$item['sports_id'],
				'name' => $item['name'] ?? '',
				'image' => !empty($item['image']) ? Yii::getAlias("@web/frontapi/uploads/games/{$item['image']}") : '',
			];
		})];
	}

	/**
	 * Get list of events
	 *
	 * @OA\Get(
	 *   path="/game/events",
	 *   operationId="events",
	 *   tags={"Games"},
	 *   @OA\Parameter(name="live", in="query", required=false, description="Select only live", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="ids[]", in="query", required=false, description="Event id", @OA\Schema(type="array", @OA\Items(
	 *       description="Participant item",
	 *   ))),
	 *   @OA\Parameter(name="tournaments[]", in="query", required=false, description="Tournaments id", @OA\Schema(type="array", @OA\Items(
	 *       description="Tournament item",
	 *   ))),
	 *   @OA\Parameter(name="page", in="query", required=false, description="Page number", @OA\Schema(type="integer")),
	 *
	 *   @OA\Parameter(name="sports_id", in="query", required=false, description="Sport id", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="tournament_id", in="query", required=false, description="Tournament id", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="starts_from", in="query", required=false, description="Event starts time greater than", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="starts_to", in="query", required=false, description="Event starts time less than", @OA\Schema(type="integer")),
	 *   @OA\Response(
	 *     response="200",
	 *     description="Events list",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(
     *         @OA\Property(property="total", type="number", description="Total amount"),
	 *         @OA\Property(property="results", type="array", description="Events list",
	 *           @OA\Items(
	 *             description="Event info",
	 *             @OA\Property(property="id", type="number", description="ID"),
	 *             @OA\Property(property="title", type="string", description="Title"),
	 *             @OA\Property(property="sports_id", type="number", description="Sport id"),
	 *             @OA\Property(property="sports_image", type="string", description="Sport image"),
	 *             @OA\Property(property="sports_title", type="string", description="Sport title"),
	 *             @OA\Property(property="tournament_id", type="number", description="Tournament id"),
	 *             @OA\Property(property="tournament_title", type="string", description="Tournament title"),
	 *             @OA\Property(property="outcomes_count", type="number", description="Outcomes count"),
	 *             @OA\Property(property="starts_date", type="date", description="Start date"),
	 *             @OA\Property(property="starts_time", type="time", description="Start time"),
	 *             @OA\Property(property="is_finished", type="boolean", description="Is finished"),
	 *             @OA\Property(property="is_live", type="boolean", description="Is live"),
	 *             @OA\Property(property="participants", type="array", description="Participants list", @OA\Items(
	 *                 description="Participant item",
	 *                 @OA\Property(property="id", type="number", description="ID"),
	 *                 @OA\Property(property="label", type="string", description="Label"),
	 *                 @OA\Property(property="image", type="string", description="Participant logo"),
	 *               )
	 *             ),
	 *             @OA\Property(property="groups", type="array", description="Groups list", @OA\Items(
	 *                 description="Group item",
	 *                 @OA\Property(property="id", type="number", description="ID"),
	 *                 @OA\Property(property="label", type="string", description="Label"),
	 *               )
	 *             ),
	 *             @OA\Property(property="outcomes", type="array", description="Outcomes list", @OA\Items(
	 *                 description="Outcome item",
	 *                 @OA\Property(property="id", type="number", description="ID"),
	 *                 @OA\Property(property="gid", type="number", description="Group ID"),
	 *                 @OA\Property(property="label", type="string", description="Label"),
	 *                 @OA\Property(property="rate", type="number", description="Rate"),
	 *               )
	 *             )
	 *           )
	 *         )
	 *       )
	 *     })
	 *   )
	 * )
	 *
	 * @param bool $live
	 * @param array $ids
	 * @param array $tournaments
	 * @param int $page
	 * @return array
	 */
	public function actionEvents(bool $live = false, array $ids = null, array $tournaments = null, int $page = 1) {
		$query = Yii::createObject([
			'class' => EventGameSearch::class,
			'scenario' => EventGameSearch::SCENARIO_SEARCH
		])->search(Yii::$app->request->get())->query;
		/** @var $query EventGameQuery */
		$query->active()->live($live)->andFilterWhere([
			EventGameModel::tableName() . '.id' => $ids,
			EventGameModel::tableName() . '.tournament_id' => $tournaments,
		])->andWhere([
			'>', EventGameModel::tableName() . '.outcomes_count', 0
		])->groupBy(EventGameModel::tableName() . '.id');
		$countQuery = clone $query;
		$query->innerJoinWith([
			'sports',
			'sportsLocales sports_locales_en' => function(GameSportsLocaleQuery $query) {
				return $query->andOnCondition(['sports_locales_en.lang_code' => 'en']);
			},
			'eventGameLocales event_locales_en' => function(EventGameLocaleQuery $query) {
				return $query->andOnCondition(['event_locales_en.lang_code' => 'en']);
			},
			'tournamentLocales tournament_locales_en' => function(EventTournamentLocaleQuery $query) {
				return $query->andOnCondition(['tournament_locales_en.lang_code' => 'en']);
			},
		], false)->with([
			'eventParticipants' => function(EventParticipantsQuery $query) {
				$query->innerJoinWith([
					'participant',
					'participantLocale locale_en' => function(GameParticipantsLocaleQuery $query) {
						return $query->andOnCondition([
							'locale_en.lang_code' => 'en',
						]);
					},
				], false)->addSelect([
					EventParticipantsModel::tableName() . '.*',
					GameParticipantsModel::tableName() . '.image',
					'name' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.name, locale_en.name)') : 'locale_en.name',
				]);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'participantLocale locale_tr' => function(GameParticipantsLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				}
			},
			'eventOutcomes' => function(EventOutcomesQuery $query) {
				return $query->innerJoinWith([
					'outcome' => function (GameOutcomesQuery $query) {
						return $query->active();
					},
					'market' => function (GameMarketsQuery $query) {
						return $query->active();
					}
				], false)->andWhere([
					GameOutcomesModel::tableName() . '.is_main' => 1
				])->select([
					EventOutcomesModel::tableName() . '.id',
					EventOutcomesModel::tableName() . '.game_id',
					EventOutcomesModel::tableName() . '.outcome_id',
					GameOutcomesModel::tableName() . '.short_title',
					EventOutcomesModel::tableName() . '.rate',

					EventOutcomesModel::tableName() . '.is_enabled',
					EventOutcomesModel::tableName() . '.is_hidden',
					EventOutcomesModel::tableName() . '.is_finished',
					EventOutcomesModel::tableName() . '.is_banned',
					'outcome_enabled' => GameOutcomesModel::tableName() . '.is_enabled',
				])->orderBy([
					GameMarketsModel::tableName() . '.sort_order' => SORT_ASC,
					GameOutcomesModel::tableName() . '.market_id' => SORT_ASC,
					GameOutcomesModel::tableName() . '.sort_order' => SORT_ASC,
					GameOutcomesModel::tableName() . '.id' => SORT_ASC,
				]);
			},
		])->addSelect([
			EventGameModel::tableName() . '.*',
			'bets_count' => BetsItemsModel::find()->select([
				'count' => new Expression('COUNT(*)')
			])->andWhere('{{bets_items}}.`game_id` = {{event_game}}.`id`'),
			'sport_image' => GameSportsModel::tableName() . '.image',
			'sport_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ?
				new Expression('COALESCE(sports_locales_tr.title, sports_locales_en.title)') : 'sports_locales_en.title',
			'event_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ?
				new Expression('COALESCE(event_locales_tr.title, event_locales_en.title)') : 'event_locales_en.title',
			'tournament_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ?
				new Expression('COALESCE(tournament_locales_tr.title, tournament_locales_en.title)') : 'tournament_locales_en.title',
			'tournament_description' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ?
				new Expression('COALESCE(tournament_locales_tr.description, tournament_locales_en.description)') : 'tournament_locales_en.description',
		])->orderBy([
			'sort_order' => SORT_ASC,
			'id' => SORT_ASC,
		])->asArray()->limit(50)->offset(($page - 1) * 50);

		if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$query->joinWith([
				'sportsLocales sports_locales_tr' => function(GameSportsLocaleQuery $query) {
					return $query->andOnCondition([
						'sports_locales_tr.lang_code' => Yii::$app->language,
						'sports_locales_tr.is_active' => 1,
					]);
				},
				'eventGameLocales event_locales_tr' => function(EventGameLocaleQuery $query) {
					return $query->andOnCondition([
						'event_locales_tr.lang_code' => Yii::$app->language,
						'event_locales_tr.is_active' => 1,
					]);
				},
				'tournamentLocales tournament_locales_tr' => function(EventTournamentLocaleQuery $query) {
					return $query->andOnCondition([
						'tournament_locales_tr.lang_code' => Yii::$app->language,
						'tournament_locales_tr.is_active' => 1,
					]);
				},
			], false);
		}

		$items = $query->all();
		return [
			'success' => true,
			'total' => +$countQuery->count(),
			'results' => \array_values(ArrayHelper::map($items, 'id', function($model) {
				return [
					'id' => +$model['id'],
					'title' => $model['event_title'] ?? '',
					'sports_id' => +$model['sports_id'],
					'sports_image' => !empty($model['sport_image']) ? Yii::getAlias("@web/frontapi/uploads/games/{$model['sport_image']}") : '',
					'sports_title' => $model['sport_title'] ?? '',
					'tournament_id' => +$model['tournament_id'],
					'tournament_title' => $model['tournament_title'] ?? '',
					'outcomes_count' => +$model['outcomes_count'],
					'bets_count' => +$model['bets_count'],
					'starts_at' => +$model['starts_at'],
					'is_finished' => !!$model['is_finished'],
					'is_live' => !!$model['is_live'],
					'participants' => ArrayHelper::getColumn($model['eventParticipants'], function ($participant) {
						return [
							'id' => +$participant['id'],
							'label' => $participant['name'] ?? '',
							'image' => !empty($participant['image']) ? Yii::getAlias("@web/frontapi/uploads/games/{$participant['image']}") : '',
						];
					}, false),
					'groups' => [
						['id' => 0, 'label' => 'Group 1'],
						['id' => 1, 'label' => 'Group 2'],
						['id' => 2, 'label' => 'Group 3'],
					],
					'outcomes' => ArrayHelper::getColumn($model['eventOutcomes'], function($outcome) {
						return [
							'id' => +$outcome['id'],
							'l' => $outcome['short_title'] ?? '',
							'r' => ($outcome['is_enabled'] && !$outcome['is_hidden']  && !$outcome['is_finished'] && !$outcome['is_banned']  && $outcome['outcome_enabled']) ? +$outcome['rate'] : 0,
						];
					}, false),
				];
			}))
		];
	}

	/**
	 * Get list of tournaments
	 *
	 * @param int|array $sport
	 * @param int|null $live
	 * @param int|null $starts_from
	 * @param int|null $starts_to
	 * @return array
	 *
	 * @OA\Get(
	 *   path="/game/tournaments",
	 *   operationId="tournaments",
	 *   tags={"Games"},
	 *   @OA\Parameter(name="sport[]", in="query", required=false, description="Sport IDS", @OA\Schema(type="array", @OA\Items(
	 *       description="Sport id",
	 *   ))),
	 *   @OA\Parameter(name="live", in="query", required=false, description="Only live", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="start_from", in="query", required=false, description="Time filter start", @OA\Schema(type="integer")),
	 *   @OA\Parameter(name="start_to", in="query", required=false, description="Time filter end", @OA\Schema(type="integer")),
	 *   @OA\Response(
	 *     response="200",
	 *     description="Tournaments list",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(
	 *         @OA\Property(property="results", type="array", description="Tournaments list",
	 *           @OA\Items(
	 *             description="Tournament info",
	 *             @OA\Property(property="id", type="number", description="ID"),
	 *             @OA\Property(property="sports_id", type="number", description="Sport id"),
	 *             @OA\Property(property="starts_at", type="number", description="Start time"),
	 *             @OA\Property(property="sort_order", type="number", description="Sort order"),
	 *             @OA\Property(property="title", type="string", description="Title"),
	 *             @OA\Property(property="icon", type="string", description="Tournament icon"),
	 *             @OA\Property(property="description", type="string", description="Description"),
	 *             @OA\Property(property="count_events", type="number", description="Events count"),
	 *           )
	 *         ),
	 *       )
	 *     })
	 *   )
	 * )
	 */
	public function actionTournaments (array $sport = [], int $live = null, int $starts_from = null, int $starts_to = null) {
		$tournamentsQuery = EventTournamentModel::find()->visible()->andFilterWhere([
			EventTournamentModel::tableName() . '.sports_id' => $sport
		])->innerJoinWith([
			'eventTournamentLocales locale_en' => function (EventTournamentLocaleQuery $query) use($sport) {
				$query->andFilterWhere([
					'locale_en.sports_id' => $sport
				])->andOnCondition([
					'locale_en.lang_code' => 'en',
				]);
			},
		], false)->asArray();
		if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$tournamentsQuery->joinWith([
				'eventTournamentLocales locale_tr' => function(EventTournamentLocaleQuery $query) {
					return $query->andOnCondition([
						'locale_tr.lang_code' => Yii::$app->language,
						'locale_tr.is_active' => 1,
					]);
				},
			], false);
		}
		$tournamentsQuery->innerJoinWith(['eventGames' => function (EventGameQuery $query) use ($live) {
			$query->active()->andWhere([
				'>', 'outcomes_count', 0
			])->andFilterWhere([
				EventGameModel::tableName() . '.is_live' => $live
			]);
		}])->groupBy(EventTournamentModel::tableName() . '.id');

		$tournamentsQuery->andFilterWhere([
			'>=', EventGameModel::tableName() . '.starts_at', $starts_from
		])->andFilterWhere([
			'<=', EventGameModel::tableName() . '.starts_at', $starts_to
		]);
		$tournaments = $tournamentsQuery->addSelect([
			EventTournamentModel::tableName() . '.*',
			'count_events' => new Expression('COUNT(*)'),
			'tournament_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.title, locale_en.title)') : 'locale_en.title',
			'tournament_description' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.description, locale_en.description)') : 'locale_en.description',
		])->asArray()->all();
		return ['success' => true, 'results' => ArrayHelper::map($tournaments, 'id', function($item) {
			return [
				'id' => +$item['id'],
				'sports_id' => +$item['sports_id'],
				'starts_at' => +$item['starts_at'],
				'sort_order' => +$item['sort_order'],
				'title' => $item['tournament_title'] ?? '',
				'icon' => !empty($item['flag']) ? Yii::getAlias("@web/frontend/static/img/flags/{$item['flag']}.png") : '',
				'description' => $item['tournament_description'] ?? '',
				'count_events' => +$item['count_events'],
			];
		})];
	}

	/**
	 * View event item
	 *
	 * @param int $id
	 * @return array
	 *
	 * @OA\Get(
	 *   path="/game/event",
	 *   operationId="event",
	 *   tags={"Games"},
	 *   @OA\Parameter(name="id", in="query", required=true, description="Event id", @OA\Schema(type="integer")),
	 *   @OA\Response(response="404", description="Not found", @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseFailed"),
	 *     })
	 *   ),
	 *   @OA\Response(
	 *     response="200",
	 *     description="Event item",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(
	 *         @OA\Property(property="id", type="number", description="ID"),
	 *         @OA\Property(property="sports_id", type="number", description="Sport id"),
	 *         @OA\Property(property="tournament_id", type="number", description="Tournament id"),
	 *         @OA\Property(property="tournament_title", type="string", description="Tournament title"),
	 *         @OA\Property(property="starts_at", type="number", description="Start time"),
	 *         @OA\Property(property="title", type="string", description="Title"),
	 *         @OA\Property(property="result", type="string", description="Current results"),
	 *         @OA\Property(property="result_extended", type="array", description="Current extended results list", @OA\Items(
	 *             description="Extended result item",
	 *             @OA\Property(property="0", type="string", description="1 participant value"),
	 *             @OA\Property(property="1", type="string", description="2 participant value"),
	 *           )
	 *         ),
	 *
	 *         @OA\Property(property="description", type="string", description="Description"),
	 *         @OA\Property(property="is_finished", type="boolean", description="Is finished"),
	 *         @OA\Property(property="participants", type="array", description="Participants list", @OA\Items(
	 *             description="Participant item",
	 *             @OA\Property(property="id", type="number", description="ID"),
	 *             @OA\Property(property="label", type="string", description="Label"),
	 *             @OA\Property(property="image", type="string", description="Participant logo"),
	 *           )
	 *         ),
	 *         @OA\Property(property="markets", type="array", description="Markets list", @OA\Items(
	 *             description="Market item",
	 *             @OA\Property(property="id", type="number", description="ID"),
	 *             @OA\Property(property="is_main", type="boolean", description="Is main"),
	 *             @OA\Property(property="group_id", type="number", description="Group ID"),
	 *             @OA\Property(property="sort_order", type="number", description="Sort order"),
	 *             @OA\Property(property="title", type="string", description="Title"),
	 *           )
	 *         ),
	 *         @OA\Property(property="groups", type="array", description="Groups list", @OA\Items(
	 *             description="Group item",
	 *             @OA\Property(property="id", type="number", description="ID"),
	 *             @OA\Property(property="title", type="string", description="Title"),
	 *           )
	 *         ),
	 *         @OA\Property(property="outcomes", type="array", description="Outcomes list", @OA\Items(
	 *             description="Outcome item",
	 *             @OA\Property(property="id", type="number", description="ID"),
	 *             @OA\Property(property="market_id", type="number", description="Market ID"),
	 *             @OA\Property(property="group_id", type="number", description="Group ID"),
	 *             @OA\Property(property="is_enabled", type="boolean", description="Is enabled"),
	 *             @OA\Property(property="is_finished", type="boolean", description="Is finished"),
	 *             @OA\Property(property="rate", type="number", description="Market rate"),
	 *             @OA\Property(property="title", type="string", description="Title"),
	 *           )
	 *         ),
	 *       )}
	 *     )
	 *   )
	 * )
	 */
	public function actionEvent (int $id) {
		$event = EventGameModel::find()->andWhere(['id' => $id])->visible()->with([
			'eventGameLocales' => function (EventGameLocaleQuery $query) {
				return $query->current();
			},
			'tournamentLocales' => function (EventTournamentLocaleQuery $query) {
				return $query->current();
			},
			'marketsGroups' => function (GameMarketsGroupsQuery $query) {
				$query->active()->orderBy(['sort_order' => SORT_DESC])->innerJoinWith([
					'gameMarketsGroupsLocales locale_en' => function(GameMarketsGroupsLocaleQuery $query) {
						return $query->andOnCondition([
							'locale_en.lang_code' => 'en',
						]);
					},
				], false)->addSelect([
					GameMarketsGroupsModel::tableName() . '.*',
					'title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.title, locale_en.title)') : 'locale_en.title',
				]);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'gameMarketsGroupsLocales locale_tr' => function(GameMarketsGroupsLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				}
			},
			'marketsGroups.additionalMarkets' => function (GameMarketsGroupsAdditionalQuery $query) {
				return $query->active();
			},
			'markets' => function (GameMarketsQuery $query) {
				$query->active()->orderBy(['sort_order' => SORT_DESC])->innerJoinWith([
					'gameMarketsLocales locale_en' => function(GameMarketsLocaleQuery $query) {
						return $query->andOnCondition([
							'locale_en.lang_code' => 'en',
						]);
					},
				], false)->addSelect([
					GameMarketsModel::tableName() . '.*',
					'title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.title, locale_en.title)') : 'locale_en.title',
				]);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'gameMarketsLocales locale_tr' => function(GameMarketsLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				};
			},
			'eventOutcomes' => function(EventOutcomesQuery $query) {
				$query->visible()->innerJoinWith(['outcome'], false)->select([
					'id' => EventOutcomesModel::tableName() . '.id',
					'game_id' => EventOutcomesModel::tableName() . '.game_id',
					'market_id' => EventOutcomesModel::tableName() . '.market_id',
					'group_id' => EventOutcomesModel::tableName() . '.group_id',
					'outcome_id' => GameOutcomesModel::tableName() . '.id',
					'is_enabled' =>  EventOutcomesModel::tableName() . '.is_enabled',
					'is_finished' =>  EventOutcomesModel::tableName() . '.is_finished',
					'rate' => EventOutcomesModel::tableName() . '.rate',
					'title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.title, locale_en.title)') : 'locale_en.title',
				])->andWhere([
					GameOutcomesModel::tableName() . '.is_enabled' => true
				])->innerJoinWith([
					'outcomeLocale locale_en' => function(GameOutcomesLocaleQuery $query) {
						return $query->andOnCondition([
							'locale_en.lang_code' => 'en',
						]);
					},
				], false);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'outcomeLocale locale_tr' => function(GameOutcomesLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				}

			},
			'eventParticipants' => function(EventParticipantsQuery $query) {
				$query->innerJoinWith([
					'participant',
					'participantLocale locale_en' => function(GameParticipantsLocaleQuery $query) {
						return $query->andOnCondition([
							'locale_en.lang_code' => 'en',
						]);
					},
				], false)->addSelect([
					EventParticipantsModel::tableName() . '.*',
					GameParticipantsModel::tableName() . '.image',
					'name' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(locale_tr.name, locale_en.name)') : 'locale_en.name',
				]);
				if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
					$query->joinWith([
						'participantLocale locale_tr' => function(GameParticipantsLocaleQuery $query) {
							return $query->andOnCondition([
								'locale_tr.lang_code' => Yii::$app->language,
								'locale_tr.is_active' => 1,
							]);
						},
					], false);
				}
			},
			'result',
		])->asArray()->limit(1)->one();
		if(!$event) {
			throw new NotFoundHttpException();
		}
		$eventLocale = GameHelpers::getLocalization($event['eventGameLocales']);
		$participants = \array_values(ArrayHelper::map($event['eventParticipants'], 'id', function ($participant) {
			return [
				'id' => +$participant['participant_id'],
				'label' => $participant['name'] ?? '',
				'image' => !empty($participant['image']) ? Yii::getAlias("@web/frontapi/uploads/games/{$participant['image']}") : '',
			];
		}));
		$participantReplacements = [];
		foreach($participants AS $k => $v) {
			$participantReplacements[$k + 1] = $v['label'];
		}
		$resultsExtended = json_decode($event['result']['result_extended'], true) ?? [];
		return [
			'success' => true,
			'id' => +$event['id'],
			'sports_id' => +$event['sports_id'],
			'tournament_id' => +$event['tournament_id'],
			'tournament_title' => GameHelpers::getLocalization($event['tournamentLocales'])['title'] ?? '',
			'starts_at' => +$event['starts_at'],
			'is_finished' => !!$event['is_finished'],
			'title' => $eventLocale['title'] ?? '',
			'description' => $eventLocale['description'] ?? '',
			'result' => $event['result']['result_simple'] ?? '0:0',
			'result_extended' => $resultsExtended,
			'participants' => $participants,
			'markets' => \array_values(ArrayHelper::map($event['markets'], 'id', function($item) {
				return [
					'id' => +$item['id'],
					'is_main' => !!$item['is_main'],
					'group_id' => +$item['group_id'],
					'sort_order' => +$item['sort_order'],
					'title' => $item['title'] ?? '',
				];
			})),
			'groups' => \array_values(ArrayHelper::map($event['marketsGroups'], 'id', function($item) {
				return [
					'id' => +$item['id'],
					'title' => $item['title'] ?? '',
					'additional' => ArrayHelper::getColumn($item['additionalMarkets'], function($item) {
						return [
							'market_id' => +$item['market_id'],
							'sort_order' => +$item['sort_order']
						];
					}, false),
				];
			})),
			'outcomes' => \array_values(ArrayHelper::map($event['eventOutcomes'], 'id', function($item) use ($participantReplacements) {
				return [
					'id' => +$item['id'],
					'market_id' => +$item['market_id'],
					'group_id' => +$item['group_id'],
					'is_enabled' => !!$item['is_enabled'],
					'is_finished' => !!$item['is_finished'],
					'rate' => +$item['rate'],
					'title' => GameHelpers::replacingCodeWithName($item['title'] ?? '', $participantReplacements),
				];
			})),
		];
	}

	/**
	 * View results list
	 *
	 * @param int|null $live
	 * @return array
	 *
	 * @OA\Get(
	 *   path="/game/results",
	 *   operationId="results",
	 *   tags={"Games"},
	 *   @OA\Parameter(name="live", in="query", required=false, description="Only live", @OA\Schema(type="integer")),
	 *   @OA\Response(response="404", description="Not found", @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseFailed"),
	 *     })
	 *   ),
	 *   @OA\Response(
	 *     response="200",
	 *     description="Event item",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(
	 *         @OA\Property(property="sports_id", type="number", description="Sport id"),
	 *         @OA\Property(property="sports_title", type="string", description="Sport title"),
	 *         @OA\Property(property="sports_image", type="string", description="Sport image"),
	 *
	 *         @OA\Property(property="tournament_id", type="number", description="Tournament id"),
	 *         @OA\Property(property="tournament_title", type="string", description="Tournament title"),
	 *         @OA\Property(property="tournament_events", type="array", description="Tournament events",
	 *           @OA\Items(
	 *             description="Tournament info",
	 *             @OA\Property(property="id", type="number", description="ID"),
	 *             @OA\Property(property="title", type="string", description="Title"),
	 *             @OA\Property(property="start_date", type="string", description="Start date"),
	 *             @OA\Property(property="start_time", type="string", description="Start time"),
	 *             @OA\Property(property="result", type="string", description="Result"),
	 *             @OA\Property(property="live", type="number", description="Is live"),
	 *           )
	 *         )
	 *       )}
	 *     )
	 *   )
	 * )
	 */
	public function actionResults(int $live = null) {
		$query = Yii::createObject([
			'class' => EventGameSearch::class,
			'scenario' => EventGameSearch::SCENARIO_SEARCH
		])->search(Yii::$app->request->get())->query;
		/** @var $query EventGameQuery */
		$query->active()->andWhere(['not', ['result' => null]])->andFilterWhere(['is_live' => $live])->andWhere([
			'>', 'outcomes_count', 0
		])->innerJoinWith([
			'sports' => function(GameSportsQuery $query) {
				return $query->active();
			},
			'sportsLocales sports_locales_en' => function(GameSportsLocaleQuery $query) {
				return $query->andOnCondition(['sports_locales_en.lang_code' => 'en']);
			},
			'eventGameLocales event_locales_en' => function(EventGameLocaleQuery $query) {
				return $query->andOnCondition(['event_locales_en.lang_code' => 'en']);
			},
			'tournamentLocales tournament_locales_en' => function(EventTournamentLocaleQuery $query) {
				return $query->andOnCondition(['tournament_locales_en.lang_code' => 'en']);
			},
		], false)->addSelect([
			EventGameModel::tableName() . '.*',
			'sport_image' => GameSportsModel::tableName() . '.image',
			'sport_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(sports_locales_tr.title, sports_locales_en.title)') : 'sports_locales_en.title',
			'event_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(event_locales_tr.title, event_locales_en.title)') : 'event_locales_en.title',
			'tournament_title' => (\Yii::$app->language !== \Yii::$app->sourceLanguage) ? new Expression('COALESCE(tournament_locales_tr.title, tournament_locales_en.title)') : 'tournament_locales_en.title',

		])->orderBy(['sort_order' => SORT_ASC])->asArray();
		if(\Yii::$app->language !== \Yii::$app->sourceLanguage) {
			$query->joinWith([
				'sportsLocales sports_locales_tr' => function(GameSportsLocaleQuery $query) {
					return $query->andOnCondition([
						'sports_locales_tr.lang_code' => Yii::$app->language,
						'sports_locales_tr.is_active' => 1,
					]);
				},
				'eventGameLocales event_locales_tr' => function(EventGameLocaleQuery $query) {
					return $query->andOnCondition([
						'event_locales_tr.lang_code' => Yii::$app->language,
						'event_locales_tr.is_active' => 1,
					]);
				},
				'tournamentLocales tournament_locales_tr' => function(EventTournamentLocaleQuery $query) {
					return $query->andOnCondition([
						'tournament_locales_tr.lang_code' => Yii::$app->language,
						'tournament_locales_tr.is_active' => 1,
					]);
				},
			], false);
		}
		$items = [];
		$sports = [];
		foreach($query->all() AS $model) {
			if(!isset($items[$model['tournament_id']])) {
				$items[$model['tournament_id']] = [
					'sports_id' => +$model['sports_id'],
					'sports_title' => $model['sport_title'] ?? '',
					'sports_image' => !empty($model['sport_image']) ? Yii::getAlias("@web/frontapi/uploads/games/{$model['sport_image']}") : '',
					'tournament_id' => +$model['tournament_id'],
					'tournament_title' => $model['tournament_title'] ?? '',
					'tournament_events' => [],
				];
			}
			if(!isset($sports[$model['sports_id']])) {
				$sports[$model['sports_id']] = $model['sport_title'];
			}
			$items[$model['tournament_id']]['tournament_events'][] = [
				'id' => +$model['id'],
				'title' => $model['event_title'] ?? '',
				'start_date' => \date('Y-m-d', $model['starts_at']),
				'start_time' => \date('H:i', $model['starts_at']),
				'result' => $model['result'] ?? '',
				'live' => +$model['is_live'],
			];
		}
		return [
			'events' => \array_values($items),
			'sports' => $sports,
		];
	}
}
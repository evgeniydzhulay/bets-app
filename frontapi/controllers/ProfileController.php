<?php

namespace frontapi\controllers;

use common\modules\user\controllers\AuthController as BaseAuthController;
use common\modules\user\forms\SettingsForm;
use common\modules\user\traits\UserEventTrait;
use Yii;
use common\modules\user\forms\PasswordForm;

class ProfileController extends BaseController {

	use UserEventTrait;

	/**
	 * Update password
	 *
	 * @return array
	 *
	 * @OA\Post(
	 *   path="/profile/password",
	 *   operationId="password",
	 *   tags={"Profile"},
	 *   @OA\RequestBody(
	 *     description="User passwords",
	 *     @OA\MediaType(
	 *       mediaType="application/x-www-form-urlencoded",
	 *       @OA\Schema(
	 *         required={"new_password", "repeat_password", "current_password"},
	 *         @OA\Property(property="new_password", type="string", format="password", description="New password"),
	 *         @OA\Property(property="repeat_password", type="string", format="password", description="Repeat new password"),
	 *         @OA\Property(property="current_password", type="string", format="password", description="Old password"),
	 *       )
	 *     ),
	 *   ),
	 *   security={{"bearer":{"authorized"}}},
	 *   @OA\Response(
	 *     response="200",
	 *     description="Request sent",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *     })
	 *   )
	 * )
	 */
	public function actionPassword() {
		/** @var PasswordForm $model */
		$model = Yii::createObject(PasswordForm::class);
		$event = $this->getFormEvent($model);
		$this->trigger(BaseAuthController::EVENT_BEFORE_ACCOUNT_UPDATE, $event);
		if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
			$this->trigger(BaseAuthController::EVENT_AFTER_ACCOUNT_UPDATE, $event);
			return ['success' => true];
		}
		return [
			'success' => false,
			'errors' => $model->errors
		];
	}

	/**
	 * Update or get settings
	 *
	 * @return array
	 *
	 * @OA\Post(
	 *   path="/profile/settings",
	 *   operationId="settingsUpdate",
	 *   tags={"Profile"},
	 *   @OA\RequestBody(
	 *     description="User profile info",
	 *     @OA\MediaType(
	 *       mediaType="application/x-www-form-urlencoded",
	 *       @OA\Schema(
	 *         @OA\Property(property="name", type="string", description="User's name"),
	 *         @OA\Property(property="surname", type="string", description="User's surname"),
	 *         @OA\Property(property="patrynomic", type="string", description="User's patrynomic"),
	 *         @OA\Property(property="email", type="string", format="email", description="User's email"),
	 *         @OA\Property(property="country", type="string", description="Country"),
	 *         @OA\Property(property="city", type="string", description="City"),
	 *         @OA\Property(property="address", type="string", description="Address"),
	 *         @OA\Property(property="dob", type="string", format="date", description="Date of birth"),
	 *         @OA\Property(property="passport_series", type="string", description="Password series"),
	 *         @OA\Property(property="passport_issuer", type="string", description="Password issuer"),
	 *         @OA\Property(property="passport_date", type="string", description="Password issue date"),
	 *       )
	 *     ),
	 *   ),
	 *   security={{"bearer":{"authorized"}}},
	 *   @OA\Response(
	 *     response="200",
	 *     description="Request sent",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseProcessed"),
	 *       @OA\Schema(ref="#/components/schemas/responseUserInfo"),
	 *     })
	 *   )
	 * )
	 * @OA\Get(
	 *   path="/profile/settings",
	 *   operationId="settingsGet",
	 *   tags={"Profile"},
	 *   security={{"bearer":{"authorized"}}},
	 *   @OA\Response(
	 *     response="200",
	 *     description="Request sent",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(ref="#/components/schemas/responseUserInfo"),
	 *     })
	 *   )
	 * )
	 */
	public function actionSettings() {
		/** @var SettingsForm $model */
		if(!Yii::$app->request->getIsPost()) {
			return ['success' => true] + $this->getUserDisplayInfo(Yii::$app->user->identity);
		}
		$model = Yii::createObject(SettingsForm::class);
		$event = $this->getFormEvent($model);
		$this->trigger(BaseAuthController::EVENT_BEFORE_ACCOUNT_UPDATE, $event);
		if ($model->load(Yii::$app->request->post(), '') && $model->save()) {
			$this->trigger(BaseAuthController::EVENT_AFTER_ACCOUNT_UPDATE, $event);
			return ['success' => true] + $this->getUserDisplayInfo($model->getUser());
		}
		return [
			'success' => false,
			'errors' => $model->errors
		];
	}

	/**
	 * user purse amount
	 *
	 * @OA\Get(
	 *   path="/profile/amount",
	 *   operationId="amount",
	 *   tags={"Profile"},
	 *   security={{"bearer":{"authorized"}}},
	 *   @OA\Response(
	 *     response="200",
	 *     description="User amount",
	 *     @OA\JsonContent(type="object", allOf={
	 *       @OA\Schema(
	 *         @OA\Property(
	 *              property="currency", type="string",
	 *              description="User purse currency",
	 *         ),
	 *         @OA\Property(
	 *              property="amount", type="number", format="float",
	 *              description="User purse amount",
	 *         ),
	 *       )
	 *     })
	 *   )
	 * )
	 * @return array
	 */
	public function actionAmount() {
		$user = Yii::$app->user->identity;
		return [
			'amount' => $user->purse_amount,
			'currency' => $user->purse_currency
		];
	}
}

<?php

namespace frontapi\controllers;

use common\modules\user\models\UserModel;
use frontapi\behaviors\LanguageBehavior;
use yii\rest\Controller;
use yii\filters\auth\HttpBearerAuth;

abstract class BaseController extends Controller {

	public $enableCsrfValidation = false;

	public function behaviors () {
		$behaviors = parent::behaviors();
		unset($behaviors['rateLimiter']);
		$behaviors['authenticator'] = [
			'class' => HttpBearerAuth::class,
		];
		$behaviors['language'] = [
			'class' => LanguageBehavior::class,
		];
		return $behaviors;
	}

	/**
	 * @param UserModel $user
	 * @return array
	 */
	protected function getUserDisplayInfo(UserModel $user) {
		return [
			'user' => [
				'id' => (int) $user->id,
				'amount' => (float) $user->purse_amount,
				'currency' => (string) $user->purse_currency,
				'email' => (string) $user->email,
			],
			'profile' => [
				'name' => (string) $user->profile->name,
				'surname' => (string) $user->profile->surname,
				'patrynomic' => (string) $user->profile->patrynomic,
				'phone' => (string) $user->phone,
				'country' => (string) $user->profile->country,
				'city' => (string) $user->profile->city,
				'address' => (string) $user->profile->address,
				'dob' => (string) $user->profile->dob,
				'passport_series' => (string) $user->profile->passport_series,
				'passport_issuer' => (string) $user->profile->passport_issuer,
				'passport_date' => (string) $user->profile->passport_date
			]
		];
	}

}
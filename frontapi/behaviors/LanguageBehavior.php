<?php

namespace frontapi\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\Controller;

class LanguageBehavior extends Behavior {

	/**
	 * Declares event handlers for the [[owner]]'s events.
	 * @return array events (array keys) and the corresponding event handler methods (array values).
	 */
	public function events() {
		return [
			Controller::EVENT_BEFORE_ACTION => function() {
				Yii::$app->language = $this->getCurrentLanguage();
			}
		];
	}


	/**
	 * Get current language
	 * If user is logged in gets its language
	 * Otherwise from session
	 * Otherwise from request
	 * @return string
	 */
	protected function getCurrentLanguage () {
	    if (Yii::$app->request->get('lang')) {
			return Yii::$app->request->get('lang');
		} else if (!Yii::$app->user->isGuest && !empty(Yii::$app->user->identity->lang)) {
			return Yii::$app->user->identity->lang;
		}
		return Yii::$app->getRequest()->getPreferredLanguage(array_keys(Yii::$app->params['languages']));
	}


}
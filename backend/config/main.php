<?php

use common\behaviors\LanguageRequestBehavior;
use dmstr\web\AdminLteAsset;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\ArrayHelper;
use yii\web\JqueryAsset;
use yii\web\UrlManager;
use yii\web\UrlNormalizer;

return ArrayHelper::merge([
	'id' => 'backend',
	'as LanguageRequestBehavior' => LanguageRequestBehavior::class,
	'controllerNamespace' => 'backend\controllers',
	'defaultRoute' => 'core/index',
	'basePath' => dirname(__DIR__),
	'components' => [
		'errorHandler' => [
			'errorAction' => 'core/error',
		],
		'assetManager' => [
			'appendTimestamp' => true,
			'bundles' => [
				JqueryAsset::class => [
					'js' => ['jquery.min.js',],
				],
				BootstrapAsset::class => [
					'sourcePath' => null,
					'basePath' => '@webroot',
					'baseUrl' => '@web',
					'css' => ['static/css/bootstrap.min.css'],
				],
				BootstrapPluginAsset::class => [
					'js' => ['js/bootstrap.min.js'],
				],
				AdminLteAsset::class => [
					'css' => [],
					'skin' => false,
				],
			],
		],
		'urlManager' => [
			'class' => UrlManager::class,
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'normalizer' => [
				'class' => UrlNormalizer::class,
				'collapseSlashes' => true,
				'normalizeTrailingSlash' => true,
			],
			'rules' => [
				'' => '/core/index',
			],
		],
	],
],
	require_once(BASE_PATH . '/backend/modules/user/config/backend.php'),
	require_once(BASE_PATH . '/backend/modules/games/config/backend.php'),
	require_once(BASE_PATH . '/backend/modules/support/config/backend.php'),
	require_once(BASE_PATH . '/backend/modules/content/config/backend.php'),
	require_once(BASE_PATH . '/backend/modules/blog/config/backend.php'),
	require_once(BASE_PATH . '/backend/modules/partners/config/backend.php'),
	(is_file(__DIR__ . '/main-local.php') ? require_once(__DIR__ . '/main-local.php') : [])
);
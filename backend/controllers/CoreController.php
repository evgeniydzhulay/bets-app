<?php

namespace backend\controllers;

use common\modules\user\actions\LanguageAction;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\Controller;

class CoreController extends Controller {

	/**
	 * List of available upload actions
	 *
	 * @return array
	 */
	public function actions() {
		return [
			'language' => [
				'class' => LanguageAction::class,
			],
		];
	}

	public function actionError () {
		$exception = Yii::$app->errorHandler->exception;
		$response = Yii::$app->response;
		$message = $exception->getMessage();
		if ($response->statusCode >= 500) {
			if (!($exception instanceof InvalidArgumentException)) {
				$message = $response->statusText;
			}
		}
		Yii::$app->user->enableSession = false;
		return $this->render('error', [
			'code' => $exception->statusCode ?? 503,
			'message' => !empty($message) ? $message : 'Internal error',
		]);
	}

	public function actionIndex () {
		return $this->render('index');
	}

}

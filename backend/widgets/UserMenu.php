<?php

namespace backend\widgets;

use Yii;
use yii\bootstrap\Nav;
use yii\web\User;


/**
 * @property mixed $user
 */
class UserMenu extends Nav {

    public $activateItems = false;
    public $activateParents = false;

    public $options = [
        'id' => 'nav-user-website',
        'class' => 'navbar-nav'
    ];
    
    /**
     * Render widget
     * @return string
     */
    public function run() {
	    $this->items = [
		    [
			    'label' => Yii::t('user', 'My account'),
			    'visible' => !$this->getUser()->isGuest,
			    'encodeLabels' => false,
			    'items' => [
				    [
					    'label' => Yii::t('user', 'Profile'),
					    'url' => ['/user/auth/account'],
					    'linkOptions' => ['class' => 'link-profile'],
				    ],
				    [
					    'label' => Yii::t('user', 'Change password'),
					    'url' => ['/user/auth/password'],
					    'linkOptions' => ['class' => 'link-password'],
				    ],
				    [
					    'label' => Yii::t('user', 'Logout'),
					    'url' => ['/user/auth/logout'],
					    'linkOptions' => ['class' => 'link-logout'],
				    ]
			    ]
		    ],
		    [
			    'visible' => $this->getUser()->isGuest,
			    'label' => Yii::t('user', 'Log In'),
			    'url' => ['/user/auth/login'],
			    'linkOptions' => ['class' => 'link-login'],
		    ],
	    ];
	    return parent::run();
    }

    protected function getUser() : User {
        return Yii::$app->get('user');
    }

}

<?php

namespace backend\widgets;

use Yii;
use yii\bootstrap\Nav;

/**
 *
 * @property array $menuList
 */
class MainMenu extends Nav {

    public $activateParents = true;
    public $options = [
        'id' => 'nav-main-website',
        'class' => 'navbar-nav'
    ];

    /**
     * Render widget
     * @return string
     */
    public function run() {
        $this->items = $this->getMenuList();
        return parent::run();
    }
    
    /**
     * Build menu
     *
     * Passes through modules and builds menu
     *
     * @return array Menu items
     */
    protected function getMenuList() {
        return [
            [
                'label' => Yii::t('games', 'Games'),
	            'dropDownOptions' => [
		            'id' => 'nav-main-dropdown-games',
	            ],
	            'visible' => Yii::$app->user->can('games-view') || Yii::$app->user->can('bets-view'),
	            'items' => [
		            [
			            'label' => Yii::t('games', 'Tournaments'),
			            'url' => ['/games/event-tournament/index'],
			            'visible' => Yii::$app->user->can('games-view'),
		            ],
		            [
			            'label' => Yii::t('games', 'Games'),
			            'url' => ['/games/event-game/index'],
			            'visible' => Yii::$app->user->can('games-view'),
		            ],
		            [
			            'label' => Yii::t('games', 'Dictionary'),
			            'url' => ['/games/game-sports/index'],
			            'visible' => Yii::$app->user->can('games-edit'),
		            ],
		            [
			            'label' => Yii::t('games', 'Bets'),
			            'url' => ['/games/bets/index'],
			            'visible' => Yii::$app->user->can('bets-view'),
		            ],
	            ]
            ],
	        [
		        'label' => Yii::t('partners', 'Partners'),
		        'dropDownOptions' => [
			        'id' => 'nav-main-dropdown-partners',
		        ],
		        'visible' => Yii::$app->user->can('partners-view') || Yii::$app->user->can('promo-view'),
		        'items' => [
			        [
				        'label' => Yii::t('partners', 'List'),
				        'url' => ['/partners/list/index'],
				        'visible' => Yii::$app->user->can('partners-view'),
			        ],
			        [
				        'label' => Yii::t('partners', 'Statistics'),
				        'url' => ['/partners/statistics/index'],
				        'visible' => Yii::$app->user->can('partners-view'),
			        ],
			        [
				        'label' => Yii::t('partners', 'Promo materials'),
				        'url' => '/partners/promo-materials/index',
				        'visible' => Yii::$app->user->can('promo-view'),
			        ],
			        [
				        'label' => Yii::t('partners', 'Promo campaigns'),
				        'url' => '/partners/promo-campaigns/index',
				        'visible' => Yii::$app->user->can('promo-view'),
			        ],
		        ]
	        ],
	        [
		        'label' => Yii::t('support', 'Support'),
		        'dropDownOptions' => [
			        'id' => 'nav-main-dropdown-support',
		        ],
		        'visible' => Yii::$app->user->can('support-view'),
		        'items' => [
			        [
				        'label' => Yii::t('support', 'Requests history'),
				        'url' => ['/support/requests/index'],
			        ],
			        [
				        'label' => Yii::t('support', 'Live feed'),
				        'url' => ['/support/live/index'],
			        ],
		        ]
	        ],
	        [
		        'label' => Yii::t('content', 'Content'),
		        'dropDownOptions' => [
			        'id' => 'nav-main-dropdown-content',
		        ],
		        'visible' => (
		        	Yii::$app->user->can('content-block-edit') ||
			        Yii::$app->user->can('content-category-edit') ||
			        Yii::$app->user->can('content-page-edit')
		        ),
		        'items' => [
			        [
				        'label' => Yii::t('content', 'Categories'),
				        'url' => ['/content/category/index'],
				        'visible' => Yii::$app->user->can('content-category-edit'),
			        ],
			        [
				        'label' => Yii::t('content', 'Pages'),
				        'url' => ['/content/page/index'],
				        'visible' => Yii::$app->user->can('content-page-edit'),
			        ],
			        [
				        'label' => Yii::t('content', 'Blocks'),
				        'url' => ['/content/block/index'],
				        'visible' => Yii::$app->user->can('content-block-edit'),
			        ],
		        ]
	        ],
	        [
		        'label' => Yii::t('blog', 'Blog'),
		        'dropDownOptions' => [
			        'id' => 'nav-main-dropdown-blog',
		        ],
		        'visible' => Yii::$app->user->can('blog-view'),
		        'items' => [
			        [
				        'label' => Yii::t('blog', 'Posts'),
				        'url' => ['/blog/post/index'],
			        ],
			        [
				        'label' => Yii::t('blog', 'Tags'),
				        'url' => ['/blog/tag/index'],
			        ],
		        ]
	        ],
            [
                'label' => Yii::t('app', 'Administration'),
                'dropDownOptions' => [
                    'id' => 'nav-main-dropdown-administration',
                ],
                'visible' => Yii::$app->user->can('admin'),
                'items' => [
                    [
                        'label' => Yii::t('user', 'Users'),
                    ],
                    '<hr>',
                    [
                        'label' => Yii::t('user', 'Users'),
                        'url' => ['/user/admin/index'],
                        'visible' => Yii::$app->user->can('users-view')
                    ],
                    [
                        'label' => Yii::t('rbac', 'Roles'),
                        'url' => ['/user/role/index'],
                        'visible' => Yii::$app->user->can('rbac-roles')
                    ],
                    [
                        'label' => Yii::t('rbac', 'Permissions'),
                        'url' => ['/user/permission/index'],
                        'visible' => Yii::$app->user->can('rbac-permissions')
                    ],
                ]
            ],
            [
	            'label' => Yii::t('purse', 'Funds'),
		        'dropDownOptions' => [
			        'id' => 'nav-main-dropdown-funds',
		        ],
		        'visible' => Yii::$app->user->can('users-funds'),
		        'items' => [
			        [
				        'label' => Yii::t('purse', 'Deposits'),
				        'url' => ['/user/deposits/index'],
			        ],
			        [
				        'label' => Yii::t('purse', 'Withdrawals'),
				        'url' => ['/user/withdrawals/index'],
			        ],
		        ]
	        ],
        ];
    }

}

<?php

namespace backend\modules\support\controllers;

use common\modules\support\models\SupportMessageModel;
use common\modules\support\models\SupportRequestModel;
use Yii;
use common\modules\support\search\BackendSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

class RequestsController extends Controller {
	public function actionIndex() {
		$searchModel = Yii::createObject(BackendSearch::class);
		$searchModel->scenario = BackendSearch::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->get());
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
		]);
	}

	public function actionItem(int $id) {
		$request = SupportRequestModel::find()->andWhere([
			'id' => $id
		])->one();

		$dataProvider = new ActiveDataProvider([
			'query' => SupportMessageModel::find()->andWhere([
				'request_id' => $id
			]),
			'pagination' => [
				'pageSize' => 20,
				'pageSizeLimit' => false,
				'pageSizeParam' => false,
			],
			'sort' => [
				'defaultOrder' => [
					'id' => SORT_DESC
				]
			]
		]);
		return $this->render('item', [
			'request' => $request,
			'dataProvider' => $dataProvider,
		]);
	}
}
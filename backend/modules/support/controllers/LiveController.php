<?php

namespace backend\modules\support\controllers;

use common\modules\support\models\SupportAttachmentModel;
use common\modules\support\models\SupportMessageModel;
use common\modules\support\Module;
use yii\web\Controller;
use yii\web\Response;
use Yii;
use yii\helpers\Url;
use yii\web\View;

class LiveController extends Controller {

	public function actionIndex() {
		$user = Yii::$app->user->identity;
		$key = Module::createNodeSession('', '','', Module::TYPE_OPERATOR, $user);
		$this->view->registerJs("window.paramsSocket = " . json_encode([
			'key' => $key,
			'host' => Url::base(true),
			'path' => Yii::$app->params['socketSupport.path'],
		]), View::POS_HEAD);
		return $this->render('index');
	}

	public function actionMessages (int $id) {
		Yii::$app->response->format = Response::FORMAT_JSON;
		$last = Yii::$app->request->get('last', 0);
		$messages = SupportMessageModel::find()->andWhere([
			'request_id' => $id
		])->andFilterWhere([
			'<', 'id', ($last > 0 ? $last : null)
		])->with('attachments')->orderBy(['id' => SORT_DESC])->limit(20)->asArray()->all();
		$attachmentModel = new SupportAttachmentModel();
		foreach($messages AS &$message) {
			$message['id'] = +$message['id'];
			$message['created_at'] = date('Y-m-d H:i:s', $message['created_at']);
			$attachments = [];
			foreach($message['attachments'] AS $attachment) {
				$attachmentModel->path = $attachment['path'];
				$attachments[] = [
					'name' => $attachment['name'],
					'url' => $attachmentModel->getUrl(),
				];
			}
			$message['files'] = $attachments;
			unset($message['attachments']);
		}
		return $messages;
	}
}
<?php

/* @var $this \yii\web\View */

use backend\assets\SupportAsset;

SupportAsset::register($this);

?>
<script>
    window.$locale = '<?= Yii::$app->language; ?>';
    window.$messages = <?= json_encode([
		Yii::$app->language => [
			'Submit' => Yii::t('support', 'Submit'),
			'Close' => Yii::t('support', 'Close'),
			'Message' => Yii::t('support', 'Message'),
		]
	]); ?>;
</script>
<div id="support-backend"></div>

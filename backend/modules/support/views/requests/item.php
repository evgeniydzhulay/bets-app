<?php

/* @var $this \yii\web\View */
/* @var $request \common\modules\support\models\SupportRequestModel */
/* @var $dataProvider \yii\data\ActiveDataProvider */

?>

<div class="col-md-9">
	<?= \yii\widgets\ListView::widget([
		'id' => 'request-item',
		'dataProvider' => $dataProvider,
		'itemView' => 'itemMessage',
		'layout' => '
		<div class="row"><div class="col-xs-7">{pager}</div><div class="col-xs-5 text-right">{summary}</div></div>
		{items}
		<div class="row"><div class="col-xs-7">{pager}</div><div class="col-xs-5 text-right">{summary}</div></div>
		',
		'emptyText' => "<div class='callout callout-danger'>" . Yii::t('yii', 'No results found.') . '</div>',
		'pager' => [
	        'options' => [
	            'class' => 'pagination pagination-sm no-margin'
	        ]
		]
	]); ?>
</div>
<div class="col-md-3">
	<div class="box box-primary">
		<div class="box-header with-border">
			<span class="text-bold">
				<?= Yii::t('support', 'Theme'); ?>
			</span>
			<?= $request->theme; ?>
		</div>
		<div class="box-footer">
			<dl class="dl-horizontal">
				<dt><?= Yii::t('support', 'User'); ?></dt>
				<dd>
					<?=
					implode(' ', array_filter([
						$request->name,
						$request->email,
						$request->phone,
					]));
					?>
				</dd>
				<dt><?= Yii::t('support', 'Created at'); ?></dt>
				<dd><?= date('Y-m-d H:i:s', $request->created_at); ?></dd>
				<dt><?= Yii::t('support', 'Is new'); ?></dt>
				<dd><span class="glyphicon glyphicon<?= $request->is_new ? '-ok text-success' : '-remove text-danger' ?>"></span></dd>
				<dt><?= Yii::t('support', 'Is closed'); ?></dt>
				<dd><span class="glyphicon glyphicon<?= $request->is_closed ? '-ok text-success' : '-remove text-danger' ?>"></span></dd>
			</dl>
		</div>
	</div>
</div>

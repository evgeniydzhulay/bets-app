<div class="box <?= $model['is_new'] ? 'box-warning' : 'box-primary'; ?>">
	<div class="box-header">
		<?= implode(' ', array_filter([
            $model['from_name'],
			$model['from_email'],
        ])); ?>
	</div>
	<div class="box-body">
		<?= $model['message']; ?>
	</div>
	<div class="box-footer">
		<?= date('Y-m-d H:i:s', $model['created_at']); ?>
	</div>
</div>
<?php

use common\modules\support\search\BackendSearch;
use common\widgets\GridView;
use kartik\form\ActiveForm;
use kartik\grid\BooleanColumn;
use yii\data\ActiveDataProvider;
use yii\web\View;
use yii\widgets\Pjax;
use yii\helpers\Html;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var BackendSearch $searchModel
 * @var array $statuses
 */
$this->title = Yii::t('support', 'Requests list');
$this->params['breadcrumbs'][] = $this->title;

Pjax::begin([
	'linkSelector' => '#support-list .pagination a, #support-search .search-reset',
	'formSelector' => '#support-search',
	'id' => 'support-list_container',
	'options' => [
		'class' => 'content with-sidebar',
	],
	'timeout' => 5000,
	'clientOptions' => [
		'maxCacheLength' => 0
	]
]);
?>
	<div class="content-sidebar">
		<?php
		$formSearch = ActiveForm::begin([
			'id' => 'support-search',
			'options' => [
				'class' => 'box box-primary',
			],
			'enableAjaxValidation' => false,
			'enableClientValidation' => true,
			'method' => 'GET',
			'action' => ['index']
		]);
		?>
		<div class="box-body">
			<?php
			echo $formSearch->field($searchModel, 'email');
			echo $formSearch->field($searchModel, 'phone');
			echo $formSearch->field($searchModel, 'name');
			echo $formSearch->field($searchModel, 'theme');
			echo $formSearch->field($searchModel, 'messageSearch')->textarea([
                'rows' => 2
            ]);
			echo $formSearch->field($searchModel, 'is_closed')->checkbox();
			echo $formSearch->field($searchModel, 'is_new')->checkbox();
			?>
		</div>
		<div class="box-footer">
			<?=
			Html::submitButton(Yii::t('support', 'Search'), [
				'class' => 'btn btn-block btn-success'
			]);
			?>
			<?=
			Html::a(Yii::t('support', 'Reset'), ['index'], [
				'class' => 'btn btn-block btn-info search-reset'
			]);
			?>
		</div>
		<?php
		ActiveForm::end();
		?>
	</div>
	<div class="content-body">
		<?php
		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'pjax' => false,
			'id' => 'support-list',
			'columns' => [
                [
                    'attribute' => 'id',
                    'headerOptions' => ['width' => '50px'],
	                'content' => function($model) {
		                return Html::a($model->id, ['item', 'id' => $model->id], [
			                'data' => ['pjax' => 0]
		                ]);
	                }
                ],
				[
					'attribute' => 'user',
					'headerOptions' => ['width' => '20%'],
					'content' => function($model) {
						return implode(' ', [
                            $model->name,
							$model->email,
							$model->phone,
                        ]);
					}
				],
				[
					'attribute' => 'theme',
					'headerOptions' => ['width' => '20%'],
				],
				[
					'attribute' => 'message',
					'content' => function($model) {
						return mb_substr(strip_tags($model->message), 0 , 200);
					}
				],
				[
					'class' => BooleanColumn::class,
					'attribute' => 'is_closed',
					'headerOptions' => ['width' => '50px'],
				],
				[
					'class' => BooleanColumn::class,
					'attribute' => 'is_new',
					'headerOptions' => ['width' => '50px'],
				],
			],
		]);
		?>
	</div>
<?php
Pjax::end();
?>
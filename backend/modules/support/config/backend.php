<?php

return [
	'modules' => [
		'support' => [
			'controllerNamespace' => '\backend\modules\support\controllers',
			'viewPath' => '@backend/modules/support/views'
		],
	],
];

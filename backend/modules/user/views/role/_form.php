<?php

use common\modules\user\rbac\Role;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/**
 * @var $this  yii\web\View
 * @var $model Role
 */
?>
<div class="box box-primary">
    <?php
    $form = ActiveForm::begin([
        'id' => 'admin-role-form',
        'enableClientValidation' => false,
        'enableAjaxValidation' => true,
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'formConfig' => ['labelSpan' => 3],
    ])
    ?>
    <div class="box-body">
        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'description') ?>
        <?= $form->field($model, 'rule') ?>
        <?=
        $form->field($model, 'children')->widget(Select2::class, [
            'data' => $model->getUnassignedItems(),
            'options' => [
                'id' => 'children',
                'multiple' => true,
            ],
        ])
        ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('rbac', 'Save'), ['class' => 'btn btn-success btn-block']) ?>
    </div>
    <?php ActiveForm::end() ?>
</div>
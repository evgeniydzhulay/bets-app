<?php

use common\modules\user\rbac\Role;

/**
 * @var $model Role
 * @var $this  yii\web\View
 */

$this->title = Yii::t('rbac', 'Create new role');
$this->params['breadcrumbs'][] = ['label' => Yii::t('rbac', 'Roles'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
echo $this->render('_form', [
    'model' => $model,
]);
<?php

use common\modules\user\models\UserPurseHistoryModel;
use common\modules\user\search\UserPurseDepositSearch;
use common\widgets\GridView;
use kartik\date\DatePicker;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $this \yii\web\View
 * @var $searchModel UserPurseDepositSearch
 * @var $dataProvider ActiveDataProvider
 * @var array $statuses
 */

$this->title = Yii::t('purse', 'Deposits');

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'toolbar' => [
		['content' =>
			Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::toRoute(['index']), [
				'class' => 'btn btn-default btn-sm',
				'title' => Yii::t('user', 'Reset filter')]
			)
		],
	],
	'columns' => [
		[
			'attribute' => 'user_id',
			'headerOptions' => ['width' => '25%'],
			'content' => function($model) {
				return "{$model->profile->name} {$model->profile->surname}";
			}
		],
		[
			'attribute' => 'amount',
			'headerOptions' => ['width' => '150px'],
			'content' => function($model) {
				return Yii::$app->formatter->asCurrency($model->amount, $model->purse_currency);
			}
		],
		[
			'attribute' => 'service',
			'filter' => $services,
			'value' => function ($model) {
				return UserPurseHistoryModel::sources()[$model['service']] ?? '';
			},
		],
		[
			'attribute' => 'status',
			'filter' => $statuses,
			'headerOptions' => ['width' => '130px'],
			'value' => function ($model) use ($statuses) {
				return $statuses[$model->status] ?? '';
			},
		],
		[
			'attribute' => 'created_at',
			'value' => function ($model) {
				return date('Y-m-d H:i:s', $model->created_at);
			},
			'filter' => DatePicker::widget([
				'model' => $searchModel,
				'id' => 'purse-history-search-created_at',
				'attribute' => 'created_at',
				'type' => DatePicker::TYPE_INPUT,
				'pluginOptions' => [
					'autoclose' => true,
					'format' => 'dd-mm-yyyy'
				]
			]),
			'headerOptions' => ['width' => '150px'],
		],
	],
]);
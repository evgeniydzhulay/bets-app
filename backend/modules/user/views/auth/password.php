<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model \common\modules\user\forms\PasswordForm */

$this->title = Yii::t('user', 'Account password');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-6 col-md-offset-3">
    <?php
    $form = ActiveForm::begin([
        'id' => 'account-form',
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'options' => [
            'class' => 'box box-default profile-password',
        ],
        'formConfig' => [
            'labelSpan' => 3,
        ],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]);
    ?>
    <div class="box-header text-center">
        <h3 class="box-title">
            <?= Html::encode($this->title) ?>
        </h3>
    </div>
    <div class="box-body">
        <?= $form->field($model, 'new_password')->passwordInput() ?>
        <?= $form->field($model, 'repeat_password')->passwordInput() ?>
    </div>
    <div class="box-footer">
        <?= $form->field($model, 'current_password')->passwordInput() ?>
    </div>
    <div class="box-footer text-center">
        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-warning']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>

<?php

use common\modules\user\forms\PasswordForm;
use common\modules\user\forms\SettingsForm;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this  yii\web\View
 * @var $form  yii\widgets\ActiveForm
 * @var $modelAccount SettingsForm
 * @var $modelPassword PasswordForm
 * @var $enabled2fa bool
 */

$this->title = Yii::t('user', 'Account settings');
$this->params['breadcrumbs'][] = $this->title;
$this->beginContent('@common/modules/user/views/shared/profile.php');
?>
    <div class="col-md-6 col-md-offset-3">
		<?php
		$form = ActiveForm::begin([
			'id' => 'account-form',
			'type' => ActiveForm::TYPE_HORIZONTAL,
			'options' => [
				'class' => 'box box-default',
			],
			'formConfig' => [
				'labelSpan' => 4,
			],
			'enableAjaxValidation' => false,
			'enableClientValidation' => false,
		]);
		?>
        <div class="box-header text-center">
            <h3 class="box-title"><?= Yii::t('user', 'Contact info'); ?></h3>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($modelAccount, 'name', [
                        'showRequiredIndicator' => false,
                    ])->staticInput() ?>
                </div>
                <div class="col-md-6">
		            <?= $form->field($modelAccount, 'surname', [
			            'showRequiredIndicator' => false,
		            ])->staticInput() ?>
                </div>
            </div>
        </div>
        <div class="box-body">
            <div class="row">
                <div class="col-md-6">
		            <?= $form->field($modelAccount, 'phone', [
			            'showRequiredIndicator' => false,
		            ])->staticInput() ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($modelAccount, 'email', [
	                    'showRequiredIndicator' => false,
                    ])->staticInput() ?>
                </div>
            </div>
        </div>
		<?php
		ActiveForm::end();
		?>
        <div class="box box-default profile-twofactor text-center">
            <div class="box-header">
                <h3 class="box-title"><?= Yii::t('user', 'Manage twofactor authentication'); ?></h3>
            </div>
            <div class="box-body">
			    <?= Yii::t('user', 'Twofactor authentication is enabled: {enabled}', [
				    'enabled' => $enabled2fa ? Yii::t('yii', 'Yes') : Yii::t('yii', 'No')
			    ]); ?>
            </div>
            <div class="box-footer">
	            <?= Html::a($enabled2fa ? Yii::t('user', 'Disable') : Yii::t('user', 'Enable'), ['two-factor/index'], [
		            'class' => 'btn btn-success',
		            'data' => [
			            'toggle' => 'modal',
			            'target' => '#modal2fa'
		            ]
	            ]); ?>
            </div>
        </div>
    </div>
    <div id="modal2fa" class="fade modal" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-md">
            <div class="modal-content modal2fa-form__container">
            </div>
        </div>
    </div>
<?php
$this->endContent();
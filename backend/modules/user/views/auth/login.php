<?php

use common\modules\user\forms\LoginForm;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var LoginForm $model
 */
$this->title = Yii::t('user', 'Sign in');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
	<div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3">
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
			</div>
			<div class="panel-body">
				<?php
				$form = ActiveForm::begin([
					'id' => 'login-form',
					'enableAjaxValidation' => false,
					'enableClientValidation' => true,
					'validateOnBlur' => false,
					'validateOnType' => false,
					'validateOnChange' => false,
				])
				?>
				<?= $form->field($model, 'login', ['inputOptions' => ['autofocus' => 'autofocus', 'class' => 'form-control', 'tabindex' => '1']]) ?>
				<?= $form->field($model, 'password', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '2']])->passwordInput()->label(Yii::t('user', 'Password') . ' (' . Html::a(Yii::t('user', 'Forgot password?'), ['auth/request'], ['tabindex' => '5']) . ')') ?>
				<?= $form->field($model, 'rememberMe')->checkbox(['tabindex' => '4']) ?>
				<?php
				if($model->showTwoFactorInput) {
					echo $form->field($model, 'twofacode', ['inputOptions' => ['class' => 'form-control', 'tabindex' => '3']]);
				}
				?>
				<?= Html::submitButton(Yii::t('user', 'Sign in'), ['class' => 'btn btn-primary btn-block', 'tabindex' => '3']) ?>
				<?php ActiveForm::end(); ?>
			</div>
		</div>
		<p class="text-center">
			<?= Html::a(Yii::t('user', 'Didn\'t receive confirmation message?'), ['auth/resend']) ?>
		</p>
		<p class="text-center">
			<?= Html::a(Yii::t('user', 'Don\'t have an account? Sign up!'), ['auth/register']) ?>
		</p>
	</div>
</div>

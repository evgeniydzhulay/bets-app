<?php

use common\modules\user\rbac\Search;
use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $dataProvider array
 * @var $this         yii\web\View
 * @var $filterModel  Search
 */

$this->title = Yii::t('rbac', 'Permissions');
$this->params['breadcrumbs'][] = $this->title;
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
    'toolbar' => [
        ['content' =>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', Url::toRoute(['create']), [
                'data-pjax' => 0,
                'class' => 'btn btn-default',
                'title' => Yii::t('rbac', 'New permission')]
            )
            . ' ' .
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::toRoute(['index']), [
                'class' => 'btn btn-default',
                'title' => Yii::t('user', 'Reset filter')]
            )
        ],
    ],
    'columns' => [
        [
            'attribute' => 'name',
            'header' => Yii::t('rbac', 'Name'),
            'options' => [
                'style' => 'width: 20%'
            ],
        ],
        [
            'attribute' => 'description',
            'header' => Yii::t('rbac', 'Description'),
            'options' => [
                'style' => 'width: 55%'
            ],
        ],
        [
            'attribute' => 'rule_name',
            'header' => Yii::t('rbac', 'Rule name'),
            'options' => [
                'style' => 'width: 20%'
            ],
        ],
        [
            'content' => function($model) {
                return Html::a(Yii::t('user', 'Update'), ['update', 'name' => $model['name']], [
                    'class' => 'btn btn-block btn-xs btn-success'
                ]);
            }
        ],
    ],
]);

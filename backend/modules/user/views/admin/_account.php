<?php

use common\modules\user\models\UserModel;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/**
 * @var $this yii\web\View
 * @var $user UserModel
 */
?>

<?php $this->beginContent('@common/modules/user/views/admin/update.php', ['user' => $user]) ?>
<div class="box box-primary">
    <?php
    $form = ActiveForm::begin([
                'type' => ActiveForm::TYPE_HORIZONTAL,
                'id' => 'admin-user-form-account',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'formConfig' => ['labelSpan' => 3]
    ]);
    ?>
    <div class="box-body">
        <?= $this->render('_user', ['form' => $form, 'user' => $user]) ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-block btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php $this->endContent() ?>

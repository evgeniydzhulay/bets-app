<?php

/**
 * @var yii\widgets\ActiveForm 		        $form
 * @var \common\modules\user\models\UserModel    $user
 */
?>

<?= $form->field($user, 'email')->textInput(['maxlength' => 255]) ?>
<?= $form->field($user, 'password')->passwordInput() ?>

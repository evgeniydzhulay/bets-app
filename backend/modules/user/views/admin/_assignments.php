<?php

use common\modules\user\models\UserModel;
use common\modules\user\rbac\Assignment;
use kartik\select2\Select2;
use yii\bootstrap\Alert;
use yii\helpers\Html;
use yii\web\View;
use yii\widgets\ActiveForm;

/**
 * @var View $this
 * @var UserModel $user
 * @var Assignment $model
 */

$this->beginContent('@common/modules/user/views/admin/update.php', ['user' => $user]);
?>
<div class="box box-primary">
    <div class="box-body">
        <?php if ($model->updated): ?>
            <?=
            Alert::widget([
                'options' => [
                    'class' => 'alert-success',
                ],
                'body' => Yii::t('rbac', 'Assignments have been updated'),
            ])
            ?>
        <?php endif ?>
        <?php
        $form = ActiveForm::begin([
            'id' => 'admin-user-form-assignments',
            'enableClientValidation' => false,
            'enableAjaxValidation' => false,
        ])
        ?>
        <?= Html::activeHiddenInput($model, 'user_id') ?>
        <?=
        $form->field($model, 'items')->widget(Select2::class, [
            'data' => $model->getAvailableItems(),
            'options' => [
                'id' => 'items',
                'multiple' => true,
            ],
        ])
        ?>
        <?= Html::submitButton(Yii::t('rbac', 'Update assignments'), ['class' => 'btn btn-success btn-block']) ?>
        <?php ActiveForm::end() ?>
    </div>
</div>
<?php $this->endContent() ?>

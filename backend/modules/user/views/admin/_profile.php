<?php

use common\modules\user\models\ProfileModel;
use common\modules\user\models\UserModel;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View $this
 * @var UserModel $user
 * @var ProfileModel $profile
 */
?>

<?php $this->beginContent('@common/modules/user/views/admin/update.php', ['user' => $user]) ?>

<div class="box box-primary">
    <?php
    $form = ActiveForm::begin([
        'type' => ActiveForm::TYPE_HORIZONTAL,
        'id' => 'admin-user-form-profile',
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
        'formConfig' => ['labelSpan' => 3],
    ]);
    ?>
    <div class="box-body">
        <?= $form->field($profile, 'name') ?>
        <?= $form->field($profile, 'surname') ?>
        <?= $form->field($profile, 'public_email') ?>
        <?= $form->field($profile, 'bio')->textarea() ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('user', 'Update'), ['class' => 'btn btn-block btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php $this->endContent() ?>

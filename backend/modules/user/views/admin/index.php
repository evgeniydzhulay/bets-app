<?php

use common\modules\user\search\UserSearch;
use common\widgets\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var UserSearch $searchModel
 */
$this->title = Yii::t('user', 'Manage users');
$this->params['breadcrumbs'][] = $this->title;
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'toolbar' => [
        ['content' =>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', Url::toRoute(['create']), [
                'data-pjax' => 0,
                'class' => 'btn btn-default btn-sm',
                'title' => Yii::t('user', 'New user')]
            )
            . ' ' .
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::toRoute(['index']), [
                'class' => 'btn btn-default btn-sm',
                'title' => Yii::t('user', 'Reset filter')]
            )
        ],
    ],
    'columns' => [
        [
            'attribute' => 'name',
	        'headerOptions' => ['width' => '25%'],
            'content' => function($model) {
                return "{$model->name} {$model->surname}";
            }
        ],
        [
            'attribute' => 'email',
	        'headerOptions' => ['width' => '20%'],
        ],
        [
            'attribute' => 'created_at',
            'value' => function ($model) {
                return date('Y-m-d H:i:s', $model->created_at);
            },
	        'headerOptions' => ['width' => '15%'],
        ],
        [
            'header' => Yii::t('user', 'Confirmation'),
            'value' => function ($model) {
                if ($model->isConfirmed) {
                    return '<div class="text-center"><span class="text-success">' . Yii::t('user', 'Confirmed') . '</span></div>';
                } else {
                    return Html::a(Yii::t('user', 'Confirm'), ['confirm', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                    ]);
                }
            },
            'format' => 'raw',
	        'headerOptions' => ['width' => '10%'],
        ],
        [
            'attribute' => 'blocked_at',
            'filter' => [
                '0' => Yii::t('yii', 'No'),
                '1' => Yii::t('yii', 'Yes'),
            ],
            'header' => Yii::t('user', 'Block status'),
            'value' => function ($model) {
                if ($model->isBlocked) {
                    return Html::a(Yii::t('user', 'Unblock'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-success btn-block',
                        'data-pjax' => 0,
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                    ]);
                } else {
                    return Html::a(Yii::t('user', 'Block'), ['block', 'id' => $model->id], [
                        'class' => 'btn btn-xs btn-danger btn-block',
                        'data-pjax' => 0,
                        'data-method' => 'post',
                        'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                    ]);
                }
            },
            'format' => 'raw',
        ],
        [
            'content' => function($model) {
                return Html::a(Yii::t('user', 'Update'), ['update', 'id' => $model->id], [
                    'data-pjax' => 0,
                    'class' => 'btn btn-block btn-xs btn-success'
                ]);
            }
        ],
        [
            'content' => function($model) {
                return Html::a(Yii::t('user', 'Login'), ['auth/secondary', 'id' => $model->id], [
                    'data-pjax' => 0,
                    'class' => 'btn btn-block btn-xs btn-success'
                ]);
            },
            'visible' => Yii::$app->user->can('admin')
        ],
    ],
]);
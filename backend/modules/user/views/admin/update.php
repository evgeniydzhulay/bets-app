<?php

use common\modules\user\models\UserModel;
use yii\bootstrap\Nav;
use yii\helpers\Url;
use yii\web\View;

/**
 * @var View 	$this
 * @var UserModel 	$user
 * @var string 	$content
 */
$this->title = Yii::t('user', 'Update user account');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['/user/admin/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <?=
            Nav::widget([
                'options' => [
                    'id' => 'admin-user-manu',
                    'class' => 'nav-pills nav-stacked',
                ],
                'items' => [
                    [
                        'label' => Yii::t('user', 'Account details'),
                        'url' => Url::toRoute(['/user/admin/update', 'id' => $user->id]),
                        'visible' => Yii::$app->user->can('users-edit'),
                    ],
                    [
                        'label' => Yii::t('user', 'Profile details'),
                        'url' => Url::toRoute(['/user/admin/update-profile', 'id' => $user->id]),
                        'visible' => Yii::$app->user->can('users-edit'),
                    ],
                    [
                        'label' => Yii::t('user', 'Information'),
                        'url' => Url::toRoute(['/user/admin/info', 'id' => $user->id]),
                        'visible' => Yii::$app->user->can('users-view'),
                    ],
                    [
                        'label' => Yii::t('user', 'Assignments'),
                        'url' => Url::toRoute(['/user/admin/assignments', 'id' => $user->id]),
                        'visible' => Yii::$app->user->can('users-assignments'),
                    ],
                    [
                        'label' => Yii::t('user', 'Confirm'),
                        'url' => Url::toRoute(['/user/admin/confirm', 'id' => $user->id]),
                        'visible' => (!$user->isConfirmed && Yii::$app->user->can('users-edit')),
                        'linkOptions' => [
                            'class' => 'text-success',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to confirm this user?'),
                        ],
                    ],
                    [
                        'label' => Yii::t('user', 'Block'),
                        'url' => Url::toRoute(['/user/admin/block', 'id' => $user->id]),
                        'visible' => (!$user->isBlocked && Yii::$app->user->can('users-edit')),
                        'linkOptions' => [
                            'class' => 'text-danger',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to block this user?'),
                        ],
                    ],
                    [
                        'label' => Yii::t('user', 'Unblock'),
                        'url' => Url::toRoute(['/user/admin/block', 'id' => $user->id]),
                        'visible' => ($user->isBlocked && Yii::$app->user->can('users-edit')),
                        'linkOptions' => [
                            'class' => 'text-success',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to unblock this user?'),
                        ],
                    ],
                    [
                        'label' => Yii::t('user', 'Recreate password'),
                        'url' => Url::toRoute(['/user/admin/password-recreate', 'id' => $user->id]),
                        'visible' => Yii::$app->user->can('users-edit'),
                        'linkOptions' => [
                            'class' => 'text-success',
                            'data-method' => 'post',
                            'data-confirm' => Yii::t('user', 'Are you sure you want to recreate password for this user? User will receive email with new password.'),
                        ],
                    ],
                    [
                        'label' => Yii::t('user', 'Login'),
                        'url' => Url::toRoute(['auth/secondary', 'id' => $user->id]),
                        'visible' => Yii::$app->user->can('admin')
                    ],
                ],
            ])
            ?>
        </div>
    </div>
    <div class="col-md-9">
        <?= $content ?>
    </div>
</div>
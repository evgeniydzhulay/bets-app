<?php

use common\modules\user\models\UserModel;
use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var View 	$this
 * @var UserModel 	$user
 */
$this->title = Yii::t('user', 'Create a user account');
$this->params['breadcrumbs'][] = ['label' => Yii::t('user', 'Users'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <?php
    $form = ActiveForm::begin([
                'layout' => 'horizontal',
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'fieldConfig' => [
                    'horizontalCssClasses' => [
                        'wrapper' => 'col-sm-9',
                    ],
                ],
    ]);
    ?>
    <div class="box-body">
        <div class="alert alert-info">
            <?= Yii::t('user', 'Credentials will be sent to the user by email') ?>.
            <?= Yii::t('user', 'A password will be generated automatically if not provided') ?>.
        </div>
        <?= $this->render('_user', ['form' => $form, 'user' => $user]) ?>
    </div>
    <div class="box-footer">
        <div class="row">
            <div class="col-lg-offset-3 col-lg-9">
                <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-block btn-success']) ?>
            </div>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
<?php

use common\modules\user\models\UserModel;
use yii\web\View;

/**
 * @var View $this
 * @var UserModel $user
 */

?>

<?php $this->beginContent('@common/modules/user/views/admin/update.php', ['user' => $user]) ?>

<div class="box box-primary">
    <div class="box-body">
        <table class="table">
            <tr>
                <td><strong><?= Yii::t('user', 'Registration time') ?>:</strong></td>
                <td><?= date('Y-m-d H:i:s', $user->created_at); ?></td>
            </tr>
            <?php if ($user->registration_ip !== null): ?>
                <tr>
                    <td><strong><?= Yii::t('user', 'Registration IP') ?>:</strong></td>
                    <td><?= $user->registration_ip ?></td>
                </tr>
            <?php endif ?>
            <tr>
                <td><strong><?= Yii::t('user', 'Confirmation status') ?>:</strong></td>
                <?php if ($user->isConfirmed): ?>
                    <td class="text-success"><?= Yii::t('user', 'Confirmed at {0, date, MMMM dd, YYYY HH:mm}', [$user->confirmed_at]) ?></td>
                <?php else: ?>
                    <td class="text-danger"><?= Yii::t('user', 'Unconfirmed') ?></td>
                <?php endif ?>
            </tr>
            <tr>
                <td><strong><?= Yii::t('user', 'Block status') ?>:</strong></td>
                <?php if ($user->isBlocked): ?>
                    <td class="text-danger"><?= Yii::t('user', 'Blocked at {0, date, MMMM dd, YYYY HH:mm}', [$user->blocked_at]) ?></td>
                <?php else: ?>
                    <td class="text-success"><?= Yii::t('user', 'Not blocked') ?></td>
                <?php endif ?>
            </tr>
        </table>
    </div>
</div>

<?php $this->endContent() ?>

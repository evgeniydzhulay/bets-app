<?php /** @noinspection MissedFieldInspection */

use common\modules\user\controllers\AuthController;
use common\modules\user\controllers\TwoFactorController;
use common\modules\user\models\UserModel;

return [
	'modules' => [
		'user' => [
			'controllerNamespace' => '\backend\modules\user\controllers',
			'viewPath' => '@common/modules/user/views',
			'controllerMap' => [
				'two-factor' => TwoFactorController::class,
			]
		]
	],
	'components' => [
		'view' => [
			'theme' => [
				'pathMap' => [
					'@common/modules/user/views' => '@backend/modules/user/views',
				],
			],
		],
		'user' => [
			'enableAutoLogin' => true,
			'loginUrl' => ['/user/auth/login'],
			'identityClass' => UserModel::class,
		],
	],
];

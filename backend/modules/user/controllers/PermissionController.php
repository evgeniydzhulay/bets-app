<?php

namespace backend\modules\user\controllers;

use common\modules\user\rbac\Permission;
use Yii;
use yii\filters\AccessControl;
use yii\rbac\Item;
use yii\rbac\Permission as YiiPermission;
use yii\web\NotFoundHttpException;

class PermissionController extends UsersItemControllerAbstract {

    /** @var string */
    protected $modelClass = Permission::class;

    /** @var int */
    protected $type = Item::TYPE_PERMISSION;

    /** @inheritdoc */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['rbac-permissions'],
                    ],
                ],
            ],
        ];
    }

    /** @inheritdoc
     * @throws NotFoundHttpException
     */
    protected function getItem($name) {
        $role = Yii::$app->authManager->getPermission($name);
        if ($role instanceof YiiPermission) {
            return $role;
        }
        throw new NotFoundHttpException;
    }

}

<?php

namespace backend\modules\user\controllers;

use common\modules\user\rbac\Role;
use Yii;
use yii\filters\AccessControl;
use yii\rbac\Item;
use yii\rbac\Role as YiiRole;
use yii\web\NotFoundHttpException;

class RoleController extends UsersItemControllerAbstract {

    /** @var string */
    protected $modelClass = Role::class;
    protected $type = Item::TYPE_ROLE;

    /** @inheritdoc */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['rbac-roles'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     * @throws NotFoundHttpException
     */
    protected function getItem($name) {
        $role = Yii::$app->authManager->getRole($name);
        if ($role instanceof YiiRole) {
            return $role;
        }
        throw new NotFoundHttpException;
    }

}

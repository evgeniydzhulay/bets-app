<?php

namespace backend\modules\user\controllers;

use common\modules\user\search\PurseHistorySearch;
use common\modules\user\search\UserPurseDepositSearch;
use Yii;
use common\modules\user\models\UserPurseDepositModel;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class DepositsController extends Controller {

	/** @inheritdoc */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index', 'view'],
						'roles' => ['users-view'],
					],
					[
						'allow' => true,
						'actions' => ['accept'],
						'roles' => ['users-deposit-accept'],
					],
				],
			],
		];
	}

	public function actionIndex() {
		$searchModel = Yii::createObject(UserPurseDepositSearch::class);
		$dataProvider = $searchModel->search(Yii::$app->request->get());
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'statuses' => PurseHistorySearch::statuses(),
			'services' => PurseHistorySearch::sources(),
		]);
	}

	public function actionView($id) {
		$model = $this->getModel(['id' => $id]);
		return $this->render('view', [
			'model' => $model
		]);
	}

	public function actionAccept($id) {
		$model = $this->getModel(['id' => $id]);
		if ($model->accept()) {
			Yii::$app->getSession()->addFlash('success', Yii::t('user', 'User deposit was accepted'));
		}
		return $this->redirect(['view', 'id' => $model->id]);
	}


	protected function getModel($params) {
		$model = UserPurseDepositModel::find()->andWhere($params)->limit(1)->one();
		if(!$model) {
			throw new NotFoundHttpException('Deposit not found');
		}
		return $model;
	}
}
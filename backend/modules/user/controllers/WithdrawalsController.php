<?php

namespace backend\modules\user\controllers;

use common\modules\user\models\UserPurseWithdrawModel;
use common\modules\user\search\PurseHistorySearch;
use common\modules\user\search\UserPurseWithdrawalSearch;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class WithdrawalsController extends Controller {

	public function actionIndex() {
		$searchModel = Yii::createObject(UserPurseWithdrawalSearch::class);
		$dataProvider = $searchModel->search(Yii::$app->request->get());
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'statuses' => PurseHistorySearch::statuses(),
			'services' => PurseHistorySearch::sources(),
			'paymentServices' => Yii::$app->getModule('user')->get('payment')->getWithdrawalServices(),
		]);
	}

	public function actionView($id) {
		$model = $this->getModel(['id' => $id]);
		return $this->render('view', [
			'model' => $model
		]);
	}


	protected function getModel($params) {
		$model = UserPurseWithdrawModel::find()->andWhere($params)->limit(1)->one();
		if(!$model) {
			throw new NotFoundHttpException('Withdrawal not found');
		}
		return $model;
	}
}
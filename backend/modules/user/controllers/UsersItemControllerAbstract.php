<?php

namespace backend\modules\user\controllers;

use common\modules\user\rbac\Permission;
use common\modules\user\rbac\Role;
use common\modules\user\rbac\Search;
use Yii;
use yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\base\Model;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

abstract class UsersItemControllerAbstract extends Controller {

    /**
     * @param  string $name
     * @return Role|Permission
     */
    abstract protected function getItem($name);

    /**
     * @var int
     */
    protected $type;

    /**
     * @var string
     */
    protected $modelClass;

    /**
     * @throws InvalidConfigException
     */
    public function init() {
        parent::init();
        if ($this->modelClass === null) {
            throw new InvalidConfigException('Model class should be set');
        }
        if ($this->type === null) {
            throw new InvalidConfigException('Auth item type should be set');
        }
    }

    /**
     * Lists all created items.
     * @return string
     * @throws InvalidConfigException
     */
    public function actionIndex() {
        $filterModel = new Search($this->type);
        return $this->render('index', [
                    'filterModel' => $filterModel,
                    'dataProvider' => $filterModel->search(Yii::$app->request->get()),
        ]);
    }

    /**
     * Shows create form.
     * @return string|Response
     * @throws ExitException
     * @throws InvalidConfigException
     */
    public function actionCreate() {
        /** @var Role|Permission $model */
        $model = Yii::createObject([
                    'class' => $this->modelClass,
                    'scenario' => 'create',
        ]);

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Shows update form.
     * @param  string $name
     * @return string|Response
     * @throws ExitException
     * @throws InvalidConfigException
     */
    public function actionUpdate($name) {
        /** @var Role|Permission $model */
        $item = $this->getItem($name);
        $model = Yii::createObject([
                    'class' => $this->modelClass,
                    'scenario' => 'update',
                    'item' => $item,
        ]);

        $this->performAjaxValidation($model);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['index']);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes item.
     * @param  string $name
     * @return Response
     */
    public function actionDelete($name) {
        $item = $this->getItem($name);
        Yii::$app->authManager->remove($item);
        return $this->redirect(['index']);
    }

    /**
     * Performs ajax validation.
     * @param Model $model
     * @throws ExitException
     */
    protected function performAjaxValidation(Model $model) {
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            echo json_encode(ActiveForm::validate($model));
            Yii::$app->end();
        }
    }

}

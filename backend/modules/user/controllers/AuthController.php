<?php

namespace backend\modules\user\controllers;

use common\modules\user\controllers\AuthController AS BaseAuthController;
use common\modules\user\forms\SettingsForm;
use yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\web\Response;
use Yii;

class AuthController extends BaseAuthController {

	/**
	 * Displays page where user can update account settings (email or password).
	 *
	 * @return string|Response
	 * @throws ExitException
	 * @throws InvalidConfigException
	 */
	public function actionAccount() {
		/** @var SettingsForm $modelAccount */
		$modelAccount = Yii::createObject(SettingsForm::class);
		return $this->render('account', [
			'modelAccount' => $modelAccount,
			'enabled2fa' => !!Yii::$app->user->identity->auth_tf_enabled,
		]);
	}

}
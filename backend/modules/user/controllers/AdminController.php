<?php

namespace backend\modules\user\controllers;

use common\modules\user\models\ProfileModel;
use common\modules\user\models\UserModel;
use common\modules\user\rbac\Assignment;
use common\modules\user\search\UserSearch;
use common\modules\user\traits\UserEventTrait;
use common\traits\AjaxValidationTrait;
use Yii;
use yii\base\InvalidConfigException;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * AdminController allows you to administrate users.
 */
class AdminController extends Controller {

    use UserEventTrait;

    use AjaxValidationTrait;

    /**
     * Event is triggered before creating new user.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_BEFORE_CREATE = 'beforeCreate';

    /**
     * Event is triggered after creating new user.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_AFTER_CREATE = 'afterCreate';

    /**
     * Event is triggered before updating existing user.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_BEFORE_UPDATE = 'beforeUpdate';

    /**
     * Event is triggered after updating existing user.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_AFTER_UPDATE = 'afterUpdate';

    /**
     * Event is triggered before updating existing user's profile.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_BEFORE_PROFILE_UPDATE = 'beforeProfileUpdate';

    /**
     * Event is triggered after updating existing user's profile.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_AFTER_PROFILE_UPDATE = 'afterProfileUpdate';

    /**
     * Event is triggered before confirming existing user.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_BEFORE_CONFIRM = 'beforeConfirm';

    /**
     * Event is triggered after confirming existing user.
     * Triggered with \common\modules\user\events\UserEvent.
     */
    const EVENT_AFTER_CONFIRM = 'afterConfirm';

    /** @inheritdoc */
    public function behaviors() {
        return [
            'verbs' => [
                'class' => VerbFilter::class,
                'actions' => [
                    'delete' => ['post'],
                    'confirm' => ['post'],
                    'block' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['index', 'info', 'list'],
                        'roles' => ['users-view'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['create', 'block', 'confirm', 'update', 'update-profile', 'password-recreate'],
                        'roles' => ['users-edit'],
                    ],
                    [
                        'allow' => true,
                        'actions' => ['assignments'],
                        'roles' => ['users-assignments'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     *
     * @return mixed
     * @throws InvalidConfigException
     */
    public function actionIndex() {
        Url::remember('', 'actions-redirect');
        $searchModel = Yii::createObject(UserSearch::class);
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'searchModel' => $searchModel,
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'index' page.
     *
     * @return mixed
     * @throws \yii\base\ExitException
     * @throws InvalidConfigException
     */
    public function actionCreate() {
        /** @var UserModel $user */
        $user = Yii::createObject([
	        'class' => UserModel::class,
	        'scenario' => 'create',
        ]);
        $event = $this->getUserEvent($user);
        $this->performAjaxValidation($user);
        $this->trigger(self::EVENT_BEFORE_CREATE, $event);
        if ($user->load(Yii::$app->request->post()) && $user->create()) {
            Yii::$app->getSession()->addFlash('success', Yii::t('user', 'User has been created'));
            $this->trigger(self::EVENT_AFTER_CREATE, $event);
            return $this->redirect(['update', 'id' => $user->id]);
        }
        return $this->render('create', [
            'user' => $user,
        ]);
    }

    /**
     * Updates an existing User model.
     *
     * @param int $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     * @throws InvalidConfigException
     */
    public function actionUpdate($id) {
        Url::remember('', 'actions-redirect');
	    $user = $this->getUserModel(['id' => $id]);
        $user->scenario = 'update';
        $event = $this->getUserEvent($user);
        $this->performAjaxValidation($user);
        $this->trigger(self::EVENT_BEFORE_UPDATE, $event);
        if ($user->load(Yii::$app->request->post()) && $user->save()) {
            Yii::$app->getSession()->addFlash('success', Yii::t('user', 'Account details have been updated'));
            $this->trigger(self::EVENT_AFTER_UPDATE, $event);
            return $this->refresh();
        }
        return $this->render('_account', [
            'user' => $user,
        ]);
    }

    /**
     * Updates an existing profile.
     *
     * @param int $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     * @throws \yii\base\ExitException
     * @throws InvalidConfigException
     */
    public function actionUpdateProfile($id) {
        Url::remember('', 'actions-redirect');
	    $user = $this->getUserModel(['id' => $id]);
        $profile = $user->profile;
        if ($profile == null) {
            $profile = Yii::createObject(ProfileModel::class);
            $profile->link('user', $user);
        }
        $event = $this->getProfileEvent($profile);
        $this->performAjaxValidation($profile);
        $this->trigger(self::EVENT_BEFORE_PROFILE_UPDATE, $event);
        if ($profile->load(Yii::$app->request->post()) && $profile->save()) {
            Yii::$app->getSession()->addFlash('success', Yii::t('user', 'Profile details have been updated'));
            $this->trigger(self::EVENT_AFTER_PROFILE_UPDATE, $event);
            return $this->refresh();
        }

        return $this->render('_profile', [
            'user' => $user,
            'profile' => $profile,
        ]);
    }

    /**
     * Shows information about user.
     *
     * @param int $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionInfo($id) {
        Url::remember('', 'actions-redirect');
	    $user = $this->getUserModel(['id' => $id]);
        return $this->render('_info', [
            'user' => $user,
        ]);
    }

    /**
     * @param int $id
     *
     * @return string
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     * @throws \Exception
     */
    public function actionAssignments($id) {
        Url::remember('', 'actions-redirect');
	    $user = $this->getUserModel(['id' => $id]);
        $model = Yii::createObject([
            'class' => Assignment::class,
            'user_id' => $id,
        ]);
        if ($model->load(Yii::$app->request->post())) {
            $model->updateAssignments();
        }
        return $this->render('_assignments', [
            'model' => $model,
            'user' => $user
        ]);
    }

    /**
     * Confirms the User.
     *
     * @param int $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     */
    public function actionConfirm($id) {
	    $model = $this->getUserModel(['id' => $id]);
        $event = $this->getUserEvent($model);
        $this->trigger(self::EVENT_BEFORE_CONFIRM, $event);
        $model->confirm();
        $this->trigger(self::EVENT_AFTER_CONFIRM, $event);
        Yii::$app->getSession()->addFlash('success', Yii::t('user', 'User has been confirmed'));
        return $this->redirect(Url::previous('actions-redirect'));
    }

    /**
     * Recreates user password
     *
     * @param int $id
     *
     * @return Response
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionPasswordRecreate($id) {
	    $user = $this->getUserModel(['id' => $id]);
        $user->resetPassword(null, true);
        return $this->redirect(Yii::$app->request->referrer);
    }

    /**
     * Blocks the user.
     *
     * @param int $id
     *
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionBlock($id) {
        if ($id == Yii::$app->user->getId()) {
            Yii::$app->getSession()->addFlash('danger', Yii::t('user', 'You can not block your own account'));
        } else {
            $user = $this->getUserModel(['id' => $id]);
            if ($user->getIsBlocked()) {
                $user->unblock();
                Yii::$app->getSession()->addFlash('success', Yii::t('user', 'User has been unblocked'));
            } else {
                $user->block();
                Yii::$app->getSession()->addFlash('success', Yii::t('user', 'User has been blocked'));
            }
        }

        return $this->redirect(Url::previous('actions-redirect'));
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param array|int $params
     *
     * @return UserModel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function getUserModel($params) : UserModel {
	    $user = UserModel::find()->andWhere($params)->one();
        if ($user === null) {
            throw new NotFoundHttpException('The requested page does not exist');
        }
        return $user;
    }

    /**
     * @return array
     */
    public function actionList() {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $list = ProfileModel::find()->andFilterWhere(['or',
            ['like', ProfileModel::tableName() . '.name', Yii::$app->request->get('name', NULL)],
            ['like', ProfileModel::tableName() . '.surname', Yii::$app->request->get('name', NULL)]
        ])->asArray()->limit(100)->all();
        $result = [];
        foreach ($list AS $item) {
            $result[] = [
                'id' => $item['user_id'],
                'name' => "{$item['name']} {$item['surname']}"
            ];
        }
        return ['results' => $result];
    }

}

<?php

return [
	'modules' => [
		'partners' => [
			'controllerNamespace' => '\backend\modules\partners\controllers',
			'viewPath' => '@backend/modules/partners/views'
		],
	],
];

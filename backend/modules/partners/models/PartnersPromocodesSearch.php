<?php

namespace backend\modules\partners\models;

use common\modules\partners\models\PartnersSiteModel;
use common\modules\partners\models\PromoCampaignsModel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\partners\models\PartnersPromocodesModel;


class PartnersPromocodesSearch extends PartnersPromocodesModel
{

	public $campaign_name;
	public $site_name;

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'campaign_name';
		$scenarios[self::SCENARIO_SEARCH][] = 'site_name';
		return $scenarios;
	}

	public function rules() {
		$rules = parent::rules();
		$rules[] = [['site_name', 'campaign_name'], 'string'];
		return $rules;
	}


	public function search($params) {
		$query = static::find();

		$query->joinWith(['site','campaign']);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);

		if(!$this->validate()){
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
		self::tableName()	. '.id' => $this->id,
		self::tableName()	. '.partner_id' => $this->partner_id,
		self::tableName()	. '.campaign_id' => $this->campaign_id,
		self::tableName()	. '.site_id' => $this->site_id,
		self::tableName()	. '.is_active' => $this->is_active,

		]);

		$query->andFilterWhere(['like', 'code', $this->code])
			->andFilterWhere(['like', PartnersSiteModel::tableName() . '.address', $this->site_name])
			->andFilterWhere(['like', PromoCampaignsModel::tableName() . '.title', $this->campaign_name]);

		return $dataProvider;
	}
}
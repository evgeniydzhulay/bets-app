<?php

namespace backend\modules\partners\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\partners\models\PromoCampaignsModel;


class PromoCampaignsSearch extends PromoCampaignsModel
{

    public function search($params)
    {
        $query = static::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);
        if (!$this->validate()) {
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'status' => $this->status,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'lang_code', $this->lang_code])
            ->andFilterWhere(['like', 'currency_code', $this->currency_code]);

        return $dataProvider;
    }
}

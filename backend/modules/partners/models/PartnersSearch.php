<?php

namespace backend\modules\partners\models;

use common\modules\partners\models\PartnersModel;
use common\modules\user\models\ProfileModel;
use common\modules\user\models\UserModel;
use yii\data\ActiveDataProvider;

class PartnersSearch extends PartnersModel {

	public $owner_name;
	public $owner_surname;
	public $user_name;
	public $owner_email;

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios['search'][] = 'user_name';
		return $scenarios;
	}

	public function rules() {
		$rules = parent::rules();
		$rules[] = [['user_name'], 'string'];
		return $rules;
	}

	public function search($params) {
		$query = static::find()->joinWith(['ownerProfile','owner'], false)->select([
			self::tableName() . '.*',
			'owner_name' => ProfileModel::tableName() . '.name',
			'owner_surname' => ProfileModel::tableName() . '.surname',
			'owner_email' => UserModel::tableName() . '.email'
		]);
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
				'pageSizeLimit' => false
			],
			'sort' => [
				'defaultOrder' => [
					'id' => SORT_DESC
				]
			]
		]);
		if ($this->load($params) && $this->validate()) {
			$query->andFilterWhere(['like', self::tableName() . '.payment_number', $this->payment_number]);
			$query->andFilterWhere([
				self::tableName() . '.payment_method' => $this->payment_method,
				self::tableName() . '.owner_id' => $this->owner_id,
				self::tableName() . '.status' => $this->status,
			]);
			if($this->user_name){
				$query->orFilterWhere(['like', ProfileModel::tableName() . '.name', $this->user_name]);
				$query->orFilterWhere(['like', ProfileModel::tableName() . '.surname', $this->user_name]);
			}
		}
		return $dataProvider;
	}

	/** {@inheritdoc} */
	public function formName () {
		return '';
	}
}
<?php

namespace backend\modules\partners\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\partners\models\PromoMaterialsModel;


class PromoMaterialsSearch extends PromoMaterialsModel
{

	public function search($params) {
		$query = static::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);

		if(!$this->validate()){
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'type' => $this->type,
			'size_h' => $this->size_h,
			'size_w' => $this->size_w,
			'status' => $this->status,
		]);

		$query->andFilterWhere(['like', 'path', $this->path])
			->andFilterWhere(['like', 'title', $this->title])
			->andFilterWhere(['like', 'lang_code', $this->lang_code])
			->andFilterWhere(['like', 'currency_code', $this->currency_code]);

		return $dataProvider;
	}
}

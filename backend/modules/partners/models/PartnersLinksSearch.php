<?php

namespace backend\modules\partners\models;

use common\modules\partners\models\PartnersSiteModel;
use common\modules\partners\models\PromoCampaignsModel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\partners\models\PartnersLinksModel;


class PartnersLinksSearch extends PartnersLinksModel
{
	public $campaign_name;
	public $site_name;

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'campaign_name';
		$scenarios[self::SCENARIO_SEARCH][] = 'site_name';
		return $scenarios;
	}

	public function rules() {
		$rules = parent::rules();
		$rules[] = [['site_name','campaign_name'],'string'];
		return $rules;
	}

	public function search($params)
	{
		$query = static::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['created_at' => SORT_DESC],
				'attributes' => ['created_at']
			]
		]);

		$query->joinWith(['site','campaign']);

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}


		$query->andFilterWhere([
			$this::tableName() . '.id' => $this->id,
			$this::tableName() . '.partner_id' => $this->partner_id,
			$this::tableName() . '.created_at' => $this->created_at,
			$this::tableName() . '.site_id' => $this->site_id,
			$this::tableName() . '.campaign_id' => $this->campaign_id,
		]);

		$query->andFilterWhere(['like', 'link', $this->link])
			->andFilterWhere(['like', 'description', $this->description])
			->andFilterWhere(['like', 'page_url', $this->page_url])
			->andFilterWhere(['like', 'page_sid', $this->page_sid])
			->andFilterWhere(['like', PartnersSiteModel::tableName() . '.address', $this->site_name])
			->andFilterWhere(['like', PromoCampaignsModel::tableName() . '.title', $this->campaign_name]);

		return $dataProvider;
	}
}


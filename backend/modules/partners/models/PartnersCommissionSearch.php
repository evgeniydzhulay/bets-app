<?php

namespace backend\modules\partners\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\partners\models\PartnersComissionModel;

/**
 * PartnersCommissionSearch represents the model behind the search form of `common\modules\partners\models\PartnersComissionModel`.
 */
class PartnersCommissionSearch extends PartnersComissionModel
{

	public function search($params)
	{
		$query = static::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'partner_id' => $this->partner_id,
			'deposit_profit' => $this->deposit_profit,
			'deposit_min' => $this->deposit_min,
			'profit_percent' => $this->profit_percent,
			'created_at' => $this->created_at,
		]);

		$query->andFilterWhere(['like', 'currency_code', $this->currency_code]);

		return $dataProvider;
	}
}

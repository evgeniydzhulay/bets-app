<?php

namespace backend\modules\partners\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\partners\models\PartnersSiteModel;


class PartnersSiteSearch extends PartnersSiteModel
{

	public function search($params)
	{
		$query = static::find();


		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);
		if (!$this->validate()) {
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			'id' => $this->id,
			'partner_id' => $this->partner_id,
			'category_id' => $this->category_id,
			'is_active' => $this->is_active,
		]);

		$query->andFilterWhere(['like', 'address', $this->address])
			->andFilterWhere(['like', 'lang_code', $this->lang_code]);

		return $dataProvider;
	}
}

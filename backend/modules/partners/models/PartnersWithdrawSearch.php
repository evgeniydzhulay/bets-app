<?php

namespace backend\modules\partners\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\partners\models\PartnersWithdrawModel;

class PartnersWithdrawSearch extends PartnersWithdrawModel
{

	public $sent_date_start;
	public $sent_date_end;
	public $created_date_start;
	public $created_date_end;

	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'sent_date_start';
		$scenarios[self::SCENARIO_SEARCH][] = 'sent_date_end';
		$scenarios[self::SCENARIO_SEARCH][] = 'created_date_start';
		$scenarios[self::SCENARIO_SEARCH][] = 'created_date_end';
		return $scenarios;
	}

	public function rules() {
		$rules[] = [['status'],'integer'];
		$rules[] = [['sent_date_start','sent_date_end','created_date_start','created_date_end','created_at','sent_at','amount'], 'safe'];
		return $rules;
	}


	public function search($params)
	{
		$query = static::find();

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
		]);

		$this->load($params);

		if (!$this->validate()) {
			return $dataProvider;
		}

		$query->andFilterWhere([
			'id' => $this->id,
			'partner_id' => $this->partner_id,
			//'amount' => $this->amount,
			'service' => $this->service,
			'status' => $this->status,
			//'created_at' => $this->created_at,
			//'sent_at' => $this->sent_at,
		]);

		if($this->created_date_end && $this->created_date_start){
			$this->created_date_start = strtotime($this->created_date_start);
			$this->created_date_end = strtotime($this->created_date_end) + 86400;
		}

		if($this->sent_date_end && $this->sent_date_start){
			$this->sent_date_start = strtotime($this->sent_date_start);
			$this->sent_date_end = strtotime($this->sent_date_end) + 86400;
		}

		$query->andFilterWhere(['like', 'currency_code', $this->currency_code])
			->andFilterWhere(['like', 'target', $this->target])
			->andFilterWhere(['like', 'target_additional', $this->target_additional])
			->andFilterWhere(['like', 'name', $this->name])
			->andFilterWhere(['like', 'surname', $this->surname])
			->andFilterWhere(['<=', 'created_at', $this->created_date_end])
			->andFilterWhere(['>=', 'created_at', $this->created_date_start])
			->andFilterWhere(['<=', 'sent_at', $this->sent_date_end])
			->andFilterWhere(['>=', 'sent_at', $this->sent_date_start])
			->andFilterWhere(['like', 'amount', $this->amount]);

		return $dataProvider;
	}
}
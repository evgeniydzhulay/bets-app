<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\partners\models\PromoMaterialsModel */

$this->title = Yii::t('partners','Create promo material');
$this->params['breadcrumbs'][] = ['label' => Yii::t('partners','Promo materials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-materials-model-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>

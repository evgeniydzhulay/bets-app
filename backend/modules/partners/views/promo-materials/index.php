<?php

use backend\modules\games\components\GameHelpers;
use common\modules\partners\models\PromoMaterialsModel;
use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\partners\models\PromoMaterialsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('partners', 'Promo materials');
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="">
	<div class="box-header with-border">
	</div>
	<div class="">
		<?php
		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'pjax' => false,
			'id' => 'partners-links',
			'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
			'toolbar' => [
				[
					'content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['index']), [
							'class' => 'btn btn-warning',
							'style' => 'margin-right:5px',
							'title' => Yii::t('games', 'Reset filter')
						])
						. Html::a('<span class="fa fa-plus"></span>', Url::to(['create']), [
							'class' => 'btn btn-default',
							'data-pjax' => 0,
						])
				]
			],
			'columns' => [
				'title',
				[
					'label' => '',
					'headerOptions' => ['style' => 'width:30px'],
					'format' => 'raw',
					'value' => function($data){
						return ($data->path) ? Html::img('/uploads/partners/' . $data->path,['style' => 'height:40px']) : '';
					}
				],
				[
					'attribute' => 'type',
					'filter' => PromoMaterialsModel::types(),
					'value' => function ($model) {
						return (key_exists($model->type, PromoMaterialsModel::types())) ? PromoMaterialsModel::types()[$model->type] : $model->type;
					}
				],
				[
					'attribute' => 'size_h',
					'headerOptions' => ['width' => '80px']
				],
				[
					'attribute' => 'size_w',
					'headerOptions' => ['width' => '80px']
				],
				[
					'attribute' => 'lang_code',
					'filter' => GameHelpers::languagesList(),
					'value' => function ($model) {
						return GameHelpers::languagesList($model->lang_code);
					}
				],
				[
					'attribute' => 'currency_code',
					'filter' => GameHelpers::currenciesList()
				],
				[
					'attribute' => 'status',
					'filter' => PromoMaterialsModel::statuses(),
					'format' => 'raw',
					'value' => function ($model) {
						return $model->status ? Html::a(Yii::t('partners', 'Disable'), ['set-status', 'id' => $model->id, 'status' => PromoMaterialsModel::STATUS_INACTIVE], ['class' => 'btn btn-danger btn-block btn-xs']) : Html::a(Yii::t('partners', 'Enable'), ['set-status', 'id' => $model->id, 'status' => PromoMaterialsModel::STATUS_ACTIVE], ['class' => 'btn btn-success btn-block btn-xs']);
					}
				],
				[
					'content' => function ($model) {
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['update', 'id' => $model->id]), ['class' => 'btn btn-success btn-block btn-xs']);
					},
					'headerOptions' => ['width' => '60px']
				]
			]]);
		?>
	</div>
</div>

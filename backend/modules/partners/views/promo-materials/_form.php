<?php

use backend\modules\games\components\GameHelpers;
use common\modules\partners\models\PromoMaterialsModel;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\partners\models\PromoMaterialsModel */
/* @var $form yii\widgets\ActiveForm */

$disabled = (!$model->isNewRecord) ? ['disabled' => 'disabled'] : [];
?>

<div class="promo-materials-model-form box box-primary">
	<div class="text-right" style="margin: 15px">
		<?= Html::a(Yii::t('games', 'Close'), ['index'], ['class' => 'btn btn-warning']) ?>
	</div>
	<?php $form = ActiveForm::begin(); ?>
	<div class="box-body table-responsive">
		<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
		<div class="row">
			<div class="col-md-4">
				<?= $form->field($model, 'currency_code')->dropDownList(GameHelpers::currenciesList(), ['prompt' => 'Please select'] + $disabled) ?>
			</div>
			<div class="col-md-4">
				<?= $form->field($model, 'lang_code')->dropDownList(GameHelpers::languagesList(), ['prompt' => 'Please select'] + $disabled) ?>
			</div>
			<div class="col-md-4">
				<?php if($model->isNewRecord){
					$model->status = PromoMaterialsModel::STATUS_ACTIVE;
				} ?>
				<?= $form->field($model, 'status')->dropDownList(PromoMaterialsModel::statuses(), ['prompt' => 'Please select']) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4">
				<?= $form->field($model, 'type')->dropDownList(PromoMaterialsModel::types(), ['prompt' => 'Please select'] + $disabled) ?>
			</div>
			<div class="col-md-4">
				<?= $form->field($model, 'size_h')->textInput($disabled) ?>
			</div>
			<div class="col-md-4">
				<?= $form->field($model, 'size_w')->textInput($disabled) ?>
			</div>
		</div>
		<hr/>
		<?php if(!empty($model->path)): ?>
			<div style="min-width: 80px;height: 80px;border: 1px solid #999; margin: 5px; display: inline-block">
				<img style="height: 80px;" src="/uploads/partners/<?= $model->path ?>"/>
			</div>
		<?php endif; ?>
		<?= $form->field($model, 'path')->fileInput(['maxlength' => true])->label(false) ?>
	</div>
	<div class="box-footer text-center">
		<?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</div>

<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\partners\models\PromoMaterialsModel */

$this->title = Yii::t('partners','Update promo material') . ': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('partners','Promo materials'), 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="promo-materials-model-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>

<?php

use backend\modules\games\components\GameHelpers;
use common\modules\partners\models\PromoCampaignsModel;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\modules\partners\models\PromoCampaignsModel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="promo-campaigns-model-form box box-primary">
	<p>&nbsp;</p>
	<div class="text-right">
		<?= Html::a(Yii::t('games', 'Close'), Url::to(['index']), ['class' => 'btn btn-warning','style' => 'margin-right:15px']) ?>
	</div>
	<?php $form = ActiveForm::begin(); ?>
	<div class="box-body table-responsive">
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
			</div>
			<div class="col-md-6">
				<?= $form->field($model, 'lang_code')->dropDownList(GameHelpers::languagesList(), ['prompt' => 'Please select']) ?>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'currency_code')->dropDownList(GameHelpers::currenciesList(),['prompt' => 'Please select']) ?>
			</div>
			<div class="col-md-6">
				<?php if($model->isNewRecord) {$model->status = PromoCampaignsModel::STATUS_ACTIVE;} ?>
				<?= $form->field($model, 'status')->dropDownList(PromoCampaignsModel::statuses(), ['prompt' => 'Please select']) ?>
			</div>
		</div>
	</div>
	<div class="box-footer text-center">
		<?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-success btn-flat']) ?>
	</div>
	<?php ActiveForm::end(); ?>
</div>

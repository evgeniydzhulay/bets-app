<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model common\modules\partners\models\PromoCampaignsModel */

$this->title = Yii::t('partners','Add promo campaign');
$this->params['breadcrumbs'][] = ['label' => Yii::t('partners','Promo campaigns'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="promo-campaigns-model-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>

<?php

use backend\modules\partners\models\PartnersSearch;
use common\modules\partners\models\PartnersWithdrawModel;
use common\widgets\GridView;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var PartnersSearch $searchModel
 * @var array $statuses
 */
$this->title = Yii::t('partners', 'Partners list');
$this->params['breadcrumbs'][] = $this->title;

Pjax::begin([
	'linkSelector' => '#partners-list .pagination a, #partners-search .search-reset',
	'formSelector' => '#partners-search',
	'id' => 'partners-list_container',
	'options' => [
		'class' => 'content with-sidebar',
	],
	'timeout' => 5000,
	'clientOptions' => [
		'maxCacheLength' => 0
	]
]);
?>
<div class="content-sidebar">
    <?php
    $formSearch = ActiveForm::begin([
        'id' => 'partners-search',
        'options' => [
            'class' => 'box box-primary',
        ],
        'enableAjaxValidation' => false,
        'enableClientValidation' => true,
        'method' => 'GET',
        'action' => ['index']
    ]);
    ?>
    <div class="box-body">
        <?php
        echo $formSearch->field($searchModel,'user_name')->textInput(['placeholder' => Yii::t('partners','User name')])->label(false);
        echo $formSearch->field($searchModel, 'status')->widget(Select2::class, [
            'theme' => Select2::THEME_DEFAULT,
            'data' => $statuses,
            'options' => ['placeholder' => Yii::t('partners', 'Select status')],
            'pluginOptions' => [
                'allowClear' => true,
            ],
        ])->label(false);
        echo $formSearch->field($searchModel,'payment_number')->textInput(['placeholder' => Yii::t('partners','Payment number')])->label(false);
        echo $formSearch->field($searchModel, 'payment_method')->widget(Select2::class, [
	        'theme' => Select2::THEME_DEFAULT,
	        'data' => PartnersWithdrawModel::paymentMethods(),
	        'options' => ['placeholder' => Yii::t('partners', 'Payment method')],
	        'pluginOptions' => [
		        'allowClear' => true,
	        ],
        ])->label(false);
        ?>
    </div>
    <div class="box-footer">
        <?= Html::submitButton(Yii::t('partners', 'Search'), [
            'class' => 'btn btn-block btn-success'
        ]);
        ?>
        <?= Html::a(Yii::t('partners', 'Reset'), ['index'], [
            'class' => 'btn btn-block btn-info search-reset'
        ]);
        ?>
    </div>
    <?php
    ActiveForm::end();
    ?>
</div>
<div class="content-body">
    <?php
    echo GridView::widget([
	    'dataProvider' => $dataProvider,
	    'pjax' => false,
	    'id' => 'partners-list',
	    'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
	    'toolbar' => [
		    [
			    'content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['index']), [
				    'class' => 'btn btn-warning',
				    'style' => 'margin-right:5px',
				    'title' => Yii::t('games', 'Reset filter')
			    ])
		    ]
	    ],
	    'columns' => [
		    [
			    'attribute' => 'status',
			    'headerOptions' => ['width' => '80px'],
			    'content' => function ($model) use ($statuses) {
				    return $statuses[$model['status']];
			    }
		    ],
		    [
			    'label' => '',
			    'value' => function($model){
				    return Html::a('<span class="fa fa-user"></span>', Url::to(['/user/admin/update','id' => $model->owner_id]),['class' => 'btn btn-primary btn-xs','title' => Yii::t('user','Profile details'),'target' => '_blank']);
			    },
			    'format' => 'raw',
			    'headerOptions' => ['width' => '38px'],
		    ],
		    [
			    'label' => '',
			    'value' => function($model){
				    return Html::a('<span class="fa fa-folder-open"></span>', Url::to(['/partners/list/partners-links','partner_id' => $model->id]),['class' => 'btn btn-warning btn-xs','title' => Yii::t('partners',"Partner's resources") ]);
			    },
			    'format' => 'raw',
			    'headerOptions' => ['width' => '38px'],
		    ],
		    [
			    'attribute' => 'owner',
			    'header' => Yii::t('partners', 'Owner'),
			    'content' => function ($model) {
				    $name = $model->owner_name . ' ' . $model->owner_surname;
				    return  ($name == ' ') ? $model->owner_email : $name;
			    },
		    ],
		    [
		    	 'attribute' => 'payment_method',
			    'value' => function($model){
    	            return key_exists($model->payment_method, PartnersWithdrawModel::paymentMethods()) ? PartnersWithdrawModel::paymentMethods()[$model->payment_method] : $model->payment_method;
			    }
		    ],
		    [
			    'attribute' => 'payment_number'
		    ]
	    ],
    ]);
    ?>
</div>
<?php
Pjax::end();
?>
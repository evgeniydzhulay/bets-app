<?php

/* @var $this \yii\web\View */

use backend\modules\games\components\GameHelpers;
use common\modules\partners\models\PartnersSiteModel;
use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $partner array|\common\modules\partners\models\PartnersModel|null */
/* @var $searchModel \backend\modules\partners\models\PartnersSiteSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = Yii::t('partners', 'Site');
$this->params['breadcrumbs'][] = ['label' => Yii::t('partners', 'Partners list'), 'url' => ['/partners/list/index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $this->beginContent('@backend/modules/partners/views/list/resources.php', ['model' => $partner]) ?>

<?php
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'pjax' => false,
	'id' => 'partners-promo-codes',
	'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
	'toolbar' => [
		[
			'content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['partners-commission', 'partner_id' => $partner->id]), [
					'class' => 'btn btn-warning',
					'style' => 'margin-right:5px',
					'title' => Yii::t('games', 'Reset filter')
				]) . Html::a('<span class="fa fa-plus"></span>', Url::to(['create-commission', 'partner_id' => $partner->id]), [
					'class' => 'btn btn-default',
					'data-pjax' => 0,
				])
		]
	],
	'columns' => [
		'deposit_profit',
		'deposit_min',
		'profit_percent',
		[
			'attribute' => 'currency_code',
			'filter' => GameHelpers::currenciesList()
		],
		[
			'attribute' => 'created_at',
			'filter' => false,
			'value' => function ($model) {
				return Yii::$app->formatter->asDate($model->created_at, 'php:d/m/Y H:i');
			}
		]
	],
]);
?>

<?php $this->endContent() ?>


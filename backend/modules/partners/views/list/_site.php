<?php

/* @var $this \yii\web\View */

use common\modules\partners\models\PartnersSiteModel;
use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $partner array|\common\modules\partners\models\PartnersModel|null */
/* @var $searchModel \backend\modules\partners\models\PartnersSiteSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = Yii::t('partners', 'Site');
$this->params['breadcrumbs'][] = ['label' => Yii::t('partners', 'Partners list'), 'url' => ['/partners/list/index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $this->beginContent('@backend/modules/partners/views/list/resources.php',['model' => $partner]) ?>

<?php
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'pjax' => false,
	'id' => 'partners-promo-codes',
	'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
	'toolbar' => [
		[
			'content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['partners-site','partner_id' => $partner->id]), [
				'class' => 'btn btn-warning',
				'style' => 'margin-right:5px',
				'title' => Yii::t('games', 'Reset filter')
			])
		]
	],
	'columns' => [
		'address',
		[
			'attribute' => 'is_active',
			'filter' => PartnersSiteModel::statuses(),
			'value' => function($model){
				$class = $model->is_active ? 'btn btn-danger btn-xs btn-block' : 'btn btn-success btn-xs btn-block';
				return $model->is_active ? Html::a(Yii::t('partners','Disable'),['set-status','model' => 'site','value' => 0, 'id' => $model->id],['class' => $class]) : Html::a(Yii::t('partners','Enable'),['set-status','model' => 'site','value' => 1, 'id' => $model->id],['class' => $class]);
			},
			'headerOptions' => ['width' => '100px'],
			'format' => 'raw'
		],
		[
			'attribute' => 'category_id',
			'filter' => PartnersSiteModel::categories(),
			'value' => function($model){
			return key_exists($model->category_id,PartnersSiteModel::categories()) ? PartnersSiteModel::categories()[$model->category_id] : $model->category_id;
}
		],
		[
			'attribute' =>	'lang_code'
		],
	],
]);
?>

<?php $this->endContent() ?>


<?php

/* @var $partner array|\common\modules\partners\models\PartnersModel|null */

use backend\modules\games\components\GameHelpers;
use common\modules\partners\models\PartnersWithdrawModel;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $model PartnersWithdrawModel */

$this->title = Yii::t('partners', ($model->isNewRecord) ? 'Create payment' : 'Update payment');
$this->params['breadcrumbs'][] = ['label' => Yii::t('partners', 'Partners list'), 'url' => ['/partners/list/index']];
$this->params['breadcrumbs'][] = $this->title;

$disabled = !$model->isNewRecord ? ['disabled' => 'disabled'] : [];
?>

<?php $this->beginContent('@backend/modules/partners/views/list/resources.php', ['model' => $partner]) ?>
<div class="text-right">
	<?= Html::a(Yii::t('games', 'Close'),\yii\helpers\Url::to(['partners-withdraw','partner_id' => $partner->id]), ['class' => 'btn btn-warning']) ?>
</div>
<?php $form = ActiveForm::begin(); ?>
<div class="row">
	<div class="col-md-3"><?= $form->field($model, 'amount')->textInput($disabled); ?></div>
	<div class="col-md-3"><?= $form->field($model, 'service')->dropDownList(PartnersWithdrawModel::paymentMethods(), ['prompt' => 'Please select '] + $disabled); ?></div>
	<div class="col-md-3"><?= $form->field($model, 'status')->dropDownList(PartnersWithdrawModel::statuses()); ?>  </div>
	<div class="col-md-3"><?= $form->field($model, 'currency_code')->dropDownList(GameHelpers::currenciesList(), ['prompt' => 'Please select '] + $disabled); ?> </div>
</div>
<?= $form->field($model, 'comment')->textarea(['rows' => 6]); ?>
<div class="text-center">
	<?= Html::submitButton(Yii::t('games','Save'), ['class' => 'btn btn-primary']) ?>
</div>
<?php ActiveForm::end() ?>
<?php $this->endContent() ?>

<?php

use yii\helpers\Url;
use yii\bootstrap\Nav;

?>
<div class="row">
	<div class="col-md-2">
		<div class="box box-primary">
			<div class="box-body">
				<?= Nav::widget([
					'options' => [
						'id' => 'admin-user-manu',
						'class' => 'nav-pills nav-stacked',
					],
					'items' => [
						[
							'label' => $model->ownerProfile->name . ' ' . $model->ownerProfile->surname,
							'url' => '#',
							'linkOptions' => ['style' => 'cursor:default','class' => 'bg bg-red']
						],
						[
							'label' => Yii::t('partners', 'Links'),
							'url' => ['list/partners-links', 'partner_id' => $model->id],
						],
						[
							'label' => Yii::t('partners', 'Promo codes'),
							'url' => ['list/partners-promocodes', 'partner_id' => $model->id],
						],
						[
							'label' => Yii::t('partners', 'Site'),
							'url' => ['list/partners-site', 'partner_id' => $model->id],
						],
						[
							'label' => Yii::t('partners', 'Commission'),
							'url' => ['list/partners-commission', 'partner_id' => $model->id],
						],
						[
							'label' => Yii::t('partners', 'Withdraw'),
							'url' => ['list/partners-withdraw', 'partner_id' => $model->id],
						],
					]
				]);
				?>
			</div>
		</div>
	</div>
	<div class="col-md-10">
		<div class="box box-primary">
			<div class="box-body">
				<?= $content ?>
			</div>
		</div>
	</div>
</div>


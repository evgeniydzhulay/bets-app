<?php

/* @var $this \yii\web\View */
/* @var $partner array|\common\modules\partners\models\PartnersModel|null */
/* @var $searchModel \backend\modules\partners\models\PartnersWithdrawSearch */

/* @var $dataProvider \yii\data\ActiveDataProvider */

use backend\modules\games\components\GameHelpers;
use common\modules\partners\models\PartnersWithdrawModel;
use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('partners', 'Withdraw');
$this->params['breadcrumbs'][] = ['label' => Yii::t('partners', 'Partners list'), 'url' => ['/partners/list/index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $this->beginContent('@backend/modules/partners/views/list/resources.php', ['model' => $partner]) ?>

<?php
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'pjax' => false,
	'id' => 'partners-links',
	'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
	'toolbar' => [
		[
			'content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['partners-withdraw', 'partner_id' => $partner->id]), [
					'class' => 'btn btn-warning',
					'style' => 'margin-right:5px',
					'title' => Yii::t('games', 'Reset filter')
				])
				. Html::a('<span class="fa fa-plus"></span>', Url::to(['create-withdraw', 'partner_id' => $partner->id]), [
					'class' => 'btn btn-default',
					'data-pjax' => 0,
				])
		]
	],
	'columns' => [
		[
			'class' => \kartik\grid\ExpandRowColumn::class,
			'value' => function ($model, $key, $index, $column) {
				return GridView::ROW_COLLAPSED;
			},
			'detail' => function ($model, $key, $index, $column) {
				return Yii::$app->controller->renderPartial('_withdraw_details', ['model' => $model]);
			}
		],
		[
			'attribute' => 'service',
			'filter' => PartnersWithdrawModel::paymentMethods(),
			'value' => function ($model) {
				return key_exists($model->service, PartnersWithdrawModel::paymentMethods()) ? PartnersWithdrawModel::paymentMethods()[$model->service] : $model->service;
			}
		],
		[
			'attribute' => 'amount',
			'value' => function ($model) {
				return $model->amount . ' ' . $model->currency_code;
			}
		],
		[
			'attribute' => 'currency_code',
			'filter' => GameHelpers::currenciesList()
		],
		[
			'attribute' => 'status',
			'headerOptions' => ['width' => '120px'],
			'filter' => PartnersWithdrawModel::statuses(),
			'value' => function ($model) {
				return key_exists($model->status, PartnersWithdrawModel::statuses()) ? PartnersWithdrawModel::statuses()[$model->status] : $model->status;
			}
		],
		[
			'attribute' => 'created_at',
			'filterType' => GridView::FILTER_DATE_RANGE,
			'filterWidgetOptions' => [
				'startAttribute' => 'created_date_start',
				'endAttribute' => 'created_date_end',
				'convertFormat' => true,
				'hideInput' => true,
				'pluginOptions' => [
					'locale' => [
						'format' => 'd.m.Y',
						'separator' => ' - ',
					],
				]
			],
			'filter' => true,
			'value' => function ($model) {
				return Yii::$app->formatter->asDate($model->created_at, 'php: d.m.Y H:i');
			},
			'format' => 'raw'
		],
		[
			'attribute' => 'sent_at',
			'filterType' => GridView::FILTER_DATE_RANGE,
			'filterWidgetOptions' => [
				'startAttribute' => 'sent_date_start',
				'endAttribute' => 'sent_date_end',
				'convertFormat' => true,
				'hideInput' => true,
				'pluginOptions' => [
					'locale' => [
						'format' => 'd.m.Y',
						'separator' => ' - ',
					],
				]
			],
			'filter' => true,
			'value' => function ($model) {
				return Yii::$app->formatter->asDate($model->sent_at, 'php: d.m.Y H:i');
			},
			'format' => 'raw'
		],
		[
			'content' => function ($model) {
				return Html::a('<span class="glyphicon glyphicon-pencil"></span>', Url::to(['update-withdraw', 'id' => $model->id]), ['class' => 'btn btn-success btn-block btn-xs']);
			},
			'headerOptions' => ['width' => '60px']
		]
	],
]);
?>

<?php $this->endContent() ?>


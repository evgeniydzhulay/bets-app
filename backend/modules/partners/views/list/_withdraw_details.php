<?php
/**
 * @var $model \common\modules\partners\models\PartnersWithdrawModel
 */
?>
<div class="" style="padding: 5px;background-color: #fff">
	<table class="table">
		<thead>
			<tr>
				<th><?= $model->getAttributeLabel('target') ?></th>
				<th><?= $model->getAttributeLabel('target_additional') ?></th>
				<th><?= $model->getAttributeLabel('name') ?></th>
				<th><?= $model->getAttributeLabel('surname') ?></th>
				<th><?= $model->getAttributeLabel('comment') ?></th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td><?= $model->target ?></td>
				<td><?= $model->target_additional ?></td>
				<td><?= $model->name ?></td>
				<td><?= $model->surname ?></td>
				<td><?= $model->comment ?></td>
			</tr>
		</tbody>
	</table>
</div>
<?php

/* @var $this \yii\web\View */

use common\modules\partners\models\PartnersPromocodesModel;
use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $partner array|\common\modules\partners\models\PartnersModel|null */
/* @var $searchModel \backend\modules\partners\models\PartnersPromocodesSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

$this->title = Yii::t('partners', 'Promo codes');
$this->params['breadcrumbs'][] = ['label' => Yii::t('partners', 'Partners list'), 'url' => ['/partners/list/index']];
$this->params['breadcrumbs'][] = $this->title;
?>


<?php $this->beginContent('@backend/modules/partners/views/list/resources.php',['model' => $partner]) ?>

<?php
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'pjax' => false,
	'id' => 'partners-promo-codes',
	'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
	'toolbar' => [
		[
			'content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['partners-links','partner_id' => $partner->id]), [
				'class' => 'btn btn-warning',
				'style' => 'margin-right:5px',
				'title' => Yii::t('games', 'Reset filter')
			])
		]
	],
	'columns' => [
		[
			'attribute' => 'site_name',
			'label' => Yii::t('partners', 'Site'),
			'headerOptions' => ['width' => '80px'],
			'value' => function ($model) {
				return (!empty($model->site)) ? $model->site->address . Html::a('<span class="fa fa-eye"></span>', Url::to(['list/partners-site','partner_id' => $model->partner_id,'id' => $model->site->id]), ['target' => '_blank', 'style' => 'margin-left:3px']) : null;
			},
			'format' => 'raw'
		],
		[
			'attribute' => 'campaign_name',
			'label' => Yii::t('partners', 'Campaign'),
			'headerOptions' => ['width' => '80px'],
			'value' => function ($model) {
				return (!empty($model->campaign)) ? $model->campaign->title . Html::a('<span class="fa fa-eye"></span>', Url::to(['promo-campaigns/index','PromoCampaignsSearch' => ['id' => $model->campaign->id]]), ['target' => '_blank', 'style' => 'margin-left:3px']) : null;
			},
			'format' => 'raw'
		],
		[
			'attribute' => 'is_active',
			'filter' => PartnersPromocodesModel::statuses(),
			'value' => function($model){
				$class = $model->is_active ? 'btn btn-danger btn-xs btn-block' : 'btn btn-success btn-xs btn-block';
				return $model->is_active ? Html::a(Yii::t('partners','Disable'),['set-status','model' => 'promocodes','value' => 0, 'id' => $model->id],['class' => $class]) : Html::a(Yii::t('partners','Enable'),['set-status','model' => 'promocodes','value' => 1, 'id' => $model->id],['class' => $class]);
			},
			'headerOptions' => ['width' => '100px'],
			'format' => 'raw'
		],
		'code'
	],
]);
?>

<?php $this->endContent() ?>


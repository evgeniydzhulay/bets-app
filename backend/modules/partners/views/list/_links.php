<?php

/* @var $this \yii\web\View */
/* @var $parent array|\common\modules\user\models\UserModel|null|\yii\db\ActiveRecord */
/* @var $searchModel \common\modules\partners\models\PartnersLinksModel */
/* @var $dataProvider \yii\data\ActiveDataProvider */

/* @var $partner array|\common\modules\user\models\UserModel|null|\yii\db\ActiveRecord */

use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('partners', 'Links');
$this->params['breadcrumbs'][] = ['label' => Yii::t('partners', 'Partners list'), 'url' => ['/partners/list/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<?php $this->beginContent('@backend/modules/partners/views/list/resources.php', ['model' => $partner]) ?>

<?php
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'pjax' => false,
	'id' => 'partners-links',
	'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
	'toolbar' => [
		[
			'content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['partners-links','partner_id' => $partner->id]), [
					'class' => 'btn btn-warning',
					'style' => 'margin-right:5px',
					'title' => Yii::t('games', 'Reset filter')
				])
		]
	],
	'columns' => [
		[
			'attribute' => 'site_name',
			'label' => Yii::t('partners', 'Site'),
			'headerOptions' => ['width' => '80px'],
			'value' => function ($model) {
				return (!empty($model->site)) ? $model->site->address . Html::a('<span class="fa fa-eye"></span>', Url::to(['list/partners-site','partner_id' => $model->partner_id,'id' => $model->site->id]), ['target' => '_blank', 'style' => 'margin-left:3px']) : null;
			},
			'format' => 'raw'
		],
		[
			'attribute' => 'campaign_name',
			'label' => Yii::t('partners', 'Campaign'),
			'headerOptions' => ['width' => '80px'],
			'value' => function ($model) {
				return (!empty($model->campaign)) ? $model->campaign->title . Html::a('<span class="fa fa-eye"></span>', Url::to(['promo-campaigns/index','PromoCampaignsSearch' => ['id' => $model->campaign->id]]), ['target' => '_blank', 'style' => 'margin-left:3px']) : null;
			},
			'format' => 'raw'
		],
		[
			'attribute' => 'link',
			'headerOptions' => ['style' => 'width:90px'],
		],
		[
			'attribute' => 'page_url',
			'headerOptions' => ['width' => '80px'],
		],
		[
			'attribute' => 'page_sid',
			'headerOptions' => ['width' => '80px'],
		],
		[
			'attribute' => 'description',
		],
		[
			'attribute' => 'created_at',
			'headerOptions' => ['width' => '80px'],
			'filter' => false,
			'value' => function ($model) {
				return Yii::$app->formatter->asDate($model->created_at, 'php:d.m.Y H:i');
			}
		],
	],
]);
?>

<?php $this->endContent() ?>


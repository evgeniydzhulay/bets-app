<?php

namespace backend\modules\partners\controllers;

use backend\modules\games\components\GameHelpers;
use backend\modules\partners\models\PartnersCommissionSearch;
use backend\modules\partners\models\PartnersLinksSearch;
use backend\modules\partners\models\PartnersPromocodesSearch;
use backend\modules\partners\models\PartnersSiteSearch;
use backend\modules\partners\models\PartnersWithdrawSearch;
use common\modules\partners\models\PartnersComissionModel;
use common\modules\partners\models\PartnersModel;
use common\modules\partners\models\PartnersPromocodesModel;
use common\modules\partners\models\PartnersSiteModel;
use common\modules\partners\models\PartnersWithdrawModel;
use Yii;
use backend\modules\partners\models\PartnersSearch;
use yii\bootstrap\Html;
use yii\filters\AccessControl;
use yii\web\Controller;

class ListController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}


	public function actionIndex() {
		$searchModel = Yii::createObject(PartnersSearch::class);
		$searchModel->scenario = PartnersSearch::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->get());
		return $this->render('index', [
			'dataProvider' => $dataProvider,
			'searchModel' => $searchModel,
			'statuses' => PartnersSearch::statuses(),
		]);
	}

	public function actionPartnersLinks(int $partner_id) {
		{
			$searchModel = new PartnersLinksSearch();
			$searchModel->scenario = PartnersLinksSearch::SCENARIO_SEARCH;
			$searchModel->partner_id = $partner_id;
			$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
			return $this->render('_links', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'partner' => $this->getPartner($partner_id)
			]);
		}
	}

	public function actionPartnersPromocodes(int $partner_id) {
		{
			$searchModel = new PartnersPromocodesSearch();
			$searchModel->scenario = PartnersPromocodesSearch::SCENARIO_SEARCH;
			$searchModel->partner_id = $partner_id;
			$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
			return $this->render('_promocodes.php', [
				'searchModel' => $searchModel,
				'dataProvider' => $dataProvider,
				'partner' => $this->getPartner($partner_id)
			]);
		}

	}

	public function actionPartnersSite(int $partner_id, $id = false) {
		$searchModel = new PartnersSiteSearch();
		$searchModel->scenario = PartnersSiteSearch::SCENARIO_SEARCH;
		$searchModel->partner_id = $partner_id;
		if($id){ $searchModel->id = (int) $id; }
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('_site.php', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'partner' => $this->getPartner($partner_id)
		]);
	}

	/* Partners Withdraw */

	public function actionPartnersWithdraw(int $partner_id) {
		$searchModel = new PartnersWithdrawSearch();
		$searchModel->scenario = PartnersWithdrawSearch::SCENARIO_SEARCH;
		$searchModel->partner_id = $partner_id;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('_withdraw', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'partner' => $this->getPartner($partner_id)
		]);
	}

	public function actionCreateWithdraw(int $partner_id){
		$model = new PartnersWithdrawModel(['scenario' => PartnersWithdrawModel::SCENARIO_CREATE]);
		$partner = $this->getPartner($partner_id);
		if($model->load(Yii::$app->request->post())){
			$model->name = $partner->ownerProfile->name;
			$model->surname = $partner->ownerProfile->surname;
			$model->sent_at = time();
			$model->created_at = time();
			$model->partner_id = $partner_id;
			if($model->save()){
				GameHelpers::setMessage(true);
				return $this->redirect(['update-withdraw','id' => $model->id]);
			}else{
				GameHelpers::setMessage(false);
			}
		}

		return $this->render('withdraw/_form.php',[
			'model' => $model,
			'partner' => $partner
		]);
	}

	public function actionUpdateWithdraw(int $id){
		$model = PartnersWithdrawModel::findOne($id);
		$model->scenario = PartnersWithdrawModel::SCENARIO_UPDATE;
		if($model == null){
			throw new \Exception('Payment not found');
		}

		$partner = $this->getPartner($model->partner_id);
		if($model->load(Yii::$app->request->post())){
			GameHelpers::setMessage($model->save());
		}

		return $this->render('withdraw/_form.php',[
			'model' => $model,
			'partner' => $partner
		]);
	}

	/* Partners Commission */

	public function actionPartnersCommission(int $partner_id){
		$searchModel = new PartnersCommissionSearch();
		$searchModel->scenario = PartnersComissionModel::SCENARIO_SEARCH;
		$searchModel->partner_id = $partner_id;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('_commission', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'partner' => $this->getPartner($partner_id)
		]);
	}

	public function actionCreateCommission($partner_id){
		$model = new PartnersComissionModel(['scenario' => PartnersComissionModel::SCENARIO_CREATE]);
		$partner = $this->getPartner($partner_id);
		if($model->load(Yii::$app->request->post())){
			$model->partner_id = $partner_id;
			if($model->save()){
				GameHelpers::setMessage(true);
				return $this->redirect(['update-commission','id' => $model->id]);
			}else{
				GameHelpers::setMessage(false);
			}
		}

		return $this->render('commission/_form.php',[
			'model' => $model,
			'partner' => $partner
		]);
	}

	public function actionUpdateCommission($id){
		$model = PartnersComissionModel::findOne($id);
		$model->scenario = PartnersComissionModel::SCENARIO_UPDATE;
		if($model == null){
			throw new \Exception('Data not found');
		}

		$partner = $this->getPartner($model->partner_id);
		if($model->load(Yii::$app->request->post())){
			GameHelpers::setMessage($model->save());
		}

		return $this->render('commission/_form.php',[
			'model' => $model,
			'partner' => $partner
		]);
	}

	/* Helpers */

	private function getPartner($id) {
		$model = PartnersModel::find()->where(['id' => $id])->with(['ownerProfile' => function ($q) {
			$q->select('name,surname');
		}])->one();
		if($model == null){
			throw new \Exception('Partner Not Found');
		}
		return $model;
	}

	public function actionSetStatus(string $model, int $value, int $id) {
		switch($model){
			case 'promocodes' :
				GameHelpers::setMessage(PartnersPromocodesModel::updateAll(['is_active' => $value], ['id' => $id]));
				break;
			case 'site' :
				GameHelpers::setMessage(PartnersSiteModel::updateAll(['is_active' => $value], ['id' => $id]));
				break;
			default:
				GameHelpers::setMessage(false);
		}
		return $this->redirect(Yii::$app->request->referrer);
	}
}
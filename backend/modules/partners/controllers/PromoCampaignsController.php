<?php

namespace backend\modules\partners\controllers;

use backend\modules\games\components\GameHelpers;
use Yii;
use common\modules\partners\models\PromoCampaignsModel;
use backend\modules\partners\models\PromoCampaignsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PromoCampaignsController implements the CRUD actions for PromoCampaignsModel model.
 */
class PromoCampaignsController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}

	public function actionIndex() {
		$searchModel = new PromoCampaignsSearch();
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}


	public function actionCreate() {
		$model = new PromoCampaignsModel(['scenario' => PromoCampaignsModel::SCENARIO_CREATE]);

		if($model->load(Yii::$app->request->post())){
			if($model->save()){
				GameHelpers::setMessage(true);
				return $this->redirect(['update', 'id' => $model->id]);
			}else{
				GameHelpers::setMessage(false);
			}
		}
		return $this->render('create', [
			'model' => $model,
		]);
	}


	public function actionUpdate($id) {
		$model = $this->findModel($id);
		$model->scenario = PromoCampaignsModel::SCENARIO_UPDATE;

		if($model->load(Yii::$app->request->post())){
			if($model->save()){
				GameHelpers::setMessage(true);
				return $this->redirect(['index']);
			}else{
				GameHelpers::setMessage(false);
			}
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionSetStatus(int $id, int $status) {
		$model = $this->findModel($id);
		GameHelpers::setMessage((bool)$model->updateAttributes(['status' => $status]));
		return $this->redirect(Yii::$app->request->referrer);
	}


	protected function findModel($id) {
		if(($model = PromoCampaignsModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}


}

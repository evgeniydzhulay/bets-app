<?php

namespace backend\modules\partners\controllers;

use backend\modules\games\components\GameHelpers;
use Yii;
use common\modules\partners\models\PromoMaterialsModel;
use backend\modules\partners\models\PromoMaterialsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PromoMaterialsController implements the CRUD actions for PromoMaterialsModel model.
 */
class PromoMaterialsController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}

	public function actionIndex() {
		$searchModel = new PromoMaterialsSearch(['scenario' => PromoMaterialsModel::SCENARIO_SEARCH]);
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}


	public function actionCreate() {
		$model = new PromoMaterialsModel(['scenario' => PromoMaterialsModel::SCENARIO_CREATE]);
		if($model->load(Yii::$app->request->post())){
			if($model->save()){
				GameHelpers::setMessage(true);
				return $this->redirect(['update', 'id' => $model->id]);
			}
			GameHelpers::setMessage(false);
		}
		return $this->render('create', [
			'model' => $model,
		]);
	}


	public function actionUpdate($id) {
		$model = $this->findModel($id);
		$model->scenario = PromoMaterialsModel::SCENARIO_UPDATE;
		if($model->load(Yii::$app->request->post())){
			GameHelpers::setMessage($model->save());
		}

		return $this->render('update', [
			'model' => $model,
		]);
	}

	public function actionSetStatus(int $id, int $status) {
		$model = $this->findModel($id);
		GameHelpers::setMessage((bool)$model->updateAttributes(['status' => $status]));
		return $this->redirect(Yii::$app->request->referrer);
	}


	protected function findModel($id) {
		if(($model = PromoMaterialsModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}

<?php

namespace backend\modules\games\components;

use common\modules\games\models\BetsModel;
use Yii;
use yii\helpers\ArrayHelper;
use common\modules\games\helpers\GameHelpers AS BaseGameHelpers;

class GameHelpers extends BaseGameHelpers {

	/**
	 * @param array $locales
	 * @param string $attribute
	 * @param bool|string $truncate_len
	 * @param bool $user_lang
	 * @return string
	 */

	public static function getStringForLocale($locales, $attribute, $truncate_len = false,$user_lang = false) {
		$string = 'Not Set';
		if($locales && is_array($locales)){
			$locales = ArrayHelper::index($locales, 'lang_code');
		}else{
			return $string;
		}

		$language = (key_exists(Yii::$app->language, $locales) && $user_lang) ? Yii::$app->language : 'en';

		if(key_exists($language, $locales)){
			$string = ($locales[$language][$attribute]) ?? $string;

			if(is_int($truncate_len)){
				$string = self::truncate($string, $truncate_len);
			}
		}
		return $string;
	}

	/*public static function getStringForLocale($locales, $attribute, $truncate_len = false) {
		$string = 'Not Set';
		if($locales && is_array($locales)){
			$locales = ArrayHelper::index($locales, 'lang_code');
		}else{
			return $string;
		}

		$language = (key_exists(Yii::$app->language, $locales)) ? Yii::$app->language : 'en';

		if(key_exists($language, $locales)){
			$string = ($locales[$language][$attribute]) ?? $string;

			if(is_int($truncate_len)){
				$string = self::truncate($string, $truncate_len);
			}
		}
		return $string;
	}*/

	/**
	 * @param $string
	 * @param $length
	 * @param string $suffix
	 * @return string
	 */

	public static function truncate($string, $length, $suffix = '...') {
		if(strlen($string) > $length){
			return mb_substr(htmlspecialchars(strip_tags($string)), 0, $length) . $suffix;
		}
		return $string;
	}

	/**
	 * @return array
	 */
	static public function languagesList($locale = false) {
		$languages = [];
		if(Yii::$app->params['languages'] && is_array(Yii::$app->params['languages'])){
			foreach(Yii::$app->params['languages'] as $key => $item){
				$languages[$key] = $item['label'];
			}
		}
		ksort($languages);
		if(!$locale){
			return $languages; // languages list
		}else{
			return (key_exists($locale,$languages)) ? $languages[$locale] : $locale; // language
		}
	}

	static public function currenciesList(){
		$array = [];
		$list = Yii::$app->get('currencies')->getList();
		if(empty($list) || !is_array($list)){
			return [];
		}
		foreach($list as $key => $v){
			$array[$key] = $key;
		}
		return $array;
	}

	static public function findExceptionInLanguages($class, $attribute, $id) {
		$locales = $class::find()->select('lang_code')->where([$attribute => $id])->column();
		$languages = self::languagesList();
		foreach($locales as $locale){
			if(key_exists($locale, $languages)){
				unset($languages[$locale]);
			}
		}
		return $languages ?? [];
	}

	/**
	 * @param bool $result
	 * @param bool|string $value
	 */
	static public function setMessage(bool $result, $value = false) {
		$key = $result ? 'success' : 'warning';
		if(!$value){
			$value = $result ? Yii::t('games', 'All data was successfully saved') : Yii::t('games', 'Some error occurred during saving data');
		}
		Yii::$app->session->addFlash($key, $value);
	}


	static public function colorStatuses($status) {
		switch($status){
			case BetsModel::STATUS_VICTORY :
				return 'text-green';
			case BetsModel::STATUS_DEFEAT :
				return 'text-red';
			case BetsModel::STATUS_REVERTED :
				return 'text-muted';
			default :
				return '';
		}
	}

	/*
	 * EventParticipantsModel => GameParticipantsLocaleModel
	 *
	 * */
	static public function getParticipantsList($participants){
		$array = [];
		if($participants && is_array($participants)){
			foreach($participants as $participant){
				if(!empty($participant['participantLocale'])){
					$array[$participant['sort_order']] = self::getStringForLocale($participant['participantLocale'],'name',false,true);
				}
			}
		}
		return $array;
	}


}
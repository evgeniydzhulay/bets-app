<?php
namespace backend\modules\games\components;


use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventTournamentModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesModel;
use common\modules\games\models\GameSportsLocaleModel;
use backend\modules\games\components\GameHelpers;

class GameBreadcrumbs
{
	public $parent;
	public $parent_id;

	public function getBreadcrumbs() {
		$html = '';
		if($this->parent && $this->parent_id){
			$html = $this->getHtml();
		}
		return $html;
	}

	/* template: <li><a href="" class="right-shadow"><span class="title-box two-rows">Category <br /><span class="title">title</span></a></li> */
	private function getHtml() {
		$li = '';
		$items = $this->getData();
		if(!empty($items)){
			foreach($items as $item){
				$li .= '<li><a href="' . $item['url'] . '" class="right-shadow"><span class="title-box two-rows">' . $item['category'] . ' <br /><span class="title">' . $item['title'] . '</span></a></li>' . PHP_EOL;
			}
		}
		return $li;
	}

	private function getData(){
		switch($this->parent){
			case 'GameSports' : return $this->gameSport(); break;
			case 'GameMarketsGroups' : return $this->gameMarketsGroups(); break;
			case 'GameMarkets' : return $this->gameMarkets(); break;
			case 'EventTournament' : return $this->eventTournament(); break;
			case 'EventGame' : return $this->eventGame(); break;
			default : [];
		}
	}

	private function gameSport(){
		$data = [];
		$game = GameSportsLocaleModel::find()->where(['sports_id' => $this->parent_id])->indexBy('lang_code')->all();
		if($game != Null){
			$data[0]['category'] = \Yii::t('games','Game type');
			$data[0]['title'] = GameHelpers::getStringForLocale($game, 'title', false, true);
			$data[0]['url'] = '/games/game-sports/index';
		}
		return $data;
	}

	private function gameMarketsGroups(){
		$data = [];
		$group = GameMarketsGroupsModel::find()->where(['id' => $this->parent_id])->with([
			'gameMarketsGroupsLocales' => function($query){$query->indexBy('lang_code');},
			'gameSportsLocales' => function($query){$query->indexBy('lang_code');}
		])->asArray()->one();
		if($group != Null){
			if(isset($group['gameSportsLocales'])){
				$data[0]['category'] = \Yii::t('games','Game type');
				$data[0]['title'] = GameHelpers::getStringForLocale($group['gameSportsLocales'], 'title', false, true);
				$data[0]['url'] = '/games/game-sports/index';
			}
			if(isset($group['gameMarketsGroupsLocales'])){
				$data[1]['category'] = \Yii::t('games','Group');
				$data[1]['title'] =  GameHelpers::getStringForLocale($group['gameMarketsGroupsLocales'], 'title', false, true);
				$data[1]['url'] = '/games/game-markets-groups/index?sport_id=' . $group['sports_id'];
			}
		}
		return $data;
	}

	private function gameMarkets(){
		$data = [];
		$market = GameMarketsModel::find()->where(['id' => $this->parent_id])->with([
			'gameMarketsGroupsLocales' => function($query){$query->indexBy('lang_code');},
			'gameMarketsLocales' => function($query){$query->indexBy('lang_code');},
			'gameSportsLocales' => function($query){$query->indexBy('lang_code');}
		])->asArray()->one();
		if($market != Null){
			if(isset($market['gameSportsLocales'])){
				$data[0]['category'] = \Yii::t('games','Game type');
				$data[0]['title'] = GameHelpers::getStringForLocale($market['gameSportsLocales'], 'title', false, true);
				$data[0]['url'] = '/games/game-sports/index';
			}
			if(isset($market['gameMarketsGroupsLocales'])){
				$data[1]['category'] = \Yii::t('games','Group');
				$data[1]['title'] = GameHelpers::getStringForLocale($market['gameMarketsGroupsLocales'], 'title', false, true);
				$data[1]['url'] = '/games/game-markets-groups/index?sport_id=' . $market['sports_id'];
			}
			if(isset($market['gameMarketsLocales'])){
				$data[2]['category'] = \Yii::t('games','Market');
				$data[2]['title'] = GameHelpers::getStringForLocale($market['gameMarketsLocales'], 'title', false, true);
				$data[2]['url'] = '/games/game-markets/index?group_id=' . $market['group_id'];
			}
		}
		return $data;
	}

	private function eventTournament(){
		$data = [];
		$tournament = EventTournamentModel::find()->where(['id' => $this->parent_id])->with([
			'gameSportsLocales' => function($query){$query->indexBy('lang_code');},
			'eventTournamentLocales' => function($query){$query->indexBy('lang_code');}
		])->asArray()->one();
		if($tournament != Null){
			if(isset($tournament['gameSportsLocales'])){
				$data[0]['category'] = \Yii::t('games','Game type');
				$data[0]['title'] = GameHelpers::getStringForLocale($tournament['gameSportsLocales'], 'title', false, true);
				$data[0]['url'] = '/games/game-sports/index';
			}
			if(isset($tournament['eventTournamentLocales'])){
				$data[1]['category'] = \Yii::t('games','Tournament');
				$data[1]['title'] = GameHelpers::getStringForLocale($tournament['eventTournamentLocales'], 'title', false, true);
				$data[1]['url'] = '/games/event-tournament/index?sport_id=' . $tournament['sports_id'];
			}
		}
		return $data;
	}

	private function eventGame(){
		$data = [];
		$game = EventGameModel::find()->where(['id' => $this->parent_id])->with([
			'sportsLocales' => function($query){$query->indexBy('lang_code');},
			'tournamentLocales' => function($query){$query->indexBy('lang_code');},
			'eventGameLocales' => function($query){$query->indexBy('lang_code');}
		])->asArray()->one();
		if($game != Null){
			if(isset($game['sportsLocales'])){
				$data[0]['category'] = \Yii::t('games','Game type');
				$data[0]['title'] = GameHelpers::getStringForLocale($game['sportsLocales'], 'title', false, true);
				$data[0]['url'] = '/games/game-sports/index';
			}
			if(isset($game['tournamentLocales'])){
				$data[1]['category'] = \Yii::t('games','Tournament');
				$data[1]['title'] = GameHelpers::getStringForLocale($game['tournamentLocales'], 'title', false, true);
				$data[1]['url'] = '/games/event-tournament/index?sport_id=' . $game['sports_id'];
			}
			if(isset($game['eventGameLocales'])){
				$data[2]['category'] = \Yii::t('games','Game');
				$data[2]['title'] = GameHelpers::getStringForLocale($game['eventGameLocales'], 'title', false, true);
				$data[2]['url'] = '/games/event-game/index?tournament_id=' . $game['tournament_id'];
			}
		}
		return $data;
	}


}
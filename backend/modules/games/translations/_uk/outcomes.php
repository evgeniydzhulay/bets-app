<?php
/*
UPDATE  game_outcomes_locale
SET    title = replace(title, 'Home', '{item1}')
WHERE  title LIKE 'Home%';

UPDATE  game_outcomes_locale
SET    title = replace(title, 'Away', '{item2}')
WHERE  title LIKE 'Away%';

(new \backend\modules\games\models\TranslatorModel())->translate();
*/

return [

	'/^([-+\d\.\sxX:,]+)$/' => '${1}', // 1 | X | 1x | X2 | 11 22 | 11-22 | 11 - 22 | +27.0 X | +28.0 2 || 20:00 X || -1.5, -2.0
	'/^([\d\.,\s]*)Over([\d\.,\s]*)$/' => '${1}Більше${2}', // 0-9. , Over || Over 0-9. ,
	'/^([\d\.,\s]*)Under([\d\.,\s]*)$/' => '${1}Менше${2}', // 0-9. , Under || Under 0-9. ,
	'/^Over\s([\d\.,\s]+)\s{item(\d+)}$/' => 'Більше ${1} {item${2}}', //Over 10.5 {item2}
	'/^Under\s([\d\.,\s]+)\s{item(\d+)}$/' => 'Менше ${1} {item${2}}', //Under 2.5 {item2}
	'/^([-+\d\.\s]*)\s*Tie\s*([-+\d\.\s]*)$/' => '${1} Нічия ${2}', // +-0-9 . Tie || Tie +-0-9 .
	'/^{item(\d+)}\sBy\s([-+\s0-9]+)$/' => '{item${1}} Від ${2}', // {item1} By 1+ || {item1} By 1 - 2
	'/^{item(\d+)}$/' => '{item${1}}', //{item1}
	'/^{item(\d+)}([-\s]*)\s{item(\d+)}$/' => '{item${1}}${2} {item${3}}', //{item2} - {item2} || {item1} {item2}
	'/^{item(\d+)}\s-\s{item(\d+)}\s-\s{item(\d+)}$/' => '{item${1}} - {item${2}} - {item${3}}', //{item2} - {item2} - {item1}
	'/^{item(\d+)}\s-\s{item(\d+)}\s-\s{item(\d+)}\s-\s{item(\d+)}$/' => '{item${1}} - {item${2}} - {item${3}}  - {item${4}}', //{item4} - {item3} - {item2} - {item1}
	'/^{item(\d+)} Or {item(\d+)}$/' => '{item${1}} або {item${2}}', //{item1} Or {item2}
	'/^{item(\d+)}\sOnly$/' => 'Тільки {item${1}}',// {item1} Only
	'/^{item(\d+)} (\d+) Mins$/' => '{item${1}} ${2} Хвилин', // {item1} 90 Mins
	'/^{item(\d+)}\s([-\+\d\.\s,\)#\(]+)$/' => '{item${1}} ${2}', // {item1} #-+ 0-9.,)(
	'/^{item(\d+)}\s([-\+]*)([0-9\.]+)\s{item(\d+)}$/' => '{item${1}} ${2}${3} {item${4}}', // {item1} +0.5 {item1}
	'/^([-\+\d\.\s,:]+)\s{item(\d+)}$/' => '${1} {item${2}}', // -+ 0-9.,: {item1}
	'/^Exactly\s([\.\d]+)$|^([\.\d]+)\sExactly$/' => 'Дорівнює ${1}${2}', // Exactly 1 || 1 Exactly || Exactly 58.0 || 58.0 Exactly
	'/^{item(\d+)}\sBy\sExactly([-+\s0-9]+)$/' => '{item${1}} Дорівнює ${2}', //{item1} By Exactly 1
	'/^Exactly\s(\d+)\s{item(\d+)}$/' => 'Дорівнює ${1} {item${2}}', //Exactly 20 {item1}
	'/^{item(\d+)}\sExactly\s(\d+)$/' => '{item${1}} Дорівнює ${2}', // {item1} Exactly 1
	'/^(\d+\s)*Yes(\s\d+)*$/' => '${1}Так${2}', // 1 Yes || Yes || Yes 1
	'/^(\d+\s)*No(\s\d+)*$/' => '${1}Ні${2}', // 2 No || No || No 1
	'/^Yes\s\/\sYes$/' => 'Так / Так', // Yes / Yes
	'/^Yes\s\/\sNo$/' => 'Так / Ні', // Yes / No
	'/^No\s\/\sYes$/' => 'Ні / Так', // No / Yes
	'/^No\s\/\sNo$/' => 'Ні / Ні', // No / No
	'/^Yes {item(\d+)}$/' => 'Так {item${1}}', // Yes {item1}
	'/^No {item(\d+)}$/' => 'Ні {item${1}}', // No {item1}
	'/^{item(\d+)} - Yes$/' => '{item${1}} - Так', // {item1} - Yes
	'/^{item(\d+)} - No$/' => '{item${1}} - Ні', // {item1} - No
	'/^[The ]*Draw([\s0-9-#]*)$/' => 'Нічия${1}', // Draw || Draw #0-0 || Draw 0-0
	'/^([+-:\.0-9]+) Draw$/' => '${1} Нічия',// +4.0 Draw || 20:00 Draw
	'/^(\d+) And Under$/' => '${1} і Менше', //16 And Under
	'/^(\d+) And Over$/' => '${1} і Більше',//182 And Over
	'/^(\d+) Neither$/' => 'Ніхто ${1}',// 55 Neither
	'/^{item(\d+)} & Over ([\d\.*]+)$/' => '{item${1}} і Більше ${2}',// {item1} & Over 51.5 || {item1} & Over 51
	'/^{item(\d+)} & Under ([\d\.*]+)$/' => '{item${1}} і Менше ${2}',// {item1} & Under 52.5 || {item1} & Under 52
	'/^(Tie|Draw) & Over ([0-9\.*]+)$/' => 'Нічия і Більше ${2}',// Tie & Over 27.5 || Tie & Over 27 || Draw & Over 2.5 || Draw & Over 2
	'/^(Tie|Draw) & Under\s+([0-9\.*]+)$/' => 'Нічия і Менше ${2}',// Tie & Under 27.5 || Tie & Under 27 || Draw & Under 2.5 || Draw & Under 2
	'/^Over\s([0-9\.]+)\s&\sYes$/' => 'Більше ${1} і Так',// Over 2.5 & Yes
	'/^Over\s([0-9\.]+)\s&\sNo$/' => 'Більше ${1} і Ні',// Over 2.5 & No
	'/^Under\s([0-9\.]+)\s&\sYes$/' => 'Менше ${1} і Так',// Under 2.5 & Yes
	'/^Under\s([0-9\.]+)\s&\sNo$/' => 'Менше ${1} і Ні',// Under 2.5 & No
	'/^{item(\d+)} & Yes$/' => '{item${1}} і Так', //{item1} & Yes
	'/^{item(\d+)} & No$/' => '{item${1}} і Ні', // {item1} & No
	'/^Over ([0-9]+) Minutes$/' => 'Більше ${1} Хвилин',// Over 170 Minutes
	'/^Under ([0-9]+) Minutes$/' => 'Менше ${1} Хвилин',// Under 160 Minutes
	'/^Odd\s*(\d*)$/' => 'Непарне ${1}', //Odd || Odd 1
	'/^Even\s*(\d*)$/' => 'Парне${1}', //Even || Even 1
	'/^Odd\s{item(\d+)}$/' => 'Непарне {item${1}}', // Odd {item2}
	'/^Even\s{item(\d+)}$/' => 'Парне{item${1}}', // Even {item1}
	'/^Draw\s-\sDraw$/' => 'Нічия - Нічия', // Draw - Draw
	'/^{item(\d+)} Or (Draw|Tie)$/' => '{item${1}} або Нічия', // {item1} Or Tie | {item1} Or Draw
	'/^Tie - Tie$/' => 'Нічия - Нічия', // Tie - Tie
	'/^(Tie|Draw) - {item(\d+)}$/' => 'Нічия - {item${2}}', // Tie - {item2} || Draw - {item1}
	'/^Tie {item(\d+)}$/' => 'Нічия {item${1}}', // Tie {item2}
	'/^{item(\d+)}\s-\sDraw$/' => '{item${1}} - Нічия', // {item1} - Draw
	'/^{item(\d+)}\s-\sEven$/' => '{item${1}} - Парне', //{item1} - Even
	'/^{item(\d+)}\s-\sOdd$/' => '{item${1}} - Непарне', // {item1} - Odd
	'/^{item(\d+)}\sYes$/' => '{item${1}} Так', //{item1} Yes
	'/^{item(\d+)}\sNo$/' => '{item${1}} Ні', //{item1} No
	'/^{item(\d+)}\s-\sTie$/' => '{item${1}} - Нічия', // {item2} - Tie
	'/^Round\s(\d+)\s{item(\d+)}$/' => 'Раунд ${1} {item${2}}', //Round 12 {item2}
	'/^Round (\d+)$/' => 'Раунд ${1}', //Round 12
	'/^Round\s(\d+)\s-\sWinner\s{item(\d+)}$/' => 'Раунд ${1} - Переможець {item${2}}', // Round 1 - Winner {item1}
	'/^Rounds\s([-0-9]+)$/' => 'Раунди ${1}', //Rounds 1-3
	'/^{item(\d+)} \/ Rounds ([\s0-9-]+)$/' => '{item${1}} / Раунди ${2}', // {item1} / Rounds 1 - 3
	'/^{item(\d+)}\s-\sGoal\sBefore\s([0-9:]+)$/' => '{item${1}} Гол До ${2}', //{item1} - Goal Before 50:00
	'/^{item(\d+)}\s-\sNo\sGoal\sBefore\s([0-9:]+)$/' => '{item${1}} Немає Гола До ${2}', //{item1} - No Goal Before 50:00
	'/^Goal\sBefore\s([0-9:]+)$/' => 'Гол До ${1}', //Goal Before 50:00
	'/^No\sGoal\sBefore\s([0-9:]+)$/' => 'Немає Гола До ${1}', //No Goal Before 50:00
	'/^1([+]*) Goal$/' => '1${1} Гол', //1 Goal | 1+ Goal
	'/^(2|3|4)([+]*) Goals$/' => '${1}${2} Гола', // 2 Goals | 3 Goals | 4 Goals | 4+ Goals
	'/^(5|6|7|8|9|0)([+]*) Goals$/' => '${1}${2} Голів', // 0 Goals | 6 Goals | 7 Goals | 5 Goals | 8 Goals | 9 Goals | 9+ Goals
	'/^{item(\d+)}\s-\s1([+]*) Goal$/' => '{item${1}} - 1${2} Гол', // {item1} - 1 Goal | {item1} - 1+ Goal
	'/^{item(\d+)}\s-\s(2|3|4)([+]*) Goals$/' => '{item${1}} - ${2}${3} Гола', // {item1} - 3+ Goals | {item1} - 3 Goals
	'/^{item(\d+)}\s-\s(5|6|7|8|9|0)([+]*) Goals$/' => '{item${1}} - ${2}${3} Голів', // {item1} - 0 Goals | {item1} - 6+ Goals
	'/^No Goal After ([0-9:]+)$/' => 'Немає Гола після ${1}', //No Goal After 64:59
	'/^No Goal$/' => 'Немає Гола', //No Goal
	'/^Goal {item(\d+)}$/' => 'Гол {item${1}}', // Goal {item1}
	'/^Goal After ([0-9:]+)$/' => 'Гол Після ${1}', //Goal After 66:59
	'/^{item(\d+)}\sUnder\s([0-9\.*]+)$/' => '{item${1}} Менше ${2}', // {item1} Under 5.5
	'/^{item(\d+)}\sOver\s([0-9\.*]+)$/' => '{item${1}} Більше ${2}', // {item1} Over 5.5
	'/^1st Half$|^First Half$/' => '1-й Тайм', // 1st Half | First Half
	'/^2nd Half$|^Second Half$/' => '2-й Тайм', // 2nd Half | Second Half
	'/^1st Half Yes$/' => '1-й Тайм Так', // 1st Half Yes
	'/^1st Half No$/' => '1-й Тайм Ні', // 1st Half No
	'/^2nd Half Yes$/' => '2-й Тайм Так', // 2nd Half Yes
	'/^2nd Half No$/' => '2-й Тайм Ні', // 2nd Half No
	'/^{item(\d+)}\s(\d+)\sTo\s(\d+)$/' => '{item${1}} ${2} До ${3}', //{item2} 50 To 60
	'/^(\d+)\sTo\s(\d+)$/' => '${1} До ${2}', // 56 To 90
	'/^(\d+)\sTo\s(\d+)\sMinutes\sInc.$/' => '${1} До ${2} Хвилин Включно', // 100 To 120 Minutes Inc.
	'/^No Goal$/' => 'Немає Гола', // No Goal
	'/^No Goals$/' => 'Немає Голів', // No Goals
	'/^{item(\d+)}\s-\s1st Half$/' => '{item${1}} - 1-й Тайм', //{item1} - 1st Half
	'/^{item(\d+)}\s-\s2nd Half$/' => '{item${1}} - 2-й Тайм', //{item1} - 2nd Half
	'/^No Card$/' => 'Немає Картки', // No Card
	'/^Card Before ([0-9:]+)$/' => 'Картка До ${1}', // Card Before 34:00
	'/^No Card Before ([0-9:]+)$/' => 'Немає Картки До ${1}', // No Card Before 34:00
	'/^Corner Before ([0-9:]+)$/' => 'Кутовий Удар До ${1}', // Corner Before 9:00
	'/^No Corner Before ([0-9:]+)$/' => 'Немає Кутового Удара До ${1}', // No Corner Before 9:00
	'/^Card Yes$/' => 'Картка Так', // Card Yes
	'/^Card No$/' => 'Картка Ні', // Card No
	'/^Goal Yes$/' => 'Гол Так', // Goal Yes
	'/^Goal No$/' => 'Гол Ні', // Goal No
	'/^Draw & Yes$/' => 'Нічия і Так', // Draw & Yes
	'/^Draw & No$/' => 'Нічия і Ні', // Draw & No
	'/^Draw Yes$/' => 'Нічия Так', // Draw Yes
	'/^Draw No$/' => 'Нічия Ні', // Draw No
	'/^No Goalscorer First$/' => 'Не Заб\'є Гол Першим', // No Goalscorer First ???
	'/^No Goalscorer Last$/' => 'не Заб\'є Гол Останнім', // No Goalscorer Last ???
	'/^Neither\s(\d+)$/' => 'Ніхто ${1}', // Neither 9
	'/^Corners Over$/' => 'Кутових Ударів Більше', // Corners Over
	'/^Corners Under$/' => 'Кутових Ударів Менше', // Corners Under
	'/^Corner Yes$/' => 'Кутовий Удар Так', // Corner Yes
	'/^Corner No$/' => 'Кутовий Удар Ні', // Corner No
	'/^Goals Over$/' => 'Голів Більше', //Goals Over
	'/^Goals Under$/' => 'Голів Менше', //Goals Under
	'/^Cards Over$/' => 'Карток Більше', //Cards Over
	'/^Cards Under$/' => 'Карток Менше', //Cards Under
	'/^{item(\d+)}\s([-+0-9]+)\s\/\s{item(\d+)}\s([-+0-9]+)$/' => '{item${1}} ${2} / {item${3}} ${4}', // {item2} 19+ / {item1} 1-36
	'/^Any Other Result$/' => 'Будь-який Інший Результат', // Any Other Result
	'/^Any Other$/' => 'Будь-який Інший', // Any Other
	'/^First$/' => 'Перший', // First
	'/^Last$/' => 'Останній', // Last
	'/^(\d+)\sOr\sMore$/' => '${1} або Більше', //3 Or More
	'/^(\d+)\sOr\sMore\s{item(\d+)}$/' => '${1} або Більше {item${2}}', // 6 Or More {item1}
	'/^{item(\d+)} And Yes (\d+)$/' => '{item${1}} і Так ${2}', // {item1} And Yes 60
	'/^{item(\d+)} And No (\d+)$/' => '{item${1}} і Ні ${2}', // {item1} And No 60
	'/^([:0-9]+) To ([:0-9]+)$/' => '${1} До ${2}', // 00:00 To 00:59
	'/^Over {item(\d+)}$/' => 'Більше {item${1}}', // Over {item1}
	'/^Under {item(\d+)}$/' => 'Менше {item${1}}', // Under {item1}
	'/^Tie ([-\s]*)(\(*){item(\d+)}(\)*)([+\-\s0-9\.]+)(\)*)$/' => 'Нічия ${1}${2}{item${3}}${4}${5}${6}', // Tie - {item2} -1.0 || Tie - ({item2} +1) || Tie ({item1}) +1
	'/^Green$/' => 'Зелений', //Green
	'/^Brown$/' => 'Коричневий', //Brown
	'/^Blue$/' => 'Синій', //Blue
	'/^Pink$/' => 'Рожевий', //Pink
	'/^Black$/' => 'Чорний', //Black
	'/^Red$/' => 'Червоний', //Red
	'/^Yellow$/' => 'Жовтий', //Yellow
	'/^{item(\d+)} To Win ([0-9-]+)$/' => '{item${1}} Переможе ${2}', // {item1} To Win 4-0
	'/^To Win {item(\d+)}$/' => 'Переможе {item${1}}', // To Win {item2}
	'/^([+0-9]+)\s+Yes {item(\d+)}$/' => '${1} Так {item${2}}', //50+  Yes {item1}
	'/^([+0-9]+)\s+No {item(\d+)}$/' => '${1} Ні {item${2}}', //50+  No {item1}
	'/^Draw Or {item(\d+)}$/' => 'Нічия або {item${1}}', //Draw Or {item2}
	'/^(\d+)[stndrth]+\sQuarter$/' => '${1} Чверть', // 1st Quarter | 2nd Quarter | 3rd Quarter | 4th Quarter
	'/^{item(\d+)} Penalties$/' => '{item${1}} Пенальті', // {item1} Penalties
	'/^({item)*(\d*)(} )*Extra Time( {item)*(\d*)(})*$/' => '${1}${2}${3}Додатковий Час${4}${5}${6}', // Extra Time || {item1} Extra Time || Extra Time {item1}
	'/^Handicap {item(\d+)}$/' => 'Фора {item${1}}', // Handicap {item2}
	'/^([0-9-]+) {item(\d+)} V {item(\d+)}$/' => '${1} {item${2}} Vs {item${3}}', // 30-0 {item1} V {item2}
	'/^{item(\d+)} To Win In Rounds ([\-\s0-9]+)$/' => '{item${1}} Переможе у Раундах ${2}', // {item1} To Win In Rounds 1-3
	'/^To Win To 0 Or 15 {item(\d+)}$/' => 'Виграє, віддавши 0 або 15 {item${1}}', // To Win To 0 Or 15 {item1}
	'/^Any Other Outcome {item(\d+)}$/' => 'Будь-який інший результат {item${1}}', // Any Other Outcome {item1}
	'/^1 Point {item(\d+)}$/' => '1 Очко {item${1}}', //1 Point {item1}
	'/^(2|3|4) Points {item(\d+)}$/' => '${1} Очка {item${2}}', // 2 Points {item1} | 3 Points {item1} | 4 Points {item1}
	'/^(5|6|7|8|9|0) Points {item(\d+)}$/' => '${1} Очок {item${2}}', // 0 Points {item1} | 6 Points {item1} | 7 Points {item1} | 5 Points {item1} | 8 Points {item1} | 9 Points {item1}
	'/^Over (\d+) Points {item(\d+)}$/' => 'Більше ${1} Очок {item${2}}', //Over 6 Points {item1}
	'/^No Tryscorer$/' => 'Немає занесених спроб', // No Tryscorer
	'/^{item(\d+)} Score First Try & Lose$/' => '{item${1}} Рахунок Після Першої Спроби і Поразки', // {item1} Score First Try & Lose
	'/^{item(\d+)} Score First Try & Tie$/' => '{item${1}} Рахунок Після Першої Спроби і Нічиєї', // {item1} Score First Try & Tie
	'/^{item(\d+)} Score First Try & Win$/' => '{item${1}} Рахунок Після Першої Спроби і Перемоги', // {item1} Score First Try & Win
	'/^{item(\d+)} Score First & Win$/' => '{item${1}} Заб\'є Першою і Виграє', //{item1} Score First & Win
	'/^{item(\d+)} Score First & Lose$/' => '{item${1}} Заб\'є Першою і Програє', //{item1} Score First & Lose
	'/^{item(\d+)} Score First & Tie$/' => '{item${1}} Заб\'є Першою і Буде Нічия', //{item1} Score First & Tie
	'/^{item(\d+)} By (\d+) Or More$/' => '{item${1}} Від ${2} або Більше', // {item1} By 7 Or More
	'/^No Try Before (\d+) Mins (\d+) Secs$/' => 'Немає Спроби До ${1} хвилини ${2} секунд', // No Try Before 7 Mins 30 Secs
	'/^No Try Before (\d+) Mins (\d+) Secs {item(\d+)}$/' => 'Немає Спроби До ${1} хвилини ${2} секунд {item${3}}', //No Try Before 22 Mins 30 Secs {item1}
	'/^Try Before (\d+) Mins (\d+) Secs {item(\d+)}$/' => 'Спроба До ${1} хвилини ${2} секунд {item${3}}', //Try Before 22 Mins 30 Secs {item2}
	'/^Try Before (\d+) Mins (\d+) Secs$/' => 'Спроба До ${1} хвилини ${2} секунд', // Try Before 7 Mins 30 Secs
	'/^No Try After (\d+) Mins (\d+) Secs$/' => 'Немає Спроби Після ${1} хвилини ${2} секунд', // No Try After 7 Mins 30 Secs
	'/^Try After (\d+) Mins (\d+) Secs$/' => 'Спроба Після ${1} хвилини ${2} секунд', // Try Before 7 After 30 Secs
	'/^No Try Before (\d+) Mins (\d+) Secs$/' => 'Немає Спроби До ${1} хвилини ${2} секунд', // No Try Before 7 Mins 30 Secs
	'/^Try Before (\d+) Mins (\d+) Secs$/' => 'Спроба До ${1} хвилини ${2} секунд', // Try Before 7 Mins 30 Secs
	'/^No Try After (\d+) Mins (\d+) Secs$/' => 'Немає Спроби Після ${1} хвилини ${2} секунд', // No Try After 7 Mins 30 Secs
	'/^Try After (\d+) Mins (\d+) Secs$/' => 'Спроба Після ${1} хвилини ${2} секунд', // Try Before 7 After 30 Secs
	'/^Both Teams To Score ([+0-9]+) Yes$/' => 'Обидві Команди Заб\'ють ${1} Так', // Both Teams To Score 61+ Yes
	'/^Both Teams To Score ([+0-9]+) No$/' => 'Обидві Команди Заб\'ють ${1} Ні', // Both Teams To Score 61+ No
	'/^No Goalscorer \({item(\d+)}\) First$/' => 'Ніхто Не Заб\'є ({item${1}}) Перший', // No Goalscorer ({item2}) First
	'/^No Goalscorer \({item(\d+)}\) Last$/' => 'Ніхто Не Заб\'є ({item${1}}) Останній', // No Goalscorer ({item2}) Last
	'/^Total {item(\d+)}$/' => 'Тотал {item${1}}', // Total {item1}
	'/^No Pen Converted Before (\d+) Mins (\d+) Secs$/' => 'Без штрафних спроб до ${1} хвилини ${2} секунд', //No Pen Converted Before 11 Mins 20 Secs
	'/^Pen Converted Before (\d+) Mins (\d+) Secs$/' => 'З штрафними спробами до ${1} хвилини ${2} секунд', // Pen Converted Before 16 Mins 15 Secs
	'/^No Pen Converted Before (\d+) Mins (\d+) Secs {item(\d+)}$/' => 'Без штрафних спроб до ${1} хвилини ${2} секунд {item${3}}', //No Pen Converted Before 12 Mins 15 Secs {item2}
	'/^Pen Converted Before (\d+) Mins (\d+) Secs {item(\d+)}$/' => 'З штрафними спробами до ${1} хвилини ${2} секунд {item${3}}', // Pen Converted Before 16 Mins 15 Secs {item2}
	'/^Try {item(\d+)}$/' => 'Спроба {item${1}}', // Try {item1}
	'/^Penalty {item(\d+)}$/' => 'Пенальті {item${1}}', // Penalty {item1}
	'/^Drop Goal {item(\d+)}$/' => 'Дроп-гол {item${1}}', // Drop Goal {item1}
	'/^Score On Or After ([:0-9]+) Elapsed$/' => 'Рахунок До Або Після Закінчення ${1}', // Score On Or After 6:00 Elapsed
	'/^Score Before ([:0-9]+) Elapsed$/' => 'Рахунок До Закінчення ${1}', // Score Before 6:00 Elapsed
	'/^([+0-9]+) Centuries$/' => '${1} Сто Очок', // 9+ Centuries
	'/^1 Century$/' => '1 Сто Очок', // 1 Century
	'/^{item(\d+)} \(Frames ([-0-9]+)\) - {item(\d+)}$/' => '{item${1}} (Фрейми ${2}) - {item${3}}', //{item1} (Frames 1-4) - {item2}
	'/^Draw \(Frames ([-0-9]+)\) - {item(\d+)}$/' => 'Нічия (Фрейми ${1}) - {item${2}}', // Draw (Frames 1-4) - {item1}
	//'//' => '', //
	'/^{item(\d+)} To Break First$/' => '{item${1}} Перерве Першим', // {item1} To Break First
	'/^{item(\d+)} To Win (1st|First) Set And Win Match$/' => '{item${1}} Виграє Перший Сет і Виграє Матч', // {item1} To Win 1st Set And Win Match
	'/^{item(\d+)} To Win (1st|First) Set And Lose Match$/' => '{item${1}} Виграє Перший Сет і Програє Матч', // {item1} To Win 1st Set And Lose Match
	'/^To Win To 0, 15 Or 30 {item(\d+)}$/' => 'Виграє, Віддавши 0, 15 або 30 {item${1}}', // To Win To 0, 15 Or 30 {item1}
	'/^Any other outcome$/' => 'Будь Інший Результат', // Any other outcome
	'/^(\d+) Or More$/' => '${1} або Більше', //10 Or More
	'/^(\d+) Or Less$/' => '${1} або Менше', // 10 Or Less
	'/^(\d+) Or More Yes$/' => '${1} або Більше Так', //10 Or More Yes
	'/^(\d+) Or More No$/' => '${1} або Більше Ні', //10 Or More No
	'/^Win To Love {item(\d+)}$/' => 'Виграє Всуху {item${1}}', // Win To Love {item2}
	'/^Lose To Love {item(\d+)}$/' => 'Програти До 0 {item${1}}', // Lose To Love {item1}
	'/^Foul$/' => 'Фол', //
	'/^(\d+) & Over$/' => '${1} і Більше', // 100 & Over
	'/^Score Draw$/' => 'Нічия', //
	'/^(\d+) - Full Time$/' => '${1} - Основний Час', // 81 - Full Time
	'/^Both Teams$/' => 'Обидві Команди', //
	'/^Under (\d+) Goals$/' => 'Менше ${1} Голів', // Under 2 Goals
	'/^(\d+) Or (\d+) Goals$/' => '${1} або ${2} Гола', // 2 Or 3 Goals
	'/^Over (\d+) Goals$/' => 'Більше ${1} Голів', // Over 3 Goals
	'/^To Win From Behind {item(\d+)}$/' => 'Виграє Програючи {item${1}}', // To Win From Behind {item1}
	'/^To Win To Nil {item(\d+)}$/' => 'Виграє Всуху {item${1}}', // To Win To Nil {item1}
	'/^To Win Either Half {item(\d+)}$/' => 'Виграє в Одному з Таймів {item${1}}', // To Win Either Half {item1}
	'/^To Win Both Halves {item(\d+)}$/' => 'Виграє Обидва Тайма {item${1}}', // To Win Both Halves {item1}
	'/^To Score In Both Halves {item(\d+)}$/' => 'Заб\'є в Обох Таймах {item${1}}', // To Score In Both Halves {item1}
	'/^Touchdown$/' => 'Тачдаун', //
	'/^Field Goal$/' => 'Філд-Гол', //
	'/^Touchdown {item(\d+)}$/' => 'Тачдаун {item${1}}', //
	'/^Field Goal {item(\d+)}$/' => 'Філд-Гол {item${1}}', //
	'/^Any Other {item(\d+)}$/' => 'Будь-який інший {item${1}}', //
	'/^Neither$/' => 'Ніхто', //
	'/^Either$/' => 'Будь-який', //
	'/^Solo Hr$/' => 'Соло Хоум-Ран', //
	'/^2-Run Hr$/' => 'Подвійний Хоум-Ран', //
	'/^3-Run Hr$/' => 'Потрійний Хоум-Ран', //
	'/^No Hr Scored$/' => 'Хоум-Рани Не зарахувани', //
	'/^(\d+) Runs Or Less$/' => '${1} або Меньше Ранов', // 5 Runs Or Less
	'/^(\d+) Or (\d+) Runs$/' => '${1} або ${2} Ранов', // 8 Or 9 Runs
	'/^(\d+) Or More Runs$/' => '${1} або Більше Ранов', // 12 Or More Runs
	'/^Neither Team$/' => 'Ні Одна з Команд', //
	'/^Either Team$/' => 'Будь-яка Команда', //
	'/^Regular Time {item(\d+)}$/' => 'Основний Час {item${1}}', //Regular Time {item2}
	'/^Try$/' => 'Спроба', //
	'/^No Try$/' => 'Немає Спроби', //
	'/^Goal$/' => 'Гол', //
	'/^Behind$/' => 'Позаду', //
	'/^Behind {item(\d+)}$/' => 'Позаду {item${1}}', //Behind {item1}
	'/^Period (\d+)$/' => 'Період ${1}', //
	'/^Caught$/' => 'Спійманий', //
	'/^Bowled$/' => 'Боулд', //
	'/^Lbw$/' => 'Нога Перед Каліткою', //
	'/^Run Out$/' => 'Ран-Аут', //
	'/^Stumped$/' => 'Стампд', //
	'/^Other(s*)$/' => 'Інші', //
	'/^{item(\d+)} ([\+\-0-9\.]+) Wkts \/ ([\+\-0-9\.]+) Runs$/' => '{item${1}} ${2} wkts / ${3} Рани', // {item1} +1.5 Wkts / +2.5 Runs
	'/^([\s\-0-9]+) Inclusive$/' => '${1} Включно', // 90 - 100 Inclusive
	'/^(2|3|4) Legs Draw$/' => '${1} Лега Нічия', // 3 Legs Draw
	'/^(2|3|4) Legs {item(\d+)}$/' => '${1} Лега {item${2}}', // 3 Legs {item1}
	'/^(5|6|7|8|9|0) Legs {item(\d+)}$/' => '${1} Легов {item${2}}', //6 Legs {item2}
	'/^(5|6|7|8|9|0) Legs Draw$/' => '${1} Легов Нічия', // 5 Legs Draw
	'/^{item(\d+)} By Decision Or Technical Decision$/' => '{item${1}} Рішення / Тех. Рішення', //
	'/^Draw Or Technical Draw$/' => 'Нічия / Технічна Нічия', //
	'/^{item(\d+)} By Ko, Tko Or Disqualification$/' => '{item${1}} Нок., Тех. Нок., Дискваліфікація', //
	'/^First (\d+) Innings$/' => 'Перші ${1} Іннінгсов', // First 5 Innings
	'/^Rest Of The Game$/' => 'Частина гри, що залишилася', //
	'/^Grand Slam$/' => 'Гренд Слем', //
	'/^After Extra Points {item(\d+)}$/' => 'Після Додаткових Очок {item${1}}', // After Extra Points {item1}
	'/^([0-9\-]+) Or Better {item(\d+)}$/' => '${1} або Краще {item${2}}', // 25-15 Or Better {item1}
	'/^{item(\d+)} Over/' => '{item${1}} Більше', //
	'/^{item(\d+)} Under/' => '{item${1}} Менше', //
	'/^Rd (\d+) - Winner {item(\d+)}$/' => 'Раунд ${1} - Переможець {item${2}}', // Rd 2 - Winner {item1}
	'/^Treble 20$/' => 'Требл 20',
	'/^Single 20$/' => 'Ординар 20',
	'/^{item(\d+)} \((\d+) Legs\) - {item(\d+)}$/' => '{item${1}} (${2} Легов) - {item${3}}', //{item1} (6 Legs) - {item2}
	'/^Draw \((\d+) Legs\) - {item(\d+)}$/' => 'Нічия (${1} Легов) - {item${2}}', //Draw (6 Legs) - {item2}
	'/^[By ]*Ko, Tko Or Disqualification {item(\d+)}$/' => 'Нок., Тех. Нок., або Дискваліфікація {item${1}}', // By Ko, Tko Or Disqualification {item1}
	'/^[By ]*Decision Or Technical Decision {item(\d+)}$/' => 'Рішення / Тех. Рішення {item${1}}', //By Decision Or Technical Decision {item1}
	'/^To Go The Distance$/' => 'Бій Триватиме Всі Раунди',
	'/^To Win In Rounds ([\-0-9]+) {item(\d+)}$/' => 'Виграє в раундах ${1} {item${2}}', // To Win In Rounds 4-6 {item1}
	'/^{item(\d+)} \/ Decision Or Technical Decision$/' => '{item${1}} / Рішення або Тех. рішення', // {item2} / Decision Or Technical Decision
	'/^{item(\d+)} \/ Ko, Tko Or Disqualification$/' => '{item${1}} / Нок., Тех. Нок., або Дискваліфікація', // {item2} / Ko, Tko Or Disqualification
	'/^1 Leg$/' => '1 Лег',
	'/^(2|3|4) Legs$/' => '${1} Лега',
	'/^(5|6|7|8|9|0) Legs$/' => '${1} Легів',
	'/^To Win To Love {item(\d+)}$/' => 'Виграє всуху {item${1}}', //To Win To Love {item1}
	'/^To Win To (\d+) {item(\d+)}$/' => 'Виграє, віддавши ${1} {item${2}}', //To Win To 15 {item1}
	'/^Lineout \/ Throw In$/' => 'Лайн-аут / Вкидання м\'яча',
	'/^Penalty \/ Free Kick$/' => 'Пенальті / Штрафний удар',
	'/^Scrum$/' => 'Сутичка За М\'яч',
	'/^Mark$/' => 'Перехоплення',
	'/^22m Drop Out$/' => '22-метровий Дроп-аут',
	'/^Drop Goal$/' => 'Дроп-гол',
	'/^Tries$/' => 'Спроби',
	'/^Converted Penalties$/' => 'Штрафні Пенальті',
	'/^To Win In Rounds ([\-0-9]+) Either$/' => 'Виграє в Раундах ${1} Будь-який', //To Win In Rounds 1-2 Either
	'/^Round (\d+) Either$/' => 'Раунд ${1} Будь-який', //Round 10 Either
	'/^(\d+)(st|nd|rd|th) {item(\d+)}$/' => '${1}-й {item${3}}', // 1st {item1} | 2nd {item1} | 3rd {item1} | 4th {item1} | 5th {item1}
	'/^Last {item(\d+)}$/' => 'Останній {item${1}}', //Last {item1}
	'/^Last Neither$/' => 'Останній Ні Одна',
	'/^(\d+)(st|nd|rd|th) Neither$/' => '${1}-й Ні Одна', //1st Neither | 2nd Neither | 3rd Neither | 4th Neither 5th Neither
	'/^Shot$/' => 'Удар',
	'/^Header$/' => 'Удар Головою',
	'/^Penalty$/' => 'Пенальті',
	'/^Free Kick$/' => 'Штрафний Удар',
	'/^Own Goal$/' => 'Автогол',
	//'//' => '',
	/* contains first and last name */

	'/^([-\'A-Za-z\s\.]+)\sFirst$/' => '${1} Перший', // Felix Myhre First
	'/^([-\'A-Za-z\s\.]+)\sLast$/' => '${1} Останній', // Rafael Ramazotti Last
	'/^([-\'A-Za-z\s\.]+)\sAnytime$/' => '${1} В будь-який час', // Rafael Ramazotti Anytime
	'/^([-\'A-Za-z\s\.]+)\s\({item(\d+)}\)$/' => '${1} ({item${2}})', // Jackson Macrae ({item1})
	'/^([-\'A-Za-z\s\.]+)\sOver([\s0-9\.]*)$/' => '${1} Більше${2}', // Jean Segura Over || Aj Finch Over 0.5
	'/^([-\'A-Za-z\s\.]+)\sUnder([\s0-9\.]*)$/' => '${1} Менше${2}', // Jean Segura Under || Ej Pollock Under 0.5
	'/^([-\'A-Za-z\s\.]+)\s(\d+)\sOr\sMore$/' => '${1} ${2} або Більше', // Will Bruin 2 Or More
	'/^([-\'A-Za-z\s\.]+)\s([-\s]*)Yes$/' => '${1} ${2}Так', // Boca Juniors - Yes || Boca Juniors Yes
	'/^([-\'A-Za-z\s\.]+)\s([-\s]*)No$/' => '${1} ${2}Ні', // Boca Juniors - No || Boca Juniors No
	'/^([-\'A-Za-z\s\.]+)\s-\s{item(\d+)}$/' => '${1} - {item${2}}', // Jaeger O'meara - {item2}
	'/^Neither\s\(([-\'A-Za-z\s\.]+)\s&\s([-\'A-Za-z\s\.]+)\)$/' => 'Ніхто (${1} & ${2})', // Neither (Dl Lloyd & Jl Lawlor)
	'/^([-\'A-Za-z\s\.]+) \(Vs ([-\'A-Za-z\s\.]+)\)$/' => '${1} (vs ${2})', // Mj Cosgrove (Vs Al Davies)
	'/^([-\'A-Za-z\s\.]+)\s\({item(\d+)}\)\sOver$/' => '${1} ({item${2}}) Більше', // Matt Skole ({item3}) Over
	'/^([-\'A-Za-z\s\.]+)\s\({item(\d+)}\)\sUnder$/' => '${1} ({item${2}}) Менше', //Brian Dozier ({item1}) Under
	'/^([-\'A-Za-z\s\.]+) \({item(\d+)}\) Period (\d+)$/' => '${1} ({item${2}}) ${3}-й Період', // Victor Ejdsell ({item2}) Period 1
	'/^([-\'A-Za-z\s\.]+) \({item(\d+)}\) (\d+) Or More$/' => '${1} ({item${2}}) ${3} або Більше', // Michal Cajkovsky ({item2}) 2 Or More
	'/^Individual Total {item(\d+)} \((\d+)\)$/' => 'Інд. Тотал {item${1}} Дорівнюе (${2})',

];

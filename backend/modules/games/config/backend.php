<?php

return [
	'modules' => [
		'games' => [
			'controllerNamespace' => '\backend\modules\games\controllers',
			'viewPath' => '@backend/modules/games/views'
		],
	],
];

<?php

namespace backend\modules\games\actions;

class ImageUploadModel extends FileUploadModel {

    public function rules() {
        return [
            ['file', 'file', 'extensions' => [
                'jpg', 'jpeg', 'png', 'gif'
            ]]
        ];
    }

}

<?php

namespace backend\modules\games\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;

class ImageUploadAction extends Action {

    /**
     * @return string
     * @throws InvalidConfigException
     */
    function run() {
        if (isset($_FILES)) {
            $model = Yii::createObject([
                'class' => ImageUploadModel::class
            ]);
            $CKEditorFunc = Yii::$app->request->get('CKEditorFuncNum');
            $message = '';
            $url = '';
            if ($model->upload()) {
                $result = $model->getResponse();
                $url = $result['url'];
            } else {
                $message = 'Unable to save image file';
            }
            return "<script type=\"text/javascript\">window.parent.CKEDITOR.tools.callFunction(\"{$CKEditorFunc}\", \"{$url}\", \"{$message}\");</script>";
        }
    }

}

<?php

namespace backend\modules\games\controllers;

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\GameParticipantsLocaleModel;
use Yii;
use common\modules\games\models\GameParticipantsModel;
use backend\modules\games\models\search\GameParticipantsSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * GameParticipantsController implements the CRUD actions for GameParticipantsModel model.
 */
class GameParticipantsController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}

	/**
	 * Lists all GameParticipantsModel models.
	 * @param int $sport_id
	 * @return mixed
	 */
	public function actionIndex(int $sport_id) {
		$searchModel = new GameParticipantsSearch();
		$searchModel->scenario = GameParticipantsSearch::SCENARIO_SEARCH;
		$searchModel->sports_id = $sport_id;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate(int $sport_id) {
		$model = new GameParticipantsModel(['scenario' => GameParticipantsModel::SCENARIO_CREATE]);
		$model->sports_id = $sport_id;
		if(!empty(Yii::$app->request->post()['locale']) && $model->save()){

			/* set locales */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){continue;}
					$locale = new GameParticipantsLocaleModel(['scenario' => GameParticipantsLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1,'participant_id' => $model->id, 'sports_id' => $model->sports_id, 'lang_code' => $lang, 'name' => strip_tags($title)]);
					$locale->save();
				}
			}

			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'participant_id' => $model->id]);
		}
		return $this->render('participant', compact('model', 'searchModel', 'dataProvider'));
	}

	public function actionUpdate(int $participant_id) {
		/* locales */
		$searchModel = new GameParticipantsLocaleModel();
		$searchModel->participant_id = (int)$participant_id;
		$searchModel->scenario = GameParticipantsLocaleModel::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$model = $this->findModel($participant_id);
		$model->scenario = GameParticipantsModel::SCENARIO_UPDATE;

		if($model->load(Yii::$app->request->post()) && $model->save()){
			/* set locales */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){
						continue;
					}
					$locale = new GameParticipantsLocaleModel(['scenario' => GameParticipantsLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1, 'participant_id' => $model->id, 'sports_id' => $model->sports_id, 'lang_code' => $lang, 'name' => strip_tags($title)]);
					$locale->save();
				}
			}
			GameHelpers::setMessage(true);
			return $this->refresh();
		}
		return $this->render('participant', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'model' => $model
		]);
	}

	public function actionLocaleUpdate(int $id) {
		$model = $this->findParticipantsLocale($id);
		$model->scenario = GameParticipantsLocaleModel::SCENARIO_UPDATE;
		$model->is_active = 1;
		if($model->load(Yii::$app->request->post()) && $model->save()){
			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'participant_id' => $model->participant_id]);
		}
		return $this->render('locale-update', compact('model'));
	}


	/**
	 * Finds the GameParticipantsModel model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return GameParticipantsModel the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = GameParticipantsModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * Finds the GameParticipantsLocaleModel model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return GameParticipantsLocaleModel the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */

	protected function findParticipantsLocale($id) {
		if(($model = GameParticipantsLocaleModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}
}

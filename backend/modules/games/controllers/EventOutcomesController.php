<?php

namespace backend\modules\games\controllers;


use backend\modules\games\components\GameHelpers;
use backend\modules\games\forms\BanForm;
use backend\modules\games\models\EventOutcomesHandler;
use backend\modules\games\models\GameOutcomesModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameMarketsModel;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class EventOutcomesController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}

	public function actionEventGameOutcomes($game_id, $group_id = null, $market_id = null) {
		$game = $this->findGame((int)$game_id);
		$searchModel = new EventOutcomesHandler();
		$searchModel->sport_id = $game->sports_id;
		$data = $searchModel->search(Yii::$app->request->queryParams);

		/* set filter settings */
		$groupsList = $this->findMarketGroupsLocale($game->sports_id);
		$marketsList = $this->findMarketLocale($group_id);
		/* ./ set filter settings */

		return $this->render('event-game-outcomes', [
			'game' => $game,
			'groupsList' => $groupsList,
			'group_id' => $group_id,
			'market_id' => $market_id,
			'marketsList' => $marketsList,
			'data' => $data,
			'participants' => $this->getParticipants($game),
			'outcomesList' => $this->findEventOutcomesForGame($game->id)
		]);
	}

	public function actionUpdate($game_id, $group_id = null, $market_id = null) {
		$game = $this->findEventGame($game_id);
		$rates = Yii::$app->request->post('rates') ?? [];
		$statuses = Yii::$app->request->post('statuses') ?? [];
		$finished = Yii::$app->request->post('finished') ?? [];
		$hidden = Yii::$app->request->post('hidden') ?? [];
		GameHelpers::setMessage((new EventOutcomesHandler)->saveData($game,$rates,$statuses,$finished,$hidden));
		return $this->redirect(Url::to([
			'event-game-outcomes',
			'game_id' => $game->id,
			'group_id' => $group_id,
			'market_id' => $market_id
		]));

	}

	public function actionGetMarketsList() {
		$html = '<option value="">No data</option>';
		if(Yii::$app->request->isPost && !empty($id = (int)Yii::$app->request->post('id'))){
			$markets = $this->findMarketLocale($id);
			if(!empty($markets)){
				$html = '<option value="">Please select</option>' . PHP_EOL;
				foreach($markets as $key => $value){
					$html .= "<option value='{$key}'>{$value}</option>" . PHP_EOL;
				}
			}
		}
		return $html;
	}

	public function actionSetBan(){
		if(Yii::$app->request->isPost && !empty($id = (int) Yii::$app->request->post('item_id'))){
			$model = Yii::createObject(BanForm::class);
			if($model->load(Yii::$app->request->post()) && $model->banOutcome($this->findOutcome($id))){
				GameHelpers::setMessage(true, Yii::t('games','Outcome') . ' ' . Yii::t('games','Banned'));
			}
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	protected function findGame($id) {
		if(($model = EventGameModel::find()->where(['id' => $id])->with(['eventGameLocales', 'eventParticipants.participantLocale' => function ($query) {
				$query->where(['lang_code' => 'en']);
			}])->one()) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findEventGame($id) {
		if(($model = EventGameModel::find()->where(['id' => $id])->one()) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findOutcome($id) {
		if(($model = \common\modules\games\models\EventOutcomesModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findMarketGroupsLocale($sports_id) {
		$list = [];
		$groups = GameMarketsGroupsModel::find()->where(['sports_id' => $sports_id])->with(['gameMarketsGroupsLocales' => function($q){$q->indexBy('lang_code');}])->all();
		foreach($groups as $group){
			$list[$group->id] = GameHelpers::getStringForLocale($group->gameMarketsGroupsLocales,'title',false,true);
		}
		asort($list);
		return $list;
	}

	protected function findMarketLocale($group_id) {
		$list = [];
		if(empty($group_id)){
			return $list;
		}

		$markets = GameMarketsModel::find()->where(['group_id' => $group_id])->with(['gameMarketsLocales' => function($q){$q->indexBy('lang_code');}])->all();
		foreach($markets as $market){
			$list[$market->id] = GameHelpers::getStringForLocale($market->gameMarketsLocales,'title',false,true);
		}
		asort($list);
		return $list;
	}

	protected function getParticipants($model) {
		$list = [];
		if(!empty($model->eventParticipants)){
			foreach($model->eventParticipants as $participant){
				if(!empty($participant->participantLocale)){
					$list[$participant->sort_order] = $participant->participantLocale[0]->name ?? 'Not Set';
				}
			}
			ksort($list);
		}
		return $list;
	}

	protected function findEventOutcomesForGame($id,$group_id = null, $market_id = null) {
		return \common\modules\games\models\EventOutcomesModel::find()
			//->select('rate,is_enabled,outcome_id,game_id')
			->where(['game_id' => $id])
			->andFilterWhere(['group_id' => $group_id,'market_id' => $market_id])
			->asArray()->indexBy('outcome_id')->all();
	}

	public function actionGetOutcomesList(){

		if(Yii::$app->request->isPost){
			$data = '';
			\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
			$market_id = (int) Yii::$app->request->post('market');
			$game_id = (int) Yii::$app->request->post('game');
			$outcomes = \common\modules\games\models\GameOutcomesModel::find()->where(['{{%game_outcomes}}.market_id' => $market_id])
				->andWhere("game_outcomes.id NOT IN (SELECT `event_outcomes`.outcome_id FROM `event_outcomes` WHERE `market_id` = {$market_id} AND `game_id` = {$game_id})")
				->with(['gameOutcomesLocales' => function($q){return $q->indexBy('lang_code');}])
				->all();
			if($outcomes != []){
				$data = [];
				foreach($outcomes as $outcome){
					$data[$outcome->id] = GameHelpers::getStringForLocale($outcome->gameOutcomesLocales,'title',false,true);
				}
			}
			return $data;
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

}
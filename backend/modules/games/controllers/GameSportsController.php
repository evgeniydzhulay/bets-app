<?php

namespace backend\modules\games\controllers;

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameSportsLocaleModel;
use Yii;
use common\modules\games\models\GameSportsModel;
use backend\modules\games\models\search\GameSportsSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GameSportsController implements the CRUD actions for GameSportsModel model.
 */
class GameSportsController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}

	/**
	 * Lists all GameSportsModel models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new GameSportsSearch();
		$searchModel->scenario = GameSportsSearch::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate() {
		$model = new GameSportsModel(['scenario' => GameSportsModel::SCENARIO_CREATE]);

		if($model->load(Yii::$app->request->post()) && $model->save()){

			/* set locales */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){
						continue;
					}
					$locale = new GameSportsLocaleModel(['scenario' => GameSportsLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1,'sports_id' => $model->id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
					$locale->save();
				}
			}

			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'id' => $model->id]);
		}

		return $this->render('game', compact('model', 'searchModel', 'dataProvider'));
	}

	/**
	 * @param $id
	 * @return string
	 * @throws NotFoundHttpException
	 */
	public function actionUpdate(int $id) {
		/* locales */
		$searchModel = new GameSportsLocaleModel();
		$searchModel->sports_id = (int)$id;
		$searchModel->scenario = GameSportsLocaleModel::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		/* game */
		$model = $this->findSport($id);
		$model->scenario = GameSportsModel::SCENARIO_UPDATE;

		if($model->load(Yii::$app->request->post()) && $model->save()){

			/* set new locales in game, if added */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){
						continue;
					}
					$locale = new GameSportsLocaleModel(['scenario' => GameSportsLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1,'sports_id' => $model->id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
					$locale->save();
				}
			}

			GameHelpers::setMessage(true);
			return $this->refresh();
		}

		return $this->render('game', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'model' => $model
		]);
	}

	public function actionLocaleUpdate($id) {
		$model = $this->findSportLocale($id);
		$model->scenario = GameSportsLocaleModel::SCENARIO_UPDATE;
		$model->is_active = 1;
		if($model->load(Yii::$app->request->post()) && $model->save()){
			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'id' => $model->sports_id]);
		}
		return $this->render('locale-update', compact('model'));
	}

	public function actionChangeStatus(int $id, int $status) {
		$model = $this->findSport($id);
		if(in_array($status, [$model::STATUS_DISABLED, $model::STATUS_ENABLED])){
			$model->is_enabled = $status;
			GameHelpers::setMessage($model->save(false, ['is_enabled']));
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionUpdateMarkets() {

		if(Yii::$app->request->isPost && !empty($items = Yii::$app->request->post('items'))){
			foreach($items as $market_id => $group_id){
				if(!$group_id){ // if group not selected
					continue;
				}
				$market = \backend\modules\games\models\GameMarketsModel::findOne((int)$market_id);
				if($market){
					$market->setGroup($group_id);
				}
			}
		}

		$data = \backend\modules\games\models\GameMarketsModel::getDataForUpdateMarkets();

		if(empty($data)){
			return $this->redirect('index');
		}
		return $this->render('update-markets', compact('data'));
	}

	protected function findSportLocale($id) {
		if(($model = GameSportsLocaleModel::findOne($id)) !== null){
			return $model;
		}
		throw new NotFoundHttpException(Yii::t('games', 'The requested page does not exist.'));
	}

	protected function findSport($id) {
		if(($model = GameSportsModel::findOne($id)) !== null){
			return $model;
		}
		throw new NotFoundHttpException(Yii::t('games', 'The requested page does not exist.'));
	}

}

<?php

namespace backend\modules\games\controllers;

use backend\modules\games\components\GameHelpers;
use backend\modules\games\forms\BanForm;
use common\modules\games\models\EventTournamentLocaleModel;
use common\modules\games\models\GameSportsLocaleModel;
use common\modules\games\models\GameSportsModel;
use Yii;
use common\modules\games\models\EventTournamentModel;
use backend\modules\games\models\search\EventTournamentSearch;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * EventTournamentController implements the CRUD actions for EventTournamentModel model.
 */
class EventTournamentController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}

	/**
	 * Lists all EventTournamentModel models.
	 * @param int $sport_id
	 * @return mixed
	 */
	public function actionIndex($sport_id = false) {
		$searchModel = new EventTournamentSearch();
		$searchModel->scenario = EventTournamentSearch::SCENARIO_SEARCH;
		if($sport_id){
			$searchModel->sports_id = (int)$sport_id;
			$positioning = 'sport';
			$sportList = [];
		}else{
			$positioning = 'all';
			$sportList = $this->getSportList();
			$searchModel->extraOptions = true;
		}
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'positioning' => $positioning,
			'sportList' => $sportList
		]);
	}

	public function actionCreate(int $sport_id) {
		$model = new EventTournamentModel(['scenario' => EventTournamentModel::SCENARIO_CREATE]);
		$model->sports_id = $sport_id;
		if($model->load(Yii::$app->request->post()) && $model->save()){

			/* set locales */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){
						continue;
					}
					$locale = new EventTournamentLocaleModel(['scenario' => EventTournamentLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1, 'sports_id' => $model->sports_id, 'tournament_id' => $model->id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
					$locale->save();
				}
			}

			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'tournament_id' => $model->id]);
		}

		return $this->render('tournament', compact('model', 'searchModel', 'dataProvider'));
	}

	public function actionUpdate(int $tournament_id) {
		/* locales */
		$searchModel = new EventTournamentLocaleModel();
		$searchModel->tournament_id = (int)$tournament_id;
		$searchModel->scenario = EventTournamentLocaleModel::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		/* tournament */
		$model = $this->findTournament($tournament_id);
		$model->scenario = EventTournamentModel::SCENARIO_UPDATE;

		if($model->load(Yii::$app->request->post()) && $model->save()){

			/* set new locales */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){
						continue;
					}
					$locale = new EventTournamentLocaleModel(['scenario' => EventTournamentLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1, 'sports_id' => $model->sports_id, 'tournament_id' => $model->id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
					$locale->save();
				}
			}

			GameHelpers::setMessage(true);
			return $this->refresh();
		}

		return $this->render('tournament', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'model' => $model
		]);
	}

	public function actionLocaleUpdate(int $id) {
		$model = $this->findTournamentLocale($id);
		$model->scenario = EventTournamentLocaleModel::SCENARIO_UPDATE;
		$model->is_active = 1;
		if($model->load(Yii::$app->request->post()) && $model->save()){
			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'tournament_id' => $model->tournament_id]);
		}
		return $this->render('locale-update', compact('model'));
	}

	public function actionChangeStatus(int $id, int $status) {
		$model = $this->findTournament($id);
		if(in_array($status, [$model::STATUS_DISABLED, $model::STATUS_ENABLED])){
			$model->is_enabled = $status;
			GameHelpers::setMessage($model->save(false, ['is_enabled']));
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionSetBan() {
		if(Yii::$app->request->isPost && !empty($id = (int)Yii::$app->request->post('item_id'))){
			$model = Yii::createObject(BanForm::class);
			if($model->load(Yii::$app->request->post()) && $model->banTournament($this->findTournament($id))){
				GameHelpers::setMessage(true, Yii::t('games', 'Tournament') . ' ' . Yii::t('games', 'Banned'));
			}
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionSetFinishedTournament($id) {
		$model = $this->findTournament($id);
		GameHelpers::setMessage((bool)$model->updateAttributes(['is_finished' => 1]));;
		$this->redirect(Yii::$app->request->referrer);
	}

	protected function findTournament($id) {
		if(($model = EventTournamentModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findTournamentLocale($id) {
		if(($model = EventTournamentLocaleModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function getSportList() {
		$list = [];
		$sports = GameSportsModel::find()->with(['gameSportsLocales' => function($q){$q->indexBy('lang_code');}])->all();
		foreach($sports as $sport){
			$list[$sport->id] = GameHelpers::getStringForLocale($sport->gameSportsLocales,'title',false,true);
		}
		asort($list);
		return $list;
	}
}

<?php

namespace backend\modules\games\controllers;

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\GameMarketsGroupsAdditionalModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameSportsModel;
use Yii;
use common\modules\games\models\GameMarketsGroupsModel;
use backend\modules\games\models\search\GameMarketsGroupsSearch;
use yii\db\ActiveQuery;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * GameMarketsGroupsController implements the CRUD actions for GameMarketsGroupsModel model.
 */
class GameMarketsGroupsController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}


	/**
	 * @param $sport_id
	 * @return string
	 */

	public function actionIndex(int $sport_id) {
		$searchModel = new GameMarketsGroupsSearch();
		$searchModel->sports_id = $sport_id;
		$searchModel->scenario = GameMarketsGroupsSearch::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionCreate(int $sport_id) {
		$model = new GameMarketsGroupsModel(['scenario' => GameMarketsGroupsModel::SCENARIO_CREATE]);
		$model->sports_id = $sport_id;
		if($model->load(Yii::$app->request->post()) && $model->save()){

			/* set locales */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){
						continue;
					}
					$locale = new GameMarketsGroupsLocaleModel(['scenario' => GameMarketsGroupsLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1, 'sports_id' => $model->sports_id, 'group_id' => $model->id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
					$locale->save();
				}
			}

			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'group_id' => $model->id]);
		}

		return $this->render('group', compact('model', 'searchModel', 'dataProvider'));
	}

	public function actionUpdate(int $group_id) {
		/* locales */
		$searchModel = new GameMarketsGroupsLocaleModel();
		$searchModel->group_id = (int)$group_id;
		$searchModel->scenario = GameMarketsGroupsLocaleModel::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		/* group */
		$model = $this->findGroupWithAdditionalMarkets($group_id);
		$model->scenario = GameMarketsGroupsModel::SCENARIO_UPDATE;

		if($model->load(Yii::$app->request->post()) && $model->save()){

			/* set new locales */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){
						continue;
					}
					$locale = new GameMarketsGroupsLocaleModel(['scenario' => GameMarketsGroupsLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1, 'sports_id' => $model->sports_id, 'group_id' => $model->id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
					$locale->save();
				}
			}

			GameHelpers::setMessage(true);
			return $this->refresh();
		}

		return $this->render('group', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'model' => $model,
			'sport_groups' => $this->getSportGroupsWithoutRequested($model->sports_id, $model->id)
		]);
	}

	public function actionLocaleUpdate(int $id) {
		$model = $this->findGroupLocale($id);
		$model->scenario = GameMarketsGroupsLocaleModel::SCENARIO_UPDATE;
		$model->is_active = 1;
		if($model->load(Yii::$app->request->post()) && $model->save()){
			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'group_id' => $model->group_id]);
		}
		return $this->render('locale-update', compact('model'));
	}

	public function actionChangeStatus(int $id, int $status) {
		$model = $this->findGroup($id);
		if(in_array($status, [$model::STATUS_DISABLED, $model::STATUS_ENABLED])){
			$model->is_enabled = $status;
			GameHelpers::setMessage($model->save(false, ['is_enabled']));
		}
		return $this->redirect(Yii::$app->request->referrer);
	}


	/**
	 * Finds the GameMarketsGroupsModel model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return GameMarketsGroupsModel the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findGroup($id) {
		if(($model = GameMarketsGroupsModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findGroupWithAdditionalMarkets($id) {
		if(($model = \backend\modules\games\models\GameMarketsGroupsModel::find()->where(['id' => $id])->with(['additionalMarkets' => function ($q) {
				$q->orderBy(['sort_order' => SORT_ASC])->with([
					'market.group.gameMarketsGroupsLocales' => function($q){$q->indexBy('lang_code');},
					'market.gameMarketsLocales' => function($q){$q->indexBy('lang_code');}
				]);
			}])->one()) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findGroupLocale($id) {
		if(($model = GameMarketsGroupsLocaleModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function getSportGroupsWithoutRequested($sports_id, $requested_id) {
		$result = [];
		$groups = GameMarketsGroupsModel::find()->where(['sports_id' => $sports_id])->andWhere(['!=', 'id', $requested_id])->with(['gameMarketsGroupsLocales' => function ($q) {
			return $q->indexBy('lang_code');
		}])->all();
		if($groups != []){
			$result = ArrayHelper::map($groups, 'id', function ($array) {
				return GameHelpers::getStringForLocale($array->gameMarketsGroupsLocales, 'title', true, true);
			});
		}
		return $result;
	}

	public function actionGetMarketsGroup(int $id) {
		$list = '';
		if(!empty($group_id = (int) Yii::$app->request->post('id'))){
			$markets = GameMarketsModel::find()
				->where(['group_id' => $group_id])
				->andWhere("game_markets.id NOT IN (SELECT market_id FROM game_markets_groups_additional WHERE `group_id` = {$id})")
				->with(['gameMarketsGroupsLocales' => function ($q) {	return $q->indexBy('lang_code'); }])
				->all();
			if($markets != []){
				$data = ArrayHelper::map($markets,'id',function($array){
					return GameHelpers::getStringForLocale($array->gameMarketsLocales, 'title', false, true);
				});
				foreach($data as $id => $title){
					$list .= "<li data-id='{$id}'>{$title}<span class='fa fa-angle-double-right'></span></li>";
				}
			}
		}
		return $list;
	}

	public function actionSaveSelectedMarkets(){
		$result = false;
		if(!empty($group_id = (int) Yii::$app->request->post('group'))){
			$markets = Yii::$app->request->post('market') ?? [];
			$group = \backend\modules\games\models\GameMarketsGroupsModel::find()->where(['id' => $group_id])->with('additionalMarkets')->one();
			if($group != Null){
				$group->saveAdditionalMarkets($markets);
				$result = true;
			}
		}
		GameHelpers::setMessage($result);
		return  $this->redirect(Yii::$app->request->referrer);
	}
}

<?php

namespace backend\modules\games\controllers;

use backend\modules\games\components\GameHelpers;
use backend\modules\games\forms\BanForm;
use backend\modules\games\models\EventGamePromoModel;
use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventTournamentLocaleModel;
use common\modules\games\models\EventTournamentModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameParticipantsLocaleModel;
use common\modules\games\models\GameParticipantsModel;
use common\modules\games\models\GameSportsLocaleModel;
use common\modules\games\models\GameSportsModel;
use Yii;
use common\modules\games\models\EventGameModel;
use backend\modules\games\models\search\EventGameSearch;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * EventGameController implements the CRUD actions for EventGameModel model.
 */
class EventGameController extends Controller
{
	/**
	 * @inheritdoc
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}

	/**
	 * Lists all EventGameModel models.
	 * @param int $tournament_id
	 * @return mixed
	 * @internal param int $sport
	 */
	public function actionIndex($tournament_id = false) {
		$searchModel = new EventGameSearch();
		$searchModel->scenario = EventGameModel::SCENARIO_SEARCH;

		if($tournament_id){
			$searchModel->tournament_id = $tournament_id;
			$positioning = 'tournament';
			$sportList = [];
			$tournamentList = [];
			$tournamentModel = EventTournamentModel::findOne($tournament_id);
		}else{
			$positioning = 'all';
			$sportList = $this->getSportList();
			$tournamentList = ArrayHelper::map(EventTournamentLocaleModel::find()->select('tournament_id,title')->where(['lang_code' => 'en'])->orderBy('title')->all(), 'tournament_id', 'title');
			$tournamentModel = null;
			$searchModel->extraOptions = true;
		}

		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'tournamentModel' => $tournamentModel,
			'sportList' => $sportList,
			'tournamentList' => $tournamentList,
			'positioning' => $positioning
		]);
	}

	public function actionCreate(int $tournament_id) {
		$model = new \backend\modules\games\models\EventGameModel(['scenario' => EventGameModel::SCENARIO_CREATE]);
		$tournament = $this->findTournament($tournament_id);
		$model->tournament_id = $tournament->id;
		$model->sports_id = $tournament->sports_id;

		if($model->load(Yii::$app->request->post()) && $model->save()){

			/* set locales */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){
						continue;
					}
					$locale = new EventGameLocaleModel(['scenario' => EventGameLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1, 'game_id' => $model->id, 'sports_id' => $model->sports_id, 'tournament_id' => $model->tournament_id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
					$locale->save();
				}
			}

			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'game_id' => $model->id]);
		}
		return $this->render('game', [
			'model' => $model,
			'participantsList' => '',
			'searchModel' => '',
			'dataProvider' => '',
		]);
	}

	public function actionUpdate(int $game_id) {
		/* locales */
		$searchModel = new EventGameLocaleModel();
		$searchModel->game_id = (int)$game_id;
		$searchModel->scenario = EventGameLocaleModel::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		/* game */
		$model = \backend\modules\games\models\EventGameModel::findOne($game_id);
		if(!$model){
			throw new NotFoundHttpException('The requested page does not exist.');
		}
		$model->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;

		if($model->load(Yii::$app->request->post()) && $model->save()){

			/* set new locales */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){
						continue;
					}
					$locale = new EventGameLocaleModel(['scenario' => EventGameLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1, 'game_id' => $model->id, 'sports_id' => $model->sports_id, 'tournament_id' => $model->tournament_id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
					$locale->save();
				}
			}

			GameHelpers::setMessage(true);
			return $this->refresh();
		}

		return $this->render('game', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'model' => $model,
			'participantsList' => $model->getParticipantsHtml(),
		]);
	}

	public function actionLocaleUpdate(int $id) {
		$model = $this->findGameLocale($id);
		$model->scenario = EventGameLocaleModel::SCENARIO_UPDATE;
		$model->is_active = 1;
		if($model->load(Yii::$app->request->post()) && $model->save()){
			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'game_id' => $model->game_id]);
		}
		return $this->render('locale-update', compact('model'));
	}

	public function actionChangeStatus(int $id, int $status) {
		$model = $this->findGame($id);
		if(in_array($status, [$model::STATUS_DISABLED, $model::STATUS_ENABLED])){
			$model->is_enabled = $status;
			GameHelpers::setMessage($model->save(false, ['is_enabled']));
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionSetBan() {
		if(Yii::$app->request->isPost && !empty($id = (int)Yii::$app->request->post('item_id'))){
			$model = Yii::createObject(BanForm::class);
			if($model->load(Yii::$app->request->post()) && $model->banGame($this->findGame($id))){
				GameHelpers::setMessage(true, Yii::t('games', 'Game banned'));
			}
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionSetFinishedGame($id) {
		$model = $this->findGame($id);
		GameHelpers::setMessage((bool)$model->updateAttributes(['is_finished' => 1]));;
		$this->redirect(Yii::$app->request->referrer);
	}


	/**
	 * Finds the EventGameModel model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return EventGameModel the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findGame($id) {
		if(($model = EventGameModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * Finds the EventGameLocaleModel model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return EventGameLocaleModel the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findGameLocale($id) {
		if(($model = EventGameLocaleModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findTournament($id) {
		if(($model = EventTournamentModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function getSportList() {
		$list = [];
		$sports = GameSportsModel::find()->with(['gameSportsLocales' => function ($q) {
			$q->indexBy('lang_code');
		}])->all();
		foreach($sports as $sport){
			$list[$sport->id] = GameHelpers::getStringForLocale($sport->gameSportsLocales, 'title', false, true);
		}
		asort($list);
		return $list;
	}

	public function actionGetParticipantsList($string, $sport_id) {
		\Yii::$app->response->format = Response::FORMAT_JSON;
		$out = ['results' => ['id' => '', 'text' => '']];
		if(!is_null($string) && !is_null($sport_id)){
			$query = new Query();
			$query->select('participant_id AS id, name AS text')
				->from(GameParticipantsLocaleModel::tableName())
				->where(['like', 'name', $string])
				->andWhere(['sports_id' => (int)$sport_id, 'lang_code' => 'en'])
				->limit(20);

			$command = $query->createCommand();
			$data = $command->queryAll();
			$out['results'] = array_values($data);
		}
		return $out;
	}

	/* Promo Game */

	public function actionPromoGame(int $id) {
		$game = $this->findGame($id);
		$promo = EventGamePromoModel::findPromo($game);
		$outcomes = $promo->marketOutcomesHtml(false, $game->id);
		return $this->render('promo-game', [
			'markets' => $promo->marketsList(),
			'game' => $game,
			'promo' => $promo,
			'outcomes' => $outcomes
		]);
	}

	public function actionGetOutcomes() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		if(Yii::$app->request->isPost){
			$market = (int)Yii::$app->request->post()['market'];
			$game = (int)Yii::$app->request->post()['game'];
			return !empty($market) ? (new EventGamePromoModel())->marketOutcomesHtml($market, $game) : false;
		}
		return false;
	}

	public function actionCudPromoGame($id = false) {
		if($id){ // delete promo
			$promo = EventGamePromoModel::findOne((int)$id);
			if($promo->delete()){
				GameHelpers::setMessage(true);
				return $this->redirect('index');
			}
		}elseif(!$id && Yii::$app->request->isPost){ // update or create promo
			$game = $this->findGame((int)Yii::$app->request->post('game_id'));
			$promo = EventGamePromoModel::findPromo($game);
			$outcomes = array_keys(Yii::$app->request->post()['outcomes']);
			$promo->setAttributes([
				'outcome_1st_id' => (int)$outcomes[0],
				'outcome_2nd_id' => (int)$outcomes[1]
			]);
			if($promo->save()){
				GameHelpers::setMessage(true);
				return $this->redirect('index');
			}
		}
		GameHelpers::setMessage(false);
		return $this->redirect(Yii::$app->request->referrer);
	}

}

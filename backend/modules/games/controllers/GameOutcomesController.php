<?php

namespace backend\modules\games\controllers;

use backend\modules\games\components\GameHelpers;
use backend\modules\games\models\search\GameOutcomesSearch;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use common\modules\games\models\GameOutcomesModel;
use Yii;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\Controller;
use yii\web\NotFoundHttpException;


class GameOutcomesController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}

	/**
	 * @param int $market_id
	 * @return string
	 */

	public function actionIndex(int $market_id) {
		$searchModel = new GameOutcomesSearch();
		$searchModel->scenario = GameOutcomesSearch::SCENARIO_SEARCH;
		$searchModel->market_id = $market_id;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$marketModel = GameMarketsModel::findOne($market_id);

		$outcomesList = $this->getOutcomesList($market_id);
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'marketModel' => $marketModel,
			'outcomesList' => $outcomesList
		]);
	}

	public function actionCreate(int $market_id) {
		$model = new GameOutcomesModel(['scenario' => GameOutcomesModel::SCENARIO_CREATE]);
		$market = $this->findMarket($market_id);

		if($model->load(Yii::$app->request->post())){
			$model->setAttributes(['market_id' => $market->id, 'sports_id' => $market->sports_id, 'group_id' => $market->group_id]);
			if($model->save()){

				/* set locales */
				if(!empty(Yii::$app->request->post()['locale'])){
					foreach(Yii::$app->request->post()['locale'] as $lang => $title){
						if(empty($title)){
							continue;
						}
						$locale = new GameOutcomesLocaleModel(['scenario' => GameOutcomesLocaleModel::SCENARIO_CREATE]);
						$locale->setAttributes(['is_active' => 1,'outcome_id' => $model->id, 'market_id' => $model->market_id, 'sports_id' => $model->sports_id, 'group_id' => $model->group_id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
						$locale->save();
					}
				}
			}
			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'outcome_id' => $model->id]);
		}

		return $this->render('outcome', compact('model', 'market', 'searchModel', 'dataProvider'));
	}

	public function actionUpdate(int $outcome_id) {
		/* locales */
		$searchModel = new GameOutcomesLocaleModel();
		$searchModel->outcome_id = (int)$outcome_id;
		$searchModel->scenario = GameOutcomesLocaleModel::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		/* outcome */
		$model = $this->findOutcomes($outcome_id);
		$model->scenario = GameOutcomesModel::SCENARIO_UPDATE;

		if($model->load(Yii::$app->request->post()) && $model->save()){

			/* set new locales */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){
						continue;
					}
					$locale = new GameOutcomesLocaleModel(['scenario' => GameOutcomesLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1,'outcome_id' => $model->id, 'market_id' => $model->market_id, 'sports_id' => $model->sports_id, 'group_id' => $model->group_id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
					$locale->save();
				}
			}

			GameHelpers::setMessage(true);
			return $this->refresh();
		}

		return $this->render('outcome', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'model' => $model,
			'market' => null,
		]);
	}

	public function actionLocaleUpdate(int $id) {
		$model = $this->findOutcomesLocale($id);
		$model->scenario = GameOutcomesLocaleModel::SCENARIO_UPDATE;
		$model->is_active = 1;
		if($model->load(Yii::$app->request->post()) && $model->save()){
			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'outcome_id' => $model->outcome_id]);
		}
		return $this->render('locale-update', compact('model'));
	}

	public function actionChangeStatus(int $id, int $status) {
		$model = $this->findOutcomes($id);
		if(in_array($status, [$model::STATUS_DISABLED, $model::STATUS_ENABLED])){
			$model->is_enabled = $status;
			GameHelpers::setMessage($model->save(false, ['is_enabled']));
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionMergerOutcomes() {
		if(Yii::$app->request->isPost){

			$targetOutcomeId = !empty(Yii::$app->request->post('target_outcome')) ? (int)Yii::$app->request->post('target_outcome') : null;
			$selectedOutcomes = !empty(Yii::$app->request->post('selected-outcomes')) ? json_decode(Yii::$app->request->post('selected-outcomes')) : null;
			$selected = !empty(Yii::$app->request->post('selected')) ? (int) Yii::$app->request->post('selected') : null;

			if($targetOutcomeId && $selectedOutcomes && !in_array($targetOutcomeId, $selectedOutcomes)){
				$outcome = \backend\modules\games\models\GameOutcomesModel::findOne($targetOutcomeId);
				if($outcome !== null){

					if($selected){
						$result = $outcome->mergerOutcomesSetCondition($selectedOutcomes,$selected);
						GameHelpers::setMessage($result);
						return $this->redirect(Url::to(['index', 'market_id' => $outcome->market_id]));
					}

					/* if condition empty */
					if(empty($outcome->condition) && count($selectedOutcomes) > 1){
						$outcomes = GameOutcomesModel::find()->where(['IN','id',$selectedOutcomes])->with(['gameOutcomesLocales' => function($q){ $q->indexBy('lang_code'); }])->all();
						return $this->render('merge_with_condition',[
							'targetModel' => $outcome,
							'models' => $outcomes,
							'selectedOutcomes' => Yii::$app->request->post('selected-outcomes')
						]);
					}
					/* ./if condition empty */

					$result = $outcome->mergerOutcomes($selectedOutcomes);
					GameHelpers::setMessage($result);
				}
			}else{
				GameHelpers::setMessage(false);
			}
		}
		return $this->redirect(Yii::$app->request->referrer);
	}


	protected function findOutcomes($id) {
		if(($model = GameOutcomesModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findOutcomesLocale($id) {
		if(($model = GameOutcomesLocaleModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function findMarket($id) {
		if(($model = GameMarketsModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	protected function getOutcomesList($market_id) {
		$list = [];
		$outcomes = GameOutcomesModel::find()->where(['market_id' => $market_id])->with(['gameOutcomesLocales' => function($q){$q->indexBy('lang_code');}])->all();
		foreach($outcomes as $outcome){
			$list[$outcome->id] = GameHelpers::getStringForLocale($outcome->gameOutcomesLocales,'title',false,true);
		}
		asort($list);
		return $list;
	}
}

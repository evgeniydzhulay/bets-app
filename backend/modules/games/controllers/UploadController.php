<?php

namespace backend\modules\games\controllers;

use backend\modules\games\actions\FileUploadAction;
use backend\modules\games\actions\ImageUploadAction;
use yii\web\Controller;

/**
 * @package common\modules\content\controllers
 *
 * @property mixed $redactor
 * @property mixed $storage
 */

class UploadController extends Controller {

    public $enableCsrfValidation = false;

    /**
     * List of available upload actions
     *
     * @return array
     */
    public function actions() {
        return [
            'file' => [
                'class' => FileUploadAction::class,
            ],
            'image' => [
                'class' => ImageUploadAction::class,
            ]
        ];
    }
}

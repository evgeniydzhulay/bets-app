<?php

namespace backend\modules\games\controllers;

use backend\modules\games\components\GameHelpers;
use backend\modules\games\models\GameOutcomesModel;
use backend\modules\games\models\Translator_1X_Model;
use backend\modules\games\models\Translator_365_Model;
use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventTournamentLocaleModel;
use common\modules\games\models\EventTournamentModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use common\modules\games\models\GameParticipantsLocaleModel;
use common\modules\games\models\GameParticipantsModel;
use common\modules\games\models\GameSportsLocaleModel;
use common\modules\games\models\GameSportsModel;
use Google\Cloud\Translate\TranslateClient;
use Yii;
use yii\base\DynamicModel;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;


class AdminController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			]
		];
	}


	public function beforeAction($action) {
		if($action->id == 'verify-translate-update'){
			$this->enableCsrfValidation = false;
		}
		return parent::beforeAction($action);
	}


	public function actionIndex() {
		throw new NotFoundHttpException('The requested page does not exist.');
	}

	public function actionTranslate365() {
		(new Translator_365_Model())->translate();
		die('Done!');
	}

	public function actionTranslate1x() {
		(new Translator_1X_Model())->translate();
		die('Done!');
	}

	public function actionVerifyTranslate($item = false) {
		$this->enableCsrfValidation = false;
		$dataItems = [
			'game-types' => GameSportsLocaleModel::find()->where(['is_active' => 0])->count(),
			'markets-group' => GameMarketsGroupsLocaleModel::find()->where(['is_active' => 0])->count(),
			'markets' => GameMarketsLocaleModel::find()->where(['is_active' => 0])->count(),
			'outcomes' => GameOutcomesLocaleModel::find()->where(['is_active' => 0])->count(),
			'tournament' => EventTournamentLocaleModel::find()->where(['is_active' => 0])->count(),
			'games' => EventGameLocaleModel::find()->where(['is_active' => 0])->count(),
			'participants' => GameParticipantsLocaleModel::find()->where(['is_active' => 0])->count()
		];

		if(!$item && array_sum($dataItems) > 0){
			arsort($dataItems);
			foreach($dataItems as $key => $v){
				$item = $key;
				break;
			}
		}

		switch($item){
			case 'game-types' :
				$query = GameSportsModel::find()->joinWith([
					'gameSportsLocales' => function ($q) {
						$q->indexBy('lang_code');
					}
				])->where([GameSportsLocaleModel::tableName() . '.is_active' => 0])->groupBy(GameSportsModel::tableName() . '.id');
				break;

			case 'markets-group' :
				$query = GameMarketsGroupsModel::find()->joinWith(['gameMarketsGroupsLocales' => function ($q) {
					$q->indexBy('lang_code');
				}])->where([GameMarketsGroupsLocaleModel::tableName() . '.is_active' => 0])->with([
					'sports.gameSportsLocales' => function ($q) {
						$q->indexBy('lang_code');
					}
				]);
				break;

			case 'markets' :
				$query = GameMarketsModel::find()->where([GameMarketsLocaleModel::tableName() . '.is_active' => 0])
					->joinWith([
						'gameMarketsLocales' => function ($q) {
							$q->indexBy('lang_code');
						}
					])
					->with([
						'sports.gameSportsLocales' => function ($q) {
							$q->indexBy('lang_code');
						},
						'group.gameMarketsGroupsLocales' => function ($q) {
							$q->indexBy('lang_code');
						}
					]);
				break;

			case 'outcomes' :
				$query = \common\modules\games\models\GameOutcomesModel::find()->where([GameOutcomesLocaleModel::tableName() . '.is_active' => 0])
					->joinWith([
						'gameOutcomesLocales' => function ($q) {
							$q->indexBy('lang_code');
						}
					])
					->with([
						'sports.gameSportsLocales' => function ($q) {
							$q->indexBy('lang_code');
						},
						'group.gameMarketsGroupsLocales' => function ($q) {
							$q->indexBy('lang_code');
						},
						'market.gameMarketsLocales' => function ($q) {
							$q->indexBy('lang_code');
						}
					]);
				break;

			case 'tournament' :
				$query = EventTournamentModel::find()->where([EventTournamentLocaleModel::tableName() . '.is_active' => 0])
					->joinWith([
						'eventTournamentLocales' => function ($q) {
							$q->indexBy('lang_code');
						}
					])
					->with([
						'sports.gameSportsLocales' => function ($q) {
							$q->indexBy('lang_code');
						}
					]);
				break;

			case 'games' :
				$query = EventGameModel::find()->where([EventGameLocaleModel::tableName() . '.is_active' => 0])
					->joinWith([
						'eventGameLocales' => function ($q) {
							$q->indexBy('lang_code');
						}
					])
					->with([
						'sports.gameSportsLocales' => function ($q) {
							$q->indexBy('lang_code');
						},
						'tournament.eventTournamentLocales' => function ($q) {
							$q->indexBy('lang_code');
						}
					]);
				break;

			case 'participants' :
				$query = GameParticipantsModel::find()->where([GameParticipantsLocaleModel::tableName() . '.is_active' => 0])
					->joinWith([
						'gameParticipantsLocales' => function ($q) {
							$q->indexBy('lang_code');
						}
					])
					->with([
						'sports.gameSportsLocales' => function ($q) {
							$q->indexBy('lang_code');
						}
					]);
				break;
			default:
				throw new NotFoundHttpException('No data for verify!');
		}


		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 50,
			],
		]);

		$get = $item;

		return $this->render('verify-translate', compact('dataProvider', 'dataItems', 'get'));
	}

	public function actionVerifyTranslateUpdate() {
		if(Yii::$app->request->isPost){
			$item = Yii::$app->request->post('model');
			$item_id = (int)Yii::$app->request->post('model-id');
			$title = strip_tags(Yii::$app->request->post('title'));
			switch($item){
				case 'game-types' :
					$model = GameSportsLocaleModel::findOne($item_id);
					break;

				case 'markets-group' :
					$model = GameMarketsGroupsLocaleModel::findOne($item_id);
					break;

				case 'markets' :
					$model = GameMarketsLocaleModel::findOne($item_id);
					break;

				case 'outcomes' :
					$model = GameOutcomesLocaleModel::findOne($item_id);
					break;

				case 'tournament' :
					$model = EventTournamentLocaleModel::findOne($item_id);
					break;

				case 'games' :
					$model = EventGameLocaleModel::findOne($item_id);
					break;

				case 'participants' :
					$model = GameParticipantsLocaleModel::findOne($item_id);
					break;
				default:
					$model = null;
			}
			$attribute = ($item == 'participants') ? 'name' : 'title';
			if($model != null && $model->updateAttributes([$attribute => $title, 'is_active' => 1])){
				GameHelpers::setMessage(true);
				return $this->redirect(Yii::$app->request->referrer);
			}
		}
		GameHelpers::setMessage(false);
		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionImagesApprove() {
		if(Yii::$app->request->isPost && !empty(Yii::$app->request->post('ids'))){
			$ids = json_decode(Yii::$app->request->post('ids'));
			$selected = Yii::$app->request->post('selected') ?? [];
			if(is_array($ids)){
				foreach($ids as $id){
					$model = GameParticipantsModel::findOne($id);
					if($model !== null){
						$attributes = ['image_is_approved' => 1];
						if(in_array($model->id, $selected)){
							$attributes = $attributes + ['image' => Null];
						}
						$model->updateAttributes($attributes);
						if($model->image === Null){
							$model->deleteImage();
						}
					}
				}
			}
			return $this->refresh();
		}
		$models = GameParticipantsModel::find()->where('`image_is_approved` = 0 AND `image` IS NOT NULL')->with(['gameParticipantsLocales' => function ($q) {
			return $q->indexBy('lang_code');
		}])->limit(120)->all();
		return $this->render('images-approve', compact('models'));
	}

	/* ! Participants Titles ! */

	public function actionLoadParticipantsTitles($result = false) {
		$sportList = GameSportsLocaleModel::find()->select('title')->where(['lang_code' => 'ru'])->indexBy('sports_id')->column();

		$model = new DynamicModel(['game_sport', 'file']);
		$model->addRule(['game_sport','file'], 'required')
			->addRule(['game_sport'], 'in', ['range' => array_keys($sportList)])
			->addRule(['file'], 'file', ['skipOnEmpty' => false], ['extensions' => 'csv']);

		if( $model->load(Yii::$app->request->post()) && !empty($file = $_FILES['DynamicModel'])){
			$sport_id = (int) $model->game_sport;
			$f = fopen($file['tmp_name']['file'], "r");
			while (!feof($f)) {
				$str = fgets($f);
				if($str == ''){
					continue;
				}
				list($title_en,$title_ru) = explode(';',$str);
				if(!empty($title_en) && !empty($title_ru)){
					$title_ru = trim(strip_tags($title_ru));
					$title_en = BaseInflector::transliterate(trim($title_en));
					$locale_en = GameParticipantsLocaleModel::find()->where(['lang_code' => 'en','sports_id' => $sport_id,'name' => $title_en])->one();

					$result .= "[{$title_en} - {$title_ru}]";

					if($locale_en != null){
						$locale_ru = GameParticipantsLocaleModel::find()->where([
							'participant_id' => $locale_en->participant_id,
							'lang_code' => 'ru'
						])->one();
						if($locale_ru == null){
							$ru = new GameParticipantsLocaleModel(['scenario' => GameParticipantsLocaleModel::SCENARIO_CREATE]);
							$ru->sports_id = $locale_en->sports_id;
							$ru->participant_id = $locale_en->participant_id;
							$ru->lang_code = 'ru';
							$ru->is_active = 1;
							$ru->name = $title_ru;
							$ru->save();
							$result .= " --- <span>Сохранен</span>";
						}else{
							$result .= " --- <span>Уже есть</span>";
						}
					}else{
						$result .= " --- <span class='text-bold text-danger' style='font-size: 14px'>Не найден</span>";
					}
				}
				$result .= '<br />';
			}
			fclose($f);
		}

		return $this->render('load-participants-titles', compact('model', 'sportList','result'));
	}

	public function actionLoadMarketsTitles($result = false) {
		$sportList = GameSportsLocaleModel::find()->select('title')->where(['lang_code' => 'en'])->indexBy('sports_id')->column();

		$model = new DynamicModel(['game_sport', 'file']);
		$model->addRule(['game_sport','file'], 'required')
			->addRule(['game_sport'], 'in', ['range' => array_keys($sportList)])
			->addRule(['file'], 'file', ['skipOnEmpty' => false], ['extensions' => 'csv']);

		if( $model->load(Yii::$app->request->post()) && !empty($file = $_FILES['DynamicModel'])){
			$sport_id = (int) $model->game_sport;
			$f = fopen($file['tmp_name']['file'], "r");
			while (!feof($f)) {
				$str = fgets($f);
				if($str == ''){
					continue;
				}
				list($title_en,$title_ru) = explode(';',$str);
				if(!empty($title_en) && !empty($title_ru)){
					$title_ru = trim(strip_tags($title_ru));
					$title_en = BaseInflector::transliterate(trim($title_en));
					$locale_en = GameMarketsLocaleModel::find()->where(['lang_code' => 'en','sports_id' => $sport_id,'title' => $title_en])->one();

					$result .= "[{$title_en} - {$title_ru}]";

					if($locale_en != null){
						$locale_ru = GameMarketsLocaleModel::find()->where([
							'market_id' => $locale_en->market_id,
							'lang_code' => 'ru'
						])->one();

						if($locale_ru == null){
							$ru = new GameMarketsLocaleModel(['scenario' => GameMarketsLocaleModel::SCENARIO_CREATE]);
							$ru->sports_id = $locale_en->sports_id;
							$ru->market_id = $locale_en->market_id;
							$ru->group_id = $locale_en->group_id;
							$ru->lang_code = 'ru';
							$ru->is_active = 1;
							$ru->title = $title_ru;
							$ru->save();
							$result .= " --- <span>Сохранен</span>";
						}else{
							$result .= " --- <span>Уже есть</span>";
						}
					}else{
						$result .= " --- <span class='text-bold text-danger' style='font-size: 14px'>Не найден</span>";
					}
				}
				$result .= '<br />';
			}
			fclose($f);
		}

		return $this->render('load-markets-titles', compact('model', 'sportList','result'));
	}

	public function actionLoadTournamentsTitles($result = false) {
		$sportList = GameSportsLocaleModel::find()->select('title')->where(['lang_code' => 'en'])->indexBy('sports_id')->column();

		$model = new DynamicModel(['game_sport', 'file']);
		$model->addRule(['game_sport','file'], 'required')
			->addRule(['game_sport'], 'in', ['range' => array_keys($sportList)])
			->addRule(['file'], 'file', ['skipOnEmpty' => false], ['extensions' => 'csv']);

		if( $model->load(Yii::$app->request->post()) && !empty($file = $_FILES['DynamicModel'])){
			$sport_id = (int) $model->game_sport;
			$f = fopen($file['tmp_name']['file'], "r");
			while (!feof($f)) {
				$str = fgets($f);
				if($str == ''){
					continue;
				}
				list($title_en,$title_ru) = explode(';',$str);
				if(!empty($title_en) && !empty($title_ru)){
					$title_ru = trim(strip_tags($title_ru));
					$title_en = BaseInflector::transliterate(trim($title_en));
					$locale_en = EventTournamentLocaleModel::find()->where(['lang_code' => 'en','sports_id' => $sport_id,'title' => $title_en])->one();

					$result .= "[{$title_en} - {$title_ru}]";

					if($locale_en != null){
						$locale_ru = EventTournamentLocaleModel::find()->where([
							'tournament_id' => $locale_en->tournament_id,
							'lang_code' => 'ru'
						])->one();

						if($locale_ru == null){
							$ru = new EventTournamentLocaleModel(['scenario' => EventTournamentLocaleModel::SCENARIO_CREATE]);
							$ru->sports_id = $locale_en->sports_id;
							$ru->tournament_id = $locale_en->tournament_id;
							$ru->lang_code = 'ru';
							$ru->is_active = 1;
							$ru->title = $title_ru;
							$ru->save();
							$result .= " --- <span>Сохранен</span>";
						}else{
							$result .= " --- <span>Уже есть</span>";
						}
					}else{
						$result .= " --- <span class='text-bold text-danger' style='font-size: 14px'>Не найден</span>";
					}
				}
				$result .= '<br />';
			}
			fclose($f);
		}

		return $this->render('load-tournaments-titles', compact('model', 'sportList','result'));
	}
}

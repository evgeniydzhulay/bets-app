<?php

namespace backend\modules\games\controllers;

use backend\modules\games\components\GameHelpers;
use backend\modules\games\models\MergerMarkets;
use backend\modules\games\models\search\GameMarketsSearch;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\GameMarketsGroupsAdditionalModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use common\modules\games\models\GameOutcomesModel;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

/**
 * GameMarketsGroupsController implements the CRUD actions for GameMarketsModel model.
 */
class GameMarketsController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			],
		];
	}


	/**
	 * @param int $group_id
	 * @return string
	 */

	public function actionIndex(int $group_id) {
		$searchModel = new GameMarketsSearch();
		$searchModel->scenario = GameMarketsSearch::SCENARIO_SEARCH;
		$searchModel->group_id = $group_id;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);
		$gameMarketsGroupsModel = GameMarketsGroupsModel::findOne($group_id);

		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'gameMarketsGroupsModel' => $gameMarketsGroupsModel,
			'markersList' => $this->getMarketsList($group_id)
		]);
	}

	public function actionCreate(int $group_id) {
		$model = new GameMarketsModel(['scenario' => GameMarketsModel::SCENARIO_CREATE]);
		$group = $this->findGroup($group_id);

		if($model->load(Yii::$app->request->post())){
			$model->setAttributes(['sports_id' => $group->sports_id, 'group_id' => $group->id]);
			if($model->save()){
				/* set locales */
				if(!empty(Yii::$app->request->post()['locale'])){
					foreach(Yii::$app->request->post()['locale'] as $lang => $title){
						if(empty($title)){continue;}
						$locale = new GameMarketsLocaleModel(['scenario' => GameMarketsLocaleModel::SCENARIO_CREATE]);
						$locale->setAttributes(['is_active' => 1,'market_id' => $model->id, 'sports_id' => $model->sports_id, 'group_id' => $model->group_id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
						$locale->save();
					}
				}
			}
			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'market_id' => $model->id]);
		}

		return $this->render('market', compact('model', 'group', 'searchModel', 'dataProvider'));
	}

	public function actionUpdate(int $market_id) {
		/* locales */
		$searchModel = new GameMarketsLocaleModel();
		$searchModel->market_id = (int)$market_id;
		$searchModel->scenario = GameMarketsLocaleModel::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		/* market */
		$model = $this->findMarket($market_id);
		$model->scenario = GameMarketsModel::SCENARIO_UPDATE;

		if($model->load(Yii::$app->request->post())){
			if($model->isAttributeChanged('group_id', false)){
				$db = \Yii::$app->db;
				$transaction = $db->beginTransaction();
				try{
					$model->save();
					GameMarketsLocaleModel::updateAll(['group_id' => $model->group_id],['market_id' => $model->id]);
					GameOutcomesModel::updateAll(['group_id' => $model->group_id],['market_id' => $model->id]);
					GameOutcomesLocaleModel::updateAll(['group_id' => $model->group_id],['market_id' => $model->id]);
					EventOutcomesModel::updateAll(['group_id' => $model->group_id],['market_id' => $model->id]);
					GameMarketsGroupsAdditionalModel::deleteAll(['group_id' => $model->group_id,'market_id' => $model->id]);
					$transaction->commit();
				}catch(Exception $e){
					$transaction->rollback();
					GameHelpers::setMessage(false);
					return $this->refresh();
				}
			}else{
				$model->save();
			}
			/* set new locales */
			if(!empty(Yii::$app->request->post()['locale'])){
				foreach(Yii::$app->request->post()['locale'] as $lang => $title){
					if(empty($title)){continue;}
					$locale = new GameMarketsLocaleModel(['scenario' => GameMarketsLocaleModel::SCENARIO_CREATE]);
					$locale->setAttributes(['is_active' => 1,'market_id' => $model->id, 'sports_id' => $model->sports_id, 'group_id' => $model->group_id, 'lang_code' => $lang, 'title' => strip_tags($title)]);
					$locale->save();
				}
			}
			GameHelpers::setMessage(true);
			return $this->refresh();
		}

		return $this->render('market', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'model' => $model,
			'group' => null,
			'groupsList' => $this->getGroupsList($model)
		]);
	}

	public function actionLocaleUpdate(int $id) {
		$model = $this->findMarketLocale($id);
		$model->scenario = GameMarketsLocaleModel::SCENARIO_UPDATE;
		$model->is_active = 1;
		if($model->load(Yii::$app->request->post()) && $model->save()){
			GameHelpers::setMessage(true);
			return $this->redirect(['update', 'market_id' => $model->market_id]);
		}
		return $this->render('locale-update', compact('model'));
	}

	public function actionChangeStatus(int $id, int $status) {
		$model = $this->findMarket($id);
		if(in_array($status, [$model::STATUS_DISABLED, $model::STATUS_ENABLED])){
			$model->is_enabled = $status;
			GameHelpers::setMessage($model->save(false, ['is_enabled']));
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionMergeMarkets(){
		if(Yii::$app->request->isPost){
			$targetMarketId = !empty(Yii::$app->request->post('target_market')) ? (int) Yii::$app->request->post('target_market') : null;
			$selectedMarkets = !empty(Yii::$app->request->post('selected-markets')) ? json_decode(Yii::$app->request->post('selected-markets')) : null;
			if($targetMarketId && $selectedMarkets && !in_array($targetMarketId,$selectedMarkets)){
				$market = \backend\modules\games\models\GameMarketsModel::findOne($targetMarketId);
				if($market !== null){
					$result = $market->mergerMarkets($selectedMarkets);
					GameHelpers::setMessage($result);
				}
			}else{
				GameHelpers::setMessage(false);
			}
		}
		return $this->redirect(Yii::$app->request->referrer);
	}


	/**
	 * @param $id
	 * @return GameMarketsModel|null
	 * @throws NotFoundHttpException
	 */
	protected function findMarket($id) {
		if(($model = GameMarketsModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * @param $id
	 * @return GameMarketsLocaleModel|null
	 * @throws NotFoundHttpException
	 */
	protected function findMarketLocale($id) {
		if(($model = GameMarketsLocaleModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}

	}

	/**
	 * @param $id
	 * @return GameMarketsGroupsModel|null
	 * @throws NotFoundHttpException
	 */
	protected function findGroup($id) {
		if(($model = GameMarketsGroupsModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}

	/**
	 * @param $model GameMarketsModel
	 * @return array
	 */
	protected function getGroupsList($model) {
		$groups = GameMarketsGroupsLocaleModel::find()->select(['game_markets_groups_locale.group_id','title' => new Expression('COALESCE(current.title,game_markets_groups_locale.title)')])
			->leftJoin(GameMarketsGroupsLocaleModel::tableName() . 'AS current','game_markets_groups_locale.group_id = current.group_id AND current.lang_code = "'. Yii::$app->language .'"')
			->where([
				'game_markets_groups_locale.sports_id' => $model->sports_id,
				'game_markets_groups_locale.lang_code' => 'en',
			])->asArray()->all();

		return ArrayHelper::map($groups,'group_id','title');
	}

	protected function getMarketsList($group_id) {
		$list = [];
		$markets = GameMarketsModel::find()->where(['group_id' => $group_id])->with(['gameMarketsLocales' => function($q){$q->indexBy('lang_code');}])->all();
		foreach($markets as $market){
			$list[$market->id] = GameHelpers::getStringForLocale($market->gameMarketsLocales,'title',false,true);
		}
		asort($list);
		return $list;
	}
}

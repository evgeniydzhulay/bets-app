<?php

namespace backend\modules\games\controllers;

use backend\modules\games\components\GameHelpers;
use backend\modules\games\forms\BanForm;
use backend\modules\games\models\SetOutcomeStatus;
use common\modules\games\models\SetBetsResult;
use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\EventTournamentLocaleModel;
use common\modules\games\models\GameSportsLocaleModel;
use Yii;
use common\modules\games\models\BetsModel;
use backend\modules\games\models\search\BetsSearch;
use yii\db\Expression;
use yii\db\Query;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * BetsController implements the CRUD actions for BetsModel model.
 */
class BetsController extends Controller
{

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['admin'],
					],
				],
			]
		];
	}

	/**
	 * Lists all BetsModel models.
	 * @return mixed
	 */
	public function actionIndex() {
		$searchModel = new BetsSearch();
		$searchModel->scenario = BetsSearch::SCENARIO_SEARCH;
		$dataProvider = $searchModel->search(Yii::$app->request->queryParams);

		$modalForm = Yii::createObject(BanForm::class);

		return $this->render('index', [
			'sportsList' => $this->getSportList(),
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
			'total' => $searchModel->total_sum,
			'modalForm' => $modalForm
		]);
	}

	public function actionBetsRevertIndex($id = false) {
		$searchModel = new BetsSearch();
		$searchModel->scenario = BetsSearch::SCENARIO_SEARCH;

		if($id){
			$searchModel->id = $id;
		}

		$dataProvider = $searchModel->searchBetsReverted(Yii::$app->request->queryParams);

		return $this->render('bets-reverted-index', [
			'searchModel' => $searchModel,
			'dataProvider' => $dataProvider,
		]);
	}

	public function actionBetBan() {
		$id = (int)Yii::$app->request->post('bet_id');
		$form = Yii::createObject(BanForm::class);
		if($form->load(Yii::$app->request->post()) && $form->banBet($this->findModel($id))){
			GameHelpers::setMessage(true, 'Bet banned');
		}
		return $this->redirect(Yii::$app->request->referrer);
	}

	public function actionExpiredOutcome(int $outcome_id) {
		if(Yii::$app->request->isPost){
			if(!empty(Yii::$app->request->post('data')['selected_outcomes'])){
				$model = new SetOutcomeStatus();
				$model->load(Yii::$app->request->post('data'));
				$model->setStatusManually();
				return $this->redirect('index');
			}
			GameHelpers::setMessage(false, Yii::t('games', 'Please select'));
		}
		$data = EventOutcomesModel::find()->where(['id' => $outcome_id])->with(
			[
				'sportsLocale' => function ($q) {
					$q->where(['lang_code' => ['en','ru']])->indexBy('lang_code');
				},
				'tournamentLocale' => function ($q) {
					$q->where(['lang_code' => ['en','ru']])->indexBy('lang_code');
				},
				'groupLocale' => function ($q) {
					$q->where(['lang_code' => ['en','ru']])->indexBy('lang_code');
				},
				'market.gameMarketsLocales' => function ($q) {
					$q->where(['lang_code' => ['en','ru']])->indexBy('lang_code');
				},
				'game.eventGameLocales' => function ($q) {
					$q->where(['lang_code' => ['en','ru']])->indexBy('lang_code');
				},
				'game.eventParticipants.participantLocale' => function ($q) {
					$q->where(['lang_code' => ['en','ru']])->indexBy('lang_code');
				},
			])->one();
		if($data != null){
			$outcomes = EventOutcomesModel::find()->where([EventOutcomesModel::tableName() . '.game_id' => $data->game_id, EventOutcomesModel::tableName() . '.market_id' => $data->market_id])->with(['outcomeLocale' => function ($q) {
				$q->where(['lang_code' => ['en','ru']])->indexBy('lang_code');
			}])
				->joinWith('outcome', false)->orderBy('game_outcomes.sort_order')/* order as in dictionary  */
				->all();
			return $this->render('expired-outcome', compact('outcomes', 'data'));
		}
		throw new NotFoundHttpException('The requested page does not exist.');
	}


	/**
	 * Finds the BetsModel model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 * @param integer $id
	 * @return BetsModel the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel($id) {
		if(($model = BetsModel::findOne($id)) !== null){
			return $model;
		}else{
			throw new NotFoundHttpException('The requested page does not exist.');
		}
	}




	/* Helpers */


	/**
	 * @return array
	 */
	private function getSportList() {
		$list = GameSportsLocaleModel::find()->select('COALESCE(`current`.`title`,`game_sports_locale`.`title`) as title, game_sports_locale.sports_id')
			->leftJoin(GameSportsLocaleModel::tableName() . ' as current', 'current.sports_id = game_sports_locale.sports_id AND current.lang_code = "' . Yii::$app->language . '"')
			->where('game_sports_locale.lang_code = "en"')->all();
		return ArrayHelper::map($list,'sports_id','title');
	}

	/**
	 * @param string $title
	 * @param int|bool $sport = id game sport
	 * @return array
	 */

	public function actionTournamentsList($title, $sport = false) {
		\Yii::$app->response->format = Response::FORMAT_JSON;
		$out = ['results' => ['id' => '', 'text' => '']];
		if(!is_null($title)){
			$query = new Query();
			$query->select('tournament_id AS id, title AS text')
				->from(EventTournamentLocaleModel::tableName())
				->where(['like', 'title', $title])
				->limit(20);

			if($sport){
				$query->andWhere(['sports_id' => (int)$sport]);
			}

			$command = $query->createCommand();
			$data = $command->queryAll();
			$out['results'] = array_values($data);
		}
		return $out;
	}

	/**
	 * @param string $title
	 * @param int|bool $sport
	 * @param int|bool $tournament
	 * @return array
	 */

	public function actionGamesList($title, $sport = false, $tournament = false) {
		\Yii::$app->response->format = Response::FORMAT_JSON;
		$out = ['results' => ['id' => '', 'text' => '']];
		if(!is_null($title)){
			$query = new Query();
			$query->select('game_id AS id, title AS text')
				->from(EventGameLocaleModel::tableName())
				->where(['like', 'title', $title])
				->limit(20);

			if($sport){
				$query->andWhere(['sports_id' => (int)$sport]);
			}

			if($tournament){
				$query->andWhere(['tournament_id' => (int)$tournament]);
			}

			$command = $query->createCommand();
			$data = $command->queryAll();
			$out['results'] = array_values($data);
		}
		return $out;
	}


}

<?php

namespace backend\modules\games\widgets;


use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventTournamentLocaleModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use common\modules\games\models\GameParticipantsLocaleModel;
use common\modules\games\models\GameParticipantsModel;
use common\modules\games\models\GameSportsLocaleModel;
use yii\base\Widget;

class NotificationButtons extends Widget
{
	private $translations = false;
	private $markets = false;
	private $participant_image = false;

	public function init() {
		$this->translations = $this->checkTranslations();
		$this->markets = $this->checkMarketsWithoutGroup();
		$this->participant_image = $this->checkNewImages();
	}

	public function run() {
		return $this->render('notification-buttons', [
			'translations' => $this->translations,
			'markets' => $this->markets,
			'participant_image' => $this->participant_image
		]);
	}

	private function checkTranslations() {
		if(EventTournamentLocaleModel::find()->where(['is_active' => 0])->exists())
			return true;
		if(EventGameLocaleModel::find()->where(['is_active' => 0])->exists())
			return true;
		if(GameOutcomesLocaleModel::find()->where(['is_active' => 0])->exists())
			return true;
		if(GameSportsLocaleModel::find()->where(['is_active' => 0])->exists())
			return true;
		if(GameMarketsGroupsLocaleModel::find()->where(['is_active' => 0])->exists())
			return true;
		if(GameMarketsLocaleModel::find()->where(['is_active' => 0])->exists())
			return true;
		if(GameParticipantsLocaleModel::find()->where(['is_active' => 0])->exists())
			return true;
		return false;
	}

	private function checkMarketsWithoutGroup() {
		if(GameMarketsModel::find()->where("group_id IS NULL")->exists()){
			return true;
		}
		return false;
	}

	private function checkNewImages() {
		if(GameParticipantsModel::find()->where('`image_is_approved` = 0 AND `image` IS NOT NULL')->exists()){
			return true;
		}
		return false;
	}

}
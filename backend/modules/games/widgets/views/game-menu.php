<?php

/* @var $this \yii\web\View */
/* @var $sportId null */

use yii\bootstrap\Nav;
use yii\helpers\Url;

/*echo Nav::widget([
	'items' => [
		[
			'label' => Yii::t('games','Sport') ,
			'url' => ['/games/game-sports/index'],
			'linkOptions' => [],
		],
		[
			'label' => Yii::t('games','Group') ,
			'url' => ['#'],
			'linkOptions' => [],
		],
		[
			'label' => Yii::t('games','Market') ,
			'url' => ['#'],
			'linkOptions' => [],
		],
		[
			'label' => Yii::t('games','Outcome') ,
			'url' => ['#'],
			'linkOptions' => [],
		],
		[
			'label' => Yii::t('games','Participant') ,
			'url' => ['#'],
			'linkOptions' => [],
		],
		[
			'label' => 'Dropdown',
			'items' => [
				['label' => 'Level 1 - Dropdown A', 'url' => '#'],
				'<li class="divider"></li>',
				'<li class="dropdown-header">Dropdown Header</li>',
				['label' => 'Level 1 - Dropdown B', 'url' => '#'],
			],
		],
	],
	'options' => ['class' => 'nav nav-pills nav-stacked box box-primary'],
]);*/
?>
<?php
$controller = Yii::$app->controller->id;
$action = Yii::$app->controller->action->id;
?>
<div class="box box-default">
	<ul class="nav nav-pills nav-stacked">
		<li role="presentation" <?= ($controller == 'game-sports-controller' || $action == 'index') ? 'class="active"' : "" ?>>
			<a href="<?= Url::toRoute('/games/game-sports/index') ?>"><?= Yii::t('games', 'Sport') ?></a>
		</li>
		<?php if(in_array($action,['view-sport-locale','markets-groups'])): ?>
			<!-- Markets Groups -->
			<li role="presentation" style="padding-left: 15px">
				<a href="<?= Url::toRoute(['/games/game-markets-groups/markets-groups','sport' => $sportId]) ?>"><?= Yii::t('games', 'Group') ?></a>
			</li>
		<?php endif; ?>
		<?php if(in_array($action,['view'])): ?>
			<!-- Markets -->
			<li role="presentation" style="padding-left: 15px">
				<a href="<?= Url::toRoute(['/games/game-markets-groups/markets-groups','sport' => $sportId]) ?>"><?= Yii::t('games','Market') ?>, ?></a>
			</li>
		<?php endif; ?>

		<li role="presentation"><a href="#"><?= Yii::t('games', 'Participant') ?></a></li>
	</ul>
</div>


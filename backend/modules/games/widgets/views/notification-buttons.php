<?php

/* @var $translations bool */
/* @var $this \yii\web\View */
/* @var $markets bool */

/* @var $participant_image bool */

use yii\helpers\Html;

if($translations){
	echo Html::a('<span class="fa fa-language"></span>', '/games/admin/verify-translate', ['class' => 'btn btn-danger blink', 'title' => Yii::t('games', 'Translations')]);
}

if($markets){
	echo Html::a('<span class="fa fa-unlink"></span>', '/games/game-sports/update-markets', ['class' => 'btn btn-danger blink', 'title' => Yii::t('games', 'Markets without a group')]);
}

if($participant_image){
	echo Html::a('<span class="fa fa-file-image-o"></span>', '/games/admin/images-approve', ['class' => 'btn btn-danger blink', 'title' => '']);
}

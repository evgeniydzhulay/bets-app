<?php

namespace backend\modules\games\widgets;

use yii\base\Widget;

class GameMenu extends Widget
{
	public $sportId = Null;

	public function init(){
		parent::init();
	}

	public function run(){
		return $this->render('game-menu',[
			'sportId' => $this->sportId,
		]);
	}

}
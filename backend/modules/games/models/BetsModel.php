<?php

namespace backend\modules\games\models;

use common\modules\games\models\BetsModel as BetsBaseModel;
use common\modules\games\models\BetsRevertedModel;

/**
 * @property BetsItemsModel[] $itemsWithOutcomes
 * @property string $userName
 */
class BetsModel extends BetsBaseModel
{

	/**
	 * @return string
	 */
	public function getUserName () {
		return !empty($this->userProfile->name) ? $this->userProfile->name . ' ' . $this->userProfile->surname : 'not set';
	}

}
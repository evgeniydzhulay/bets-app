<?php

namespace backend\modules\games\models;

use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventParticipantsModel;
use common\modules\games\models\EventTournamentLocaleModel;
use common\modules\games\models\EventTournamentModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use common\modules\games\models\GameOutcomesModel;
use common\modules\games\models\GameParticipantsLocaleModel;
use common\modules\games\models\GameParticipantsModel;
use common\modules\games\models\GameSportsLocaleModel;
use common\modules\games\models\GameSportsModel;
use Google\Cloud\Translate\TranslateClient;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\helpers\VarDumper;
use yii\web\Controller;

/* composer require google/cloud-translate */

class TranslatorModel extends \yii\base\Model
{

	private $countItems = 100000;
	private $countIterations = 3;
	private $key = 'AIzaSyDgzhIaSYGzin4dFupUJOGkXSFrHffaNak';
	private $projectId = 'mytestproduct-1562239866362';

	public function translate() {
		$data = $this->getData();

		if(!empty($data)){

			if(in_array($data['table'], ['outcomes'])){
				$method = $data['table'] . 'Locale';
				$data = $this->$method($data);
				die();
			}

			die();
			$translates = $this->getTranslate($data['titles'], $data['language']);
			unset($data['titles']);
			$this->saveTranslate($data, $translates);
		}
		die('Done!');
	}

	private function getTranslate(array $titles, $target) {
		try{
			$translate = new TranslateClient([
				'projectId' => $this->projectId,
				'key' => $this->key
			]);

			$translation = $translate->translateBatch($titles, [
				'target' => $target
			]);

		}catch(\Throwable $e){
			echo $e->getMessage();
			die();
		}

		return $translation;
	}

	private function getData() {
		$locales = [/*'sport-type', 'markets-group', 'markets', 'tournaments', 'games', 'participants',*/'outcomes'];
		foreach($locales as $locale){
			switch($locale){
				case 'sport-type' :
					$className = GameSportsLocaleModel::class;
					$relationAttr = 'sports_id';
					$title = 'title';
					break;
				case 'markets-group' :
					$className = GameMarketsGroupsLocaleModel::class;
					$relationAttr = 'group_id';
					$title = 'title';
					break;
				case 'markets' :
					$className = GameMarketsLocaleModel::class;
					$relationAttr = 'market_id';
					$title = 'title';
					break;
				case 'tournaments' :
					$className = EventTournamentLocaleModel::class;
					$relationAttr = 'tournament_id';
					$title = 'title';
					break;
				case 'games' :
					$className = EventGameLocaleModel::class;
					$relationAttr = 'game_id';
					$title = 'title';
					break;
				case 'outcomes' :
					$className = GameOutcomesLocaleModel::class;
					$relationAttr = 'outcome_id';
					$title = 'title';
					break;
				case 'participants' :
					$className = GameParticipantsLocaleModel::class;
					$relationAttr = 'participant_id';
					$title = 'name';
					break;
			}

			foreach(\Yii::$app->params['languages'] AS $k => $v){
				if($k == 'en'){
					continue;
				}
				$data = $className::find()->select([
					$className::tableName() . '.' . $relationAttr,
					$className::tableName() . '.' . $title,
					'lang_code' => new Expression("'{$k}'"),
				])->leftJoin(['translates' => $className::tableName()], [
					$className::tableName() . '.' . $relationAttr => new Expression('translates.' . $relationAttr),
					'translates.lang_code' => $k
				])->andWhere([
					$className::tableName() . '.lang_code' => 'en',
					'translates.' . $title => NULL,
				])->limit($this->countItems)->asArray()->all();
				if($data != []){
					$titles = ArrayHelper::getColumn($data, $title);
					$data = ArrayHelper::map($data, $relationAttr, $title);
					return (['table' => $locale, 'language' => $k, 'titles' => $titles, 'data' => $data]);
				}
			}
		}
		return false;
	}

	private function saveTranslate($data, $translates) {
		switch($data['table']){
			case 'sport-type' :
				$this->sportLocales($data, $translates);
				break;
			case 'markets-group' :
				$this->marketsGroupLocales($data, $translates);
				break;
			case 'markets' :
				$this->marketsLocales($data, $translates);
				break;
			case 'tournaments' :
				$this->tournamentsLocales($data, $translates);
				break;
			case 'games' :
				$this->gamesLocales($data, $translates);
				break;
			case 'outcomes' :
				$this->outcomesLocale($data, $translates);
				break;
			case 'participants' :
				$this->participantsLocales($data, $translates);
				break;
		}
	}

	private function sportLocales($data, $translates) {
		if(!empty($translates)){
			foreach($translates as $translate){
				if(!empty($id = array_search($translate['input'], $data['data']))){
					$parent = GameSportsModel::findOne($id);
					if($parent != null){
						$model = new GameSportsLocaleModel();
						$model->scenario = GameSportsLocaleModel::SCENARIO_CREATE;
						$model->sports_id = $parent->id;
						$model->lang_code = $data['language'];
						$model->is_active = 1;
						$model->title = mb_convert_case($translate['text'], MB_CASE_TITLE);
						$model->save();
						unset($data['data'][$id]);
					}
				}
			}
		}
	}

	private function marketsGroupLocales($data, $translates) {
		if(!empty($translates)){
			foreach($translates as $translate){
				if(!empty($id = array_search($translate['input'], $data['data']))){
					$parent = GameMarketsGroupsModel::findOne($id);
					if($parent != null){
						$model = new GameMarketsGroupsLocaleModel();
						$model->scenario = GameMarketsGroupsLocaleModel::SCENARIO_CREATE;
						$model->sports_id = $parent->sports_id;
						$model->group_id = $parent->id;
						$model->lang_code = $data['language'];
						$model->is_active = 1;
						$model->title = mb_convert_case($translate['text'], MB_CASE_TITLE);
						$model->save();
						unset($data['data'][$id]);
					}
				}
			}
		}
	}

	private function marketsLocales($data, $translates) {
		if(!empty($translates)){
			foreach($translates as $translate){
				if(!empty($id = array_search($translate['input'], $data['data']))){
					$parent = GameMarketsModel::findOne($id);
					if($parent != null){
						$model = new GameMarketsLocaleModel();
						$model->scenario = GameMarketsLocaleModel::SCENARIO_CREATE;
						$model->sports_id = $parent->sports_id;
						$model->group_id = $parent->group_id;
						$model->market_id = $parent->id;
						$model->lang_code = $data['language'];
						$model->is_active = 1;
						$model->title = mb_convert_case($translate['text'], MB_CASE_TITLE);
						$model->save();
						unset($data['data'][$id]);
					}
				}
			}
		}
	}

	private function outcomesLocale($data, $translates = null) {
		// from google translate
		if(is_array($translates)){
			foreach($translates as $translate){
				if(!empty($id = array_search($translate['input'], $data['data']))){
					$parent = GameOutcomesModel::findOne($id);
					if($parent != null){
						$model = new GameOutcomesLocaleModel();
						$model->scenario = GameOutcomesLocaleModel::SCENARIO_CREATE;
						$model->sports_id = $parent->sports_id;
						$model->group_id = $parent->group_id;
						$model->market_id = $parent->market_id;
						$model->outcome_id = $parent->id;
						$model->lang_code = $data['language'];
						$model->is_active = 1;
						$model->title = mb_convert_case($translate['text'], MB_CASE_TITLE);
						$model->save();
						unset($data['data'][$id]);
					}
				}
			}
			// for dictionary
		}else{
			$templates = include \Yii::getAlias('@backend') . '/modules/games/translations/_' . $data['language'] . '/outcomes.php';
			$pattern = array_keys($templates);
			$replacement = array_values($templates);
			$i = 1;
			if(!empty($data['data'])){
				foreach($data['data'] as $id => $title){
					$string = preg_replace($pattern,$replacement,$title, -1, $count);
					if($count == 1){
						$parent = GameOutcomesModel::findOne($id);
						if($parent != null){
							$model = new GameOutcomesLocaleModel();
							$model->scenario = GameOutcomesLocaleModel::SCENARIO_CREATE;
							$model->sports_id = $parent->sports_id;
							$model->group_id = $parent->group_id;
							$model->market_id = $parent->market_id;
							$model->outcome_id = $parent->id;
							$model->lang_code = $data['language'];
							$model->is_active = 1;
							$model->title = trim($string);
							$model->save();
							unset($data['data'][$id]);
							echo $i++ . ' ' . $string . PHP_EOL;
						}
					}
				}
				echo '<pre>' . count($data['data']) . '</pre>';
				echo '<pre>' . print_r($data['data'],true) . '</pre>';
				die();
			}
		}
	}

	private function tournamentsLocales($data, $translates) {
		if(!empty($translates)){
			foreach($translates as $translate){
				if(!empty($id = array_search($translate['input'], $data['data']))){
					$parent = EventTournamentModel::findOne($id);
					if($parent != null){
						$model = new EventTournamentLocaleModel();
						$model->scenario = EventTournamentLocaleModel::SCENARIO_CREATE;
						$model->tournament_id = $parent->id;
						$model->sports_id = $parent->sports_id;
						$model->lang_code = $data['language'];
						$model->is_active = 1;
						$model->title = mb_convert_case($translate['text'], MB_CASE_TITLE);
						$model->save();
						unset($data['data'][$id]);
					}
				}
			}
		}
	}

	private function gamesLocales($data, $translates) {
		if(!empty($translates)){
			foreach($translates as $translate){
				if(!empty($id = array_search($translate['input'], $data['data']))){
					$parent = EventGameModel::findOne($id);
					if($parent != null){
						$model = new EventGameLocaleModel();
						$model->scenario = EventGameLocaleModel::SCENARIO_CREATE;
						$model->tournament_id = $parent->tournament_id;
						$model->game_id = $parent->id;
						$model->sports_id = $parent->sports_id;
						$model->lang_code = $data['language'];
						$model->is_active = 1;
						$model->title = mb_convert_case($translate['text'], MB_CASE_TITLE);
						$model->save();
						unset($data['data'][$id]);
					}
				}
			}
		}
	}


	private function participantsLocales($data, $translates) {
		if(!empty($translates)){
			foreach($translates as $translate){
				if(!empty($id = array_search($translate['input'], $data['data']))){
					$parent = GameParticipantsModel::findOne($id);
					if($parent != null){
						$model = new GameParticipantsLocaleModel();
						$model->scenario = GameParticipantsLocaleModel::SCENARIO_CREATE;
						$model->sports_id = $parent->sports_id;
						$model->participant_id = $parent->id;
						$model->lang_code = $data['language'];
						$model->is_active = 1;
						$model->name = mb_convert_case($translate['text'], MB_CASE_TITLE);
						$model->save();
						unset($data['data'][$id]);
					}
				}
			}
		}
	}

}

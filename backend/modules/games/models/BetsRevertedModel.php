<?php

namespace backend\modules\games\models;

use common\modules\games\models\BetsModel;
use common\modules\games\models\BetsRevertedModel as BetsRevertedBaseModel;
use common\modules\user\models\ProfileModel;
use common\modules\user\models\UserModel;
use yii\twig\Profile;

class BetsRevertedModel extends BetsRevertedBaseModel
{

	/**
	 * @return array
	 */
	public static function getAdminsList() {
		return ProfileModel::find()
			->select('surname')
			->leftJoin('{{user_rbac_assignment}} AS rbac ON rbac.user_id = user_profile.user_id')
			->where(['rbac.item_name' => 'admin'])
			->indexBy('id')->column();
	}

	public static function saveReport(BetsModel $bet, $message){
		$model = new self();
		$model->scenario = self::SCENARIO_CREATE;
		$model->bet_id = $bet->id;
		$model->user_id = $bet->user_id;
		$model->comment = $message;
		if($model->save()){
			return true;
		}
		return false;
	}

}
<?php

namespace backend\modules\games\models;

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\ApiGameMarketsModel;
use common\modules\games\models\BetsItemsModel;
use common\modules\games\models\CartsItemsModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\GameMarketsGroupsAdditionalModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsModel as GameMarketsBaseModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use common\modules\games\models\GameSportsLocaleModel;
use yii\helpers\ArrayHelper;

class GameMarketsModel extends GameMarketsBaseModel
{

	public function setGroup(int $group_id) {
		if($this->group_id == null && $this->updateAttributes(['group_id' => $group_id])){
			/* update locale */
			foreach($this->gameMarketsLocales as $locale){
				$locale->updateAttributes(['group_id' => $group_id]);
			}
			/* update outcomes */
			foreach($this->gameOutcomes as $outcome){
				if($outcome->updateAttributes(['group_id' => $group_id])){
					/* update outcome locale */
					foreach($outcome->gameOutcomesLocales as $locale){
						$locale->updateAttributes(['group_id' => $group_id]);
					}
				}
			}
			return true;
		}
		return false;
	}

	/* get Markets without Group  */
	public static function getDataForUpdateMarkets() {
		$data = [];
		$models = self::find()->where("group_id IS NULL")->with(['gameMarketsLocales' => function ($query) {
			$query->indexBy('lang_code');
		}])->all();
		if(!empty($models)){
			foreach($models as $model){
				/* set group list and sport title*/
				if(empty($data[$model->sports_id]['group_list'])){
					$data[$model->sports_id]['group_list'] = ArrayHelper::map(GameMarketsGroupsLocaleModel::find()->where(['lang_code' => 'en', 'sports_id' => $model->sports_id])->orderBy('title')->all(), 'group_id', 'title');
					$sport = GameSportsLocaleModel::find()->select('title')->where(['lang_code' => 'en', 'sports_id' => $model->sports_id])->one();
					$data[$model->sports_id]['sport_title'] = ($sport != null) ? $sport->title : 'Not Set';
				}
				/* set [market id] => [title] */
				$data[$model->sports_id]['items'][$model->id] = !empty($model->gameMarketsLocales['en']->title) ? $model->gameMarketsLocales['en']->title : 'Not Set';
			}
		}
		return $data;
	}

	public function mergerMarkets($selectedMarkets) {
		if(is_array($selectedMarkets)){
			foreach($selectedMarkets as $market_id){
				$market = GameMarketsModel::find()->where(['id' => $market_id])->with(['gameOutcomes.gameOutcomesLocales' => function ($q) {
					$q->indexBy('lang_code');
				}])->one();
				if($market != Null){

					$duplicates = \Yii::$app->db->createCommand(
						"SELECT game_outcomes.id AS outcome_id, go_2.id AS in_outcome_id  FROM game_outcomes JOIN game_outcomes AS go_2 ON go_2.condition = game_outcomes.condition WHERE game_outcomes.market_id = {$this->id} AND go_2.market_id = {$market_id}
							UNION
						SELECT game_outcomes_locale.outcome_id, gl.outcome_id AS in_market_id  FROM game_outcomes_locale JOIN game_outcomes_locale AS gl ON gl.title = game_outcomes_locale.title WHERE game_outcomes_locale.lang_code = 'en' AND gl.lang_code = 'en' AND game_outcomes_locale.market_id = {$this->id} AND gl.market_id = {$market_id};"
					)->queryAll();

					if(!empty($duplicates)){
						\Yii::$app->session->addFlash('danger', 'Слияние маркетов не произошло. В одном из объединяемых исходах есть совпадения в названии или условии.');
						return false;
					}

					$transaction = \Yii::$app->db->beginTransaction();
					try{
						GameOutcomesModel::updateAll(['market_id' => $this->id], ['market_id' => $market_id]);
						GameOutcomesLocaleModel::updateAll(['market_id' => $this->id], ['market_id' => $market_id]);
						EventOutcomesModel::updateAll(['market_id' => $this->id], ['market_id' => $market_id]);
						ApiGameMarketsModel::updateAll(['market_id' => $this->id], ['market_id' => $market_id]);
						BetsItemsModel::updateAll(['market_id' => $this->id], ['market_id' => $market_id]);
						CartsItemsModel::updateAll(['market_id' => $this->id], ['market_id' => $market_id]);
						$transaction->commit();

						GameMarketsGroupsAdditionalModel::deleteAll(['market_id' => $market_id]);
						$market->delete();
					}catch(\Throwable $e){
						$transaction->rollBack();
						return false;
					}
				}else{
					return false;
				}
			}
			return true;
		}
		return false;
	}

	private function searchMatchesInMarket($outcome) {

		######### search by condition ########

		if(!empty($outcome->condition)){
			$searchOutcome = GameOutcomesModel::find()->where(['market_id' => $this->id, 'condition' => $outcome->condition])->one();
			if($searchOutcome !== null){
				return $searchOutcome->id;
			}
		}

		######## search by title ##########

		if(!empty($outcome->gameOutcomesLocales['en']->title)){
			$searchOutcome = GameOutcomesLocaleModel::find()->where([
				'lang_code' => 'en',
				'market_id' => $this->id,
				'title' => $outcome->gameOutcomesLocales['en']->title
			])->one();
			if($searchOutcome !== null){
				return $searchOutcome->id;
			}
		}

		########## no matches ##########

		return false;
	}

}
<?php

namespace backend\modules\games\models;

use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesModel;
use yii\base\Model;
use yii\db\ActiveQuery;
use yii\debug\models\search\Event;

class EventOutcomesHandler extends Model
{
	public $game_id;
	public $group_id;
	public $market_id;
	public $sport_id;

	public function formName() {
		return '';
	}

	public function rules() {
		return [
			[['game_id', 'group_id', 'market_id', 'sport_id'], 'safe'],
		];
	}

	public function search($params) {
		$this->load($params, '');

		if(!$this->game_id || !$this->group_id){
			return null;
		}

		$data = GameMarketsGroupsModel::find();
		$data->joinWith(['gameMarketsGroupsLocales' => function ($query) {
			return $query->indexBy('lang_code');
		}]);


		/* filters */
		$data->andFilterWhere([
			GameMarketsGroupsModel::tableName() . '.sports_id' => $this->sport_id,
			GameMarketsGroupsModel::tableName() . '.is_enabled' => GameMarketsGroupsModel::STATUS_ENABLED
		]);
		if($this->group_id != 'all'){
			$data->andFilterWhere([GameMarketsGroupsModel::tableName() . '.id' => $this->group_id]);
		}
		/*if(!$this->market_id){
			$data->joinWith([
				'gameMarkets.gameMarketsLocales' => function ($query) {
					return $query->indexBy('lang_code');
				},
				'gameMarkets.gameOutcomes.gameOutcomesLocales' => function ($query) {
					return $query->indexBy('lang_code');
				}
			]);
		}else{
			$data->joinWith(['gameMarkets' => function ($query) {
				return $query
					->where([GameMarketsModel::tableName() . '.id' => $this->market_id])
					->with(['gameMarketsLocales' => function ($query) {
						return $query->indexBy('lang_code');
					},
						'gameOutcomes.gameOutcomesLocales' => function ($query) {
							return $query->indexBy('lang_code');
						}
					]);
			}]);
		}*/

		$data->joinWith(['gameMarkets' => function ($query) {
			return $query
				->andFilterWhere([GameMarketsModel::tableName() . '.id' => $this->market_id]) // if isset
				->with(['gameMarketsLocales' => function ($query) {
					return $query->indexBy('lang_code');
				},
					'gameOutcomes' => function ($query) {
					/** @var $query ActiveQuery */
						$query->leftJoin(EventOutcomesModel::tableName(),'event_outcomes.outcome_id = game_outcomes.id');
						$query->andWhere('event_outcomes.rate IS NOT NULL');
						$query->andWhere(['event_outcomes.game_id' => $this->game_id]);
						$query->joinWith(['gameOutcomesLocales' => function ($query) {
							return $query->indexBy('lang_code');
						}]);
					}
				]);
		}]);

		$data = $data->all();
		return $data;
	}


	/**
	 * @param $gameModel \common\modules\games\models\EventGameModel
	 * @param $rates array
	 * @param $statuses array
	 * @param $finished array
	 * @param $hidden array
	 * @return bool
	 */
	public function saveData($gameModel, $rates, $statuses, $finished, $hidden) {
		$gameOutcomes = EventOutcomesModel::find()->select(['is_enabled', 'rate', 'outcome_id', 'is_finished', 'is_banned', 'is_hidden'])->where(['game_id' => $gameModel->id])->asArray()->indexBy('outcome_id')->all();
		foreach($rates as $outcome_id => $rate) {
			/* add new outcome */
			if(!empty($rate) && !key_exists($outcome_id, $gameOutcomes)){
				$this->createEventOutcome($outcome_id, $rate, $gameModel, $statuses);
				/* update outcome */
			} elseif(key_exists($outcome_id, $gameOutcomes)) {
				$rate = (float)str_replace(',', '.', $rate);
				$status = (key_exists($outcome_id,$statuses)) ? EventOutcomesModel::STATUS_ENABLED : EventOutcomesModel::STATUS_DISABLED;
				$finish = (key_exists($outcome_id,$finished)) ? EventOutcomesModel::STATUS_ENABLED : EventOutcomesModel::STATUS_DISABLED;
				$hide = (key_exists($outcome_id,$hidden)) ? EventOutcomesModel::STATUS_ENABLED : EventOutcomesModel::STATUS_DISABLED;
				/* change rate,status,finished */
				if($rate != $gameOutcomes[$outcome_id]['rate'] // if changed rate
					|| $status != $gameOutcomes[$outcome_id]['is_enabled'] // if changed status
					|| $finish != $gameOutcomes[$outcome_id]['is_finished'] // if set status finished
					|| $hide != $gameOutcomes[$outcome_id]['is_hidden'] // if set status hidden
				) {
					$this->updateEventOutcome($outcome_id, $rate, $gameModel, $statuses, $finished, $hidden);
				}
			}
		}
		return true;
	}

	/**
	 * @param $outcome_id int
	 * @param $rate string
	 * @param $eventGameModel \common\modules\games\models\EventGameModel
	 * @param $statuses array
	 * @return bool
	 */
	private function createEventOutcome($outcome_id, $rate, $eventGameModel, $statuses) {
		$gameOutcomeModel = GameOutcomesModel::findOne((int)$outcome_id);
		if($gameOutcomeModel){
			$model = new EventOutcomesModel();
			$model->sports_id = $eventGameModel->sports_id;
			$model->tournament_id = $eventGameModel->tournament_id;
			$model->game_id = $eventGameModel->id;
			$model->market_id = $gameOutcomeModel->market_id;
			$model->group_id = $gameOutcomeModel->group_id;
			$model->is_enabled = (key_exists($outcome_id, $statuses)) ? EventOutcomesModel::STATUS_ENABLED : EventOutcomesModel::STATUS_DISABLED;
			$model->outcome_id = $outcome_id;
			$model->rate = (float)str_replace(',', '.', $rate);
			return $model->save();
		}
		return false;
	}

	private function updateEventOutcome($outcome_id, $rate, $eventGameModel, $statuses, $finished,$hidden) {
		$eventOutcomeModel = EventOutcomesModel::find()->where(['game_id' => $eventGameModel->id, 'outcome_id' => $outcome_id])->one();
		if($eventOutcomeModel !== null) {
			if(!empty($rate)) {
				$eventOutcomeModel->setRate($rate);
				$eventOutcomeModel->is_enabled = (key_exists($outcome_id, $statuses)) ? EventOutcomesModel::STATUS_ENABLED : EventOutcomesModel::STATUS_DISABLED;
				$eventOutcomeModel->is_hidden = (key_exists($outcome_id, $hidden)) ? EventOutcomesModel::STATUS_ENABLED : EventOutcomesModel::STATUS_DISABLED;
				if($eventOutcomeModel->is_finished != EventOutcomesModel::STATUS_ENABLED && key_exists($outcome_id, $finished)){
					$eventOutcomeModel->is_finished = EventOutcomesModel::STATUS_ENABLED;
				}
			} else {
				$eventOutcomeModel->is_enabled = EventOutcomesModel::STATUS_DISABLED;
			}
			return $eventOutcomeModel->save();
		}
		return false;
	}
}

<?php

namespace backend\modules\games\models;

use common\modules\games\models\Api1xbetTemplateMarketsModel;
use common\modules\games\models\Api1xbetTemplateOutcomesModel;
use common\modules\games\models\ApiGameMarketsModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use common\modules\games\models\GameOutcomesModel;
use yii\helpers\Json;

/* DELETE FROM `game_outcomes_locale` WHERE `lang_code` = 'ru' AND (`group_id` = 162 OR `group_id` = 165);
 DELETE FROM `game_markets_locale` WHERE `lang_code` = 'ru' AND (`group_id` = 162 OR `group_id` = 165) */

class Translator_1X_Model extends \yii\base\Model
{
	//const SELECTED_GROUP_IDX = ['344', '345']; // https://bettestingi.com/
	const SELECTED_GROUP_IDX = ['162', '165']; // https://luxe.bet/

	private $patternRegExp = [];
	private $replacementRegExp = [];
	private $languageTemplates = [];
	private $selectedLanguage = '';
	private $path;


	public function init() {
		$this->patternRegExp = array_keys($this->getOutcomesRegExpTemplates());
		$this->replacementRegExp = array_values($this->getOutcomesRegExpTemplates());
		$this->path = \Yii::getAlias('@backend') . '/modules/games/translations/1x';
		$this->findFilesWithTemplates();
		parent::init();
	}

	public function translate() {
		if($this->languageTemplates == []){
			die('Files Not Found');
		}
		foreach($this->languageTemplates as $language){
			$this->selectedLanguage = $language;
			$this->translateOutcomes();
			$this->translateMarkets();
		}

	}

	private function findFilesWithTemplates() {
		$dir = scandir($this->path);
		if($dir){
			foreach($dir as $file){
				if(in_array($file, ['.', '..']) || strlen($file) !== 2){
					continue;
				}
				$this->languageTemplates[] = $file;
			}
		}
	}

	/* Outcomes */

	private function translateOutcomes() {
		$outcomes = $this->searchUntranslatedOutcomes();
		$languageTemplates = $this->getDictionaryFromFile('outcomes');
		if(!empty($outcomes) && !empty($languageTemplates)){
			$dictionaryTitles = $this->getDictionaryOutcomes($languageTemplates);
			foreach($outcomes as $outcome){
				$this->translateOutcomeAndSave($outcome, $dictionaryTitles);
			}
		}
	}

	/* перевод с подстановкой шаблона */

	private function translateOutcomeAndSave($outcome, $dictionaryTitles) {
		$condition = Json::decode($outcome['condition']);
		if(empty($condition['name'])){
			return false;
		}

		$title = (key_exists($condition['name'], $dictionaryTitles[$outcome['external_market_id']])) ? $dictionaryTitles[$outcome['external_market_id']][$condition['name']] : false;
		$handicap = (isset($condition['handicap'])) ? $condition['handicap'] : false;
		if($title === false){
			return false;
		}

		$title = strip_tags($title);

		/* Добавить условие - handicap */
		if($count = mb_substr_count($title, '()')){
			if($handicap === false || $count != 1){
				return false;
			}
			$title = str_replace('()', '(' . $handicap . ')', $title);
		}

		if(in_array($this->selectedLanguage, ['ru','uk'])){

			$title = mb_convert_case($title, MB_CASE_TITLE);

			/* Изменить шаблон для русского языка  */
			if(preg_match('/\^(\d+)\^/', $condition['name']) == 1){
				$title = preg_replace($this->patternRegExp, $this->replacementRegExp, $title, -1, $count);
			}

			$title = str_replace([' Нп1', ' Нп2', ' Нн', ' С ', ' И ', ' В ', '-Й', '-М', '-Го',' Тб ',' Тм '], [' НП1', ' НП2', ' НН', ' с ', ' и ', ' в ', '-й', '-м', '-го',' ТБ ',' ТМ '], $title);
		}else{
			$title = ucfirst($title);
		}


		/* save locale */
		$row = new GameOutcomesLocaleModel(['scenario' => GameOutcomesLocaleModel::SCENARIO_CREATE]);
		$row->sports_id = $outcome['sports_id'];
		$row->group_id = $outcome['group_id'];
		$row->market_id = $outcome['market_id'];
		$row->outcome_id = $outcome['id'];
		$row->lang_code = $this->selectedLanguage;
		$row->is_active = 1;
		$row->title = $title;
		if($row->save()){
			echo $row->id . ' - ' . $title . PHP_EOL;
		}else{
			print_r($row->errors);
		}
	}


	private function searchUntranslatedOutcomes() {
		$localeTab = GameOutcomesLocaleModel::tableName();
		$outcomeTab = GameOutcomesModel::tableName();
		$apiMarkets = ApiGameMarketsModel::tableName();
		$outcomes = GameOutcomesModel::find()->select(["{$outcomeTab}.*", "{$apiMarkets}.code_api AS external_market_id"])
			->leftJoin($localeTab, "{$localeTab}.outcome_id = {$outcomeTab}.id AND {$localeTab}.lang_code = '" . $this->selectedLanguage . "'")
			->leftJoin($apiMarkets, "{$apiMarkets}.market_id = {$outcomeTab}.market_id AND {$apiMarkets}.service_api = 2")
			->where(["{$outcomeTab}.group_id" => self::SELECTED_GROUP_IDX])->andWhere("{$localeTab}.id IS NULL")
			->asArray()->all();
		return $outcomes;
	}


	private function getDictionaryOutcomes($outerOutcomes) {
		$dictionary = [];
		$templates = Api1xbetTemplateOutcomesModel::find()->asArray()->all();
		if(!empty($templates)){
			foreach($templates as $en){
				if(empty($outerOutcomes[$en['outcome_id']])){
					continue;
				}
				$dictionary[$en['market_id']][$en['name']] = $outerOutcomes[$en['outcome_id']];
			}
		}
		return $dictionary;
	}

	private function getDictionaryFromFile($type) {
		$array = [];
		$json = \json_decode(file_get_contents($this->path . DIRECTORY_SEPARATOR . $this->selectedLanguage));
		if(json_last_error() !== 0){
			return false;
		}

		if($type == 'markets'){
			foreach($json as $id => $item){
				$array[$id] = $item->G;
			}
		}

		if($type == 'outcomes'){
			foreach($json as $m_id => $outcomes){
				foreach($outcomes->B as $o_id => $outcome){
					$array[$o_id] = $this->outcomeFilter($outcome->N);
				}
			}
		}
		return $array;
	}

	private function outcomeFilter($str) {
		return str_replace(['|Не используется| ','|Не  використовується| '],['',''], $str);
	}

	private function getOutcomesRegExpTemplates() {
		return [
			'/^Индивидуальный Тотал (1|2) Больше \(([0-9\.]+)\)$/' => '${2} Б',
			'/^Индивидуальный Тотал (1|2) Меньше \(([0-9\.]+)\)$/' => '${2} М'

			//'/Команда (\d{1,1})/' => '{item${1}}',
			//'/Первая Команда/' => '{item1}',
			//'/Вторая Команда/' => '{item2}',
			//'/Второй Победит/' => '{item2} Победит',
			//'/Первый Победит/' => '{item1} Победит',
			//'/Второй Не Проиграет/' => '{item2} Не Проиграет',
			//'/Первый Не Проиграет/' => '{item1} Не Проиграет',
			//'/Второй Сухая/' => '{item2} Сухая',
			//'/Первый Сухая/' => '{item1} Сухая',
			//'/^П(\d+)$/' => '{item${1}}'
			//'/^П(\d+)$/' => '{item${1}}',
			//'/(П|п)(\d{1,1})/' => '{item${2}}',
			//'/\s(\d{1,1})\s/' => ' {item${1}} ',
		];
	}

	/* # Markets # */

	private function translateMarkets() {
		$markets = $this->searchUntranslatedMarkets();
		$languageTemplates = $this->getDictionaryFromFile('markets');

		if(!empty($markets) && !empty($languageTemplates)){
			$dictionaryTitles = $this->getDictionaryMarkets($languageTemplates);
			foreach($markets as $market){
				$this->translateMarketAndSave($market, $dictionaryTitles);
			}
		}
	}

	private function searchUntranslatedMarkets() {
		$localeTab = GameMarketsLocaleModel::tableName();
		$marketsTab = GameMarketsModel::tableName();
		$apiMarkets = ApiGameMarketsModel::tableName();
		return GameMarketsModel::find()
			->select([$marketsTab . '.*', 'en.title', "{$apiMarkets}.code_api AS external_market_id"])
			->leftJoin("{$localeTab}", "{$localeTab}.market_id = {$marketsTab}.id AND {$localeTab}.lang_code = '" . $this->selectedLanguage . "'")
			->leftJoin("{$localeTab} as en", "en.market_id = {$marketsTab}.id AND en.lang_code = 'en'")
			->leftJoin($apiMarkets, "{$apiMarkets}.market_id = {$marketsTab}.id AND {$apiMarkets}.service_api = 2")
			->where(["{$marketsTab}.group_id" => self::SELECTED_GROUP_IDX])->andWhere("{$localeTab}.id IS NULL")
			->asArray()->all();
	}

	private function getDictionaryMarkets($outerMarkets) {
		$dictionary = [];
		$templates = Api1xbetTemplateMarketsModel::find()->asArray()->all();
		if(!empty($templates)){
			foreach($templates as $en){
				if(empty($outerMarkets[$en['market_id']])){
					continue;
				}
				$dictionary[$en['market_id']][$en['name']] = $outerMarkets[$en['market_id']];
			}
		}
		return $dictionary;
	}

	private function translateMarketAndSave($market, $dictionaryTitles) {

		$title = key_exists($market['title'], $dictionaryTitles[$market['external_market_id']]) ? $dictionaryTitles[$market['external_market_id']][$market['title']] : false;
		if($title){
			$title = strip_tags($title);
			if(in_array($this->selectedLanguage, ['ru','uk'])){
				$title = mb_convert_case($title, MB_CASE_TITLE);
				$title = str_replace([' С ', ' И ', ' В ', '-Го'], [' с ', ' и ', ' в ', '-го'], $title);
			}else{
				$title = ucfirst($title);
			}

			$row = new GameMarketsLocaleModel(['scenario' => GameMarketsLocaleModel::SCENARIO_CREATE]);
			$row->sports_id = $market['sports_id'];
			$row->market_id = $market['id'];
			$row->group_id = $market['group_id'];
			$row->lang_code = $this->selectedLanguage;
			$row->is_active = 1;
			$row->title = strip_tags($title);
			if($row->save()){
				echo $row->id . ' - ' . $title . PHP_EOL;
			}else{
				print_r($row->errors);
			}
		}

	}

}

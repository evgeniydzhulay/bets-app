<?php

namespace backend\modules\games\models;

use common\modules\games\models\BetsItemsModel;
use common\modules\games\models\BetsModel;
use common\modules\games\models\EventOutcomesModel;
use yii\base\Model;


class SetOutcomeStatus extends Model
{
	public $gameId;
	public $marketId;
	public $selected_outcomes = [];

	public function formName() {
		return '';
	}

	public function rules() {
		return [
			[['gameId', 'marketId', 'selected_outcomes'], 'safe']
		];
	}

	public function setStatusManually() {
		$outcomes = EventOutcomesModel::findAll(['market_id' => $this->marketId, 'game_id' => $this->gameId]);
		foreach($outcomes as $outcome){
			$status = (key_exists($outcome->id, $this->selected_outcomes) && !empty($this->selected_outcomes[$outcome->id])) ? $this->selected_outcomes[$outcome->id] : BetsItemsModel::STATUS_DEFEAT;
			$outcome->setStatus($status);
		}
	}

	/*public function setResultByApi($id, $status) {
		$outcome = EventOutcomesModel::findOne((int)$id);
		if($outcome != null){
			$outcome->setStatus($status);
		}
	}*/

}
<?php

namespace backend\modules\games\models;

use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventParticipantsModel;
use common\modules\games\models\EventTournamentLocaleModel;
use common\modules\games\models\EventTournamentModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use common\modules\games\models\GameOutcomesModel;
use common\modules\games\models\GameParticipantsLocaleModel;
use common\modules\games\models\GameParticipantsModel;
use common\modules\games\models\GameSportsLocaleModel;
use common\modules\games\models\GameSportsModel;
use Google\Cloud\Translate\TranslateClient;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\helpers\FileHelper;
use yii\helpers\VarDumper;
use yii\web\Controller;


class Translator_365_Model extends \yii\base\Model
{

	const COUNT_ITEMS    = 1000000;
	const EXCLUDE_GROUPS = '162,165'; // https://luxe.bet/
	//const EXCLUDE_GROUPS = '344,345'; // https://bettestingi.com/
	const LANGUAGES      = ['uk', 'ru'];

	public function translateOutcomes() {
		foreach(self::LANGUAGES as $lang_code){
			$data = $this->getData($lang_code);
			if(empty($data['titles'])){
				continue;
			}
			if(in_array($data['table'], ['outcomes'])){
				$method = $data['table'] . 'Locale';
				$this->$method($data);
			}
		}
		die('Done!');
	}


	private function getData($lang_code) {
		$locales = ['outcomes'];
		foreach($locales as $locale){
			switch($locale){
				case 'sport-type' :
					$className = GameSportsLocaleModel::class;
					$relationAttr = 'sports_id';
					$title = 'title';
					break;
				case 'markets-group' :
					$className = GameMarketsGroupsLocaleModel::class;
					$relationAttr = 'group_id';
					$title = 'title';
					break;
				case 'markets' :
					$className = GameMarketsLocaleModel::class;
					$relationAttr = 'market_id';
					$title = 'title';
					break;
				case 'tournaments' :
					$className = EventTournamentLocaleModel::class;
					$relationAttr = 'tournament_id';
					$title = 'title';
					break;
				case 'games' :
					$className = EventGameLocaleModel::class;
					$relationAttr = 'game_id';
					$title = 'title';
					break;
				case 'outcomes' :
					$className = GameOutcomesLocaleModel::class;
					$relationAttr = 'outcome_id';
					$title = 'title';
					break;
				case 'participants' :
					$className = GameParticipantsLocaleModel::class;
					$relationAttr = 'participant_id';
					$title = 'name';
					break;
			}

			$data = $className::find()->select([
				$className::tableName() . '.' . $relationAttr,
				$className::tableName() . '.' . $title,
				$className::tableName() . '.group_id',
				'lang_code' => new Expression("'{$lang_code}'"),
			])->leftJoin(['translates' => $className::tableName()], [
				$className::tableName() . '.' . $relationAttr => new Expression('translates.' . $relationAttr),
				'translates.lang_code' => $lang_code
			])->andWhere([
				$className::tableName() . '.lang_code' => 'en',
				'translates.' . $title => NULL,
			])->andWhere(
				$className::tableName() . '.group_id NOT IN (' . self::EXCLUDE_GROUPS . ')'
			)->limit(self::COUNT_ITEMS)->asArray()->all();
			if($data != []){
				$titles = ArrayHelper::getColumn($data, $title);
				$data = ArrayHelper::map($data, $relationAttr, $title);
				return (['table' => $locale, 'language' => $lang_code, 'titles' => $titles, 'data' => $data]);
			}
		}
		return false;
	}


	private function outcomesLocale($data) {
		$templates = include \Yii::getAlias('@backend') . '/modules/games/translations/_' . $data['language'] . '/outcomes.php';
		$pattern = array_keys($templates);
		$replacement = array_values($templates);
		$i = 1;
		if(!empty($data['data'])){
			foreach($data['data'] as $id => $title){
				$string = preg_replace($pattern, $replacement, $title, -1, $count);
				if($count == 1){
					$parent = GameOutcomesModel::findOne($id);
					if($parent != null){
						$model = new GameOutcomesLocaleModel();
						$model->scenario = GameOutcomesLocaleModel::SCENARIO_CREATE;
						$model->sports_id = $parent->sports_id;
						$model->group_id = $parent->group_id;
						$model->market_id = $parent->market_id;
						$model->outcome_id = $parent->id;
						$model->lang_code = $data['language'];
						$model->is_active = 1;
						$model->title = trim($string);
						$model->save();
						unset($data['data'][$id]);
						echo $i++ . ' ' . $string . PHP_EOL;
					}
				}
			}
			echo '<pre>' . count($data['data']) . '</pre>';
			echo '<pre>' . print_r($data['data'], true) . '</pre>';
			echo '################# Lang Code: ' . $data['language'] . ' - Done! #################';
		}
	}

	/* ### Markets ### */

	public function translateMarkets() {
		foreach(self::LANGUAGES as $lang_code){
			$pathToFile = \Yii::getAlias('@backend') . '/modules/games/translations/_' . $lang_code . '/markets.php';
			$markets = $this->getNotTranslatedMarkets($lang_code);
			$templates = (is_file($pathToFile)) ? include $pathToFile : false;
			if($markets && $templates){
				foreach($markets as $market){
					if(!key_exists($market['sports_id'], $templates)){
						continue;
					}
					if(key_exists($market['title'], $templates[$market['sports_id']])){
						$this->saveNewLocale($market, $templates[$market['sports_id']][$market['title']], $lang_code);
					}
				}
			}


		}
		die('Done!');
	}

	private function getNotTranslatedMarkets($lang_code) {
		$locale = GameMarketsLocaleModel::tableName();
		return GameMarketsLocaleModel::find()
			->leftJoin("{$locale} AS other_lang", "{$locale}.market_id = other_lang.market_id AND other_lang.lang_code = '{$lang_code}'")
			->where(["{$locale}.lang_code" => 'en', 'other_lang.title' => NULL])
			->andWhere($locale . '.group_id NOT IN (' . self::EXCLUDE_GROUPS . ')')
			->asArray()->all();
	}

	private function saveNewLocale($attributes, $title, $lang_code) {
		if($title == ''){
			return false;
		}
		$model = new GameMarketsLocaleModel();
		$model->scenario = GameMarketsLocaleModel::SCENARIO_CREATE;
		$model->sports_id = $attributes['sports_id'];
		$model->group_id = $attributes['group_id'];
		$model->market_id = $attributes['market_id'];
		$model->lang_code = $lang_code;
		$model->is_active = 1;
		$model->title = mb_convert_case($title, MB_CASE_TITLE);
		if($model->save()){
			echo $attributes['title'] . ' : ' . $title . PHP_EOL;
		}
	}

	public static function dd($var = 'Done!', $die = false) {
		echo 'file:' . debug_backtrace()[0]['file'] . ' (' . debug_backtrace()[0]['line'] . ')' . '<br />';
		echo '<pre>' . print_r($var, true) . '</pre>';
		if(!$die){
			die();
		}
	}


}

<?php

namespace backend\modules\games\models;

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\GameMarketsGroupsAdditionalModel;
use common\modules\games\models\GameMarketsGroupsModel as GameMarketsGroupsBaseModel;
use yii\helpers\ArrayHelper;

class GameMarketsGroupsModel extends GameMarketsGroupsBaseModel
{

	public function saveAdditionalMarkets($items) {
		$i = 1;
		$markets = [];
		foreach($items as $item){
			$markets[$item] = $i++;
		}
		if($this->additionalMarkets != []){

			foreach($this->additionalMarkets as $row){
				if(key_exists($row->market_id,$markets)){ // if isset market in table
					if($markets[$row->market_id] != $row->sort_order){ // if sort order changed
						$row->updateAttributes(['sort_order' => $markets[$row->market_id]]);
					}
				}else{
					$row->delete();
				}
				unset($markets[$row->market_id]);
			}

			// add new markets
			if(!empty($markets)){
				$this->saveMarket($markets);
			}

		}else{
			$this->saveMarket($markets);
		}
	}

	private function saveMarket($markets){
		foreach($markets as $market_id => $sort){
			$row = new GameMarketsGroupsAdditionalModel();
			$row->sports_id = $this->sports_id;
			$row->group_id = $this->id;
			$row->market_id = $market_id;
			$row->sort_order = $sort;
			$row->save();
		}
	}

	public function getAdditionalMarketsHtml() {
		$html = '';

		foreach($this->additionalMarkets as $item){
			$html .= '<li data-id="' . $item->market_id . '">
							<input type="hidden" name="market[' . $item->market_id . ']" value="' . $item->market_id . '">
								<strong>' . GameHelpers::getStringForLocale($item->market->group->gameMarketsGroupsLocales, 'title', false, true) . ':</strong> ' . GameHelpers::getStringForLocale($item->market->gameMarketsLocales, 'title', false, true) . '
									<span class="fa fa-remove">
									</span>
						</li>' . PHP_EOL;
		}
		return $html;
	}

}
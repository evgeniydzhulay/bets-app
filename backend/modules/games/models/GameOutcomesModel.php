<?php

namespace backend\modules\games\models;

use common\modules\games\models\GameOutcomesModel as GameOutcomesBaseModel;

class GameOutcomesModel extends GameOutcomesBaseModel
{

	public function mergerOutcomes($outcomes) {
		foreach($outcomes as $id){
			$outcome = self::findOne($id);

			if($outcome !== null){
				if(empty($this->condition)){
					$this->updateAttributes(['condition' => $outcome->condition]);
				}

				foreach($outcome->apiGameOutcomes as $apiOutcome){
					$apiOutcome->updateAttributes(['outcome_id' => $this->id]);
				}
				$outcome->delete();
			}
		}
		return true;
	}

	public function mergerOutcomesSetCondition($outcomes, $selected_id) {
		foreach($outcomes as $id){
			$outcome = self::findOne($id);
			if($outcome !== null){
				if($outcome->id == $selected_id){
					$this->updateAttributes(['condition' => $outcome->condition]);
				}

				foreach($outcome->apiGameOutcomes as $apiOutcome){
					$apiOutcome->updateAttributes(['outcome_id' => $this->id]);
				}
				$outcome->delete();
			}
		}
		return true;
	}
}
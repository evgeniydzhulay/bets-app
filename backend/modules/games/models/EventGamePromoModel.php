<?php

namespace backend\modules\games\models;

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameOutcomesLocaleModel;
use Yii;
use common\modules\games\models\EventGamePromoModel as EventGamePromoBaseModel;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

class EventGamePromoModel extends EventGamePromoBaseModel
{

	/**
	 * @param $game \common\modules\games\models\EventGameModel
	 * @return EventGamePromoModel|null|static
	 */

	public function findPromo($game) {
		$promo = self::findOne(['game_id' => $game->id]);
		if($promo == null){
			return new self([
				'game_id' => $game->id,
				'sports_id' => $game->sports_id
			]);
		}
		return $promo;
	}


	public function marketsList() {
		$list = [];
		$marketsLocale = GameMarketsLocaleModel::tableName();
		$groupLocale = GameMarketsGroupsLocaleModel::tableName();
		$eventTable = EventOutcomesModel::tableName();

		$data = EventOutcomesModel::find()->where(['game_id' => $this->game_id, 'is_enabled' => EventOutcomesModel::STATUS_ENABLED])
			->select("{$eventTable}.group_id,{$eventTable}.market_id,{$eventTable}.id,{$eventTable}.game_id")
			->addSelect([
				'group_title' => new Expression('COALESCE(g_current.title,g_default.title)'),
				'market_title' => new Expression('COALESCE(m_current.title,m_default.title)'),
			])
			->leftJoin("{$groupLocale} as g_default", "{$eventTable}.group_id = g_default.group_id AND g_default.lang_code = 'en'")
			->leftJoin("{$groupLocale} as g_current", "{$eventTable}.group_id = g_current.group_id AND g_current.lang_code = '" . Yii::$app->language . "'")
			->leftJoin("{$marketsLocale} as m_default", "{$eventTable}.market_id = m_default.market_id AND m_default.lang_code = 'en'")
			->leftJoin("{$marketsLocale} as m_current", "{$eventTable}.market_id = m_current.market_id AND m_current.lang_code = '" . Yii::$app->language . "'")
			->groupBy("{$eventTable}.id")->asArray()->all();

		if(!empty($data)){
			foreach($data as $item){
				$list[$item['group_title']][$item['market_id']] = $item['market_title'];
			}
		}
		return $list;
	}

	public function marketOutcomesHtml($market_id = false, $game_id ) {

		// if caused by controller
		if(!$market_id && !empty($this->outcome_1st_id)){
			$event = EventOutcomesModel::findOne($this->outcome_1st_id);
			$market_id = ($event !== null) ? $event->market_id : null;
		}

		$gameOutcomes = $this->marketOutcomes($market_id, $game_id);
		$promo = self::findOne(['game_id' => $game_id]);
		if($promo !== null){
			$selected[] = $promo->outcome_1st_id;
			$selected[] = $promo->outcome_2nd_id;
		}else{
			$selected = [];
		}

		$html = '';
		if(!empty($gameOutcomes)){
			foreach($gameOutcomes as $id => $title){
				$checked = (in_array($id, $selected)) ? "checked" : '';
				$html .= "<div class='form-group'><input type='checkbox' name='outcomes[{$id}]' id='{$id}' {$checked}><label for='{$id}'>{$title}</label></div>";
			}
		}
		return $html;
	}


	private function marketOutcomes($market_id, $game_id) {
		$eventTable = EventOutcomesModel::tableName();
		$outcomeLocale = GameOutcomesLocaleModel::tableName();
		$data = EventOutcomesModel::find()->where([$eventTable . '.game_id' => $game_id, $eventTable . '.market_id' => $market_id, $eventTable . '.is_enabled' => EventOutcomesModel::STATUS_ENABLED])
			->select("{$eventTable}.market_id,{$eventTable}.id,{$eventTable}.game_id,{$eventTable}.outcome_id")
			->addSelect([
				'outcome_title' => new Expression('COALESCE(current.title,default.title)'),
			])
			->leftJoin("{$outcomeLocale} as default", "{$eventTable}.outcome_id = default.outcome_id AND default.lang_code = 'en'")
			->leftJoin("{$outcomeLocale} as current", "{$eventTable}.outcome_id = current.outcome_id AND current.lang_code = '" . Yii::$app->language . "'")
			->groupBy("{$eventTable}.id")->asArray()->all();
		return ArrayHelper::map($data, 'id', 'outcome_title'); // id - EventOutcomesModel, outcome_title - GameOutcomesLocaleModel
	}

}

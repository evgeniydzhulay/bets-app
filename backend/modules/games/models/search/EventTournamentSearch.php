<?php

namespace backend\modules\games\models\search;

use common\modules\games\models\EventTournamentLocaleModel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\games\models\EventTournamentModel;

/**
 * EventTournamentSearch represents the model behind the search form of `common\modules\games\models\EventTournamentModel`.
 */
class EventTournamentSearch extends EventTournamentModel
{
	public $tournament_title;
	public $date_start;
	public $date_end;
	public $sport_title;

	public $extraOptions = false;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		$rules = parent::rules();
		$rules[] = [['tournament_title','date_start','date_end'], 'safe'];
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'tournament_title';
		$scenarios[self::SCENARIO_SEARCH][] = 'date_start';
		$scenarios[self::SCENARIO_SEARCH][] = 'date_end';
		return $scenarios;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = static::find()->groupBy(self::tableName() . '.id');
		$query->joinWith('eventTournamentLocales');
		if($this->extraOptions){
			$query->with(['gameSportsLocales' => function($q){$q->indexBy('lang_code');}]);
		}

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['starts_at' => SORT_DESC,'id' => SORT_DESC],
				'attributes' => [
					'tournament_title' => [
						'asc' => [EventTournamentLocaleModel::tableName().'.title' => SORT_ASC],
						'desc' => [EventTournamentLocaleModel::tableName().'.title' => SORT_DESC],
					],
					'id',
					'is_enabled',
					'sports_id',
					'sort_order',
					'starts_at',
					'is_banned',
					'is_finished',
					'is_hidden',
				],
			]
		]);

		$this->load($params);

		if(!$this->validate()){
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			self::tableName() . '.id' => $this->id,
			self::tableName() . '.sports_id' => $this->sports_id,
			//'starts_at' => $this->starts_at,
			self::tableName() . '.sort_order' => $this->sort_order,
			self::tableName() . '.is_enabled' => $this->is_enabled,
			self::tableName() . '.is_banned' => $this->is_banned,
			self::tableName() . '.is_finished' => $this->is_finished,
			self::tableName() . '.is_hidden' => $this->is_hidden,
		]);

		if($this->date_start && $this->date_end){
			$start = strtotime($this->date_start);
			$end = strtotime($this->date_end) + 86400;
			$query->andFilterWhere(['>', self::tableName() . '.starts_at', $start]);
			$query->andFilterWhere(['<', self::tableName() . '.starts_at', $end]);
		}

			$query->andFilterWhere(['like','event_tournament_locale.title', $this->tournament_title]);

		return $dataProvider;
	}
}

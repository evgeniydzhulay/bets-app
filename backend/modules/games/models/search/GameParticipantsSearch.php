<?php

namespace backend\modules\games\models\search;

use common\modules\games\models\GameParticipantsLocaleModel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\games\models\GameParticipantsModel;

/**
 * GameParticipantsSearch represents the model behind the search form of `\common\modules\games\models\GameParticipantsModel`.
 */
class GameParticipantsSearch extends GameParticipantsModel
{
	public $participant_name;

	/**
	 * @inheritdoc
	 */
	public function rules(){
		$rules = parent::rules();
		$rules[] =  ['participant_name','safe'];
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios(){
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'participant_name';
		return $scenarios;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params){
		$query = GameParticipantsModel::find()->groupBy(self::tableName() . '.id');;
		$query->joinWith('gameParticipantsLocales');

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => 'participant_name',
				'attributes' => [
					'participant_name' => [
						'asc' => [GameParticipantsLocaleModel::tableName().'.name' => SORT_ASC],
						'desc' => [GameParticipantsLocaleModel::tableName().'.name' => SORT_DESC],
						'default' => SORT_ASC
					],
					'id',
				],
			]
		]);

		$this->load($params);

		if(!$this->validate()){
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			self::tableName() . '.id' => $this->id,
			self::tableName() . '.sports_id' => $this->sports_id,
		]);

			$query->andFilterWhere(['like', 'game_participants_locale.name', $this->participant_name]);

		return $dataProvider;
	}
}

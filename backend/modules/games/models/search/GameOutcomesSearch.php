<?php

namespace backend\modules\games\models\search;

use common\modules\games\models\GameOutcomesLocaleModel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\games\models\GameOutcomesModel;

/**
 * GameOutcomesSearch represents the model behind the search form of `\common\modules\games\models\GameOutcomesModel`.
 */
class GameOutcomesSearch extends GameOutcomesModel
{
	public $outcome_title;

	/**
	 * @inheritdoc
	 */
	public function rules()
	{
			$rules = parent::rules();
			$rules[] = ['outcome_title','safe'];
			return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios()
	{
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'outcome_title';
		return $scenarios;
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search($params)
	{
		$query = static::find()->groupBy(self::tableName() . '.id');
		$query->joinWith('gameOutcomesLocales');

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['sort_order' => SORT_ASC,'outcome_title' => SORT_ASC],
				'attributes' => [
					'outcome_title' => [
						'asc' => [GameOutcomesLocaleModel::tableName().'.title' => SORT_ASC],
						'desc' => [GameOutcomesLocaleModel::tableName().'.title' => SORT_DESC],
					],
					'id',
					'is_enabled',
					'is_main',
					'sort_order',
					'short_title'
				],
			]
		]);

		$this->load($params);

		if (!$this->validate()) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			self::tableName() . '.id' => $this->id,
			self::tableName() . '.sports_id' => $this->sports_id,
			self::tableName() . '.group_id' => $this->group_id,
			self::tableName() . '.market_id' => $this->market_id,
			self::tableName() . '.is_enabled' => $this->is_enabled,
			self::tableName() . '.is_main' => $this->is_main,
		]);

			$query->andFilterWhere(['like', 'game_outcomes_locale.title', $this->outcome_title]);
			$query->andFilterWhere(['like', 'short_title', $this->short_title]);


		return $dataProvider;
	}
}
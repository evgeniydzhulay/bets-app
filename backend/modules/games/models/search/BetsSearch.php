<?php

namespace backend\modules\games\models\search;

use backend\modules\games\models\BetsItemsModel;
use backend\modules\games\models\BetsModel;
use common\modules\games\models\BetsRevertedModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\user\models\ProfileModel;
use common\modules\user\models\UserModel;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\Expression;
use yii\twig\Profile;

/**
 * @property string $user_name
 * @property int amount_bet_from;
 * @property int amount_bet_to;
 * @property int amount_win_from;
 * @property int amount_win_to;
 *
 */

/**
 * BetsSearch represents the model behind the search form of `common\modules\games\models\BetsModel`.
 */
class BetsSearch extends BetsModel
{
	public $user_name;
	public $creator_name; /* for bets_reverted */
	public $date_start;
	public $date_end;

	public $sport_id;
	public $tournament_id;
	public $game_id;

	public $amount_bet_from;
	public $amount_bet_to;
	public $amount_win_from;
	public $amount_win_to;

	public $total_sum;

	public function rules() {
		$rules = parent::rules();
		$rules[] = [['user_name', 'date_start', 'date_end', 'sport_id', 'tournament_id', 'game_id'], 'safe'];
		$rules[] = [['amount_bet_from', 'amount_bet_to', 'amount_win_from', 'amount_win_to', 'creator_name'], 'integer'];
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'user_name';
		$scenarios[self::SCENARIO_SEARCH][] = 'date_start';
		$scenarios[self::SCENARIO_SEARCH][] = 'date_end';
		$scenarios[self::SCENARIO_SEARCH][] = 'sport_id';
		$scenarios[self::SCENARIO_SEARCH][] = 'tournament_id';
		$scenarios[self::SCENARIO_SEARCH][] = 'game_id';
		$scenarios[self::SCENARIO_SEARCH][] = 'amount_bet_from';
		$scenarios[self::SCENARIO_SEARCH][] = 'amount_bet_to';
		$scenarios[self::SCENARIO_SEARCH][] = 'amount_win_from';
		$scenarios[self::SCENARIO_SEARCH][] = 'amount_win_to';
		$scenarios[self::SCENARIO_SEARCH][] = 'creator_name';
		return $scenarios;
	}

	/**
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = static::find()->groupBy(self::tableName() . '.id');
		$query->joinWith('userProfile');
		$query->joinWith(['items.outcome' => function ($query) {
			$query->with(
				[
					'sportsLocale' => function ($query) {
						$query->addSelect('id,title,sports_id,lang_code')->andWhere(['lang_code' => ['en', 'ru']])->indexBy('lang_code');
					},
					'tournamentLocale' => function ($query) {
						$query->addSelect('id,title,sports_id,lang_code,tournament_id')->andWhere(['lang_code' => ['en', 'ru']])->indexBy('lang_code');
					},
					'game.eventGameLocales' => function ($query) {
						$query->addSelect('id,title,sports_id,lang_code,tournament_id,game_id')->andWhere(['lang_code' => ['en', 'ru']])->indexBy('lang_code');
					},
					'outcomeLocale' => function ($query) {
						$query->andWhere(['lang_code' => ['en', 'ru']])->indexBy('lang_code');
					},
					'marketLocale' => function ($query) {
						$query->andWhere(['lang_code' => ['en', 'ru']])->indexBy('lang_code');
					},
					'game.eventParticipants.participantLocale' => function ($query) {
						$query->andWhere(['lang_code' => 'en']);
					},
				])->asArray();
		}]);

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['created_at' => SORT_DESC],
				'attributes' => [
					'user_name' => [
						'asc' => [ProfileModel::tableName() . '.name' => SORT_ASC],
						'desc' => [ProfileModel::tableName() . '.name' => SORT_DESC],
					],
					'id',
					'status',
					'created_at',
					'amount_bet',
					'amount_win'
				],
			]
		]);

		$this->load($params);

		if(!$this->validate()){
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			self::tableName() . '.id' => $this->id,
			self::tableName() . '.user_id' => $this->user_id,
			self::tableName() . '.withdraw_id' => $this->withdraw_id,
			self::tableName() . '.refill_id' => $this->refill_id,
			self::tableName() . '.amount_bet' => $this->amount_bet,
			self::tableName() . '.amount_win' => $this->amount_win,
			self::tableName() . '.status' => $this->status,
			//self::tableName() . '.created_at' => $this->created_at,
		]);

		// Search by events
		$query->andFilterWhere([
			EventOutcomesModel::tableName() . '.sports_id' => $this->sport_id,
			EventOutcomesModel::tableName() . '.tournament_id' => $this->tournament_id,
			EventOutcomesModel::tableName() . '.game_id' => $this->game_id
		]);

		// Search by sum
		$query->andFilterWhere(['>=', self::tableName() . '.amount_bet', $this->amount_bet_from]);
		$query->andFilterWhere(['<=', self::tableName() . '.amount_bet', $this->amount_bet_to]);
		$query->andFilterWhere(['>=', self::tableName() . '.amount_win', $this->amount_win_from]);
		$query->andFilterWhere(['<=', self::tableName() . '.amount_win', $this->amount_win_to]);

		// Search by user
		$query->orFilterWhere(['like', ProfileModel::tableName() . '.name', $this->user_name]);
		$query->orFilterWhere(['like', ProfileModel::tableName() . '.surname', $this->user_name]);

		//search by date
		if($this->date_start && $this->date_end){
			$start = strtotime($this->date_start);
			$end = strtotime($this->date_end) + 86400;
			$query->andFilterWhere(['>', self::tableName() . '.created_at', $start]);
			$query->andFilterWhere(['<', self::tableName() . '.created_at', $end]);
		}

		/* set sum */
		$this->total_sum = $this->getTotalSumHtml();
		return $dataProvider;
	}


	public function searchBetsReverted($params) {
		$query = static::find()->where([self::tableName() . '.status' => BetsModel::STATUS_BANNED])->groupBy(self::tableName() . '.id');
		/* user */
		$query->joinWith('userProfile');
		/* bets reverted / creator */
		$query->joinWith(['betsReverted' => function ($query) {
			$query->with('creatorProfile');
		}]);
		/* outcomes */
		$query->joinWith(['items.outcome' => function ($query) {
			$query->with(
				[
					'sportsLocale' => function ($query) {
						$query->addSelect('id,title,sports_id,lang_code')->andWhere(['lang_code' => ['en', 'ru']])->indexBy('lang_code');
					},
					'tournamentLocale' => function ($query) {
						$query->addSelect('id,title,sports_id,lang_code,tournament_id')->andWhere(['lang_code' => ['en', 'ru']])->indexBy('lang_code');
					},
					'gameLocale' => function ($query) {
						$query->addSelect('id,title,sports_id,lang_code,tournament_id,game_id')->andWhere(['lang_code' => ['en', 'ru']])->indexBy('lang_code');
					},
					'outcomeLocale' => function ($query) {
						$query->andWhere(['lang_code' => ['en', 'ru']])->indexBy('lang_code');
					},
					'marketLocale' => function ($query) {
						$query->andWhere(['lang_code' => ['en', 'ru']])->indexBy('lang_code');
					},
				])->asArray();
		}]);


		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['user_name' => SORT_ASC],
				'attributes' => [
					'user_name' => [
						'asc' => [ProfileModel::tableName() . '.name' => SORT_ASC],
						'desc' => [ProfileModel::tableName() . '.name' => SORT_DESC],
					],
					'creator_name' => [
						'asc' => [ProfileModel::tableName() . '.name' => SORT_ASC],
						'desc' => [ProfileModel::tableName() . '.name' => SORT_DESC],
					],
					'id',
					'amount_bet',
				],
			]
		]);

		$this->load($params);

		if(!$this->validate()){
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			self::tableName() . '.id' => $this->id,
			self::tableName() . '.user_id' => $this->user_id,
			self::tableName() . '.amount_bet' => $this->amount_bet,
			self::tableName() . '.status' => $this->status,
		]);

		// Search by user
		$query->orFilterWhere(['like', ProfileModel::tableName() . '.name', $this->user_name]);
		$query->orFilterWhere(['like', ProfileModel::tableName() . '.surname', $this->user_name]);

		//search by date
		if($this->date_start && $this->date_end){
			$start = strtotime($this->date_start);
			$end = strtotime($this->date_end) + 86400;
			$query->andFilterWhere(['>', BetsRevertedModel::tableName() . '.created_at', $start]);
			$query->andFilterWhere(['<', BetsRevertedModel::tableName() . '.created_at', $end]);
		}

		//search by "created_by"
		$query->andFilterWhere([BetsRevertedModel::tableName() . '.created_by' => $this->creator_name]);
		return $dataProvider;
	}

	public function getTotalSumHtml() {//Total winning bets on the amount
		$total = BetsModel::find()->sum('amount_bet');
		$total_win = BetsModel::find()->where(['status' => self::STATUS_VICTORY])->sum('amount_win');
		return '<div  class="text-bold" style="font-size: 14px">' . Yii::t('bets', 'Total bets on amount: ') . Yii::$app->formatter->asCurrency($total, '') . '</div>'
			. '<div  class="text-bold text-green text-bold" style="font-size: 14px">' . Yii::t('bets', 'Total winning bets on the amount: ') . $a = $total_win ? Yii::$app->formatter->asCurrency($total_win, '') : '0.00' . '</div>';
	}
}

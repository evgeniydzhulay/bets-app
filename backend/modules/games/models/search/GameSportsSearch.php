<?php

namespace backend\modules\games\models\search;

use common\modules\games\models\GameSportsLocaleModel;
use yii\data\ActiveDataProvider;
use common\modules\games\models\GameSportsModel;

/**
 * GameSportsSearch represents the model behind the search form of `common\modules\games\models\GameSportsModel`.
 */
class GameSportsSearch extends GameSportsModel
{

	public $sport_title;

	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		$rules = parent::rules();
		$rules[] = ['sport_title','safe'];
		$rules[] = ['id', 'integer'];
		return $rules;
	}

	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'sport_title';
		$scenarios[self::SCENARIO_SEARCH][] = 'id';
		return $scenarios;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = static::find()->groupBy(self::tableName() . '.id');
		$query->joinWith('gameSportsLocales');

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['sport_title' => SORT_ASC],
				'attributes' => [
					'sport_title' => [
						'asc' => [GameSportsLocaleModel::tableName() . '.title' => SORT_ASC],
						'desc' => [GameSportsLocaleModel::tableName() . '.title' => SORT_DESC],
					],
					'id',
					'is_enabled',
					'sort_order'
				],
			]
		]);

		$this->load($params);
		if(!$this->validate()){
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			self::tableName() . '.id' => $this->id,
			self::tableName() . '.is_enabled' => $this->is_enabled,
		]);

		$query->andFilterWhere(['like', 'game_sports_locale.title', $this->sport_title]);
		return $dataProvider;
	}
}

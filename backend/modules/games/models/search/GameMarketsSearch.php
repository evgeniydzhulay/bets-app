<?php

namespace backend\modules\games\models\search;

use common\modules\games\models\GameMarketsLocaleModel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\games\models\GameMarketsModel;

/**
 * GameMarketsSearch represents the model behind the search form of `\common\modules\games\models\GameMarketsModel`.
 */
class GameMarketsSearch extends GameMarketsModel
{
	public $market_title;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		$rules = parent::rules();
		$rules[] = ['market_title', 'safe'];
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {

		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'market_title';
		return $scenarios;
	}

	/**
	 * Creates data provider instance with search query applied
	 * @param array $params
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = static::find()->groupBy(self::tableName() . '.id');;
		$query->joinWith('gameMarketsLocales');

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['market_title' => SORT_ASC],
				'attributes' => [
					'market_title' => [
						'asc' => [GameMarketsLocaleModel::tableName() . '.title' => SORT_ASC],
						'desc' => [GameMarketsLocaleModel::tableName() . '.title' => SORT_DESC],
					],
					'id',
					'is_enabled',
					'sort_order',
					'is_main'
				],
			]
		]);

		$this->load($params);


		if(!$this->validate()){
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			self::tableName() . '.id' => $this->id,
			self::tableName() . '.sports_id' => $this->sports_id,
			self::tableName() . '.group_id' => $this->group_id,
			self::tableName() . '.is_enabled' => $this->is_enabled,
			self::tableName() . '.is_main' => $this->is_main,
			self::tableName() . '.sort_order' => $this->sort_order,
		]);

		$query->andFilterWhere(['like', 'game_markets_locale.title', $this->market_title]);

		return $dataProvider;
	}
}
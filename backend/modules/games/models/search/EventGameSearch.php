<?php

namespace backend\modules\games\models\search;

use backend\modules\games\models\EventGamePromoModel;
use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventTournamentLocaleModel;
use common\modules\games\models\GameSportsLocaleModel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\games\models\EventGameModel;

/**
 * EventGameSearch represents the model behind the search form of `common\modules\games\models\EventGameModel`.
 */
class EventGameSearch extends EventGameModel
{

	public $game_title;
	public $date_start;
	public $date_end;
	public $tournament_title;
	public $link;
	public $promo_game;
	public $extraOptions = false;

	/**
	 * @inheritdoc
	 */
	public function rules() {
		$rules = parent::rules();
		$rules[] = [['game_title', 'date_start', 'date_end','tournament_title','link','promo_game'], 'safe'];
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios() {
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'game_title';
		$scenarios[self::SCENARIO_SEARCH][] = 'date_start';
		$scenarios[self::SCENARIO_SEARCH][] = 'date_end';
		$scenarios[self::SCENARIO_SEARCH][] = 'tournament_title';
		$scenarios[self::SCENARIO_SEARCH][] = 'link';
		$scenarios[self::SCENARIO_SEARCH][] = 'promo_game';
		return $scenarios;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params) {
		$query = EventGameModel::find()->groupBy(self::tableName() . '.id');
		$query->joinWith('eventGameLocales');

		if($this->extraOptions){
			$query->joinWith(['tournamentLocales' => function ($q) {
				$q->indexBy('lang_code');
			}]);
			$query->with(['sportsLocales' => function ($q) {
				$q->indexBy('lang_code');
			}]);
		}
		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['starts_at' => SORT_DESC, 'id' => SORT_DESC],
				'attributes' => [
					'game_title' => [
						'asc' => [EventGameLocaleModel::tableName() . '.title' => SORT_ASC],
						'desc' => [EventGameLocaleModel::tableName() . '.title' => SORT_DESC],
					],
					'id',
					'is_enabled',
					'sports_id',
					'tournament_id',
					'is_banned',
					'is_finished',
					'is_hidden',
					'sort_order',
					'starts_at',
				],
			]
		]);

		$this->load($params);
		if(!$this->validate()){
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		if($this->extraOptions){
			/* search tournament */
			$query->andFilterWhere(['like',EventTournamentLocaleModel::tableName() . '.title' , $this->tournament_title]);
			/* sort tournament */
			$dataProvider->sort->attributes['tournament_title'] = [
				'asc' => [EventTournamentLocaleModel::tableName() . '.title' => SORT_ASC],
				'desc' => [EventTournamentLocaleModel::tableName() . '.title' => SORT_DESC],
			];
		}

		// grid filtering conditions
		$query->andFilterWhere([
			self::tableName() . '.id' => $this->id,
			self::tableName() . '.sports_id' => $this->sports_id,
			self::tableName() . '.tournament_id' => $this->tournament_id,
			//'starts_at' => $this->starts_at,
			self::tableName() . '.sort_order' => $this->sort_order,
			self::tableName() . '.is_enabled' => $this->is_enabled,
			self::tableName() . '.is_banned' => $this->is_banned,
			self::tableName() . '.is_finished' => $this->is_finished,
			self::tableName() . '.is_hidden' => $this->is_hidden,
		]);

		if($this->date_start && $this->date_end){
			$start = strtotime($this->date_start);
			$end = strtotime($this->date_end) + 86400;
			$query->andFilterWhere(['>', self::tableName() . '.starts_at', $start]);
			$query->andFilterWhere(['<', self::tableName() . '.starts_at', $end]);
		}

		if($this->promo_game){
			$query->joinWith('eventGamePromo',false);
			$query->andWhere(EventGamePromoModel::tableName() . '.id IS NOT NULL');
		}

		if(!empty($this->link)){
			$query->andWhere(['!=','video_link','']);
		}

		$query->andFilterWhere(['like', 'event_game_locale.title', $this->game_title]);

		return $dataProvider;
	}
}

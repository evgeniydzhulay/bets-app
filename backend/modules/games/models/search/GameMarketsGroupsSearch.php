<?php

namespace backend\modules\games\models\search;

use common\modules\games\models\GameMarketsGroupsLocaleModel;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\modules\games\models\GameMarketsGroupsModel;

/**
 * GameMarketsGroupsSearch represents the model behind the search form of `common\modules\games\models\GameMarketsGroupsModel`.
 */
class GameMarketsGroupsSearch extends GameMarketsGroupsModel
{

	public $group_title;

	public function rules(){
		$rules = parent::rules();
		$rules[] = ['group_title','safe'];
		return $rules;
	}

	/**
	 * @inheritdoc
	 */
	public function scenarios(){
		$scenarios = parent::scenarios();
		$scenarios[self::SCENARIO_SEARCH][] = 'group_title';
		return $scenarios;
	}

	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search($params){
		$query = static::find();
		$query->joinWith('gameMarketsGroupsLocales');

		// add conditions that should always apply here

		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'sort' => [
				'defaultOrder' => ['group_title' => SORT_ASC],
				'attributes' => [
					'group_title' => [
						'asc' => [GameMarketsGroupsLocaleModel::tableName().'.title' => SORT_ASC],
						'desc' => [GameMarketsGroupsLocaleModel::tableName().'.title' => SORT_DESC],
					],
					'id',
					'is_enabled',
					'sort_order'
				],
			]
		]);

		$this->load($params);

		if(!$this->validate()){
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}

		// grid filtering conditions
		$query->andFilterWhere([
			self::tableName() . '.id' => $this->id,
			self::tableName() . '.sports_id' => $this->sports_id,
			self::tableName() . '.is_enabled' => $this->is_enabled,
			self::tableName() . '.sort_order' => $this->sort_order,
		]);

			$query->andFilterWhere(['like','game_markets_groups_locale.title', $this->group_title]);

		return $dataProvider;
	}
}

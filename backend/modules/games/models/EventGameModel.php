<?php

namespace backend\modules\games\models;

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\ApiEventGameModel;
use common\modules\games\models\EventGameModel as EventGameBaseModel;
use common\modules\games\models\EventParticipantsModel;
use Yii;


/**
 *
 * @property void $participantsHtml
 */
class EventGameModel extends EventGameBaseModel
{

	public function afterSave($insert, $changedAttributes) {
		$this->setParticipants($insert);
		parent::afterSave($insert, $changedAttributes);
	}

	private function setParticipants($insert) {
		$participants = !empty(Yii::$app->request->post('participants')) ? Yii::$app->request->post('participants') : false;
		if(!$insert){ // if update
			EventParticipantsModel::deleteAll(['sports_id' => $this->sports_id, 'tournament_id' => $this->tournament_id, 'game_id' => $this->id]);
		}
		if($participants && is_array($participants)){
			$i = 1;
			foreach($participants as $key => $participant_id ){
				$model = new EventParticipantsModel(['scenario' => EventParticipantsModel::SCENARIO_CREATE]);
				$model->setAttributes([
					'participant_id' => $participant_id,
					'sports_id' => $this->sports_id,
					'tournament_id' => $this->tournament_id,
					'game_id' => $this->id,
					'sort_order' => $i++
				]);
				$model->save();
			}
			return true;
		}
		return false;
	}

	public function getParticipantsHtml() {
		$html = '';
		if(ApiEventGameModel::find()->where(['game_id' => $this->id])->exists()){
			return $html;
		}
		$participants = EventParticipantsModel::find()->where(
			[
				'sports_id' => $this->sports_id,
				'tournament_id' => $this->tournament_id,
				'game_id' => $this->id
			]
		)->orderBy(['sort_order' => SORT_ASC])->with(['participantLocale' => function ($query) {
			$query->indexBy('lang_code');
		}])->all();
		if(!empty($participants)){
			foreach($participants as $participant){
				$name = !empty($participant->participantLocale) ? GameHelpers::getStringForLocale($participant->participantLocale,'name',false,true) : 'Not set';
				$html .= '<li>' . $name . '<input type="hidden" value="' . $participant->participant_id . '" name="participants[' . $participant->participant_id . ']"><span class="remove-participant glyphicon glyphicon-remove"></span></li>';
			}
		}
		return $html;
	}
}
<?php

/* @var $this \yii\web\View */
/* @var $model \common\modules\games\models\GameParticipantsLocaleModel */

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;


/*$this->title = Yii::t('games', 'Update') .': ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['game-sports/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Participants'), 'url' => ['update', 'participant_id' => $model->participant_id]];
$this->params['breadcrumbs'][] = $this->title;*/

echo $this->render('/_parts/_breadcrumbs.php',['location' => Yii::t('games','Update participant'),'parent' => 'GameSports','parent_id' => $model->sports_id]);
?>
<div class="game-participants-model-update box box-primary">

	<div class="box-header with-border">
		<h3 class="box-title"><?= Yii::t('games', 'Participant') ?>:
			<span class="text-red"><?= $model->name ?></span> &nbsp; <?= Yii::t('games', 'Language') ?>:
			<span class="text-red"> <?= $model->lang_code ?></span></h3>
	</div>
	<div class="box-body">
		<!-- content -->
		<div class="text-right">
			<?= Html::a(Yii::t('games','Close'), Url::to(['update','participant_id' => $model->participant_id]), ['class' => 'btn btn-md btn-warning']) ?>
		</div>
		<?php $form = ActiveForm::begin() ?>
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'name')->textInput() ?>
			</div>
		</div>

		<div class="text-right">
			<?= Html::submitButton(Yii::t('games','Update'), ['class' => 'btn btn-primary']) ?>
		</div>
		<?php ActiveForm::end() ?>

	</div>

</div>

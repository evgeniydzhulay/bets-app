<?php

use backend\modules\games\components\GameHelpers;
use yii\helpers\Html;
use common\widgets\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\games\models\GameParticipantsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

/*$this->registerCssFile('/static/css/games.css');
$this->title = Yii::t('games', 'Participants');
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['game-sports/index']];
$this->params['breadcrumbs'][] = $this->title;*/
echo $this->render('/_parts/_breadcrumbs.php',['location' => Yii::t('games','Participants'),'parent' => 'GameSports','parent_id' => $searchModel->sports_id]);
?>
<div class="game-participants-model-index">

	<div class="box box-primary">
		<div class="box-header with-border" style="min-height:43px;">
			<div class="box-tools pull-right">
				<?= Html::a('<span class="fa fa-angle-double-left"></span> ' . Yii::t('games', 'Sport'), Url::to(['/games/game-sports/index']), ['class' => 'btn btn-primary']) ?>
			</div>
			<!-- /.box-tools -->
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<?php
			echo GridView::widget([
				'dataProvider' => $dataProvider,
				'filterModel' => $searchModel,
				'pjax' => false,
				'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
				'toolbar' => [
					[
						'content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['game-participants/index', 'sport_id' => $searchModel->sports_id]), [
								'class' => 'btn btn-warning',
								'style' => 'margin-right:5px',
									'title' => Yii::t('games', 'Reset filter')
							])
							. Html::a('<span class="fa fa-plus"></span>', Url::to(['game-participants/create', 'sport_id' => $searchModel->sports_id]), [
								'class' => 'btn btn-default',
								'data-pjax' => 0,
								'title' => Yii::t('games', 'Add participant')
							])
					]
				],
				'columns' => [
					[
						'attribute' => 'id',
						'headerOptions' => ['width' => '80px'],
					],
					[
						'label' => Yii::t('games', 'Title'),
						'attribute' => 'participant_name',
						'value' => function ($data){
							return GameHelpers::getStringForLocale($data->gameParticipantsLocales, 'name',false,true);
						}
					],
					[
						'content' => function ($data){
							return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'participant_id' => $data->id], [
								'data-pjax' => 0,
								'class' => 'btn btn-block btn-xs btn-success'
							]);
						},
						'headerOptions' => ['width' => '60px'],
					],
				],
			]);
			?>
		</div>
	</div>
</div>

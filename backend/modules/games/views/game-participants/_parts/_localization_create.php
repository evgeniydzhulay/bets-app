<?php
/**
 * @var $model \common\modules\games\models\GameParticipantsModel
 * @var $this \yii\web\View
 */

use backend\modules\games\components\GameHelpers;
use yii\helpers\Html;

$marker = '';
if($model->isNewRecord){
	$languages = GameHelpers::languagesList();
}else{
	$languages = GameHelpers::findExceptionInLanguages(\common\modules\games\models\GameParticipantsLocaleModel::class, 'participant_id', $model->id);
	$marker = !empty($languages) ? 'class="text-danger"' : '';
}
?>

<?php if(!empty($languages)): ?>
	<h4 <?= $marker ?>><span class="fa fa-language text-warning"></span> <?= Yii::t('games', 'Localization:') ?></h4>
	<div class="row">
		<?php foreach($languages as $lang => $label): ?>
			<div class="form-group col-md-3">
				<label for="<?= $lang ?>"><?= $label ?></label>
				<?php $required = ($lang == 'en') ? ['required' => 'required'] : [] ?>
				<?= Html::textInput("locale[{$lang}]",null,['id' => $lang,'class' => 'form-control'] + $required) ?>
			</div>
		<?php endforeach; ?>
	</div>
<?php endif; ?>

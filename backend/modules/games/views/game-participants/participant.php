<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\modules\games\models\GameParticipantsModel
 * @var $searchModel \common\modules\games\models\GameParticipantsLocaleModel
 * @var $dataProvider \yii\data\ActiveDataProvider,
 */

use backend\modules\games\components\GameHelpers;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/*$this->title = $model->isNewRecord ? Yii::t('games', 'Add participant') : Yii::t('games', 'Update participant');
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['game-sports/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Participant'), 'url' => ['games/game-participants/index', 'sport_id' => $model->sports_id]];
$this->params['breadcrumbs'][] = $this->title;*/
$location = $model->isNewRecord ? Yii::t('games', 'Add participant') : Yii::t('games', 'Update participant');
echo $this->render('/_parts/_breadcrumbs.php', ['location' => $location, 'parent' => 'GameSports', 'parent_id' => $model->sports_id]);
$marker = '';
if($model->isNewRecord){
	$languages = GameHelpers::languagesList();
}else{
	$languages = GameHelpers::findExceptionInLanguages(\common\modules\games\models\GameParticipantsLocaleModel::class, 'participant_id', $model->id);
	$marker = !empty($languages) ? 'class="text-danger"' : '';
}
?>
<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">
			<?= Html::a(Yii::t('games', 'Close'), Url::to(['index', 'sport_id' => $model->sports_id]), ['class' => 'btn btn-warning']) ?>
		</div>
		<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body" style="padding:15px">
		<!-- block left -->
		<div class="col-sm-3">
			<?php $form = ActiveForm::begin() ?>
			<h4><span class="fa fa-gear text-warning"></span> <?= Yii::t('games', 'Settings:') ?></h4>
			<?php if(!empty($model->image)): ?>
				<div style="width: 60px;height: 60px;border: 1px solid #999; margin: 5px">
					<img style="width: 60px; height: 60px;" src="/uploads/games/<?= $model->image ?>"/>
				</div>
			<?php endif; ?>
			<div class="">
				<?= $form->field($model, 'image')->fileInput() ?>
			</div>
			<hr/>
			<!-- For insert -->
			<?php if(!empty($languages)): ?>
				<h4 <?= $marker ?>>
					<span class="fa fa-language text-warning"></span> <?= Yii::t('games', 'Localization:') ?></h4>
				<div class="row">
					<?php foreach($languages as $lang => $label): ?>
						<div class="form-group">
							<label for="<?= $lang ?>"><?= $label ?></label>
							<?php $required = ($lang == 'en') ? ['required' => 'required'] : [] ?>
							<?= Html::textInput("locale[{$lang}]", null, ['id' => $lang, 'class' => 'form-control'] + $required) ?>
						</div>
					<?php endforeach; ?>
				</div>
			<?php endif; ?>
			<div class="text-center">
				<?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-primary', 'style' => 'width:120px;margin: 25px 0']) ?>
			</div>
			<?php ActiveForm::end() ?>
		</div><!-- ./col-sm-3 -->
		<!-- right -->
		<div class="col-sm-9">
			<!-- For update -->
			<?= !$model->isNewRecord ? $this->render('_parts/_localization_update.php', ['model' => $model, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel]) : '' ?>
		</div>
	</div>
</div>

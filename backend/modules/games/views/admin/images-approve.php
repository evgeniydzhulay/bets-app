<?php
/* @var $this \yii\web\View */

use backend\modules\games\components\GameHelpers;
use kartik\form\ActiveForm;
use yii\helpers\Html;

?>
<style>
	.box-body{
		min-height:600px;
		background-color: lightslategrey;
		color: azure;
	}

	.select-image {
		position: relative;
		font-size: 12px;
		text-align: center;
	}

	.select-image input[type=checkbox] {
		display: none;
	}

	.select-image label {
		padding-left: 30px;
	}

	.select-image label:before {
		content: "";
		display: inline-block;
		background-repeat: no-repeat;
		background-position: center;
		display: inline-block;
		max-width: 100%;
		font-weight: 700;
		height: 100px;
		width: 100px;
		border-radius: 3px;
		margin: 5px 5px 10px 5px;
		padding: 5px;
		cursor: pointer;
	}

	.select-image input[type=checkbox]:checked + label:before {
		background-color: red;
		opacity: 0.6;
	}

	<?php foreach($models as $model): ?>
	.select-image label[for='<?= $model->id ?>']:before {
		background-image: url("/uploads/games/<?= $model->image ?>");
	}
	<?php endforeach; ?>
</style>

<div class="images-approve">
	<div class="box box-primary">
		<div class="box-header with-border" style="min-height:43px; padding: 10px 5px">
			<h4 class="text-bold text-danger">
				<?= Yii::t('games', 'Select images to delete') ?>
			</h4>
		</div>
		<!-- /.box-header -->
		<div class="box-body">
			<?php if($models !== []): ?>
				<?php $ids = [] ?>
				<?php $form = ActiveForm::begin() ?>
				<div class="row">
					<?php foreach($models as $model): ?>
						<?php $ids[] = $model->id ?>
						<div class="col-sm-2 select-image">
							<div class="text-center">
								<?= GameHelpers::getStringForLocale($model->gameParticipantsLocales, 'name', false, true) ?>
							</div>
							<input type="checkbox" id="<?= $model->id ?>" name="selected[<?= $model->id ?>]" value="<?= $model->id ?>"><label for="<?= $model->id ?>" ></label>
						</div>
					<?php endforeach; ?>
				</div>
				<?= Html::hiddenInput('ids', json_encode($ids)) ?>
				<div class="text-center">
					<?= Html::submitButton(Yii::t('games', 'Save'), [
						'class' => 'btn btn-danger',
						'data' => [
							'confirm' => Yii::t('games', 'Selected images will be deleted!'),
							'method' => 'post',
						],
					]) ?>
				</div>
				<?php ActiveForm::end() ?>
			<?php else: ?>
				<h2 class="text-center">Нет новых изображений!</h2>
			<?php endif; ?>
		</div>
	</div>
</div>

<?php

use backend\modules\games\components\GameHelpers;
use yii\helpers\Html;

?>

<form action="<?= \yii\helpers\Url::to(['verify-translate-update']) ?>" method="post">
	<table class="table">
		<tr>
			<td colspan="3" style="background-color: #f7f7f7">
				<b><?= Yii::t('games', 'Game type') ?>:</b> <?= !empty($model->sports->gameSportsLocales) ? GameHelpers::getStringForLocale($model->sports->gameSportsLocales,'title',false,true) : '' ?> &nbsp; - &nbsp;
				<b><?= Yii::t('games', 'Tournament') ?>:</b> <?= !empty($model->tournament->eventTournamentLocales) ? GameHelpers::getStringForLocale($model->tournament->eventTournamentLocales,'title',false,true) : '' ?>
			</td>
		</tr>
		<tr>
			<td>
				<strong> &nbsp;  &nbsp; En: &nbsp; </strong> <?= !empty($model->eventGameLocales['en']->title) ? $model->eventGameLocales['en']->title : '' ?>
				<?= Html::hiddenInput('model',$get) ?>
				<?= Html::hiddenInput('model-id',$model->eventGameLocales['ru']->id) ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php $ru = !empty($model->eventGameLocales['ru']->title) ? $model->eventGameLocales['ru']->title : '' ?>
				<div class="input-group">
					<span class="input-group-addon"><strong>Ru: </strong></span>
					<?= Html::textInput('title', $ru, ['class' => 'form-control']) ?>
					<span class="input-group-btn">
						<?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-info btn-flat']) ?>
               </span>
				</div>
			</td>
		</tr>
	</table>
</form>
<br/>
<?php

use backend\modules\games\components\GameHelpers;
use yii\helpers\Html;

?>

<form action="<?= \yii\helpers\Url::to(['verify-translate-update']) ?>" method="post">
	<table class="table">
		<tr>
			<td colspan="3" style="background-color: #f7f7f7">
				<b><?= Yii::t('games', 'Game type') ?>:</b> <?= !empty($model->sports->gameSportsLocales) ? GameHelpers::getStringForLocale($model->sports->gameSportsLocales,'title',false,true) : '' ?> &nbsp; - &nbsp;
				<b><?= Yii::t('games', 'Markets group') ?>:</b> <?= !empty($model->group->gameMarketsGroupsLocales) ? GameHelpers::getStringForLocale($model->group->gameMarketsGroupsLocales,'title',false,true) : '' ?>&nbsp;
			</td>
		</tr>
		<tr>
			<td>
				<strong> &nbsp;  &nbsp; En: &nbsp; </strong> <?= !empty($model->gameMarketsLocales['en']->title) ? $model->gameMarketsLocales['en']->title : '' ?>
				<?= Html::hiddenInput('model',$get) ?>
				<?= Html::hiddenInput('model-id',$model->gameMarketsLocales['ru']->id) ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php $ru = !empty($model->gameMarketsLocales['ru']->title) ? $model->gameMarketsLocales['ru']->title : '' ?>
				<div class="input-group">
					<span class="input-group-addon"><strong>Ru: </strong></span>
					<?= Html::textInput('title', $ru, ['class' => 'form-control']) ?>
					<span class="input-group-btn">
						<?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-info btn-flat']) ?>
               </span>
				</div>
			</td>
		</tr>
	</table>
</form>
<br/>
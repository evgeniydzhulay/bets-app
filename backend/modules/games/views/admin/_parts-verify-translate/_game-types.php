<?php

use yii\helpers\Html;

?>

<form action="<?= \yii\helpers\Url::to(['verify-translate-update']) ?>" method="post">
	<table class="table">
		<tr>
			<td colspan="3" style="background-color: #f7f7f7">
				<strong> &nbsp;  &nbsp; En: &nbsp; </strong> <?= !empty($model->gameSportsLocales['en']->title) ? $model->gameSportsLocales['en']->title : '' ?>
				<?= Html::hiddenInput('model',$get) ?>
				<?= Html::hiddenInput('model-id',$model->gameSportsLocales['ru']->id) ?>
			</td>
		</tr>
		<tr>
			<td>
				<?php $ru = !empty($model->gameSportsLocales['ru']->title) ? $model->gameSportsLocales['ru']->title : '' ?>
				<div class="input-group">
					<span class="input-group-addon"><strong>Ru: </strong></span>
					<?= Html::textInput('title', $ru, ['class' => 'form-control']) ?>
					<span class="input-group-btn">
						<?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-info btn-flat']) ?>
               </span>
				</div>
			</td>
		</tr>
	</table>
</form>
<br/>
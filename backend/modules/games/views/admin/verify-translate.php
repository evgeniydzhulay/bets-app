<?php

use yii\helpers\Url;
use yii\widgets\ListView;

/* @var $this \yii\web\View */

$this->title = Yii::t('games', 'Translations');
?>
<style>
	strong{
		color: red;
		font-size: 16px;
	}
</style>

<div class="verify-translate">
	<div class="box box-primary">
		<div class="box-header with-border" style="min-height:43px; padding: 10px 5px">

		</div>
		<!-- /.box-header -->
		<div class="box-body" style="min-height:600px;">
			<div class="row">
				<div class="col-sm-3">
					<div class="list-group">
						<?php if($dataItems['game-types'] != 0): ?>
							<a href="<?= Url::to(['verify-translate', 'item' => 'game-types']) ?>" class="list-group-item <?= ($get == 'game-types') ? 'active' : '' ?>">
								<?= Yii::t('games', 'Game type') ?> <span class="badge"><?= $dataItems['game-types'] ?></span>
							</a>
						<?php endif; ?>

						<?php if($dataItems['markets-group'] != 0): ?>
							<a href="<?= Url::to(['verify-translate', 'item' => 'markets-group']) ?>" class="list-group-item <?= ($get == 'markets-group') ? 'active' : '' ?>">
								<?= Yii::t('games', 'Market groups') ?>
								<span class="badge"><?= $dataItems['markets-group'] ?></span>
							</a>
						<?php endif; ?>

						<?php if($dataItems['markets'] != 0): ?>
							<a href="<?= Url::to(['verify-translate', 'item' => 'markets']) ?>" class="list-group-item <?= ($get == 'markets') ? 'active' : '' ?>">
								<?= Yii::t('games', 'Markets') ?> <span class="badge"><?= $dataItems['markets'] ?></span>
							</a>
						<?php endif; ?>

						<?php if($dataItems['outcomes'] != 0): ?>
							<a href="<?= Url::to(['verify-translate', 'item' => 'outcomes']) ?>" class="list-group-item <?= ($get == 'outcomes') ? 'active' : '' ?>">
								<?= Yii::t('games', 'Outcomes') ?> <span class="badge"><?= $dataItems['outcomes'] ?></span>
							</a>
						<?php endif; ?>

						<?php if($dataItems['tournament'] != 0): ?>
							<a href="<?= Url::to(['verify-translate', 'item' => 'tournament']) ?>" class="list-group-item <?= ($get == 'tournament') ? 'active' : '' ?>">
								<?= Yii::t('games', 'Tournaments') ?> <span class="badge"><?= $dataItems['tournament'] ?></span>
							</a>
						<?php endif; ?>

						<?php if($dataItems['games'] != 0): ?>
							<a href="<?= Url::to(['verify-translate', 'item' => 'games']) ?>" class="list-group-item <?= ($get == 'games') ? 'active' : '' ?>">
								<?= Yii::t('games', 'Games') ?> <span class="badge"><?= $dataItems['games'] ?></span>
							</a>
						<?php endif; ?>

						<?php if($dataItems['participants'] != 0): ?>
							<a href="<?= Url::to(['verify-translate', 'item' => 'participants']) ?>" class="list-group-item <?= ($get == 'participants') ? 'active' : '' ?>">
								<?= Yii::t('games', 'Participants') ?>
								<span class="badge"><?= $dataItems['participants'] ?></span>
							</a>
						<?php endif; ?>
					</div>
				</div>
				<div class="col-sm-9">
					<?php
					echo ListView::widget([
						'dataProvider' => $dataProvider,
						'itemView' => '_parts-verify-translate/_' . $get,
						'viewParams' => ['get' => $get]
					]);
					?>
				</div>
			</div>
		</div>
	</div>
</div>
</div>











<?php

/* @var $this \yii\web\View */

use kartik\form\ActiveForm;
use yii\helpers\Html;

?>
<div class="box box-primary" style="min-height: 400px;padding:15px">
	<h1>Команды</h1>
	<?php $form = ActiveForm::begin() ?>
	<div class="row">
		<div class="col-md-6">
			<?= $form->field($model, 'game_sport')->dropDownList($sportList, ['prompt' => 'Please select']) ?>
		</div>
		<div class="col-md-6">
			<?= $form->field($model, 'file')->fileInput(['accept' => '.csv']) ?>
		</div>
	</div>
	<div class="text-center" style="margin-top: 15px">
		<?= Html::submitButton('Load',['class' => 'btn btn-primary']) ?>
	</div>
	<?php ActiveForm::end() ?>
	<div class="" style="margin-top: 15px">
		<?php
		if(!empty($result)){
			echo $result;
		}
		?>
	</div>
</div>


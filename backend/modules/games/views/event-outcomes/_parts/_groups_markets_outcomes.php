<?php

/* @var $data \common\modules\games\models\GameMarketsGroupsLocaleModel|null */

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\EventOutcomesModel;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/* @var $this \yii\web\View */
/* @var $participants array */
/* @var $game \common\modules\games\models\EventGameModel */
/* @var $outcomesList array|EventOutcomesModel[] */

$collapse_in = (Yii::$app->request->get('group_id') != 'all') ? 'in' : '';
?>

<style>
	.td-status, .td-hidden, .td-finished, .td-ban {
		text-align: center;
		width: 70px;
	}

	.td-title {
		text-align: left;
	}

	input[type="number"] {
		text-align: center;
		width: 60px;
		margin: 0 8px;
	}

	.switch {
		cursor: pointer;
		color: #367fa9;
		font-size: 10px;
	}

	.show-all {
		font-style: italic;
		display: none;
	}

	div.button-add-outcome,
	div.select-outcome{
		display: inline-block;
		vertical-align: top;
	}

	div.select-outcome{
		min-width: 200px;
	}
</style>

<?php if(!empty($data)): ?>
	<?php $form = ActiveForm::begin(['action' => \yii\helpers\Url::to(['update', 'game_id' => $game->id, 'group_id' => Yii::$app->request->get('group_id') ?? '', 'market_id' => Yii::$app->request->get('market_id') ?? ''])]) ?>
	<p class="text-right"><?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-primary']) ?></p>
	<?php foreach($data as $group): ?>
		<!-- Market groups  -->
		<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			<div class="panel panel-default">
				<div class="panel-heading" role="tab" id="heading<?= $group->id ?>">
					<h4 class="panel-title">
						<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $group->id ?>" aria-expanded="true" aria-controls="collapse<?= $group->id ?>">
							<span class="glyphicon <?= ($collapse_in) ? 'glyphicon-minus' : 'glyphicon-plus'; ?>"></span>
							<strong><?= Yii::t('games', 'Group') ?>:</strong> &nbsp; <?= GameHelpers::getStringForLocale($group->gameMarketsGroupsLocales, 'title', false, true) ?>
						</a>
					</h4>
				</div>
				<div id="collapse<?= $group->id ?>" class="panel-collapse collapse <?= $collapse_in ?>" role="tabpanel" aria-labelledby="heading<?= $group->id ?>"><!-- class="panel-collapse collapse in" -->
					<div class="panel-body">
						<!-- Markets -->
						<?php if(!empty($group->gameMarkets)): ?>
							<?php foreach($group->gameMarkets as $market): ?>
								<table class="table table-hover">
									<thead>
									<tr>
										<th class="td-title">
											<strong><?= Yii::t('games', 'Market') . ':</strong> &nbsp; ' . GameHelpers::getStringForLocale($market->gameMarketsLocales, 'title', false, true) ?>
												<?= Html::a('&nbsp; - &nbsp;' . Yii::t('games', 'show all'), '#', ['class' => 'show-all', 'id' => 'market_' . $market->id]) ?>
										</th>
										<th class="td-status"><?= Yii::t('games', 'Is enabled') ?> <br>
											<span class="switch" data-marker='1'><?= Yii::t('games', 'on') ?></span> /
											<span class="switch" data-marker=''><?= Yii::t('games', 'off') ?></th>
										<th class="td-hidden"><?= Yii::t('games', 'Is hidden') ?> <br>
											<span class="switch" data-marker='1'><?= Yii::t('games', 'on') ?></span> /
											<span class="switch" data-marker=''><?= Yii::t('games', 'off') ?></th>
										<th class="td-finished"><?= Yii::t('games', 'Is finished') ?> <br>
											<span class="switch" data-marker='1'><?= Yii::t('games', 'on') ?></span> /
											<span class="switch" data-marker=''><?= Yii::t('games', 'off') ?></th>
										<th class="td-ban"><?= Yii::t('games', 'Is banned') ?><br> &nbsp;</th>
									</tr>
									</thead>
									<tbody>
									<!-- Outcomes -->
									<?= $this->render('_event_outcome', ['market' => $market, 'outcomesList' => $outcomesList, 'participants' => $participants]) ?>
									<!-- ./Outcomes -->
									</tbody>
									<tfoot>
									<tr>
										<td colspan="5">
											<div class="select-outcome" style="display: none">
												<?= Select2::widget([
													'id' => 'select_' . $market->id,
													'disabled' => true,
													'name' => 'add_outcome',
													'data' => [],
													'size' => Select2::SMALL,
													'options' => ['placeholder' => 'Select a outcome:'],
													'showToggleAll' => false,
													'pluginOptions' => [
														'allowClear' => true,
														'multiple' => true,
													],
												]);
												?>
											</div>
											<div class="button-add-outcome">
												<?= Html::button(Yii::t('games','Add outcome'), ['class' => 'btn btn-default add-outcome btn-sm', 'data-id' => $market->id, 'data-game_id' => $game->id]) ?>
											</div>
										</td>
									</tr>
									</tfoot>
								</table>
							<?php endforeach; ?>
						<?php endif; ?>
					</div>
				</div>
			</div>
		</div>
	<?php endforeach; ?>
	<div class="text-center"><?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-primary']) ?></div>
	<?php ActiveForm::end() ?>
<?php endif; ?>
<?php
$js = <<<JS

$('span.switch').on('click',function() { 
	var list =  $(this).parents('table');
	var marker = $(this).attr('data-marker');
	var statusName = $(this).parent('th').attr('class');
  	list.find('td.'+ statusName +' input[type="checkbox"]').map(function(idx,el) {
  	  $(el).prop("checked", marker);
  	})
});

/* ADD OUTCOME */

$('button.add-outcome').on('click',function() {
	var button = $(this);
	var market_id = button.attr('data-id');
	var game_id = button.attr('data-game_id');
	console.log(game_id);
 	var select = $('select#select_' + market_id);
	var div_select_outcome = button.parent('div.button-add-outcome').siblings('div.select-outcome');
 	if($('select#select_' + market_id)[0].options.length == 0 ){
 		console.log('show');
 		div_select_outcome.show().animate({'width':500},500);
 		button.html('<span class="fa fa-plus"></span>');
 		$.ajax({
  			url: '/games/event-outcomes/get-outcomes-list',
  			type:'POST',
  			data:{market:market_id,game:game_id},
  			success: function(data) {
  			  if(data){
  			  	var options = $.map(data,function(title,value) {
  			  	  return '<option value="' + value + '">'+ title +'</option>';
  			  	});
  			  	options.join(' ');
  			  	$(select).append(options); // added options in select
		  	 	select.removeAttr('disabled'); 
  			  }else{
  			  		alert('Нет больше исходов для этого маркета.');	
  			  }
  			}
  		});	
 	}else{
 		console.log('select');
 		var values = select.val();
 		if(values.length > 0){
 			values.map(function(val,idx) {
 			  addNewEvent(select,val);
 			})
 		}else{
 			alert('Please, select outcome');
 		}
 	}
});

	
	function addNewEvent(select,val){
		var option = $(select.find('option[value=' + val + ']'));
		var title = option.text();
		if(title){
			var row = '<tr class="new-outcome"><td class="td-title"><input type="number" min="1" step="0.001" name="rates[' + val + ']">' + title + '</td><td class="td-status"><input type="checkbox" name="statuses[' + val + ']"></td><td class="td-hidden"></td><td class="td-finished"></td><td class="td-ban"></td></tr>'; 
			select.parents('tr').before(row);
			option.remove();
		}else{
			return false;
		}
	}
JS;

$this->registerJs($js, $this::POS_READY);
/* Modal form */
echo $this->render('/_parts/_banned_form.php', ['title' => 'Are you sure you want to ban this outcome?']);
?>



<?php
/* @var $market */
/* @var $this \yii\web\View */
/* @var $outcomesList array|\common\modules\games\models\EventOutcomesModel[] */

/* @var $participants array */

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\EventOutcomesModel;
use kartik\select2\Select2;
use yii\helpers\Html;

?>
<?php $i = 1; ?>
<?php if(!empty($market->gameOutcomes)): ?>

	<?php foreach($market->gameOutcomes as $outcome): ?>
		<?php if(!empty($outcomesList[$outcome->id])): ?>
			<!-- # saved event # -->
			<tr>
				<!-- title and rate  -->
				<td class="td-title">
					<input type="number" min=1 step=0.001 name="rates[<?= $outcome->id ?>]" value= <?= $outcomesList[$outcome->id]['rate'] ?>>
					<?php $title = \backend\modules\games\components\GameHelpers::getStringForLocale($outcome->gameOutcomesLocales, 'title', false, true) ?>
					<?= \backend\modules\games\components\GameHelpers::replacingCodeWithName($title, $participants) ?>
				</td>
				<!-- Outcome status  -->
				<td class="td-status">
					<input type="checkbox" name="statuses[<?= $outcome->id ?>]" <?= ($outcomesList[$outcome->id]['is_enabled'] == EventOutcomesModel::STATUS_ENABLED) ? 'checked' : '' ?>>
				</td>
				<!-- Outcome hidden -->
				<td class="td-hidden">
					<input type="checkbox" name="hidden[<?= $outcome->id ?>]" <?= ($outcomesList[$outcome->id]['is_hidden'] == EventOutcomesModel::STATUS_ENABLED) ? 'checked' : '' ?>>
				</td>
				<!-- Outcome finished -->
				<td class="td-finished">
					<?php if($outcomesList[$outcome->id]['is_finished']): ?>
						<span class="text-warning"> <?= Yii::t('games', 'Is finished') ?> </span>
					<?php else: ?>
						<input type="checkbox" name="finished[<?= $outcome->id ?>]">
					<?php endif ?>
				</td>
				<!-- Outcome ban -->
				<td class="td-ban">
					<?php
					if($outcomesList[$outcome->id]['is_banned']){
						echo '<span class="text-danger">' . Yii::t('games', 'Is banned') . '</span>';
					}else{
						echo Html::button(Yii::t('games', 'Ban'), ['class' => 'btn btn-danger btn-ban', 'data-id' => $outcomesList[$outcome->id]['id']]);
					}
					?>
				</td>
			</tr>
		<?php endif; ?>
	<?php endforeach; ?>
<?php endif; ?>


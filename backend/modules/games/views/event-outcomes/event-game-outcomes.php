<?php
/**
 * @var $this \yii\web\View
 * @var $game \common\modules\games\models\EventGameModel;
 * @var $groupsList array
 */
/* @var $group_id null */
/* @var $market_id int|null */
/* @var $marketsList array */
/* @var $data null| \common\modules\games\models\GameMarketsGroupsLocaleModel */
/* @var $participants array */
/* @var $outcomesList array|\common\modules\games\models\EventOutcomesModel[] */

use backend\modules\games\components\GameHelpers;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

//$this->registerCssFile('/static/css/games.css');
$gameName = GameHelpers::getStringForLocale($game->eventGameLocales, 'title');

/*$this->title = Yii::t('games', 'Outcomes for:') . ' ' . $gameName;
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['game-sports/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Tournaments'), 'url' => ['event-tournament/index', 'sport_id' => $game->sports_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Game'), 'url' => ['event-game/update', 'game_id' => $game->id]];
$this->params['breadcrumbs'][] = $this->title;*/
echo $this->render('/_parts/_breadcrumbs.php',['location' => Yii::t('games','Update outcomes'),'parent' => 'EventGame','parent_id' => $game->id]);
?>
<script>
	function addLink(id){
		document.getElementById(id).style.display = 'inline-block';
	}


</script>
	<div class="box box-primary">
		<div class="box-header with-border" style="min-height:43px;">
			<div class="box-tools pull-right">
				<?= Html::a('<span class="fa fa-reply"></span> &nbsp;' . $gameName, Url::to(['event-game/update', 'game_id' => $game->id]), ['class' => 'btn btn-warning']) ?>
			</div>
			<!-- /.box-tools -->
		</div>
		<!-- /.box-header -->
		<div class="box-body" style="padding:15px">
			<!-- Filter -->
			<div class="col-sm-3" style="padding-right: 5px; border-right: 1px solid #f4f4f4;">
				<?php $form = ActiveForm::begin(['method' => 'GET', 'action' => Url::to('event-game-outcomes?game_id=' . $game->id)]) ?>
				<!-- Market Groups -->
				<div class="">
					<?php //$groupsList = ['all' => Yii::t('games','All groups')] + $groupsList; ?>
					<h4><span class="fa fa-eye"></span> <?= Yii::t('games', 'Market groups') ?>:</h4>
					<?= Select2::widget([
						'name' => 'group_id',
						'id' => 'select_markets_group',
						'value' => $group_id,
						'data' =>  $groupsList,
						'options' => ['placeholder' => 'Please select...'],
						'pluginOptions' => [
							'tags' => true,
							'allowClear' => true,
						],
						'pluginEvents' => [
							'change' => 'groupClearLists',
						],
					]); ?>
				</div>
				<!-- Markets -->
				<div class="">
					<h4><span class="fa fa-eye"></span> <?= Yii::t('games', 'Markets') ?>:</h4>
					<?= Select2::widget([
						'name' => 'market_id',
						'id' => 'select_market',
						'value' => $market_id,
						'data' => $marketsList,
						'options' => ['placeholder' => 'Please select...'],
						'pluginOptions' => [
							'tags' => true,
							'allowClear' => true,
						],
					]); ?>
				</div>
				<?= Html::submitButton('<span class="fa fa-filter"></span>', ['class' => 'btn btn-primary', 'style' => 'width:100%; margin-top:10px']) ?>
				<?php ActiveForm::end() ?>
			</div>
			<!-- content -->
			<div class="col-sm-9">
				<?= $this->render('_parts/_groups_markets_outcomes',['data' => $data,'participants' => $participants,'game' => $game,'outcomesList' => $outcomesList]) ?>
			</div>

		</div>
	</div>

<?php
$js = <<<JS
	var group_id = $("#select_markets_group").val();
	var select_market = $("#select_market");
	
	if(group_id == '' || group_id == 'all'){
		select_market.attr('disabled',true)
	}
	
	function groupClearLists() {
		group_id = $("#select_markets_group").val();
		select_market.val(false);
		select_market.html("");
		select_market.attr('disabled',true);
		
		if(group_id == '' || group_id == 'all'){
			return true;
		}
		
		$.ajax({
			type:'POST',
			url:'/games/event-outcomes/get-markets-list',
			data:{id:group_id},
			success:function(data) {
				select_market.attr('disabled',false)
			  	select_market.html(data)
			}
		})
	}
JS;
$this->registerJs($js, $this::POS_READY);
?>
<?php

/* @var $this \yii\web\View */
/* @var $title string */
?>
<?php
use kartik\form\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* !!! MODAL FORM FOR EVENT TOURNAMENT, EVENT GAME, EVENT OUTCOME !!! */
$model = new \backend\modules\games\forms\BanForm();

Modal::begin([
	'header' => '<h3>' . Yii::t('games',$title) . '</h3>',
	'headerOptions' => ['class' => 'bg-red-active'],
	'id' => 'modal-form',
]);

/* Content */
$form = ActiveForm::begin(['action' => Url::to(['set-ban']), 'id' => 'form-ban']);
echo Html::hiddenInput('item_id', null, ['id' => 'item-id']);
echo $form->field($model, 'comment')->textarea(['rows' => 6])->label(Yii::t('bets', 'Comment'));
echo '<br /><div class="text-right">' . Html::button(Yii::t('bets', 'Close'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) . ' ' . Html::submitButton(Yii::t('games', 'Ban'), ['class' => 'btn btn-danger']) . '</div>';
ActiveForm::end();
/* ./Content */

Modal::end();

$js = <<<JS
	$(document).on('click','button.btn-ban',function() {
		var id = $(this).attr('data-id');
		$('#item-id').val(id);
		$('#modal-form').modal('show');
	});
		$('#modal-form').on('hide.bs.modal', function (e) {
		document.getElementById("form-ban").reset();
	});
JS;
$this->registerJs($js, $this::POS_READY);


<?php
/* @var $this \yii\web\View */
/* @var $location string */
/* @var $parent bool|string */
/* @var $parent_id bool|int */
?>

<?php $this->beginBlock('gameBreadcrumbs'); ?>
<?php $this->registerCssFile('/static/css/games.css', ['depends' => \backend\assets\MainAsset::class]); ?>
	<ul id="breadcrumbs-game">
		<li><a href="/"><span class="title-box"><?= Yii::t('games', 'Home') ?></span></a></li>
		<?= (Yii::createObject([
			'class' => \backend\modules\games\components\GameBreadcrumbs::class,
			'parent' => $parent,
			'parent_id' => $parent_id
		]))->getBreadcrumbs() ?>
		<li><a href="#" class="current"><?= $location ?></a></li>
	</ul>
<?php $this->endBlock('gameBreadcrumbs'); ?>
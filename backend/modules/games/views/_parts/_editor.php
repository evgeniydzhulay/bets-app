<?php
/* @var $form static */
/* @var $this \yii\web\View */
/* @var $model \common\modules\games\models\GameSportsLocaleModel */
/* @var $attribute string */

use dosamigos\ckeditor\CKEditor;

echo $form->field($model, $attribute)->widget(CKEditor::class, [
	'options' => ['rows' => 6],
	'preset' => 'basic'
]);
//$form->field($model,$attribute)->textarea(['rows' => 6]);

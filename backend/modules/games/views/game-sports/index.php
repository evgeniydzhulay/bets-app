<?php

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\GameSportsModel;
use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\games\models\GameSportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

echo $this->render('/_parts/_breadcrumbs.php', ['location' => Yii::t('games', 'Games'), 'parent' => false, 'parent_id' => false]);
?>
<style>
	.btn-group .btn + .btn {
		margin-left: 1px;
	}
</style>
<div class="game-sports-model-index">
	<div class="box-body table-responsive no-padding">
		<?php
		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'pjax' => true,
			'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
			'toolbar' => [
				[
					'content' =>
						\backend\modules\games\widgets\NotificationButtons::widget() .
						Html::a('<span class="fa fa-refresh"></span>', Url::to(['game-sports/index']), [
							'class' => 'btn btn-warning',
							'title' => Yii::t('games', 'Reset filter')
						])
						. ' ' .
						Html::a('<span class="fa fa-plus"></span>', Url::to(['game-sports/create']), [
							'class' => 'btn btn-default',
							'data-pjax' => 0,
							'title' => Yii::t('games', 'Add game')
						])
				]
			],

			'columns' => [
				[
					'attribute' => 'id',
					'headerOptions' => ['width' => '80px'],
				],
				[
					'label' => Yii::t('games', 'Title'),
					'attribute' => 'sport_title',
					'headerOptions' => ['width' => '20%'],
					'value' => function ($data) {
						return GameHelpers::getStringForLocale($data->gameSportsLocales, 'title', false, true);
					}
				],
				[
					'label' => '',
					'headerOptions' => ['style' => 'width:30px'],
					'format' => 'raw',
					'value' => function($data){
						return ($data->image) ? Html::img('/uploads/games/' . $data->image,['style' => 'height:20px']) : '';
					}
				],
				[
					'attribute' => 'is_enabled',
					'filter' => GameSportsModel::statuses(),
					'format' => 'raw',
					'value' => function ($data) {
						return ($data->is_enabled) ?
							Html::a(Yii::t('games', 'Disable'), Url::to(['change-status', 'id' => $data->id, 'status' => GameSportsModel::STATUS_DISABLED]), ['class' => 'btn btn-danger btn-block btn-xs', 'data' => ['pjax' => 0]])
							: Html::a(Yii::t('games', 'Enable'), Url::to(['change-status', 'id' => $data->id, 'status' => GameSportsModel::STATUS_ENABLED]), ['class' => 'btn btn-success btn-block btn-xs', 'data' => ['pjax' => 0]]);
					},
					'headerOptions' => ['width' => '120px'],
				],
				[
					'attribute' => 'sort_order',
					'filter' => false,
					'headerOptions' => ['width' => '150px']
				],
				/*[
					'label' => Yii::t('games', 'Description'),
					'value' => function ($data) {
						return strip_tags(GameHelpers::getStringForLocale($data->gameSportsLocales, 'description', 120));
					}
				],*/
				[
					'content' => function ($data) {
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-success',
							'title' => Yii::t('games', 'Update')
						]);
					},
					'headerOptions' => ['style' => 'width:20px'],
				],
				[
					'content' => function ($data) {
						return Html::a(Yii::t('games', 'Market groups'), ['game-markets-groups/index', 'sport_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-primary'
						]);
					},
					'headerOptions' => ['style' => 'width:120px']
				],
				[
					'content' => function ($data) {
						return Html::a(Yii::t('games', 'Participants'), ['game-participants/index', 'sport_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs bg-maroon-active'
						]);
					},
					'headerOptions' => ['style' => 'width:120px']
				],
				[
					'content' => function ($data) {
						return Html::a(Yii::t('games', 'Tournaments'), ['event-tournament/index', 'sport_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs bg-purple-active'
						]);
					},
					'headerOptions' => ['style' => 'width:120px']
				]
			],
		]);

		?>
	</div>
</div>

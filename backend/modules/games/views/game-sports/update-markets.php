<?php
/**
 * @var $this \yii\web\View
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

$this->title = Yii::t('games', 'Update market');
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['/games/game-sports/index']];;
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">
			<?= Html::a(Yii::t('games', 'Close'), Url::to(['index']), ['class' => 'btn btn-warning']) ?>
		</div>
		<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body" style="padding:15px">
		<?php if(!empty($data)): ?>

			<?php foreach($data as $sport_id => $sport): ?>
				<a name="s<?= $sport_id ?>"></a>
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="box box-success">
							<div class="box-header" style="background-color: #e4ffd2">
								<h3 class="box-title"><?= $sport['sport_title'] ?></h3>
							</div>
							<!-- /.box-header -->
							<div class="box-body table-responsive no-padding">
								<?= Html::beginForm() ?>
								<table class="table table-hover">
									<tbody>
									<tr>
										<th style="width: 50%"><?= Yii::t('games', 'Title') ?></th>
										<th><?= Yii::t('games', 'Group') ?></th>
									</tr>
									<?php foreach($sport['items'] as $market_id => $title): ?>
										<tr>
											<td><?= $title ?></td>
											<td>
												<?= Select2::widget([
													'name' => 'items[' . $market_id . ']',
													'data' => $sport['group_list'],
													'pluginOptions' => [
														'placeholder' => 'Please select',
														'allowClear' => true
													]
												]) ?>

											</td>
										</tr>
									<?php endforeach; ?>
									</tbody>
								</table>
								<div class="text-center" style="margin-bottom: 15px"><?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-primary']) ?></div>
								<?= Html::endForm() ?>
							</div>
							<!-- /.box-body -->
						</div>
						<!-- /.box -->
					</div>
				</div>

			<?php endforeach; ?>

		<?php else: ?>
			<h3><?= Yii::t('games', 'Markets not found') ?></h3>
		<?php endif; ?>
	</div>
</div>




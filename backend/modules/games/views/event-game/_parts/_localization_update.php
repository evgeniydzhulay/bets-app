<?php

/* @var $this \yii\web\View */

use backend\modules\games\components\GameHelpers;
use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model \common\modules\games\models\GameMarketsGroupsModel */
/* @var $searchModel  */
/* @var $dataProvider \yii\data\ArrayDataProvider */
?>
	<h4><span class="fa fa-language text-warning"></span> <?= Yii::t('games', 'Localization:') ?></h4>

<?php
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'toolbar' => [
		['content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['event-game/update', 'game_id' => $model->id]), ['class' => 'btn btn-warning'])],
	],
	'columns' => [
		[
			'attribute' => 'id',
			'headerOptions' => ['width' => '80px'],
		],
		[
			'attribute' => 'title',
		],
		[
			'attribute' => 'lang_code',
			'headerOptions' => ['width' => '80px'],
		],
		[
			'attribute' => 'description',
			'filter' => false,
			'value' => function ($data) {
				return strip_tags(GameHelpers::truncate($data->description, 80));
			}
		],
		[
			'attribute' => 'meta_keywords',
			'filter' => false,
			'value' => function ($data) {
				return GameHelpers::truncate($data->meta_keywords, 80);
			}
		],
		[
			'attribute' => 'meta_description',
			'filter' => false,
			'value' => function ($data) {
				return GameHelpers::truncate($data->meta_description, 80);
			}
		],
		[
			'class' => 'yii\grid\ActionColumn',
			'template' => '{locale-update}',
			'buttons' => [
				'locale-update' => function($url, $model, $key){
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,['data-pjax' => '0']);
				}
			]
		],
	],
]);


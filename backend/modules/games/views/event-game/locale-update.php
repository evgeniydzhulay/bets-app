<?php

/* @var $this \yii\web\View */
/* @var $model \common\modules\games\models\EventGameLocaleModel */

use kartik\form\ActiveForm;
use yii\helpers\Html;



/*$this->title = Yii::t('games', 'Games') . ': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Sport'), 'url' => ['game-sports/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Tournaments'), 'url' => ['event-tournament/index','sport_id' => $model->sports_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['event-game/index','tournament_id' => $model->tournament_id]];
$this->params['breadcrumbs'][] = $model->title;*/

echo $this->render('/_parts/_breadcrumbs.php',['location' => Yii::t('games','Update'),'parent' => 'EventGame','parent_id' => $model->game_id]);
?>
<div class="event-game-model-update box box-default">

	<div class="box-header with-border">
		<h3 class="box-title"><?= Yii::t('games', 'Game') ?>:
			<span class="text-red"><?= $model->title ?></span> &nbsp; <?= Yii::t('games', 'Language') ?>:
			<span class="text-red"> <?= $model->lang_code ?></span></h3>
	</div>
	<div class="box-body">
		<!-- content -->
		<div class="text-right">
			<?= Html::a(Yii::t('games','Close'), \yii\helpers\Url::to(['update','game_id' => $model->game_id]), ['class' => 'btn btn-md btn-warning']) ?>
		</div>
		<?php $form = ActiveForm::begin() ?>
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'title')->textInput() ?>
			</div>
		</div>
		<?= $this->render('/_parts/_editor.php', ['form' => $form, 'attribute' => 'description', 'model' => $model]) ?>
		<?= $form->field($model, 'meta_keywords')->textarea(['rows' => 6]) ?>
		<?= $form->field($model, 'meta_description')->textarea(['rows' => 6]) ?>

		<div class="text-right">
			<?= Html::submitButton(Yii::t('games','Update'), ['class' => 'btn btn-primary']) ?>
		</div>
		<?php ActiveForm::end() ?>

	</div>

</div>

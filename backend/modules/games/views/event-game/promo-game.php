<?php
/**
 * @var $model \common\modules\games\models\EventGameModel
 * @var $this \yii\web\View
 * */

use kartik\form\ActiveForm;
use kartik\select2\Select2;


?>
<style>
	.select2-container .select2-selection--single .select2-selection__rendered {
		padding-top: 5px;
	}

	.outcomes-cx {
		padding: 5px 25px;
		min-height: 200px;
	}

	.outcomes-cx label {
		margin-left: 15px;
		font-size: 16px;
	}
</style>
<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<?= \yii\helpers\Html::a(Yii::t('games', 'Close'), Yii::$app->request->referrer, ['class' => 'btn btn-warning pull-right']) ?>
		<h4><?= Yii::t('games', 'Game') . ': ' . $game->eventGameLocales[0]->title ?></h4>
	</div>
	<!-- /.box-header -->
	<div class="box-body" style="min-height:400px;">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<h3><?= Yii::t('games', 'Markets') ?>:</h3>
					<?php
					echo Select2::widget([
						'name' => 'markets',
						'data' => $markets,
						'options' => [
							'placeholder' => 'Select market...',
							'multiple' => false
						],
						'pluginEvents' => [
							"change" => "function(e) { 
								var selected_val = $(e.target).val();
								if(selected_val){
									$.ajax({
										type:'post',
										url:'/games/event-game/get-outcomes',
										data:{market:selected_val,game:" . $game->id . "},
										dataType:'json',
										errors: function(){console.log('caught error');},
										success: function(data){
											if(data){
												$('.outcomes-cx').html(data)
											}
										}
									});
								}
							}",
						]
					]);
					?>
				</div>
				<div class="col-sm-8">
					<?php $form = ActiveForm::begin(['action' => 'cud-promo-game']) ?>
					<input type="hidden" value="<?= $game->id ?>" name="game_id">
					<h3><?= Yii::t('games', 'Outcomes') ?>:</h3>
					<div class="outcomes-cx"><?= $outcomes ?></div>
					<div class="text-right">
						<?= (!empty($promo->id)) ? \yii\helpers\Html::a(Yii::t('games', 'Delete'), ['cud-promo-game', 'id' => $promo->id], [
							'class' => 'btn btn-danger',
							'data' => [
								'confirm' => Yii::t('games', 'Are you sure you want to delete this promo game?'),
								''
							]
						]) : '' ?>
						<?= \yii\helpers\Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-primary', 'id' => 'btn-submit']) ?></div>
					<?php ActiveForm::end() ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
$js = <<<JS
	$('#btn-submit').on('click',function(e) { console.log(111);
	   var count = 0;
		$('div.outcomes-cx input:checkbox:checked').map(function(){
			count++;
		});
		if(count !== 2){
			e.preventDefault();
			alert('Выберите два исхода!');
			return false;
		}
	})
JS;
$this->registerJs($js);
?>

<?php

/* @var $gameParticipantsArray array */
?>
<?php

/* @var $participantsList string */
?>
<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\modules\games\models\EventGameModel
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $searchModel \common\modules\games\models\EventGameLocaleModel
 */

use common\modules\games\models\GameParticipantsLocaleModel;
use kartik\datetime\DateTimePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/*$this->title = $model->isNewRecord ? Yii::t('games', 'Add game') : Yii::t('games', 'Update game');
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['game-sports/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Tournaments'), 'url' => ['event-tournament/index', 'sport_id' => $model->sports_id]];
$this->params['breadcrumbs'][] = $this->title;
$this->registerCssFile('/static/css/games.css');*/
$location = $model->isNewRecord ? Yii::t('games', 'Add game') : Yii::t('games', 'Update game');
echo $this->render('/_parts/_breadcrumbs.php',['location' => $location,'parent' => 'EventTournament','parent_id' => $model->tournament_id]);
?>
	<style>
		.event-game ul#participants-list {
			padding: 0;
		}

		.event-game ul#participants-list li {
			list-style: none;
			padding: 5px 0 5px 15px;
			margin-bottom: 2px;
			border: 1px solid #ddd;
			background-color: #f4f4f4;
			border-radius: 3px;
			cursor: move;
			position: relative;
		}

		.event-game ul#participants-list li span.remove-participant{
			position: absolute;
			color: #ddd;
			right: 8px;
			top: 8px;
			cursor: pointer;
		}
		.event-game ul#participants-list li span.remove-participant:hover{
			position: absolute;
			color: #ff5654;
			right: 8px;
			top: 8px;
			cursor: pointer;
		}

	</style>

	<div class="box box-primary event-game">
		<div class="box-header with-border" style="min-height:43px;">
			<div class="box-tools pull-right">
				<?= (!$model->isNewRecord) ? Html::a('<span class="fa fa-sitemap"></span> ' . Yii::t('games', 'Outcomes'), Url::to(['event-outcomes/event-game-outcomes', 'game_id' => $model->id]), ['class' => 'btn btn-primary']) : '' ?>
				<?= Html::a(Yii::t('games', 'Close'), Url::to(['index', 'tournament_id' => $model->tournament_id]), ['class' => 'btn btn-warning']) ?>
			</div>
			<!-- /.box-tools -->
		</div>
		<!-- /.box-header -->
		<div class="box-body" style="padding:15px">
			<div class="row">
				<!-- block left -->
				<div class="col-sm-3">
					<?php $form = ActiveForm::begin() ?>
					<h4><span class="fa fa-gear text-warning"></span> <?= Yii::t('games', 'Settings:') ?></h4>
					<div class="row">
						<div class="col-sm-6">
							<?php $data = [Yii::t('games', 'Disabled'), Yii::t('games', 'Enabled')]; ?>
							<?php if($model->isNewRecord) $model->is_enabled = 1 ?>
							<label class="control-label has-star" for=""><?= Yii::t('games', 'Status') ?></label>
							<?= $form->field($model, 'is_enabled')->radioButtonGroup($data, ['itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']]])->label(false) ?>
						</div>
						<div class="col-sm-6">
							<?php $data = [Yii::t('games', 'No'), Yii::t('games', 'Yes')]; ?>
							<?php if($model->isNewRecord) $model->is_hidden = 0 ?>
							<label class="control-label has-star" for=""><?= Yii::t('games', 'Is hidden') ?></label>
							<?= $form->field($model, 'is_hidden')->radioButtonGroup($data, ['itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']]])->label(false) ?>
						</div>
					</div>
					<div class="">
						<?= $form->field($model, 'starts_at')->widget(DateTimePicker::class, [
							'options' => [
								'value' => Yii::$app->formatter->asDate($model->starts_at ?? time(), 'php:d/m/Y H:i'),
							],
							'type' => DateTimePicker::TYPE_INPUT,
							'pluginOptions' => [
								'autoclose' => true,
								'format' => 'dd/mm/yyyy hh:ii',
							]
						]); ?>
					</div>
					<!-- Participants -->
					<?php if($model->isNewRecord || !empty($participantsList)): ?>
					<label class="control-label"><?= Yii::t('games', 'Participants') ?>:</label>
					<div style="border-radius: 3px;border: 1px solid #468847;padding: 5px;margin-bottom: 5px">
						<div class="" style="min-height: 100px; padding-bottom: 5px">
							<!-- Participants list -->
							<ul id="participants-list"><?= $participantsList ?? '' ?></ul>
						</div>
						<?= Select2::widget([
							'options' => ['placeholder' => Yii::t('games', 'Please select a participant')],
							'name' => 'participants-select2',
							'id' => 'select-2',
							'showToggleAll' => false,
							'pluginOptions' => [
								'tags' => false,
								'allowClear' => true,
								'minimumInputLength' => 3,
								'ajax' => [
									'url' => Url::to(['event-game/get-participants-list']),
									'dataType' => 'json',
									'data' => new JsExpression('function(params) { return {string: params.term, sport_id: '. $model->sports_id .' }}')
								],
							],
							'addon' => [
								'append' => [
									'content' => Html::button('<span class="fa fa-plus"></span>', [
										'class' => 'btn btn-default',
										'title' => Yii::t('games', 'Add participant'),
										'data-toggle' => 'tooltip',
										'id' => 'add-item'
									]),
									'asButton' => true
								]],
						]);
						?>
					</div>
					<?php endif; ?>
					<!-- ./Participants -->
					<div>
						<?= $form->field($model,'video_link')->textInput() ?>
					</div>
					<div class="">
						<?= $form->field($model, 'sort_order')->textInput(['style' => 'width:120px']) ?>
					</div>
					<div class="">
						<?php if(!$model->isNewRecord): ?>
						<label class="control-label has-star" for="">&nbsp;</label><br/>
						<?= (!$model->is_finished) ? Html::a(Yii::t('games', 'Finish game'), Url::to(['set-finished-game', 'id' => $model->id]), ['class' => 'btn btn-warning', 'data' => [
							'confirm' => Yii::t('games', 'Are you sure you want to finish this game?'),
							//'method' => 'post',
						]]) : '' ?>
						<?= (!$model->is_banned) ? Html::button(Yii::t('games', 'Ban'), ['class' => 'btn btn-danger btn-ban', 'data-id' => $model->id]) : '' ?>
					</div>
				<?php endif; ?>
					<hr/>
					<!-- For insert -->
					<?= $this->render('_parts/_localization_create.php', ['model' => $model]) ?>
					<div class="text-center">
						<?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-primary', 'style' => 'width:100%;margin: 25px 0']) ?>
					</div>
					<?php ActiveForm::end() ?>
				</div><!-- ./col-sm-3 -->
				<!-- right -->
				<div class="col-sm-9">
					<!-- For update -->
					<?= !$model->isNewRecord ? $this->render('_parts/_localization_update.php', ['model' => $model, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel]) : '' ?>
				</div>
			</div>
		</div>
	</div>
<?= $this->render('/_parts/_banned_form.php', ['title' => 'Are you sure you want to bane this game?']) ?>


<?php
$js = <<<'JS'
	/* Sorter */
	$( function() {
		$( "ul#participants-list" ).sortable();
	});
	
	/* add participant */
	$('#add-item').on('click',function() {
		var participant_id = $('#select-2').val();
		var participant_name = $('#select-2 option:selected').text();
		var checkParticipant = false
		
		/* check for repetition */
		$('ul#participants-list li input').map(function(idx,input) {
		  	if($(input).val() == participant_id){
		  		alert('Participant already selected!');
		  		checkParticipant = true;
		  	}
		});
		if(checkParticipant){
			return false;
		}
		
		/* add participant in form */
		if(participant_id && participant_name){
			var item = '<li>' + participant_name + '<input type="hidden" value="' + participant_id + '" name="participants[' + participant_id + ']"><span class="remove-participant glyphicon glyphicon-remove"></span></li>';
			$('#participants-list').append(item);
		}
	});
	/* remove participant */
	$(document).on('click','span.remove-participant',function() {
	  $(this).parent('li').remove();
	});
	
JS;
$this->registerJs($js, $this::POS_READY);

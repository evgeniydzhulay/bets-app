<?php

/* @var $this \yii\web\View */
/* @var $searchModel \backend\modules\games\models\EventGameSearch */
/* @var $dataProvider \yii\data\ActiveDataProvider */

/* @var $tournamentModel \common\modules\games\models\EventTournamentModel */
/* @var $tournamentList array */
/* @var $sportList array */

/* @var $positioning string */

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventOutcomesModel;
use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

$this->registerCssFile('/static/css/games.css');

if($positioning == 'all'){
	$this->title = Yii::t('games', 'Games');
}else{
	echo $this->render('/_parts/_breadcrumbs.php', ['location' => Yii::t('games', 'Games'), 'parent' => 'EventTournament', 'parent_id' => $tournamentModel->id]);

}
?>
<style>
	.btn-group .btn + .btn {
		margin-left: 1px;
	}
</style>
<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">

		</div>
		<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<?php
		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'pjax' => true,
			'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
			'filterModel' => $searchModel,
			'toolbar' => [
				['content' =>
					\backend\modules\games\widgets\NotificationButtons::widget() .
					$create = ($positioning == 'tournament') ?
						Html::a('<span class="fa fa-refresh"></span>', Url::to(['event-game/index', 'tournament_id' => $tournamentModel->id]), ['class' => 'btn btn-warning', 'title' => Yii::t('games', 'Reset filter')])
						. Html::a('<span class="fa fa-plus"></span>', Url::to(['event-game/create', 'tournament_id' => $tournamentModel->id]), [
							'class' => 'btn btn-default',
							'data-pjax' => 0,
							'title' => Yii::t('games', 'Add game')
						]) : Html::a('<span class="fa fa-refresh"></span>', Url::to(['event-game/index']), ['class' => 'btn btn-warning', 'title' => Yii::t('games', 'Reset filter')])
				]
			],
			'columns' => [
				[
					'attribute' => 'id',
					'headerOptions' => ['width' => '80px'],
				],
				[
					'label' => Yii::t('games', 'Title'),
					'attribute' => 'game_title',
					'value' => function ($data) {
						return GameHelpers::getStringForLocale($data->eventGameLocales, 'title', false, true);
					}
				],
				[
					'label' => 'Promo<br />Игра',
					'encodeLabel' => false,
					'attribute' => 'promo_game',
					'format' => 'raw',
					'filter' => [1 => 'пок.'],
					'filterOptions' => ['style' => 'padding: 8px 0'],
					'value' => function ($data) {
						return Html::a('<span class="fa fa-lightbulb-o"></span>', ['promo-game', 'id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-default',
							'title' => Yii::t('games','Add to promo games')
						]);
					},
					'headerOptions' => ['style' => 'width:16px;text-align:center'],
				],
				[
					'label' => Yii::t('games', 'Game type'),
					'attribute' => 'sports_id',
					'filterType' => GridView::FILTER_SELECT2,
					'filterWidgetOptions' => [
						'theme' => 'default',
						'data' => $sportList,
						'options' => ['placeholder' => ''],
						'pluginOptions' => ['allowClear' => true],
					],
					'value' => function ($data) {
						return (!empty($data->sportsLocales)) ? GameHelpers::getStringForLocale($data->sportsLocales, 'title', false, true) : 'Not Set';
					},
					'headerOptions' => ['width' => '160'],
					'visible' => ($positioning == 'all') ? true : false
				],
				[
					'label' => Yii::t('games', 'Tournament'),
					'attribute' => 'tournament_title',
					'value' => function ($data) {
						return (!empty($data->tournamentLocales)) ? GameHelpers::getStringForLocale($data->tournamentLocales, 'title', false, true) : 'Not Set';
					},
					'headerOptions' => ['width' => '180px'],
					'visible' => ($positioning == 'all') ? true : false,
				],
				[
					'attribute' => 'starts_at',
					'filterType' => GridView::FILTER_DATE_RANGE,
					'filterWidgetOptions' => [
						'startAttribute' => 'date_start',
						'endAttribute' => 'date_end',
						'convertFormat' => true,
						'hideInput' => true,
						'pluginOptions' => [
							'locale' => [
								'format' => 'd.m.Y',
								'separator' => ' - ',
							],
						],
						'pluginEvents' => [
							'cancel.daterangepicker' => "function(ev, picker) {\$('#eventgamesearch-starts_at-container input').val(''); \$('.grid-view').yiiGridView('applyFilter');}"
						]
					],
					'filter' => true,
					'value' => function ($model) {
						return Yii::$app->formatter->asDate($model->starts_at, 'php: d.m.Y H:i');
					},
					'format' => 'raw'
				],
				[
					'attribute' => 'is_enabled',
					'filter' => [Yii::t('games', 'No'), Yii::t('games', 'Yes')],
					'format' => 'raw',
					'value' => function ($model) {
						return ($model->is_enabled) ?
							Html::a(Yii::t('games', 'Disable'), Url::to(['change-status', 'id' => $model->id, 'status' => EventGameModel::STATUS_DISABLED]), ['class' => 'btn btn-danger btn-block btn-xs', 'data' => ['pjax' => 0]])
							: Html::a(Yii::t('games', 'Enable'), Url::to(['change-status', 'id' => $model->id, 'status' => EventGameModel::STATUS_ENABLED]), ['class' => 'btn btn-success btn-block btn-xs', 'data' => ['pjax' => 0]]);
					},
					'contentOptions' => ['style' => 'text-align:center']
				],
				[
					'attribute' => 'is_hidden',
					'filter' => [Yii::t('games', 'No'), Yii::t('games', 'Yes')],
					'value' => function ($model) {
						return ($model->is_hidden) ? '<span class="">' . Yii::t('games', 'Yes') . '</span>' : '<span>' . Yii::t('games', 'No') . '</span>';
					},
					'format' => 'raw',
					'contentOptions' => ['style' => 'text-align:center']
				],
				[
					'attribute' => 'is_finished',
					'filter' => [Yii::t('games', 'No'), Yii::t('games', 'Yes')],
					'value' => function ($model) {
						return ($model->is_finished) ? '<span  class="">' . Yii::t('games', 'Yes') . '</span>' : '<span>' . Yii::t('games', 'No') . '</span>';
					},
					'format' => 'raw',
					'contentOptions' => ['style' => 'text-align:center']
				],
				[
					'attribute' => 'is_banned',
					'filter' => [Yii::t('games', 'No'), Yii::t('games', 'Yes')],
					'value' => function ($model) {
						return ($model->is_banned) ? '<span class="">' . Yii::t('games', 'Yes') . '</span>' : '<span>' . Yii::t('games', 'No') . '</span>';
					},
					'format' => 'raw',
					'contentOptions' => ['style' => 'text-align:center']
				],
				[
					'attribute' => 'link',
					'filter' => [1 => Yii::t('games', 'Yes')],
					'label' => Yii::t("games", 'Video'),
					'headerOptions' => ['style' => 'width:100px'],
					'contentOptions' => ['style' => 'text-align:center'],
					'content' => function ($model) {
						return (!empty($model->video_link)) ? '<span class="glyphicon glyphicon-film"></span>' : '';
					}
				],
				[
					'attribute' => 'sort_order',
					'filter' => false,
					'headerOptions' => ['style' => 'width:100px'],
					'contentOptions' => ['style' => 'text-align:center']
				],
				[
					'content' => function ($data) {
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'game_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-success'
						]);
					},
					'headerOptions' => ['style' => 'width:20px'],
				],
				[
					'content' => function ($data) {
						return Html::a(Yii::t('games', 'Outcomes'), ['event-outcomes/event-game-outcomes', 'game_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-primary'
						]);
					},
					'headerOptions' => ['style' => 'width:60px'],
				],
			],
		]);
		?>

	</div>
</div>
<?php

use common\modules\games\models\BetsItemsModel;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */

?>
<style>
	.select-result table td {
		font-size: 15px;
	}

	.title-select {
		color: #ff1524;
		text-shadow: 0 0 4px #ff8e7b;
		font-weight: bold;
	}

	.checkbox input[type=checkbox] {
		margin-top: 5px;
	}

	.checkbox{
		font-size: 15px;
	}

	.checkbox input:checked + span {
		color: #ff1524;
		text-shadow: 0 0 4px #ff8e7b;
		font-weight: bold;
	}
</style>

<?php $form = ActiveForm::begin() ?>
<?php if(!empty($outcomes)): ?>
	<?= Html::hiddenInput('data[gameId]', $data->game_id) ?>
	<?= Html::hiddenInput('data[marketId]', $data->market_id) ?>
	<div class="form-group select-result">
		<?php $participants = !empty($data['game']['eventParticipants']) ? $data['game']['eventParticipants'] : [] ?>
		<?php $participantsList = \backend\modules\games\components\GameHelpers::getParticipantsList($participants) ?>
		<table class="table table-hover select-result">
			<?php foreach($outcomes as $outcome): ?>
				<tr>
					<td>
						<?php $title = !empty($outcome['outcomeLocale'][0]['title']) ? $outcome['outcomeLocale'][0]['title'] : 'Not set' ?>
						<?= \backend\modules\games\components\GameHelpers::replacingCodeWithName($title, $participantsList) ?>
					</td>
					<td>
						<?= Html::dropDownList('data[selected_outcomes][' . $outcome->id . ']', null, $result_array, ['prompt' => '', 'class' => 'select-outcome']) ?>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
		<hr/>
		<div class="checkbox">
			<label class="text-danger">
				<input class="" type="checkbox" name="data[selected_outcomes][not-one]" id="not-one" value="not-one">
				<span class="">Ни один исход не выиграл</span>
			</label>
		</div>
	</div>
	<div class="text-center"><?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-success']) ?></div>
<?php endif; ?>
<?php ActiveForm::end() ?>
<?php
$js = <<<JS
	$('.select-result table .select-outcome').change(function(el) {
	  	var sl = $(this);
		if(sl.val()){
	  		sl.parent('td').siblings('td').addClass('title-select');
	  		$('input#not-one').prop("checked", false);
	  	}else{
			sl.parent('td').siblings('td').removeClass('title-select');
	  	}
	});


$('input#not-one').on('click',function() {
	  	if($(this).prop('checked')){
	  		$('.select-result table .select-outcome').map(function(idx,el) {
	  	  		$(el).val(false);
	  	  		$(el).parent('td').siblings('td').removeClass('title-select');
	  		})
	  	}
	});

JS;
$this->registerJs($js);
?>

<?php

use common\modules\games\models\BetsItemsModel;
use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this \yii\web\View */

?>
<style>
	.select-result table td {
		font-size: 15px;
	}

	.title-select {
		color: #ff1524;
		text-shadow: 0 0 4px #ff8e7b;
		font-weight: bold;
	}

	.checkbox input[type=checkbox] {
		margin-top: 5px;
	}

	.checkbox {
		font-size: 15px;
	}

	.checkbox input:checked + span {
		color: #ff1524;
		text-shadow: 0 0 4px #ff8e7b;
		font-weight: bold;
	}

	/* button */
	.checkbox-result input[type="checkbox"] {
		display: none;
	}

	.checkbox-result label {
		display: inline-block;
		padding: 4px 6px;
		cursor: pointer;
		background-color: #fff;
		color: #444;
		border: 1px solid #a0a0a0;
		font-size: 13px;
	}

	.checkbox-result input[type="checkbox"]:checked + label {
		background-color: #ff1524;
		border: 1px solid #ff1524;
		color: #fff;
	}

	.checkbox-result label:hover {
		background-color: #fcfcfc;
		border: 1px solid #484848;
	}

</style>

<?php $form = ActiveForm::begin() ?>
<?php if(!empty($outcomes)): ?>
	<?= Html::hiddenInput('data[gameId]', $data->game_id) ?>
	<?= Html::hiddenInput('data[marketId]', $data->market_id) ?>
	<div class="form-group select-result">
		<?php $participants = !empty($data['game']['eventParticipants']) ? $data['game']['eventParticipants'] : [] ?>
		<?php $participantsList = \backend\modules\games\components\GameHelpers::getParticipantsList($participants) ?>
		<table class="table table-hover select-result">
			<?php foreach($outcomes as $outcome): ?>
				<tr>
					<td>
						<?php $title = !empty($outcome['outcomeLocale']) ? \backend\modules\games\components\GameHelpers::getStringForLocale($outcome['outcomeLocale'],'title',false,true) : 'Not set' ?>
						<?= \backend\modules\games\components\GameHelpers::replacingCodeWithName($title, $participantsList) ?>
					</td>
					<td>
						<div class="checkbox-result">
							<?php foreach($result_array as $status => $value): ?>
								<input type="checkbox" id="checkbox_<?= $outcome->id . $status ?>" name="data[selected_outcomes][<?= $outcome->id ?>]" value="<?= $status ?>">
								<label for="checkbox_<?= $outcome->id . $status ?>"><?= $value ?></label>
							<?php endforeach; ?>
						</div>
					</td>
				</tr>
			<?php endforeach; ?>
		</table>
		<hr/>
		<div class="checkbox">
			<label class="text-danger">
				<input class="" type="checkbox" name="data[selected_outcomes][not-one]" id="not-one" value="not-one">
				<span class="">Ни один исход не выиграл</span>
			</label>
		</div>
	</div>
	<div class="text-center"><?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-success']) ?></div>
	<p>&nbsp;</p>
<?php endif; ?>
<?php ActiveForm::end() ?>
<?php
$js = <<<JS
	$('.checkbox-result input[type=checkbox]').on('click',function() {
	 	$(this).siblings('input[type=checkbox]').map(function(idx,el) {
	 		if($(el).prop('checked')){
	 			$(el).prop('checked',false);
	 		}
	 	});
	 	if($(this).prop('checked')){
	 		$(this).parents('td').siblings('td').addClass('title-select');	
	 	}else{
	 		$(this).parents('td').siblings('td').removeClass('title-select');
	 	}
	 	
	 	if($('input#not-one').prop('checked')){
	 		$('input#not-one').prop("checked", false);
	 	}
	})

	$('input#not-one').on('click',function() {
	  	if($(this).prop('checked')){
	  		$('.select-result table .checkbox-result input[type=checkbox]').map(function(idx,el) {
	  	  		el = $(el);
	  			if(el.prop('checked')){
	 				el.prop('checked',false);
	 				el.parents('td').siblings('td').removeClass('title-select');
	 			}
	  		});
	  	}
	});
JS;
$this->registerJs($js);
?>

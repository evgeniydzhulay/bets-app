<?php

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\BetsItemsModel;
use common\modules\games\models\BetsModel;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * @var $model \backend\modules\games\models\BetsModel
 * @var $item \backend\modules\games\models\BetsItemsModel
 */

?>
<div class="box box-danger" style="padding: 5px">
	<div class="bet-outcome">
		<h4 class="bg-purple disabled color-palette" style="padding: 3px 5px">
			<span class="fa fa-eye"></span> <?= Yii::t('games', 'Outcome') ?></h4>
		<div class="row">
			<div class="col-md-12">
				<table class="table table table-condensed">
					<thead>
					<tr>
						<th>#</th>
						<th style="width: 150px"><?= Yii::t('games', 'Sport') ?></th>
						<th style="width: 250px"><?= Yii::t('games', 'Tournament') ?></th>
						<th style="width: 250px"><?= Yii::t('games', 'Game') ?></th>
						<th style=""><i class="glyphicon glyphicon-calendar"></i></th>
						<th><?= Yii::t('games', 'Market') ?></th>
						<th><?= Yii::t('games', 'Outcome') ?></th>
						<th><?= Yii::t('games', 'Status') ?></th>
						<th><?= Yii::t('games', 'Rate') ?></th>
					</tr>
					</thead>
					<?php
							/* $model - Bets
							 * $model->items - Bet items
							 * $item->outcome,$outcome -Event outcome
							 * */
					?>
					<?php if($model->items && is_array($model->items)): ?>
						<?php $i = 1 ?>
						<?php foreach($model->items as $item): ?>
							<?php
							$outcome = $item->outcome;
							/* prepare data */
							$eventGame = (isset($outcome['game'])) ? $outcome['game'] : false;
							$sportTitle = (isset($outcome['sportsLocale'])) ? GameHelpers::getStringForLocale($outcome['sportsLocale'], 'title',false,true) : '';
							$tournamentTitle = (isset($outcome['tournamentLocale'])) ? GameHelpers::getStringForLocale($outcome['tournamentLocale'], 'title',false,true) : '';
							$gameTitle = (isset($outcome['game']['eventGameLocales'])) ? GameHelpers::getStringForLocale($outcome['game']['eventGameLocales'], 'title',false,true) : '';
							$marketTitle = (isset($outcome['marketLocale'])) ? GameHelpers::getStringForLocale($outcome['marketLocale'], 'title',false,true) : '';

							/* Outcome title */
							$participantsList = (isset($outcome['game']['eventParticipants'])) ? GameHelpers::getParticipantsList($outcome['game']['eventParticipants'],false,true) : [];
							$title = (isset($outcome['outcomeLocale'])) ? GameHelpers::getStringForLocale($outcome['outcomeLocale'], 'title',false,true) : '';
							$outcomeTitle = GameHelpers::replacingCodeWithName($title,$participantsList,false,true);
							?>

							<?php
							$extraTime = 12 * 3600;
							$time = (empty($eventGame['starts_at']) || ($eventGame['starts_at'] + $extraTime) > time()) ? false : true;
							$expiredOutcome = ($item->status == BetsItemsModel::STATUS_ACTIVE && $time) ? true : false;

							$timeWaiting = (time() < ($eventGame['starts_at'] + $extraTime) && time() > $eventGame['starts_at']) ? true : false;
							$waiting = ($item->status == BetsItemsModel::STATUS_ACTIVE && $timeWaiting) ? true : false;
							?>

							<tr class="<?= GameHelpers::colorStatuses($item->status) ?> <?= ($expiredOutcome) ? 'bg-red-active' : '' ?> <?= ($waiting) ? 'bg-yellow' : '' ?>">
								<td><?= $i++ ?></td>
								<td><?= $sportTitle ?></td>
								<td><?= $tournamentTitle ?></td>
								<td><?= $gameTitle ?></td>
								<td><?= (!empty($eventGame['starts_at'])) ? date('d.m.Y H:i', $eventGame['starts_at']) : '?' ?> <?= (!empty($eventGame['starts_at']) && $time && $item['status'] == BetsItemsModel::STATUS_ACTIVE) ? '&nbsp; <span class="fa  fa-bell"></span>' : '' ?></td>
								<td><?= $marketTitle ?></td>
								<td><?= $outcomeTitle ?></td>
								<td><?= BetsItemsModel::statuses()[$item->status] ?></td>
								<td><?= $item->rate ?></td>
								<td><?= ($expiredOutcome) ? Html::a('<span class="fa fa-eye"></span>', Url::to(['expired-outcome','outcome_id' => $outcome['id']]), ['data-pjax' => 0,'class' => 'btn btn-xs', 'style' => 'color:#fff;border-color:#fff', 'title' => Yii::t('app', 'View')]) : '' ?></td>
							</tr>
						<?php endforeach; ?>
					<?php endif; ?>
				</table>
			</div>
		</div>
	</div>
	<hr/>
</div>


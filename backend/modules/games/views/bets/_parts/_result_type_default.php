<?php

use backend\modules\games\components\GameHelpers;
use kartik\form\ActiveForm;
use yii\helpers\Html;

?>

	<style>
		.checkbox label {
			font-size: 15px;
		}

		.checkbox input[type=checkbox] {
			margin-top: 5px;
		}

		.checkbox input:checked + span {
			color: #ff1524;
			text-shadow: 0 0 4px #ff8e7b;
			font-weight: bold;
		}
	</style>

<?php $form = ActiveForm::begin() ?>
<?php if(!empty($outcomes)): ?>
	<?= Html::hiddenInput('data[gameId]', $data->game_id) ?>
	<?= Html::hiddenInput('data[marketId]', $data->market_id) ?>
	<div class="form-group">
		<?php $participants = !empty($data['game']['eventParticipants']) ? $data['game']['eventParticipants'] : [] ?>
		<?php $participantsList = \backend\modules\games\components\GameHelpers::getParticipantsList($participants) ?>
		<?php foreach($outcomes as $outcome): ?>
			<div class="checkbox">
				<label>
					<?php $title = !empty($outcome['outcomeLocale']) ? GameHelpers::getStringForLocale($outcome['outcomeLocale'],'title',false,true) : 'Not set' ?>
					<input type="checkbox" class='input-outcome' name="data[selected_outcomes][<?= $outcome->id ?>]" id="" value="1">
					<span><?= \backend\modules\games\components\GameHelpers::replacingCodeWithName($title, $participantsList) ?></span>
				</label>

			</div>
		<?php endforeach; ?>
		<hr/>
		<div class="checkbox">
			<label class="text-danger">
				<input class="" type="checkbox" name="data[selected_outcomes][not-one]" id="not-one" value="not-one">
				<span>Ни один исход не выиграл</span>
			</label>
		</div>
	</div>
	<div class="text-center"><?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-success']) ?></div>
<?php endif; ?>
<?php ActiveForm::end() ?>

<?php
$js = <<<JS
	$('input#not-one').on('click',function() {
	  	if($(this).prop('checked')){
	  		$('input.input-outcome').map(function(idx,el) {
	  	  		$(el).prop("checked", false);
	  		})
	  	}
	});

	$('input.input-outcome').on('click',function() {
	  	if($(this).prop('checked')){
	  	  	$('input#not-one').prop("checked", false);
	  	}
	})
JS;
$this->registerJs($js);

<?php

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use common\widgets\GridView;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\games\models\search\BetsRevertedSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $sportsList array */
/* @var $adminsList array */

$this->registerJsFile('/static/js/games.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerCssFile('/static/css/games.css');

$this->title = Yii::t('games', 'Bets reverted');
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Bets'), 'url' => ['bets/index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div id="pre-loader">
	<div class="spinner"></div>
</div>
<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">
			<?= Html::a('<span class="fa fa-refresh"></span>', Url::to(['bets/bets-revert-index']), ['class' => 'btn btn-warning']) ?>
			<?= Html::a('<span class="fa fa-angle-double-left"></span> ' . Yii::t('games','Bets'), Url::to(['bets/index']), ['class' => 'btn btn-primary','style' => 'margin-right:5px']) ?>
		</div>
		<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'pjax' => true,
			'id' => 'bets-grid-pjax',
			'pjaxSettings' => [],
			'toolbar' => [],
			'columns' => [
				[
					'class' => \kartik\grid\ExpandRowColumn::class,
					'value' => function ($model, $key, $index, $column) {
						return GridView::ROW_COLLAPSED;
					},
					'detail' => function ($model, $key, $index, $column) {
						return Yii::$app->controller->renderPartial('_parts/_bet-details', ['model' => $model]);
					}
				],

				[
					'attribute' => 'id',
					'label' => '#',
					'headerOptions' => ['style' => 'width:60px'],
					'hAlign' => GridView::ALIGN_CENTER,
				],
				[
					'attribute' => 'user_name',
					'label' => Yii::t('games', 'User'),
					'value' => 'userName',
				],
				[
					'attribute' => 'creator_name',
					'label' => Yii::t('bets','Created by'),
					'filter' => \backend\modules\games\models\BetsRevertedModel::getAdminsList(),
					'value' => function($model){
						return !empty($model->betsReverted->creatorProfile->name) ? $model->betsReverted->creatorProfile->name . ' ' . $model->betsReverted->creatorProfile->surname : "Not Set";
					}
				],
				[
					'attribute' => 'amount_bet',
					'format' => 'decimal',
					'headerOptions' => ['style' => 'width:100px']
				],
				[
					'attribute' => 'created_at',
					'filterType' => GridView::FILTER_DATE_RANGE,
					'filterWidgetOptions' => [
						'startAttribute' => 'date_start',
						'endAttribute' => 'date_end',
						'convertFormat' => true,
						'hideInput' => true,
						'pluginOptions' => [
							'locale' => [
								'format' => 'd.m.Y',
								'separator' => ' - ',
							],
						]
					],
					'filter' => true,
					'headerOptions' => ['style' => 'width:200px'],
					'value' => function ($model) {
						return $model->betsReverted ? Yii::$app->formatter->asDate($model->betsReverted->created_at, 'php: d.m.Y H:i') : 'not set';
					},
					'format' => 'raw'
				],
				[
					'label' => Yii::t('bets','Comment'),
					'value' => function($model){
						return $model->betsReverted ? $model->betsReverted->comment : '';
					},
					'headerOptions' => ['style' => 'width:50%']
				]
			],
		]); ?>

	</div>
</div>
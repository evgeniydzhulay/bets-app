<?php

/* @var $modalForm \yii\base\DynamicModel */
?>
<?php

use common\modules\games\models\BetsItemsModel;
use common\modules\games\models\BetsModel;
use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventTournamentLocaleModel;
use common\widgets\GridView;
use kartik\field\FieldRange;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\games\models\BetsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $sportsList array */
/* @var $totalBetSum float */
/* @var $totalWinSum float */
/* @var $total mixed */

$this->title = Yii::t('games', 'Bets');
$this->params['breadcrumbs'][] = $this->title;
$this->registerJsFile('/static/js/games.js', ['depends' => 'yii\web\YiiAsset']);
$this->registerCssFile('/static/css/games.css');

/* prepare data */
//$data_tournament = [];
//$data_game = [];
/*if(!empty(Yii::$app->request->get('tournament'))){
	$data_tournament = EventTournamentLocaleModel::find()->select('title')->where(['tournament_id' => Yii::$app->request->get('tournament'), 'lang_code' => 'en'])->indexBy('tournament_id')->column();
}
if(!empty(Yii::$app->request->get('game'))){
	$data_game = EventGameLocaleModel::find()->select('title')->where(['game_id' => Yii::$app->request->get('game'), 'lang_code' => 'en'])->indexBy('game_id')->column();
}*/
?>

	<div id="pre-loader">
		<div class="spinner"></div>
	</div>
	<div class="bets-model-index">
		<div class="box box-primary">
			<div class="box-header with-border" style="min-height:43px; padding: 10px 5px">
				<div class="row">
					<div class="col-md-1 text-center" style="font-size: 20px"><span class="fa fa-filter text-primary"></span>
					</div>
					<?php $form = ActiveForm::begin() ?>
					<div class="col-md-3">
						<!-- Sport -->
						<?= $form->field($searchModel,'sport_id')->widget(Select2::class,[
							'data' => $sportsList,
							'options' => ['placeholder' => Yii::t('games', 'Sport')],
							'pluginOptions' => [
								'tags' => true,
								'allowClear' => true,
							],
							'pluginEvents' => [
								'change' => 'sportClearLists',
							],
						])->label(false); ?>
					</div>
					<div class="col-md-3">
						<!-- Tournament -->
						<?= $form->field($searchModel,'tournament_id')->widget(Select2::class,[
							'data' => [],
							'options' => ['placeholder' => Yii::t('games', 'Tournament')],
							'pluginOptions' => [
								'allowClear' => true,
								'minimumInputLength' => 3,
								'ajax' => [
									'url' => Url::to(['bets/tournaments-list']),
									'dataType' => 'json',
									'data' => new JsExpression('function(params) { return requestTournamentsList(params) }')
								],
							],
							'pluginEvents' => [
								'change' => 'tournamentClearLists',
							],
						])->label(false); ?>
					</div>
					<div class="col-md-3">
						<!-- Game -->
						<?= $form->field($searchModel,'game_id')->widget(Select2::class,[
							'data' => [],
							'options' => ['placeholder' => Yii::t('games', 'Game')],
							'pluginOptions' => [
								'allowClear' => true,
								'minimumInputLength' => 3,
								'ajax' => [
									'url' => Url::to(['bets/games-list']),
									'dataType' => 'json',
									'data' => new JsExpression('function(params) { return requestGamesList(params) }')
								],
							],
							'pluginEvents' => [
							],
						])->label(false); ?>
					</div>
					<?php ActiveForm::end() ?>
					<div class="col-md-1">
						<?= Html::a('<span class="fa fa-refresh"></span>', Url::to(['bets/index']), ['class' => 'btn btn-warning']) ?>
					</div>
				</div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<?php
				echo GridView::widget([
					'dataProvider' => $dataProvider,
					'filterModel' => $searchModel,
					'filterSelector' => '#betssearch-game_id,#betssearch-tournament_id,#betssearch-sport_id',
					'pjax' => true,
					'id' => 'bets-grid',
					'pjaxSettings' => [],
					'showFooter' => true,
					'footerRowOptions' => ['style' => 'background-color:#fffcba'],
					'rowOptions' => function ($model, $key, $index, $grid) {
						/* model - bet, item - bet_item */
						if($model->status == BetsModel::STATUS_ACTIVE && !empty($model->items)){
							$extraTime = 12 * 3600;// ! 12h

							/* Danger, game is over and 12 hours passed */
							foreach($model->items as $item){
								$time = (empty($item->outcome['game']['starts_at']) || ($item->outcome['game']['starts_at'] + $extraTime) > time()) ? false : true;
								if($item->status == BetsItemsModel::STATUS_ACTIVE && $time){
									return ['class' => 'bg-red-active'];
								}
							}

							/* Warning, game is over */
							foreach($model->items as $item){
								$time = (!empty($item->outcome['game']['starts_at']) && time() > $item->outcome['game']['starts_at']) ? true : false;
								if($item->status == BetsItemsModel::STATUS_ACTIVE && $time){
									return ['class' => 'bg-yellow'];
								}
							}
						}
					},
					'toolbar' => [
						['content' => Html::a(Yii::t('games', 'Bets reverted'), Url::to(['bets/bets-revert-index']), ['class' => 'btn btn-primary', 'data-pjax' => 0])]
					],
					'columns' => [
						[
							'class' => \kartik\grid\ExpandRowColumn::class,
							'value' => function ($model, $key, $index, $column) {
								return GridView::ROW_COLLAPSED;
							},
							'detail' => function ($model, $key, $index, $column) {
								return Yii::$app->controller->renderPartial('_parts/_bet-details', ['model' => $model]);
							}
						],
						[
							'attribute' => 'id',
							'label' => '#',
							'headerOptions' => ['style' => 'width:60px'],
							'hAlign' => GridView::ALIGN_CENTER,
						],
						[
							'attribute' => 'user_name',
							'label' => Yii::t('games', 'User'),
							'value' => 'userName',
							'footer' => $total
						],
						[
							'attribute' => 'status',
							'filter' => BetsModel::statuses(),
							'value' => function ($model) {
								if(key_exists($model->status, BetsModel::statuses())){
									$class = setStatusStyle($model->status);
									return "<span class='{$class}'>" . BetsModel::statuses()[$model->status] . "</span>";
								}
							},
							'format' => 'raw'
						],
						[
							'attribute' => 'created_at',
							'filterType' => GridView::FILTER_DATE_RANGE,
							'filterWidgetOptions' => [
								'startAttribute' => 'date_start',
								'endAttribute' => 'date_end',
								//'presetDropdown' => true,
								'convertFormat' => true,
								'hideInput' => true,
								'pluginOptions' => [
									'locale' => [
										'format' => 'd.m.Y',
										'separator' => ' - ',
									],
								]
							],
							'filter' => true,
							'value' => function ($model) {
								return Yii::$app->formatter->asDate($model->created_at, 'php: d.m.Y H:i');
							},
							'format' => 'raw'
						],
						[
							'attribute' => 'amount_bet',
							'hAlign' => GridView::ALIGN_CENTER,
							'pageSummary' => true,
							'format' => 'raw',
							'value' => function ($model) {
								return Yii::$app->formatter->asCurrency($model->amount_bet, $model->user->purse_currency);
							},
							'filter' => FieldRange::widget([
								'model' => $searchModel,
								'label' => '',
								'labelOptions' => ['style' => 'display:none'],
								'attribute1' => 'amount_bet_from',
								'attribute2' => 'amount_bet_to',
								'options1' => ['placeholder' => 'from'],
								'options2' => ['placeholder' => 'to'],
								'separator' => '/',
								'useAddons' => false,
								'widgetContainer' => [
									'style' => 'width: 160px'
								],
								'type' => FieldRange::INPUT_TEXT,
							]),
							'headerOptions' => ['style' => 'width:160px'],

						],
						[
							'attribute' => 'amount_win',
							'hAlign' => GridView::ALIGN_CENTER,
							'value' => function ($model) {
								$class = setStatusStyle($model->status);
								return "<span class='{$class}'>" . Yii::$app->formatter->asCurrency($model->amount_win, $model->user->purse_currency) . "</span>";
							},
							'format' => 'raw',
							'filter' => FieldRange::widget([
								'model' => $searchModel,
								'label' => '',
								'labelOptions' => ['style' => 'display:none'],
								'attribute1' => 'amount_win_from',
								'options1' => ['placeholder' => 'from'],
								'options2' => ['placeholder' => 'to'],
								'attribute2' => 'amount_win_to',
								'separator' => '/',
								'useAddons' => false,
								'widgetContainer' => [
									'style' => 'width: 160px'
								],
								'type' => FieldRange::INPUT_TEXT,
							]),

							'headerOptions' => ['style' => 'width:160px'],
						],
						[
							'attribute' => 'withdraw_id',
							'headerOptions' => ['width' => '80px'],
							'filter' => false,
							'hAlign' => GridView::ALIGN_CENTER,
							'value' => function ($model) {
								return Html::a('<span class="fa fa-external-link-square text-primary"></span> ', Url::to(['/user/withdrawals/view', 'id' => $model->withdraw_id]), ['target' => '_blank', 'data-pjax' => 0]);
							},
							'format' => 'raw',
						],
						[
							'attribute' => 'refill_id',
							'headerOptions' => ['width' => '80px'],
							'filter' => false,
							'hAlign' => GridView::ALIGN_CENTER,
							'value' => function ($model) {
								if($model->status == BetsModel::STATUS_VICTORY){
									return Html::a('<span class="fa fa-external-link-square text-success"></span> ', Url::to(['/user/deposits/view', 'id' => $model->refill_id]), ['target' => '_blank', 'data-pjax' => 0]);
								}
								return '';
							},
							'format' => 'raw'
						],
						[
							'content' => function ($model) {
								if($model->status == BetsModel::STATUS_BANNED){
									return Html::a(' <span class="fa fa-eye"></span>', Url::to(['bets/bets-revert-index', 'id' => $model->id]), ['target' => '_blank', 'data-pjax' => 0]);

								}
								return Html::Button('<span class="fa fa-reply"></span>', [
									'data-pjax' => 0,
									'data-id' => $model->id,
									'class' => 'btn btn-block btn-xs btn-danger btn-revert-bet',
									'title' => Yii::t('games', 'Cancel a bet')
								]);
							},
							'headerOptions' => ['width' => '20px'],
						],
					],
				]);
				?>
			</div>
		</div>
	</div>

<?php
function setStatusStyle($status) {
	switch($status){
		case BetsModel::STATUS_VICTORY :
			return 'text-green text-bold';
		case BetsModel::STATUS_DEFEAT :
			return 'text-red text-bold';
		case BetsModel::STATUS_REVERTED :
			return 'text-muted text-bold';
		default :
			return '';
	}
}

/* Modal window */

Modal::begin([
	'header' => '<h3>' . Yii::t('bets', 'Do you want cancel bet') . '<b> #<span id="bet"></span></b></h3>',
	'headerOptions' => ['class' => 'bg-red-active'],
	'id' => 'modal-bet-revert',
]);

/* Content */
$form = ActiveForm::begin(['action' => Url::to(['bets/bet-ban']), 'id' => 'modal-form-ban']);
echo Html::hiddenInput('bet_id', null, ['id' => 'bet-id']);
echo $form->field($modalForm, 'comment')->textarea(['rows' => 6])->label(Yii::t('bets', 'Comment'));
echo '<br /><div class="text-right">' . Html::button(Yii::t('bets', 'Close'), ['class' => 'btn btn-default', 'data-dismiss' => 'modal']) . ' ' . Html::submitButton(Yii::t('bets', 'Disable Bet'), ['class' => 'btn btn-danger']) . '</div>';
ActiveForm::end();
/* ./Content */

Modal::end();

$js = <<<JS
	$(document).on('click','button.btn-revert-bet',function() {
		var id = $(this).attr('data-id'); 
		$('#bet-id').val(id);
		$('span#bet').text(id);
		$('#modal-bet-revert').modal('show');
	});
	$('#modal-bet-revert').on('hide.bs.modal', function (e) {
  		document.getElementById("modal-form-ban").reset();
	})
	
JS;
$this->registerJs($js, $this::POS_READY);

/* ./Modal window */



<?php

/* @var $this \yii\web\View */

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\BetsItemsModel;
use common\modules\games\models\GameMarketsModel;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">
			<?= Html::a('<span class="fa fa-angle-double-left"></span> ' . Yii::t('games', 'Bets'), Url::to(['bets/index']), ['class' => 'btn btn-primary', 'style' => 'margin-right:5px']) ?>
		</div>
		<!-- /.box-tools -->
	</div>

	<!-- /.box-header -->
	<div class="box-body">
		<div class="row">
			<div class="col-sm-4 col-sm-offset-1">
				<table class="table">
					<tr>
						<td><?= Yii::t('games', 'Game type') ?>:</td>
						<td><?= (!empty($data['sportsLocale'])) ? '<span class="text-danger">' . GameHelpers::getStringForLocale($data['sportsLocale'],'title',false,true) . '</span>' : '' ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('games', 'Tournament') ?>:</td>
						<td><?= (!empty($data['tournamentLocale'])) ? '<span class="text-danger">' . GameHelpers::getStringForLocale($data['tournamentLocale'],'title',false,true) . '</span>' : '' ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('games', 'Game') ?>:</td>
						<td><?= (!empty($data['game']['eventGameLocales'])) ? '<span class="text-danger">' . GameHelpers::getStringForLocale($data['game']['eventGameLocales'],'title',false,true) . '</span>' : '' ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('games', 'Group') ?>:</td>
						<td><?= (!empty($data['groupLocale'])) ? '<span class="text-danger">' . GameHelpers::getStringForLocale($data['groupLocale'],'title',false,true) . '</span>' : '' ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('games', 'Market') ?>:</td>
						<td><?= (!empty($data['marketLocale'])) ? '<span class="text-danger">' . GameHelpers::getStringForLocale($data['marketLocale'],'title',false,true) . '</span>' : '' ?></td>
					</tr>
					<tr>
						<td><?= Yii::t('games', 'Game will start') ?>:</td>
						<td><?= (!empty($data['game']['starts_at'])) ? '<span class="">' . date('d.m.Y H:i', $data['game']['starts_at']) . '</span>' : '?' ?></td>
					</tr>
				</table>
			</div>
			<div class="col-sm-7">
				<h3>Выберите исход для маркета <?= (!empty($data['marketLocale'])) ? '<span class="text-danger">' . GameHelpers::getStringForLocale($data['marketLocale'],'title',false,true) . '</span>' : "" ?>:</h3>
				<?php
				$file_name = '_result_type_total_handicap.php';
				$result_array = [];
				if($data->market->result_type == GameMarketsModel::RESULT_TYPE_2_OPTIONS){
					$result_array = [
						BetsItemsModel::STATUS_VICTORY => Yii::t('games', 'Victory'),
						BetsItemsModel::STATUS_REFUND => Yii::t('games', 'Refund')
					];
				}elseif($data->market->result_type == GameMarketsModel::RESULT_TYPE_3_OPTIONS){
					$result_array = [
						BetsItemsModel::STATUS_VICTORY => Yii::t('games', 'Victory'),
						BetsItemsModel::STATUS_HALF_VICTORY => Yii::t('games', 'Half/Victory'),
						BetsItemsModel::STATUS_HALF_DEFEAT => Yii::t('games', 'Half/Defeat')
					];
				}else{ //RESULT_TYPE_DEFAULT
					$file_name = '_result_type_default.php';
				}

				echo $this->render('_parts/' . $file_name, ['outcomes' => $outcomes, 'data' => $data, 'result_array' => $result_array])
				?>
			</div>
		</div>
	</div>
</div>



<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\modules\games\models\GameMarketsGroupsLocaleModel */

/*$this->title = Yii::t('games','Update') . ': ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['/games/game-sports/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Market groups'), 'url' => ['index','sport_id' => $model->sports_id]];
$this->params['breadcrumbs'][] = $this->title;*/
echo $this->render('/_parts/_breadcrumbs.php',['location' => Yii::t('games','Update'),'parent' => 'GameMarketsGroups','parent_id' => $model->group_id]);
?>
<div class="game-markets-groups-model-update box box-default">

	<div class="box-header with-border">
		<h3 class="box-title"><?= Yii::t('games', 'Group') ?>:
			<span class="text-red"><?= $model->title ?></span> &nbsp; <?= Yii::t('games', 'Language') ?>:
			<span class="text-red"> <?= $model->lang_code ?></span></h3>
	</div>
	<div class="box-body">
		<!-- content -->
		<div class="text-right">
			<?= Html::a(Yii::t('games','Close'), \yii\helpers\Url::to(['update','group_id' => $model->group_id]), ['class' => 'btn btn-md btn-warning']) ?>
		</div>
		<?php $form = ActiveForm::begin() ?>
		<div class="row">
			<div class="col-md-6">
				<?= $form->field($model, 'title')->textInput() ?>
			</div>
		</div>

		<div class="text-right">
			<?= Html::submitButton(Yii::t('games','Update'), ['class' => 'btn btn-primary']) ?>
		</div>
		<?php ActiveForm::end() ?>

	</div>

</div>

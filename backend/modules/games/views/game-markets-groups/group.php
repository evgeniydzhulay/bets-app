<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\modules\games\models\GameMarketsGroupsModel
 * @var $searchModel \common\modules\games\models\GameMarketsGroupsLocaleModel
 * @var $dataProvider \yii\data\ActiveDataProvider
 */
/* @var $sport_groups array */

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/*$this->title = $model->isNewRecord ? Yii::t('games', 'Add market group') : Yii::t('games', 'Update market group');
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['/games/game-sports/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Market groups'), 'url' => ['game-markets-groups/index','sport_id' => $model->sports_id]];
$this->params['breadcrumbs'][] = $this->title;*/

$location = $model->isNewRecord ? Yii::t('games', 'Add market group') : Yii::t('games', 'Update market group');
echo $this->render('/_parts/_breadcrumbs.php',['location' => $location,'parent' => 'GameSports','parent_id' => $model->sports_id]);
?>
<div class="box box-primary game-markets-groups">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">
			<?= Html::a(Yii::t('games', 'Close'), Url::to(['index','sport_id' => $model->sports_id]), ['class' => 'btn btn-warning']) ?>
		</div>
		<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body" style="padding:15px">
		<div class="row">
			<!-- block left -->
			<div class="col-sm-3">
				<?php $form = ActiveForm::begin() ?>
				<h4><span class="fa fa-gear text-warning"></span> <?= Yii::t('games', 'Settings:') ?></h4>
				<div class="">
					<?php $data = [Yii::t('games', 'Disabled'), Yii::t('games', 'Enabled')]; ?>
					<?php if($model->isNewRecord) $model->is_enabled = 1 ?>
					<label class="control-label has-star" for=""><?= Yii::t('games', 'Status') ?></label>
					<?= $form->field($model, 'is_enabled')->radioButtonGroup($data, ['itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']]])->label(false) ?>
				</div>
				<div class="">
					<?= $form->field($model, 'sort_order')->textInput(['style' => 'width:120px']) ?>
				</div>
				<hr/>
				<!-- For insert -->
				<?= $this->render('_parts/_localization_create.php', ['model' => $model]) ?>
				<div class="text-center">
					<?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-primary', 'style' => 'width:100%;margin: 25px 0']) ?>
				</div>
				<?php ActiveForm::end() ?>
			</div><!-- ./col-sm-3 -->
			<!-- right -->
			<div class="col-sm-9">
				<!-- For update -->
				<?= !$model->isNewRecord ? $this->render('_parts/_localization_update.php', ['model' => $model, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel,'sport_groups' => $sport_groups]) : '' ?>
			</div>
		</div>
	</div>
</div>

<?php

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\GameMarketsGroupsModel;
use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\games\models\GameSportsSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->registerCssFile('/static/css/games.css');

/*$this->title = Yii::t('games', 'Market groups');
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['/games/game-sports/index']];
$this->params['breadcrumbs'][] = $this->title;*/
echo $this->render('/_parts/_breadcrumbs.php', ['location' => Yii::t('games', 'Market groups'), 'parent' => 'GameSports', 'parent_id' => $searchModel->sports_id]);
?>
<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">
			<?= Html::a('<span class="fa fa-angle-double-left"></span> ' . Yii::t('games', 'Games'), Url::to(['/games/game-sports/index']), ['class' => 'btn btn-primary']) ?>
		</div>
	</div>
	<div class="box-body">
		<?php
		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'pjax' => true,
			'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
			'toolbar' => [
				[
					'content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['game-markets-groups/index', 'sport_id' => $searchModel->sports_id]), [
							'class' => 'btn btn-warning',
							'style' => 'margin-right:5px',
							'title' => Yii::t('games', 'Reset filter')
						])
						. Html::a('<span class="fa fa-plus"></span>', Url::to(['game-markets-groups/create', 'sport_id' => $searchModel->sports_id]), [
							'class' => 'btn btn-default',
							'data-pjax' => 0,
							'title' => Yii::t('games', 'Add market group')
						])
				]
			],
			'columns' => [
				[
					'attribute' => 'id',
					'headerOptions' => ['width' => '80px'],
				],
				[
					'label' => Yii::t('games', 'Title'),
					'attribute' => 'group_title',
					'headerOptions' => ['width' => '70%'],
					'value' => function ($data) {
						return GameHelpers::getStringForLocale($data->gameMarketsGroupsLocales, 'title',false,true);

					}
				],
				[
					'attribute' => 'is_enabled',
					'filter' => GameMarketsGroupsModel::statuses(),
					'format' => 'raw',
					'value' => function ($data) {
						return ($data->is_enabled) ?
							Html::a(Yii::t('games', 'Disable'), Url::to(['change-status', 'id' => $data->id, 'status' => GameMarketsGroupsModel::STATUS_DISABLED]), ['class' => 'btn btn-danger btn-block btn-xs', 'data' => ['pjax' => 0]])
							: Html::a(Yii::t('games', 'Enable'), Url::to(['change-status', 'id' => $data->id, 'status' => GameMarketsGroupsModel::STATUS_ENABLED]), ['class' => 'btn btn-success btn-block btn-xs', 'data' => ['pjax' => 0]]);
					},
					'headerOptions' => ['width' => '120px'],
				],
				[
					'attribute' => 'sort_order',
					'headerOptions' => ['width' => '60px'],
					'contentOptions' => ['style' => 'text-align:center']
				],
				[
					'content' => function ($data) {
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'group_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-success'
						]);
					},
					'headerOptions' => ['width' => '20px'],
				],
				[
					'content' => function ($data) {
						return Html::a(Yii::t('games', 'Markets'), ['game-markets/index', 'group_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-primary'
						]);
					}
				]
			],
		]);

		?>

	</div>
</div>


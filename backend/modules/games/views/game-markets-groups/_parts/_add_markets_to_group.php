<?php

use kartik\select2\Select2;
use yii\widgets\ActiveForm;

/* @var $this \yii\web\View */
/* @var $sport_groups array */
/* @var $model \common\modules\games\models\GameMarketsGroupsModel */


?>

<hr/>
<h4>
	<span class="fa fa-sitemap text-warning"></span> <?= Yii::t('games', 'Add markets to group:') ?> <!--<button class="btn btn-warning" style="float: right"><i class="fa fa-angle-double-down"></i></button>-->
</h4>
<div class="link-markets" style="border-radius: 4px; border: 1px solid #3c8dbc; padding: 5px">
	<div class="row">
		<!-- Filters -->
		<div class="col-sm-5">
			<?= Select2::widget([
				'id' => 'group_id',
				'name' => 'group',
				'data' => $sport_groups,
				'size' => Select2::SMALL,
				'options' => ['placeholder' => 'Select a group:'],
				'pluginOptions' => [
					//'allowClear' => true,
				],
			]);
			?>
			<div id="market-list"><ul></ul></div>
		</div>
		<!-- Items -->
		<div class="col-sm-7" id="selected-market-list">
			<h4 class="text-center"><span class="fa fa-chain  text-warning"></span> <?= Yii::t('games', 'Added markets to the group:') ?></h4>
			<?php $form = ActiveForm::begin(['action' => 'save-selected-markets']) ?>
			<ul id="selected-market-list_sort"><?php echo $list = $model->getAdditionalMarketsHtml() ?></ul>
			<?= \yii\helpers\Html::hiddenInput('group',$model->id) ?>
			<div class="text-center" id="button-submit" style="<?= (!$list) ? 'display:none' : '' ?>"><?= \yii\helpers\Html::submitButton(Yii::t('games','Save'),['class' => 'btn btn-warning btn-md', 'style'=>"width:200px;margin-top:15px"]) ?></div>
			<?php ActiveForm::end() ?>
		</div>
	</div>
</div>
<?php
$js = <<<JS
	$( function() {
		$( "ul#selected-market-list_sort" ).sortable();
	});
	// GET MARKETS_GROUP
	$('.link-markets #group_id').on('change',function(el) {
	  	var group_id = $(el.target).val();
	  	if(group_id){
	  		$.ajax({
	  			type: 'POST',
	  			url: '/games/game-markets-groups/get-markets-group?id=' + {$model->id},
	  			data: {id:group_id},
	  			success: function(data) {
	  			  $('#market-list ul').html(data);
	  			}
	  		});
	  	}
	});
	// SELECTED MARKETS
	$(document).on('click','#market-list ul li span',function() {
		$(this).removeClass('fa-angle-double-right').addClass('fa-remove');
	  	var group_title = ($('span#select2-group_id-container').text());
		var li = $($(this).parent('li'));
		var input = '<input type="hidden" name="market['+ li.attr('data-id') +']" value="'+ li.attr('data-id') +'">';
		if(!$('div#button-submit').is(':visible')){
			$('div#button-submit').show();
		}
		li.html(input + '<strong>' + group_title + ':</strong> ' + li.html()) 
	  	$('#selected-market-list ul').prepend(li)
	  
	});
	// DELETE SELECTED MARKETS
	$(document).on('click','#selected-market-list ul li span',function() {
		$(this).parent('li').remove();
	});

JS;
$this->registerJs($js);
?>
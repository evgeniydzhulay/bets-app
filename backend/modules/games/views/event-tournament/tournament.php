<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\modules\games\models\EventTournamentModel
 * @var $searchModel \backend\modules\games\models\search\EventTournamentSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 */

use kartik\datetime\DateTimePicker;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/*$this->title = $model->isNewRecord ? Yii::t('games', 'Add tournament') : Yii::t('games', 'Update tournament');
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['game-sports/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Tournaments'), 'url' => ['event-tournament/index', 'sport_id' => $model->sports_id]];
$this->params['breadcrumbs'][] = $this->title;*/

$location = $model->isNewRecord ? Yii::t('games', 'Add tournament') : Yii::t('games', 'Update tournament');
echo $this->render('/_parts/_breadcrumbs.php',['location' => $location,'parent' => 'GameSports','parent_id' => $model->sports_id]);

?>
<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">
			<?php echo Html::a('<span class="fa fa-reply"></span> ' . Yii::t('games','Dictionary'),['event-tournament/index','sport_id' => $model->sports_id],['class' => 'btn btn-warning']). ' ' .Html::a('<span class="fa fa-reply"></span> ' . Yii::t('games','Tournaments'),['event-tournament/index'],['class' => 'btn btn-warning']) ?>
		</div>
		<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body" style="padding:15px">
		<div class="row">
			<!-- block left -->
			<div class="col-sm-3">
				<?php $form = ActiveForm::begin() ?>
				<h4><span class="fa fa-gear text-warning"></span> <?= Yii::t('games', 'Settings:') ?></h4>
				<div class="row">
					<div class="col-sm-6">
						<?php $data = [Yii::t('games', 'Disabled'), Yii::t('games', 'Enabled')]; ?>
						<?php if($model->isNewRecord) $model->is_enabled = 1 ?>
						<label class="control-label has-star" for=""><?= Yii::t('games', 'Status') ?></label>
						<?= $form->field($model, 'is_enabled')->radioButtonGroup($data, ['itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']]])->label(false) ?>
					</div>
					<div class="col-sm-6">
						<?php $data = [Yii::t('games', 'No'), Yii::t('games', 'Yes')]; ?>
						<?php if($model->isNewRecord) $model->is_hidden = 0 ?>
						<label class="control-label has-star" for=""><?= Yii::t('games', 'Is hidden') ?></label>
						<?= $form->field($model, 'is_hidden')->radioButtonGroup($data, ['itemOptions' => ['labelOptions' => ['class' => 'btn btn-default']]])->label(false) ?>
					</div>
				</div>
				<div class="">
					<?= $form->field($model, 'starts_at')->widget(DateTimePicker::class, [
						'options' => [
							'value' => Yii::$app->formatter->asDate($model->starts_at ?? time(), 'php:d/m/Y H:i'),
						],
						'type' => DateTimePicker::TYPE_INPUT,
						'pluginOptions' => [
							'autoclose' => true,
							'format' => 'dd/mm/yyyy hh:ii',
							'startDate' => 'd',
						]
					]); ?>
				</div>
					<div class="">
						<?= $form->field($model, 'sort_order')->textInput(['style' => 'width:120px']) ?>
					</div>
					<div class="">
						<?php if(!$model->isNewRecord): ?>
							<label class="control-label has-star" for="">&nbsp;</label><br/>
							<?= (!$model->is_finished) ? Html::a(Yii::t('games', 'Finish tournament'), Url::to(['set-finished-tournament','id' => $model->id]), ['class' => 'btn btn-warning','data' => [
								'confirm' => Yii::t('games', 'Are you sure you want to finish this tournament?'),
							]]) : '' ?>
							<?= (!$model->is_banned) ? Html::button(Yii::t('games', 'Ban'), ['class' => 'btn btn-danger btn-ban','data-id' => $model->id]) :  '' ?>
						<?php endif; ?>
					</div>
				<hr/>
				<!-- For insert -->
				<?= $this->render('_parts/_localization_create.php', ['model' => $model]) ?>
				<div class="text-center">
					<?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-primary', 'style' => 'width:100%;margin: 25px 0']) ?>
				</div>
				<?php ActiveForm::end() ?>
			</div><!-- ./col-sm-3 -->
			<!-- right -->
			<div class="col-sm-9">
				<!-- For update -->
				<?= !$model->isNewRecord ? $this->render('_parts/_localization_update.php', ['model' => $model, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel]) : '' ?>
			</div>
		</div>
	</div>
</div>
<?= $this->render('/_parts/_banned_form.php',['title' => 'Are you sure you want to ban this tournament?' ]) ?>
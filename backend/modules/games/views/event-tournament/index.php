<?php

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\EventTournamentModel;
use yii\helpers\Html;
use yii\helpers\Url;
use common\widgets\GridView;

/* @var $this yii\web\View */
/* @var $searchModel backend\modules\games\models\EventTournamentSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $positioning string */
/* @var $sportList array */

$this->registerCssFile('/static/css/games.css');

if($positioning == 'all'){
	$this->title = Yii::t('games', 'Tournaments');
}else{
	echo $this->render('/_parts/_breadcrumbs.php', ['location' => Yii::t('games', 'Tournaments'), 'parent' => 'GameSports', 'parent_id' => $searchModel->sports_id]);
}

?>
<style>
	.btn-group .btn + .btn {
		margin-left: 1px;
	}
</style>
<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">
			<?= ($positioning == 'sport') ? Html::a('<span class="fa fa-angle-double-left"></span> ' . Yii::t('games', 'Games'), Url::to(['game-sports/index']), ['class' => 'btn btn-primary']) : '' ?>
		</div>
	</div>
	<!-- /.box-header -->
	<div class="box-body">
		<?php
		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'pjax' => true,
			'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
			'toolbar' => [
				['content' =>
					\backend\modules\games\widgets\NotificationButtons::widget() .
					Html::a('<span class="fa fa-refresh"></span>', Url::to(['event-tournament/index', 'sport_id' => $searchModel->sports_id]), ['class' => 'btn btn-warning', 'style' => 'margin-right:5px'])
					. $create = ($positioning == 'sport') ? Html::a('<span class="fa fa-plus"></span>', Url::to(['event-tournament/create', 'sport_id' => $searchModel->sports_id]), [
						'class' => 'btn btn-default',
						'data-pjax' => 0,
						'title' => Yii::t('games', 'Add tournament')
					]) : '',
				]],
			'columns' => [
				[
					'attribute' => 'id',
					'headerOptions' => ['width' => '80px'],
				],
				[
					'label' => Yii::t('games', 'Title'),
					'attribute' => 'tournament_title',
					'value' => function ($data) {
						return GameHelpers::getStringForLocale($data->eventTournamentLocales, 'title',false,true);

					}
				],
				[
					'label' => Yii::t('games', 'Game type'),
					'attribute' => 'sports_id',
					'filterType' => GridView::FILTER_SELECT2,
					'filterWidgetOptions' => [
						'theme' => 'default',
						'data' => $sportList,
						'options' => ['placeholder' => ''],
						'pluginOptions' => ['allowClear' => true],
					],
					'value' => function($data){
						return GameHelpers::getStringForLocale($data->gameSportsLocales,'title',false,true);
					},
					'visible' => ($positioning == 'all') ? true : false
				],
				[
					'attribute' => 'starts_at',
					'filterType' => GridView::FILTER_DATE_RANGE,
					'filterWidgetOptions' => [
						'startAttribute' => 'date_start',
						'endAttribute' => 'date_end',
						'convertFormat' => true,
						'hideInput' => true,
						'pluginOptions' => [
							'locale' => [
								'format' => 'd.m.Y',
								'separator' => ' - ',
							],
						],
						'pluginEvents'=>[
							'cancel.daterangepicker'=>"function(ev, picker) {\$('#eventtournamentsearch-starts_at-container input').val(''); \$('.grid-view').yiiGridView('applyFilter');}"
						]
					],
					'filter' => true,
					'value' => function ($model) {
						return Yii::$app->formatter->asDate($model->starts_at, 'php: d.m.Y H:i');
					},
					'format' => 'raw'
				],
				[
					'attribute' => 'is_enabled',
					'filter' => [Yii::t('games', 'No'), Yii::t('games', 'Yes')],
					'format' => 'raw',
					'value' => function ($model) {
						return ($model->is_enabled) ?
							Html::a(Yii::t('games', 'Disable'), Url::to(['change-status', 'id' => $model->id, 'status' => EventTournamentModel::STATUS_DISABLED]), ['class' => 'btn btn-danger btn-block btn-xs', 'data' => ['pjax' => 0]])
							: Html::a(Yii::t('games', 'Enable'), Url::to(['change-status', 'id' => $model->id, 'status' => EventTournamentModel::STATUS_ENABLED]), ['class' => 'btn btn-success btn-block btn-xs', 'data' => ['pjax' => 0]]);
					},
					'contentOptions' => ['style' => 'text-align:center']
				],
				[
					'attribute' => 'is_hidden',
					'filter' => [Yii::t('games', 'No'), Yii::t('games', 'Yes')],
					'value' => function ($model) {
						return ($model->is_hidden) ? '<span>' . Yii::t('games', 'Yes') . '</span>' : '<span>' . Yii::t('games', 'No') . '</span>';
					},
					'format' => 'raw',
					'contentOptions' => ['style' => 'text-align:center']
				],
				[
					'attribute' => 'is_finished',
					'filter' => [Yii::t('games', 'No'), Yii::t('games', 'Yes')],
					'value' => function ($model) {
						return ($model->is_finished) ? '<span  class="">' . Yii::t('games', 'Yes') . '</span>' : '<span>' . Yii::t('games', 'No') . '</span>';
					},
					'format' => 'raw',
					'contentOptions' => ['style' => 'text-align:center']
				],
				[
					'attribute' => 'is_banned',
					'filter' => [Yii::t('games', 'No'), Yii::t('games', 'Yes')],
					'value' => function ($model) {
						return ($model->is_banned) ? '<span class="">' . Yii::t('games', 'Yes') . '</span>' : '<span>' . Yii::t('games', 'No') . '</span>';
					},
					'format' => 'raw',
					'contentOptions' => ['style' => 'text-align:center']
				],
				[
					'attribute' => 'sort_order',
					'filter' => false,
					'contentOptions' => ['style' => 'text-align:center'],
					'headerOptions' => ['width' => '100px'],
				],
				[
					'content' => function ($data) {
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'tournament_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-success'
						]);
					},
					'headerOptions' => ['width' => '20px'],
				],
				[
					'content' => function ($data) {
						return Html::a(Yii::t('games', 'Games'), ['event-game/index', 'tournament_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-primary'
						]);
					}
				]
			],
		]);

		?>

	</div>
</div>



<?php

use backend\modules\games\components\GameHelpers;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsModel;
use common\widgets\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel \common\modules\games\models\GameMarketsModel */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $gameMarketsGroupsModel \common\modules\games\models\GameMarketsModel */
/* @var $markersList array */

$this->registerCssFile('/static/css/games.css');

/*$this->title = Yii::t('games', 'Markets');
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['game-sports/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Market groups'), 'url' => ['game-markets-groups/index','sport_id' => $gameMarketsGroupsModel->sports_id]];
$this->params['breadcrumbs'][] = $this->title;*/

echo $this->render('/_parts/_breadcrumbs.php', ['location' => Yii::t('games', 'Markets'), 'parent' => 'GameMarketsGroups', 'parent_id' => $gameMarketsGroupsModel->id]);
?>

<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">
			<?= Html::a('<span class="fa fa-angle-double-left"></span> ' . Yii::t('games', 'Games'), Url::to(['/games/game-sports/index']), ['class' => 'btn btn-primary']) ?>
			<?= Html::a('<span class="fa fa-angle-double-left"></span> ' . Yii::t('games', 'Market groups'), Url::to(['game-markets-groups/index', 'sport_id' => $gameMarketsGroupsModel->sports_id]), ['class' => 'btn btn-primary']) ?>
		</div>
	</div>
	<div class="box-body">
		<?php
		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'pjax' => false,
			'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
			'toolbar' => [
				[
					'content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['game-markets/index', 'group_id' => $gameMarketsGroupsModel->id]), [
							'class' => 'btn btn-warning',
							'style' => 'margin-right:5px',
							'title' => Yii::t('games', 'Reset filter')
						])
						. Html::a('<span class="fa fa-plus"></span>', Url::to(['game-markets/create', 'group_id' => $gameMarketsGroupsModel->id]), [
							'class' => 'btn btn-default',
							'data-pjax' => 0,
							'title' => Yii::t('games', 'Add market')
						])
				]
			],
			'columns' => [
				[
					'attribute' => 'id',
					'headerOptions' => ['width' => '80px'],
				],
				[
					'class' => 'kartik\grid\CheckboxColumn',
					'headerOptions' => ['class' => 'kartik-sheet-style '],
				],
				[
					'label' => Yii::t('games', 'Title'),
					'headerOptions' => ['width' => '50%'],
					'attribute' => 'market_title',
					'value' => function ($data) {
						return \backend\modules\games\components\GameHelpers::getStringForLocale($data->gameMarketsLocales, 'title',false,true);

					}
				],
				[
					'attribute' => 'is_enabled',
					'filter' => GameMarketsModel::statuses(),
					'format' => 'raw',
					'value' => function ($data) {
						return ($data->is_enabled) ?
							Html::a(Yii::t('games','Disable'),Url::to(['change-status','id' => $data->id,'status' => GameMarketsModel::STATUS_DISABLED]),['class' => 'btn btn-danger btn-block btn-xs','data' => ['pjax' => 0]])
							: Html::a(Yii::t('games','Enable'),Url::to(['change-status','id' => $data->id,'status' => GameMarketsModel::STATUS_ENABLED]),['class' => 'btn btn-success btn-block btn-xs','data' => ['pjax' => 0]]);
					},
					'headerOptions' => ['width' => '120px'],
				],
				[
					'attribute' => 'is_main',
					'filter' => [Yii::t('games', 'No'), Yii::t('games', 'Yes')],
					'value' => function ($data) {
						return ($data->is_main) ? '<span>' . Yii::t('games', 'Yes') . '</span>' : '<span class="text-danger">' . Yii::t('games', 'No') . '</span>';
					},
					'format' => 'raw'
				],
				[
					'attribute' => 'sort_order',
					'filter' => false
				],
				[
					'content' => function ($data) {
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'market_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-success'
						]);
					},
					'headerOptions' => ['width' => '20px'],
				],
				[
					'content' => function ($data) {
						return Html::a(Yii::t('games', 'Outcome'), ['/games/game-outcomes', 'market_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-primary'
						]);
					}
				]
			],
		]);
		?>
		<div class="marge-market row" style="display: none">
			<?= Html::beginForm('merge-markets','post',['id' => 'form-merge-market']) ?>
			<div class="col-sm-3 col-sm-offset-1">
				<h4><?= Yii::t('games', 'Merge highlighted with market:') ?></h4>
			</div>
			<div class="col-sm-2">
				<?= Select2::widget([
					'name' => 'target_market',
					'id' => 'target_market',
					'data' => $markersList,
					'options' => ['placeholder' => 'Select market ...'],
					'pluginOptions' => [
						'allowClear' => true
					],
				]); ?>
			</div>
			<div class="col-sm-2">
				<?= Html::input('text','selected-markets',null,['id' => 'selected-markets','style' => 'display:none']) ?>
				<?= Html::button(Yii::t('games', 'Merge'), ['class' => 'btn btn-warning btn-md','id' => 'send-form']) ?>
			</div>
			<?= Html::endForm() ?>
		</div>
	</div>
</div>
<?php
$js = <<<JS
	/* fill form and send */
	$('button#send-form').on('click',function(e) {
	  	e.preventDefault();
	  	var values=[];
	  	$('table td.kv-row-select input[type="checkbox"]').map(function(idx,el) {
	  	  	if($(el).prop('checked')){
	  	  		values.push($(el).val());	  	  		
	  	  	}
	  	});
	  	$('input#selected-markets').val(JSON.stringify(values));
	  	
	  	/* is parent market selected */
	  	if(!$('select#target_market').val()){
	  		$('form#form-merge-market .select2-container--krajee .select2-selection').css('border','2px solid red');
	  		return false;
	  	}
	  	
	  	$('form#form-merge-market').submit();
	})

	/* switch for form */
	$('table td.kv-row-select input[type="checkbox"],input[type="checkbox"].select-on-check-all').on('change',function() {
	  	var checkbox = $(this);
		if(checkbox.prop('checked')){
			$('div.box-body .marge-market').show(); 	
		}else{
			var mark = true;
			$('table td.kv-row-select input[type="checkbox"]').map(function(idx,el) {
			  	if($(el).prop('checked')){
			  		mark = false;
			  	}
			});
			if(mark){
				$('div.box-body .marge-market').hide();
			}
		}
	});
	
	/* for select */
	$('select#target_market').on('change',function(){
		$('form#form-marge-market .select2-container--krajee .select2-selection').css('border','1px solid #ccc');
	}); 
JS;
$this->registerJs($js);
?>

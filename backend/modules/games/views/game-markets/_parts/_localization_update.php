<?php

/* @var $this \yii\web\View */

use backend\modules\games\components\GameHelpers;
use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model \common\modules\games\models\GameMarketsModel */
/* @var $searchModel  \backend\modules\games\models\search\GameMarketsSearch */
/* @var $dataProvider \yii\data\ArrayDataProvider */
?>
	<h4><span class="fa fa-language text-warning"></span> <?= Yii::t('games', 'Localization:') ?></h4>

<?php
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'toolbar' => [
		['content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['game-markets/update','market_id' => $model->id]), ['class' => 'btn btn-warning'])],
	],
	'columns' => [
		[
			'attribute' => 'id',
			'headerOptions' => ['width' => '80px'],
		],
		[
			'attribute' => 'title',
		],
		[
			'attribute' => 'lang_code',
			'headerOptions' => ['width' => '80px'],
		],
		[
			'class' => 'yii\grid\ActionColumn',
			'template' => '{locale-update}',
			'buttons' => [
				'locale-update' => function($url, $model, $key){
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,['data-pjax' => '0']);
				}
			]
		],
	],
]);


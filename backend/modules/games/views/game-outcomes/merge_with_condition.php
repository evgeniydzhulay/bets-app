<?php
/* @var $this \yii\web\View */

use backend\modules\games\components\GameHelpers;
use yii\helpers\Html;

/* @var $targetModel null|static */
/* @var $models array|\common\modules\games\models\GameOutcomesModel[] */
/* @var $selectedOutcomes array|mixed */
?>

<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">

		</div>
	</div>
	<div class="box-body">
		<h3 class="text-danger">
			В исходе <b><?= !empty($targetModel->gameOutcomesLocales[1]->title) ? $targetModel->gameOutcomesLocales[1]->title : $targetModel->gameOutcomesLocales[0]->title ?></b> нет условия! Выберите из какого исхода взять условие:
		</h3>
		<?php if(!empty($models)): ?>
			<?= Html::beginForm() ?>
			<table class="table table-hover">
				<tr>
					<th></th>
					<th><?= Yii::t('games', 'Title') ?></th>
					<th><?= Yii::t('games', 'Condition') ?></th>
				</tr>

				<tbody>
				<?php foreach($models as $model): ?>
					<tr>
						<td  class="text-center"><?= Html::radio('selected', false,['value' => $model->id]) ?></td>
						<td><?= GameHelpers::getStringForLocale($model->gameOutcomesLocales, 'title', false, true) ?></td>
						<td><?= $model->condition ?></td>
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
			<div class="text-center">
			<?= \yii\bootstrap\Html::submitButton(Yii::t('games','Save'),['class' => 'btn btn-primary']) ?>
			</div>
			<?= Html::hiddenInput('target_outcome',$targetModel->id) ?>
			<?= Html::hiddenInput('selected-outcomes',$selectedOutcomes) ?>
			<?= Html::endForm() ?>
		<?php endif; ?>
		<p>&nbsp;</p>
	</div>
</div>

<?php
/**
 * @var $model \common\modules\games\models\GameMarketsModel
 * @var $this \yii\web\View
 */

use backend\modules\games\components\GameHelpers;
use yii\helpers\Html;

$marker = '';
if($model->isNewRecord){
	$languages = GameHelpers::languagesList();
}else{
	$languages = GameHelpers::findExceptionInLanguages(\common\modules\games\models\GameOutcomesLocaleModel::class,'outcome_id',$model->id);
	$marker = !empty($languages) ? 'class="text-danger"' : '';
}
?>

<?php if(!empty($languages)): ?>
	<h4 <?= $marker ?>><span class="fa fa-language text-warning"></span> <?= Yii::t('games', 'Localization:') ?></h4>
<div class="">
		<div>
			<label><?= Yii::t('games', 'Apply to participant:') ?></label><br/>
			<?= Html::input('number', 'participant', null, ['id' => 'participant-numb','min' => 1,'style' => 'height: 29px;width: 60px;text-align:center']) ?>
			<?= Html::button('<span class="fa fa-plus"></span>',['class' => 'btn btn-default','id' => 'add-item']) ?>
		</div>
	<?php foreach($languages as $lang => $label): ?>
	<div class="form-group lang-list">
		<label for="<?= $lang ?>"><?= $label ?></label>
		<?php $required = ($lang == 'en') ? ['required' => 'required'] : [] ?>
		<?= Html::textInput("locale[{$lang}]",null,['id' => $lang,'class' => 'form-control'] + $required) ?>
	</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>

<?php
$js = <<<JS
	$('button#add-item').on('click',function() {
	  var value = $('input#participant-numb').val();
	  if(value){
	  		$('.lang-list input').map(function(idx,el) {
	  		  $(el).val('{item' + value + '}' + $(el).val());
	  		})
	  }
	})
JS;
$this->registerJs($js,$this::POS_READY)
?>
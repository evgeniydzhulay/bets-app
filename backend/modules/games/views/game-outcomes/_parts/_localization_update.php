<?php

/* @var $this \yii\web\View */

use common\widgets\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $model \common\modules\games\models\GameOutcomesModel */
/* @var $searchModel  \backend\modules\games\models\search\GameOutcomesSearch */
/* @var $dataProvider \yii\data\ArrayDataProvider */
?>
	<h4><span class="fa fa-language text-warning"></span> <?= Yii::t('games', 'Localization:') ?></h4>

<?php
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'toolbar' => [
		['content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['game-outcomes/update','outcome_id' => $model->id]), ['class' => 'btn btn-warning'])],
	],
	'columns' => [
		[
			'attribute' => 'id',
			'headerOptions' => ['width' => '80px'],
		],
		[
			'attribute' => 'title',
		],
		[
			'attribute' => 'lang_code',
			'headerOptions' => ['width' => '80px'],
		],
		[
			'class' => 'yii\grid\ActionColumn',
			'template' => '{locale-update}',
			'buttons' => [
				'locale-update' => function($url, $model, $key){
					return Html::a('<span class="glyphicon glyphicon-pencil"></span>',$url,['data-pjax' => '0']);
				}
			]
		],
	],
]);


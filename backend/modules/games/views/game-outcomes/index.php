<?php

use common\modules\games\models\GameOutcomesModel;
use common\widgets\GridView;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel GameOutcomesModel */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $marketModel \common\modules\games\models\GameMarketsModel */
/* @var $outcomesList array */

$this->registerCssFile('/static/css/games.css');

/*$this->title = Yii::t('games', 'Outcomes');
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['game-sports/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Market groups'), 'url' => ['game-markets-groups/index','sport_id' => $marketModel->sports_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Markets'), 'url' => ['game-markets/index','group_id' => $marketModel->group_id]];
$this->params['breadcrumbs'][] = $this->title;*/

echo $this->render('/_parts/_breadcrumbs.php',['location' => Yii::t('games','Outcomes'),'parent' => 'GameMarkets','parent_id' => $marketModel->id]);
?>

<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">
			<?= Html::a('<span class="fa fa-angle-double-left"></span> ' . Yii::t('games', 'Games'), Url::to(['/games/game-sports/index']), ['class' => 'btn btn-primary']) ?>
			<?= Html::a('<span class="fa fa-angle-double-left"></span> ' . Yii::t('games', 'Market groups'), Url::to(['game-markets-groups/index','sport_id' => $marketModel->sports_id]), ['class' => 'btn btn-primary']) ?>
			<?= Html::a('<span class="fa fa-angle-double-left"></span> ' . Yii::t('games', 'Markets'), Url::to(['game-markets/index','group_id' => $marketModel->group_id]), ['class' => 'btn btn-primary']) ?>
		</div>
	</div>
	<div class="box-body">
		<?php
		echo GridView::widget([
			'dataProvider' => $dataProvider,
			'filterModel' => $searchModel,
			'id' => 'grid',
			'pjax' => true,
			'tableOptions' => ['class' => 'kv-grid-table table table-bordered kv-table-wrap table-hover'],
			'toolbar' => [
				[
					'content' => Html::a('<span class="fa fa-refresh"></span>', Url::to(['game-outcomes/index','market_id' => $marketModel->id]), [
							'class' => 'btn btn-warning',
							'style' => 'margin-right:5px',
							'title' => Yii::t('games', 'Reset filter')
						])
						. Html::a('<span class="fa fa-plus"></span>', Url::to(['game-outcomes/create', 'market_id' => $marketModel->id]), [
							'class' => 'btn btn-default',
							'data-pjax' => 0,
							'title' => Yii::t('games', 'Add market')
						])
				]
			],
			'columns' => [
				[
					'attribute' => 'id',
					'headerOptions' => ['style' => 'width:60px'],
				],
				[
					'class' => 'kartik\grid\CheckboxColumn',
					'headerOptions' => ['class' => 'kartik-sheet-style '],
				],
				[
					'label' => Yii::t('games', 'Title'),
					'attribute' => 'outcome_title',
					'headerOptions' => [],
					'value' => function ($data){
						return \backend\modules\games\components\GameHelpers::getStringForLocale($data->gameOutcomesLocales,'title',false,true);

					}
				],
				[
					'attribute' => 'is_enabled',
					'filter' => GameOutcomesModel::statuses(),
					'format' => 'raw',
					'value' => function ($data) {
						return ($data->is_enabled) ?
							Html::a(Yii::t('games','Disable'),Url::to(['change-status','id' => $data->id,'status' => GameOutcomesModel::STATUS_DISABLED]),['class' => 'btn btn-danger btn-block btn-xs','data' => ['pjax' => 0]])
							: Html::a(Yii::t('games','Enable'),Url::to(['change-status','id' => $data->id,'status' => GameOutcomesModel::STATUS_ENABLED]),['class' => 'btn btn-success btn-block btn-xs','data' => ['pjax' => 0]]);
					},
					'headerOptions' => ['style' => 'width:120px'],
				],
				[
					'attribute' => 'is_main',
					'filter' => [Yii::t('games', 'No'), Yii::t('games', 'Yes')],
					'value' => function ($data) {
						return ($data->is_main) ? '<span>' . Yii::t('games', 'Yes') . '</span>' : '<span class="text-danger">' . Yii::t('games', 'No') . '</span>';
					},
					'format' => 'raw'
				],
				[
					'attribute' => 'short_title',
				],
				[
					'attribute' => 'sort_order',
					'headerOptions' => ['style' => 'width:160px'],
					'contentOptions' => ['class' => 'text-center']
				],
				[
					'content' => function ($data){
						return Html::a('<span class="glyphicon glyphicon-pencil"></span>', ['update', 'outcome_id' => $data->id], [
							'data-pjax' => 0,
							'class' => 'btn btn-block btn-xs btn-success'
						]);
					},
					'headerOptions' => ['style' => 'width:20px'],
				],
			],
		]);

		?>
		<div class="marge-outcomes row" style="display: none">
			<?= Html::beginForm(Url::to(['/games/game-outcomes/merger-outcomes']),'post',['id' => 'form-merger-outcomes']) ?>
			<div class="col-sm-3 col-sm-offset-1">
				<h4><?= Yii::t('games', 'Merge highlighted with outcome:') ?></h4>
			</div>
			<div class="col-sm-2">
				<?= Select2::widget([
					'name' => 'target_outcome',
					'id' => 'target_outcome',
					'data' => $outcomesList,
					'options' => ['placeholder' => 'Select outcome ...'],
					'pluginOptions' => [
						'allowClear' => true
					],
				]); ?>
			</div>
			<div class="col-sm-2">
				<?= Html::input('text','selected-outcomes',null,['id' => 'selected-outcomes','style' => 'display:none']) ?>
				<?= Html::submitButton(Yii::t('games', 'Merge'), ['class' => 'btn btn-warning btn-md','id' => 'send-form']) ?>
			</div>
			<?= Html::endForm() ?>
		</div>

	</div>
</div>
<?php
$js = <<<JS
	/* fill form and send */
	$('form#form-merger-outcomes').on('submit', function() {
		var keys = $('#grid').yiiGridView('getSelectedRows');
   	$('input#selected-outcomes').attr('value', JSON.stringify(keys));
   	if(!$('select#target_outcome').val()){
	  		$('form#form-merger-outcomes .select2-container--krajee .select2-selection').css('border','2px solid red');
	  		return false;
	  	}
	  	return true;
	});

	/* switch for form */
	$('table td.kv-row-select input[type="checkbox"],input[type="checkbox"].select-on-check-all').on('change',function() {
	  	var checkbox = $(this);
		if(checkbox.prop('checked')){
			$('div.box-body .marge-outcomes').show(); 	
		}else{
			var mark = true;
			$('table td.kv-row-select input[type="checkbox"]').map(function(idx,el) {
			  	if($(el).prop('checked')){
			  		mark = false;
			  	}
			});
			if(mark){
				$('div.box-body .marge-outcomes').hide();
			}
		}
	});
	/* for select */
	$('select#target_market').on('change',function(){
		$('form#form-marge-market .select2-container--krajee .select2-selection').css('border','1px solid #ccc');
	}); 
	

JS;
$this->registerJs($js);
?>

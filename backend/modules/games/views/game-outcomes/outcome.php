<?php
/**
 * @var $this \yii\web\View
 * @var $model \common\modules\games\models\GameOutcomesModel
 * @var $searchModel \backend\modules\games\models\search\GameOutcomesSearch
 * @var $dataProvider \yii\data\ActiveDataProvider,
 * @var $market \common\modules\games\models\GameMarketsModel
 */

use common\modules\games\models\GameMarketsModel;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/*$this->title = $model->isNewRecord ? Yii::t('games', 'Add outcome') : Yii::t('games', 'Update outcome');
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Games'), 'url' => ['/games/game-sports/index']];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Market groups'), 'url' => ['game-markets-groups/index', 'sport_id' => (!empty($market)) ? $market->sports_id : $model->sports_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Markets'), 'url' => ['game-markets/index', 'group_id' => (!empty($market)) ? $market->group_id : $model->group_id]];
$this->params['breadcrumbs'][] = ['label' => Yii::t('games', 'Outcomes'), 'url' => ['game-outcomes/index', 'market_id' => (!empty($market)) ? $market->id : $model->market_id]];
$this->params['breadcrumbs'][] = $this->title;*/

$location = $model->isNewRecord ? Yii::t('games', 'Add outcome') : Yii::t('games', 'Update outcome');
echo $this->render('/_parts/_breadcrumbs.php',['location' => $location,'parent' => 'GameMarkets','parent_id' => (!empty($market)) ? $market->id : $model->market_id]);

?>
<div class="box box-primary">
	<div class="box-header with-border" style="min-height:43px;">
		<div class="box-tools pull-right">
			<?= Html::a(Yii::t('games', 'Close'), Url::to(['index', 'market_id' => (!empty($market)) ? $market->id : $model->market_id]), ['class' => 'btn btn-warning']) ?>
		</div>
		<!-- /.box-tools -->
	</div>
	<!-- /.box-header -->
	<div class="box-body" style="padding:15px">
		<div class="row">
			<!-- block left -->
			<div class="col-sm-3">
				<?php $form = ActiveForm::begin() ?>
				<h4><span class="fa fa-gear text-warning"></span> <?= Yii::t('games', 'Settings:') ?></h4>
				<!-- is enabled -->
				<div class="">
					<?php $data = [Yii::t('games', 'Disabled'), Yii::t('games', 'Enabled')]; ?>
					<?php if($model->isNewRecord) $model->is_enabled = GameMarketsModel::STATUS_ENABLED ?>
					<label class="control-label has-star" for=""><?= Yii::t('games', 'Status') ?></label>
					<?= $form->field($model, 'is_enabled')->radioButtonGroup($data, ['itemOptions' => ['labelOptions' => ['class' => 'btn btn-default', 'style' => 'width:80px']]])->label(false) ?>
				</div>
				<div class="">
					<?php $data = [Yii::t('games', 'No'), Yii::t('games', 'Yes')]; ?>
					<?php if($model->isNewRecord) $model->is_main = 0 ?>
					<label class="control-label has-star" for=""><?= Yii::t('games', 'Is main') ?>?</label>
					<?= $form->field($model, 'is_main')->radioButtonGroup($data, ['itemOptions' => ['labelOptions' => ['class' => 'btn btn-default','style' => 'width:80px']]])->label(false) ?>
				</div>
				<div class="">
					<?= $form->field($model, 'short_title')->textInput() ?>
				</div>
				<div class="">
					<?= $form->field($model, 'sort_order')->textInput(['style' => 'width:120px']) ?>
				</div>
				<hr/>
				<!-- For insert -->
				<?= $this->render('_parts/_localization_create.php', ['model' => $model]) ?>
				<div class="text-center">
					<?= Html::submitButton(Yii::t('games', 'Save'), ['class' => 'btn btn-primary', 'style' => 'width:100%;margin: 25px 0']) ?>
				</div>
				<?php ActiveForm::end() ?>
			</div><!-- ./col-sm-3 -->
			<!-- right -->
			<div class="col-sm-9">
				<!-- For update -->
				<?= !$model->isNewRecord ? $this->render('_parts/_localization_update.php', ['model' => $model, 'dataProvider' => $dataProvider, 'searchModel' => $searchModel]) : '' ?>
			</div>
		</div>
	</div>
</div>

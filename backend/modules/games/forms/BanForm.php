<?php

namespace backend\modules\games\forms;

use common\modules\games\models\BetsModel;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\EventTournamentModel;
use yii\base\Model;

class BanForm extends Model {

	public $comment;

	/** @inheritdoc */
	public function rules() {
		return [
			['comment', 'trim'],
			['comment', 'required'],
		];
	}

	public function formName () {
		return '';
	}

	public function banTournament(EventTournamentModel $model) {
		return $model->ban($this->comment);
	}

	public function banGame(EventGameModel $model) {
		return $model->ban($this->comment);
	}

	public function banOutcome(EventOutcomesModel $model) {
		return $model->ban($this->comment);
}

	public function banBet(BetsModel $model) {
		return $model->ban($this->comment);
	}
}
<?php

namespace backend\modules\blog\models;

use common\modules\blog\models\PostModel;
use common\modules\blog\models\TagModel;
use Yii;
use yii\base\Model;
use yii\web\UploadedFile;
use yii\helpers\ArrayHelper;

/**
 * Form for comments saving and updating
 *
 * @property PostModel $_model Post item
 */
class PostForm extends Model {

    public $key;
    public $title;
    public $content;
    public $image;
    public $lang_code;
    public $tags = [];
    public $active_from;

    protected $_model;

    public function getId() {
        return $this->_model->id;
    }

    public function init() {
	    $this->active_from = date('Y-m-d H:i', time());
	    parent::init();
    }

    public function setModel(PostModel $model) {
	    $this->_model = $model;
	    $this->_model->scenario = $this->scenario;
	    if (!$this->_model->getIsNewRecord()) {
		    $this->setAttributes($this->_model->getAttributes());
		    $this->active_from = date('Y-m-d H:i', $this->active_from);
		    $this->lang_code = $this->_model->lang_code;
		    $this->setAttributes(['tags' => ArrayHelper::map($this->_model->tagRelation, 'tag_id', 'tag_id')], false);
	    } else {
		    $this->active_from = date('Y-m-d H:i', time());
	    }
    }

    /**
     * Attribute labels
     * @return array
     */
    public function attributeLabels() {
        return [
        	'active_from'  => Yii::t('blog', 'Active from'),
	        'key'  => Yii::t('blog', 'Key'),
	        'title'  => Yii::t('blog', 'Title'),
	        'content'  => Yii::t('blog', 'Content'),
	        'image'  => Yii::t('blog', 'Image'),
	        'tags'  => Yii::t('blog', 'Tags'),
	        'lang_code'  => Yii::t('blog', 'Language'),
        ];
    }

    /**
     * Validation rules
     * @return array
     */
    public function rules() {
        $rules = $this->_model->rules();
        $rules[] = ['tags', 'exist', 'targetClass' => TagModel::class, 'targetAttribute' => 'id', 'allowArray' => true];
        $rules['keyExists'] = ['key', 'unique', 'targetClass' => PostModel::class, 'targetAttribute' => ['key', 'lang_code'], 'when' => function($model) {
            return $model->key != $this->_model->key;
        }];
        return $rules;
    }

    public function scenarios() {
        $scenarios = $this->_model->scenarios();
        $scenarios['create'][] = 'tags';
        $scenarios['update'][] = 'tags';
        return $scenarios;
    }

    public function save() {
        if ($this->validate()) {
        	$this->_model->setAttributes([
        		'key' => $this->key,
		        'lang_code' => $this->lang_code,
		        'title' => $this->title,
		        'content' => $this->content,
		        'active_from' => strtotime($this->active_from),
	        ]);
            if(($image = $this->getImage()) && ($result = $this->_model->saveImage($image))) {
                $this->_model->image = $result;
            }
            if ($this->_model->save(false)) {
                foreach ($this->_model->tagRelation AS $tag) {
                    $tag->delete();
                }
                if (is_array($this->tags)) {
                    foreach ($this->tags AS $tag) {
                        $this->_model->addTag($tag);
                    }
                }
                return $this->_model;
            }
        }
        return false;
    }

    public function getImageUrl() {
    	return $this->_model->getImageUrl();
    }

    protected function getImage() {
        return UploadedFile::getInstance($this, 'image');
    }

}

<?php

namespace backend\modules\blog\controllers;

use backend\modules\blog\models\PostForm;
use common\modules\blog\models\PostModel;
use common\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class PostController extends Controller {

	use AjaxValidationTrait;

	/** @inheritdoc */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index'],
						'roles' => ['blog-view']
					],
					[
						'allow' => true,
						'actions' => ['create', 'update'],
						'roles' => ['blog-edit'],
					],
				]
			]
		];
	}

	/** @inheritdoc */
    public function beforeAction($action) {
        Yii::$app->view->params['breadcrumbs'][] = Yii::t('blog', 'Blog');
        if($action->id == 'index') {
            Yii::$app->view->params['breadcrumbs'][] = Yii::t('blog', 'Posts');
        } else {
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => Yii::t('blog', 'Posts'),
                'url' => ['index']
            ];
        }
        return parent::beforeAction($action);
    }

	/**
	 * Lists all created items.
	 * @return string
	 */
	public function actionIndex() {
		$filterModel = Yii::createObject([
			'class' => PostModel::class
		]);
		$filterModel->scenario = PostModel::SCENARIO_SEARCH;
		return $this->render('index', [
			'filterModel' => $filterModel,
			'dataProvider' => $filterModel->search(Yii::$app->request->get()),
		]);
	}

    public function actionCreate() {
	    $model = Yii::createObject([
		    'class' => PostForm::class,
	    ]);
	    $model->scenario = PostModel::SCENARIO_CREATE;
	    $model->setModel(Yii::createObject(PostModel::class));
	    $this->performAjaxValidation($model);
	    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		    return $this->redirect(['update', 'id' => $model->id]);
	    }
	    Yii::$app->view->title = Yii::t('blog', 'Create post');
	    return $this->render('item', [
		    'model' => $model,
	    ]);
    }

    public function actionUpdate($id) {
	    $model = Yii::createObject([
		    'class' => PostForm::class,
	    ]);
	    $model->scenario = PostModel::SCENARIO_UPDATE;
	    $model->setModel($this->getItem($id));
	    $this->performAjaxValidation($model);
	    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		    return $this->refresh();
	    }
	    Yii::$app->view->title = Yii::t('blog', 'Edit post {title}', ['title' => $model->title]);
	    return $this->render('item', [
		    'model' => $model,
	    ]);
    }

	/**
	 * Deletes item.
	 * @param  int $id Item id
	 * @return Response
	 * @throws \Throwable
	 */
	public function actionDelete($id) {
		$item = $this->getItem($id);
		$item->delete();
		return $this->redirect(['index']);
	}

    protected function getItem($id) {
        $item = PostModel::findOne(['id' => $id]);
        if (!$item) {
	        throw new \yii\web\NotFoundHttpException(Yii::t('blog', 'The requested post does not exist'));
        }
	    return $item;
    }

}

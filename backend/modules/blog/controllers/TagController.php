<?php

namespace backend\modules\blog\controllers;

use common\modules\blog\models\TagModel;
use common\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

class TagController extends Controller {

	use AjaxValidationTrait;

	/** @inheritdoc */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'actions' => ['index'],
						'roles' => ['blog-view']
					],
					[
						'allow' => true,
						'actions' => ['create', 'update'],
						'roles' => ['blog-edit'],
					],
				]
			]
		];
	}

	/** @inheritdoc */
    public function beforeAction($action) {
        Yii::$app->view->params['breadcrumbs'][] = Yii::t('blog', 'Blog');
        if($action->id == 'index') {
            Yii::$app->view->params['breadcrumbs'][] = Yii::t('blog', 'Tags');
        } else {
            Yii::$app->view->params['breadcrumbs'][] = [
                'label' => Yii::t('blog', 'Tags'),
                'url' => ['index']
            ];
        }
        return parent::beforeAction($action);
    }

	/**
	 * Lists all created items.
	 * @return string
	 */
	public function actionIndex() {
		$filterModel = Yii::createObject([
			'class' => TagModel::class
		]);
		$filterModel->scenario = TagModel::SCENARIO_SEARCH;
		return $this->render('index', [
			'filterModel' => $filterModel,
			'dataProvider' => $filterModel->search(Yii::$app->request->get()),
		]);
	}

	public function actionCreate() {
		$model = Yii::createObject([
			'class' => TagModel::class,
		]);
		$model->scenario = TagModel::SCENARIO_CREATE;
		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['update', 'id' => $model->id]);
		}
		Yii::$app->view->title = Yii::t('blog', 'Create tag');
		return $this->render('item', [
			'model' => $model,
		]);
	}

	public function actionUpdate($id) {
		$model = $this->getItem($id);
		$model->scenario = TagModel::SCENARIO_UPDATE;
		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->refresh();
		}
		Yii::$app->view->title = Yii::t('blog', 'Edit tag {title}', ['title' => $model->title]);
		return $this->render('item', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes item.
	 * @param  int $id Item id
	 * @return Response
	 * @throws \Throwable
	 */
	public function actionDelete($id) {
		$item = $this->getItem($id);
		$item->delete();
		return $this->redirect(['index']);
	}

    protected function getItem($id) {
        $item = TagModel::findOne(['id' => $id]);
        if (!$item) {
	        throw new \yii\web\NotFoundHttpException(Yii::t('blog', 'The requested tag does not exist'));
        }
	    return $item;
    }

}

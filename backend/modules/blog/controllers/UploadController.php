<?php

namespace backend\modules\blog\controllers;

use backend\modules\blog\actions\FileUploadAction;
use backend\modules\blog\actions\ImageUploadAction;
use yii\filters\AccessControl;
use yii\web\Controller;

class UploadController extends Controller {

	public $enableCsrfValidation = false;

	/** @inheritdoc */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'actions' => ['file', 'image'],
						'roles' => ['blog-edit'],
					],
				]
			]
		];
	}

	/**
	 * List of available upload actions
	 *
	 * @return array
	 */
	public function actions() {
		return [
			'file' => [
				'class' => FileUploadAction::class,
			],
			'image' => [
				'class' => ImageUploadAction::class,
			]
		];
	}
}

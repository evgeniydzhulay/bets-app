<?php

use common\modules\blog\models\TagModel;
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/**
 * @var $this  yii\web\View
 * @var $model TagModel
 */

$this->params['breadcrumbs'][] = $this->title;

$form = ActiveForm::begin([
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'horizontalCssClasses' => [
                    'wrapper' => 'col-sm-9',
                ],
            ],
        ])
?>
<?= $form->field($model, 'key') ?>
<?= $form->field($model, 'title') ?>
<?= $form->field($model, 'content')->widget(\backend\modules\blog\CKEditor::class, [
    'clientOptions' => [
        'lang' => Yii::$app->language,
        'minHeight' => 200,
        'plugins' => [
            'fontsize',
            'fontcolor',
            'fontfamily',
            'table',
            'counter',
            'fullscreen',
            'imagemanager'
        ],
    ]
])
?>
<?= Html::submitButton(Yii::t('content', 'Save'), ['class' => 'btn btn-success btn-block']) ?>
<?php ActiveForm::end() ?>

<?php

use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;
use kartik\file\FileInput;
use common\modules\blog\models\TagModel;

/**
 * @var $this  yii\web\View
 */
$this->params['breadcrumbs'][] = $this->title;
?>
<?php
$form = ActiveForm::begin([
    //'layout' => 'horizontal',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => ['enctype' => 'multipart/form-data'],
    'fieldConfig' => [
        'horizontalCssClasses' => [
            'label' => 'col-md-2',
            'wrapper' => 'col-md-10',
        ],
    ],
])
?>
<div class="row">
    <div class="col-md-9">
        <div class="box box-default">
            <div class="box-body">
                <div class="row">
                    <div class="col-md-8">
	                    <?= $form->field($model, 'key') ?>
                    </div>
                    <div class="col-md-4">
		                <?= $form->field($model, 'lang_code')->widget(Select2::class, [
                            'theme' => 'default',
                            'data' => \yii\helpers\ArrayHelper::getColumn(Yii::$app->params['languages'], 'label', true),
                            'pluginEvents' => [
                                "change" => "function() { $('#postform-tags').val('').trigger('change'); }",
                            ],
                            'options' => [
                                'placeholder' => Yii::t('blog', 'Select language'),
                            ]
                        ]); ?>
                    </div>
                </div>
                <?= $form->field($model, 'title') ?>
                <?=
                $form->field($model, 'content')->widget(\backend\modules\blog\CKEditor::class)?>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="box box-default">
            <div class="box-header with-border">
                <?= Yii::t('blog', 'Publication options'); ?>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <?=
                $form->field($model, 'active_from', [
                    'template' => $model->getAttributeLabel('active_from') . '{input}{hint}{error}'
                ])->widget(kartik\datetime\DateTimePicker::class);
                ?>
                <?=
                $form->field($model, 'tags', [
	                'template' => '{input}{hint}{error}'
                ])->widget(Select2::class, [
	                'data' => ArrayHelper::map(TagModel::find()->orderBy(['title' => SORT_ASC])->asArray()->all(), 'id', 'title'),
	                'options' => [
		                'placeholder' => Yii::t('blog', 'Select tags'),
		                'multiple' => true,
	                ],
	                'theme' => 'default'
                ]);
                ?>
            </div>
        </div>
        <div class="box box-default">
            <div class="box-header with-border">
                <?= $model->getAttributeLabel('image'); ?>
                <div class="box-tools pull-right">
                    <button type="button" class="btn btn-box-tool" data-widget="collapse">
                        <i class="fa fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="box-body">
                <?=
                $form->field($model, 'image', [
                    'template' => '{input}{hint}{error}'
                ])->widget(FileInput::class, [
                    'options' => ['accept' => 'image/*'],
                    'pluginOptions' => [
                        'previewTemplates' => [
                            'image' => "<img src='{data}' style='width:{width};'/>"
                        ],
                        'previewSettings' => [
                            'image' => [
                                'width' => '100%',
                                'height' => 'auto'
                            ]
                        ],
                        'showClose' => false,
                        'showRemove' => false,
                        'showUpload' => false,
                        'dropZoneEnabled' => false,
                        'defaultPreviewContent' => [Html::img($model->imageUrl, ['style' => 'width: 100%;'])],
                        'overwriteInitial' => true
                    ]
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-9">
        <?= Html::submitButton(Yii::t('blog', 'Save'), ['class' => 'btn btn-success btn-block']) ?>
    </div>
</div>
<?php ActiveForm::end() ?>
<?php

return [
	'modules' => [
		'blog' => [
			'controllerNamespace' => '\backend\modules\blog\controllers',
			'viewPath' => '@backend/modules/blog/views'
		],
	],
];

<?php

return [
	'modules' => [
		'content' => [
			'controllerNamespace' => '\backend\modules\content\controllers',
			'viewPath' => '@backend/modules/content/views'
		],
	],
];

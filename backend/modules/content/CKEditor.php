<?php

namespace backend\modules\content;

use yii\helpers\Url;

class CKEditor extends \dosamigos\ckeditor\CKEditor {

	/**
	 * @inheritdoc
	 */
	public function init () {
		$this->preset = 'custom';
		$this->clientOptions = [
			'language' => \Yii::$app->language,
			'extraPlugins' => 'autogrow',
			'filebrowserUploadUrl' => Url::toRoute(['/content/upload/image']),
			'autoGrow_onStartup' => true,
			'autoGrow_maxHeight' => 1000,
			'autoGrow_bottomSpace' => 50,
			'contentsCss' => [
				\Yii::getAlias('@web/frontend/static/css/bootstrap.css'),
				\Yii::getAlias('@web/frontend/static/css/main.css'),
			],
			'toolbarGroups' => [
				['name' => 'document', 'groups' => ['mode', 'document', 'doctools']],
				['name' => 'clipboard', 'groups' => ['clipboard', 'undo']],
				['name' => 'basicstyles', 'groups' => ['basicstyles', 'colors', 'cleanup']],
				['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align']],
				['name' => 'links', 'groups' => ['links']],
				['name' => 'insert'],
				['name' => 'tools'],
			],
			'removeButtons' => 'Subscript,Superscript,About,Styles,SpecialChar,Anchor',
		];
		parent::init();
	}
}
<?php

namespace backend\modules\content\controllers;

use common\modules\content\models\BlockModel;
use common\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * @package backend\modules\content\controllers
 */

class BlockController extends Controller {

	use AjaxValidationTrait;

    /** @inheritdoc */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['content-block-edit'],
                    ],
                ],
            ],
        ];
    }

    /** @inheritdoc */
    public function beforeAction($action) {
        if(parent::beforeAction($action)) {
            Yii::$app->view->title = Yii::t('content', 'Blocks');
            Yii::$app->view->params['breadcrumbs'][] = Yii::t('content', 'Content');
            Yii::$app->view->params['breadcrumbs'][] = ['label' => Yii::t('content', 'Blocks'), 'url' => ['index']];
            return true;
        }
        return false;
    }

	/**
	 * Lists all created items.
	 * @return string
	 */
	public function actionIndex() {
		$filterModel = Yii::createObject([
			'class' => BlockModel::class
		]);
		$filterModel->scenario = BlockModel::SCENARIO_SEARCH;
		return $this->render('index', [
			'filterModel' => $filterModel,
			'dataProvider' => $filterModel->search(Yii::$app->request->get()),
		]);
	}

    /** @inheritdoc */
    public function actionCreate() {
	    $model = Yii::createObject([
		    'class' => BlockModel::class,
	    ]);
	    $model->scenario = BlockModel::SCENARIO_CREATE;
	    $this->performAjaxValidation($model);
	    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		    return $this->redirect(['update', 'id' => $model->id]);
	    }
	    Yii::$app->view->title = Yii::t('content', 'Create block');
	    return $this->render('item', [
		    'model' => $model,
	    ]);
    }

    /** @inheritdoc */
    public function actionUpdate($id) {
	    $model = $this->getItem($id);
	    $model->scenario = BlockModel::SCENARIO_UPDATE;
	    $this->performAjaxValidation($model);
	    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		    return $this->refresh();
	    }
	    Yii::$app->view->title = Yii::t('content', 'Edit block {title}', ['title' => $model->title]);
	    return $this->render('item', [
		    'model' => $model,
	    ]);
    }

	/**
	 * Deletes item.
	 * @param  int $id Item id
	 * @return Response
	 * @throws \Throwable
	 */
	public function actionDelete($id) {
		$item = $this->getItem($id);
		$item->delete();
		return $this->redirect(['index']);
	}

    /**
     * Get block item
     * @param int $id
     * @return BlockModel
     * @throws \yii\web\NotFoundHttpException
     */
    protected function getItem(int $id) {
        $item = BlockModel::find()->andWhere(['id' => $id])->one();
        if ($item) {
            return $item;
        }
        throw new \yii\web\NotFoundHttpException(Yii::t('content', 'The requested block does not exist'));
    }

}
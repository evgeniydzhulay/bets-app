<?php

namespace backend\modules\content\controllers;

use common\modules\content\models\CategoryModel;
use common\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * @package backend\modules\content\controllers
 */

class CategoryController extends Controller {

	use AjaxValidationTrait;

    /** @inheritdoc */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['content-category-edit'],
                    ],
                ],
            ],
        ];
    }

    /** @inheritdoc */
    public function beforeAction($action) {
        if(parent::beforeAction($action)) {
            Yii::$app->view->title = Yii::t('content', 'Categories');
            Yii::$app->view->params['breadcrumbs'][] = Yii::t('content', 'Content');
            Yii::$app->view->params['breadcrumbs'][] = ['label' => Yii::t('content', 'Categories'), 'url' => ['index']];
            return true;
        }
        return false;
    }

	/**
	 * Lists all created items.
	 * @return string
	 */
	public function actionIndex() {
		$filterModel = Yii::createObject([
			'class' => CategoryModel::class
		]);
		$filterModel->scenario = CategoryModel::SCENARIO_SEARCH;
		return $this->render('index', [
			'filterModel' => $filterModel,
			'dataProvider' => $filterModel->search(Yii::$app->request->get()),
		]);
	}

    /** @inheritdoc */
    public function actionCreate() {
	    $model = Yii::createObject([
		    'class' => CategoryModel::class,
	    ]);
	    $model->scenario = CategoryModel::SCENARIO_CREATE;
	    $this->performAjaxValidation($model);
	    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		    return $this->redirect(['update', 'id' => $model->id]);
	    }
	    Yii::$app->view->title = Yii::t('content', 'Create category');
	    return $this->render('item', [
		    'model' => $model,
	    ]);
    }

    /** @inheritdoc */
    public function actionUpdate($id) {
	    $model = $this->getItem($id);
	    $model->scenario = CategoryModel::SCENARIO_UPDATE;
	    $this->performAjaxValidation($model);
	    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		    return $this->refresh();
	    }
	    Yii::$app->view->title = Yii::t('content', 'Edit category {title}', ['title' => $model->title]);
	    return $this->render('item', [
		    'model' => $model,
	    ]);
    }

	/**
	 * Deletes item.
	 * @param  int $id Item id
	 * @return Response
	 * @throws \Throwable
	 */
	public function actionDelete($id) {
		$item = $this->getItem($id);
		$item->delete();
		return $this->redirect(['index']);
	}

	/**
	 * @param int $id
	 * @return CategoryModel
	 */
    protected function getItem(int $id) {
	    $item = CategoryModel::find()->andWhere(['id' => $id])->one();
        if ($item) {
            return $item;
        }
        throw new \yii\web\NotFoundHttpException(Yii::t('content', 'The requested category does not exist'));
    }
    
    public function actionList($lang = ''){
        \Yii::$app->response->format = Response::FORMAT_JSON;
        return ['results' => CategoryModel::find()->andWhere(['lang_code' => $lang])->select([
        	'id' => 'key', 'text' => 'title'
        ])->asArray()->all()];
    }

}

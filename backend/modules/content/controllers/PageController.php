<?php

namespace backend\modules\content\controllers;

use common\modules\content\models\PageModel;
use common\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;

/**
 * @package backend\modules\content\controllers
 */

class PageController extends Controller {

	use AjaxValidationTrait;

    /** @inheritdoc */
    public function behaviors() {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['content-page-edit'],
                    ],
                ],
            ],
        ];
    }
    
    public function beforeAction($action) {
        if(parent::beforeAction($action)) {
            Yii::$app->view->title = Yii::t('content', 'Pages');
            Yii::$app->view->params['breadcrumbs'][] = Yii::t('content', 'Content');
            Yii::$app->view->params['breadcrumbs'][] = ['label' => Yii::t('content', 'Pages'), 'url' => ['index']];
            return true;
        }
        return false;
    }

	/**
	 * Lists all created items.
	 * @return string
	 */
	public function actionIndex() {
		$filterModel = Yii::createObject([
			'class' => PageModel::class
		]);
		$filterModel->scenario = PageModel::SCENARIO_SEARCH;
		return $this->render('index', [
			'filterModel' => $filterModel,
			'dataProvider' => $filterModel->search(Yii::$app->request->get()),
		]);
	}

    public function actionCreate() {
	    $model = Yii::createObject([
		    'class' => PageModel::class,
	    ]);
	    $model->scenario = PageModel::SCENARIO_CREATE;
	    $this->performAjaxValidation($model);
	    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		    return $this->redirect(['update', 'id' => $model->id]);
	    }
	    Yii::$app->view->title = Yii::t('content', 'Create page');
	    return $this->render('item', [
		    'model' => $model,
	    ]);
    }

    public function actionUpdate($id) {
	    $model = $this->getItem($id);
	    $model->scenario = PageModel::SCENARIO_UPDATE;
	    $this->performAjaxValidation($model);
	    if ($model->load(Yii::$app->request->post()) && $model->save()) {
		    return $this->refresh();
	    }
	    Yii::$app->view->title = Yii::t('content', 'Edit page {title}', ['title' => $model->title]);
	    return $this->render('item', [
		    'model' => $model,
	    ]);
    }

	/**
	 * Deletes item.
	 * @param  int $id Item id
	 * @return Response
	 * @throws \Throwable
	 */
	public function actionDelete($id) {
		$item = $this->getItem($id);
		$item->delete();
		return $this->redirect(['index']);
	}

    protected function getItem(int $id) {
	    $item = PageModel::find()->andWhere(['id' => $id])->one();
        if ($item) {
            return $item;
        }
        throw new \yii\web\NotFoundHttpException(Yii::t('content', 'The requested page does not exist'));
    }

}

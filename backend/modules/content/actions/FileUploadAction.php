<?php

namespace backend\modules\content\actions;

use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;

/**
 * @author Nghia Nguyen <yiidevelop@hotmail.com>
 * @since 2.0
 */
class FileUploadAction extends Action {

    public $storage;
    public $module;

    /**
     * @throws InvalidConfigException
     */
    public function init() {
        if (empty($this->module)) {
            $this->module = Yii::$app->controller->module;
        }
        if (empty($this->storage)) {
            throw new InvalidConfigException('Invalid config: storage not set');
        }
    }

    /**
     * @return array
     * @throws InvalidConfigException
     */
    function run() {
        if (isset($_FILES)) {
            $model = Yii::createObject([
                'class' => FileUploadModel::class
            ]);
            if ($model->upload()) {
                return $model->getResponse();
            } else {
                return ['error' => 'Unable to save file'];
            }
        }
    }

}

<?php

namespace backend\modules\content\actions;

use common\modules\Storage;
use creocoder\flysystem\Filesystem;
use Yii;
use yii\base\Model;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 *
 * @property string $fileName
 * @property \common\modules\Storage $module
 * @property \creocoder\flysystem\Filesystem $storage
 * @property array $response
 */
class FileUploadModel extends Model {

    public $uploadedFileName = 'upload';

    /**
     * @var UploadedFile
     */
    public $file;
    private $_fileName;

    public function getModule() : Storage {
        return Yii::$app->getModule('contentStorage');
    }

    public function getStorage() : Filesystem {
        return $this->getModule()->get('storage');
    }

    public function rules() {
        return [
            ['file', 'file', 'extensions' => NULL]
        ];
    }

    public function getFileName() {
        if (!$this->_fileName) {
            $fileName = substr(uniqid(md5(rand()), true), 0, 10);
            $fileName .= md5(Inflector::slug($this->file->baseName));
            $fileName .= '.' . $this->file->extension;
            $this->_fileName = "{$fileName[0]}/{$fileName[1]}/{$fileName}";
        }
        return $this->_fileName;
    }

    public function upload() {
        if ($this->validate()) {
            return $this->getStorage()->writeStream($this->getFileName(), fopen($this->file->tempName, 'r+'));
        }
        return false;
    }

    public function getResponse() {
        return [
            'fileName' => $this->getFileName(),
            'url' => $this->getModule()->getUrl($this->getFileName()),
        ];
    }

    public function beforeValidate() {
        if (parent::beforeValidate()) {
            if (!$this->file) {
                $this->file = UploadedFile::getInstanceByName($this->uploadedFileName);
            }
            return true;
        }
        return false;
    }

}

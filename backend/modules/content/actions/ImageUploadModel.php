<?php

namespace backend\modules\content\actions;

class ImageUploadModel extends FileUploadModel {

    public function rules() {
        return [
            ['file', 'file', 'extensions' => [
                'jpg', 'jpeg', 'png', 'gif'
            ]]
        ];
    }

}

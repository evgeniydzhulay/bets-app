<?php

use common\modules\content\models\PageModel;
use yii\helpers\Url;
use yii\helpers\Html;
use yii\web\JsExpression;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/**
 * @var $this  yii\web\View
 * @var $model  PageModel
 */

$this->params['breadcrumbs'][] = $this->title;
$form = ActiveForm::begin([
	'type' => ActiveForm::TYPE_HORIZONTAL,
	'id' => 'content-page',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'options' => [
		'class' => 'box box-primary',
	],
])
?>
<div class="box-body">
    <?= $form->field($model, 'key') ?>
    <?= $form->field($model, 'lang_code')->widget(Select2::class, [
        'theme' => 'default',
        'data' => \yii\helpers\ArrayHelper::getColumn(Yii::$app->params['languages'], 'label', true),
        'pluginEvents' => [
            "change" => "function() { $('#pagemodel-category_key').val('').trigger('change'); }",
        ],
        'options' => [
            'placeholder' => Yii::t('content', 'Select language'),
        ],
    ]);
    ?>
    <?= $form->field($model, 'title') ?>
    <?= $form->field($model, 'content')->widget(\backend\modules\content\CKEditor::class) ?>
    <?= $form->field($model, 'category_key')->widget(Select2::class, [
        'theme' => 'default',
        'pluginOptions' => [
            'ajax' => [
                'url' => Url::toRoute('/content/category/list'),
                'dataType' => 'json',
                'delay' => 50,
                'data' => new JsExpression('function(params) { return {lang: $(\'#pagemodel-lang_code\').val()}; }')
            ],
            'escapeMarkup' => new JsExpression('function (markup) { return markup; }'),
            'templateResult' => new JsExpression('function (procedure) { return procedure.text; }'),
            'templateSelection' => new JsExpression('function (procedure) { return procedure.text; }'),
        ],
        'options' => [
            'placeholder' => Yii::t('content', 'Select category'),
        ],
    ]);
    ?>
    <?= $form->field($model, 'meta_keywords')->textarea([
        'rows' => 5
    ]) ?>
    <?= $form->field($model, 'meta_description')->textarea([
        'rows' => 5
    ]) ?>
</div>
<div class="box-footer">
	<?= Html::submitButton(Yii::t('content', 'Save'), ['class' => 'btn btn-success btn-block']) ?>
</div>
<?php ActiveForm::end() ?>

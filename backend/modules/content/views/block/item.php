<?php

use common\modules\content\models\BlockModel;
use yii\helpers\Html;
use kartik\form\ActiveForm;
use kartik\select2\Select2;

/**
 * @var $this  yii\web\View
 * @var $model  BlockModel
 */

$this->params['breadcrumbs'][] = $this->title;
$form = ActiveForm::begin([
    'type' => ActiveForm::TYPE_HORIZONTAL,
	'id' => 'content-block',
    'enableAjaxValidation' => true,
    'enableClientValidation' => true,
    'options' => [
	    'class' => 'box box-primary',
    ]
])
?>
<div class="box-body">
	<?= $form->field($model, 'key') ?>
	<?= $form->field($model, 'lang_code')->widget(Select2::class, [
		'theme' => 'default',
		'data' => \yii\helpers\ArrayHelper::getColumn(Yii::$app->params['languages'], 'label', true),
		'options' => [
			'placeholder' => Yii::t('content', 'Select language'),
		],
	]);
	?>
	<?= $form->field($model, 'title') ?>
	<?= $form->field($model, 'content')->widget(\backend\modules\content\CKEditor::class) ?>
</div>
<div class="box-footer">
	<?= Html::submitButton(Yii::t('content', 'Save'), ['class' => 'btn btn-success btn-block']) ?>
</div>
<?php ActiveForm::end() ?>

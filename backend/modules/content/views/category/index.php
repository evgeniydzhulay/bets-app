<?php

use common\modules\content\models\CategoryModel;
use yii\helpers\ArrayHelper;
use yii\web\View;
use yii\helpers\Url;
use yii\helpers\Html;
use common\widgets\GridView;
use yii\data\ActiveDataProvider;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var CategoryModel $filterModel
 */
echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $filterModel,
	'id' => 'list-categories',
    'toolbar' => [
        ['content' =>
            Html::a('<i class="glyphicon glyphicon-plus"></i>', Url::toRoute(['create']), [
                'data-pjax' => 0,
                'class' => 'btn btn-default',
                'title' => \Yii::t('content', 'New category')]
            )
            . ' ' .
            Html::a('<i class="glyphicon glyphicon-repeat"></i>', Url::toRoute(['index']), [
                'data-pjax' => 0,
                'class' => 'btn btn-default',
                'title' => Yii::t('content', 'Reset filter')]
            )
        ],
    ],
    'columns' => [
        [
            'attribute' => 'lang_code',
            'filterType' => GridView::FILTER_SELECT2,
            'filterWidgetOptions' => [
                'theme' => 'default',
                'data' => ArrayHelper::getColumn(Yii::$app->params['languages'], 'label', true),
                'options' => ['placeholder' => Yii::t('content', 'Language')],
                'pluginOptions' => ['allowClear' => true],
            ],
	        'headerOptions' => ['width' => '10%'],
        ],
	    [
		    'attribute' => 'key',
		    'headerOptions' => ['width' => '40%'],
	    ],
	    [
		    'attribute' => 'title',
		    'headerOptions' => ['width' => '40%'],
	    ],
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{update} {delete}',
        ],
    ],
]);
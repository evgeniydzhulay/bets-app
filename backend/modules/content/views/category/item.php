<?php

use common\modules\content\models\CategoryModel;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;

/**
 * @var $this  yii\web\View
 * @var $model  CategoryModel
 */

$this->params['breadcrumbs'][] = $this->title;
$form = ActiveForm::begin([
	'type' => ActiveForm::TYPE_HORIZONTAL,
	'id' => 'content-category',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'options' => [
		'class' => 'box box-primary',
	],
])
?>
<div class="box-body">
	<?= $form->field($model, 'key') ?>
	<?= $form->field($model, 'lang_code')->widget(Select2::class, [
		'theme' => 'default',
		'data' => \yii\helpers\ArrayHelper::getColumn(Yii::$app->params['languages'], 'label', true),
		'options' => [
			'placeholder' => Yii::t('content', 'Select language'),
		],
	]);
	?>
	<?= $form->field($model, 'title') ?>
	<?= $form->field($model, 'description')->widget(\backend\modules\content\CKEditor::class) ?>
</div>
<div class="box-footer">
	<?= Html::submitButton(Yii::t('content', 'Save'), ['class' => 'btn btn-success btn-block']) ?>
</div>
<?php ActiveForm::end() ?>

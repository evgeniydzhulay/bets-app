<?php

use backend\assets\MainAsset;
use kartik\alert\AlertBlock;
use kartik\growl\Growl;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;

/* @var $this View */
/* @var $content string */
MainAsset::register($this);
$mainClass = str_replace('/', '-', Yii::$app->controller->route);
$bodyClass = isset($this->params['bodyClass']) ? ' ' . $this->params['bodyClass'] : '';
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= (!empty($this->title) ? Html::encode("{$this->title} | ") : '') . Yii::$app->name; ?></title>
        <?php $this->head() ?>
        <?= Html::csrfMetaTags() ?>
        <link rel="icon" href="/static/favicon/default.ico" type="image/x-icon">
    </head>
    <body class="layout-top-nav skin-black-light <?= $mainClass ?><?= $bodyClass ?>">
        <?php 
        $this->beginBody();
        echo AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true,
            'delay' => 500,
            'id' => 'alerts',
            'alertSettings' => [
                'success' => [
                    'type' => Growl::TYPE_SUCCESS,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
                'danger' => [
                    'type' => Growl::TYPE_DANGER,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
                'warning' => [
                    'type' => Growl::TYPE_WARNING,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
                'info' => [
                    'type' => Growl::TYPE_INFO,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
            ]
        ]);
        ?>
        <div class="wrapper">
            <header class="main-header">
                <nav class="navbar navbar-static-top">
                    <div class="container">
                        <div class="navbar-header">
                            <a href="<?= Url::toRoute(['/core/index']); ?>" class="navbar-brand"><b>BET</b> <span class="hidden-sm">company</span></a>
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#nav-main">
                                <i class="fa fa-bars"></i>
                            </button>
                        </div>
                        <div class="navbar-collapse pull-left collapse" id="nav-main" aria-expanded="false" style="height: 1px;">
	                        <?= \backend\widgets\MainMenu::widget(); ?>
                        </div>
                        <div class="navbar-custom-menu">
	                        <?= \backend\widgets\UserMenu::widget(); ?>
                            <?= \common\widgets\LanguageMenu::widget(); ?>
                        </div>
                    </div>
                </nav>
            </header>
            <div class="content-wrapper">
                <div class="container">
                    <section class="content-header">
                        <h1>
					        <?= $this->title ?? '' ?>
                        </h1>
				        <?=
				        \yii\widgets\Breadcrumbs::widget([
					        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
				        ])
				        ?>
	                    <?= isset($this->blocks['gameBreadcrumbs']) ? $this->blocks['gameBreadcrumbs'] : '' ?>
                    </section>
                    <section class="content">
				        <?= $content ?>
                    </section>
                </div>
            </div>
            <footer class="main-footer">
                <?= Yii::$app->name; ?> &copy; <?= date('Y'); ?>
            </footer>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

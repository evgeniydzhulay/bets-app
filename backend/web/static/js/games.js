/* Bets */
console.log('::bets::');

var select_sport = $("select#betssearch-sport_id");
var select_tournament = $("select#betssearch-tournament_id");
var select_game = $("select#betssearch-game_id");

function requestTournamentsList(params){
	return {title: params.term, sport: select_sport.val()}
}

function requestGamesList(params){
	return {title: params.term, sport: select_sport.val(), tournament: select_tournament.val()}
}

function sportClearLists(){
	select_tournament.val(false);
	select_tournament.html("");
	select_game.val(false);
	select_game.html("");
}

function tournamentClearLists(){
	select_game.val(false);
	select_game.html("");
}

$(document).on('pjax:send', function(){
	$('#pre-loader').show()
})
$(document).on('pjax:complete', function(){
	$('#pre-loader').hide()
});

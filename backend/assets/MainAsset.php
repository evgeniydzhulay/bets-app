<?php

namespace backend\assets;

use dmstr\web\AdminLteAsset;
use kartik\icons\FlagIconAsset;
use yii\bootstrap\BootstrapAsset;
use yii\jui\JuiAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class MainAsset extends AssetBundle {

    /**
     * {@inheritdoc}
     */
    public $sourcePath = null;

    /**
     * {@inheritdoc}
     */
    public $basePath = '@webroot';

    /**
     * {@inheritdoc}
     */
    public $baseUrl = '@web';

    /**
     * {@inheritdoc}
     */
    public $css = [
        'static/css/adminlte.min.css',
        'static/css/main.min.css',
    ];

    /**
     * {@inheritdoc}
     */
    public $js = [
        'static/js/main.min.js'
    ];

    /**
     * {@inheritdoc}
     */
    public $depends = [
        BootstrapAsset::class,
        AdminLteAsset::class,
        FlagIconAsset::class,
	    JuiAsset::class,
	    JqueryAsset::class,
    ];

}
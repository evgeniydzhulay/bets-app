<?php

namespace backend\assets;

use common\assets\SocketIoAsset;
use common\assets\VueAsset;
use yii\web\AssetBundle;
use yii\web\JqueryAsset;

class SupportAsset  extends AssetBundle {

	/** {@inheritdoc} */
	public $sourcePath = null;

	/** {@inheritdoc} */
	public $basePath = '@webroot/static';

	/** {@inheritdoc} */
	public $baseUrl = '@web/static';

	/** {@inheritdoc} */
	public $css = [
		'css/support.css'
	];

	/** {@inheritdoc} */
	public $js = [
		'js/vue.js',
		'js/support.js',
	];

	/** {@inheritdoc} */
	public $depends = [
		JqueryAsset::class,
		SocketIoAsset::class,
	];
}
module.exports = {
    server: {
        port: 8082
    },
    redis: {
        isCluster: true,
        instances: [
            {
                port: 6379,
                host: "127.0.0.1"
            }
        ]
    },
    website: {
        host: 'localhost',
        api: 'http://api.localhost'
    },
};
'use strict';

process.env.NODE_CONFIG_DIR = __dirname + "/config/";

/**
 * @typedef {Object} GameUpdate
 * @property {integer} e Event
 * @property {integer} m Market
 * @property {integer} o Outcome
 * @property {number} r Rate
 */

const config = require('config');
const request = require('request-promise-native');
const pmx = require('pmx');
const Probe = pmx.probe();
const connectedCount = Probe.counter({
    name: 'Active connections'
});
process.send = process.send || function () {};

const IOredis = require('ioredis');
const redisConfig = config.get('redis');
let outcomesChannel = null;
let gamesChannel = null;
if(!redisConfig.isCluster) {
    outcomesChannel = new IOredis(redisConfig.instances[0]);
    gamesChannel = new IOredis(redisConfig.instances[0]);
} else {
    outcomesChannel = new IOredis.Cluster(redisConfig.instances);
    gamesChannel = new IOredis.Cluster(redisConfig.instances);
}

outcomesChannel.subscribe('orate');
outcomesChannel.subscribe('oban');
outcomesChannel.subscribe('ostatus');
outcomesChannel.subscribe('oenabled');
outcomesChannel.subscribe('ohidden');
outcomesChannel.subscribe('ofinished');

gamesChannel.subscribe('gresult');
gamesChannel.subscribe('gstats');
gamesChannel.subscribe('gtimer');

const games = {};
function loadCurrentGames() {
    request({uri: `${config.website.api}/games/events/index`, json: true}).then((data) => {
        Object.keys(data).forEach(function(key) {
            if(games.hasOwnProperty(key)) {
                return;
            }
            games[key] = data[key];
        });
    }).catch((err) => {
        console.error({message: err.message, name: err.name})
    });
}
outcomesChannel.on('error', () => {
    console.error('redis connection failed');
});
outcomesChannel.on('message', (channel, params) => {
    const data = JSON.parse(params);
    if(channel === 'orate') {
        io.in('game' + data.g).emit('or', data);
    } else  if(channel === 'ostatus') {
        io.in('game' + data.g).emit('os', data);
    } else if(channel === 'oenabled') {
        io.in('game' + data.g).emit('oise', data);
    } else if(channel === 'ohidden') {
        io.in('game' + data.g).emit('oish', data);
    } else if(channel === 'ofinished') {
        io.in('game' + data.g).emit('oisf', data);
    } else if(channel === 'oban') {
        io.in('game' + data.g).emit('oisb', data);
    }
});

gamesChannel.on('error', () => {
    console.error('redis connection failed');
});
const timers = {};
gamesChannel.on('message', (channel, params) => {
    const data = JSON.parse(params);
    if(channel === 'gresult') {
        io.in('game' + data.g).emit('gres', data);
    } else if(channel === 'gtimer') {
        timers[data.g] = data;
        io.in('game' + data.g).emit('gtimer', data);
    } else if(channel === 'gstats') {
        io.in('game' + data.g).emit('gstats', data);
    }
});

const io = require('socket.io').listen(config.get('server.port'));

io.on('connection', (socket) => {
    connectedCount.inc(1);
    socket.on('disconnect', () => {
        connectedCount.dec(1);
    });
    socket.on('sub', (data) => {
        if(!socket.rooms.hasOwnProperty('game' + data.g)) {
            socket.join('game' + data.g);
        }
        if(timers.hasOwnProperty(data.g)) {
            socket.emit('gtimer', timers[data.g]);
        }
    });
    socket.on('unsub', (data) => {
        socket.leave('game' + data.g);
    });
    socket.on('reload', (data) => {
        if(!socket.rooms.hasOwnProperty('game' + data.g)) {
            return;
        }
        (games[data.g] || []).forEach((item) => {
            socket.emit('or', {g: data.g, o: item.o, r: item.r});
        });
    });
});
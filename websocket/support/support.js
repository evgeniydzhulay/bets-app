'use strict';

process.env.NODE_CONFIG_DIR = __dirname + "/config/";

const TYPE_OPERATOR = 1;
const TYPE_USER = 2;

const config = require('config');
const pmx = require('pmx');
const Probe = pmx.probe();
const {redisPublisher, redisSubscriber} = require('./classes/redis');
const io = require('socket.io').listen(config.get('server.port'));
const metricPending = Probe.counter({
    name: 'Pending requests'
});
const requests = require('./classes/_request');
const user = require('./classes/_user');
process.send = process.send || function () {};
redisSubscriber.on('message', (channel, params) => {
    if(channel === 'support') {
        const data = JSON.parse(params);
        if(data.type === 'new-message') {
            io.in('request' + data.p.request_id).emit('message', data.p);
        } else if(data.type === 'request') {
            io.in('operators').emit('request', data.params);
        } else if(data.type === 'request-processing') {
            io.in('operators').emit('request-processing', data.params);
        } else if(data.type === 'request-closed') {
            io.in('operators').emit('request-closed', data.params);
        }
    }
});
io.on('connection', (socket) => {
    socket.on('disconnect', () => {
        console.log('disconnect');
    });
    console.log('connection');
    const userkey = socket.handshake.query.key || false;
    user.loadIdentity(userkey).then((result) => {
        const userData = JSON.parse(result);
        if(!userData) {
            throw new Error('no user data');
        }
        const usertype = userData.type;
        const request = socket.handshake.query.request || false;
        if(!request && usertype === TYPE_USER) {
            throw new Error('no request id');
        }
        socket.user = userData;
        if(usertype === TYPE_USER) {
            socket.join('request' + request);
        } else if(usertype === TYPE_OPERATOR) {
            socket.join('operators');
            requests.loadByOperator(userData.id).then(({data: list = [], success = false} = {}) => {
                if(!success) {
                    throw new Error('Invalid response');
                }
                list.forEach((item) => {
                    socket.join('request' + item.id);
                    socket.emit('request', item);
                });
            }).catch((err) => {
                console.error(err);
            });
        }
    }).catch((err) => {
        console.error(err);
        socket.disconnect();
    });
    socket.on('message', (message, callback) => {
        if(!callback || typeof callback !== "function") {
            callback = () => {};
        }
        requests.sendMessage(socket, message.request, message.message, message.files, message.key).then((result) => {
            redisPublisher.publish("support", JSON.stringify({
                type: 'new-message',
                p: result
            }));
            callback(true);
        }).catch(() => {
            callback(false);
        });
    });
});
requests.loadActive(metricPending);


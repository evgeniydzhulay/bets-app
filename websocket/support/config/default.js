module.exports = {
    server: {
        port: 8083
    },
    redis: {
        isCluster: true,
        instances: [
            {
                port: 6379,
                host: "127.0.0.1"
            }
        ]
    },
    website: {
        api: 'http://api.localhost'
    },
};
'use strict';

const query = require('./_query');

module.exports = {
    loadActive: function(counter) {
        query.send({
            uri: 'support/requests/new'
        }).then((response) => {
            counter.inc(response.countPending);
        }).catch((err) => {
            console.error('Failed loading active requests.', err);
        });
    },
    loadByOperator: function(operator) {
        return query.send({
            uri: 'support/requests/operator',
            qs: {
                id: operator
            }
        });
    },
    sendMessage(socket, request, text, files, key) {
        return query.send({
            method: 'POST',
            uri: 'support/message/send',
            qs: {
                uid: socket.user.id,
                request: request,
                key: key,
            },
            form: {
                message: text,
                files: files,
                email: socket.user.email,
                phone: socket.user.phone,
                name: socket.user.name,
            }
        });
    },
};
'use strict';

const request = require('request-promise-native');
const config = require('config');
const host = config.get('website.api');

module.exports = {
    send: function(params) {
        params.uri = `${host}/${params.uri}`;
        params.json = params.json || true;
        const query = request(params).then((response) => {
            if (!response || !response.success) {
                throw new Error(`Incorrect result`);
            }
            return response;
        });
        query.catch((err) => {
            console.error('api request with options ', params, ' has error ', err.error);
        });
        return query;
    }
};
const config = require('config');
const IOredis = require('ioredis');
const redisConfig = config.get('redis');
let redisCache = null;
let redisSubscriber = null;
let redisPublisher = null;
if(!redisConfig.isCluster) {
    redisCache = new IOredis(redisConfig.instances[0]);
    redisSubscriber = new IOredis(redisConfig.instances[0]);
    redisPublisher = new IOredis(redisConfig.instances[0]);
} else {
    redisCache = new IOredis.Cluster(redisConfig.instances);
    redisSubscriber = new IOredis.Cluster(redisConfig.instances);
    redisPublisher = new IOredis.Cluster(redisConfig.instances);
}
redisSubscriber.subscribe('support');
redisCache.on('error', (err) => {
    console.error(err);
    console.error('redis connection failed');
});
redisSubscriber.on('error', (err) => {
    console.error(err);
    console.error('redis connection failed');
});
redisPublisher.on('error', (err) => {
    console.error(err);
    console.error('redis connection failed');
});

module.exports = {
    redisCache,
    redisSubscriber,
    redisPublisher,
};
'use strict';

const {redisCache} = require('./redis');
const {promisify} = require('util');
const redisGet = promisify(redisCache.get).bind(redisCache);

module.exports = {
    loadIdentity: async function(key) {
        if(!key) {
            throw new Error('no identity key');
        }
        return await redisGet(key);
    }
};
const baseConfig = require('./webpack.prod.js');
const path = require('path');
const VueLoaderPlugin = require('vue-loader/lib/plugin');
const CssExtractPlugin = require('mini-css-extract-plugin');

module.exports = Object.assign({}, baseConfig, {
    mode: 'development',
    name: 'frontend-dev',
    devtool: false,
    output: {
        pathinfo: false,
        path: path.resolve(__dirname, 'web/static'),
        filename: 'js/[name].js',
        chunkFilename: 'js/[name].js',
    },
    plugins: [
        new VueLoaderPlugin(),
        new CssExtractPlugin({
            filename: 'css/[name].css',
            chunkFilename: 'css/[id].css'
        }),
    ],
});
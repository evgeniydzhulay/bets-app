<?php

namespace partners\widgets;

use Yii;
use yii\bootstrap\Nav;
use yii\web\User;

/**
 * @property mixed $user
 */
class UserMenu extends Nav {

    public $activateItems = false;
    public $activateParents = false;

    public $options = [
        'id' => 'nav-user-website',
        'class' => 'navbar-nav'
    ];
    
    /**
     * Render widget
     * @return string
     */
    public function run() {
	    $this->items = [
		    [
			    'label' => Yii::t('support', 'Ask question'),
			    'url' => ['/support/request/create']
		    ],
		    [
			    'label' => Yii::t('partners', 'Aff ID: {id}', [
			    	'id' => Yii::$app->user->id,
			    ]),
			    'visible' => !$this->getUser()->isGuest,
			    'items' => [
				    [
					    'label' => Yii::t('user', 'Profile'),
					    'url' => ['/user/auth/account'],
				    ],
				    [
					    'label' => Yii::t('user', 'Password'),
					    'url' => ['/user/auth/password'],
				    ],
				    [
					    'label' => Yii::t('user', 'Logout'),
					    'url' => ['/user/auth/logout'],
				    ],
			    ]
		    ],
		    [
			    'label' => Yii::t('user', 'Registration'),
			    'visible' => $this->getUser()->isGuest,
			    'url' => ['/user/auth/register']
		    ],
	        [
			    'label' => Yii::t('user', 'Log In'),
		        'visible' => $this->getUser()->isGuest,
			    'url' => ['/user/auth/login']
		    ],
	    ];
        return parent::run();
    }

    protected function getUser() : User {
        return Yii::$app->get('user');
    }

}

<?php

namespace partners\widgets;

use Yii;
use yii\bootstrap\Nav;

/**
 * @property array $menuList
 */
class MainMenu extends \dmstr\widgets\Menu {

	public static $iconClassPrefix = '';

    public $activateParents = true;
    public $options = [
        'id' => 'nav-main-website',
	    'class' => 'sidebar-menu tree',
	    'data' => [
		    'widget' => 'tree'
	    ]
    ];

    /**
     * Render widget
     * @return string
     */
    public function run() {
        $this->items = $this->getMenuList();
        return parent::run();
    }
    
    /**
     * Build menu
     *
     * Passes through modules and builds menu
     *
     * @return array Menu items
     */
    protected function getMenuList() {
        return [
        	[
        		'label' => Yii::t('partners', 'Main page'),
		        'options' => [
		        	'class' => 'header'
		        ],
	        ],
            [
		        'label' => Yii::t('partners', 'Main page'),
		        'url' => ['/report/index'],
		        'icon' => 'fas fa-fw fa-home',
	        ],
	        [
		        'label' => Yii::t('partners', 'Add site'),
		        'url' => ['/sites/index'],
		        'icon' => 'fas fa-fw fa-link',
	        ],
	        [
		        'label' => Yii::t('partners', 'Payments history'),
		        'url' => ['/payments/index'],
		        'icon' => 'fas fa-fw fa-dollar-sign',
	        ],
	        [
		        'label' => Yii::t('user', 'Account'),
		        'url' => ['/user/auth/account'],
		        'icon' => 'far fa-fw fa-user',
	        ],
	        [
		        'label' => Yii::t('support', 'Contacts'),
		        'url' => ['/core/contacts'],
		        'icon' => 'far fa-fw fa-envelope',
	        ],
	        [
		        'label' => Yii::t('partners', 'Marketing tools'),
		        'options' => [
			        'class' => 'header'
		        ],
	        ],
	        [
		        'label' => Yii::t('partners', 'Affiliate links'),
		        'url' => ['/links/index'],
		        'icon' => 'fas fa-fw fa-external-link-alt',
	        ],
	        [
		        'label' => Yii::t('partners', 'Promo codes'),
		        'url' => ['/promo/codes'],
		        'icon' => 'fas fa-fw fa-code',
	        ],
	        [
		        'label' => Yii::t('partners', 'Promo materials'),
		        'url' => ['/promo/materials'],
		        'icon' => 'fas fa-fw fa-bullhorn',
	        ],
	        [
		        'label' => Yii::t('partners', 'Reports'),
		        'options' => [
			        'class' => 'header'
		        ],
	        ],
	        [
		        'label' => Yii::t('partners', 'Summary'),
		        'url' => ['/report/summary'],
		        'icon' => 'far fa-fw fa-chart-bar',
	        ],
	        [
		        'label' => Yii::t('partners', 'Full report'),
		        'url' => ['/report/full'],
		        'icon' => 'fas fa-fw fa-chart-area',
	        ],
	        [
		        'label' => Yii::t('partners', 'Promo materials'),
		        'url' => ['/report/promo'],
		        'icon' => 'fas fa-fw fa-bullhorn',
	        ],
	        [
		        'label' => Yii::t('partners', 'Players'),
		        'url' => ['/report/players'],
		        'icon' => 'fas fa-fw fa-child',
	        ],
	        [
		        'label' => Yii::t('partners', 'Sub-affiliates'),
		        'url' => ['/report/affiliates'],
		        'icon' => 'fas fa-fw fa-users',
	        ],
        ];
    }

}

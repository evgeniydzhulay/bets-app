<?php

use partners\assets\MainAsset;
use yii\helpers\Html;
use yii\web\View;
use yii\helpers\Url;

/* @var $this View */
/* @var $content string */

MainAsset::register($this);
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
	<meta charset="<?= Yii::$app->charset ?>"/>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title><?= (!empty($this->title) ? Html::encode("{$this->title} | ") : '') . Yii::$app->name; ?></title>
	<?php $this->head() ?>
	<?= Html::csrfMetaTags() ?>
	<link rel="icon" href="/static/favicon/favicon.ico" type="image/x-icon">
</head>
<body class="skin-black layout-top-nav">
<?php
$this->beginBody();
?>
<div class="wrapper">
	<header class="main-header guest_header">
		<a href="<?= \yii\helpers\Url::to(['/']) ?>" class="logo logo_main">
			<!-- <span class="logo-mini"><b>BET</b></span>
			<span class="logo-lg"><b>BET</b> company</span> -->
        </a>
        <button class="open_menu hide"></button>
		<nav class="navbar navbar-static-top" role="navigation">
            <button class="close_menu hide"></button>
			<div class="navbar-custom-menu guest_menu">
                <ul class="guest_header_links">
                    <li><a href="<?= Url::toRoute(['/core/rules']); ?>">Правила</a></li>
                    <li><a href="<?= Url::toRoute(['/core/faq']); ?>">FAQ</a></li>
                </ul>
				<?= \partners\widgets\UserMenu::widget(); ?>
				<?= \common\widgets\LanguageMenu::widget(); ?>
			</div>
		</nav>
	</header>
	<div class="content-wrapper">
		<div class="container">
            <?= $content ?>
		</div>
	</div>
    <footer class="main-footer">
        <div class="footer-navigation">
            <div class="footer-cookies">
                <div class="footer-cookies-icon"></div>
                <div class="footer-cookies-info">
                    <span class="footer-cookies-info-text"><?= Yii::t('user', 'Luxe.bet uses cookies to enhance your website experience.'); ?></span>
                    <br>
                    <span class="footer-cookies-info-text"><?= Yii::t('user', 'By staying on the website, you agree to the use of these cookies.'); ?></span>
                    <div class="footer-cookies-info-link"><?= Yii::t('user', 'Find out more'); ?>&nbsp;<i class="fas fa-long-arrow-alt-right"></i></div>
                </div>
            </div>
            <div class="footer-navigation-links">
                <a href="<?= Url::toRoute(['/support/request/create']); ?>" class="support">
                    <div class="picture"></div>
                </a>
                <a href="<?= Url::toRoute(['/core/rules']); ?>"><?= Yii::t('user', 'Terms and conditions'); ?></a>
            </div>
            <div class="footer-social">
                <div class="footer-social-text">
                    <span><?= Yii::t('user', 'you’ll like us:'); ?></span>
                </div>
                <div class="footer-social-icons">
                    <div class="ui-social"><i class="fab fa-vk"></i></div>
                    <div class="ui-social"><i class="fab fa-facebook-f"></i></div>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
			<?= Yii::t('app', 'Copyright &copy; {year} "{name}". All rights reserved and protected by law.', [
				'year' =>  date('Y'),
				'name' => Yii::$app->name
			]); ?>
        </div>
    </footer>
</div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

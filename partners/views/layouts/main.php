<?php

use yii\helpers\Url;
use kartik\alert\AlertBlock;
use kartik\growl\Growl;
use partners\assets\MainAsset;
use yii\helpers\Html;
use yii\web\View;

/* @var $this View */
/* @var $content string */

MainAsset::register($this);
$mainClass = str_replace('/', '-', Yii::$app->controller->route);
$bodyClass = isset($this->params['bodyClass']) ? ' ' . $this->params['bodyClass'] : '';
$this->beginPage();
?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>"/>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?= (!empty($this->title) ? Html::encode("{$this->title} | ") : '') . Yii::$app->name; ?></title>
        <?php $this->head() ?>
        <?= Html::csrfMetaTags() ?>
        <link rel="icon" href="/static/favicon/favicon.ico" type="image/x-icon">
    </head>
    <body class="skin-black sidebar-mini <?= $mainClass ?><?= $bodyClass ?>">
        <?php 
        $this->beginBody();
        echo AlertBlock::widget([
            'type' => AlertBlock::TYPE_GROWL,
            'useSessionFlash' => true,
            'delay' => 500,
            'id' => 'alerts',
            'alertSettings' => [
                'success' => [
                    'type' => Growl::TYPE_SUCCESS,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
                'danger' => [
                    'type' => Growl::TYPE_DANGER,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
                'warning' => [
                    'type' => Growl::TYPE_WARNING,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
                'info' => [
                    'type' => Growl::TYPE_INFO,
                    'pluginOptions' => [
                        'placement' => [
                            'from' => 'top',
                            'align' => 'center',
                        ]
                    ]
                ],
            ]
        ]);
        ?>
        <div class="wrapper">
            <header class="main-header">
                <a href="<?= \yii\helpers\Url::to(['/']) ?>" class="logo"></a>
                <nav class="navbar navbar-static-top" role="navigation">
                    <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
                        <i class="fas fa-bars"></i>
                        <span class="sr-only">Toggle navigation</span>
                    </a>
                    <?php if(!empty($this->title)) { ?>
                        <ul class="nav navbar-nav hidden-sm hidden-xs">
                            <li><a><?= $this->title ?></a></li>
                        </ul>
                    <?php } ?>
                    <div class="navbar-custom-menu">
		                <?= \partners\widgets\UserMenu::widget(); ?>
		                <?= \common\widgets\LanguageMenu::widget(); ?>
                    </div>
                </nav>
            </header>
            <aside class="main-sidebar">
                <section class="sidebar" style="height: auto;">
	                <?= \partners\widgets\MainMenu::widget(); ?>
                </section>
            </aside>
            <div class="content-wrapper">
                <section class="content">
                    <?= $content ?>
                </section>
            </div>
            <footer class="main-footer">
                <div class="footer-navigation">
                    <div class="footer-cookies">
                        <div class="footer-cookies-icon"></div>
                        <div class="footer-cookies-info">
                            <span class="footer-cookies-info-text"><?= Yii::t('user', 'Luxe.bet uses cookies to enhance your website experience.'); ?></span>
                            <br>
                            <span class="footer-cookies-info-text"><?= Yii::t('user', 'By staying on the website, you agree to the use of these cookies.'); ?></span>
                            <div class="footer-cookies-info-link"><?= Yii::t('user', 'Find out more'); ?>&nbsp;<i class="fas fa-long-arrow-alt-right"></i></div>
                        </div>
                    </div>
                    <div class="footer-navigation-links">
                        <a href="<?= Url::toRoute(['/support/request/create']); ?>" class="support">
                            <div class="picture"></div>
                        </a>
                        <a href="<?= Url::toRoute(['/content/view/tos']); ?>"><?= Yii::t('user', 'Terms and conditions'); ?></a>
                    </div>
                    <div class="footer-social">
                        <div class="footer-social-text">
                            <span><?= Yii::t('user', 'you’ll like us:'); ?></span>
                        </div>
                        <div class="footer-social-icons">
                            <div class="ui-social"><i class="fab fa-vk"></i></div>
                            <div class="ui-social"><i class="fab fa-facebook-f"></i></div>
                        </div>
                    </div>
                </div>
	            <div class="footer-copyright">
		            <?= Yii::t('app', 'Copyright &copy; {year} "{name}". All rights reserved and protected by law.', [
			            'year' =>  date('Y'),
			            'name' => Yii::$app->name
		            ]); ?>
                </div>
            </footer>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>

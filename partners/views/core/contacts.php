
<?php
$contacts = Yii::$app->params['contacts'];
?>
<div class="support-contacts">
    <h3 class="support-contacts-header"><?= Yii::t('support', 'Contact info'); ?></h3>
    <h4 class="support-contacts-invite">
	    <?= Yii::t('support', 'With any questions please feel free to reach us:'); ?>
    </h4>
    <div class="support-contacts-list">
	    <?php if(!empty($contacts['email'])) { ?>
            <div class="contacts-contact">
                <i class="fas fa-fw fa-envelope"></i>
                <span class="contact-label">Email:</span>
                <span class="contact-address"><?= $contacts['email']; ?></span>
            </div>
	    <?php } ?>

	    <?php if(!empty($contacts['telegram'])) { ?>
            <div class="contacts-contact">
                <i class="fab fa-fw fa-telegram-plane"></i>
                <span class="contact-label">Telegram:</span>
                <span class="contact-address"><?= $contacts['telegram']; ?></span>
            </div>
	    <?php } ?>

	    <?php if(!empty($contacts['skype'])) { ?>
            <div class="contacts-contact">
                <i class="fab fa-fw fa-skype"></i>
                <span class="contact-label">Skype:</span>
                <span class="contact-address"><?= $contacts['skype']; ?></span>
            </div>
	    <?php } ?>
    </div>
</div>
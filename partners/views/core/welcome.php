<div class="row">
    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
        <h1 class="title1">Партнерская программа компании Luxe.bet</h1>
        <div class="guest_fearures">
            <div class="guest_fearures_item">
                <div class="fearures_item_img">
                    <img src="/static/img/icons/icon1.png" alt="">
                </div>
                <p>Креативный маркетинг</p>
            </div>
            <div class="guest_fearures_item">
                <div class="fearures_item_img">
                    <img src="/static/img/icons/icon2.png" alt="">
                </div>
                <p>Поддержка пользователей и партнеров 24\7</p>
            </div>
            <div class="guest_fearures_item">
                <div class="fearures_item_img">
                    <img src="/static/img/icons/icon3.png" alt="">
                </div>
                <p>Быстрые и стабильные выплаты</p>
            </div>
            <div class="guest_fearures_item">
                <div class="fearures_item_img">
                    <img src="/static/img/icons/icon4.png" alt="">
                </div>
                <p>Стоп шейв</p>
            </div>
            <div class="guest_fearures_item">
                <div class="fearures_item_img">
                    <img src="/static/img/icons/icon5.png" alt="">
                </div>
                <p>Реальная Rev. Share до 40%</p>
            </div>
            <div class="guest_fearures_item">
                <div class="fearures_item_img">
                    <img src="/static/img/icons/icon6.png" alt="">
                </div>
                <p>Новейшие продукты на букмекерском рынке</p>
            </div>
            <div class="guest_fearures_item">
                <div class="fearures_item_img">
                    <img src="/static/img/icons/icon7.png" alt="">
                </div>
                <p>Действительно справедливый букмекер </p>
            </div>
            <div class="guest_fearures_item">
                <div class="fearures_item_img">
                    <img src="/static/img/icons/icon8.png" alt="">
                </div>
                <p>Бонусы и акции для игроков и партнеров</p>
            </div>
            <div class="guest_fearures_item">
                <div class="fearures_item_img">
                    <img src="/static/img/icons/icon9.png" alt="">
                </div>
                <p>Уникальный дизайн для играющих на Киберспорте</p>
            </div>
        </div>
        <div class="guest_reg_wrap">
            <a class="btn btn-red guest_reg" href="<?= \yii\helpers\Url::toRoute(['user/auth/register']); ?>">Регистрация</a>
        </div>
    </div>
</div>
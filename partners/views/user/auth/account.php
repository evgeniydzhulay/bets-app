<?php

use common\modules\user\forms\PasswordForm;
use common\modules\user\forms\SettingsForm;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use kartik\select2\Select2;

/**
 * @var $this  yii\web\View
 * @var $form  yii\widgets\ActiveForm
 * @var $modelAccount SettingsForm
 * @var $modelPassword PasswordForm
 * @var $twofaenabled bool
 */

$this->title = Yii::t('user', 'Account settings');
$this->params['breadcrumbs'][] = $this->title;
$this->beginContent('@common/modules/user/views/shared/profile.php');
?>
<div class="col-md-6">
    <?php
    $form = ActiveForm::begin([
        'id' => 'account-form',
        'type' => ActiveForm::TYPE_VERTICAL,
        'options' => [
            'class' => 'box box-default',
        ],
        'enableAjaxValidation' => true,
        'enableClientValidation' => false,
    ]);
    ?>
    <div class="box-header">
        <h3 class="box-title"><?= Yii::t('user', 'Contact info'); ?></h3>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-6"><?= $form->field($modelAccount, 'name') ?></div>
            <div class="col-md-6"><?= $form->field($modelAccount, 'surname') ?></div>
        </div>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-6"><?= $form->field($modelAccount, 'phone') ?></div>
            <div class="col-md-6"><?= $form->field($modelAccount, 'country')->widget(Select2::class, [
                'theme' => Select2::THEME_DEFAULT,
                'data' => $modelAccount->getCountriesList(),
            ]) ?></div>
        </div>
    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-red']) ?>
    </div>

	<?php
	ActiveForm::end();
	?>
    <div class="box box-default profile-twofactor">
        <div class="box-header">
            <h3 class="box-title"><?= Yii::t('user', 'Manage twofactor authentication'); ?></h3>
        </div>
        <div class="box-body">
			<?= Yii::t('user', 'Twofactor authentication is enabled: {enabled}', [
				'enabled' => !!Yii::$app->user->identity->auth_tf_enabled ? Yii::t('yii', 'Yes') : Yii::t('yii', 'No')
			]); ?>
            <?= Html::a(!!Yii::$app->user->identity->auth_tf_enabled ? Yii::t('user', 'Disable') : Yii::t('user', 'Enable'), ['two-factor/index'], [
				'class' => 'btn btn-red pull-right',
				'data' => [
					'toggle' => 'modal',
					'target' => '#modal2fa'
				]
			]); ?>
        </div>
    </div>
</div>
<div class="col-md-6">
	<?php
	$form = ActiveForm::begin([
		'id' => 'password-form',
		'type' => ActiveForm::TYPE_VERTICAL,
		'options' => [
			'class' => 'box box-default',
		],
		'enableAjaxValidation' => true,
		'enableClientValidation' => false,
	]);
	?>
    <div class="box-header">
        <h3 class="box-title"><?= Yii::t('user', 'Change password') ?></h3>
    </div>
    <div class="box-body">
		<?= $form->field($modelPassword, 'current_password')->passwordInput() ?>
    </div>
    <div class="box-body">
        <div class="row">
            <div class="col-md-6"><?= $form->field($modelPassword, 'new_password')->passwordInput() ?></div>
            <div class="col-md-6"><?= $form->field($modelPassword, 'repeat_password')->passwordInput() ?></div>
        </div>
    </div>
    <div class="box-footer text-right">
		<?= Html::submitButton(Yii::t('user', 'Save'), ['class' => 'btn btn-red']) ?>
    </div>    
	<?php ActiveForm::end(); ?>
</div>
<div id="modal2fa" class="fade modal" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-md">
        <div class="modal-content modal2fa-form__container">
        </div>
    </div>
</div>
<?php
$this->endContent();
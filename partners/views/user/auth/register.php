<?php

use yii\helpers\Html;
use kartik\form\ActiveForm;

/**
 * @var yii\web\View $this
 * @var array $categories
 * @var array $languages
 * @var array $countries
 */
$this->title = Yii::t('user', 'Sign up');

$form = ActiveForm::begin([
    'id' => 'registration-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'type' => ActiveForm::TYPE_VERTICAL,
    'options' => [
        'class' => 'box box-default registration-form',
    ],
]);
?>
<div class="box-header">
    <div class="col-md-6"><?= $form->field($model, 'email') ?></div>
    <div class="col-md-6"><?= $form->field($model, 'password')->passwordInput() ?></div>
</div>
<div class="box-footer">
    <div class="col-md-6"><?= $form->field($model, 'name') ?></div>
    <div class="col-md-6"><?= $form->field($model, 'surname') ?></div>
    <div class="col-md-6"><?= $form->field($model, 'country')->dropDownList($countries) ?></div>
    <div class="col-md-6"><?= $form->field($model, 'phone') ?></div>
</div>
<div class="box-footer">
    <div class="col-md-6"><?= $form->field($model, 'website_url') ?></div>
    <div class="col-md-3"><?= $form->field($model, 'website_category')->dropDownList($categories) ?></div>
    <div class="col-md-3"><?= $form->field($model, 'website_language')->dropDownList($languages) ?></div>
</div>
<div class="box-footer">
    <div class="col-md-6"><?= $form->field($model, 'payment_method')->dropDownList($paymentMethods) ?></div>
    <div class="col-md-6"><?= $form->field($model, 'payment_number') ?></div>
</div>
<div class="text-center checkbox-branded">
    <?= $form->field($model, 'agree', [
        'showRequiredIndicator' => false,
    ])->checkbox([
        'template' => '{input}{label}{error}'
    ]); ?>
</div>
<div class="box-footer text-center">
    <?= Html::submitButton(Yii::t('user', 'Sign up'), ['class' => 'btn btn-red']) ?>
</div>
<div class="box-footer text-center">
    <?= Html::a(Yii::t('user', 'Already registered? Sign in!'), ['login']) ?>
</div>
<?php ActiveForm::end(); ?>
<?php

use yii\helpers\Html;
use common\modules\partners\search\PartnersSiteSearch;
use common\widgets\GridView;
use kartik\form\ActiveForm;
use yii\data\ActiveDataProvider;

/**
 * @var $this \yii\web\View
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel PartnersSiteSearch
 * @var $categories array
 * @var $languages array
 */

$this->title = Yii::t('partners', 'Websites');
$this->params['breadcrumbs'][] = $this->title;
$form = ActiveForm::begin([
	'type' => ActiveForm::TYPE_VERTICAL,
	'id' => 'site-add',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'options' => [
		'class' => 'box box-primary form-linear',
	]
]);
?>
<div class="box-body">
	<?php
	echo $form->field($model, 'address');
	echo $form->field($model, 'category_id')->dropDownList($categories);
	echo $form->field($model, 'lang_code')->dropDownList($languages);
	echo Html::submitButton(Yii::t('partners', 'Add site'), ['class' => 'btn btn-red']);
	?>
</div>
<?php
ActiveForm::end();

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'id' => 'sites-list',
	'panelHeading' => 'Websites',
	'columns' => [
		[
			'attribute' => 'address',
		],
		[
			'attribute' => 'category_id',
			'content' => function($model) use ($categories){
				return $categories[$model->category_id] ?? '';
			},
			'headerOptions' => ['width' => '200px'],
			'filter' => $categories,
		],
		[
			'attribute' => 'lang_code',
			'content' => function($model) use ($languages){
				return $languages[$model->lang_code] ?? '';
			},
			'headerOptions' => ['width' => '200px'],
			'filter' => $languages,
		],
	],
]);

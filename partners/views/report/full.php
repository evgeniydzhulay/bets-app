<?php

use common\widgets\GridView;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use partners\search\FullReportSearch;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var $this \yii\web\View
 * @var $searchModel FullReportSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $currencies
 * @var $sites array
 */

Pjax::begin([
	'linkSelector' => '#full-list .pagination a',
	'formSelector' => '#full-list-search',
	'id' => 'full-list-container',
	'timeout' => 5000,
	'clientOptions' => [
		'maxCacheLength' => 0,
	],
]);
$formSearch = ActiveForm::begin([
	'id' => 'full-list-search',
	'options' => [
		'class' => 'box box-primary form-linear not-boxed',
		'autocomplete' => 'off',
	],
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'method' => 'GET',
	'action' => ['full'],
]);
?>
	<div class="box-body">
		<?php
		echo $formSearch->field($searchModel, 'amount_currency')->widget(Select2::class, [
			'theme' => Select2::THEME_DEFAULT,
			'data' => $currencies,
			'options' => [
				'placeholder' => Yii::t('partners', 'Select currency'),
				'multiple' => false,
			],
		]);
		echo $formSearch->field($searchModel, 'site_id')->widget(Select2::class, [
			'theme' => Select2::THEME_DEFAULT,
			'data' => $sites,
			'options' => [
				'placeholder' => Yii::t('partners', 'Select website'),
				'multiple' => false,
			],
			'pluginOptions' => [
				'allowClear' => true,
			],
		]);
		echo $formSearch->field($searchModel, 'created_from')->widget(DatePicker::class, [
			'id' => 'created_at-date_range',
			'model' => $searchModel,
			'attribute2' => 'created_to',
			'type' => DatePicker::TYPE_RANGE,
			'separator' => Yii::t('app', 'to'),
			'pluginOptions' => [
				'todayHighlight' => true,
				'weekStart' => 1,
				'autoclose' => true,
				'format' => 'yyyy-mm-dd',
			],
		]);
		echo Html::submitButton(Yii::t('partners', 'Generate report'), ['class' => 'btn btn-red']);
		?>
	</div>
<?php
ActiveForm::end();
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'id' => 'full-list',
	'columns' => [
		'site_id',
		[
			'header' => Yii::t('partners', 'Website'),
			'content' => function($model) use ($sites) {
				return $sites[$model->site_id];
			}
		],
		'registrations',
		'deposit_count',
		[
			'attribute' => 'amount_deposit',
			'content' => function ($model) {
				if(!empty($model->amount_deposit)) {
					return Yii::$app->formatter->asCurrency($model->amount_deposit, $model->amount_currency);
				}
				return Yii::t('yii', '(not set)');
			}
		],
		[
			'attribute' => 'amount_profit',
			'content' => function ($model) {
				if(!empty($model->amount_profit)) {
					return Yii::$app->formatter->asCurrency($model->amount_profit, $model->amount_currency);
				}
				return Yii::t('yii', '(not set)');
			}
		],
	]
]);
Pjax::end();
<?php

use common\assets\HighchartsAsset;
use kartik\form\ActiveForm;
use yii\helpers\Html;
use yii\web\View;

/**
 * @var $this View
 * @var $stats array
 * @var $currencies array
 * @var $currency string
 * @var $period integer
 */

HighchartsAsset::register($this);

?>
<div class="statistics-main">
    <div class="statistics-header">
        <div class="statistics-amount withdraw">
            <div class="amount"><?= Yii::$app->formatter->asCurrency($stats['withdraw'], $currency); ?></div>
            <div class="label">
                <i class="fas fa-money-bill-alt"></i> <?= Yii::t('partners', 'Available to withdraw'); ?>
            </div>
        </div>
        <div class="statistics-amount yesterday">
            <div class="amount"><?= Yii::$app->formatter->asCurrency($stats['yesterday'], $currency); ?></div>
            <div class="label">
                <span class="glyphicon glyphicon-flash" aria-hidden="true"></span> <?= Yii::t('partners', 'Yesterday'); ?>
            </div>
        </div>
        <div class="statistics-amount month">
            <div class="amount"><?= Yii::$app->formatter->asCurrency($stats['month'], $currency); ?></div>
            <div class="label">
                <i class="far fa-chart-bar"></i> <?= Yii::t('partners', 'Current month'); ?>
            </div>
        </div>
        <div class="statistics-amount days30">
            <div class="amount"><?= Yii::$app->formatter->asCurrency($stats['days30'], $currency); ?></div>
            <div class="label">
                <i class="far fa-calendar-alt"></i> <?= Yii::t('partners', '30 days'); ?>
            </div>
        </div>
        <div class="statistics-amount total">
            <div class="amount"><?= Yii::$app->formatter->asCurrency($stats['total'], $currency); ?></div>
            <div class="label">
                <i class="far fa-check-circle"></i> <?= Yii::t('partners', 'Total'); ?>
            </div>
        </div>
    </div>
    <?php
    $formSearch = ActiveForm::begin([
	    'id' => 'mainpage-search',
	    'options' => [
		    'class' => 'box box-primary form-linear not-boxed',
		    'autocomplete' => 'off',
	    ],
	    'enableAjaxValidation' => false,
	    'enableClientValidation' => true,
	    'method' => 'GET',
	    'action' => ['index'],
    ]);
    ?>
    <div class="box-body">
        <div class="form-group">
	        <?= Html::dropDownList('currency', $currency, $currencies, [
                'class' => 'form-control'
            ]); ?>
        </div>
        <div class="form-group">
		    <?= Html::dropDownList('period', $period, [
			    30 => Yii::t('partners', '1 month'),
			    90 => Yii::t('partners', '3 months'),
			    180 => Yii::t('partners', '6 months'),
		    ], [
			    'class' => 'form-control'
		    ]); ?>
        </div>
		<?= Html::submitButton(Yii::t('partners', 'Show'), [
            'class' => 'btn btn-red no-margin'
        ]); ?>
    </div>
	<?php
	ActiveForm::end();
    ?>
    <div class="charts">
        <div class="highcharts-block" id="conversion-chart"></div>
        <div class="highcharts-block" id="registrations-chart"></div>
    </div>
</div>
<script>
    function legendClick(n) {
        var i = [], t;
        n.target.visible !== !0 && i.push(n.target.index);
        $.each(n.target.chart.legend.allItems, function(t, r) {
            r.index !== n.target.index && r.visible === !0 && i.push(r.index)
        });
        var u = Math.floor((100 - i.length) / i.length), r = 0;
        for (t = 0; t < n.target.chart.yAxis.length; t++) {
            t < n.target.chart.yAxis.length && i.indexOf(t) !== -1 && (n.target.chart.yAxis[t].update({
                height: u + "%",
                top: r === 0 ? "0%" : (u + 1) * r + "%"
            }), r++);
        }
    }
    window.onload = function() {
        $.getJSON('/report/statistics?currency=<?= $currency; ?>&period=<?= $period; ?>', function (data) {
            var registrations = [], depositors = [], comissions = [], views = [], clicks = [], links = [],
                dataLength = data.length, i = 0, groupingUnits = [["week", [1]], ["month", [1]]];
            for (i; i < dataLength; i += 1) {
                registrations.push([data[i][0], Math.round(data[i][1])]);
                depositors.push([data[i][0], Math.round(data[i][2])]);
                comissions.push([data[i][0], Math.round(data[i][3])]);
                views.push([data[i][0], Math.round(data[i][4])]);
                clicks.push([data[i][0], Math.round(data[i][5])]);
                links.push([data[i][0], Math.round(data[i][6])]);
            }

            Highcharts.stockChart('conversion-chart', {
                chart: { styledMode:true },
                credits: { enabled: !1 },
                rangeSelector: {
                    selected: 3,
                    inputEnabled: !1,
                    buttons: [
                        {type: "week", count: 1, text: "1w"},
                        {type: "month", count: 1, text: "1m"},
                        {type: "all", text: "All"},
                    ]
                },
                title: {
                    name: '<?= Yii::t('partners', 'Conversions statistics'); ?>',
                    align: 'left',
                },
                yAxis: [{
                    labels: {align: 'right', x: -3},
                    height: '33%',
                    lineWidth: 2,
                    showEmpty: !1,
                }, {
                    labels: {align: 'right', x: -3},
                    top: '33%',
                    height: '33%',
                    offset: 0,
                    lineWidth: 2,
                    showEmpty: !1,
                }, {
                    labels: {align: 'right', x: -3},
                    top: '66%',
                    height: '33%',
                    offset: 0,
                    lineWidth: 2,
                    showEmpty: !1,
                }],
                plotOptions: {
                    series: {
                        events: {
                            legendItemClick: function(n) {
                                legendClick(n);
                            }
                        }
                    }
                },
                tooltip: {split: !0, xDateFormat: "%A, %b %e"},
                legend: {
                    enabled: !0,
                    align: "center",
                    floating: !1,
                    itemMarginTop: 5,
                    itemMarginBottom: 5,
                    symbolRadius: 0,
                    borderWidth: 0,
                    verticalAlign: "bottom"
                },
                series: [{
                    name: '<?= Yii::t('partners', 'Views'); ?>',
                    data: views,
                    dataGrouping: {
                        units: groupingUnits
                    },
                    colorIndex: 0,
                }, {
                    name: '<?= Yii::t('partners', 'Clicks'); ?>',
                    data: clicks,
                    yAxis: 1,
                    dataGrouping: {
                        units: groupingUnits
                    },
                    colorIndex: 1,
                }, {
                    name: '<?= Yii::t('partners', 'Direct links'); ?>',
                    data: links,
                    yAxis: 2,
                    dataGrouping: {
                        units: groupingUnits
                    },
                    colorIndex: 2,
                }]
            });
            Highcharts.stockChart('registrations-chart', {
                chart: { styledMode:true },
                credits: { enabled: !1 },
                rangeSelector: {
                    selected: 3,
                    inputEnabled: !1,
                    buttons: [
                        {type: "week", count: 1, text: "1w"},
                        {type: "month", count: 1, text: "1m"},
                        {type: "all", text: "All"},
                    ]
                },
                title: {
                    name: '<?= Yii::t('partners', 'Registrations statistics'); ?>',
                    align: 'left',
                },
                yAxis: [{
                    labels: {align: 'right', x: -3},
                    height: '33%',
                    lineWidth: 2,
                    showEmpty: !1,
                }, {
                    labels: {align: 'right', x: -3},
                    top: '33%',
                    height: '33%',
                    offset: 0,
                    lineWidth: 2,
                    showEmpty: !1,
                }, {
                    labels: {align: 'right', x: -3},
                    top: '66%',
                    height: '33%',
                    offset: 0,
                    lineWidth: 2,
                    showEmpty: !1,
                }],
                plotOptions: {
                    series: {
                        events: {
                            legendItemClick: function(n) {
                                legendClick(n);
                            }
                        }
                    }
                },
                tooltip: {split: !0, xDateFormat: "%A, %b %e"},
                legend: {
                    enabled: !0,
                    align: "center",
                    floating: !1,
                    itemMarginTop: 5,
                    itemMarginBottom: 5,
                    symbolRadius: 0,
                    borderWidth: 0,
                    verticalAlign: "bottom"
                },
                series: [{
                    name: '<?= Yii::t('partners', 'Registrations'); ?>',
                    data: registrations,
                    dataGrouping: {
                        units: groupingUnits
                    },
                    colorIndex: 3,
                }, {
                    name: '<?= Yii::t('partners', 'New depositors'); ?>',
                    data: depositors,
                    yAxis: 1,
                    dataGrouping: {
                        units: groupingUnits
                    },
                    colorIndex: 4,
                }, {
                    name: '<?= Yii::t('partners', 'Comission amount'); ?>',
                    data: comissions,
                    yAxis: 2,
                    dataGrouping: {
                        units: groupingUnits
                    },
                    colorIndex: 5,
                }]
            });
        });
    };
</script>

<?php

use kartik\form\ActiveForm;

/**
 * @var $this \yii\web\View
 *
 * @var $website null|string
 * @var $websites array
 * @var $currency null|string
 * @var $currencies array
 * @var $dateFrom string
 * @var $dateTo string
 * @var $views int
 * @var $clicks int
 * @var $links int
 * @var $registrations int
 * @var $deposits int
 * @var $deposits_amount mixed
 * @var $profit_amount mixed
 */

$formSearch = ActiveForm::begin([
	'id' => 'summary-search',
	'options' => [
		'class' => 'box box-primary form-inline not-boxed',
		'autocomplete' => 'off',
	],
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'method' => 'GET',
	'action' => ['summary'],
]);
?>
<div class="box-body">
	<div class="form-group">
		<?= \yii\helpers\Html::dropDownList('currency', $currency, $currencies, [
			'class' => 'form-control'
		]); ?>
	</div>
	<div class="form-group">
		<?= \yii\helpers\Html::dropDownList('website', $website, [null => Yii::t('partners', 'Websites')] + $websites, [
			'class' => 'form-control'
		]); ?>
	</div>
	<div class="form-group">
		<?= \kartik\date\DatePicker::widget([
			'name' => 'from',
			'value' => $dateFrom,
			'pluginOptions' => [
				'todayHighlight' => true,
				'weekStart' => 1,
				'autoclose' => true,
				'format' => 'yyyy-mm-dd',
			]
		]); ?>
	</div>
	<div class="form-group">
		<?= \kartik\date\DatePicker::widget([
			'name' => 'to',
			'value' => $dateTo,
			'pluginOptions' => [
				'todayHighlight' => true,
				'weekStart' => 1,
				'autoclose' => true,
				'format' => 'yyyy-mm-dd',
			]
		]); ?>
	</div>
	<?= \yii\helpers\Html::submitButton(Yii::t('partners', 'Show'), [
		'class' => 'btn btn-red no-margin'
	]); ?>
</div>
<div class="box-body">
<table class="table table-bordered">
	<tr><td><?= Yii::t('partners', 'Views'); ?></td><td><?= $views; ?></td></tr>
	<tr><td><?= Yii::t('partners', 'Clicks'); ?></td><td><?= $clicks; ?></td></tr>
	<tr><td><?= Yii::t('partners', 'Links'); ?></td><td><?= $links; ?></td></tr>
	<tr><td><?= Yii::t('partners', 'Registrations'); ?></td><td><?= $registrations; ?></td></tr>
	<tr>
		<td><?= Yii::t('partners', 'Registrations'); ?> / <?= Yii::t('partners', 'Clicks'); ?></td>
		<td><?= Yii::$app->formatter->asPercent(($clicks > 0 ? ($registrations / $clicks) : 0)); ?></td>
	</tr>
	<tr><td><?= Yii::t('partners', 'Deposits count'); ?></td><td><?= $deposits; ?></td></tr>
	<tr>
		<td><?= Yii::t('partners', 'Deposits count'); ?> / <?= Yii::t('partners', 'Registrations'); ?></td>
		<td><?= Yii::$app->formatter->asPercent(($registrations > 0 ? ($deposits / $registrations) : 0)); ?></td>
	</tr>
	<tr>
		<td><?= Yii::t('partners', 'Deposit amount'); ?></td>
		<td><?= Yii::$app->formatter->asCurrency($deposits_amount, $currency); ?></td>
	</tr>
	<tr>
		<td><?= Yii::t('partners', 'Profit'); ?></td>
		<td><?= Yii::$app->formatter->asCurrency($profit_amount, $currency); ?></td>
	</tr>
	<tr>
		<td><?= Yii::t('partners', 'Average profit'); ?></td>
		<td><?= Yii::$app->formatter->asCurrency(($registrations > 0 ? ($profit_amount / $registrations) : 0), $currency); ?></td>
	</tr>
</table>
</div>
<?php
ActiveForm::end();
?>
<?php

/**
 * @var $this \yii\web\View
 * @var $searchModel \partners\search\PromoStatisticsSearch
 * @var $dataProvider \yii\data\ActiveDataProvider
 * @var $currencies
 * @var $sites array
 */

use common\widgets\GridView;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\Html;
use yii\widgets\Pjax;

Pjax::begin([
	'linkSelector' => '#players-list .pagination a',
	'formSelector' => '#players-list-search',
	'id' => 'players-list-container',
	'timeout' => 5000,
	'clientOptions' => [
		'maxCacheLength' => 0,
	],
]);
$formSearch = ActiveForm::begin([
	'id' => 'players-list-search',
	'options' => [
		'class' => 'box box-primary form-linear not-boxed',
		'autocomplete' => 'off',
	],
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'method' => 'GET',
	'action' => ['players'],
]);
?>
	<div class="box-body">
		<?php
		echo $formSearch->field($searchModel, 'amount_currency')->widget(Select2::class, [
			'theme' => Select2::THEME_DEFAULT,
			'data' => $currencies,
			'options' => [
				'placeholder' => Yii::t('partners', 'Select currency'),
				'multiple' => false,
			],
		]);
		echo $formSearch->field($searchModel, 'sid');
		echo $formSearch->field($searchModel, 'created_from')->widget(DatePicker::class, [
			'id' => 'created_at-date_range',
			'model' => $searchModel,
			'attribute2' => 'created_to',
			'type' => DatePicker::TYPE_RANGE,
			'separator' => Yii::t('app', 'to'),
			'pluginOptions' => [
				'todayHighlight' => true,
				'weekStart' => 1,
				'autoclose' => true,
				'format' => 'yyyy-mm-dd',
			],
		]);
		?>
	</div>
	<div class="box-body">
		<?php
		echo Html::submitButton(Yii::t('partners', 'Generate report'), ['class' => 'btn btn-red']);
		?>
	</div>
<?php
ActiveForm::end();
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'id' => 'players-list',
	'columns' => [
		'material_id',
		'sid',
		'registrations',
		'deposit_count',
		[
			'attribute' => 'amount_deposit',
			'content' => function ($model) {
				if(!empty($model->amount_deposit)) {
					return Yii::$app->formatter->asCurrency($model->amount_deposit, $model->amount_currency);
				}
				return Yii::t('yii', '(not set)');
			}
		],
		[
			'attribute' => 'amount_profit',
			'content' => function ($model) {
				if(!empty($model->amount_profit)) {
					return Yii::$app->formatter->asCurrency($model->amount_profit, $model->amount_currency);
				}
				return Yii::t('yii', '(not set)');
			}
		],
	]
]);
Pjax::end();
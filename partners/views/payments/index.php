<?php

use common\modules\partners\search\PartnersWithdrawSearch;
use common\widgets\GridView;
use kartik\date\DatePicker;
use kartik\form\ActiveForm;
use kartik\select2\Select2;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;
use yii\widgets\Pjax;

/**
 * @var $this \yii\web\View
 * @var $searchModel PartnersWithdrawSearch
 * @var $dataProvider ActiveDataProvider
 * @var $statuses array
 * @var $currencies array
 */

Pjax::begin([
	'linkSelector' => '#full-list .pagination a',
	'formSelector' => '#full-list-search',
	'id' => 'full-list-container',
	'timeout' => 5000,
	'clientOptions' => [
		'maxCacheLength' => 0,
	],
]);
$formSearch = ActiveForm::begin([
	'id' => 'full-list-search',
	'options' => [
		'class' => 'box box-primary form-linear not-boxed',
		'autocomplete' => 'off',
	],
	'enableAjaxValidation' => false,
	'enableClientValidation' => true,
	'method' => 'GET',
	'action' => ['index'],
]);
?>
<div class="box-body">
	<?php
	echo $formSearch->field($searchModel, 'currency_code')->dropDownList($currencies);
	echo $formSearch->field($searchModel, 'status')->dropDownList($statuses);
	?>
	<div class="form-group col-md-3 highlight-addon field-created_at">
		<label class="control-label has-star" for="created_at"><?= Yii::t('partners', 'Created at'); ?></label>
		<?= DatePicker::widget([
			'name' => 'created_at',
			'value' => date('Y-m-d', $searchModel->created_at ?? time()),
			'pluginOptions' => [
				'todayHighlight' => true,
				'weekStart' => 1,
				'autoclose' => true,
				'format' => 'yyyy-mm-dd',
			],
		]) ?>
		<div class="help-block"></div>
	</div>
	<?= Html::submitButton(Yii::t('partners', 'Generate report'), ['class' => 'btn btn-red']); ?>
</div>
<?php
ActiveForm::end();
echo GridView::widget([
	'dataProvider' => $dataProvider,
	'id' => 'payments-report',
	'panelHeading' => 'Payments',
	'columns' => [
		[
			'attribute' => 'currency_code',
			'filter' => $currencies,
		],
		[
			'attribute' => 'created_at',
			'content' => function ($model) {
				return date('Y-m-d H:i:s', $model['created_at']);
			},
		],
		[
			'attribute' => 'amount',
			'content' => function($model) {
				return Yii::$app->formatter->asCurrency($model->amount, $model->currency_code);
			},
		],
		[
			'attribute' => 'status',
			'content' => function($model) use ($statuses) {
				return $statuses[$model->status] ?? '';
			}
		],
	],
]);
Pjax::end();

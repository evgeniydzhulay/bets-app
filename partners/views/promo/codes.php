<?php

use common\modules\partners\search\PartnersPromocodesSearch;
use yii\helpers\Html;
use common\widgets\GridView;
use kartik\form\ActiveForm;
use yii\data\ActiveDataProvider;

/**
 * @var $this \yii\web\View
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel PartnersPromocodesSearch
 * @var $sites array
 */

$this->title = Yii::t('partners', 'Websites');
$this->params['breadcrumbs'][] = $this->title;
$form = ActiveForm::begin([
	'type' => ActiveForm::TYPE_VERTICAL,
	'id' => 'site-add',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'options' => [
		'class' => 'box box-primary form-linear',
	]
]);
?>
	<div class="box-body">
		<?php
		echo $form->field($model, 'site_id')->dropDownList($sites);
		echo $form->field($model, 'campaign_id')->dropDownList($campaigns);
		echo $form->field($model, 'amount');
		echo Html::submitButton(Yii::t('partners', 'Generate promo code'), ['class' => 'btn btn-red']);
		?>
	</div>
<?php
ActiveForm::end();

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'id' => 'sites-list',
	'panelHeading' => 'Websites',
	'columns' => [
		[
			'attribute' => 'code',
		],
		[
		    'attribute' => 'amount',
        ],
		[
			'attribute' => 'site_id',
			'content' => function($model) use ($sites){
				return $sites[$model->site_id] ?? '';
			},
			'headerOptions' => ['width' => '200px'],
			'filter' => $sites,
		],
		[
			'attribute' => 'campaign_id',
			'content' => function($model) use ($campaigns){
				return $campaigns[$model->campaign_id] ?? '';
			},
			'headerOptions' => ['width' => '200px'],
			'filter' => $campaigns,
		],
	],
]);

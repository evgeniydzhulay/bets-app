<?php
use common\modules\partners\search\PromoMaterialsSearch;
use common\widgets\GridView;
use yii\data\ActiveDataProvider;
use yii\helpers\Html;

/**
 * @var $this \yii\web\View
 * @var $dataProvider ActiveDataProvider
 * @var $searchModel PromoMaterialsSearch
 * @var $sites array
 * @var $types array
 * @var $languages array
 * @var $currencies array
 * @var $sizes_w array
 * @var $sizes_h array
 */

echo GridView::widget([
	'dataProvider' => $dataProvider,
	'filterModel' => $searchModel,
	'id' => 'sites-list',
	'panelHeading' => 'Websites',
	'columns' => [
		[
			'attribute' => 'id',
			'headerOptions' => ['width' => '50px'],
		],
		[
			'attribute' => 'title',
		],
		[
			'attribute' => 'type',
			'headerOptions' => ['width' => '200px'],
			'filter' => $types,
			'content' => function($model) use ($types) {
				return $types[$model->type] ?? '';
			}
		],
		[
			'attribute' => 'lang_code',
			'headerOptions' => ['width' => '100px'],
			'filter' => $languages,
		],
		[
			'attribute' => 'currency_code',
			'headerOptions' => ['width' => '100px'],
			'filter' => $currencies,
		],
		[
			'attribute' => 'size',
			'headerOptions' => ['width' => '250px'],
			'filter' => '<div class="input-group">' .
				Html::activeDropDownList($searchModel, 'size_w', [null => Yii::t('yii', '(not set)')] + $sizes_w, [
					'class' => 'form-control'
				]) .
				'<span class="input-group-addon">x</span>' .
				Html::activeDropDownList($searchModel, 'size_h', [null => Yii::t('yii', '(not set)')] + $sizes_h, [
					'class' => 'form-control'
				]) .
			'</div>',
			'content' => function($model) {
				return "{$model->size_h}x{$model->size_w}";
			}
		],
	],
]);

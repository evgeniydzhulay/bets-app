<?php

use common\modules\partners\models\PartnersLinksModel;
use common\modules\partners\search\PartnersLinksSearch;
use common\widgets\GridView;
use kartik\form\ActiveForm;
use yii\data\ActiveDataProvider;
use yii\web\View;
use yii\helpers\Html;

/**
 * @var View $this
 * @var ActiveDataProvider $dataProvider
 * @var PartnersLinksSearch $searchModel
 * @var array $statuses
 * @var array $sites
 * @var array $campaigns
 */

$this->title = Yii::t('partners', 'Links');
$this->params['breadcrumbs'][] = $this->title;
$form = ActiveForm::begin([
	'type' => ActiveForm::TYPE_VERTICAL,
	'id' => 'site-add',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'options' => [
		'class' => 'box box-primary form-linear',
	]
]);
?>
<div class="box-body">
    <?php
    echo $form->field($model, 'site_id')->dropDownList($sites);
    echo $form->field($model, 'campaign_id')->dropDownList($campaigns);
    echo $form->field($model, 'page_url');
    echo $form->field($model, 'page_sid');
    echo Html::submitButton(Yii::t('partners', 'Generate link'), ['class' => 'btn btn-red']);
    ?>
</div>
<?php
ActiveForm::end();

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'filterModel' => $searchModel,
    'id' => 'materials-list',
    'columns' => [
	    [
		    'attribute' => 'site_id',
		    'content' => function($model) use ($sites){
			    return $sites[$model->site_id] ?? '';
		    },
		    'headerOptions' => ['width' => '200px'],
		    'filter' => $sites,
	    ],
	    [
		    'attribute' => 'page_url',
		    'headerOptions' => ['width' => '200px'],
	    ],
	    [
		    'attribute' => 'page_sid',
		    'headerOptions' => ['width' => '200px'],
	    ],
        [
            'attribute' => 'link',
            'headerOptions' => ['width' => '300px'],
            'content' => function($model) {
                /* @var $model PartnersLinksModel */
                $link = $model->getFullLink();
                $result = Yii::t('partners', 'Link copied');
                return "<span class='content-copy' data-copy='{$link}' data-result='{$result}'><i class='fa fa-copy'></i></span> {$model['link']}";
            }
        ],
        [
            'attribute' => 'description',
            'content' => function($model) {
                return mb_substr(strip_tags($model->description), 0 , 500);
            }
        ],
        [
            'class' => \yii\grid\ActionColumn::class,
            'template' => '{update}',
            'headerOptions' => ['width' => '20px'],
        ],
    ],
]);
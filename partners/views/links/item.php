<?php

use common\modules\partners\models\PartnersLinksModel;
use kartik\form\ActiveForm;

/* @var $this \yii\web\View */
/* @var $model PartnersLinksModel */
/* @var $partners array */


$this->params['breadcrumbs'][] = ['label' => Yii::t('partners', 'Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = Yii::t('partners', 'Edit link {link}', ['link' => $model->id]);
$form = ActiveForm::begin([
	'type' => ActiveForm::TYPE_HORIZONTAL,
	'id' => 'content-block',
	'enableAjaxValidation' => true,
	'enableClientValidation' => true,
	'options' => [
		'class' => 'box box-primary',
	]
])
?>
<div class="box-body">
    <h4 class='content-copy text-center' data-copy='<?= $model->getFullLink(); ?>' data-result='<?= Yii::t('partners', 'Link copied') ?>'>
	    <?= $model->getFullLink(); ?>
    </h4>
	<?= $form->field($model, 'description')->textarea([
		'rows' => 5
	]); ?>
</div>
<div class="box-footer text-center">
	<?= \yii\helpers\Html::submitButton(Yii::t('partners', 'Save'), ['class' => 'btn btn-red']) ?>
</div>
<?php ActiveForm::end() ?>

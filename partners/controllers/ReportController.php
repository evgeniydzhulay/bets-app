<?php

namespace partners\controllers;

use common\modules\partners\models\PartnersEarningsModel;
use common\modules\partners\models\PartnersLeadsModel;
use common\modules\partners\models\PartnersLogClicksModel;
use common\modules\partners\models\PartnersLogLinksModel;
use common\modules\partners\models\PartnersLogViewsModel;
use common\modules\partners\models\PartnersSiteModel;
use common\modules\partners\models\PartnersWithdrawModel;
use common\modules\partners\models\PromoCampaignsModel;
use common\traits\DateToTimeTrait;
use partners\search\FullReportSearch;
use partners\search\PartnersLeadsSearch;
use partners\search\PromoStatisticsSearch;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\web\Controller;
use Yii;
use yii\web\Response;

class ReportController extends Controller {

	use DateToTimeTrait;

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'except' => ['index'],
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionIndex($period = 30, $currency = 'USD') {
		if(Yii::$app->user->isGuest) {
			return $this->redirect(['/core/welcome']);
		}
		if($period < 30) $period = 30;
		if($period > 180) $period = 180;
		$total = PartnersEarningsModel::find()->select([
			'amount' => new Expression('SUM(amount)')
		])->andWhere([
			'partner_id' => \Yii::$app->partner->id,
			'currency_code' => $currency,
		])->column()[0] ?? 0;
		return $this->render('index', [
			'currency' => $currency,
			'currencies' => Yii::$app->get('currencies')->getlist(),
			'period' => $period,
			'stats' => [
				'withdraw' => $total - (PartnersWithdrawModel::find()->select([
					'amount' => new Expression('SUM(amount)')
				])->andWhere(['partner_id' => \Yii::$app->partner->id, 'currency_code' => $currency])->column()[0] ?? 0),
				'yesterday' => PartnersEarningsModel::find()->select([
					'amount' => new Expression('SUM(amount)')
				])->andWhere(['partner_id' => \Yii::$app->partner->id, 'currency_code' => $currency ])->andWhere([
					'>', 'created_at', mktime(0, 0, 0, date('m'), date('d') - 1)
				])->andWhere([
					'<', 'created_at', mktime(0, 0, 0, date('m'), date('d'))
				])->column()[0] ?? 0,
				'month' => PartnersEarningsModel::find()->select([
					'amount' => new Expression('SUM(amount)')
				])->andWhere(['partner_id' => \Yii::$app->partner->id, 'currency_code' => $currency])->andWhere([
					'>', 'created_at', mktime(0, 0, 0, date('m'), 1)
				])->column()[0] ?? 0,
				'days30' => PartnersEarningsModel::find()->select([
					'amount' => new Expression('SUM(amount)')
				])->andWhere(['partner_id' => \Yii::$app->partner->id, 'currency_code' => $currency])->andWhere([
					'>', 'created_at', mktime(0, 0, 0, date('m'), date('d') - 30)
				])->column()[0] ?? 0,
				'total' => $total,
			]
		]);
	}

	public function actionStatistics($period = 30, $currency = 'USD') {
		Yii::$app->response->format = Response::FORMAT_JSON;
		if($period < 30) $period = 30;
		if($period > 180) $period = 180;
		$leads = PartnersLeadsModel::find()->select([
			'time' => new Expression("FROM_UNIXTIME(`created_at`, '%Y-%m-%d')"),
			'registrations' => new Expression('COUNT(*)'),
			'depositors' => new Expression('SUM( CASE WHEN `amount_profit` > 0 THEN 1 ELSE 0 END )'),
			'profit' => new Expression('SUM(`amount_profit`)'),
		])->andWhere([
			'partner_id' => \Yii::$app->partner->id,
			'amount_currency' => $currency,
		])->andWhere([
			'>', 'created_at', $period * 86400
		])->groupBy('time')->indexBy('time')->asArray()->all();
		$logClicks = PartnersLogClicksModel::find()->select([
			'time' => new Expression("FROM_UNIXTIME(`created_at`, '%Y-%m-%d')"),
			'count' => new Expression('COUNT(*)'),
		])->andWhere(['partner_id' => \Yii::$app->partner->id])->andWhere([
			'>', 'created_at', $period * 86400
		])->groupBy('time')->indexBy('time')->asArray()->all();
		$logLinks = PartnersLogLinksModel::find()->select([
			'time' => new Expression("FROM_UNIXTIME(`created_at`, '%Y-%m-%d')"),
			'count' => new Expression('COUNT(*)'),
		])->andWhere(['partner_id' => \Yii::$app->partner->id])->andWhere([
			'>', 'created_at', $period * 86400
		])->groupBy('time')->indexBy('time')->asArray()->all();
		$logViews = PartnersLogViewsModel::find()->select([
			'time' => new Expression("FROM_UNIXTIME(`created_at`, '%Y-%m-%d')"),
			'count' => new Expression('COUNT(*)'),
		])->andWhere(['partner_id' => \Yii::$app->partner->id])->andWhere([
			'>', 'created_at', $period * 86400
		])->groupBy('time')->indexBy('time')->asArray()->all();
		$result = [];
		for($i = 0; $i <= $period; $i++) {
			$time = mktime(0,0,0, date('m'), date('d') - $period + $i);
			$date = date('Y-m-d', $time);
			$result[] = [
				$time * 1000,
				(int) (($leads[$date] ?? [])['registrations'] ?? 0),
				(int) (($leads[$date] ?? [])['depositors'] ?? 0),
				(float) (($leads[$date] ?? [])['profit'] ?? 0),
				(int) (($logViews[$date] ?? [])['count'] ?? 0),
				(int) (($logClicks[$date] ?? [])['count'] ?? 0),
				(int) (($logLinks[$date] ?? [])['count'] ?? 0),
			];
		}
		return $result;
	}

	public function actionSummary($website = null, $currency = null, $from = null, $to = null) {
		$websites = PartnersSiteModel::find()->andWhere(['partner_id' => Yii::$app->partner->id])->select('address')->indexBy('id')->column();
		$currencies = Yii::$app->get('currencies')->getlist();
		if(is_null($from) || !self::getDateIsValid($from)) {
			$from = date('Y-m-d', mktime(0,0,0, date('m') - 1));
		}
		$timeFrom = self::getTimeFromDate($from);
		if(is_null($to) || !self::getDateIsValid($to)) {
			$to = date('Y-m-d', mktime(23,59,59));
		}
		$timeTo = self::getTimeFromDate($to);
		return $this->render('summary', [
			'website' => $website, 'websites' => $websites,
			'currency' => $currency, 'currencies' => $currencies,
			'dateFrom' => $from, 'dateTo' => $to,
			'views' => PartnersLogViewsModel::find()->andWhere(['partner_id' => Yii::$app->partner->id])->andWhere(['>', 'created_at', $timeFrom])->andWhere(['<', 'created_at', $timeTo])->count(),
			'clicks' => PartnersLogClicksModel::find()->andWhere(['partner_id' => Yii::$app->partner->id])->andWhere(['>', 'created_at', $timeFrom])->andWhere(['<', 'created_at', $timeTo])->count(),
			'links' => PartnersLogLinksModel::find()->andWhere(['partner_id' => Yii::$app->partner->id])->andWhere(['>', 'created_at', $timeFrom])->andWhere(['<', 'created_at', $timeTo])->count(),

			'registrations' => PartnersLeadsModel::find()->joinWith(['link'], false)->andFilterWhere(['amount_currency' => $currency, 'site_id' => $website])->andWhere([PartnersLeadsModel::tableName() . '.partner_id' => Yii::$app->partner->id])->andWhere(['>', PartnersLeadsModel::tableName() . '.created_at', $timeFrom])->andWhere(['<', PartnersLeadsModel::tableName() . '.created_at', $timeTo])->count(),
			'deposits' => PartnersLeadsModel::find()->joinWith(['link'], false)->andFilterWhere(['amount_currency' => $currency, 'site_id' => $website])->andWhere([PartnersLeadsModel::tableName() . '.partner_id' => Yii::$app->partner->id])->andWhere(['>', PartnersLeadsModel::tableName() . '.created_at', $timeFrom])->andWhere(['<', PartnersLeadsModel::tableName() . '.created_at', $timeTo])->andWhere(['not', ['amount_deposit' => null]])->count(),
			'deposits_amount' => PartnersLeadsModel::find()->joinWith(['link'], false)->andFilterWhere(['amount_currency' => $currency, 'site_id' => $website])->andWhere([PartnersLeadsModel::tableName() . '.partner_id' => Yii::$app->partner->id])->andWhere(['>', PartnersLeadsModel::tableName() . '.created_at', $timeFrom])->andWhere(['<', PartnersLeadsModel::tableName() . '.created_at', $timeTo])->andWhere(['not', ['amount_deposit' => null]])->sum('amount_deposit'),
			'profit_amount' => PartnersLeadsModel::find()->joinWith(['link'], false)->andFilterWhere(['amount_currency' => $currency, 'site_id' => $website])->andWhere([PartnersLeadsModel::tableName() . '.partner_id' => Yii::$app->partner->id])->andWhere(['>', PartnersLeadsModel::tableName() . '.created_at', $timeFrom])->andWhere(['<', PartnersLeadsModel::tableName() . '.created_at', $timeTo])->andWhere(['not', ['amount_deposit' => null]])->sum('amount_profit'),
		]);
	}

	public function actionFull() {
		$searchModel = Yii::createObject(FullReportSearch::class);
		return $this->render('full', [
			'searchModel' => $searchModel,
			'dataProvider' => $searchModel->search([
				'partner_id' => Yii::$app->partner->id
			] + Yii::$app->request->get()),
			'currencies' => Yii::$app->get('currencies')->getlist(),
			'sites' => PartnersSiteModel::find()->andWhere([
				'partner_id' => Yii::$app->partner->id
			])->select('address')->indexBy('id')->column(),
		]);
	}

	public function actionPromo() {
		$searchModel = Yii::createObject(PromoStatisticsSearch::class);
		return $this->render('promo', [
			'searchModel' => $searchModel,
			'dataProvider' => $searchModel->search([
				'partner_id' => Yii::$app->partner->id
			] + Yii::$app->request->get()),
			'currencies' => Yii::$app->get('currencies')->getlist(),
		]);
	}

	public function actionPlayers() {
		$searchModel = Yii::createObject(PartnersLeadsSearch::class);
		return $this->render('players', [
			'searchModel' => $searchModel,
			'dataProvider' => $searchModel->search([
				'partner_id' => Yii::$app->partner->id
			] + Yii::$app->request->get()),
			'currencies' => Yii::$app->get('currencies')->getlist(),
			'sites' => PartnersSiteModel::find()->andWhere([
				'partner_id' => Yii::$app->partner->id
			])->select('address')->indexBy('id')->column(),
			'countries' => Yii::$app->get('locations')->getCountries(),
			'campaigns' => PromoCampaignsModel::find()->select('title')->indexBy('id')->column(),
		]);
	}

	public function actionAffiliates() {
		return $this->render('affiliates');
	}

}
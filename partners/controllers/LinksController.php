<?php

namespace partners\controllers;

use common\modules\partners\models\PartnersLinksModel;
use common\modules\partners\models\PartnersSiteModel;
use common\modules\partners\models\PromoCampaignsModel;
use common\traits\AjaxValidationTrait;
use Yii;
use common\modules\partners\search\PartnersLinksSearch;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;

class LinksController extends Controller {

	use AjaxValidationTrait;

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	/**
	 * @return string
	 */
	public function actionIndex() {
		$model = Yii::createObject(PartnersLinksModel::class);
		$model->scenario = PartnersLinksModel::SCENARIO_CREATE;
		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post())) {
			$model->partner_id = Yii::$app->partner->id;
			if($model->save()) {
				return $this->redirect(['update', 'id' => $model->id]);
			}
		}
		$searchModel = Yii::createObject(PartnersLinksSearch::class);
		$searchModel->scenario = PartnersLinksSearch::SCENARIO_SEARCH;
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $searchModel->search([
				'partner_id' => Yii::$app->partner->id
			] + Yii::$app->request->get()),
			'sites' => PartnersSiteModel::find()->andWhere([
				'partner_id' => Yii::$app->partner->id
			])->select('address')->indexBy('id')->column(),
			'campaigns' => PromoCampaignsModel::find()->select('title')->indexBy('id')->column(),
			'model' => $model,
		]);
	}

	/**
	 * @return string|Response
	 */
	public function actionCreate() {
		$model = Yii::createObject(PartnersLinksModel::class);
		$model->scenario = PartnersLinksModel::SCENARIO_CREATE;
		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->redirect(['update', 'id' => $model->id]);
		}
		Yii::$app->view->title = Yii::t('partners', 'Create link');
		return $this->render('item', [
			'model' => $model
		]);
	}

	/**
	 * @param int $id
	 * @return string|Response
	 */
	public function actionUpdate(int $id) {
		$model = $this->getItem($id);
		$model->scenario = PartnersLinksModel::SCENARIO_UPDATE;
		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post()) && $model->save()) {
			return $this->refresh();
		}
		Yii::$app->view->title = Yii::t('partners', 'Edit link {link}', ['link' => $model->link]);
		return $this->render('item', [
			'model' => $model,
		]);
	}

	/**
	 * Deletes item.
	 * @param  int $id Item id
	 * @return Response
	 * @throws \Throwable
	 */
	public function actionDelete($id) {
		$item = $this->getItem($id);
		$item->delete();
		return $this->redirect(['index']);
	}

	/**
	 * Get block item
	 * @param int $id
	 * @return PartnersLinksModel
	 * @throws NotFoundHttpException
	 */
	protected function getItem(int $id) {
		$item = PartnersLinksModel::find()->andWhere(['id' => $id])->one();
		if ($item) {
			return $item;
		}
		throw new NotFoundHttpException(Yii::t('partners', 'The requested link does not exist'));
	}

}
<?php

namespace partners\controllers;

use common\modules\partners\models\PartnersPromocodesModel;
use common\modules\partners\models\PartnersSiteModel;
use common\modules\partners\models\PromoCampaignsModel;
use common\modules\partners\search\PromoMaterialsSearch;
use common\traits\AjaxValidationTrait;
use Yii;
use common\modules\partners\search\PartnersPromocodesSearch;
use yii\web\Controller;

class PromoController extends Controller {

	use AjaxValidationTrait;

	public function actionCodes() {

		$model = Yii::createObject(PartnersPromocodesModel::class);
		$model->scenario = PartnersPromocodesModel::SCENARIO_CREATE;

		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post())) {
			$model->partner_id = Yii::$app->partner->id;
			if($model->save()) {
				return $this->refresh();
			}
		}

		$searchModel = Yii::createObject(PartnersPromocodesSearch::class);
		$searchModel->scenario = PartnersPromocodesSearch::SCENARIO_SEARCH;
		return $this->render('codes', [
			'searchModel' => $searchModel,
			'dataProvider' => $searchModel->search([
				'partner_id' => Yii::$app->partner->id
			] + Yii::$app->request->get()),
			'campaigns' => PromoCampaignsModel::find()->select('title')->indexBy('id')->column(),
			'sites' => PartnersSiteModel::find()->andWhere([
				'partner_id' => Yii::$app->partner->id
			])->select('address')->indexBy('id')->column(),
			'model' => $model,
		]);
	}

	public function actionMaterials() {
		$searchModel = Yii::createObject(PromoMaterialsSearch::class);
		$searchModel->scenario = PartnersPromocodesSearch::SCENARIO_SEARCH;
		return $this->render('materials', [
			'searchModel' => $searchModel,
			'dataProvider' => $searchModel->search(Yii::$app->request->get() + [
				'partner_id' => Yii::$app->partner->id
			]),
			'types' => PromoMaterialsSearch::types(),
			'languages' => \yii\helpers\ArrayHelper::getColumn(Yii::$app->params['languages'], 'label', true),
			'currencies' => Yii::$app->get('currencies')->getlist(),
			'sizes_h' => PromoMaterialsSearch::find()->groupBy(['size_h'])->orderBy(['size_h' => SORT_ASC])->select('size_h')->indexBy('size_h')->column(),
			'sizes_w' => PromoMaterialsSearch::find()->groupBy(['size_w'])->orderBy(['size_w' => SORT_ASC])->select('size_w')->indexBy('size_w')->column(),
		]);
	}

}
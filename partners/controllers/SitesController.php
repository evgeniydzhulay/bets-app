<?php

namespace partners\controllers;

use common\modules\partners\models\PartnersSiteModel;
use common\modules\partners\search\PartnersSiteSearch;
use common\traits\AjaxValidationTrait;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

class SitesController extends Controller {

	use AjaxValidationTrait;

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionIndex() {
		$model = Yii::createObject(PartnersSiteModel::class);
		$model->scenario = PartnersSiteModel::SCENARIO_CREATE;

		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post())) {
			$model->partner_id = Yii::$app->partner->id;
			if($model->save()) {
				return $this->refresh();
			}
		}

		$searchModel = Yii::createObject(PartnersSiteSearch::class);
		$searchModel->scenario = PartnersSiteSearch::SCENARIO_SEARCH;
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $searchModel->search([
				'partner_id' => Yii::$app->partner->id
			] + Yii::$app->request->get()),
			'categories' => PartnersSiteModel::categories(),
			'languages' => ArrayHelper::getColumn(Yii::$app->params['languages'], 'label'),
			'model' => $model,
		]);
	}
}
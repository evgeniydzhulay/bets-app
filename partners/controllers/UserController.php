<?php

namespace partners\controllers;

use common\modules\partners\models\PartnersSiteModel;
use common\modules\partners\models\PartnersWithdrawModel;
use common\modules\user\forms\PasswordForm;
use common\modules\user\forms\SettingsForm;
use partners\forms\RegistrationForm;
use Yii;
use yii\base\ExitException;
use yii\base\InvalidConfigException;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\modules\user\controllers\AuthController;
use yii\web\Response;

class UserController extends AuthController {

	/**
	 * Displays page where user can update account settings (email or password).
	 *
	 * @return string|Response
	 * @throws ExitException
	 * @throws InvalidConfigException
	 */
	public function actionAccount() {
		/** @var SettingsForm $modelAccount */
		$modelAccount = Yii::createObject(SettingsForm::class);
		$eventAccount = $this->getFormEvent($modelAccount);
		$this->performAjaxValidation($modelAccount);
		$this->trigger(self::EVENT_BEFORE_ACCOUNT_UPDATE, $eventAccount);
		if ($modelAccount->load(Yii::$app->request->post()) && $modelAccount->save()) {
			Yii::$app->session->addFlash('success', Yii::t('user', 'Your account details have been updated'));
			$this->trigger(self::EVENT_AFTER_ACCOUNT_UPDATE, $eventAccount);
			return $this->refresh();
		}

		/** @var PasswordForm $modelPassword */
		$modelPassword = Yii::createObject(PasswordForm::class);
		$eventPassword = $this->getFormEvent($modelPassword);
		$this->performAjaxValidation($modelPassword);
		$this->trigger(self::EVENT_BEFORE_ACCOUNT_UPDATE, $eventPassword);
		if ($modelPassword->load(Yii::$app->request->post()) && $modelPassword->save()) {
			Yii::$app->session->addFlash('success', Yii::t('user', 'Your account password have been updated'));
			$this->trigger(self::EVENT_AFTER_ACCOUNT_UPDATE, $eventPassword);
			return $this->refresh();
		}

		return $this->render('account', [
			'modelAccount' => $modelAccount,
			'modelPassword' => $modelPassword,
		]);
	}

	/**
	 * Displays page where user can update account settings (email or password).
	 *
	 * @return string|Response
	 * @throws ExitException
	 * @throws InvalidConfigException
	 */
	public function actionPassword() {
		return $this->redirect(['auth/account']);
	}

	/**
	 * Displays the registration page.
	 * After successful registration if enableConfirmation is enabled shows info message otherwise redirects to home page.
	 *
	 * @return string
	 * @throws ExitException
	 * @throws InvalidConfigException
	 */
	public function actionRegister() {
		if (!Yii::$app->user->isGuest) {
			$this->goHome();
		}
		/* @var $model RegistrationForm */
		$model = Yii::createObject(RegistrationForm::class);
		$event = $this->getFormEvent($model);
		$this->trigger(self::EVENT_BEFORE_REGISTER, $event);
		$this->performAjaxValidation($model);
		if ($model->load(Yii::$app->request->post()) && $model->register()) {
			$this->trigger(self::EVENT_AFTER_REGISTER, $event);
			return $this->redirect(Url::toRoute(['/']));
		}
		return $this->render('register', [
			'model' => $model,
			'countries' => Yii::$app->get('locations')->getCountries(),
			'categories' => PartnersSiteModel::categories(),
			'languages' => ArrayHelper::getColumn(Yii::$app->params['languages'], 'label'),
			'paymentMethods' => PartnersWithdrawModel::paymentMethods()
		]);
	}
}
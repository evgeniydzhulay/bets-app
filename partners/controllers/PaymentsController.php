<?php

namespace partners\controllers;

use common\modules\partners\search\PartnersWithdrawSearch;
use common\traits\AjaxValidationTrait;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;
use Yii;

class PaymentsController extends Controller {

	use AjaxValidationTrait;

	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::class,
				'rules' => [
					[
						'allow' => true,
						'roles' => ['@'],
					],
				],
			],
		];
	}

	public function actionIndex() {
		$searchModel = Yii::createObject(PartnersWithdrawSearch::class);
		$searchModel->scenario = PartnersWithdrawSearch::SCENARIO_SEARCH;
		return $this->render('index', [
			'searchModel' => $searchModel,
			'dataProvider' => $searchModel->search([
				'partner_id' => Yii::$app->partner->id
			] + Yii::$app->request->get()),
			'currencies' => Yii::$app->get('currencies')->getlist(),
			'statuses' => PartnersWithdrawSearch::statuses(),
		]);
	}
}
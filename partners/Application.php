<?php

namespace partners;

use common\modules\partners\models\PartnersModel;
use yii\web\Application as BaseApplication;

class Application extends BaseApplication {

	protected $_partner = null;

	public function init () {
		parent::init();
		if(\Yii::$app->user->isGuest) {
			$this->layout = '@partners/views/layouts/guest';
		}
	}

	public function getPartner() {
		if(\Yii::$app->user->isGuest) {
			return null;
		}
		if(is_null($this->_partner)) {
			$this->_partner = PartnersModel::findOne([
				'owner_id' => \Yii::$app->user->id
			]);

		}
		return $this->_partner;
	}

}

const path = require('path');
const frontDev = require('./webpack.dev.js');
const frontProd = require('./webpack.prod.js');
module.exports = [
    frontDev, frontProd,
];
<?php

namespace partners\search;

use common\traits\DateToTimeTrait;
use Yii;
use common\modules\partners\models\PartnersLeadsModel;
use yii\data\ActiveDataProvider;
use yii\db\Expression;

class PromoStatisticsSearch extends  PartnersLeadsModel {

	use DateToTimeTrait;

	public $registrations;
	public $deposit_count;
	public $created_from;
	public $created_to;

	public $amount_currency = 'USD';

	public function scenarios () {
		return [
			self::SCENARIO_DEFAULT => ['amount_currency', 'created_from', 'created_to', 'partner_id'],
		];
	}

	public function rules () {
		return [
			'safe' => [['amount_currency', 'created_from', 'created_to'], 'safe'],
		];
	}

	public function load ($data, $formName = null) {
		if(parent::load($data, $formName)) {
			$this->created_from = $this->created_from ?? date('Y-m-d', mktime(0,0,0, date('m'), date('d') - 7));
			$this->created_to = $this->created_to ?? date('Y-m-d', time());
			return true;
		}
		return false;
	}

	public function search($params = []) {
		$query = static::find()->select([
			'material_id', 'sid',
			'registrations' => new Expression("COUNT(*)"),
			'deposit_count' => new Expression("SUM( CASE WHEN `amount_deposit` > 0 THEN 1 ELSE 0 END )"),
			'amount_deposit' => new Expression("SUM(`amount_deposit`)"),
			'amount_profit' => new Expression("SUM(`amount_profit`)"),
		])->groupBy(['material_id', 'sid', 'amount_currency']);
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
				'pageSizeLimit' => false,
			],
		]);
		if ($this->load($params) && $this->validate()) {
			$query->andFilterWhere([
				PartnersLeadsModel::tableName() . '.partner_id' => $this->partner_id,
				PartnersLeadsModel::tableName() . '.amount_currency' => $this->amount_currency,
			]);
		} else if(array_key_exists('partner_id', $params)) {
			$query->andWhere([
				PartnersLeadsModel::tableName() . '.partner_id' => +$params['partner_id'],
				PartnersLeadsModel::tableName() . '.amount_currency' => 'USD',
			]);
		}
		$query->andFilterWhere(['>', PartnersLeadsModel::tableName() . '.created_at', self::getTimeFromDate($this->created_from)]);
		$query->andFilterWhere(['<', PartnersLeadsModel::tableName() . '.created_at', self::getTimeFromDate($this->created_to) + 86399]);
		return $dataProvider;
	}

	/** {@inheritdoc} */
	public function formName () {
		return '';
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'sid' => Yii::t('partners', 'SubID'),
			'registrations' => Yii::t('partners', 'Registrations'),
			'deposit_count' => Yii::t('partners', 'Deposits count'),
			'created_from' => Yii::t('partners', 'Created at'),
		] + parent::attributeLabels();
	}
}
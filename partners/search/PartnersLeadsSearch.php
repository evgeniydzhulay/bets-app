<?php

namespace partners\search;

use common\modules\partners\models\PartnersLeadsModel;
use common\modules\partners\models\PartnersLinksModel;
use common\modules\user\models\ProfileModel;
use common\modules\user\models\UserModel;
use common\traits\DateToTimeTrait;
use Yii;
use yii\data\ActiveDataProvider;

class PartnersLeadsSearch extends PartnersLeadsModel {

	use DateToTimeTrait;

	public $name;
	public $link;
	public $created_from;
	public $created_to;
	public $site_id;
	public $page_sid;
	public $registered_at;
	public $country_id;
	public $not_deposited = 0;

	/**
	 * {@inheritdoc}
	 */
	public function load($data, $formName = null) {
		if(parent::load($data, $formName)) {
			$this->created_from = $this->created_from ?? date('Y-m-d', mktime(0,0,0, date('m'), date('d') - 7));
			$this->created_to = $this->created_to ?? date('Y-m-d', time());
			return true;
		}
		return false;
	}

	public function scenarios () {
		return [
			self::SCENARIO_DEFAULT => ['amount_currency', 'country_id', 'created_from', 'created_to', 'campaign_id', 'not_deposited', 'partner_id'],
		];
	}

	public function rules () {
		return [
			'safe' => [['amount_currency', 'country_id', 'created_from', 'created_to', 'campaign_id', 'not_deposited'], 'safe'],
		];
	}

	public function search ($params = []) {
		$query = self::find()->joinWith([
			'link', 'partner', 'user', 'user.profile',
		], false)->select([
			PartnersLeadsModel::tableName() . '.*',
			PartnersLinksModel::tableName() . '.link',
			PartnersLinksModel::tableName() . '.site_id',
			PartnersLinksModel::tableName() . '.page_sid',
			'registered_at' => UserModel::tableName() . '.created_at',
			'country_id' => ProfileModel::tableName() . '.country'
		]);
		$dataProvider = new ActiveDataProvider([
			'query' => $query,
			'pagination' => [
				'pageSize' => 20,
				'pageSizeLimit' => false,
			],
			'sort' => [
				'attributes' => [
					'id' => [
						'asc' => [self::tableName() . '.id' => SORT_ASC],
						'desc' => [self::tableName() . '.id' => SORT_DESC],
					],
					'created_at' => [
						'asc' => [self::tableName() . '.created_at' => SORT_ASC],
						'desc' => [self::tableName() . '.created_at' => SORT_DESC],
					],
				],
				'defaultOrder' => [
					'id' => SORT_DESC,
				],
			],
		]);
		if ($this->load($params) && $this->validate()) {
			$query->andFilterWhere([
				PartnersLeadsModel::tableName() . '.amount_currency' => $this->amount_currency,
				PartnersLeadsModel::tableName() . '.campaign_id' => $this->campaign_id,
				PartnersLeadsModel::tableName() . '.material_id' => $this->material_id,
				PartnersLeadsModel::tableName() . '.partner_id' => $this->partner_id,
				ProfileModel::tableName() . '.country' => $this->country_id,
			]);
			if($this->not_deposited) {
				$query->andWhere([PartnersLeadsModel::tableName() . '.amount_deposit' => null]);
			}
			$query->andFilterWhere(['like', PartnersLinksModel::tableName() . '.link', $this->link]);
			$query->andFilterWhere(['>', PartnersLeadsModel::tableName() . '.created_at', self::getTimeFromDate($this->created_from)]);
			$query->andFilterWhere(['<', PartnersLeadsModel::tableName() . '.created_at', self::getTimeFromDate($this->created_to) + 86399]);
		} else if(array_key_exists('partner_id', $params)) {
			$query->andWhere([
				PartnersLeadsModel::tableName() . '.partner_id' => +$params['partner_id']
			]);
		}
		return $dataProvider;
	}

	/** {@inheritdoc} */
	public function formName () {
		return '';
	}

	/** {@inheritdoc} */
	public function attributeLabels () {
		return [
			'user_id' => Yii::t('user', 'User ID'),
			'country_id' => Yii::t('user', 'Country'),
			'site_id' => Yii::t('partners', 'Website ID'),
			'page_sid' => Yii::t('partners', 'SubID'),
			'link' => Yii::t('partners', 'Link'),
			'name' => Yii::t('user', 'Name'),
			'created_from' => Yii::t('partners', 'Created at'),
			'not_deposited' => Yii::t('partners', 'Non-deposited players only'),
		] + parent::attributeLabels();
	}
}
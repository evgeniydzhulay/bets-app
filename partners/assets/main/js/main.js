function copyToClipboard(content, message) {
    var $temp = jQuery("<input>");
    jQuery("body").append($temp);
    $temp.val(content).select();
    document.execCommand("copy");
    $temp.remove();
    if(message.length > 0) {
        alert(message);
    }
}
(function () {
    jQuery(document).on('click', '.content-copy', function(event) {
        copyToClipboard($(this).attr('data-copy'), $(this).attr('data-result') || '');
    });
})();
<?php

namespace partners\forms;

use common\modules\partners\models\PartnersComissionModel;
use common\modules\partners\models\PartnersModel;
use common\modules\partners\models\PartnersSiteModel;
use common\modules\partners\models\PartnersWithdrawModel;
use Yii;

class RegistrationForm extends \common\modules\user\forms\RegistrationForm {

	public $website_url;
	public $website_category;
	public $website_language;

	public $payment_method;
	public $payment_number;

	/** @inheritdoc */
	public function rules() {
		$rules = parent::rules() + [
			'websiteString' => [['website_url', 'website_language'], 'string', 'max' => 250],
			'partnerIntegers' => [[
				'website_category', 'payment_method'
			], 'integer'],
			'partnerPaymentString' => ['payment_number', 'string', 'max' => 250],
			'partnerRequired' => [[
				'email', 'name', 'surname', 'country', 'website_url', 'website_language', 'website_category', 'payment_method', 'payment_number'
			], 'required'],
			'paymentMethodExists' => ['payment_method',  'in', 'range' => \array_keys(PartnersWithdrawModel::paymentMethods())],
		];
		unset($rules['currencyRequired']);
		return $rules;
	}

	/** @inheritdoc */
	public function attributeLabels() {
		return parent::attributeLabels() + [
			'website_url' => Yii::t('partners', 'Website url'),
			'website_language' => Yii::t('partners', 'Website language'),
			'website_category' => Yii::t('partners', 'Website category'),
			'payment_method' => Yii::t('partners', 'Payment method'),
			'payment_number' => Yii::t('partners', 'Payment number'),
		];
	}

	/** @inheritdoc */
	public function register () {
		if(!$this->validate()) {
			return false;
		}
		$tr = Yii::$app->db->beginTransaction();
		$partner = Yii::createObject(PartnersModel::class);
		$partner->scenario = PartnersModel::SCENARIO_CREATE;
		$partner->setAttributes([
			'payment_method' => $this->payment_method,
			'payment_number' => $this->payment_number,
		]);
		if(!$partner->validate(['payment_method', 'payment_number'])) {
			$this->addErrors($partner->errors);
			return false;
		}
		$website = Yii::createObject(PartnersSiteModel::class);
		$website->scenario = PartnersSiteModel::SCENARIO_CREATE;
		$website->setAttributes([
			'category_id' => $this->website_category,
			'lang_code' => $this->website_language,
			'address' => $this->website_url,
		]);
		if(!$website->validate(['category_id', 'lang_code', 'address'])) {
			$this->addErrors(['website_url' => $website->getErrors('address')]);
			$this->addErrors(['category_id' => $website->getErrors('website_category')]);
			$this->addErrors(['lang_code' => $website->getErrors('website_language')]);
			return false;
		}

		if(parent::register()) {
			$partner->setAttributes(['owner_id' => $this->getUser()->id]);
			if($partner->save(false)) {
				$website->setAttributes(['partner_id' => $partner->id]);
				$comission = Yii::createObject(PartnersComissionModel::class);
				$comission->scenario = PartnersComissionModel::SCENARIO_CREATE;
				$comission->setAttributes([
					'partner_id' => $partner->id,
					'currency_code' => 'RUR',
					'deposit_profit' => 0,
					'deposit_min' => 0,
					'profit_percent' => Yii::$app->params['partners.comission.bet'],
				]);
				if($website->save(false) && $comission->save(false)) {
					$tr->commit();
					return true;
				}
				!empty($website->errors) && Yii::error($website->errors);
				!empty($comission->errors) && Yii::error($comission->errors);
			}
			!empty($partner->errors) && Yii::error($partner->errors);
			Yii::$app->user->logout();
		}
		$tr->rollBack();
		Yii::error($this->errors);
		return false;
	}

}
<?php

use common\assets\FontAmesomeAsset;
use common\behaviors\LanguageRequestBehavior;
use dmstr\web\AdminLteAsset;
use partners\controllers\UserController;
use rmrevin\yii\fontawesome\AssetBundle;
use yii\bootstrap\BootstrapAsset;
use yii\bootstrap\BootstrapPluginAsset;
use yii\helpers\ArrayHelper;
use yii\web\JqueryAsset;
use yii\web\UrlManager;
use yii\web\UrlNormalizer;

return ArrayHelper::merge([
	'id' => 'partners',
	'as LanguageRequestBehavior' => LanguageRequestBehavior::class,
	'controllerNamespace' => 'partners\controllers',
	'defaultRoute' => 'report/index',
	'basePath' => dirname(__DIR__),
	'modules' => [
		'user' => [
			'controllerMap' => [
				'auth' => UserController::class
			]
		]
	],
	'components' => [
		'view' => [
			'theme' => [
				'pathMap' => [
					'@common/modules/user/views' => '@partners/views/user',
				]
			]
		],
		'errorHandler' => [
			'errorAction' => 'core/error',
		],
		'assetManager' => [
			'appendTimestamp' => true,
			'bundles' => [
				JqueryAsset::class => [
					'js' => ['jquery.min.js',],
				],
				BootstrapAsset::class => [
					'sourcePath' => null,
					'basePath' => '@webroot',
					'baseUrl' => '@web',
					'css' => ['static/css/bootstrap.min.css'],
				],
				BootstrapPluginAsset::class => [
					'js' => ['js/bootstrap.min.js'],
				],
				AdminLteAsset::class => [
					'css' => [],
					'skin' => false,
				],
				AssetBundle::class => [
					'class' => FontAmesomeAsset::class,
				],
			],
		],
		'urlManager' => [
			'class' => UrlManager::class,
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'normalizer' => [
				'class' => UrlNormalizer::class,
				'collapseSlashes' => true,
				'normalizeTrailingSlash' => true,
			],
			'rules' => [
				'' => 'report/index',
				'contacts' => 'core/contacts',
				'user/<action>' => 'user/auth/<action>',
				'<controller:(report|links|sites|payments)>' => '<controller>/index',
			],
		],
	],
],
	require_once(BASE_PATH . '/common/modules/user/config/partners.php'),
	(is_file(__DIR__ . '/main-local.php') ? require_once(__DIR__ . '/main-local.php') : [])
);
#### Модели

Базовый поиск располагается в основной модели  и называется search  
Базовая структура - search($params) {} где params это параметры поиска  
  
Общий метод поиска располагается в в папке %module%/search/%model%Search  
например common/modules/games/search/GameSportsSearch  

Специфичные для окружения ( backend / frontend / etc. ) методы располагаются в аналогичных папках окружения  
например frontend/modules/games/search/GameSportsSearch

У всех моделей должны быть 3 минимум сценария:  
`const SCENARIO_CREATE = 'create';`  
`const SCENARIO_UPDATE = 'update';`  
`const SCENARIO_SEARCH = 'search';`  
и должны быть методы и валидаторы доступные для сценариев  
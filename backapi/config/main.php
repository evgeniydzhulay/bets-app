<?php

use yii\helpers\ArrayHelper;
use yii\web\Request;
use yii\web\Response;
use yii\web\UrlManager;
use yii\web\UrlNormalizer;

return ArrayHelper::merge([
	'controllerNamespace' => 'backapi\controllers',
	'basePath' => dirname(__DIR__),
	'language' => 'en',
	'components' => [
		'log' => [
			'targets' => [
				'errors' => [
					'logVars' => [],
				],
			],
		],
		'urlManager' => [
			'class' => UrlManager::class,
			'enablePrettyUrl' => true,
			'showScriptName' => false,
			'normalizer' => [
				'class' => UrlNormalizer::class,
				'collapseSlashes' => true,
				'normalizeTrailingSlash' => true,
			],
		],
		'request' => [
			'class' => Request::class,
			'enableCookieValidation' => false,
			'enableCsrfValidation' => false,
		],
		'response' => [
			'class' => Response::class,
			'format' => Response::FORMAT_JSON,
			'on beforeSend' => function ($event) {
				$response = $event->sender;
				if ($response->format == Response::FORMAT_JSON && $response->data !== null && is_array($response->data)) {
					$response->data['success'] = $response->data['success'] ?? $response->isSuccessful;
					if (!$response->isSuccessful) {
						$response->data['status'] = $response->statusCode;
					}
				}
			},
		],
	],
],
	require_once(BASE_PATH . '/backapi/modules/user/config/backapi.php'),
	require_once(BASE_PATH . '/backapi/modules/support/config/backapi.php'),
	require_once(BASE_PATH . '/backapi/modules/games/config/backapi.php'),
	(is_file(__DIR__ . '/main-local.php') ? require_once(__DIR__ . '/main-local.php') : [])
);

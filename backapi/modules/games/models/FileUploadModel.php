<?php

namespace backapi\modules\games\models;

use common\modules\Storage;
use creocoder\flysystem\Filesystem;
use Yii;
use yii\base\Model;
use yii\helpers\Inflector;
use yii\web\UploadedFile;

/**
 *
 * @property string $fileName
 * @property \common\modules\Storage $module
 * @property \creocoder\flysystem\Filesystem $storage
 * @property array $response
 */
class FileUploadModel extends Model {

	/**
	 * @var UploadedFile
	 */
	public $file;
	public $fileExtension;
	private $_newFileName;

	public function getModule() : Storage {
		return Yii::$app->getModule('gamesStorage');
	}

	public function getStorage() : Filesystem {
		return $this->getModule()->get('storage');
	}

	public function getFileName() {
		if (!$this->_newFileName) {
			$newFileName = substr(uniqid(md5(rand()), true), 0, 10);
			$newFileName .= md5(Inflector::slug($this->file));
			$newFileName .= '.' . $this->fileExtension;
			$this->_newFileName = "{$newFileName[0]}/{$newFileName[1]}/{$newFileName}";
		}
		return $this->_newFileName;
	}

	public function upload() {
		if ($this->validate()) {
			return $this->getStorage()->writeStream($this->getFileName(), fopen($this->file, 'r+'));
		}
		return false;
	}

	public function getResponse() {
		return [
			'fileName' => $this->getFileName(),
		];
	}

}

<?php

namespace backapi\modules\games\services;

use backapi\modules\games\models\FileUploadModel;
use backapi\modules\games\services\sport_result\Basketball1xbetResultService;
use backapi\modules\games\services\sport_result\Soccer1xbetResultService;
use common\modules\games\models\Api1xbetTemplateMarketsModel;
use common\modules\games\models\Api1xbetTemplateOutcomesModel;
use common\modules\games\models\ApiEventGameModel;
use common\modules\games\models\ApiGameMarketsModel;
use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\EventParticipantsModel;
use common\modules\games\models\EventTournamentLocaleModel;
use common\modules\games\models\EventTournamentModel;
use common\modules\games\models\GameParticipantsLocaleModel;
use common\modules\games\models\GameParticipantsModel;
use common\modules\games\queries\GameParticipantsLocaleQuery;
use common\modules\games\queries\GameParticipantsQuery;
use backapi\modules\games\services\BetsapiService;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\helpers\Json;
use yii\log\FileTarget;
use yii\redis\Connection;

/*
 Processing Requests for betsapi.com (1xbet)
*/

class Betsapi1xbetService extends BaseApiService {

	const SOURCE_BETSAPI = 2;
	const LANG_CODE_EN = 'en';
	const LANG_CODE_RU = 'ru';

	const TIME_STATUS_NOT_STARTED = 0;
	const TIME_STATUS_INPLAY = 1;
	const TIME_STATUS_TO_BE_FIXED = 2;
	const TIME_STATUS_ENDED = 3;
	const TIME_STATUS_POSTPONED = 4;
	const TIME_STATUS_CANCELLED = 5;
	const TIME_STATUS_WALKOVER = 6;
	const TIME_STATUS_INTERRUPTED = 7;
	const TIME_STATUS_ABANDONED = 8;
	const TIME_STATUS_RETIRED = 9;
	const TIME_STATUS_REMOVED = 99;

	public $sports_id = null;
	public $sports_name = null;
	public $tournament_id = null;
	public $game_id = null;
	public $group_id = null;
	public $market_id = null;
	public $event_FI = null;
	public $timeGetData = null;
	public $participantsValues = null;
	public $participantsValuesFormat = null;
	public $participantsNames = null;
	public $alternativeNameParticipants = [
		'{item1}' =>['Home',1],
		'{item2}' => ['Away',2],
		'Draw' => ['X']
	];
	public $outcomesNameFormat = [
		'Home' => '{item1}',
		'Away' => '{item2}',
		/*' O ' => ' Over ',
		' U ' => ' Under ',
		' E ' => ' Exactly ',*/
	];

	protected function setParticipantsValuesDefault() {
		$this->participantsValues = ['Home','Away','Home','Away'];
		$this->participantsValuesFormat = ['{item1}','{item2}','{item1}','{item2}'];
	}

	public static function sports() {
		return [
			1 => 'Soccer', // Football
			3 => 'Basketball',
			/*4 => 'Tennis',
			2 => 'Ice Hockey',
			6 => 'Volleyball',
			10 => 'Table Tennis',
			67 => 'Floorball',
			87 => 'Special bets',
			8 => 'Handball',
			5 => 'Baseball',
			30 => 'Snooker',
			44 => 'Horse Racing',
			17 => 'Water Polo',
			7 => 'Rugby Union',
			9 => 'Boxing/UFC',
			11 => 'Chess',
			12 => 'Billiards',
			13 => 'American Football',
			14 => 'Futsal',
			16 => 'Badminton',
			18 => 'Motorsport',
			20 => 'TV-Games',
			21 => 'Darts',
			26 => 'Formula 1',
			31 => 'Motorbikes',
			36 => 'Bicycle Racing',
			41 => 'Golf',
			46 => 'Curling',
			49 => 'Netball',
			56 => 'Martial Arts',
			57 => 'Athletics',
			66 => 'Cricket',
			68 => 'Greyhound Racing',
			69 => 'Toto',
			70 => 'Inline Hockey',
			80 => 'Gaelic Sports',
			82 => 'Lottery',
			92 => 'Trotting',
			102 => 'Speedway',
			105 => 'Streetball',
			118 => 'Sepak takraw',
			126 => 'Hurling',
			132 => 'Horse Racing AntePost',
			133 => 'Trotting AntePost',
			138 => 'Surfing',
			151 => 'Greyhound AntePost',
			157 => 'Board games',
			176 => 'Weather',
			180 => 'Kabaddi',
			182 => 'Muay Thai',
			189 => 'Boxing/UFC',
			202 => 'Politics',
			220 => 'Boat Race'*/

		];
	}

	public function getSportsResultService($sportsName) {
		$sportsResultServices = [
			'Soccer' => Soccer1xbetResultService::class,
			'Basketball' => Basketball1xbetResultService::class,
		];
		return $sportsResultServices[$sportsName];
	}

	/*
	* Set markets 1xbet
	*/
	public function setMarkets() {
		if(!is_file(__DIR__ . '/1xbetMarkets.php')){
			return false;
		}
		require_once(__DIR__ . '/1xbetMarkets.php');
		$transaction = Yii::$app->db->beginTransaction();
		try {
			foreach($markets1xbet as $markerId => $marker){
				if(($model = Api1xbetTemplateMarketsModel::find()->andWhere([
					'market_id' => $markerId,
				])->one()) === null){
					$api1xbetTemplateMarketsModel = new Api1xbetTemplateMarketsModel();
					$api1xbetTemplateMarketsModel->setAttributes([
						'market_id' => (int) $markerId,
						'name' => (string) $marker['N'],
						'count_fields' => (int) $marker['C'],
					]);
					$api1xbetTemplateMarketsModel->save();
				}
			}
			$transaction->commit();
		} catch (\Throwable $e) {
			Yii::error($e);
			$transaction->rollBack();
		}
		return true;
	}
	/*
	* Set outcomes 1xbet
	*/
	public function setOutcomes() {
		if(!is_file(__DIR__ . '/1xbetOutcome.php')){
			return false;
		}
		require_once(__DIR__ . '/1xbetOutcome.php');
		$transaction = Yii::$app->db->beginTransaction();
		try {
			foreach($outcomes1xbet as $outcomeId => $outcome){
				if(($model = Api1xbetTemplateOutcomesModel::find()->andWhere([
					'outcome_id' => $outcomeId,
				])->one()) === null){
					$name = $outcome['N'];
					$marketId = $outcome['IdG'];
					unset($outcome['N']);
					unset($outcome['IdG']);
					$api1xbetTemplateOutcomesModel = new Api1xbetTemplateOutcomesModel();
					$api1xbetTemplateOutcomesModel->setAttributes([
						'outcome_id' => (int) $outcomeId,
						'market_id' => (int) $marketId,
						'name' => (string) $name,
						'params' => Json::encode($outcome),
					]);
					$api1xbetTemplateOutcomesModel->save();
				}
			}
			$transaction->commit();
		} catch (\Throwable $e) {
			Yii::error($e);
			$transaction->rollBack();
		}
		return true;
	}

	protected function getSport($code, $name){

		if(($sports_id = $this->findSport(self::SOURCE_BETSAPI, $code)) !== null){
			return $sports_id;
		} else {
			if(($sports_id = $this->findSportByName([
					'name' => $name, 
					'lang' => Yii::$app->language,
				])) !== null){
					$this->createApiSport([
						'sports_id' => $sports_id,
						'service_api' => self::SOURCE_BETSAPI,
						'code_api' => $code,
					]);
					return $sports_id;
			} else {
				/*$sports_id = $this->createSport([
					'title' => $name,
					'lang' => Yii::$app->language,
					'service_api' => self::SOURCE_BETSAPI,
					'code_api' => $code,
				]);
				return $sports_id;*/
			}
		}
		return null;
	}

	protected function getMarket($params) {
		if(($marketApi = $this->findMarket(self::SOURCE_BETSAPI, $params['code'], $params['sports_id'])) !== null){
			$market_id = $marketApi->market_id;
			$group_id = $marketApi->group_id;
			return ['market_id' => $market_id, 'group_id' => $group_id];
		} else {
			if(($templateMarkets1xbet = Api1xbetTemplateMarketsModel::find()->andWhere([
				'market_id' => $params['code'],
			])->one()) === null){
				return null;
			}
			if(( $marketLocale = $this->findMarketByName([
					'name' => $templateMarkets1xbet['name'],
					'sports_id' => $params['sports_id'],
					'lang' => Yii::$app->language,
				])) !== null){
				if(( $marketApi = $this->findMarketApiByMarketId([self::SOURCE_BETSAPI, $marketLocale->market_id])) !== null){
					$market_id = $marketApi->market_id;
					$group_id = $marketApi->group_id;
					$this->createApiMarket([
						'market_id' => $market_id,
						'group_id' => $group_id,
						'sports_id' => $params['sports_id'],
						'service_api' => self::SOURCE_BETSAPI,
						'code_api' => $params['code'],
					]);
					return ['market_id' => $market_id, 'group_id' => $group_id];
				}
			}

			if($params['group_id'] !== null){
				$market_id = $this->createMarket([
					'title' => $templateMarkets1xbet['name'],
					'lang' => Yii::$app->language,
					'service_api' => self::SOURCE_BETSAPI,
					'code_api' => $params['code'],
					'sports_id' => $params['sports_id'],
					'group_id' =>  $params['group_id'],
				]);
				return ['market_id' => $market_id, 'group_id' => $params['group_id']];
			}
		}
		return null;
	}

	protected function createUpdateOutcome($params) {
		$outcomeCondition = null;
		if(($outcome_id = $this->findOutcome(self::SOURCE_BETSAPI, $params['code'])) === null){
			if(($templateOutcomes1xbet = Api1xbetTemplateOutcomesModel::find()->andWhere([
				'outcome_id' => (int)$params['outcomeData']['T']
			])->one()) === null){
				return null;
			}
			$outcomeCondition['name'] = $templateOutcomes1xbet['name'];
			if( strpos($outcomeCondition['name'],'^1^') !== false ){
				$outcomeCondition['header'] = 1;
			} elseif(strpos($outcomeCondition['name'],'^2^') !== false){
				$outcomeCondition['header'] = 2;
			}
			if( isset($params['outcomeData']['P']) ){
				$dP = fmod($params['outcomeData']['P'],1);
				if($dP == 0 or $dP == 0.5){
					$outcomeCondition['handicap'] = (float)$params['outcomeData']['P'];
				} else {
					return null;
				}
			}

			if(substr_count($templateOutcomes1xbet['name'], '()') > 1){
				return null;
			}
			//Any Other Score
			if( strpos($outcomeCondition['name'],'Any Other Score') !== false ){
				return null;
			}

			$name = str_replace(['^1^','^1 ^','^2^','^2 ^'],['{item1}','{item1}','{item2}','{item2}'],$templateOutcomes1xbet['name']);
			if( strpos($outcomeCondition['name'],'()') !== false ){
				if(!isset($params['outcomeData']['P'])){
					$params['outcomeData']['P'] = 0;
					$outcomeCondition['handicap'] = 0;
				}
				$name = str_replace(['()'],['('.(float)$params['outcomeData']['P'].')'],$name);
			}
			if(($outcome_id = $this->findOutcomeByName([
					'names' => [$name],
					'market_id' => $params['market_id'],
					'lang' => Yii::$app->language,
				])) !== null){
					$this->createApiOutcome([
						'outcome_id' => $outcome_id,
						'service_api' => self::SOURCE_BETSAPI,
						'code_api' => $params['code'],
					]);
			}
		}

		if($outcome_id === null ){
			if($outcomeCondition == null ){
				return null;
			}
			$outcome_id = $this->createOutcome([
				'title' => $name,
				'lang' => Yii::$app->language,
				'service_api' => self::SOURCE_BETSAPI,
				'code_api' => $params['code'],
				'sports_id' => $params['sports_id'],
				'tournament_id' => $params['tournament_id'],
				'game_id' => $params['game_id'],
				'group_id' => $params['group_id'],
				'market_id' => $params['market_id'],
				'rate' => $params['rate'],
				'is_enabled' => $params['is_enabled'],
				'condition' => Json::encode($outcomeCondition),
				]);
			return $outcome_id;
		} else {
			if(($eventOutcome = $this->findEventOutcome($params['game_id'], $outcome_id)) === null){
				if((float)$params['rate'] <= 1){
					return null;
				}
				$eventOutcome = $this->createEventOutcomes([
					'sports_id' => $params['sports_id'],
					'tournament_id' => $params['tournament_id'],
					'game_id' => $params['game_id'],
					'group_id' => $params['group_id'],
					'market_id' => $params['market_id'],
					'outcome_id' => $outcome_id,
					'is_enabled' => $params['is_enabled'],
					'rate' => $params['rate'],
				]);

			} else {
				if( abs($params['rate'] - $eventOutcome->rate) < 0.001 && (int)$params['is_enabled'] == (int)$eventOutcome->is_enabled)
						return $outcome_id;

				if((float)$params['rate'] > 1){
					$eventOutcome->setRate($params['rate']);
				}
				if($eventOutcome->is_enabled != $params['is_enabled']){
					$eventOutcome->is_enabled = $params['is_enabled'];
					$eventOutcome->save(false);
				}
			}
			return $outcome_id;
		}
		return null;
	}

	protected function uploadParticipantImage($image) {
		$imageHeaders = get_headers('https://v2l.ccdnss.com/genfiles/logo_teams/'.$image,1);
		if($imageHeaders['Content-Type'] == 'image/png' && $imageHeaders['Content-Length'] > 200 && $imageHeaders['Content-Length'] < 100000 ){
			$file = BASE_PATH . '/backapi/uploads/'.$image.'.png';
			$content = file_get_contents('https://v2l.ccdnss.com/genfiles/logo_teams/'.$image);
			file_put_contents($file, $content);
			$model = Yii::createObject([
				'class' => FileUploadModel::class,
				'file' => $file,
				'fileExtension' => 'png',
			]);
			if ($model->upload()) {
				unlink($file);
				$result = $model->getResponse();
				return $result['fileName'];
			}
			return null;
		}
		return null;
	}

	public function findGameLocale($game_id, $lang_code) {
		if(($model = EventGameLocaleModel::find()->andWhere([
				'game_id'=>$game_id,
				'lang_code'=> $lang_code
			])->one()) !== null){
			return $model->tournament_id;
		}else{
			return null;
		}
	}

	public function prematch ($jsonData) {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-1xbet-service-prematch.log');
		}
		//$jsonData = file_get_contents('https://api.betsapi.com/v1/1xbet/upcoming?sport_id=1&token=23807-8GffU48caA3vLs');
		$data =  Json::decode($jsonData);

		foreach($data['results'] as $event){
			/*if( (int)$event['our_event_id'] == 0){
				continue;
			}*/
			$sports_id = $this->getSport($event['sport_id'], self::sports()[$event['sport_id']] );
			$this->processingPrematch([
				'sports_id' => $sports_id,
				'event' => $event,
				'service_api' => self::SOURCE_BETSAPI,
				'is_live' => false
			]);

			/*if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $event['id'])) === null){
					if(($gameApi = $this->findGameByApiEventId($event['our_event_id'])) !== null){
						$this->createApiGame([
							'game_id' => $gameApi->game_id,
							'sports_id' => $gameApi->sports_id,
							'tournament_id' => $gameApi->tournament_id,
							'service_api' => self::SOURCE_BETSAPI,
							'code_api' => $event['id'],
							'api_event_id' => $event['our_event_id'],
						]);
					}
				}*/
		}
		return true;
	}

	public function event($jsonData,$eventsCodeApi = [] ) {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-1xbet-service-event.log');
		}
		//$jsonData = file_get_contents("https://api.betsapi.com/v1/1xbet/event?token=23807-64J9ST5D911ANZ&event_id=213871262");
		$data =  Json::decode($jsonData);

		foreach($data['results'] as $event){
			if(!empty($eventsCodeApi[(int)$event['id']])){
				unset($eventsCodeApi[(int)$event['id']]);
			}
			$event_id = (int)$event['id'];
			if($redis = Yii::$app->get('redis', false)) {
				$key = '1xbet-event-live-time-update-'.$event_id;
				if(($updated_at = $redis->get($key)) != null && (int)$updated_at == (int)$event['updated_at']){
					//Yii::warning('Event id = '.$event['id'].'.updated_at:'.$event['updated_at']);
					return false;
				}
				$redis->set($key,(int)$event['updated_at'],'EX',3600*24);
			}

			$eventHash = hash('adler32', Json::encode($event['Value']['GE']));
			$transaction = Yii::$app->db->beginTransaction();
			try {
				if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $event['id'])) === null){
					$transaction->commit();
					Yii::warning('Event id = '.$event['id'].'.Event not found');
					continue;
				}
				if($gameApi->hash == $eventHash){
					$transaction->commit();
					continue;
				} else {
					$gameApi->hash = $eventHash;
					Yii::warning('Event id = '.$event['id'].'.Event update'.$eventHash);
					$gameApi->save(false);
				}

				if(!empty($event['Value']['LR'])){
					if(($tournamentLocale = EventTournamentLocaleModel::find()->andWhere([
							'lang_code' => self::LANG_CODE_RU,
							'tournament_id' => $gameApi->tournament_id,
						])->all()) === null){
							$localeTournament = new EventTournamentLocaleModel(['scenario' => EventTournamentLocaleModel::SCENARIO_CREATE]);
							$localeTournament->setAttributes([
								'sports_id' => $gameApi->sports_id, 
								'tournament_id' => $gameApi->tournament_id, 
								'lang_code' => self::LANG_CODE_RU,
								'title' => $event['Value']['LR'],
								'is_active' => 1,
							]);
							$localeTournament->save();
					}
				}

				/*
				$this->participantsNames = null;
				$this->setParticipantsValuesDefault();

				if(count($participants) == 2){
					$this->participantsNames = $this->getParticipantsNames($participants[0]['participantLocale'][0]['name'],$participants[1]['participantLocale'][0]['name']);
					$participantsReplacement = array_combine($this->participantsNames, $this->participantsValues);
					$participantsReplacementFormat = array_combine($this->participantsNames, $this->participantsValuesFormat);
				}*/

				if($gameApi->is_main_provider == true){
					if($gameApi->is_checked == false ){
						//If the game is new and the event is being processed for the first time
						$newEventName = null;
						$participants = EventParticipantsModel::find()->andWhere(['game_id' => $gameApi->game_id])->with([
							'participantLocale' => function (GameParticipantsLocaleQuery $query) {
								return $query->all();
							},
							'participant' => function (GameParticipantsQuery $query) {
								return $query->all();
							},
						])->orderBy([
							'sort_order' => SORT_ASC,
							'id' => SORT_ASC,
						])->asArray()->all();

						//print_r($participants);

						if(count($participants) == 2){
							foreach($participants as $k => $participant){
								$participants[$k]['participantLocale'] = ArrayHelper::map($participant['participantLocale'], 'lang_code', 'name');
							}
							if(!empty($event['Value']['O1R']) && empty($participants[0]['participantLocale'][self::LANG_CODE_RU]) ){
								$localeParticipant = new GameParticipantsLocaleModel(['scenario' => GameParticipantsLocaleModel::SCENARIO_CREATE]);
								$localeParticipant->setAttributes([
									'sports_id' => $participants[0]['sports_id'],
									'participant_id' => $participants[0]['participant_id'],
									'lang_code' => self::LANG_CODE_RU,
									'name' => $event['Value']['O1R'],
									'is_active' => 1,
								]);
								$localeParticipant->save();
								$newEventName = $localeParticipant->name;
							} else {
								$newEventName = $participants[0]['participantLocale'][self::LANG_CODE_RU];
							}
							if(!empty($event['Value']['O2R']) && empty($participants[1]['participantLocale'][self::LANG_CODE_RU]) ){
								$localeParticipant = new GameParticipantsLocaleModel(['scenario' => GameParticipantsLocaleModel::SCENARIO_CREATE]);
								$localeParticipant->setAttributes([
									'sports_id' => $participants[1]['sports_id'],
									'participant_id' => $participants[1]['participant_id'],
									'lang_code' => self::LANG_CODE_RU,
									'name' => $event['Value']['O2R'],
									'is_active' => 1,
								]);
								$localeParticipant->save();
								$newEventName .= ' - '.$localeParticipant->name;
							} else {
								$newEventName .= ' - '.$participants[1]['participantLocale'][self::LANG_CODE_RU];
							}
							if(!empty($event['Value']['O1IMG']) && empty($participants[0]['participant']['image'][0]) ){
								$participant = GameParticipantsModel::findOne($participants[0]['participant']['id']);
								$this->setApiParticipantImage([
									'sports_id' => $participant->sports_id,
									'participant_id' => $participant->id,
									'service_api' => self::SOURCE_BETSAPI,
									'code_image' => (string)$event['Value']['O1IMG'][0],
								]);
							}
							if(!empty($event['Value']['O2IMG']) && empty($participants[1]['participant']['image'][0]) ){
								$participant = GameParticipantsModel::findOne($participants[1]['participant']['id']);
								$this->setApiParticipantImage([
									'sports_id' => $participant->sports_id,
									'participant_id' => $participant->id,
									'service_api' => self::SOURCE_BETSAPI,
									'code_image' => (string)$event['Value']['O2IMG'][0],
								]);
							}
						}
						if(($localeEventGame = EventGameLocaleModel::find()->andWhere([
								'lang_code' => self::LANG_CODE_RU,
								'game_id' => $gameApi->game_id,
							])->one()) !== null){
							if($localeEventGame->title != $newEventName){
								$localeEventGame->title = $newEventName;
								$localeEventGame->save(false);
							}
						} else {
							$localeEventGame = new EventGameLocaleModel(['scenario' => EventGameLocaleModel::SCENARIO_CREATE]);
							$localeEventGame->setAttributes([
								'game_id' => $gameApi->game_id,
								'sports_id' => $gameApi->sports_id,
								'tournament_id' => $gameApi->tournament_id,
								'lang_code' => self::LANG_CODE_RU,
								'title' => $newEventName,
								'is_active' => 1,
							]);
							$localeEventGame->save();
						}
					}

					$marketGroupCode = 'all_markets';
						if(($group_id = $this->findMarketGroup(self::SOURCE_BETSAPI, $marketGroupCode, $gameApi->sports_id)) === null){
							$group_id = $this->createMarketGroup([
								'title' => 'All markets',
								'lang' => Yii::$app->language,
								'service_api' => self::SOURCE_BETSAPI,
								'code_api' => $marketGroupCode,
								'sports_id' => $gameApi->sports_id,
						]);
					}

					//foreach($event['Value']['E'] as  $outcome){
					foreach($event['Value']['GE'] as  $market){
						if(($marketData = $this->getMarket([
							'sports_id' => $gameApi->sports_id,
							'code' => $market['G'],
							'group_id' => $group_id,
						])) !== null){
							$market_id = $marketData['market_id'];
						} else {
							Yii::warning('Event id = '.$event['id'].'.Market error:'.$market['G']);
							continue;
						}

						foreach($market['E'] as $outcomeList){
							foreach($outcomeList as $outcome){
								if(empty($outcome['C'])){
									continue;
								}
								$odds = $outcome['C'];
								unset($outcome['C']);
								if(!$outcome){
									continue;
								}

								$is_enabled = ($odds > 1) ? true : false;

								$outcomeCodeString = $market_id .'T:'. $outcome['T'];
								if(!empty($outcome['P'])){
									$outcomeCodeString .= ';P:'.$outcome['P'];
								}
								if(!empty($outcome['CE'])){
									$outcomeCodeString .= ';CE:'.$outcome['CE'];
								}

								$this->createUpdateOutcome([
									'code' => md5($outcomeCodeString),
									'outcomeData' => $outcome,
									'sports_id' => $gameApi->sports_id,
									'tournament_id' => $gameApi->tournament_id,
									'game_id' => $gameApi->game_id,
									'group_id' => $group_id,
									'market_id' => $market_id,
									'rate' => $odds,
									'is_enabled' => $is_enabled
								]);

							}
						}
					}
				}

				if(!empty($event['Value']['VI']) && (int)$event['Value']['VI'] > 0 ){
					Yii::warning('Game id = '.$gameApi->game_id.'.Video:'.$event['Value']['VI']);
					$eventGame = EventGameModel::findOne($gameApi->game_id);
					$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
					$eventGame->video_link = (int)$event['Value']['VI'];
					$eventGame->save(false);
				}
				//if($gameApi->is_checked == false && ($gameApi->is_main_provider == false or ($gameApi->is_main_provider == true && $gameApi->api_event_id == 0 ) )){
				$isNewEvent = 0;
				if($gameApi->is_checked == false ){
					$gameApi->is_checked = true;
					$gameApi->save(false);
					$isNewEvent = 1;
				}
				$transaction->commit();
			} catch (\Throwable $e) {
				Yii::error($e);
				$transaction->rollBack();
			}
		}
		try{
			if(count($eventsCodeApi) > 0){
				$idsGame = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['game_id'])->andWhere([
					ApiEventGameModel::tableName() . '.service_api' => self::SOURCE_BETSAPI,
					ApiEventGameModel::tableName() . '.is_main_provider' => true,
					ApiEventGameModel::tableName() . '.api_event_id' => 0,
					ApiEventGameModel::tableName() . '.code_api' => $eventsCodeApi,
				])->asArray()->all(), 'game_id');

				EventGameModel::updateAll(
					//['is_resulted' => true,'is_finished' => 1],
					['is_finished' => 1],
					['in', 'id', $idsGame]
				);
			}
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		return false;
	}

	public function eventLive($event) {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-1xbet-service-event-live.log');
		}
		//$eventsCodeApi = [214690264];
		//$jsonData = file_get_contents("https://api.betsapi.com/v1/1xbet/event?token=23807-64J9ST5D911ANZ&event_id=214690264");

		/*$event_id = (int)$event['id'];
		if($redis = Yii::$app->get('redis', false)) {
			$key = '1xbet-event-live-time-update-'.$event_id;
			if(($updated_at = $redis->get($key)) != null && (int)$updated_at == (int)$event['updated_at']){
				//Yii::warning('Event id = '.$event['id'].'.updated_at:'.$event['updated_at']);
				return false;
			}
			$redis->set($key,(int)$event['updated_at'],'EX',3600*3);
		}*/

			$transaction = Yii::$app->db->beginTransaction();
			try {
				if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $event['id'])) === null){
					$transaction->commit();
					Yii::warning('Event id = '.$event['id'].'.Event not found');
					return false;
				}

				$marketGroupCode = 'all_markets';
				if(($group_id = $this->findMarketGroup(self::SOURCE_BETSAPI, $marketGroupCode, $gameApi->sports_id)) === null){
						$transaction->commit();
						Yii::warning('Event id = '.$event['id'].'.all_markets not found');
						return false;
					}

					$outcomeIds = EventOutcomesModel::find()->select('outcome_id')
					->andWhere([
						'game_id'=> $gameApi->game_id,
						//'is_enabled'=> true,
					])->indexBy('outcome_id')->asArray()->column();

						foreach($event['Value']['GE'] as  $market){
							if(($marketData = $this->getMarket([
								'sports_id' => $gameApi->sports_id,
								'code' => $market['G'],
								'group_id' => $group_id,
							])) !== null){
								$market_id = $marketData['market_id'];
							} else {
								Yii::warning('Event id = '.$event['id'].'.Market error:'.$market['G']);
								continue;
							}

							foreach($market['E'] as $outcomeList){
								foreach($outcomeList as $outcome){
									if(empty($outcome['C'])){
										continue;
									}
									$odds = $outcome['C'];
									unset($outcome['C']);
									if(!$outcome){
										continue;
									}

									$is_enabled = ($odds > 1) ? true : false;

									$outcomeCodeString = $market_id .'T:'. $outcome['T'];
									if(!empty($outcome['P'])){
										$outcomeCodeString .= ';P:'.$outcome['P'];
									}
									if(!empty($outcome['CE'])){
										$outcomeCodeString .= ';CE:'.$outcome['CE'];
									}

									$outcome_id = $this->createUpdateOutcome([
										'code' => md5($outcomeCodeString),
										'outcomeData' => $outcome,
										'sports_id' => $gameApi->sports_id,
										'tournament_id' => $gameApi->tournament_id,
										'game_id' => $gameApi->game_id,
										'group_id' => $group_id,
										'market_id' => $market_id,
										'rate' => $odds,
										'is_enabled' => $is_enabled
									]);
									unset($outcomeIds[$outcome_id]);
								}
							}
						}
						if(count($outcomeIds) > 0){
							if(($eventOutcomes = EventOutcomesModel::find()
								->andWhere(['outcome_id'=> $outcomeIds, 'game_id'=> $gameApi->game_id])
								->all()) !== null){
									foreach($eventOutcomes as $eventOutcome){
										$eventOutcome->is_enabled = 0;
										$eventOutcome->save();
									}
							}
						}


				if($gameApi->api_event_id == 0 && !empty( $event['Value']['SC'])){
					$eventGame = EventGameModel::findOne($gameApi->game_id);
					$timer = null;
					if(!empty($event['Value']['SC']['TS'])){
						$sec = $event['Value']['SC']['TS'] % 60;
						$min = floor($event['Value']['SC']['TS'] / 60);
						$run = 1;
						if(!empty( $event['Value']['SN'])){
							switch($event['Value']['SN']){
								case 'Football':
									if($event['Value']['SC']['TS'] == 2700){
										$run = 0;
									}
								break;
								case 'Basketball':
									$run = 0;
								break;
							}
						}
						$timer = ['m' => $min, 's'=> $sec, 'run' => 1, 'timeGetData'=> time(),'timeSend' => time()];
					}
					$resultShort = '0-0';
					$resultShortSimple = '0:0';
					$resultExtended = [];
					$statistics = [];
					$gamePeriod = '';
					if(!empty($event['Value']['SC']['PS']) &&  !empty($event['Value']['TN']) ){
						$gamePeriod = count($event['Value']['SC']['PS']) .' '.$event['Value']['TN'];
					}
					if(!empty($event['Value']['SC']['FS']['S1']) or !empty($event['Value']['SC']['FS']['S2'])){
						$scoreHome = ($event['Value']['SC']['FS']['S1']) ?? 0;
						$scoreAway = ($event['Value']['SC']['FS']['S2']) ?? 0;
						$resultShort = $scoreHome.'-'.$scoreAway;
						$resultShortSimple = $scoreHome.':'.$scoreAway;
					}
					if(!empty($event['Value']['SC']['PS']) && count($event['Value']['SC']['PS']) > 0 ){
						$resultShort .= ' [';
						$scoreAllPeriods = [];
						foreach($event['Value']['SC']['PS'] as $scorePeriod){
							$scorePeriodHome = ($scorePeriod['Value']['S1']) ?? 0;
							$scorePeriodAway = ($scorePeriod['Value']['S2']) ?? 0;
							$scoreAllPeriods[$scorePeriod['Key']] = $scorePeriodHome.'-'.$scorePeriodAway;
							if(!empty( $event['Value']['SN'])){
								switch($event['Value']['SN']){
									case 'Football':
										$resultExtended[$scorePeriod['Key'].'/2'] = [$scorePeriodHome,$scorePeriodAway];
									break;
									case 'Basketball':
										$resultExtended[$scorePeriod['Key'].'/4'] = [$scorePeriodHome,$scorePeriodAway];
									break;
								}
							}
						}
						ksort($scoreAllPeriods);
						$resultShort .= implode(', ',$scoreAllPeriods);
						$resultShort .= ']';
					} else {
						//$resultShort .= '[0-0]';
					}

					if(!empty( $event['Value']['SN']) && !empty( $event['Value']['SC']['S']) ){
						$statData = [];
						foreach($event['Value']['SC']['S'] as $statLine){
							$statData[$statLine['Key']] = $statLine['Value'];
						}
						switch($event['Value']['SN']){
							case 'Football':
								$resultExtended['Yellow cards'] = (isset($statData['IYellowCard1']) && isset($statData['IYellowCard2']) ) ? [$statData['IYellowCard1'],$statData['IYellowCard2']] : [0,0];
								$resultExtended['Red cards'] = (isset($statData['IRedCard1']) && isset($statData['IRedCard2']) ) ? [$statData['IRedCard1'],$statData['IRedCard2']] : [0,0];
								$resultExtended['Corners'] = (isset($statData['ICorner1']) && isset($statData['ICorner2']) ) ? [$statData['ICorner1'],$statData['ICorner2']] : [0,0];
								$resultExtended['Penalties'] = (isset($statData['IPenalty1']) && isset($statData['IPenalty2']) ) ? [$statData['IPenalty2'],$statData['IPenalty2']] : [0,0];
							break;
							case 'Basketball':

							break;
						}
					}

					if($eventGame->result != $resultShort){
						$eventGame->updateResult([
							'resultShort' => $resultShort,
							'timer' => $timer,
							'resultShortSimple' => $resultShortSimple,
							'resultExtended' => Json::encode($resultExtended),
							'gamePeriod' => $gamePeriod,
							'statistics' => Json::encode($statistics)
						]);
					} elseif($timer != null) {
						$eventGame->setTimer($timer);
					}
				}

				if($gameApi->is_checked == false ){
					$gameApi->is_checked = true;
					$gameApi->save(false);
					if(!empty($event['Value']['VI']) && (int)$event['Value']['VI'] > 0 ){
					Yii::warning('Game id = '.$gameApi->game_id.'.Video:'.$event['Value']['VI']);
						$eventGame = EventGameModel::findOne($gameApi->game_id);
						$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
						$eventGame->video_link = (int)$event['Value']['VI'];
						$eventGame->save(false);
					}
				}
				$transaction->commit();
			} catch (\Throwable $e) {
				Yii::error($e);
				$transaction->rollBack();
			}
		return false;
	}

	public function filterInplay($jsonData, $gamesLive) {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-1xbet-service-inplay.log');
		}
		$data =  Json::decode($jsonData);

		$transaction = Yii::$app->db->beginTransaction();
		try {

			$codeApiIds = [];
			$apiEventIds = [];
			$ids1xbetGame = [];
			$gamesOnly1xbet = [];
			$gameNotFinished = [];

			foreach($data['results'] as $game){
				//$ids1xbetGame[$game['id']] = $game['our_event_id'];
				if(!empty($gamesLive[$game['id']])){
					if($gamesLive[$game['id']]['is_finished'] == 1){
						$gameNotFinished[] = $gamesLive[$game['id']];
					}
					unset($gamesLive[$game['id']]);
				}
				if($game['our_event_id'] > 0){
					$codeApiIds[$game['our_event_id']] = $game['id'];
					$apiEventIds[$game['id']] = $game['our_event_id'];
				}
				$gamesOnly1xbet[$game['id']] = $game;
			}

			$apiGames = ArrayHelper::index(ApiEventGameModel::find()->select( ApiEventGameModel::tableName() .'.*')->andWhere([
				ApiEventGameModel::tableName() . '.service_api'=> self::SOURCE_BETSAPI,
				//ApiEventGameModel::tableName() . '.code_api'=> $codeApiIds,
				ApiEventGameModel::tableName() . '.api_event_id'=> $apiEventIds,
				//EventGameModel::tableName() . '.is_live'=> false
				//ApiEventGameModel::tableName() . '.is_main_provider' => true,
			])->joinWith('game', false)->asArray()->all(),'game_id');

			if(count($apiGames) > 0){
				$idsGame = array_keys($apiGames);
				foreach($apiGames as $apiGame){
					if(!empty($codeApiIds[$apiGame['api_event_id']])){
						unset($gamesOnly1xbet[$codeApiIds[$apiGame['api_event_id']]]);
						if($this->findGame(self::SOURCE_BETSAPI, $codeApiIds[$apiGame['api_event_id']]) === null){
							ApiEventGameModel::updateAll(
								['is_main_provider' => false],
								['game_id' => $apiGame['game_id']]
							);
							$this->createApiGame([
								'game_id' => $apiGame['game_id'],
								'sports_id' => $apiGame['sports_id'],
								'tournament_id' => $apiGame['tournament_id'],
								'service_api' => self::SOURCE_BETSAPI,
								'code_api' => $codeApiIds[$apiGame['api_event_id']],
								'api_event_id' => $apiGame['api_event_id'],
								'is_main_provider' => 1,
							]);

							\Yii::$app->db->createCommand('UPDATE '.EventOutcomesModel::tableName().', '.ApiGameMarketsModel::tableName().'
							SET '.EventOutcomesModel::tableName().'.is_hidden = 1
							WHERE '.EventOutcomesModel::tableName().'.market_id = '.ApiGameMarketsModel::tableName().'.market_id 
							AND '.EventOutcomesModel::tableName().'.game_id = '.$apiGame['game_id'].'
							AND '.ApiGameMarketsModel::tableName().'.service_api = '.BetsapiService::SOURCE_BETSAPI)
							->execute();
							\Yii::$app->db->createCommand('UPDATE '.EventOutcomesModel::tableName().', '.ApiGameMarketsModel::tableName().'
							SET '.EventOutcomesModel::tableName().'.is_hidden = 0
							WHERE '.EventOutcomesModel::tableName().'.market_id = '.ApiGameMarketsModel::tableName().'.market_id 
							AND '.EventOutcomesModel::tableName().'.game_id = '.$apiGame['game_id'].'
							AND '.ApiGameMarketsModel::tableName().'.service_api = '.Betsapi1xbetService::SOURCE_BETSAPI)
							->execute();
						}
					}
				}

				EventGameModel::updateAll(
					['is_live' => true],
					['and',
						['is_live' => false],
						['<=','starts_at',time()],
						['in', 'id', $idsGame],
					]);
			}

			if(count($gameNotFinished) > 0){
				$gamesLiveIds = ArrayHelper::map($gameNotFinished, 'code_api', 'game_id');
				EventGameModel::updateAll(
					['is_finished' => 0, 'is_resulted' => 0],
					['in', 'id', $gamesLiveIds]
				);
			}

			$transaction->commit();

			if(count($gamesOnly1xbet) > 0){
				foreach($gamesOnly1xbet as $event){
					if(empty(self::sports()[$event['sport_id']])){
						continue;
					}
					$sports_id = $this->getSport($event['sport_id'], self::sports()[$event['sport_id']] );
					$game_id = $this->processingPrematch([
						'sports_id' => $sports_id,
						'event' => $event,
						'service_api' => self::SOURCE_BETSAPI,
						'is_live' => true,
					]);
				}
			}
/*
			$event_ids = ArrayHelper::getColumn(ApiEventGameModel::find()->select(['code_api'])->andWhere([
				ApiEventGameModel::tableName() . '.service_api' => self::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.is_main_provider' => true,
				ApiEventGameModel::tableName() . '.is_checked' => false,
				ApiEventGameModel::tableName() . '.api_event_id' => 0,
				EventGameModel::tableName() . '.is_finished' => false,
				EventGameModel::tableName() . '.is_live' => true,
			])->joinWith('game', false)->asArray()->all(), 'code_api');

			while(count($slice = array_splice($event_ids, 0, 10)) > 0) {
				exec('php yii.php 1xbet/inplay-start-event '.Json::encode($slice).' > /dev/null 2>/dev/null &');
			}*/

			return $gamesLive;
		} catch (\Exception $e) {
			Yii::error($e);
		}
		$transaction->rollBack();
		return false;
	}

	public function result ($jsonData, $eventsCodeApi) {
		$this->timeGetData = time();
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-1xbet-service-result.log');
		}
		/*$eventsCodeApi = [215262498];
		$jsonData = file_get_contents('https://api.betsapi.com/v1/1xbet/result?token=23807-64J9ST5D911ANZ&event_id=215262498');*/
		$data =  Json::decode($jsonData);

		$bet365service = new BetsapiService;
		/*
		print_r($eventsCodeApi);
		print_r($data['results']);
		exit();*/

		foreach($data['results'] as $kEventId => $game){

			$transaction = Yii::$app->db->beginTransaction();
			try {

				if(isset($game['success']) && $game['success'] == 0){
					//if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $game['id'])) !== null){
					if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $eventsCodeApi[$kEventId])) !== null){
						$eventGame = EventGameModel::findOne($gameApi->game_id);
						$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
						if($eventGame != null ){
							$eventGame->is_resulted = 1;
							$eventGame->is_finished = 1;
							$eventGame->save();
						}
					}
					$transaction->commit();
					Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.Event result empty');
					continue;
				}
				if(empty($game['sport_id']) or ($this->sports_id = $bet365service->getSport($game['sport_id'], $bet365service->sports()[$game['sport_id']])) === null){
					Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.Sport id empty');
					$transaction->commit();
					continue;
				}

				if($game['time_status'] == self::TIME_STATUS_NOT_STARTED){
					//Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.Event not started');
					$transaction->commit();
					continue;
				}

				if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $eventsCodeApi[$kEventId])) === null){

					if(($tournament_id = $this->findTournamentByName([
							'name' => $game['league']['name'],
							'sports_id' => $this->sports_id,
							'lang' => Yii::$app->language,
						])) === null){
							Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.Tournament error:'.Json::encode($game['league']));
							$transaction->commit();
							continue;
						}

					//$game['home']['name'] = BaseInflector::transliterate($game['home']['name']);
					//$game['away']['name'] = BaseInflector::transliterate($game['away']['name']);

					$gameName = BaseInflector::transliterate($game['home']['name'] . ' - ' . $game['away']['name']);

					if(($this->game_id = $this->findGameByName([
							'name' => $gameName,
							'sports_id' => $this->sports_id,
							'tournament_id' => $tournament_id,
							'lang' => Yii::$app->language,
						])) === null){
							$transaction->commit();
							Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'. Event name:'.$gameName.'.Event not found');
							continue;
						}

				} else {
					$this->sports_id = $gameApi->sports_id;
					$tournament_id = $gameApi->tournament_id;
					$this->game_id = $gameApi->game_id;
				}

				if($game['league']['cc'] != null){
					$eventTournament = EventTournamentModel::findOne((int)$tournament_id);
					$eventTournament->scenario = \common\modules\games\models\EventTournamentModel::SCENARIO_UPDATE;
					if($eventTournament != null ){
						$eventTournament->flag = (string)$game['league']['cc'];
						$eventTournament->save();
					}
				}

				if($game['time_status'] == self::TIME_STATUS_RETIRED){
					$eventGame = EventGameModel::findOne((int)$this->game_id);
					$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
					if($eventGame != null ){
						$eventGame->is_finished = 1;
						$eventGame->save();
					}
					Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.Time status: TIME_STATUS_RETIRED');
					$transaction->commit();
					continue;
				}

				if($game['time_status'] == self::TIME_STATUS_INPLAY or $game['time_status'] == self::TIME_STATUS_ENDED){
					$resultExtended = [];
					$statistics = [];
					$resultShortSimple = '';
					$resultShort = '';
					$gamePeriod = '';
					$eventGame = EventGameModel::findOne((int)$this->game_id);
					$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
					if($eventGame != null ){
						switch($bet365service->sports()[$game['sport_id']]){
							case 'Soccer':
								if(!empty( $game['timer'])){
									$timer = ['m' => $game['timer']['tm'], 's'=> $game['timer']['ts'], 'run' => $game['timer']['tt'] ?? 0, 'timeGetData'=> $this->timeGetData,'timeSend' => time()];
									$eventGame->setTimer($timer);
								}
								$resultShort = $game['ss'] ?? '';
								$resultShortSimple = $game['ss'] ? str_replace('-', ':', $game['ss']) : '';
								if(!empty($game['stats'])){
									$resultExtended['Yellow cards'] = $game['stats']['yellowcards'] ?? null;
									$resultExtended['Red cards'] = $game['stats']['redcards'] ?? null;
									if($game['time_status'] == self::TIME_STATUS_ENDED){
										$statistics = [
											'Ball possession' => $game['stats']['possession_rt'] ?? null,
											'Goal attempts' => $game['stats']['goalattempts'] ?? null,
											'Shots on target' => $game['stats']['on_target'] ?? null,
											'Shots off target' => $game['stats']['off_target'] ?? null,
											'Shots blocked' => $game['stats']['shots_blocked'] ?? null,
											'Corners' => $game['stats']['corners'] ?? null,
											'Offsides' => $game['stats']['offsides'] ?? null,
											'Saves' => $game['stats']['saves'] ?? null,
											'Fouls' => $game['stats']['fouls'] ?? null,
											'Penalties' => $game['stats']['penalties'] ?? null,
											'Ball Safe' => $game['stats']['ball_safe'] ?? null,
											'Injuries' => $game['stats']['injuries'] ?? null,
											'Substitutions' => $game['stats']['substitutions'] ?? null,
											'Attacks' => $game['stats']['attacks'] ?? null,
											'Dangerous Attacks' => $game['stats']['dangerous_attacks'] ?? null,
											'Yellow Cards' => $game['stats']['yellowcards'] ?? null,
											'Red Cards' => $game['stats']['redcards'] ?? null,
										];
									}
								}
								if(!empty($game['scores'])){
									$resultShort .= ' [';
									if(!empty($game['scores'][1]) && !empty($game['scores'][2])){
										$resultShort .= implode('-',$game['scores'][1]);
										$resultExtended['1/2'] = array_values($game['scores'][1]);
										$gameScores2 = [
											'home' => $game['scores'][2]['home'] - $game['scores'][1]['home'],
											'away' => $game['scores'][2]['away'] - $game['scores'][1]['away'],
										];
										$resultShort .= ', '.implode('-',$gameScores2);
										$resultExtended['2/2'] = array_values($gameScores2);
										$gamePeriod = '2 Half';
									} elseif(!empty($game['scores'][2])) {
										$resultShort .= implode('-',$game['scores'][2]);
										$resultExtended['1/2'] = array_values($game['scores'][2]);
										$gamePeriod = '1 Half';
									}
									$resultShort .= ']';
									if(!empty($game['stats'])){
										$resultExtended['Corners'] = $game['stats']['corners'] ?? null;
										$resultExtended['Penalties'] = $game['stats']['penalties'] ?? null;
									}
								}
							break;
							case 'Basketball':
								$resultShort = $game['ss'] ?? '';
								$resultShortSimple = $game['ss'] ? str_replace('-', ':', $game['ss']) : '';
								if(!empty($game['stats']) && $game['time_status'] == self::TIME_STATUS_ENDED && $bet365service->sports()[$game['sport_id']] == 'Basketball'){
									$statistics = [
										'2-point field goals scored' => $game['stats']['2points'] ?? null,
										'3-point field goals scored' => $game['stats']['3points'] ?? null,
										'Free throws scored' => $game['stats']['free_throws'] ?? null,
										'Free throws %' => $game['stats']['free_throws_rate'] ?? null,
										'Biggest lead' => $game['stats']['biggest_lead'] ?? null,
										'Fouls' => $game['stats']['fouls'] ?? null,
										'Possession %' => $game['stats']['possession'] ?? null,
										'Successful Attempts' => $game['stats']['success_attempts'] ?? null,
										'Time Outs' => $game['stats']['time_outs'] ?? null,
										'Lead changes' => $game['stats']['lead_changes'] ?? null,
										'Max Points in a Row' => $game['stats']['maxpoints_inarow'] ?? null,
										'Time spent in lead' => $game['stats']['timespent_inlead'] ?? null,
									];
								}
								$gamePeriodInt = 1;
								if(!empty($game['scores'])){
									$resultShort .= ' [';
									$i = 0;
									foreach($game['scores'] as $scoreKey => $score){
										if(array_search($scoreKey, [1,2,4,5]) !== false){
											$i++;
											$resultShort .= (($i > 1) ? ', ' : '').implode('-',$score);
											$resultExtended[$i.'/4'] = array_values($score);
											$gamePeriod = $i.' Quarter';
											$gamePeriodInt = $i;
										}
									}
									$resultShort .= ']';
									if(!empty( $game['timer'])){
										if($game['timer']['tm'] == 10){
											$timeMinutes = ($gamePeriodInt - 1) * 10;
										} else {
											$timeMinutes = ($gamePeriodInt - 1) * 10 + (10-$game['timer']['tm']);
										}
										$timeSeconds =  ($game['timer']['ts'] > 0) ? 60 - $game['timer']['ts'] : 0;
										$timer = ['m' => $timeMinutes, 's'=> $timeSeconds, 'run' => ($game['timer']['tm'] == 10) ? 0 : 1, 'timeGetData'=> $this->timeGetData,'timeSend' => time()];
										$eventGame->setTimer($timer);
									}
								}
							break;
							default:
								$resultShort = $game['ss'] ?? '';
								$resultShortSimple = $game['ss'] ? str_replace(['-','/'], ':', $game['ss']) : '';
							break;
						}
						if($game['time_status'] == self::TIME_STATUS_INPLAY){
							if($redis = Yii::$app->get('redis', false)) {
								$key = '1xbet-event-result-'.$gameApi->game_id;
								if(($resultExtendedRedis = $redis->get($key)) != null && $resultExtendedRedis == Json::encode($resultExtended)){
									$transaction->commit();
									continue;
								}
								$redis->set($key,Json::encode($resultExtended),'EX',3600*3);
							}
						}

						$eventGame->updateResult([
							'resultShort' => $resultShort,
							'timer' => '',
							'resultShortSimple' => $resultShortSimple,
							'resultExtended' => Json::encode($resultExtended),
							'gamePeriod' => $gamePeriod,
							'statistics' => Json::encode($statistics)
						]);
					}
					if($game['time_status'] == self::TIME_STATUS_INPLAY){
						$transaction->commit();
						continue;
					}
					if($game['time_status'] == self::TIME_STATUS_ENDED && ($eventGame->starts_at + 300) > time() ){
						$transaction->commit();
						Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.TIME_STATUS_ENDED.Start of event less than 5 min.');
						continue;
					}
				}

				if(($sportsResultService = $this->getSportsResultService($bet365service->sports()[$game['sport_id']])) === null){
					// TODO add logs
					$transaction->commit();
					continue;
				}

				if($game['time_status'] == self::TIME_STATUS_ENDED){
					$success = (new $sportsResultService($game, $this->sports_id, $this->game_id))->processingResults();
				}

				/* TODO time status
					const TIME_STATUS_POSTPONED = 4;
					const TIME_STATUS_CANCELLED = 5;
					const TIME_STATUS_WALKOVER = 6;
					const TIME_STATUS_INTERRUPTED = 7;
					const TIME_STATUS_ABANDONED = 8;
					const TIME_STATUS_REMOVED = 99;
				*/

				$eventGame = EventGameModel::findOne((int)$this->game_id);
				$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
				if($eventGame != null ){
					$eventGame->is_resulted = true;
					$eventGame->is_finished = 1;
					$eventGame->save();
				}

				$transaction->commit();
			} catch (\Throwable $e) {
				Yii::error($e);
				$transaction->rollBack();
			}
		}
		return true;
	}

}

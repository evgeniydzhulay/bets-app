<?php

namespace backapi\modules\games\services;

use backapi\modules\games\models\FileUploadModel;
use backapi\modules\games\services\sport_result\AmericanFootballBetsapiResultService;
use backapi\modules\games\services\sport_result\AustralianRulesBetsapiResultService;
use backapi\modules\games\services\sport_result\BadmintonBetsapiResultService;
use backapi\modules\games\services\sport_result\BaseballBetsapiResultService;
use backapi\modules\games\services\sport_result\BasketballBetsapiResultService;
use backapi\modules\games\services\sport_result\BeachVolleyballBetsapiResultService;
use backapi\modules\games\services\sport_result\BowlsBetsapiResultService;
use backapi\modules\games\services\sport_result\BoxingUFCBetsapiResultService;
use backapi\modules\games\services\sport_result\CricketBetsapiResultService;
use backapi\modules\games\services\sport_result\DartsBetsapiResultService;
use backapi\modules\games\services\sport_result\EsportsBetsapiResultService;
use backapi\modules\games\services\sport_result\FloorballBetsapiResultService;
use backapi\modules\games\services\sport_result\FutsalBetsapiResultService;
use backapi\modules\games\services\sport_result\GaelicSportsBetsapiResultService;
use backapi\modules\games\services\sport_result\HandballBetsapiResultService;
use backapi\modules\games\services\sport_result\IceHockeyBetsapiResultService;
use backapi\modules\games\services\sport_result\RugbyLeagueBetsapiResultService;
use backapi\modules\games\services\sport_result\RugbyUnionBetsapiResultService;
use backapi\modules\games\services\sport_result\SnookerBetsapiResultService;
use backapi\modules\games\services\sport_result\SoccerBetsapiResultService;
use backapi\modules\games\services\sport_result\SquashBetsapiResultService;
use backapi\modules\games\services\sport_result\TableTennisBetsapiResultService;
use backapi\modules\games\services\sport_result\TennisBetsapiResultService;
use backapi\modules\games\services\sport_result\VolleyballBetsapiResultService;
use backapi\modules\games\services\sport_result\WaterPoloBetsapiResultService;
use common\modules\games\models\ApiEventGameModel;
use common\modules\games\models\ApiGameMarketsModel;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\EventParticipantsModel;
use common\modules\games\models\EventTournamentModel;
use common\modules\games\models\GameParticipantsModel;
use common\modules\games\queries\GameParticipantsLocaleQuery;
use common\modules\games\queries\GameParticipantsQuery;
use backapi\modules\games\services\Betsapi1xbetService;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\helpers\Json;
use yii\log\FileTarget;

/*
 Processing Requests for betsapi.com
*/

class BetsapiService extends BaseApiService {

	const SOURCE_BETSAPI = 1;

	const TIME_STATUS_NOT_STARTED = 0;
	const TIME_STATUS_INPLAY = 1;
	const TIME_STATUS_TO_BE_FIXED = 2;
	const TIME_STATUS_ENDED = 3;
	const TIME_STATUS_POSTPONED = 4;
	const TIME_STATUS_CANCELLED = 5;
	const TIME_STATUS_WALKOVER = 6;
	const TIME_STATUS_INTERRUPTED = 7;
	const TIME_STATUS_ABANDONED = 8;
	const TIME_STATUS_RETIRED = 9;
	const TIME_STATUS_REMOVED = 99;

	public $sports_id = null;
	public $sports_name = null;
	public $tournament_id = null;
	public $game_id = null;
	public $group_id = null;
	public $market_id = null;
	public $outcome_id = null;
	public $event_FI = null;
	public $timeGetData = null;
	public $participantsValues = null;
	public $participantsValuesFormat = null;
	public $participantsNames = null;
	public $eventChanged = [];
	public $alternativeNameParticipants = [
		'{item1}' =>['Home',1],
		'{item2}' => ['Away',2],
		'Draw' => ['X']
	];
	public $outcomesNameFormat = [
		'Home' => '{item1}',
		'Away' => '{item2}',
		/*' O ' => ' Over ',
		' U ' => ' Under ',
		' E ' => ' Exactly ',*/
	];

	protected function setParticipantsValuesDefault() {
		$this->participantsValues = ['Home','Away','Home','Away'];
		$this->participantsValuesFormat = ['{item1}','{item2}','{item1}','{item2}'];
	}

	protected function setOutcomesNameFormat($name) {
		$name = strtr($name, $this->outcomesNameFormat);
		$replacementRules = [
			'/^(.*)\sO\s(.*)$/' => '${1} Over ${2}',
			'/^(.*)\sU\s(.*)$/' => '${1} Under ${2}',
			'/^(.*)\sE\s(.*)$/' => '${1} Exactly ${2}',
			'/^O\s(.*)$/' => 'Over ${1}',
			'/^U\s(.*)$/' => 'Under ${1}',
			'/^E\s(.*)$/' => 'Exactly ${1}',
			'/^(.*)\sO$/' => '${1} Over',
			'/^(.*)\sU$/' => '${1} Under',
			'/^(.*)\sE$/' => '${1} Exactly',
			'/[\s]+/s' => ' '
		];
		$patterns = array_keys($replacementRules);
		$replacements = array_values($replacementRules);
		$nameFormat = preg_replace($patterns,$replacements,$name);
		return $nameFormat;
	}

	public static function sports() {
		return [
			1 => 'Soccer',
			3 => 'Cricket',
			8 => 'Rugby Union',
			9 => 'Boxing/UFC',
			12 => 'American Football',
			13 => 'Tennis',
			14 => 'Snooker',
			15 => 'Darts',
			16 => 'Baseball',
			17 => 'Ice Hockey',
			18 => 'Basketball',
			19 => 'Rugby League',
			36 => 'Australian Rules',
			66 => 'Bowls',
			75 => 'Gaelic Sports',
			78 => 'Handball',
			83 => 'Futsal',
			90 => 'Floorball',
			91 => 'Volleyball',
			92 => 'Table Tennis',
			94 => 'Badminton',
			95 => 'Beach Volleyball',
			107 => 'Squash',
			110 => 'Water Polo',
			151 => 'E-sports'
		];
	}

	public function getSportsResultService($sportsName) {
		$sportsResultServices = [
			'Soccer' => SoccerBetsapiResultService::class,
			'Cricket' => CricketBetsapiResultService::class,
			'Rugby Union' => RugbyUnionBetsapiResultService::class,
			'Boxing/UFC' => BoxingUFCBetsapiResultService::class,
			'American Football' => AmericanFootballBetsapiResultService::class,
			'Tennis' => TennisBetsapiResultService::class,
			'Snooker' => SnookerBetsapiResultService::class,
			'Darts' => DartsBetsapiResultService::class,
			'Baseball' => BaseballBetsapiResultService::class,
			'Ice Hockey' => IceHockeyBetsapiResultService::class,
			'Basketball' => BasketballBetsapiResultService::class,
			'Rugby League' => RugbyLeagueBetsapiResultService::class,
			'Australian Rules' => AustralianRulesBetsapiResultService::class,
			'Bowls' => BowlsBetsapiResultService::class,
			'Gaelic Sports' => GaelicSportsBetsapiResultService::class,
			'Handball' => HandballBetsapiResultService::class,
			'Futsal' => FutsalBetsapiResultService::class,
			'Floorball' => FloorballBetsapiResultService::class,
			'Volleyball' => VolleyballBetsapiResultService::class,
			'Table Tennis' => TableTennisBetsapiResultService::class,
			'Badminton' => BadmintonBetsapiResultService::class,
			'Beach Volleyball' => BeachVolleyballBetsapiResultService::class,
			'Squash' => SquashBetsapiResultService::class,
			'Water Polo' => WaterPoloBetsapiResultService::class,
			'E-sports'  => EsportsBetsapiResultService::class,
		];
		return $sportsResultServices[$sportsName];

	}

//Lisa Sabino (Svr) -> Lisa Sabino

	public static function markets_name_live() {
		return [
			//Soccer
			//'Match Goals' => 'Alternative Total Goals',
			'Match Goals' => 'Total Goals',
			'Alternative Match Goals' => 'Alternative Total Goals',
			'Goals Odd/Even' => 'Goals Odd Even',
			'1st Goal Method' => 'First Goal Method',
			'2-Way Corners' => 'Corners 2 Way',
			'1st Half Corners' => 'First Half Corners',
			'Result / Both Teams To Score' => 'Result Both Teams To Score',
			'Final Score' =>'Correct Score',
			'Half Time/Full Time' => 'Half Time Full Time',
			'To Win 2nd Half' => '2nd Half Result',
			//'3-Way Handicap' => 'Handicap Result',
			'3-Way Handicap' => 'Alternative Handicap Result',

			//Basketball
			'1st Half - Team Totals' => '1st Half Team Totals',
			'1st Quarter - Total Odd/Even' => '1st Quarter Total Odd Even',
			'1st Quarter - Total (3-Way)' => '1st Quarter Total 3 Way',
			'1st Quarter Double Chance' => '1st Quarter Double Chance 1',
			'1st Quarter - Total (2-Way)' => '1st Quarter Totals',
			'1st Half - Total (3-Way)' => '1st Half Totals 3 Way',
			'Spread' => 'Point Spread',
			//Tennis
			'Set 1 Winner' => 'First Set Winner',
			'Set 1 to Break Serve' => 'First Set Player To Break Serve',
			'Set 1 Handicap' => 'First Set Handicap',
			'Set 1 Correct Score Group' => 'First Set Correct Score Group',
			'Set 1 Correct Score Any Player' => 'First Set Score Any Player',
			'Total Games in Match' => 'Total Games 2 Way',
			'Alternative Total Games in Match' => 'Alternative Total Games 2 Way',
			'Total Games in Set 1' => '1st Set Total Games',
			'Set 1 Score after 4 Games' => 'First Set Score After 4 Games',
			'Set 1 Score after 6 Games' => 'First Set Score After 6 Games',
			'Set 1 Score' => '1st Set Game Score',
			'Tie Break in Match?' => 'Tie Break In Match',
			//Baseball
			'Game Total' => 'Game Totals',
			'Total Hits - Inning 1' => '1st Innings Hits',
			'Most Hits - Inning 1' => 'Most Hits In The 1st Innings',
			'Alternative Run Lines' => 'Alternative Run Line',
			'Alternative Game Totals' => 'Alternative Game Total',
			'To Go to Extra Innings' => 'Extra Innings',
			//
			'Full Time Result' => 'Fulltime Result',
			//Other
			'Match Winner' => 'To Win Match',
			'Game 1 Winner' => '1st Game',
			'Match Winner 2-Way' => 'To Win Match',
			//'To Win Match' => 'Match Lines',
		];
	}

	public function inplay_filter ($jsonData) {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-bet365-service-inplay-filter.log');
		}
		//$jsonData = file_get_contents('https://betsapi.com/docs/samples/bet365_inplay_filter.json');
		$data =  Json::decode($jsonData);

		$transaction = Yii::$app->db->beginTransaction();
		try {
			$codeApiIds = [];
			$apiEventIds = [];
			$gamesLive = ArrayHelper::index(ApiEventGameModel::find()->select( ApiEventGameModel::tableName() .'.*')->andWhere([
				ApiEventGameModel::tableName() . '.service_api'=> self::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.is_main_provider' => true,
				EventGameModel::tableName() . '.is_finished' => false,
				EventGameModel::tableName() . '.is_live'=> true
			])->joinWith('game', false)->asArray()->all(),'code_api');

			foreach($data['results'] as $game){
				$codeApiIds[] = $game['id'];
				if(!empty($gamesLive[$game['id']])){
					unset($gamesLive[$game['id']]);
				}
				if($game['our_event_id'] > 0){
					$apiEventIds[$game['id']] = $game['our_event_id'];
				}
			}
			$apiGames = ArrayHelper::index(ApiEventGameModel::find()->select( ApiEventGameModel::tableName() .'.*')->andWhere([
				ApiEventGameModel::tableName() . '.service_api'=> self::SOURCE_BETSAPI,
				ApiEventGameModel::tableName() . '.code_api'=> $codeApiIds,
				EventGameModel::tableName() . '.is_live'=> false
			])->joinWith('game', false)->asArray()->all(),'game_id');
			if(count($apiGames) > 0){
				$idsGame = array_keys($apiGames);
				foreach($apiGames as $apiGame){
					if(!empty($apiEventIds[$apiGame['code_api']])){
						if($this->findGameByApiEventId($apiEventIds[$apiGame['code_api']]) === null){
							ApiEventGameModel::updateAll(
								['is_main_provider' => false],
								['game_id' => $apiGame['game_id']]
							);

							$this->createApiGame([
								'game_id' => $apiGame['game_id'],
								'sports_id' => $apiGame['sports_id'],
								'tournament_id' => $apiGame['tournament_id'],
								'service_api' => self::SOURCE_BETSAPI,
								'code_api' => $apiGame['code_api'],
								'api_event_id' => $apiEventIds[$apiGame['code_api']],
								'is_main_provider' => 1,
							]);

							\Yii::$app->db->createCommand('UPDATE '.EventOutcomesModel::tableName().', '.ApiGameMarketsModel::tableName().'
							SET '.EventOutcomesModel::tableName().'.is_hidden = 1
							WHERE '.EventOutcomesModel::tableName().'.market_id = '.ApiGameMarketsModel::tableName().'.market_id 
							AND '.EventOutcomesModel::tableName().'.game_id = '.$apiGame['game_id'].'
							AND '.ApiGameMarketsModel::tableName().'.service_api = '.Betsapi1xbetService::SOURCE_BETSAPI)
							->execute();
							\Yii::$app->db->createCommand('UPDATE '.EventOutcomesModel::tableName().', '.ApiGameMarketsModel::tableName().'
							SET '.EventOutcomesModel::tableName().'.is_hidden = 0
							WHERE '.EventOutcomesModel::tableName().'.market_id = '.ApiGameMarketsModel::tableName().'.market_id 
							AND '.EventOutcomesModel::tableName().'.game_id = '.$apiGame['game_id'].'
							AND '.ApiGameMarketsModel::tableName().'.service_api = '.BetsapiService::SOURCE_BETSAPI)
							->execute();
						}
					}
				}
				EventGameModel::updateAll(
					['is_live' => true],
					['and',
						['is_live' => false],
						['<=','starts_at',time()],
						['in', 'id', $idsGame],
					]);
			}
			if(count($gamesLive) > 0){
				$gamesLiveIds = ArrayHelper::map($gamesLive, 'code_api', 'game_id');
				EventGameModel::updateAll(
					['is_finished' => true],
					['in', 'id', $gamesLiveIds]
				);
			}
			$transaction->commit();
			return true;
		} catch (\Exception $e) {
			Yii::error($e);
		}
		$transaction->rollBack();
		return false;
	}

	public function inplay ($jsonData, $eventsQueue,$eventsBet365) {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-bet365-service-inplay.log');
		}
		//$jsonData = file_get_contents('https://api.betsapi.com/v1/bet365/inplay?token=23807-8GffU48caA3vLs');
	//	$jsonData = file_get_contents(Yii::getAlias("@app/bet365_4/inplay52.json"));
		$data =  Json::decode($jsonData);
		$this->timeGetData = time();
		$this->eventChanged = [];

		foreach($data['results'][0] as $k => $item){
			$transaction = Yii::$app->db->beginTransaction();
			try {
				switch($item['type']){
					case 'CL':
						$this->inplaySport($item);
					break;
					case 'CT':
						$this->inplayTournament($item);
					break;
					case 'EV':
						if(array_search($item['FI'], $eventsBet365) === false)
							break;

						if( !empty($data['results'][0][$k+1]) && $data['results'][0][$k+1]['type'] == 'EV')
							break;

						$this->inplayEvent($item);
					break;
					case 'MA':

						$this->inplayMarket($item);
					break;
					case 'PA':
					//print_r($item);
						$this->inplayOutcome($item,'inplay');
					break;
				}
				$transaction->commit();
			} catch (\Exception $e) {
				Yii::error($e);
				$transaction->rollBack();
			}
		}

		if(count($eventsQueue) > 0){
			foreach($eventsQueue as $event_fi => $timeGetEvent){
				if(($this->timeGetData - $timeGetEvent) >= 7 ){
					unset($eventsQueue[$event_fi]);
				}
			}
		}
		if(count($this->eventChanged) > 0){
			foreach($this->eventChanged as $event_fi){
				if(empty($eventsQueue[$event_fi])){
					$eventsQueue[$event_fi] = $this->timeGetData;
					exec('php yii.php bet365/inplay-event '.$event_fi.' > /dev/null 2>/dev/null &');
				}
			}
		}
		return $eventsQueue;
	}

	public function inplayEventAllMarkets ($jsonData, $event_fi) {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-bet365-service-inplay-event.log');
		}
		/*$jsonData = file_get_contents('https://api.betsapi.com/v1/bet365/event?token=23807-64J9ST5D911ANZ&FI=83710096');
		$event_fi = 83710096;*/

		$data =  Json::decode($jsonData);
		if(empty($data['results']) or empty($data['results'][0])){
			Yii::warning('Event id = '.$event_fi.'.Data event not found');
			return false;
		}
		$transaction = Yii::$app->db->beginTransaction();
		try {
			if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $event_fi)) == null){
				$transaction->commit();
				Yii::warning('Event id = '.$event_fi.'.Event not found');
				return false;
			}
			$this->sports_id = $gameApi->sports_id;
			$this->tournament_id = $gameApi->tournament_id;
			$this->game_id = $gameApi->game_id;
			Yii::warning('Event id = '.$event_fi);

			if(($outcomeIds = EventOutcomesModel::find()->select('outcome_id')
				->andWhere([
					'game_id'=> $this->game_id,
					//'is_enabled'=> true,
				])
				->indexBy('outcome_id')->asArray()->column()) !== null){
					foreach($data['results'][0] as $k => $item){
						switch($item['type']){
							case 'EV':
								$this->inplayEvent($item);
							break;
							case 'MA':
								/*if(!empty(self::markets_name_live()[$item["NA"]]))
									$item["NA"] = !empty(self::markets_name_live()[$item["NA"]]) ? self::markets_name_live()[$item["NA"]] : $item["NA"];*/

								if( strpos($item["NA"], '(') !== false){
									$item["NA"] = trim(explode('(',$item["NA"])[0]);
								}
								$this->inplayMarket($item);
								//unset($marketIds[$this->market_id]);
							break;
							case 'PA':
								$this->inplayOutcome($item,'inplayEventAllMarkets');

								unset($outcomeIds[$this->outcome_id]);
							break;
						}
					}

				if($outcomeIds){
					if(($eventOutcomes = EventOutcomesModel::find()
						->andWhere(['outcome_id'=> $outcomeIds, 'game_id'=> $this->game_id])
						->all()) !== null){
							foreach($eventOutcomes as $eventOutcome){
								$eventOutcome->is_enabled = 0;
								$eventOutcome->save();
							}
					}
				}
			}
			$transaction->commit();
			return true;
		} catch (\Exception $e) {
			Yii::error($e);
			$transaction->rollBack();
		}
		return false;
	}

	public function getSport($code, $name){

		if(($sports_id = $this->findSport(self::SOURCE_BETSAPI, $code)) !== null){
			return $sports_id;
		} else {
			if(($sports_id = $this->findSportByName([
					'name' => $name, 
					'lang' => Yii::$app->language,
				])) !== null){
					$this->createApiSport([
						'sports_id' => $sports_id,
						'service_api' => self::SOURCE_BETSAPI,
						'code_api' => $code,
					]);
					return $sports_id;
			} else {
				$sports_id = $this->createSport([
					'title' => $name,
					'lang' => Yii::$app->language,
					'service_api' => self::SOURCE_BETSAPI,
					'code_api' => $code,
				]);
				return $sports_id;
			}
		}
		return null;
	}

	protected function uploadParticipantImage($image) {
		$imageHeaders = get_headers('https://assets.b365api.com/images/team/b/'.$image.'.png',1);
		if($imageHeaders['Content-Type'] == 'image/png' && $imageHeaders['Content-Length'] > 200 && $imageHeaders['Content-Length'] < 100000 ){
			$file = BASE_PATH . '/backapi/uploads/'.$image.'.png';
			$content = file_get_contents('https://assets.b365api.com/images/team/b/'.$image.'.png');
			file_put_contents($file, $content);
			$model = Yii::createObject([
				'class' => FileUploadModel::class,
				'file' => $file,
				'fileExtension' => 'png',
			]);
			if ($model->upload()) {
				unlink($file);
				$result = $model->getResponse();
				return $result['fileName'];
			}
			return null;
		}
		return null;
	}

	protected function getShortParticipantName($name,$participantsValue,$participantsValuesFormat) {
		$nameArr = explode(' ',$name);
		$namesShort = [];
		$countNamesArr = count($nameArr);
		if($countNamesArr > 1){
			while($countNamesArr > 1){
				array_shift($nameArr);
				$countNamesArr--;
				if(($nameShort = trim(implode(' ',$nameArr))) != '' && (int)$nameShort == 0 && mb_strlen($nameShort) > 1 ){
					$namesShort[] = $nameShort;
					$this->participantsValues[] = $participantsValue;
					$this->participantsValuesFormat[] = $participantsValuesFormat;
				}
			}
			return $namesShort;
		}
		return null;
	}

	protected function getParticipantsNames($home,$away) {
		$participantsNames = [$home,$away,ucwords(strtolower($home),"()-|/ \t\r\n\f\v"),ucwords(strtolower($away),"()-|/ \t\r\n\f\v")];
		if(($homeShortNames = $this->getShortParticipantName($home, 'Home','{item1}')) !== null){
			$participantsNames =  array_merge($participantsNames, $homeShortNames);
		}
		if(($awayShortNames = $this->getShortParticipantName($away,'Away','{item2}')) !== null){
			$participantsNames =  array_merge($participantsNames, $awayShortNames);
		}
		return $participantsNames;
	}

	protected function getAlternativeNameOutcome($name) {
		$names = [$name];
		if(!empty($this->alternativeNameParticipants[$name])){
			return array_merge($names, $this->alternativeNameParticipants[$name]);
		}
		return $names;
	}

	protected function getMarket($params) {
		if(($marketApi = $this->findMarket(self::SOURCE_BETSAPI, $params['code'], $params['sports_id'])) !== null){
			$market_id = $marketApi->market_id;
			$group_id = $marketApi->group_id;
			return ['market_id' => $market_id, 'group_id' => $group_id];
		} else {
			if( strpos($params['name'], '4 1 2') !== false
				or strpos($params['name'], '0 15') !== false
				or strpos($params['name'], '0 15 30') !== false	){
				$params['name'] = str_replace(['4 1 2','0 15','0 15 30'], ['4 1/2','0/15','0/15/30'], $params['name']);
			}
			if(( $marketLocale = $this->findMarketByName([
					'name' => $params['name'],
					'sports_id' => $params['sports_id'],
					'lang' => Yii::$app->language,
				])) !== null){
				if(( $marketApi = $this->findMarketApiByMarketId([self::SOURCE_BETSAPI, $marketLocale->market_id])) !== null){
					$market_id = $marketApi->market_id;
					$group_id = $marketApi->group_id;
					$this->createApiMarket([
						'market_id' => $market_id,
						'group_id' => $group_id,
						'sports_id' => $params['sports_id'],
						'service_api' => self::SOURCE_BETSAPI,
						'code_api' => $params['code'],
					]);
					return ['market_id' => $market_id, 'group_id' => $group_id];
				}
			}

			// remove add new markets inplay - TODO: remove it for next step
			if($params['group_id'] !== null){
				$market_id = $this->createMarket([
					'title' => $params['name'],
					'lang' => Yii::$app->language,
					'service_api' => self::SOURCE_BETSAPI,
					'code_api' => $params['code'],
					'sports_id' => $params['sports_id'],
					'group_id' =>  $params['group_id'],
				]);
				return ['market_id' => $market_id, 'group_id' => $params['group_id']];
			}

		}
		return null;
	}

	protected function createUpdateOutcome($params) {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-bet365-service-outcome.log');
		}

		if(($outcome_id = $this->findOutcome(self::SOURCE_BETSAPI, $params['code'])) === null){
			if(($outcome_id = $this->findOutcomeByName([
					'names' => $this->getAlternativeNameOutcome($params['name']),
					'market_id' => $params['market_id'],
					'lang' => Yii::$app->language,
				])) !== null){
					$this->createApiOutcome([
						'outcome_id' => $outcome_id,
						'service_api' => self::SOURCE_BETSAPI,
						'code_api' => $params['code'],
					]);
			} 
		}

		if($outcome_id === null ){
			if($params['condition'] == ''){
				return false;
			}
			$outcome_id = $this->createOutcome([
				'title' => $params['name'],
				'lang' => Yii::$app->language,
				'service_api' => self::SOURCE_BETSAPI,
				'code_api' => $params['code'],
				'sports_id' => $params['sports_id'],
				'tournament_id' => $params['tournament_id'],
				'game_id' => $params['game_id'],
				'group_id' => $params['group_id'],
				'market_id' => $params['market_id'],
				'rate' => $params['rate'],
				'is_enabled' => $params['is_enabled'],
				'condition' => $params['condition'],
				]);
			return $outcome_id;
		} else {
			if(($eventOutcome = $this->findEventOutcome($params['game_id'], $outcome_id)) === null){
				if((float)$params['rate'] <= 1){
					return false;
				}
				$eventOutcome = $this->createEventOutcomes([
					'sports_id' => $params['sports_id'],
					'tournament_id' => $params['tournament_id'],
					'game_id' => $params['game_id'],
					'group_id' => $params['group_id'],
					'market_id' => $params['market_id'],
					'outcome_id' => $outcome_id,
					'rate' => $params['rate'],
					'is_enabled' => $params['is_enabled'],
				]);

			} else {
				if($params['method'] == 'inplay'){
					if( abs($params['rate'] - $eventOutcome->rate) < 0.001 && (int)$params['is_enabled'] == (int)$eventOutcome->is_enabled)
						return $outcome_id;

					//$this->inplayEventAllMarkets($this->event_FI);
					$this->eventChanged[$this->event_FI] = $this->event_FI;
				} else {
					if((float)$params['rate'] > 1){
						$eventOutcome->setRate($params['rate']);
					}
					if($eventOutcome->is_enabled != $params['is_enabled']){
						$eventOutcome->is_enabled = $params['is_enabled'];
						$eventOutcome->save(false);
					}
				}
			}
			return $outcome_id;
		}
		return false;
	}

	protected function inplaySport($item) {
		$this->sports_id = null;
		$this->tournament_id = null;
		$this->game_id = null;
		$this->sports_name = $item["NA"];

		if(($this->sports_id = $this->getSport($item['ID'], $item["NA"])) === null){
			// TODO add logs
			return false;
		}
		return true;
	}

	protected function inplayTournament($item) {
		$this->tournament_id = null;
		if($item['NA'] == 'Coupons')
			return false;

		if($this->sports_id === null)
			return false;

		if(($this->tournament_id = $this->getTournament([
			'sports_id' => $this->sports_id,
			'code_api' => $item['IT'],
			'service_api' => self::SOURCE_BETSAPI,
			'name' => $item["NA"],
		])) !== null){
			return true;
		}
		return false;
	}

	protected function inplayEvent($item) {

		$this->game_id = null;
		$this->event_FI = null;
		$participantsNames = [];

		if($item['ET'] > 0 )
			return false;

		if($this->tournament_id === null){
			if(($this->tournament_id = $this->getTournament([
				'sports_id' => $this->sports_id,
				'name' => $item["CT"],
				'code_api' => md5($item["CT"]),
				'service_api' => self::SOURCE_BETSAPI,
			])) === null){
				return false;
			}
		}

		$item["NA"] = BaseInflector::transliterate($item["NA"]);

		if( strpos($item["NA"], ' v ') !== false){
			$participantsNames = explode(' v ',$item["NA"]);
		} elseif(strpos($item["NA"], ' vs ') !== false) {
			$participantsNames = explode(' vs ',$item["NA"]);
		}
		$this->setParticipantsValuesDefault();
		$this->participantsNames = (count($participantsNames) == 2) ? $this->getParticipantsNames($participantsNames[0],$participantsNames[1]) : null;
		$this->event_FI = $item['FI'];

		if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $item['FI'])) !== null){
			if($gameApi->is_main_provider != true){
				return false;
			}
			$this->game_id =  $gameApi->game_id;
			$this->group_id = null;
			$this->market_id = null;
			$timer = null;
			$gamePeriod = !empty($item['CP']) ? (int)preg_replace("/[^0-9]/", '', $item['CP']) : 1;

			switch($this->sports_name){
				case 'Ice Hockey':
					if($item['CP'] == 'OT'){
						$timeMinutes = 60 + $item['TM'];
					} else {
						$timeMinutes = ($gamePeriod - 1) * 20 + $item['TM'];
					}
					$timer = ['m' => $timeMinutes, 's'=> $item['TS'], 'run' => (strpos($item['UC'], 'At ') !== false or strpos($item['UC'], 'Time out') !== false) ? 0 : 1, 'timeGetData'=> $this->timeGetData,'timeSend' => time()];
				case 'Futsal':
				case 'Floorball':
					$timeMinutes = ($gamePeriod - 1) * 20 + (20-$item['TM']);
					$timeSeconds =  ($item['TS'] > 0) ? 60 - $item['TS'] : 0;
					$timer = ['m' => $timeMinutes, 's'=> $item['TS'], 'run' => $item['TT'] ?? 0, 'timeGetData'=> $this->timeGetData,'timeSend' => time()];
				break;
				case 'Basketball':
				case 'American Football':
					$timeMinutes = ($gamePeriod - 1) * 10 + (10-$item['TM']);
					$timeSeconds =  ($item['TS'] > 0) ? 60 - $item['TS'] : 0;
					$timer = ['m' => $timeMinutes, 's'=> $timeSeconds, 'run' => $item['TT'] ?? 0, 'timeGetData'=> $this->timeGetData,'timeSend' => time()];
					//print_r($timer);
				break;
				case 'Water Polo':
					$timeMinutes = ($gamePeriod - 1) * 8 + (8-$item['TM']);
					$timeSeconds =  60 - $item['TS'];
					$timeSeconds =  ($item['TS'] > 0) ? 60 - $item['TS'] : 0;
					$timer = ['m' => $timeMinutes, 's'=> $timeSeconds, 'run' => $item['TT'] ?? 0, 'timeGetData'=> $this->timeGetData,'timeSend' => time()];
				break;
				case 'Handball':
				case 'Gaelic Sports':
					$timer = ['m' => $item['TM'], 's'=> $item['TS'], 'run' => (strpos($item['UC'], 'At ') !== false or strpos($item['UC'], 'Time out') !== false) ? 0 : 1, 'timeGetData'=> $this->timeGetData,'timeSend' => time()];
				break;
				case 'Rugby Union':
				case 'Rugby League':
					$timer = ['m' => $item['TM'], 's'=> $item['TS'], 'run' => $item['TT'] ?? 0, 'timeGetData'=> $this->timeGetData,'timeSend' => time()];
				break;
				default:
					return true;
				break;
			}
			$eventGame = EventGameModel::findOne($gameApi->game_id);
			$eventGame->setTimer($timer);
			return true;
		} else {
			if(($this->game_id = $this->findGameByName([
					'name' => $item["NA"],
					'sports_id' => $this->sports_id,
					'tournament_id' => $this->tournament_id,
					'lang' => Yii::$app->language,
				])) !== null){
					$this->createApiGame([
						'game_id' => $this->game_id,
						'sports_id' => $this->sports_id,
						'tournament_id' => $this->tournament_id,
						'service_api' => self::SOURCE_BETSAPI,
						'code_api' => $item['FI'],
						'api_event_id' => 0,
					]);
				return true;

			} else {
				return false;
				/*if($this->participantsNames === null){
					return false;
				}

				$participants = [];
				foreach($this->participantsNames as $participantName){
					if(($participant_id = $this->getParticipant([
						'sports_id' => $this->sports_id,
						'name' => $participantName,
						'code' => md5($participantName),
					])) !== null){
						$participants[] = $participant_id;
					} else {
						return false;
					}
				}

				$game_id = $this->createGame([
					'title' => $item["NA"],
					'starts_at' => null,
					'lang' => Yii::$app->language,
					'service_api' => self::SOURCE_BETSAPI,
					'code_api' => $item['ID'],
					'sports_id' =>  $this->sports_id,
					'tournament_id' => $this->tournament_id,
					'participants' => $participants,
				]);

				return true;*/
			}
		}
		return false;
	}

	protected function inplayMarket($item) {
		$this->group_id = null;
		$this->market_id = null;

		if($this->game_id === null)
			return false;

		$marketName = !empty(self::markets_name_live()[$item["NA"]]) ? self::markets_name_live()[$item["NA"]] : $item["NA"];

		if( strpos($marketName,'[gth]') !== false){
			$marketName = str_replace('[gth]', $item["GO"] ?? '' ,$marketName);
		}
		$marketName = str_replace('-',' ',$marketName);
		$marketCode = preg_replace("![^a-z0-9]+!i", "_", mb_strtolower($marketName));

		if(($marketData = $this->getMarket([
			'sports_id' => $this->sports_id,
			'code' => $marketCode,
			'name' => $marketName,
			'group_id' => $this->group_id,
		])) !== null){
			$this->group_id = $marketData['group_id'];
			$this->market_id = $marketData['market_id'];
			return true;
		}
		return false;
	}

	protected function inplayOutcome($item,$method = '') {
		$this->outcome_id = null;
		if($this->market_id === null or $this->participantsNames === null or empty($item["OD"]) or empty($item["NA"]))
			return false;

		$is_enabled = ($item["SU"] == 1) ? false : true;
		if(!empty($item["N2"])){
			$item["NA"] = $item["N2"];
		}
		$item["NA"] = BaseInflector::transliterate($item["NA"]);
		$item["NA"] = str_replace(' (Svr)',' ',$item["NA"]);

		//$outcomeName = str_ireplace($this->participantsNames, $this->participantsValuesFormat, $item["NA"]);
		$outcomeName = strtr(ucwords($item["NA"],"()-|/ \t\r\n\f\v"), array_combine($this->participantsNames, $this->participantsValuesFormat));

		if(!empty($item["HA"])){
		//var_dump($item["NA"]);
			if($item["HA"] == $item["NA"] && !empty($item["BS"])){
				$outcomeName = trim($item["BS"]).' '.$outcomeName;
			} else {
				$outcomeName .= ' '.$item["HA"];
			}
		}
		//print_r($item);
		$outcomeName = $this->setOutcomesNameFormat($outcomeName);
		$outcomeCode = md5($this->market_id . $outcomeName);
		$oddsArr = explode('/',$item["OD"]);
		$odds = (count($oddsArr) == 2 && $oddsArr[1] > 0) ? 1 + round($oddsArr[0] / $oddsArr[1], 3) : null;
		if($odds == null)
			$is_enabled = false;

		$outcome_id = $this->createUpdateOutcome([
			'code' => $outcomeCode,
			'name' => $outcomeName,
			'sports_id' => $this->sports_id,
			'tournament_id' => $this->tournament_id,
			'game_id' => $this->game_id,
			'group_id' => $this->group_id,
			'market_id' => $this->market_id,
			'rate' => $odds,
			'is_enabled' => $is_enabled,
			'condition' => null,
			'method' => $method
		]);
		$this->outcome_id = $outcome_id != false ? $outcome_id : null;
	}

	public function prematch ($jsonData) {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-bet365-service-prematch.log');
		}
		//$jsonData = file_get_contents('https://api.betsapi.com/v1/bet365/upcoming?sport_id=1&token=23807-8GffU48caA3vLs&page=1');
		$data =  Json::decode($jsonData);
		foreach($data['results'] as $event){
			$sports_id = $this->getSport($event['sport_id'], self::sports()[$event['sport_id']] );
			$this->processingPrematch([
				'sports_id' => $sports_id,
				'event' => $event,
				'service_api' => self::SOURCE_BETSAPI,
				'is_live' => false
			]);
		}
		return true;
	}

	public function participantImage($jsonData, $eventsCodeApi) {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-bet365-service-participant-image.log');
		}
		$data =  Json::decode($jsonData);
		foreach($data['results'] as $kEventId => $game){
			$transaction = Yii::$app->db->beginTransaction();
			try {
				if(isset($game['success']) && $game['success'] == 0){
					if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $eventsCodeApi[$kEventId])) !== null){
						$gameApi->is_checked = true;
						$gameApi->save(false);
					}
					$transaction->commit();
					Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.Event result empty');
					continue;
				}

				if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $eventsCodeApi[$kEventId])) !== null){

					$participants = EventParticipantsModel::find()->andWhere(['game_id' => $gameApi->game_id])->with([
						'participant' => function (GameParticipantsQuery $query) {
							return $query->all();
						},
						])->orderBy([
							'sort_order' => SORT_ASC,
							'id' => SORT_ASC,
						])->asArray()->all();

					if(count($participants) == 2){
						if(!empty($game['home']['image_id']) && empty($participants[0]['participant']['image']) ){
							$this->setApiParticipantImage([
								'sports_id' => $participants[0]['participant']['sports_id'],
								'participant_id' => $participants[0]['participant']['id'],
								'service_api' => self::SOURCE_BETSAPI,
								'code_image' => (string)$game['home']['image_id'],
							]);
						}
						if(!empty($game['away']['image_id']) && empty($participants[1]['participant']['image']) ){
							$this->setApiParticipantImage([
								'sports_id' => $participants[1]['participant']['sports_id'],
								'participant_id' => $participants[1]['participant']['id'],
								'service_api' => self::SOURCE_BETSAPI,
								'code_image' => (string)$game['away']['image_id'],
							]);
						}
					}
					$gameApi->is_checked = true;
					$gameApi->save(false);
				}
				$transaction->commit();
			} catch (\Throwable $e) {
				Yii::error($e);
				$transaction->rollBack();
			}
		}
	}

	public function event ($jsonData) {
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-bet365-service-event.log');
		}
		//$jsonData = file_get_contents(Yii::getAlias("@app/bet365_4/start_sp_1_3.json"));
		//$jsonData = file_get_contents("https://api.betsapi.com/v1/bet365/prematch?token=23807-64J9ST5D911ANZ&FI=83995742");
		$data =  Json::decode($jsonData);
		$marketsTeamPlayers = ['goalscorers', 'multi_scorers', 'team_goalscorer', '1st_player_booked', 'tryscorers', 'team_tryscorers', 'multi_tryscorers', 'player_to_be_sent_off'];

		foreach($data['results'] as $event){
			$eventHashArr = [];
			foreach($event as $marketGroupCode => $market_group){
				if(empty($market_group['sp']) or $marketGroupCode == 'schedule'){
					continue;
				}
				$eventHashArr[$marketGroupCode] = $market_group['sp'];
			}
			$eventHash = hash('adler32', Json::encode($eventHashArr));

			$transaction = Yii::$app->db->beginTransaction();
			try {

				if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $event['FI'])) === null){
					$transaction->commit();
					Yii::warning('Event id = '.$event['FI'].'.Event not found');
					continue;
				}
				if($gameApi->hash == $eventHash){
					$transaction->commit();
					continue;
				} else {
					$gameApi->hash = $eventHash;
					$gameApi->save(false);
				}

				$participants = EventParticipantsModel::find()->andWhere(['game_id' => $gameApi->game_id])->with([
					'participantLocale' => function (GameParticipantsLocaleQuery $query) {
						return $query->current();
					},
				])->orderBy([
					'sort_order' => SORT_ASC,
					'id' => SORT_ASC,
				])->asArray()->all();

				$this->participantsNames = null;
				$this->setParticipantsValuesDefault();

				if(count($participants) == 2){
					$this->participantsNames = $this->getParticipantsNames($participants[0]['participantLocale'][0]['name'],$participants[1]['participantLocale'][0]['name']);
					$participantsReplacement = array_combine($this->participantsNames, $this->participantsValues);
					$participantsReplacementFormat = array_combine($this->participantsNames, $this->participantsValuesFormat);
				}

				foreach($event as $marketGroupCode => $market_group){

					if(empty($market_group['sp']) or $marketGroupCode == 'schedule'){
						continue;
					}

					$marketGroupName = ucwords(str_replace('_',' ',$marketGroupCode),"()-|/ \t\r\n\f\v");

					if(($group_id = $this->findMarketGroup(self::SOURCE_BETSAPI, $marketGroupCode, $gameApi->sports_id)) === null){
						$group_id = $this->createMarketGroup([
							'title' => $marketGroupName,
							'lang' => Yii::$app->language,
							'service_api' => self::SOURCE_BETSAPI,
							'code_api' => $marketGroupCode,
							'sports_id' => $gameApi->sports_id,
						]);
					}

					foreach($market_group['sp'] as $marketKey => $market){

						$marketName = ucwords(str_replace('_',' ',$marketKey),"()-|/ \t\r\n\f\v");
						$is_match_lines = 0;
						if($marketName == 'Full Time Result'){
							$marketName	= 'Fulltime Result';
						} elseif( $marketKey == 'match_lines'){
							$is_match_lines = 1;
							$marketKey = 'to_win_match';
							$marketName	= 'To Win Match';
						} elseif( $marketKey == '1st_game'){
							$is_match_lines = 1;
						}

						if(trim($marketName) == ''){
							Yii::warning('Event id = '.$event['FI'].'.Market name error:'.$marketKey);
							continue;
						}

						if(($marketData = $this->getMarket([
							'sports_id' => $gameApi->sports_id,
							'code' => $marketKey,
							'name' => $marketName,
							'group_id' => $group_id,
						])) !== null){
							$market_id = $marketData['market_id'];
						} else {
							Yii::warning('Event id = '.$event['FI'].'.Market error:'.$marketKey);
							continue;
						}

						$marker_with_header_team = 0;

						foreach($market as $outcome){
							if(!empty($outcome['header'])){
								if($marker_with_header_team != 2 && ((int)$outcome['header'] == 1 or (int)$outcome['header'] == '2' or $outcome['header'] == 'X' or  $outcome['header'] == 'Neither')){
									$marker_with_header_team = 1;
								} else {
									$marker_with_header_team = 2;
								}
							}
						}
						$outcomesNames = [];
						foreach($market as $outcome){
							if(empty($outcome['odds']) && !empty($outcome['id']) && !empty($outcome['name'])){
								$outcomesNames[$outcome['id']]['name'] = $outcome['name'] .(!empty($outcome['header']) && trim($outcome['header']) != '' ? ' ('.$outcome['header'].')' : '');
							}
						}

						foreach($market as $outcome){

							if( $is_match_lines == 1 ){
								if(!empty($outcome['name']) && $outcome['name'] != 'To Win'){
									continue;
								} elseif($outcome['name'] == 'To Win' && !empty($outcome['header'])) {
									unset($outcome['name']);
								}
							}

							$outcomeValueArr = [];
							if(empty($outcome['odds'])){
								continue;
							}
							if(!empty($outcome['id']) && !empty($outcomesNames[$outcome['id']]) ){
								$outcome['name'] = $outcomesNames[$outcome['id']]['name'];
							}
							$odds = $outcome['odds'];
							unset($outcome['odds']);
							unset($outcome['id']);

							if(!$outcome){
								continue;
							}
							if($marketKey == 'team_tryscorers' && $outcome['header'] == 'Last'){
								continue;
							}

							foreach($outcome as $kParam => $valueParam){
								$outcome[$kParam] = BaseInflector::transliterate($valueParam);
							}

							if(isset($outcome['opp'])){
								//if( $marketKey != 'goalscorers' && $marketKey != 'multi_scorers' && $marketKey != 'team_goalscorer'){
									$outcomeOppName = trim(strtr(ucwords(strtolower($outcome['opp']),"()-|/ \t\r\n\f\v"),$participantsReplacementFormat));
								//}
									//$outcomeOppName = trim(ucwords(strtolower($outcome['opp']),"()-|/ \t\r\n\f\v"));
									$outcome['opp'] = trim(strtr(ucwords(strtolower($outcome['opp']),"()-|/ \t\r\n\f\v"),$participantsReplacement));
							}
							if(isset($outcome['header'])){
								$outcome['header'] = str_replace([' (Svr)',"’",'  '],['',"'",' '],$outcome['header']);
								$outcomeHeaderName = trim(strtr(ucwords(strtolower($outcome['header']),"()-|/ \t\r\n\f\v"),$participantsReplacementFormat));
								foreach($this->alternativeNameParticipants as $itemFormatName => $itemNames){
									if($itemNames == $outcomeHeaderName){
										$outcomeHeaderName = $itemFormatName;
									}
								}
								if($marker_with_header_team == 1){
									$outcomeHeaderName = str_replace(['1','2','X'], ['{item1}','{item2}','Draw'], $outcomeHeaderName);
								}
								$outcome['header'] = trim(strtr(ucwords(strtolower($outcome['header']),"()-|/ \t\r\n\f\v"),$participantsReplacement));
							}
							if(isset($outcome['name'])){
								$outcome['name'] = str_replace([' (Svr)',"’",'  '],['',"'",' '],$outcome['name']);
								if( array_search($marketKey, $marketsTeamPlayers) === false){
									$outcomeNameName = trim(strtr(ucwords(strtolower($outcome['name']),"()-|/ \t\r\n\f\v"),$participantsReplacementFormat));
								}else {
									if( strpos($outcome['name'], '(') !== false ){
										$outcomeNameArr = explode('(',$outcome['name']);
										$outcomeNameName = $outcomeNameArr[0].'('. strtr(ucwords(strtolower($outcomeNameArr[1]),"()-|/ \t\r\n\f\v"),$participantsReplacementFormat);
									} else {
										$outcomeNameName = trim($outcome['name']);
									}
								}

								//$outcomeNameName = trim(ucwords(strtolower($outcome['name']),"()-|/ \t\r\n\f\v"));
								$outcome['name'] = trim(strtr(ucwords(strtolower($outcome['name']),"()-|/ \t\r\n\f\v"),$participantsReplacement));

								if($marker_with_header_team == 1 && array_search($outcomeNameName,  ['{item1}','{item2}','Draw']) !== false ){
									$outcomeHeaderName = '';
								}
								/*if($outcome['name'] = 'Any Other Result'){
									$outcomeHeaderName = '';
								}*/
							}
							if(isset($outcome['n2'])){
								$outcomeNameN2 = trim(strtr(ucwords(strtolower($outcome['n2']),"()-|/ \t\r\n\f\v"),$participantsReplacementFormat));
								$outcome['n2'] = trim(strtr(ucwords(strtolower($outcome['n2']),"()-|/ \t\r\n\f\v"),$participantsReplacement));
							}

							$is_enabled = ($odds > 1) ? true : false;

							$outcomeName = isset($outcome['name']) ? $outcomeNameName : '';
							$outcomeName .= isset($outcome['n2']) ? ($outcomeName != '' ? ' ' : '').'('.$outcomeNameN2.')' : '';
							if($marketKey != 'game_total_odd_even'){
								$outcomeName .= isset($outcome['header']) ? ($outcomeName != '' ? ' ' : '').$outcomeHeaderName : '';
							}
							$outcomeName .= isset($outcome['goals']) ? ($outcomeName != '' ? ' ' : '').$outcome['goals'] : '';
							$outcomeName .= isset($outcome['opp']) ? ($outcomeName != '' ? ' ' : '').$outcomeOppName : '';
							$outcomeName .= (isset($outcome['handicap']) && trim($outcome['handicap']) != '' &&  (!isset($outcome['name']) or (isset($outcome['name']) && strpos($outcome['name'], $outcome['handicap']) === false)) ) ? ($outcomeName != '' ? ' ' : '').$outcome['handicap'] : '';

							//unset($outcome['name']);
							if($outcomeName != ''){
								$outcomeCondition = Json::encode($outcome);
								if( array_search($marketKey, $marketsTeamPlayers) === false){
									$outcomeName = $this->setOutcomesNameFormat($outcomeName);
								}
								$outcomeCode = md5($market_id . $outcomeCondition);

								$this->createUpdateOutcome([
									'code' => $outcomeCode,
									'name' => $outcomeName,
									'sports_id' => $gameApi->sports_id,
									'tournament_id' => $gameApi->tournament_id,
									'game_id' => $gameApi->game_id,
									'group_id' => $group_id,
									'market_id' => $market_id,
									'rate' => $odds,
									'is_enabled' => $is_enabled,
									'condition' => $outcomeCondition,
									'method'=> '',
								]);
							}

						}
					}
				}

				$transaction->commit();
			} catch (\Throwable $e) {
				Yii::error($e);
				$transaction->rollBack();
			}
			//exit();
		}
		return true;
	}

	public function result ($jsonData, $eventsCodeApi) {
		$this->timeGetData = time();
		$target = \Yii::$app->log->targets['errors'] ?? null;
		if($target instanceof FileTarget) {
			$target->logFile = \Yii::getAlias('@runtime/logs/api-bet365-service-result.log');
		}
		/*$eventsCodeApi = [82807217];
		$jsonData = file_get_contents('https://api.betsapi.com/v1/bet365/result?event_id=82807217&token=23807-8GffU48caA3vLs');*/
		$data =  Json::decode($jsonData);

		foreach($data['results'] as $kEventId => $game){
			$transaction = Yii::$app->db->beginTransaction();
			try {

				if(isset($game['success']) && $game['success'] == 0){
					//if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $game['id'])) !== null){
					if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $eventsCodeApi[$kEventId])) !== null){
						$eventGame = EventGameModel::findOne($gameApi->game_id);
						$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
						if($eventGame != null ){
							$eventGame->is_resulted = true;
							$eventGame->is_finished = 1;
							$eventGame->save();
						}
					}
					$transaction->commit();
					Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.Event result empty');
					continue;
				}

				if(empty($game['sport_id']) or ($this->sports_id = $this->getSport($game['sport_id'], self::sports()[$game['sport_id']])) === null){
					Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.Sport id empty');
					$transaction->commit();
					continue;
				}

				if($game['time_status'] == self::TIME_STATUS_NOT_STARTED){
					if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $eventsCodeApi[$kEventId])) !== null){
						$eventGame = EventGameModel::findOne($gameApi->game_id);
						$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
						if($eventGame != null && $eventGame->starts_at != $game['time'] &&  $game['time'] > 0){
							$eventGame->starts_at = (int)$game['time'];
							$eventGame->save(false);
						}
					}
					$transaction->commit();
					continue;
				}

				if(($gameApi = $this->findGame(self::SOURCE_BETSAPI, $eventsCodeApi[$kEventId])) === null){

					if(($tournament_id = $this->findTournamentByName([
							'name' => $game['league']['name'],
							'sports_id' => $this->sports_id,
							'lang' => Yii::$app->language,
						])) === null){
							Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.Tournament error:'.Json::encode($game['league']));
							$transaction->commit();
							continue;
						}

					//$game['home']['name'] = BaseInflector::transliterate($game['home']['name']);
					//$game['away']['name'] = BaseInflector::transliterate($game['away']['name']);

					$gameName = BaseInflector::transliterate($game['home']['name'] . ' - ' . $game['away']['name']);

					if(($this->game_id = $this->findGameByName([
							'name' => $gameName,
							'sports_id' => $this->sports_id,
							'tournament_id' => $tournament_id,
							'lang' => Yii::$app->language,
						])) === null){
							$transaction->commit();
							Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'. Event name:'.$gameName.'.Event not found');
							continue;
						}

				} else {
					$this->sports_id = $gameApi->sports_id;
					$tournament_id = $gameApi->tournament_id;
					$this->game_id = $gameApi->game_id;
				}
				if($game['league']['cc'] != null){
					$eventTournament = EventTournamentModel::findOne((int)$tournament_id);
					$eventTournament->scenario = \common\modules\games\models\EventTournamentModel::SCENARIO_UPDATE;
					if($eventTournament != null ){
						$eventTournament->flag = (string)$game['league']['cc'];
						$eventTournament->save();
					}
				}

				if($game['time_status'] == self::TIME_STATUS_RETIRED){
					$eventGame = EventGameModel::findOne((int)$this->game_id);
					$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
					if($eventGame != null ){
						$eventGame->is_finished = 1;
						$eventGame->save();
					}
					Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.Time status: TIME_STATUS_RETIRED');
					$transaction->commit();
					continue;
				}

				if($game['time_status'] == self::TIME_STATUS_INPLAY or $game['time_status'] == self::TIME_STATUS_ENDED){
					$resultExtended = [];
					$statistics = [];
					$resultShortSimple = '';
					$resultShort = '';
					$gamePeriod = '';
					$eventGame = EventGameModel::findOne((int)$this->game_id);
					$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
					if($eventGame != null ){
						switch(self::sports()[$game['sport_id']]){
							case 'Soccer':
								if(!empty( $game['timer'])){
									$timer = ['m' => $game['timer']['tm'], 's'=> $game['timer']['ts'], 'run' => $game['timer']['tt'] ?? 0, 'timeGetData'=> $this->timeGetData,'timeSend' => time()];
									$eventGame->setTimer($timer);
								}
								$resultShort = $game['ss'] ?? '';
								$resultShortSimple = $game['ss'] ? str_replace('-', ':', $game['ss']) : '';
								if(!empty($game['stats'])){
									$resultExtended['Yellow cards'] = $game['stats']['yellowcards'] ?? null;
									$resultExtended['Red cards'] = $game['stats']['redcards'] ?? null;
									if($game['time_status'] == self::TIME_STATUS_ENDED){
										$statistics = [
											'Ball possession' => $game['stats']['possession_rt'] ?? null,
											'Goal attempts' => $game['stats']['goalattempts'] ?? null,
											'Shots on target' => $game['stats']['on_target'] ?? null,
											'Shots off target' => $game['stats']['off_target'] ?? null,
											'Shots blocked' => $game['stats']['shots_blocked'] ?? null,
											'Corners' => $game['stats']['corners'] ?? null,
											'Offsides' => $game['stats']['offsides'] ?? null,
											'Saves' => $game['stats']['saves'] ?? null,
											'Fouls' => $game['stats']['fouls'] ?? null,
											'Penalties' => $game['stats']['penalties'] ?? null,
											'Ball Safe' => $game['stats']['ball_safe'] ?? null,
											'Injuries' => $game['stats']['injuries'] ?? null,
											'Substitutions' => $game['stats']['substitutions'] ?? null,
											'Attacks' => $game['stats']['attacks'] ?? null,
											'Dangerous Attacks' => $game['stats']['dangerous_attacks'] ?? null,
											'Yellow Cards' => $game['stats']['yellowcards'] ?? null,
											'Red Cards' => $game['stats']['redcards'] ?? null,
										];
									}
								}
								if(!empty($game['scores'])){
									$resultShort .= ' [';
									if(!empty($game['scores'][1]) && !empty($game['scores'][2])){
										$resultShort .= implode('-',$game['scores'][1]);
										$resultExtended['1/2'] = array_values($game['scores'][1]);
										$gameScores2 = [
											'home' => $game['scores'][2]['home'] - $game['scores'][1]['home'],
											'away' => $game['scores'][2]['away'] - $game['scores'][1]['away'],
										];
										$resultShort .= ', '.implode('-',$gameScores2);
										$resultExtended['2/2'] = array_values($gameScores2);
										$gamePeriod = '2 Half';
									} elseif(!empty($game['scores'][2])) {
										$resultShort .= implode('-',$game['scores'][2]);
										$resultExtended['1/2'] = array_values($game['scores'][2]);
										$gamePeriod = '1 Half';
									}
									$resultShort .= ']';
									if(!empty($game['stats'])){
										$resultExtended['Corners'] = $game['stats']['corners'] ?? null;
										$resultExtended['Penalties'] = $game['stats']['penalties'] ?? null;
									}
								}
							break;
							case 'Basketball':
							case 'American Football':
								$resultShort = $game['ss'] ?? '';
								$resultShortSimple = $game['ss'] ? str_replace('-', ':', $game['ss']) : '';
								if(!empty($game['stats']) && $game['time_status'] == self::TIME_STATUS_ENDED && self::sports()[$game['sport_id']] == 'Basketball'){
									$statistics = [
										'2-point field goals scored' => $game['stats']['2points'] ?? null,
										'3-point field goals scored' => $game['stats']['3points'] ?? null,
										'Free throws scored' => $game['stats']['free_throws'] ?? null,
										'Free throws %' => $game['stats']['free_throws_rate'] ?? null,
										'Biggest lead' => $game['stats']['biggest_lead'] ?? null,
										'Fouls' => $game['stats']['fouls'] ?? null,
										'Possession %' => $game['stats']['possession'] ?? null,
										'Successful Attempts' => $game['stats']['success_attempts'] ?? null,
										'Time Outs' => $game['stats']['time_outs'] ?? null,
										'Lead changes' => $game['stats']['lead_changes'] ?? null,
										'Max Points in a Row' => $game['stats']['maxpoints_inarow'] ?? null,
										'Time spent in lead' => $game['stats']['timespent_inlead'] ?? null,
									];
								}
								if(!empty($game['scores'])){
									$resultShort .= ' [';
									$i = 0;
									foreach($game['scores'] as $scoreKey => $score){
										if(array_search($scoreKey, [1,2,4,5]) !== false){
											$i++;
											$resultShort .= (($i > 1) ? ', ' : '').implode('-',$score);
											$resultExtended[$i.'/4'] = array_values($score);
											$gamePeriod = $i.' Quarter';
										}
									}
									$resultShort .= ']';
								}
							break;
							case 'Ice Hockey':
								$resultShort = $game['ss'] ?? '';
								$resultShortSimple = $game['ss'] ? str_replace('-', ':', $game['ss']) : '';
								if(!empty($game['stats']) && $game['time_status'] == self::TIME_STATUS_ENDED ){
									$statistics = [
										'Shots' => $game['stats']['shots'] ?? null,
										'Penalties' => $game['stats']['penalties'] ?? null,
										'Goals on Power Play' => $game['stats']['goals_on_power_play'] ?? null,
										'-' => $game['stats']['s7'] ?? null,
									];
								}
								if(!empty($game['scores'])){
									$resultShort .= ' [';
									ksort($game['scores']);
									foreach($game['scores'] as $scoreKey => $score){
										if($scoreKey > 3){
											break;
										}
										$resultShort .= (($scoreKey > 1) ? ', ' : '').implode('-',$score);
										$resultExtended[$scoreKey.''] = array_values($score);
										$gamePeriod = $scoreKey.' Period';
									}
									$resultShort .= ']';
								}
							break;
							case 'Handball':
								$resultShort = $game['ss'] ?? '';
								$resultShortSimple = $game['ss'] ? str_replace('-', ':', $game['ss']) : '';
								if(!empty($game['stats']) && $game['time_status'] == self::TIME_STATUS_ENDED ){
									$statistics = [
										'Last 10 Mins Score' => $game['stats']['last_10_mins_score'] ?? null,
										'Possession %' => $game['stats']['possession'] ?? null,
									];
								}
								if(!empty($game['scores'])){
									$resultShort .= ' [';
									ksort($game['scores']);
									foreach($game['scores'] as $scoreKey => $score){
										if($scoreKey > 2){
											break;
										}
										$resultShort .= (($scoreKey > 1) ? ', ' : '').implode('-',$score);
										$resultExtended[$scoreKey.'/2'] = array_values($score);
										$gamePeriod = $scoreKey.' Half';
									}
									$resultShort .= ']';
								}
							break;
							case 'Baseball':
								$resultShort = $game['ss'] ?? '';
								$resultShortSimple = $game['ss'] ? str_replace('-', ':', $game['ss']) : '';
								if(!empty($game['scores'])){
									$statistics = [
										'Hit' => $game['scores']['hit'] ?? null,
										'Run' => $game['scores']['run'] ?? null,
									];
									$resultShort .= ' [';
									ksort($game['scores']);
									for($i = 1;$i <=9; $i++){
										if(!empty($game['scores'][$i]) && ( $game['scores'][$i]['home'] != '' or $game['scores'][$i]['away'] != '') ){
											$game['scores'][$i]['away'] = (int)$game['scores'][$i]['away'];
											$resultShort .= (($i > 1) ? ', ' : '').implode('-',$game['scores'][$i]);
											$resultExtended[$i.''] = array_values($game['scores'][$i]);
											$gamePeriod = $i.' Inning';
										}
									}
									$resultShort .= ']';
								}
							break;
							case 'Snooker':
								$resultShort = $game['ss'] ?? '';
								$resultShortSimple = $game['ss'] ? str_replace('-', ':', $game['ss']) : '';
								if(!empty($game['events'])){
									$resultShort .= ' [';
									$i = 1;
									foreach($game['events'] as $event){
										if( preg_match('/(?<home>\d+)-(?<away>\d+)/', $event['text'], $matches)){
											$resultShort .=(($i > 1) ? ', ' : ''). $matches['home'] .'-'. $matches['away'];
											$resultExtended[$i.''] = [0 => $matches['home'], 1 => $matches['away']];
											$gamePeriod = $i.' Frame';
											$i++;
										}
									}
									$resultShort .= ']';
								}
							break;
							case 'Rugby League':
							case 'Rugby Union':
								$resultShort = $game['ss'] ?? '';
								$resultShortSimple = $game['ss'] ? str_replace('-', ':', $game['ss']) : '';
								if(!empty($game['events'])){
									$resultShort .= ' [';
									$scoreFirstHalf = null;
									foreach($game['events'] as $event){
										if( strpos($event['text'], 'Score at the end of First Half') !== false 
											or strpos($event['text'], 'Score After First Half') !== false ){
											if( preg_match('/(?<home>\d+)-(?<away>\d+)/', $event['text'], $matches)){
												$scoreFirstHalf = [0 => $matches['home'], 1 => $matches['away']];
											}
										}
									}
									if($scoreFirstHalf == null){
										$resultShort .= $game['ss'] ?? '';
										$resultExtended['1/2'] = explode('-',$game['ss']);
										$gamePeriod = '1 Half';
									} elseif(!empty($game['ss'])) {
										$resultShort .= implode('-',$scoreFirstHalf);
										$scoreFullTime = explode('-',$game['ss']);
										$score2ndHalf = [
											'home' => (int)$scoreFullTime[0] - (int)$scoreFirstHalf[0],
											'away' => (int)$scoreFullTime[1] - (int)$scoreFirstHalf[1]
										];
										$resultShort .= ', '.implode('-',$score2ndHalf);
										$resultExtended['2/2'] = array_values($score2ndHalf);
										$gamePeriod = '2 Half';
									}
									$resultShort .= ']';
								}
							break;
							case 'Volleyball':
							case 'Table Tennis':
							case 'Badminton':
								$resultShort = '';
								$scoreFullTime = [
									'home' => 0,
									'away' => 0
								];
								if(!empty($game['stats']) && $game['time_status'] == self::TIME_STATUS_ENDED && self::sports()[$game['sport_id']] == 'Volleyball'){
									$statistics = [
										'Points Won on Serve' => $game['stats']['points_won_on_serve'] ?? null,
										'Longest Streak' => $game['stats']['longest_streak'] ?? null,
									];
								}
								if(!empty($game['scores'])){
									$curentScores = count($game['scores']);
									$resultShort .= ' [';
									ksort($game['scores']);
									foreach($game['scores'] as $scoreKey => $score){
										$resultShort .= (($scoreKey > 1) ? ', ' : '').implode('-',$score);
										$resultExtended[$scoreKey.''] = array_values($score);
										$gamePeriod = $scoreKey.' Set';
										if($game['time_status'] == self::TIME_STATUS_ENDED  or ($game['time_status'] == self::TIME_STATUS_INPLAY && $curentScores > $scoreKey) ){
											$score['home'] > $score['away'] ? $scoreFullTime['home']++ : $scoreFullTime['away']++;
										}
									}
									$resultShort .= ']';
									$resultShort = implode('-',$scoreFullTime).$resultShort;
									$resultShortSimple = implode(':',$scoreFullTime);
								}
							break;
							case 'Tennis':
								$resultShort = '';
								$scoreFullTime = [
									'home' => 0,
									'away' => 0
								];
								if(!empty($game['stats']) && $game['time_status'] == self::TIME_STATUS_ENDED ){
									$statistics = [
										'Aces' => $game['stats']['aces'] ?? null,
										'Double Faults' => $game['stats']['double_faults'] ?? null,
										'Win % 1st Serve' => $game['stats']['win_1st_serve'] ?? null,
										'Break Point Conversions' => $game['stats']['break_point_conversions'] ?? null,
									];
								}
								if(!empty($game['points'])){
									$resultShort = '('.$game['points'].') ';
									$resultExtended['Points'] = explode('-',$game['points']);
								}
								if(!empty($game['scores'])){
									$curentScores = count($game['scores']);
									ksort($game['scores']);
									foreach($game['scores'] as $scoreKey => $score){
										$resultExtended[$scoreKey.' Set'] = array_values($score);
										$gamePeriod = $scoreKey.' Set';
										if($game['time_status'] == self::TIME_STATUS_ENDED  or ($game['time_status'] == self::TIME_STATUS_INPLAY && $curentScores > $scoreKey) ){
											$score['home'] > $score['away'] ? $scoreFullTime['home']++ : $scoreFullTime['away']++;
										}
									}
									$resultShort .= implode('-',$scoreFullTime);
									$resultShortSimple = implode(':',$scoreFullTime);
								}
								$resultShort .= ' ['.$game['ss'].']' ?? '';
							break;
							case 'Cricket':
								if(!empty($game['ss'])){
									if(strpos($game['ss'], '-') !== false){
										$scoreArr = explode('-',$game['ss']);
									} else {
										$scoreArr = explode(',',$game['ss']);
										foreach($scoreArr as $kScore => $scoreTeam){
											if(trim($scoreTeam) != ''){
												$scoreTeam = str_replace(' All Out','',$scoreTeam);
												$scoreTeamArr = explode('/',$scoreTeam);
												$scoreArr[$kScore] = $scoreTeamArr[0];
											} else {
												$scoreArr[$kScore] = 0;
											}
										}
									}
									$resultShort = implode('-',$scoreArr);
									$resultShortSimple = implode(':',$scoreArr);
								}
							break;
							case 'Gaelic Sports':
								if(!empty($game['ss']) && strpos($game['ss'], '-') !== false){
									$scoreArr = explode('-',$game['ss']);
									preg_match('#\((.*?)\)#', $scoreArr[0], $match);
									$scoreHome = (int)$match[1];
									preg_match('#\((.*?)\)#', $scoreArr[1], $match);
									$scoreAway = (int)$match[1];
									$resultShort = $scoreHome . '-' .$scoreAway;
									$resultShortSimple = $scoreHome . ':' .$scoreAway;
								}
							break;
							default:
								$resultShort = $game['ss'] ?? '';
								$resultShortSimple = $game['ss'] ? str_replace(['-','/'], ':', $game['ss']) : '';
							break;
						}
						$eventGame->updateResult([
							'resultShort' => $resultShort,
							'timer' => '',
							'resultShortSimple' => $resultShortSimple,
							'resultExtended' => Json::encode($resultExtended),
							'gamePeriod' => $gamePeriod,
							'statistics' => Json::encode($statistics)
						]);
					}
					if($game['time_status'] == self::TIME_STATUS_INPLAY){
						$transaction->commit();
						continue;
					}
					if($game['time_status'] == self::TIME_STATUS_ENDED && ($eventGame->starts_at + 900) > time() ){
						$transaction->commit();
						Yii::warning('Event id = '.$eventsCodeApi[$kEventId].'.TIME_STATUS_ENDED.Start of event less than 15 min.');
						continue;
					}
				}

				if(($sportsResultService = $this->getSportsResultService(self::sports()[$game['sport_id']])) === null){
					// TODO add logs
					$transaction->commit();
					continue;
				}

				if($game['time_status'] == self::TIME_STATUS_ENDED){
					$success = (new $sportsResultService($game, $this->sports_id, $this->game_id))->processingResults();
				}

				/* TODO time status
					const TIME_STATUS_POSTPONED = 4;
					const TIME_STATUS_CANCELLED = 5;
					const TIME_STATUS_WALKOVER = 6;
					const TIME_STATUS_INTERRUPTED = 7;
					const TIME_STATUS_ABANDONED = 8;
					const TIME_STATUS_REMOVED = 99;
				*/

				$eventGame = EventGameModel::findOne((int)$this->game_id);
				$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
				if($eventGame != null ){
					$eventGame->is_resulted = true;
					$eventGame->is_finished = 1;
					$eventGame->save();
				}

				$transaction->commit();
			} catch (\Throwable $e) {
				Yii::error($e);
				$transaction->rollBack();
			}
		}
		return true;
	}
}

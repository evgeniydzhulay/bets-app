<?php

$markets1xbet = [
	"1"=>[
		"N"=>"1x2",
		"C"=>3
	],
	"2"=>[
		"N"=>"Handicap",
		"C"=>2
	],
	"4"=>[
		"N"=>"Half : 1x2",
		"C"=>3
	],
	"5"=>[
		"N"=>"Scored Goal",
		"C"=>2
	],
	"6"=>[
		"N"=>"Half : Over/Under",
		"C"=>2
	],
	"7"=>[
		"N"=>"Correct Score",
		"C"=>3
	],
	"8"=>[
		"N"=>"Double Chance",
		"C"=>3
	],
	"9"=>[
		"N"=>"Draw",
		"C"=>2
	],
	"10"=>[
		"N"=>"Result + Total",
		"C"=>3
	],
	"11"=>[
		"N"=>"HT-FT",
		"C"=>3
	],
	"12"=>[
		"N"=>"Goal Interval - Yes",
		"C"=>1
	],
	"13"=>[
		"N"=>"Goal Interval - No",
		"C"=>1
	],
	"14"=>[
		"N"=>"Even/Odd",
		"C"=>2
	],
	"15"=>[
		"N"=>"Total 1",
		"C"=>2
	],
	"16"=>[
		"N"=>"Try Scored",
		"C"=>3
	],
	"17"=>[
		"N"=>"Total",
		"C"=>2
	],
	"18"=>[
		"N"=>"Scores In Each Half",
		"C"=>3
	],
	"19"=>[
		"N"=>"Both Teams To Score",
		"C"=>2
	],
	"20"=>[
		"N"=>"Next Goal",
		"C"=>3
	],
	"21"=>[
		"N"=>"Game Scoreline",
		"C"=>2
	],
	"22"=>[
		"N"=>"Win In The Game",
		"C"=>2
	],
	"23"=>[
		"N"=>"Win In The Set",
		"C"=>2
	],
	"24"=>[
		"N"=>"Win In The Inning",
		"C"=>3
	],
	"25"=>[
		"N"=>"Penalty Shots",
		"C"=>2
	],
	"26"=>[
		"N"=>"Winner",
		"C"=>2
	],
	"27"=>[
		"N"=>"Handicap 3 Way",
		"C"=>3
	],
	"28"=>[
		"N"=>"Total Interval",
		"C"=>1
	],
	"29"=>[
		"N"=>"Individual Total Interval - 1",
		"C"=>2
	],
	"30"=>[
		"N"=>"Individual Total Interval - 2",
		"C"=>2
	],
	"32"=>[
		"N"=>"Goal In Both Halves",
		"C"=>2
	],
	"33"=>[
		"N"=>"First Goal",
		"C"=>3
	],
	"34"=>[
		"N"=>"Second Goal",
		"C"=>3
	],
	"37"=>[
		"N"=>"Team 1 To Score Their 1st Goal",
		"C"=>3
	],
	"38"=>[
		"N"=>"Team 2 To Score Their 1st Goal",
		"C"=>3
	],
	"50"=>[
		"N"=>"Penalty Awarded",
		"C"=>2
	],
	"51"=>[
		"N"=>"Penalties",
		"C"=>2
	],
	"52"=>[
		"N"=>"First Substitution",
		"C"=>1
	],
	"53"=>[
		"N"=>"Team 1 Total HT-FT",
		"C"=>1
	],
	"54"=>[
		"N"=>"Team 2 Total HT-FT",
		"C"=>1
	],
	"55"=>[
		"N"=>"Run Of Play",
		"C"=>1
	],
	"56"=>[
		"N"=>"1st Half Or Match: 1х2",
		"C"=>3
	],
	"57"=>[
		"N"=>"Bets On The Game",
		"C"=>2
	],
	"58"=>[
		"N"=>"Tie-Break",
		"C"=>2
	],
	"59"=>[
		"N"=>"Finish In Top 3",
		"C"=>2
	],
	"60"=>[
		"N"=>"Race To",
		"C"=>3
	],
	"61"=>[
		"N"=>"Own Goal",
		"C"=>2
	],
	"62"=>[
		"N"=>"Total 2",
		"C"=>2
	],
	"65"=>[
		"N"=>"Team 1 To Score Their 1st Goal",
		"C"=>1
	],
	"67"=>[
		"N"=>"Team 2 To Score Their 1st Goal",
		"C"=>1
	],
	"69"=>[
		"N"=>"Exact Points Difference",
		"C"=>2
	],
	"71"=>[
		"N"=>"Half/Half",
		"C"=>1
	],
	"73"=>[
		"N"=>"Period Of First Goal",
		"C"=>1
	],
	"75"=>[
		"N"=>"Goal In Half",
		"C"=>3
	],
	"77"=>[
		"N"=>"Goal In Time Interval - Team 1",
		"C"=>1
	],
	"79"=>[
		"N"=>"Goal In Time Interval - Team 2",
		"C"=>1
	],
	"80"=>[
		"N"=>"Group Qualification",
		"C"=>2
	],
	"81"=>[
		"N"=>"Goal In Time Interval - Team 1 - Yes/No",
		"C"=>1
	],
	"83"=>[
		"N"=>"Goal In Time Interval - Team 2 - Yes/No",
		"C"=>1
	],
	"85"=>[
		"N"=>"Will There Be Shootouts? - Yes/No",
		"C"=>2
	],
	"86"=>[
		"N"=>"First X Minutes Result",
		"C"=>3
	],
	"87"=>[
		"N"=>"3Way Total",
		"C"=>3
	],
	"88"=>[
		"N"=>"Individual 3Way Total 1",
		"C"=>3
	],
	"89"=>[
		"N"=>"Individual 3Way Total 2",
		"C"=>3
	],
	"90"=>[
		"N"=>"Will There Be Overtime? - Yes/No",
		"C"=>2
	],
	"91"=>[
		"N"=>"Individual Total 1 Even/Odd",
		"C"=>2
	],
	"92"=>[
		"N"=>"Individual Total 2 Even/Odd",
		"C"=>2
	],
	"93"=>[
		"N"=>"Clean Sheet - Yes/No",
		"C"=>2
	],
	"95"=>[
		"N"=>"Scoring Periods",
		"C"=>2
	],
	"96"=>[
		"N"=>"Goal Up To Minute",
		"C"=>2
	],
	"97"=>[
		"N"=>"Shootout Win",
		"C"=>2
	],
	"99"=>[
		"N"=>"Asian Total",
		"C"=>2
	],
	"100"=>[
		"N"=>"To Qualify",
		"C"=>1
	],
	"101"=>[
		"N"=>"Team Wins",
		"C"=>2
	],
	"102"=>[
		"N"=>"End Result",
		"C"=>3
	],
	"103"=>[
		"N"=>"Time Interval Result",
		"C"=>1
	],
	"104"=>[
		"N"=>"How Goal Will Be Scored",
		"C"=>1
	],
	"105"=>[
		"N"=>"Result For The Current Minute",
		"C"=>3
	],
	"106"=>[
		"N"=>"When The Winner Will Be Determined",
		"C"=>1
	],
	"108"=>[
		"N"=>"Total Goal Minutes",
		"C"=>1
	],
	"109"=>[
		"N"=>"Sets Handicap",
		"C"=>2
	],
	"110"=>[
		"N"=>"Exact Total Sets",
		"C"=>2
	],
	"111"=>[
		"N"=>"To Reach The Final - Yes/No",
		"C"=>2
	],
	"112"=>[
		"N"=>"Thousandths Of A Second",
		"C"=>1
	],
	"116"=>[
		"N"=>"Which Team Will Score Goals?",
		"C"=>3
	],
	"117"=>[
		"N"=>"Who Will Score The First Goal In The Match?",
		"C"=>1
	],
	"118"=>[
		"N"=>"Player To Score Last Goal",
		"C"=>1
	],
	"119"=>[
		"N"=>"Who Will Score Goal",
		"C"=>1
	],
	"123"=>[
		"N"=>"Who Will Win? - Yes/No",
		"C"=>2
	],
	"124"=>[
		"N"=>"Set Score",
		"C"=>2
	],
	"127"=>[
		"N"=>"Team To Score In Each Period - Yes/No",
		"C"=>2
	],
	"130"=>[
		"N"=>"Goal In Time Interval",
		"C"=>1
	],
	"131"=>[
		"N"=>"Will Win Point In Order",
		"C"=>3
	],
	"132"=>[
		"N"=>"Multi Corner",
		"C"=>2
	],
	"133"=>[
		"N"=>"10 Minute Total",
		"C"=>2
	],
	"134"=>[
		"N"=>"Goal After Minute - Yes/No",
		"C"=>2
	],
	"135"=>[
		"N"=>"Points",
		"C"=>2
	],
	"136"=>[
		"N"=>"Correct Score",
		"C"=>3
	],
	"137"=>[
		"N"=>"Tie-Break Winner",
		"C"=>2
	],
	"138"=>[
		"N"=>"Tie-Break Total",
		"C"=>2
	],
	"139"=>[
		"N"=>"Tie-Break Score",
		"C"=>1
	],
	"141"=>[
		"N"=>"End Winner",
		"C"=>3
	],
	"143"=>[
		"N"=>"Leader After Total Points Scored",
		"C"=>3
	],
	"145"=>[
		"N"=>"Extra Points",
		"C"=>2
	],
	"147"=>[
		"N"=>"Score After Sets",
		"C"=>2
	],
	"148"=>[
		"N"=>"Time Interval Total",
		"C"=>2
	],
	"150"=>[
		"N"=>"Number Of Correct Outcomes TOTO",
		"C"=>2
	],
	"154"=>[
		"N"=>"Last Goal",
		"C"=>2
	],
	"158"=>[
		"N"=>"Substitute Player To Score",
		"C"=>2
	],
	"160"=>[
		"N"=>"A Player Scores Two Goals (Brace)",
		"C"=>2
	],
	"162"=>[
		"N"=>"A Player Scores A Hat-Trick",
		"C"=>2
	],
	"164"=>[
		"N"=>"Loss Without Scoring",
		"C"=>2
	],
	"166"=>[
		"N"=>"Come From Behind And Win",
		"C"=>2
	],
	"168"=>[
		"N"=>"Total Breaks",
		"C"=>2
	],
	"170"=>[
		"N"=>"Overtime Win",
		"C"=>2
	],
	"172"=>[
		"N"=>"Highest Scoring Quarter Total",
		"C"=>2
	],
	"174"=>[
		"N"=>"Lowest Scoring Quarter Total",
		"C"=>2
	],
	"176"=>[
		"N"=>"Highest Scoring Period",
		"C"=>2
	],
	"178"=>[
		"N"=>"First Foul",
		"C"=>2
	],
	"180"=>[
		"N"=>"First Rebound",
		"C"=>2
	],
	"182"=>[
		"N"=>"Total Sets",
		"C"=>2
	],
	"184"=>[
		"N"=>"First Substitution - Team",
		"C"=>2
	],
	"186"=>[
		"N"=>"First Throw-in - Team",
		"C"=>2
	],
	"188"=>[
		"N"=>"First Goal Kick - Team",
		"C"=>2
	],
	"190"=>[
		"N"=>"Added Time Total",
		"C"=>2
	],
	"204"=>[
		"N"=>"Offsides - Match Total",
		"C"=>2
	],
	"206"=>[
		"N"=>"Substitutions - Match Total",
		"C"=>2
	],
	"214"=>[
		"N"=>"First Foul From Minute To Minute",
		"C"=>2
	],
	"220"=>[
		"N"=>"To Score A Short-Handed Goal",
		"C"=>2
	],
	"222"=>[
		"N"=>"To Score An Empty Net Goal",
		"C"=>2
	],
	"224"=>[
		"N"=>"Total Goals Scored During Powerplay",
		"C"=>2
	],
	"226"=>[
		"N"=>"First Penalty",
		"C"=>3
	],
	"228"=>[
		"N"=>"Highest Scoring Half",
		"C"=>2
	],
	"230"=>[
		"N"=>"Which Team Will Be The First To Score Points?",
		"C"=>2
	],
	"232"=>[
		"N"=>"Team To Win All Quarters",
		"C"=>2
	],
	"234"=>[
		"N"=>"Team To Win Both Halves",
		"C"=>2
	],
	"236"=>[
		"N"=>"Team To Win 1st Half And The Match",
		"C"=>2
	],
	"238"=>[
		"N"=>"Team To Win 1st Quarter And The Match",
		"C"=>2
	],
	"240"=>[
		"N"=>"2-Point Field Goal Percentage - Team",
		"C"=>2
	],
	"242"=>[
		"N"=>"3-Point Field Goal Percentage",
		"C"=>2
	],
	"244"=>[
		"N"=>"Free Throws Percentage",
		"C"=>2
	],
	"248"=>[
		"N"=>"Goal To Be Scored With A Header",
		"C"=>2
	],
	"250"=>[
		"N"=>"Total Goal Minutes",
		"C"=>2
	],
	"256"=>[
		"N"=>"Post-Match Penalty Shootout",
		"C"=>2
	],
	"258"=>[
		"N"=>"Who Will Kick Off The Match?",
		"C"=>2
	],
	"260"=>[
		"N"=>"Break",
		"C"=>2
	],
	"262"=>[
		"N"=>"Match Point Total",
		"C"=>2
	],
	"264"=>[
		"N"=>"Match Will Be Won By An Ace",
		"C"=>2
	],
	"266"=>[
		"N"=>"Player To Save Match Point And Win The Match",
		"C"=>2
	],
	"268"=>[
		"N"=>"Break in 1st or 2nd Game",
		"C"=>2
	],
	"270"=>[
		"N"=>"Number Of Set Points",
		"C"=>2
	],
	"272"=>[
		"N"=>"Number Of Points In First Game",
		"C"=>2
	],
	"274"=>[
		"N"=>"First Serve Percentage",
		"C"=>2
	],
	"275"=>[
		"N"=>"Any Team To Win By",
		"C"=>2
	],
	"279"=>[
		"N"=>"First Corner In X To Y Minute",
		"C"=>2
	],
	"281"=>[
		"N"=>"Draw And Total Corners Under/Over",
		"C"=>2
	],
	"283"=>[
		"N"=>"Team To Win And Total Corners Under/Over",
		"C"=>2
	],
	"285"=>[
		"N"=>"Total Each Team Will Score Under/Over",
		"C"=>2
	],
	"291"=>[
		"N"=>"Team To Score N Goals",
		"C"=>2
	],
	"295"=>[
		"N"=>"First Substitution In Half",
		"C"=>3
	],
	"297"=>[
		"N"=>"Total Goals",
		"C"=>2
	],
	"299"=>[
		"N"=>"Win And Total",
		"C"=>3
	],
	"301"=>[
		"N"=>"First To Happen",
		"C"=>1
	],
	"303"=>[
		"N"=>"Result In Minute",
		"C"=>3
	],
	"305"=>[
		"N"=>"Win Or Draw In Minute",
		"C"=>3
	],
	"307"=>[
		"N"=>"Handicap In Minute",
		"C"=>2
	],
	"309"=>[
		"N"=>"Total In Minute",
		"C"=>2
	],
	"311"=>[
		"N"=>"Official Added Time Total",
		"C"=>2
	],
	"317"=>[
		"N"=>"Corners To Be Taken From All Four Corners Of The Field",
		"C"=>2
	],
	"319"=>[
		"N"=>"Goal In Each Period",
		"C"=>2
	],
	"323"=>[
		"N"=>"Team Not To Lose Any Periods",
		"C"=>2
	],
	"325"=>[
		"N"=>"Team Will Score A Goal In Each Period",
		"C"=>2
	],
	"327"=>[
		"N"=>"Penalty Shootout Win",
		"C"=>2
	],
	"329"=>[
		"N"=>"All Periods - Total Under",
		"C"=>2
	],
	"331"=>[
		"N"=>"All Periods - Total Over",
		"C"=>2
	],
	"333"=>[
		"N"=>"Total Periods",
		"C"=>2
	],
	"335"=>[
		"N"=>"Team Offside",
		"C"=>2
	],
	"337"=>[
		"N"=>"First Offside From X to Y Minute",
		"C"=>2
	],
	"339"=>[
		"N"=>"TOTO Total",
		"C"=>1
	],
	"341"=>[
		"N"=>"Total Scoring Period",
		"C"=>2
	],
	"343"=>[
		"N"=>"Score After X Sets",
		"C"=>3
	],
	"345"=>[
		"N"=>"Individual Total In Time Interval",
		"C"=>2
	],
	"347"=>[
		"N"=>"Win In Interval",
		"C"=>3
	],
	"349"=>[
		"N"=>"No Away Wins",
		"C"=>2
	],
	"351"=>[
		"N"=>"No Draws",
		"C"=>2
	],
	"353"=>[
		"N"=>"All Teams To Score",
		"C"=>2
	],
	"355"=>[
		"N"=>"All Home Teams To Score",
		"C"=>2
	],
	"357"=>[
		"N"=>"Each Match Total",
		"C"=>2
	],
	"359"=>[
		"N"=>"At Least One Team Will Not Score In Each Match",
		"C"=>2
	],
	"361"=>[
		"N"=>"Total Wins",
		"C"=>2
	],
	"367"=>[
		"N"=>"Highest Scoring Match Total",
		"C"=>2
	],
	"369"=>[
		"N"=>"Highest Scoring Team Total",
		"C"=>2
	],
	"371"=>[
		"N"=>"Highest Scoring Half Total",
		"C"=>2
	],
	"373"=>[
		"N"=>"Total Number Of Penalties",
		"C"=>2
	],
	"375"=>[
		"N"=>"Total Number Of Sendings Off",
		"C"=>2
	],
	"377"=>[
		"N"=>"Both To Score",
		"C"=>2
	],
	"379"=>[
		"N"=>"Will Result Be W1W2 Or W2W1?",
		"C"=>2
	],
	"381"=>[
		"N"=>"To Score In Time Interval",
		"C"=>2
	],
	"385"=>[
		"N"=>"Finishing Position (Group)",
		"C"=>2
	],
	"387"=>[
		"N"=>"Individual Total Breaks",
		"C"=>2
	],
	"389"=>[
		"N"=>"Total Centuries",
		"C"=>2
	],
	"391"=>[
		"N"=>"Individual Total Centuries",
		"C"=>2
	],
	"393"=>[
		"N"=>"Maximum Break (147 Points)",
		"C"=>2
	],
	"395"=>[
		"N"=>"First Colored Ball Potted",
		"C"=>2
	],
	"397"=>[
		"N"=>"Break Of 100 Or More Points",
		"C"=>2
	],
	"399"=>[
		"N"=>"Highest Scoring Break",
		"C"=>2
	],
	"401"=>[
		"N"=>"Break Of 50 Or More Points",
		"C"=>2
	],
	"403"=>[
		"N"=>"Win In Round",
		"C"=>2
	],
	"405"=>[
		"N"=>"Leader After Frames",
		"C"=>2
	],
	"410"=>[
		"N"=>"Team To Possess Ball At Final Whistle",
		"C"=>2
	],
	"413"=>[
		"N"=>"Total Centuries Interval",
		"C"=>2
	],
	"415"=>[
		"N"=>"Who Will Pot First Ball?",
		"C"=>2
	],
	"417"=>[
		"N"=>"Winning Margin",
		"C"=>2
	],
	"681"=>[
		"N"=>"Change Of Leadership",
		"C"=>2
	],
	"683"=>[
		"N"=>"Draw 2-2 - Yes/No",
		"C"=>2
	],
	"685"=>[
		"N"=>"Total Matches With Overtime",
		"C"=>2
	],
	"687"=>[
		"N"=>"Total Shutouts",
		"C"=>2
	],
	"689"=>[
		"N"=>"Total Goals Scored From 1st To 3rd Minute",
		"C"=>2
	],
	"691"=>[
		"N"=>"Total Empty Net Goals",
		"C"=>2
	],
	"693"=>[
		"N"=>"Total Goals In The Highest Scoring Match",
		"C"=>2
	],
	"695"=>[
		"N"=>"Total Goals In The Lowest Scoring Match",
		"C"=>2
	],
	"697"=>[
		"N"=>"Total Short-Handed Goals Scored",
		"C"=>2
	],
	"699"=>[
		"N"=>"Total Goals Scored During Powerplay",
		"C"=>2
	],
	"701"=>[
		"N"=>"Total Shots",
		"C"=>2
	],
	"703"=>[
		"N"=>"Total Penalty Time (Only 2 Minutes)",
		"C"=>2
	],
	"705"=>[
		"N"=>"Minimum Total Penalty Time In The Match (Only 2 Minutes)",
		"C"=>2
	],
	"707"=>[
		"N"=>"Maximum Total Penalty Time In The Match (Only 2 Minutes)",
		"C"=>2
	],
	"709"=>[
		"N"=>"Total Victories With Difference Of 3 Or More Goals",
		"C"=>2
	],
	"711"=>[
		"N"=>"Team 1's Total Victories",
		"C"=>2
	],
	"713"=>[
		"N"=>"Team 2's Total Victories",
		"C"=>2
	],
	"715"=>[
		"N"=>"Total Draws",
		"C"=>2
	],
	"717"=>[
		"N"=>"Total Legs",
		"C"=>2
	],
	"719"=>[
		"N"=>"Total 180s",
		"C"=>2
	],
	"721"=>[
		"N"=>"Highest Checkout Total",
		"C"=>2
	],
	"725"=>[
		"N"=>"Yes/No",
		"C"=>2
	],
	"731"=>[
		"N"=>"Scorecast",
		"C"=>2
	],
	"733"=>[
		"N"=>"Percentage Of Ball Possession",
		"C"=>2
	],
	"735"=>[
		"N"=>"Total Substitutions By Team",
		"C"=>2
	],
	"737"=>[
		"N"=>"Total Goals Scored By A Player",
		"C"=>2
	],
	"739"=>[
		"N"=>"Next Event",
		"C"=>1
	],
	"741"=>[
		"N"=>"When Will They Qualify?",
		"C"=>1
	],
	"742"=>[
		"N"=>"To Reach 1/8 Finals",
		"C"=>1
	],
	"745"=>[
		"N"=>"Place In The Group",
		"C"=>1
	],
	"747"=>[
		"N"=>"Total By Groups",
		"C"=>1
	],
	"749"=>[
		"N"=>"Total Rebounds",
		"C"=>2
	],
	"751"=>[
		"N"=>"Total Assists",
		"C"=>2
	],
	"753"=>[
		"N"=>"Total Two-Point Field Goals",
		"C"=>2
	],
	"755"=>[
		"N"=>"Total Three-Point Field Goals",
		"C"=>2
	],
	"757"=>[
		"N"=>"Total Fouls (Free Throws)",
		"C"=>2
	],
	"759"=>[
		"N"=>"Penalty Kick In Minute",
		"C"=>2
	],
	"761"=>[
		"N"=>"Sending Off In Minute",
		"C"=>2
	],
	"763"=>[
		"N"=>"Both Teams To Score In Minute",
		"C"=>1
	],
	"765"=>[
		"N"=>"Scorecast",
		"C"=>1
	],
	"767"=>[
		"N"=>"First Corner - Team",
		"C"=>2
	],
	"769"=>[
		"N"=>"First Shot On Target",
		"C"=>2
	],
	"771"=>[
		"N"=>"Goals By Player",
		"C"=>1
	],
	"773"=>[
		"N"=>"First To Happen",
		"C"=>2
	],
	"775"=>[
		"N"=>"Will Get Yellow Card",
		"C"=>2
	],
	"776"=>[
		"N"=>"Penalty Shootout",
		"C"=>2
	],
	"777"=>[
		"N"=>"Run in Inning",
		"C"=>2
	],
	"779"=>[
		"N"=>"First Run",
		"C"=>2
	],
	"781"=>[
		"N"=>"Extra Inning",
		"C"=>2
	],
	"783"=>[
		"N"=>"First To Score 3 Runs",
		"C"=>2
	],
	"785"=>[
		"N"=>"Last Run",
		"C"=>2
	],
	"787"=>[
		"N"=>"First To Happen",
		"C"=>2
	],
	"789"=>[
		"N"=>"First Run And Win",
		"C"=>2
	],
	"791"=>[
		"N"=>"First Home Run",
		"C"=>2
	],
	"793"=>[
		"N"=>"Individual Total Hits",
		"C"=>2
	],
	"795"=>[
		"N"=>"Total Hits",
		"C"=>2
	],
	"797"=>[
		"N"=>"Financial Bets",
		"C"=>1
	],
	"798"=>[
		"N"=>"(Lowest) Highest Scoring Quarter Total",
		"C"=>2
	],
	"800"=>[
		"N"=>"Injury Time Goal",
		"C"=>2
	],
	"806"=>[
		"N"=>"First ACE",
		"C"=>2
	],
	"808"=>[
		"N"=>"First Double Fault",
		"C"=>2
	],
	"809"=>[
		"N"=>"Sets Scoring",
		"C"=>2
	],
	"810"=>[
		"N"=>"Sets Lowest Scoring",
		"C"=>2
	],
	"811"=>[
		"N"=>"Medical Timeout",
		"C"=>2
	],
	"812"=>[
		"N"=>"To Win/Lose 1st Serve",
		"C"=>2
	],
	"813"=>[
		"N"=>"DQ or Refusal",
		"C"=>2
	],
	"814"=>[
		"N"=>"Race To Games",
		"C"=>2
	],
	"815"=>[
		"N"=>"Line-Call Challenges",
		"C"=>2
	],
	"816"=>[
		"N"=>"To Win Own Serve First",
		"C"=>1
	],
	"817"=>[
		"N"=>"Ace In First Or Second Game",
		"C"=>2
	],
	"818"=>[
		"N"=>"Total Match Time",
		"C"=>2
	],
	"819"=>[
		"N"=>"Total Time Of First Set",
		"C"=>2
	],
	"820"=>[
		"N"=>"Double Fault",
		"C"=>2
	],
	"821"=>[
		"N"=>"First Serve Winning %",
		"C"=>2
	],
	"822"=>[
		"N"=>"First Serve",
		"C"=>2
	],
	"823"=>[
		"N"=>"First Point Won",
		"C"=>2
	],
	"824"=>[
		"N"=>"Total Points In First Game By Players",
		"C"=>2
	],
	"825"=>[
		"N"=>"Who Will Make More Service Breaks",
		"C"=>2
	],
	"826"=>[
		"N"=>"Will Score And Not Lose",
		"C"=>2
	],
	"828"=>[
		"N"=>"HT-FT",
		"C"=>3
	],
	"829"=>[
		"N"=>"HT-FT + Total",
		"C"=>2
	],
	"830"=>[
		"N"=>"Next Corner",
		"C"=>3
	],
	"832"=>[
		"N"=>"Next Yellow Card",
		"C"=>3
	],
	"837"=>[
		"N"=>"Match-Ups",
		"C"=>2
	],
	"839"=>[
		"N"=>"Individual stats",
		"C"=>1
	],
	"842"=>[
		"N"=>"Winning Margin",
		"C"=>1
	],
	"844"=>[
		"N"=>"Number Of Targets Hit",
		"C"=>1
	],
	"848"=>[
		"N"=>"Total Hat-tricks",
		"C"=>1
	],
	"850"=>[
		"N"=>"Player's Total",
		"C"=>1
	],
	"852"=>[
		"N"=>"Win To Nil",
		"C"=>1
	],
	"854"=>[
		"N"=>"Both Teams To Score + Total",
		"C"=>2
	],
	"856"=>[
		"N"=>"Win To Nil + Total",
		"C"=>1
	],
	"858"=>[
		"N"=>"At Least One Team Will Not Score + Total",
		"C"=>1
	],
	"860"=>[
		"N"=>"Both Teams To Score In Both Halves",
		"C"=>1
	],
	"862"=>[
		"N"=>"Both Teams To Score In All Periods",
		"C"=>1
	],
	"864"=>[
		"N"=>"Win By",
		"C"=>1
	],
	"866"=>[
		"N"=>"Player Total Minutes",
		"C"=>1
	],
	"868"=>[
		"N"=>"Winner",
		"C"=>1
	],
	"870"=>[
		"N"=>"Will Win Pole, Race And Fastest Lap",
		"C"=>1
	],
	"872"=>[
		"N"=>"All Cars Will Finish First Lap",
		"C"=>1
	],
	"874"=>[
		"N"=>"Both Drivers' Placings",
		"C"=>1
	],
	"876"=>[
		"N"=>"Total Frames",
		"C"=>1
	],
	"878"=>[
		"N"=>"Player To Win Set",
		"C"=>2
	],
	"880"=>[
		"N"=>"Result And Both Teams To Score",
		"C"=>2
	],
	"881"=>[
		"N"=>"First To Happen",
		"C"=>2
	],
	"883"=>[
		"N"=>"Fouls",
		"C"=>2
	],
	"887"=>[
		"N"=>"Goal In Time Interval - Yes/No",
		"C"=>2
	],
	"893"=>[
		"N"=>"Team Goal In Both Halves",
		"C"=>2
	],
	"897"=>[
		"N"=>"To Score Their First Goal",
		"C"=>2
	],
	"899"=>[
		"N"=>"To Win By Exactly One Goal Or To Draw",
		"C"=>2
	],
	"901"=>[
		"N"=>"Total Goals By Halves",
		"C"=>2
	],
	"903"=>[
		"N"=>"To Score First Goal And Not Win",
		"C"=>2
	],
	"905"=>[
		"N"=>"Time In The Lead",
		"C"=>2
	],
	"906"=>[
		"N"=>"Event",
		"C"=>2
	],
	"908"=>[
		"N"=>"Indirect Free Kick",
		"C"=>1
	],
	"910"=>[
		"N"=>"Who Will Win Sets",
		"C"=>1
	],
	"912"=>[
		"N"=>"Additional Totals",
		"C"=>1
	],
	"914"=>[
		"N"=>"Last Corner In X To Y Minute",
		"C"=>1
	],
	"916"=>[
		"N"=>"Total Errors",
		"C"=>2
	],
	"918"=>[
		"N"=>"Yellow Card In Time Interval",
		"C"=>2
	],
	"920"=>[
		"N"=>"First Points To Be Scored By",
		"C"=>1
	],
	"922"=>[
		"N"=>"Last Points To Be Scored By",
		"C"=>1
	],
	"924"=>[
		"N"=>"2 PTS Field Goal",
		"C"=>1
	],
	"926"=>[
		"N"=>"3 PTS Field Goal",
		"C"=>1
	],
	"928"=>[
		"N"=>"Free Throw",
		"C"=>1
	],
	"930"=>[
		"N"=>"Timeout",
		"C"=>1
	],
	"932"=>[
		"N"=>"Stats",
		"C"=>1
	],
	"934"=>[
		"N"=>"Team's Score in Quarter",
		"C"=>1
	],
	"936"=>[
		"N"=>"Which Team Will Score Points Last?",
		"C"=>1
	],
	"938"=>[
		"N"=>"How First Point Will Be Scored",
		"C"=>1
	],
	"940"=>[
		"N"=>"Time Of First Try",
		"C"=>1
	],
	"942"=>[
		"N"=>"Total Tries",
		"C"=>1
	],
	"944"=>[
		"N"=>"Team 1 Total Tries",
		"C"=>1
	],
	"946"=>[
		"N"=>"Team 2 Total Tries",
		"C"=>1
	],
	"948"=>[
		"N"=>"Total Points In End",
		"C"=>1
	],
	"950"=>[
		"N"=>"Correct Score In End",
		"C"=>1
	],
	"952"=>[
		"N"=>"Sets Correct Score",
		"C"=>1
	],
	"954"=>[
		"N"=>"Handicap After N Innings",
		"C"=>1
	],
	"956"=>[
		"N"=>"Total After N Innings",
		"C"=>1
	],
	"958"=>[
		"N"=>"Individual Total 1 After N Innings",
		"C"=>1
	],
	"960"=>[
		"N"=>"Individual Total 2 After N Innings",
		"C"=>1
	],
	"962"=>[
		"N"=>"Lead After N Innings",
		"C"=>3
	],
	"964"=>[
		"N"=>"Will There Be A Run?",
		"C"=>1
	],
	"966"=>[
		"N"=>"Will There Be A Hit?",
		"C"=>1
	],
	"968"=>[
		"N"=>"Scoring Hits",
		"C"=>1
	],
	"969"=>[
		"N"=>"Method Of Victory",
		"C"=>2
	],
	"970"=>[
		"N"=>"Next Point",
		"C"=>1
	],
	"972"=>[
		"N"=>"Scored Points",
		"C"=>1
	],
	"974"=>[
		"N"=>"Number Of Goals",
		"C"=>1
	],
	"976"=>[
		"N"=>"Number Of Shots",
		"C"=>1
	],
	"978"=>[
		"N"=>"Total Matches With Penalty Shootouts",
		"C"=>1
	],
	"980"=>[
		"N"=>"Total Goals Scored By Defensemen",
		"C"=>1
	],
	"982"=>[
		"N"=>"Total Times A Player Scores Two Goals",
		"C"=>1
	],
	"984"=>[
		"N"=>"Total Defeats",
		"C"=>1
	],
	"986"=>[
		"N"=>"Total Goals Conceded",
		"C"=>1
	],
	"988"=>[
		"N"=>"Total Empty Net Goals Conceded",
		"C"=>1
	],
	"990"=>[
		"N"=>"Total Goals In Match",
		"C"=>1
	],
	"992"=>[
		"N"=>"Total Defeats With Difference Of 3 Or More Goals",
		"C"=>1
	],
	"994"=>[
		"N"=>"Number Of Matches In Which The Team Will Secure A Shutout",
		"C"=>1
	],
	"996"=>[
		"N"=>"Total Runs",
		"C"=>1
	],
	"998"=>[
		"N"=>"Individual Total Runs",
		"C"=>1
	],
	"1000"=>[
		"N"=>"Individual Total Runs Even/Odd",
		"C"=>1
	],
	"1002"=>[
		"N"=>"Next Man Out",
		"C"=>1
	],
	"1004"=>[
		"N"=>"Method Of Next Batsman Dismissal (6Way)",
		"C"=>1
	],
	"1006"=>[
		"N"=>"Top Batsman Team 1",
		"C"=>1
	],
	"1007"=>[
		"N"=>"Top Batsman Team 2",
		"C"=>1
	],
	"1008"=>[
		"N"=>"Winner",
		"C"=>1
	],
	"1010"=>[
		"N"=>"Knockdown",
		"C"=>1
	],
	"1012"=>[
		"N"=>"Total Runs Per Delivery",
		"C"=>1
	],
	"1014"=>[
		"N"=>"Individual Total Runs (Overs)",
		"C"=>1
	],
	"1016"=>[
		"N"=>"Individual Total Runs Even/Odd (Overs)",
		"C"=>1
	],
	"1018"=>[
		"N"=>"Player's Total Runs",
		"C"=>1
	],
	"1020"=>[
		"N"=>"Who Will Score More Runs",
		"C"=>1
	],
	"1022"=>[
		"N"=>"Type Of Points Score",
		"C"=>1
	],
	"1024"=>[
		"N"=>"Total Behinds",
		"C"=>1
	],
	"1026"=>[
		"N"=>"Individual Total Behinds",
		"C"=>1
	],
	"1028"=>[
		"N"=>"Extra Time",
		"C"=>1
	],
	"1032"=>[
		"N"=>"Method Of Qualifying",
		"C"=>2
	],
	"1036"=>[
		"N"=>"Finish Position",
		"C"=>1
	],
	"1038"=>[
		"N"=>"Individual Total Goals",
		"C"=>1
	],
	"1040"=>[
		"N"=>"First Goal + Result",
		"C"=>1
	],
	"1042"=>[
		"N"=>"Method Of Next Batsman Dismissal (2Way)",
		"C"=>1
	],
	"1044"=>[
		"N"=>"Will A Wicket Fall In Over",
		"C"=>1
	],
	"1046"=>[
		"N"=>"Total Fallen Wickets",
		"C"=>1
	],
	"1048"=>[
		"N"=>"Last Checkout (Colour)",
		"C"=>1
	],
	"1050"=>[
		"N"=>"Round Win",
		"C"=>1
	],
	"1052"=>[
		"N"=>"Round Total",
		"C"=>1
	],
	"1054"=>[
		"N"=>"Round Handicap",
		"C"=>1
	],
	"1056"=>[
		"N"=>"First Frag In Round - Team",
		"C"=>1
	],
	"1058"=>[
		"N"=>"Suicide In Round",
		"C"=>1
	],
	"1060"=>[
		"N"=>"Total Headshots In Round",
		"C"=>1
	],
	"1062"=>[
		"N"=>"Individual Total Frags In Round",
		"C"=>1
	],
	"1064"=>[
		"N"=>"Total Frags in Round Even/Odd",
		"C"=>1
	],
	"1066"=>[
		"N"=>"Method Of Win In Round",
		"C"=>1
	],
	"1068"=>[
		"N"=>"Bomb Planted",
		"C"=>1
	],
	"1070"=>[
		"N"=>"Bomb Defused",
		"C"=>1
	],
	"1072"=>[
		"N"=>"Win To Nil In Round",
		"C"=>1
	],
	"1074"=>[
		"N"=>"Round Duration",
		"C"=>1
	],
	"1075"=>[
		"N"=>"Leader After Ends",
		"C"=>3
	],
	"1076"=>[
		"N"=>"Next Try",
		"C"=>1
	],
	"1078"=>[
		"N"=>"Last Checkout Total",
		"C"=>1
	],
	"1080"=>[
		"N"=>"First 3 Darts Total",
		"C"=>1
	],
	"1081"=>[
		"N"=>"Highest Opening Partnership",
		"C"=>3
	],
	"1083"=>[
		"N"=>"Total Sixes",
		"C"=>2
	],
	"1085"=>[
		"N"=>"Individual Total Match 180s",
		"C"=>1
	],
	"1087"=>[
		"N"=>"Most 180s In Match",
		"C"=>1
	],
	"1089"=>[
		"N"=>"180 To Be Scored",
		"C"=>1
	],
	"1091"=>[
		"N"=>"Total Matches With Over/Under",
		"C"=>1
	],
	"1093"=>[
		"N"=>"Yellow/Red Card",
		"C"=>1
	],
	"1094"=>[
		"N"=>"Main Series Outcome (5 Shots)",
		"C"=>1
	],
	"1096"=>[
		"N"=>"When Will Next Wicket Fall",
		"C"=>1
	],
	"1098"=>[
		"N"=>"Correct Score Total Matches",
		"C"=>1
	],
	"1100"=>[
		"N"=>"Score After X Games",
		"C"=>1
	],
	"1102"=>[
		"N"=>"Intermediate Result Of Set",
		"C"=>1
	],
	"1104"=>[
		"N"=>"Number In The Score",
		"C"=>2
	],
	"1106"=>[
		"N"=>"Who Will Win A Game In The Match",
		"C"=>1
	],
	"1108"=>[
		"N"=>"Serve Winning %",
		"C"=>1
	],
	"1110"=>[
		"N"=>"Average Speed Of Serve. Total",
		"C"=>1
	],
	"1112"=>[
		"N"=>"Total Winners",
		"C"=>1
	],
	"1114"=>[
		"N"=>"Fastest Serve",
		"C"=>1
	],
	"1116"=>[
		"N"=>"Average Speed Of Serve. Handicap",
		"C"=>1
	],
	"1118"=>[
		"N"=>"Serve Percentage. Handicap",
		"C"=>1
	],
	"1120"=>[
		"N"=>"Death From Bomb In Round",
		"C"=>1
	],
	"1122"=>[
		"N"=>"Last Penalty In A Shootout",
		"C"=>1
	],
	"1124"=>[
		"N"=>"Who Will Score Last Penalty In A Shootout",
		"C"=>1
	],
	"1126"=>[
		"N"=>"Both Teams Total",
		"C"=>1
	],
	"1128"=>[
		"N"=>"Total In Interval",
		"C"=>1
	],
	"1130"=>[
		"N"=>"Total Goals In Interval",
		"C"=>1
	],
	"1132"=>[
		"N"=>"Individual Total Goals In Interval",
		"C"=>1
	],
	"1134"=>[
		"N"=>"Fight To Go The Distance",
		"C"=>1
	],
	"1136"=>[
		"N"=>"Total Rounds",
		"C"=>1
	],
	"1138"=>[
		"N"=>"When Will Bout End",
		"C"=>1
	],
	"1140"=>[
		"N"=>"Total Interval.",
		"C"=>1
	],
	"1142"=>[
		"N"=>"Total Interval..",
		"C"=>3
	],
	"1144"=>[
		"N"=>"Exact Points Difference.",
		"C"=>1
	],
	"1146"=>[
		"N"=>"Exact Points Difference..",
		"C"=>1
	],
	"1148"=>[
		"N"=>"Exact Points Difference...",
		"C"=>1
	],
	"1150"=>[
		"N"=>"Total Interval (3Way)",
		"C"=>1
	],
	"1152"=>[
		"N"=>"Total Interval (5Way)",
		"C"=>1
	],
	"1154"=>[
		"N"=>"Results. Halves",
		"C"=>2
	],
	"1156"=>[
		"N"=>"Player To Score Two Goals (Brace)",
		"C"=>1
	],
	"1158"=>[
		"N"=>"Player To Score A Hat-Trick",
		"C"=>1
	],
	"1160"=>[
		"N"=>"Who Will Win Most Periods",
		"C"=>1
	],
	"1162"=>[
		"N"=>"Individual Total 1 Interval (3Way)",
		"C"=>1
	],
	"1164"=>[
		"N"=>"Individual Total 1 Interval (5Way)",
		"C"=>1
	],
	"1166"=>[
		"N"=>"To Win By (3Way)",
		"C"=>1
	],
	"1168"=>[
		"N"=>"To Win By (5Way)",
		"C"=>1
	],
	"1170"=>[
		"N"=>"Hole, Result",
		"C"=>1
	],
	"1172"=>[
		"N"=>"Hole, Match-Ups",
		"C"=>1
	],
	"1174"=>[
		"N"=>"Hole, Total Score",
		"C"=>1
	],
	"1176"=>[
		"N"=>"Hole, Over/Under Par",
		"C"=>1
	],
	"1178"=>[
		"N"=>"Hole, Player, Over/Under Par",
		"C"=>1
	],
	"1180"=>[
		"N"=>"Individual Total 2 Interval (3Way)",
		"C"=>1
	],
	"1182"=>[
		"N"=>"Individual Total 2 Interval (5Way)",
		"C"=>1
	],
	"2228"=>[
		"N"=>"Highest Scoring Quarter",
		"C"=>1
	],
	"2230"=>[
		"N"=>"Lowest Scoring Quarter",
		"C"=>1
	],
	"2232"=>[
		"N"=>"Total Of Drawn Quarters",
		"C"=>1
	],
	"2234"=>[
		"N"=>"Handicap By Quarters",
		"C"=>1
	],
	"2236"=>[
		"N"=>"Correct Score By Quarters",
		"C"=>1
	],
	"2238"=>[
		"N"=>"Hole, Winner",
		"C"=>1
	],
	"2240"=>[
		"N"=>"Last Hole Played",
		"C"=>1
	],
	"2242"=>[
		"N"=>"Total Even / Odd Points In End",
		"C"=>1
	],
	"2244"=>[
		"N"=>"First Half / Full-time",
		"C"=>1
	],
	"2246"=>[
		"N"=>"Hole, Total Score.",
		"C"=>1
	],
	"2248"=>[
		"N"=>"Number Of Drawn Quarters",
		"C"=>1
	],
	"2250"=>[
		"N"=>"Win By Quarters",
		"C"=>3
	],
	"2252"=>[
		"N"=>"Number Of Quarters Won By Team",
		"C"=>2
	],
	"2254"=>[
		"N"=>"First Three-Point Field Goal",
		"C"=>1
	],
	"2255"=>[
		"N"=>"Stats",
		"C"=>2
	],
	"2257"=>[
		"N"=>"Winner In Round",
		"C"=>1
	],
	"2258"=>[
		"N"=>"Penalty Shootout Total",
		"C"=>1
	],
	"2260"=>[
		"N"=>"Set Scoring Total",
		"C"=>2
	],
	"2262"=>[
		"N"=>"Next Touchdown",
		"C"=>1
	],
	"2264"=>[
		"N"=>"Safety",
		"C"=>1
	],
	"2266"=>[
		"N"=>"Next Field Goal",
		"C"=>1
	],
	"2268"=>[
		"N"=>"Goals In A Row",
		"C"=>1
	],
	"2270"=>[
		"N"=>"Extra Penalties",
		"C"=>1
	],
	"2272"=>[
		"N"=>"Highest Checkout",
		"C"=>1
	],
	"2274"=>[
		"N"=>"Shootout",
		"C"=>1
	],
	"2276"=>[
		"N"=>"The Outcome Of Main Rounds In Shootout",
		"C"=>1
	],
	"2278"=>[
		"N"=>"Score In Shootout",
		"C"=>1
	],
	"2280"=>[
		"N"=>"Result / Teams To Score",
		"C"=>1
	],
	"2282"=>[
		"N"=>"Teams, Goal Time",
		"C"=>1
	],
	"2284"=>[
		"N"=>"Will There Be A Goal",
		"C"=>1
	],
	"2286"=>[
		"N"=>"Goal In Minute",
		"C"=>1
	],
	"2288"=>[
		"N"=>"Who Will Take Tower",
		"C"=>1
	],
	"2290"=>[
		"N"=>"Who Will Beat Dragon",
		"C"=>1
	],
	"2292"=>[
		"N"=>"Who Will Beat Roshan",
		"C"=>1
	],
	"2294"=>[
		"N"=>"Who Will Beat Nashor",
		"C"=>1
	],
	"2296"=>[
		"N"=>"Method Of Win",
		"C"=>1
	],
	"2298"=>[
		"N"=>"Method Of Fighter's Win",
		"C"=>2
	],
	"2300"=>[
		"N"=>"Bout Duration",
		"C"=>1
	],
	"2302"=>[
		"N"=>"Totals",
		"C"=>2
	],
	"2304"=>[
		"N"=>"Total Goals (3Way)",
		"C"=>1
	],
	"2306"=>[
		"N"=>"Total Touchdowns (3Way)",
		"C"=>1
	],
	"2308"=>[
		"N"=>"Total Touchdowns (2Way)",
		"C"=>1
	],
	"2310"=>[
		"N"=>"How Will Point Be Scored (3Way)",
		"C"=>1
	],
	"2312"=>[
		"N"=>"How Will Point Be Scored (6Way)",
		"C"=>1
	],
	"2314"=>[
		"N"=>"Total Hits (3Way)",
		"C"=>1
	],
	"2316"=>[
		"N"=>"Player Number Of Runs",
		"C"=>1
	],
	"2318"=>[
		"N"=>"Who Will Pot Last Ball",
		"C"=>1
	],
	"2320"=>[
		"N"=>"Total Scored Field Goals (3Way)",
		"C"=>1
	],
	"2322"=>[
		"N"=>"Total Scored Field Goals (2Way)",
		"C"=>1
	],
	"2324"=>[
		"N"=>"Round, Match-Ups",
		"C"=>1
	],
	"2326"=>[
		"N"=>"Round, Handicap",
		"C"=>1
	],
	"2328"=>[
		"N"=>"Round, Handicap",
		"C"=>1
	],
	"2330"=>[
		"N"=>"Round, Total Score",
		"C"=>1
	],
	"2332"=>[
		"N"=>"Round Outcomes",
		"C"=>2
	],
	"2334"=>[
		"N"=>"In Which Round Will 13th Sector Appear",
		"C"=>1
	],
	"2336"=>[
		"N"=>"In Which Round Will Blitz Appear",
		"C"=>1
	],
	"2338"=>[
		"N"=>"In Which Round Will Superblitz Appear",
		"C"=>1
	],
	"2340"=>[
		"N"=>"Score After First 2 Rounds",
		"C"=>1
	],
	"2342"=>[
		"N"=>"First Round / Match",
		"C"=>1
	],
	"2344"=>[
		"N"=>"Series Score",
		"C"=>1
	],
	"2346"=>[
		"N"=>"Number Of Runs In Match",
		"C"=>1
	],
	"2348"=>[
		"N"=>"Total Runs",
		"C"=>1
	],
	"2350"=>[
		"N"=>"Total Matches With Extra Innings",
		"C"=>1
	],
	"2352"=>[
		"N"=>"Total Strike-Outs In Match",
		"C"=>1
	],
	"2354"=>[
		"N"=>"Total Strike-Outs",
		"C"=>1
	],
	"2356"=>[
		"N"=>"Leg, Last Checkout (Colour)",
		"C"=>2
	],
	"2358"=>[
		"N"=>"Leg, Last Checkout Total",
		"C"=>1
	],
	"2360"=>[
		"N"=>"Total Legs In Set",
		"C"=>1
	],
	"2362"=>[
		"N"=>"Leg Win",
		"C"=>2
	],
	"2364"=>[
		"N"=>"Leg, 180",
		"C"=>2
	],
	"2366"=>[
		"N"=>"Leg, First Three Darts Total",
		"C"=>1
	],
	"2368"=>[
		"N"=>"Event To Happen",
		"C"=>1
	],
	"2370"=>[
		"N"=>"Correct Score In Tie-Break",
		"C"=>2
	],
	"2372"=>[
		"N"=>"Even/Odd In Tie-Break",
		"C"=>2
	],
	"2374"=>[
		"N"=>"Point In Tie-Break",
		"C"=>2
	],
	"2376"=>[
		"N"=>"Any Team To Win With Difference",
		"C"=>1
	],
	"2378"=>[
		"N"=>"Team To Win In The Highest Scoring Quarter",
		"C"=>1
	],
	"2380"=>[
		"N"=>"Handicap On Tie-Break",
		"C"=>1
	],
	"2382"=>[
		"N"=>"First 5 Minutes",
		"C"=>2
	],
	"2384"=>[
		"N"=>"First To Happen In First 5 Minutes",
		"C"=>1
	],
	"2386"=>[
		"N"=>"First 5 Minutes - Corners",
		"C"=>1
	],
	"2388"=>[
		"N"=>"First 5 Minutes - Actions",
		"C"=>1
	],
	"2390"=>[
		"N"=>"Team 1 Performance",
		"C"=>1
	],
	"2392"=>[
		"N"=>"Team 2 Performance",
		"C"=>1
	],
	"2394"=>[
		"N"=>"Total Points For Cards In The Match",
		"C"=>1
	],
	"2396"=>[
		"N"=>"Total Cards In The Match",
		"C"=>1
	],
	"2398"=>[
		"N"=>"First Card Time",
		"C"=>1
	],
	"2402"=>[
		"N"=>"Any Player To Get Booked During The Match",
		"C"=>2
	],
	"2404"=>[
		"N"=>"Team 1 Total In Interval",
		"C"=>2
	],
	"2406"=>[
		"N"=>"Team 2 Total In Interval",
		"C"=>2
	],
	"2408"=>[
		"N"=>"Most Points For Cards",
		"C"=>3
	],
	"2410"=>[
		"N"=>"Corners And Cards",
		"C"=>1
	],
	"2412"=>[
		"N"=>"Team 1 First Goal In Interval",
		"C"=>1
	],
	"2414"=>[
		"N"=>"Team 2 First Goal In Interval",
		"C"=>1
	],
	"2416"=>[
		"N"=>"Team And Goal Time",
		"C"=>1
	],
	"2418"=>[
		"N"=>"To Keep Clean Sheet",
		"C"=>2
	],
	"2420"=>[
		"N"=>"First Goal To Be An Own Goal",
		"C"=>3
	],
	"2422"=>[
		"N"=>"Team 1 Scores In Halves",
		"C"=>1
	],
	"2424"=>[
		"N"=>"Team 2 Scores In Halves",
		"C"=>1
	],
	"2426"=>[
		"N"=>"Both Players To Score During The Match",
		"C"=>1
	],
	"2428"=>[
		"N"=>"Team 1, Corner From Minute To Minute",
		"C"=>1
	],
	"2430"=>[
		"N"=>"Team 2, Corner From Minute To Minute",
		"C"=>1
	],
	"2432"=>[
		"N"=>"Win In Round",
		"C"=>2
	],
	"2434"=>[
		"N"=>"Who Will Win X Rounds?",
		"C"=>1
	],
	"2436"=>[
		"N"=>"Total Maps",
		"C"=>1
	],
	"2438"=>[
		"N"=>"Total Maps Handicap",
		"C"=>1
	],
	"2440"=>[
		"N"=>"Team Goals",
		"C"=>1
	],
	"2444"=>[
		"N"=>"Total And Both To Score",
		"C"=>2
	],
	"2446"=>[
		"N"=>"Both Teams To Score In Halves",
		"C"=>1
	],
	"2448"=>[
		"N"=>"First Corner Time",
		"C"=>1
	],
	"2450"=>[
		"N"=>"Highest Scoring Half Win",
		"C"=>1
	],
	"2452"=>[
		"N"=>"Team 1, First Goal In Half",
		"C"=>3
	],
	"2454"=>[
		"N"=>"Team 2, First Goal In Half",
		"C"=>3
	],
	"2456"=>[
		"N"=>"Team 1, First Goal In Interval",
		"C"=>1
	],
	"2458"=>[
		"N"=>"Team 2, First Goal In Interval",
		"C"=>1
	],
	"2460"=>[
		"N"=>"Corner From Minute To Minute",
		"C"=>1
	],
	"2462"=>[
		"N"=>"Player To Score First And Last Goal",
		"C"=>1
	],
	"2464"=>[
		"N"=>"Player To Score First Or Last Goal",
		"C"=>1
	],
	"2466"=>[
		"N"=>"Player To Score In Both Halves",
		"C"=>1
	],
	"2468"=>[
		"N"=>"Team 1, First Player To Score",
		"C"=>1
	],
	"2470"=>[
		"N"=>"Team 2, First Player To Score",
		"C"=>1
	],
	"2472"=>[
		"N"=>"Will There Be A Particular Score In Match",
		"C"=>1
	],
	"2474"=>[
		"N"=>"Run Of Play + Match Result",
		"C"=>2
	],
	"2476"=>[
		"N"=>"Goals And Cards",
		"C"=>1
	],
	"2478"=>[
		"N"=>"Goals And Corners",
		"C"=>1
	],
	"2480"=>[
		"N"=>"Total Points In Match",
		"C"=>1
	],
	"2484"=>[
		"N"=>"Result + Corners + Cards",
		"C"=>1
	],
	"2486"=>[
		"N"=>"Goals + Corners + Cards",
		"C"=>1
	],
	"2487"=>[
		"N"=>"Player, Goal In Interval",
		"C"=>1
	],
	"2488"=>[
		"N"=>"Points, First 5 Minutes",
		"C"=>1
	],
	"2490"=>[
		"N"=>"Next Card",
		"C"=>1
	],
	"2492"=>[
		"N"=>"Any Team To Win By",
		"C"=>1
	],
	"2494"=>[
		"N"=>"When Will Map End",
		"C"=>1
	],
	"2496"=>[
		"N"=>"Goal By Header",
		"C"=>1
	],
	"2498"=>[
		"N"=>"Goal From Outside Penalty Area",
		"C"=>1
	],
	"2500"=>[
		"N"=>"First Blood",
		"C"=>1
	],
	"2502"=>[
		"N"=>"Special Bets",
		"C"=>2
	],
	"2504"=>[
		"N"=>"Bets On Teams",
		"C"=>3
	],
	"2506"=>[
		"N"=>"Players. Total Runs",
		"C"=>2
	],
	"2508"=>[
		"N"=>"Total",
		"C"=>2
	],
	"2510"=>[
		"N"=>"Individual Total 1",
		"C"=>2
	],
	"2512"=>[
		"N"=>"Individual Total 2",
		"C"=>2
	],
	"2514"=>[
		"N"=>"Total Fours",
		"C"=>2
	],
	"2516"=>[
		"N"=>"Total Runs In Over",
		"C"=>1
	],
	"2518"=>[
		"N"=>"Who Performs Knockdown",
		"C"=>1
	],
	"2520"=>[
		"N"=>"Top Batsman",
		"C"=>1
	],
	"2522"=>[
		"N"=>"Free Kicks, First 10 Minutes",
		"C"=>1
	],
	"2524"=>[
		"N"=>"Goal Kicks, First 10 Minutes",
		"C"=>1
	],
	"2526"=>[
		"N"=>"Throw-ins, First 10 Minutes",
		"C"=>1
	],
	"2528"=>[
		"N"=>"Winner's Grid Position",
		"C"=>1
	],
	"2531"=>[
		"N"=>"Inning Result",
		"C"=>1
	],
	"2534"=>[
		"N"=>"Leader At The End Of One Of The Laps",
		"C"=>1
	],
	"2536"=>[
		"N"=>"First Driver To Make A Pit Stop",
		"C"=>1
	],
	"2538"=>[
		"N"=>"Last Driver To Make A Pit Stop",
		"C"=>1
	],
	"2540"=>[
		"N"=>"Speeding In Pit Lane",
		"C"=>1
	],
	"2542"=>[
		"N"=>"Fastest Lap + Win",
		"C"=>1
	],
	"2544"=>[
		"N"=>"Best Qualification Time + Win",
		"C"=>1
	],
	"2546"=>[
		"N"=>"Best Qualification Time + Fastest Lap + Win",
		"C"=>1
	],
	"2548"=>[
		"N"=>"To Retire On First Lap",
		"C"=>1
	],
	"2550"=>[
		"N"=>"Extra Totals (3Way)",
		"C"=>1
	],
	"2552"=>[
		"N"=>"Fastest Lap",
		"C"=>1
	],
	"2554"=>[
		"N"=>"Safety Car",
		"C"=>2
	],
	"2556"=>[
		"N"=>"Driver To Be Classified",
		"C"=>1
	],
	"2558"=>[
		"N"=>"First Driver To Retire",
		"C"=>1
	],
	"2560"=>[
		"N"=>"First Driver Of A Team To Retire",
		"C"=>1
	],
	"2561"=>[
		"N"=>"Country Of Winner",
		"C"=>1
	],
	"2562"=>[
		"N"=>"Continent Of Winner",
		"C"=>1
	],
	"2563"=>[
		"N"=>"Team To Score Their Goal In Period",
		"C"=>1
	],
	"2565"=>[
		"N"=>"Results, Periods",
		"C"=>1
	],
	"2566"=>[
		"N"=>"Players, Match-Ups, Handicaps",
		"C"=>1
	],
	"2568"=>[
		"N"=>"To Be Higher (3Way)",
		"C"=>1
	],
	"2570"=>[
		"N"=>"1x2, Match-Ups",
		"C"=>1
	],
	"2572"=>[
		"N"=>"Double Chance, Match-Ups",
		"C"=>1
	],
	"2574"=>[
		"N"=>"Total, Match-Ups",
		"C"=>1
	],
	"2576"=>[
		"N"=>"Opponent From 1st Pot",
		"C"=>1
	],
	"2578"=>[
		"N"=>"Opponent From 2nd Pot",
		"C"=>1
	],
	"2580"=>[
		"N"=>"Opponent From 3rd Pot",
		"C"=>1
	],
	"2582"=>[
		"N"=>"Opponent From 4th Pot",
		"C"=>1
	],
	"2584"=>[
		"N"=>"In Top 2",
		"C"=>2
	],
	"2586"=>[
		"N"=>"In Top 3",
		"C"=>2
	],
	"2588"=>[
		"N"=>"In Top 4",
		"C"=>2
	],
	"2590"=>[
		"N"=>"In Top 5",
		"C"=>2
	],
	"2592"=>[
		"N"=>"In Top 6",
		"C"=>2
	],
	"2594"=>[
		"N"=>"In Top 7",
		"C"=>2
	],
	"2596"=>[
		"N"=>"In Top 8",
		"C"=>2
	],
	"2598"=>[
		"N"=>"In Top 9",
		"C"=>2
	],
	"2600"=>[
		"N"=>"In Top 10",
		"C"=>2
	],
	"2601"=>[
		"N"=>"The Lead",
		"C"=>1
	],
	"2603"=>[
		"N"=>"Leader After 1st Firing Line",
		"C"=>1
	],
	"2604"=>[
		"N"=>"Total Games",
		"C"=>1
	],
	"2606"=>[
		"N"=>"Number Of Balls Potted On The Break",
		"C"=>1
	],
	"2608"=>[
		"N"=>"Golden Break",
		"C"=>1
	],
	"2610"=>[
		"N"=>"Leader After 1st Changeover",
		"C"=>1
	],
	"2612"=>[
		"N"=>"To Complete Two Runs",
		"C"=>1
	],
	"2614"=>[
		"N"=>"Win Without One Of The Opponents",
		"C"=>1
	],
	"2616"=>[
		"N"=>"Finishing Position",
		"C"=>1
	],
	"2643"=>[
		"N"=>"Total Wins",
		"C"=>1
	],
	"2645"=>[
		"N"=>"Total Defeats",
		"C"=>1
	],
	"2647"=>[
		"N"=>"Total Matches With OT",
		"C"=>1
	],
	"2649"=>[
		"N"=>"Total Conceded Goals",
		"C"=>2
	],
	"2651"=>[
		"N"=>"Total Shots",
		"C"=>2
	],
	"2653"=>[
		"N"=>"Total Penalty Minutes",
		"C"=>2
	],
	"2654"=>[
		"N"=>"Team To Win In All Periods",
		"C"=>1
	],
	"2655"=>[
		"N"=>"Penalty Shot In Regular Time",
		"C"=>1
	],
	"2657"=>[
		"N"=>"Will A Penalty Shot Be Scored In Regular Time",
		"C"=>1
	],
	"2659"=>[
		"N"=>"Goalkeeper To Score",
		"C"=>1
	],
	"2661"=>[
		"N"=>"How The First Goal Will Be Scored",
		"C"=>1
	],
	"2663"=>[
		"N"=>"Team 1, Result + Total",
		"C"=>1
	],
	"2665"=>[
		"N"=>"Team 2, Result + Total",
		"C"=>1
	],
	"2667"=>[
		"N"=>"Draw + Total",
		"C"=>1
	],
	"2668"=>[
		"N"=>"Double Chance + Total",
		"C"=>1
	],
	"2669"=>[
		"N"=>"Last Penalty",
		"C"=>1
	],
	"2671"=>[
		"N"=>"Misconduct Penalty",
		"C"=>1
	],
	"2673"=>[
		"N"=>"First 2 Minute Penalty",
		"C"=>1
	],
	"2674"=>[
		"N"=>"Number Of Targets Hit From A Firing Line",
		"C"=>1
	],
	"2676"=>[
		"N"=>"Distance Length, To Be Higher",
		"C"=>1
	],
	"2678"=>[
		"N"=>"Who Will Have The Most Hits During The Race?",
		"C"=>1
	],
	"2680"=>[
		"N"=>"Hundredths Of A Second In The Winning Time",
		"C"=>1
	],
	"2681"=>[
		"N"=>"Players, Special, Total",
		"C"=>1
	],
	"2682"=>[
		"N"=>"Total Interval In Regular Time",
		"C"=>1
	],
	"2683"=>[
		"N"=>"Frags, Handicap",
		"C"=>1
	],
	"2685"=>[
		"N"=>"Frags, Total",
		"C"=>1
	],
	"2687"=>[
		"N"=>"Frags, Total Even/Odd",
		"C"=>1
	],
	"2689"=>[
		"N"=>"Frags, Race To",
		"C"=>1
	],
	"2691"=>[
		"N"=>"First 7 Meter Penalty Shot",
		"C"=>2
	],
	"2693"=>[
		"N"=>"Last 7 Meter Penalty Shot",
		"C"=>2
	],
	"2695"=>[
		"N"=>"First Unconverted 7 Meter Penalty Shot",
		"C"=>2
	],
	"2697"=>[
		"N"=>"Will The First 7 Meter Penalty Shot Be Scored",
		"C"=>2
	],
	"2699"=>[
		"N"=>"Total 7 Meter Penalty Shots In The Match",
		"C"=>2
	],
	"2701"=>[
		"N"=>"Team Total Of 7 Meter Penalty Shots In The Match",
		"C"=>2
	],
	"2703"=>[
		"N"=>"Handicap, 7 Meter Penalty Shots In The Match",
		"C"=>2
	],
	"2705"=>[
		"N"=>"Last 2 Minute Suspension",
		"C"=>1
	],
	"2707"=>[
		"N"=>"Total 2 Minute Suspensions",
		"C"=>2
	],
	"2709"=>[
		"N"=>"Handicap, 2 Minute Suspensions",
		"C"=>1
	],
	"2711"=>[
		"N"=>"7 Meter Penalty Shot Percentage",
		"C"=>2
	],
	"2713"=>[
		"N"=>"Goalkeepers' Save Percentage",
		"C"=>2
	],
	"2715"=>[
		"N"=>"Shot Efficiency Percentage",
		"C"=>2
	],
	"2717"=>[
		"N"=>"Total Rounds Even / Odd",
		"C"=>1
	],
	"2719"=>[
		"N"=>"In Top 15",
		"C"=>1
	],
	"2720"=>[
		"N"=>"1st and 2nd Place In The Group",
		"C"=>1
	],
	"2722"=>[
		"N"=>"Lowest Scoring Match Total",
		"C"=>1
	],
	"2724"=>[
		"N"=>"Highest Scoring Home Team Total",
		"C"=>1
	],
	"2726"=>[
		"N"=>"Highest Scoring Away Team Total",
		"C"=>1
	],
	"2728"=>[
		"N"=>"Who Will Score A Goal And Match Score",
		"C"=>1
	],
	"2732"=>[
		"N"=>"To Provide An Assist",
		"C"=>1
	],
	"2734"=>[
		"N"=>"Free Kick Goal",
		"C"=>1
	],
	"2736"=>[
		"N"=>"Position Of Goalscorer",
		"C"=>2
	],
	"2738"=>[
		"N"=>"Hit The Post Or Crossbar",
		"C"=>2
	],
	"2740"=>[
		"N"=>"Goal After Corner",
		"C"=>1
	],
	"2742"=>[
		"N"=>"Period / Match",
		"C"=>1
	],
	"2744"=>[
		"N"=>"Set / Match",
		"C"=>1
	],
	"2746"=>[
		"N"=>"Team Wins The Match Having Been Behind",
		"C"=>1
	],
	"2747"=>[
		"N"=>"First Inning / Match",
		"C"=>1
	],
	"2749"=>[
		"N"=>"Both Teams To Score Runs",
		"C"=>1
	],
	"2750"=>[
		"N"=>"Remaining Time Outcome",
		"C"=>1
	],
	"2752"=>[
		"N"=>"Remaining Time Double Chance",
		"C"=>1
	],
	"2754"=>[
		"N"=>"Season Race Wins",
		"C"=>1
	],
	"2756"=>[
		"N"=>"Team 1, Player's Try",
		"C"=>1
	],
	"2758"=>[
		"N"=>"Team 2, Player's Try",
		"C"=>1
	],
	"2760"=>[
		"N"=>"Player's Try",
		"C"=>1
	],
	"2761"=>[
		"N"=>"Last Potted Colored Ball",
		"C"=>1
	],
	"2763"=>[
		"N"=>"Will There Be A Foul",
		"C"=>1
	],
	"2764"=>[
		"N"=>"Highest Scoring Break",
		"C"=>1
	],
	"2766"=>[
		"N"=>"1X2 In Regular Time",
		"C"=>1
	],
	"2768"=>[
		"N"=>"Regular Time Double Chance",
		"C"=>1
	],
	"2770"=>[
		"N"=>"Total Drivers To Be Classified",
		"C"=>1
	],
	"2772"=>[
		"N"=>"Thousandths Of A Second In The Winner's Time",
		"C"=>2
	],
	"2774"=>[
		"N"=>"Fastest Pit Stop (Not Including Penalties)",
		"C"=>1
	],
	"2776"=>[
		"N"=>"Total Games Won On Player's Own Serve",
		"C"=>1
	],
	"2778"=>[
		"N"=>"Handicap In Interval",
		"C"=>1
	],
	"2780"=>[
		"N"=>"3Way Total Interval",
		"C"=>1
	],
	"2782"=>[
		"N"=>"Interval, Race To",
		"C"=>1
	],
	"2784"=>[
		"N"=>"Interval, To Win By",
		"C"=>1
	],
	"2786"=>[
		"N"=>"Team's Top Driver To Be Higher",
		"C"=>1
	],
	"2788"=>[
		"N"=>"Total Classified Drivers Even/Odd",
		"C"=>1
	],
	"2790"=>[
		"N"=>"Interval, Handicap",
		"C"=>1
	],
	"2792"=>[
		"N"=>"Player To Break Racket",
		"C"=>2
	],
	"2794"=>[
		"N"=>"Interval, Individual 3Way Total 1",
		"C"=>1
	],
	"2796"=>[
		"N"=>"Interval, Individual 3Way Total 2",
		"C"=>1
	],
	"2798"=>[
		"N"=>"Longest Game",
		"C"=>2
	],
	"2800"=>[
		"N"=>"Total Tie-Breaks In The Match",
		"C"=>1
	],
	"2802"=>[
		"N"=>"Top Batsman Of The Match",
		"C"=>1
	],
	"2804"=>[
		"N"=>"Who Will Win The Toss",
		"C"=>1
	],
	"2806"=>[
		"N"=>"Team 1 Best Bowler",
		"C"=>1
	],
	"2808"=>[
		"N"=>"Best Player Of The Match",
		"C"=>1
	],
	"2810"=>[
		"N"=>"Match Handicap",
		"C"=>1
	],
	"2812"=>[
		"N"=>"Race Between Players",
		"C"=>1
	],
	"2814"=>[
		"N"=>"1st Wicket - Method Of Fall (6Way)",
		"C"=>1
	],
	"2816"=>[
		"N"=>"Total Runs Scored Before 1st Wicket Falls",
		"C"=>1
	],
	"2818"=>[
		"N"=>"First 6 Overs Result",
		"C"=>1
	],
	"2820"=>[
		"N"=>"Players, Performance",
		"C"=>1
	],
	"2822"=>[
		"N"=>"Total Interval (10Way)",
		"C"=>1
	],
	"2823"=>[
		"N"=>"Extra End",
		"C"=>1
	],
	"2825"=>[
		"N"=>"Which Stage Will A Team Reach",
		"C"=>1
	],
	"2827"=>[
		"N"=>"Finalists",
		"C"=>1
	],
	"2828"=>[
		"N"=>"Leg, Last Throw",
		"C"=>1
	],
	"2829"=>[
		"N"=>"Interval Outcome (10m)",
		"C"=>1
	],
	"2831"=>[
		"N"=>"Interval Outcome (15m)",
		"C"=>1
	],
	"2833"=>[
		"N"=>"Double Outcome (5m)",
		"C"=>1
	],
	"2835"=>[
		"N"=>"Double Outcome (10m)",
		"C"=>1
	],
	"2837"=>[
		"N"=>"Double Outcome (15m)",
		"C"=>1
	],
	"2839"=>[
		"N"=>"Total In The Interval (5m)",
		"C"=>1
	],
	"2841"=>[
		"N"=>"Total In The Interval (10m)",
		"C"=>1
	],
	"2843"=>[
		"N"=>"Total In The Interval (15m)",
		"C"=>1
	],
	"2845"=>[
		"N"=>"Handicap Interval (5m)",
		"C"=>1
	],
	"2847"=>[
		"N"=>"Handicap Interval (10m)",
		"C"=>1
	],
	"2849"=>[
		"N"=>"Handicap Interval (15m)",
		"C"=>1
	],
	"2850"=>[
		"N"=>"Map/Match",
		"C"=>1
	],
	"2851"=>[
		"N"=>"Both Teams To Score Yes/No + Total",
		"C"=>1
	],
	"2852"=>[
		"N"=>"Interval Outcome (5m)",
		"C"=>1
	],
	"2854"=>[
		"N"=>"Asian Handicap",
		"C"=>1
	],
	"2856"=>[
		"N"=>"Aces And Double Faults",
		"C"=>1
	],
	"2858"=>[
		"N"=>"Leader's Margin",
		"C"=>1
	],
	"2860"=>[
		"N"=>"Tie-Breaks, Aces And Double Faults",
		"C"=>2
	],
	"2862"=>[
		"N"=>"Win The Game With The Score",
		"C"=>1
	],
	"2863"=>[
		"N"=>"Leader's Margin In Games",
		"C"=>1
	],
	"2866"=>[
		"N"=>"Team 1 Win To Nil",
		"C"=>1
	],
	"2867"=>[
		"N"=>"Team 2 Win To Nil",
		"C"=>1
	],
	"2868"=>[
		"N"=>"Team 1, First Goal + Match Result",
		"C"=>1
	],
	"2869"=>[
		"N"=>"Team 2, First Goal + Match Result",
		"C"=>1
	],
	"2874"=>[
		"N"=>"Handicap Rounds",
		"C"=>1
	],
	"2876"=>[
		"N"=>"Team 1 To Score N Goals",
		"C"=>1
	],
	"2878"=>[
		"N"=>"Team 2 To Score N Goals",
		"C"=>1
	],
	"2880"=>[
		"N"=>"Team 1 To Score Goals In A Row",
		"C"=>1
	],
	"2882"=>[
		"N"=>"Team 2 To Score Goals In A Row",
		"C"=>1
	],
	"2884"=>[
		"N"=>"Team 1 To Score Goal N From Minute To Minute",
		"C"=>1
	],
	"2886"=>[
		"N"=>"Team 2 To Score Goal N From Minute To Minute",
		"C"=>1
	],
	"2888"=>[
		"N"=>"Team 1 To Score Their Goal In Which Half",
		"C"=>1
	],
	"2890"=>[
		"N"=>"Team 2 To Score Their Goal In Which Half",
		"C"=>1
	],
	"2891"=>[
		"N"=>"Players (Strike-Outs, Pitchers)",
		"C"=>1
	],
	"2893"=>[
		"N"=>"Most Runs In Inning",
		"C"=>1
	],
	"2895"=>[
		"N"=>"Total Home Runs",
		"C"=>1
	],
	"2897"=>[
		"N"=>"Total Minor Penalties",
		"C"=>1
	],
	"2899"=>[
		"N"=>"Players. Total Minor Penalties",
		"C"=>1
	],
	"2901"=>[
		"N"=>"Total Matches",
		"C"=>1
	],
	"2903"=>[
		"N"=>"Team 1 Minor Penalties Total",
		"C"=>1
	],
	"2905"=>[
		"N"=>"Team 2 Minor Penalties Total",
		"C"=>1
	],
	"2907"=>[
		"N"=>"Best Player Of The Year",
		"C"=>1
	],
	"2909"=>[
		"N"=>"Best Young Player Of The Year",
		"C"=>1
	],
	"2911"=>[
		"N"=>"Handicap Yes/No",
		"C"=>2
	],
	"2913"=>[
		"N"=>"To Reach Final",
		"C"=>2
	],
	"2915"=>[
		"N"=>"To Reach Semi-finals",
		"C"=>2
	],
	"2917"=>[
		"N"=>"To Reach Quarter-finals",
		"C"=>2
	],
	"2919"=>[
		"N"=>"2nd Place",
		"C"=>1
	],
	"2921"=>[
		"N"=>"3rd Place",
		"C"=>1
	],
	"2923"=>[
		"N"=>"2nd Place In Group",
		"C"=>1
	],
	"2925"=>[
		"N"=>"3rd Place In Group",
		"C"=>1
	],
	"2927"=>[
		"N"=>"4th Place In Group",
		"C"=>1
	],
	"2929"=>[
		"N"=>"Score After Test Match",
		"C"=>1
	],
	"2931"=>[
		"N"=>"Leader After Test Match (2Way)",
		"C"=>1
	],
	"2933"=>[
		"N"=>"Leader After Test Match (3Way)",
		"C"=>1
	],
	"2935"=>[
		"N"=>"To Lose 1st Set And Win The Match",
		"C"=>1
	],
	"2937"=>[
		"N"=>"Match Result + Correct Score In 1st Set",
		"C"=>2
	],
	"2939"=>[
		"N"=>"Match Result + Correct Score In 2nd Set",
		"C"=>2
	],
	"2941"=>[
		"N"=>"Not To Win Any Set",
		"C"=>1
	],
	"2943"=>[
		"N"=>"Player's Games",
		"C"=>1
	],
	"2945"=>[
		"N"=>"Score After 2 Games",
		"C"=>1
	],
	"2947"=>[
		"N"=>"Score After 4 Games",
		"C"=>1
	],
	"2949"=>[
		"N"=>"Score After 6 Games",
		"C"=>1
	],
	"2951"=>[
		"N"=>"Player's 1st Serve In 1st Game",
		"C"=>1
	],
	"2953"=>[
		"N"=>"Player's 2nd Serve In 1st Game",
		"C"=>1
	],
	"2955"=>[
		"N"=>"Player's 3rd Serve In 1st Game",
		"C"=>1
	],
	"2957"=>[
		"N"=>"Series Leader",
		"C"=>1
	],
	"2959"=>[
		"N"=>"Russia, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2961"=>[
		"N"=>"Switzerland, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2963"=>[
		"N"=>"Slovakia, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2965"=>[
		"N"=>"Norway, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2967"=>[
		"N"=>"Denmark, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2969"=>[
		"N"=>"Germany, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2971"=>[
		"N"=>"France, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2973"=>[
		"N"=>"Kazakhstan, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2975"=>[
		"N"=>"Hungary, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2977"=>[
		"N"=>"Finishing Position Even/Odd",
		"C"=>1
	],
	"2979"=>[
		"N"=>"Duration Of The Map",
		"C"=>1
	],
	"2981"=>[
		"N"=>"1st Place In Group",
		"C"=>1
	],
	"2983"=>[
		"N"=>"Cup Winner",
		"C"=>1
	],
	"2985"=>[
		"N"=>"Contest Winner",
		"C"=>1
	],
	"2987"=>[
		"N"=>"W1 + Total 1",
		"C"=>1
	],
	"2989"=>[
		"N"=>"W2 + Total 2",
		"C"=>1
	],
	"2991"=>[
		"N"=>"USA, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2993"=>[
		"N"=>"Canada, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2995"=>[
		"N"=>"Finland, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2997"=>[
		"N"=>"Czech Republic, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"2999"=>[
		"N"=>"Sweden, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3001"=>[
		"N"=>"Latvia, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3003"=>[
		"N"=>"Belarus, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3005"=>[
		"N"=>"Outcome + Number Of Goals",
		"C"=>1
	],
	"3007"=>[
		"N"=>"Outcome Of The First Half Or The Match",
		"C"=>1
	],
	"3009"=>[
		"N"=>"Match Result + First Half Total",
		"C"=>1
	],
	"3011"=>[
		"N"=>"Total By Halves",
		"C"=>1
	],
	"3013"=>[
		"N"=>"Last Rebound",
		"C"=>1
	],
	"3015"=>[
		"N"=>"Last Ball Possession",
		"C"=>1
	],
	"3017"=>[
		"N"=>"Each Quarter Over",
		"C"=>1
	],
	"3019"=>[
		"N"=>"Each Quarter Under",
		"C"=>1
	],
	"3021"=>[
		"N"=>"Each Half Over",
		"C"=>1
	],
	"3023"=>[
		"N"=>"Each Half Under",
		"C"=>1
	],
	"3025"=>[
		"N"=>"To Lose In 1/4 Finals",
		"C"=>1
	],
	"3027"=>[
		"N"=>"To Lose In 1/8 Finals",
		"C"=>1
	],
	"3029"=>[
		"N"=>"1st Place In The Final Tournament Table",
		"C"=>1
	],
	"3031"=>[
		"N"=>"2nd Place In The Final Tournament Table",
		"C"=>1
	],
	"3033"=>[
		"N"=>"3rd Place In The Final Tournament Table",
		"C"=>1
	],
	"3035"=>[
		"N"=>"4th Place In The Final Tournament Table",
		"C"=>1
	],
	"3037"=>[
		"N"=>"Flawless Victory In Round",
		"C"=>1
	],
	"3041"=>[
		"N"=>"Goals",
		"C"=>1
	],
	"3043"=>[
		"N"=>"Yellow Cards",
		"C"=>1
	],
	"3045"=>[
		"N"=>"Red Cards",
		"C"=>1
	],
	"3047"=>[
		"N"=>"Offsides",
		"C"=>1
	],
	"3049"=>[
		"N"=>"Corners",
		"C"=>1
	],
	"3051"=>[
		"N"=>"Total Dunks",
		"C"=>1
	],
	"3053"=>[
		"N"=>"Basket To Be Scored From Shot Taken From Own Half",
		"C"=>1
	],
	"3055"=>[
		"N"=>"Player Of Which Team Will Score The Most Points",
		"C"=>1
	],
	"3057"=>[
		"N"=>"Number Of Players To Commit More Than 4 Fouls",
		"C"=>1
	],
	"3059"=>[
		"N"=>"Number Of Players To Score More Than 9 Points",
		"C"=>1
	],
	"3061"=>[
		"N"=>"Total Changes Of Leader During The Game",
		"C"=>1
	],
	"3063"=>[
		"N"=>"Maximum Points Difference During The Game",
		"C"=>1
	],
	"3065"=>[
		"N"=>"First Ball Possession",
		"C"=>1
	],
	"3067"=>[
		"N"=>"Player To Score Penalty",
		"C"=>1
	],
	"3069"=>[
		"N"=>"Official Added Time",
		"C"=>1
	],
	"3071"=>[
		"N"=>"Substitution At Half-Time",
		"C"=>1
	],
	"3073"=>[
		"N"=>"Team To Lead After Minute",
		"C"=>1
	],
	"3075"=>[
		"N"=>"Defender To Score First Goal",
		"C"=>1
	],
	"3077"=>[
		"N"=>"Face-offs Won, Percentage",
		"C"=>1
	],
	"3079"=>[
		"N"=>"Team To Win First Face-off",
		"C"=>1
	],
	"3081"=>[
		"N"=>"Draw. Special Markets",
		"C"=>1
	],
	"3083"=>[
		"N"=>"Total Passes Completed",
		"C"=>1
	],
	"3085"=>[
		"N"=>"Total Fouls Committed",
		"C"=>1
	],
	"3087"=>[
		"N"=>"Highest Number Of Red Cards (Group)",
		"C"=>1
	],
	"3089"=>[
		"N"=>"Lowest Number Of Red Cards (Group)",
		"C"=>1
	],
	"3091"=>[
		"N"=>"Player To Score And Team 1 To Win",
		"C"=>1
	],
	"3093"=>[
		"N"=>"Player To Score And Team 2 To Win",
		"C"=>1
	],
	"3095"=>[
		"N"=>"Who Will Score A Goal + Team 1 Result",
		"C"=>1
	],
	"3097"=>[
		"N"=>"Who Will Score A Goal + Team 2 Result",
		"C"=>1
	],
	"3099"=>[
		"N"=>"Distance Covered By A Player (km)",
		"C"=>1
	],
	"3101"=>[
		"N"=>"Distance Covered By A Team (km)",
		"C"=>1
	],
	"3103"=>[
		"N"=>"Team To Receive Highest Number Of Red Cards",
		"C"=>1
	],
	"3105"=>[
		"N"=>"Team To Receive Fastest Red Card",
		"C"=>1
	],
	"3107"=>[
		"N"=>"Sendings Off",
		"C"=>1
	],
	"3109"=>[
		"N"=>"Fastest Event",
		"C"=>1
	],
	"3111"=>[
		"N"=>"Spain, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3113"=>[
		"N"=>"England, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3115"=>[
		"N"=>"Italy, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3117"=>[
		"N"=>"Portugal, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3119"=>[
		"N"=>"Austria, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3121"=>[
		"N"=>"Croatia, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3123"=>[
		"N"=>"Poland, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3125"=>[
		"N"=>"Turkey, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3127"=>[
		"N"=>"Ukraine, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3129"=>[
		"N"=>"Wales, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3131"=>[
		"N"=>"Iceland, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3133"=>[
		"N"=>"Republic of Ireland, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3135"=>[
		"N"=>"Romania, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3137"=>[
		"N"=>"Albania, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3139"=>[
		"N"=>"Northern Ireland, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3141"=>[
		"N"=>"Belgium, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3143"=>[
		"N"=>"Total Goals By Top Goalscorer",
		"C"=>1
	],
	"3145"=>[
		"N"=>"Highest Number Of Yellow Cards (Group)",
		"C"=>1
	],
	"3147"=>[
		"N"=>"Lowest Number Of Yellow Cards (Group)",
		"C"=>1
	],
	"3149"=>[
		"N"=>"Team To Receive Highest Number Of Yellow Cards",
		"C"=>1
	],
	"3151"=>[
		"N"=>"Team To Receive Fastest Yellow Card",
		"C"=>1
	],
	"3153"=>[
		"N"=>"Total Forehand Winners",
		"C"=>1
	],
	"3155"=>[
		"N"=>"Total Backhand Winners",
		"C"=>1
	],
	"3157"=>[
		"N"=>"Total Passing Shots",
		"C"=>1
	],
	"3159"=>[
		"N"=>"Total Overhead Shots",
		"C"=>1
	],
	"3161"=>[
		"N"=>"Total Volleys",
		"C"=>1
	],
	"3163"=>[
		"N"=>"Total Drop Shots",
		"C"=>1
	],
	"3165"=>[
		"N"=>"Total Lobs",
		"C"=>1
	],
	"3167"=>[
		"N"=>"Player 1 Total Forehand Winners",
		"C"=>1
	],
	"3169"=>[
		"N"=>"Player 2 Total Forehand Winners",
		"C"=>1
	],
	"3171"=>[
		"N"=>"Player 1 Total Backhand Winners",
		"C"=>1
	],
	"3173"=>[
		"N"=>"Player 2 Total Backhand Winners",
		"C"=>1
	],
	"3175"=>[
		"N"=>"Player 1 Total Passing Shots",
		"C"=>1
	],
	"3177"=>[
		"N"=>"Player 2 Total Passing Shots",
		"C"=>1
	],
	"3179"=>[
		"N"=>"Player 1 Total Overhead Shots",
		"C"=>1
	],
	"3181"=>[
		"N"=>"Player 2 Total Overhead Shots",
		"C"=>1
	],
	"3183"=>[
		"N"=>"Player 1 Total Volleys",
		"C"=>1
	],
	"3185"=>[
		"N"=>"Player 2 Total Volleys",
		"C"=>1
	],
	"3187"=>[
		"N"=>"Player 1 Total Drop Shots",
		"C"=>1
	],
	"3189"=>[
		"N"=>"Player 2 Total Drop Shots",
		"C"=>1
	],
	"3191"=>[
		"N"=>"Player 1 Total Lobs",
		"C"=>1
	],
	"3193"=>[
		"N"=>"Player 2 Total Lobs",
		"C"=>1
	],
	"3195"=>[
		"N"=>"Two Breaks In A Row",
		"C"=>1
	],
	"3197"=>[
		"N"=>"Total Overtimes",
		"C"=>1
	],
	"3199"=>[
		"N"=>"First To Destroy Inhibitor",
		"C"=>1
	],
	"3201"=>[
		"N"=>"Players' Stats",
		"C"=>1
	],
	"3203"=>[
		"N"=>"Results",
		"C"=>1
	],
	"3205"=>[
		"N"=>"Total Goalkeepers' Red Cards",
		"C"=>1
	],
	"3207"=>[
		"N"=>"Penalty Converted",
		"C"=>1
	],
	"3209"=>[
		"N"=>"Exact Number Of Points In A Game",
		"C"=>1
	],
	"3211"=>[
		"N"=>"Total Points In A Game (2Way)",
		"C"=>1
	],
	"3213"=>[
		"N"=>"Total Points In A Game Even/Odd",
		"C"=>1
	],
	"3215"=>[
		"N"=>"First To Happen In Next Minutes",
		"C"=>1
	],
	"3217"=>[
		"N"=>"Total Shots On Target",
		"C"=>1
	],
	"3219"=>[
		"N"=>"Game Score After 2 Points",
		"C"=>1
	],
	"3221"=>[
		"N"=>"Game Score After 4 Points",
		"C"=>1
	],
	"3223"=>[
		"N"=>"Total Points In A Game (3Way)",
		"C"=>1
	],
	"3225"=>[
		"N"=>"Total Points In Games (3Way)",
		"C"=>1
	],
	"3227"=>[
		"N"=>"Score 40:40 To Happen In Games",
		"C"=>1
	],
	"3229"=>[
		"N"=>"Total Number Of Players Taking Part In The Match",
		"C"=>1
	],
	"3231"=>[
		"N"=>"In Top 2",
		"C"=>1
	],
	"3233"=>[
		"N"=>"To Be Winning In Minute Yes/No",
		"C"=>1
	],
	"3235"=>[
		"N"=>"Leader + Match Result",
		"C"=>1
	],
	"3237"=>[
		"N"=>"First To Take N Corners",
		"C"=>1
	],
	"3239"=>[
		"N"=>"Both Halves To Be Won By Different Teams",
		"C"=>1
	],
	"3241"=>[
		"N"=>"Player To Score In 1st Half",
		"C"=>1
	],
	"3243"=>[
		"N"=>"Player To Score In 2nd Half",
		"C"=>1
	],
	"3245"=>[
		"N"=>"Player To Score From Outside The Penalty Area",
		"C"=>1
	],
	"3247"=>[
		"N"=>"Player To Score More Than Team 2",
		"C"=>1
	],
	"3249"=>[
		"N"=>"Player To Score More Than Team 1",
		"C"=>1
	],
	"3251"=>[
		"N"=>"Team 1 Player To Get Yellow Card For A Foul On Player",
		"C"=>1
	],
	"3253"=>[
		"N"=>"Team 2 Player To Get Yellow Card For A Foul On Player",
		"C"=>1
	],
	"3255"=>[
		"N"=>"Team 1 Player To Be Sent Off For A Foul On Player",
		"C"=>1
	],
	"3257"=>[
		"N"=>"Team 2 Player To Be Sent Off For A Foul On Player",
		"C"=>1
	],
	"3259"=>[
		"N"=>"Player To Score A Penalty",
		"C"=>1
	],
	"3261"=>[
		"N"=>"Shots On Target",
		"C"=>1
	],
	"3263"=>[
		"N"=>"Fouls..",
		"C"=>1
	],
	"3265"=>[
		"N"=>"Correct Score - Group Bet",
		"C"=>1
	],
	"3267"=>[
		"N"=>"Player To Get Red Card",
		"C"=>1
	],
	"3269"=>[
		"N"=>"Player To Score First Goal And Team 1 To Win",
		"C"=>1
	],
	"3271"=>[
		"N"=>"Player To Score First Goal And Team 2 To Win",
		"C"=>1
	],
	"3273"=>[
		"N"=>"Player To Score First Goal And Draw",
		"C"=>1
	],
	"3275"=>[
		"N"=>"Player To Score A Goal And Team 1 To Win",
		"C"=>1
	],
	"3277"=>[
		"N"=>"Player To Score A Goal And Team 2 To Win",
		"C"=>1
	],
	"3279"=>[
		"N"=>"Player To Score A Goal And Draw",
		"C"=>1
	],
	"3281"=>[
		"N"=>"Player To Score Last Goal And Team 1 To Win",
		"C"=>1
	],
	"3283"=>[
		"N"=>"Player To Score Last Goal And Team 2 To Win",
		"C"=>1
	],
	"3285"=>[
		"N"=>"Player To Score Last Goal And Draw",
		"C"=>1
	],
	"3287"=>[
		"N"=>"Player To Score + W1 + Both Teams To Score",
		"C"=>1
	],
	"3289"=>[
		"N"=>"Player To Score + W2 + Both Teams To Score",
		"C"=>1
	],
	"3291"=>[
		"N"=>"Player To Get A Card",
		"C"=>1
	],
	"3293"=>[
		"N"=>"Match With The Fastest Goal",
		"C"=>1
	],
	"3295"=>[
		"N"=>"Team To Score Fastest Goal",
		"C"=>1
	],
	"3297"=>[
		"N"=>"Which Match Will Have Latest Goal",
		"C"=>1
	],
	"3299"=>[
		"N"=>"Which Team Will Score Latest Goal",
		"C"=>1
	],
	"3301"=>[
		"N"=>"How Many Teams Will Score (3Way)",
		"C"=>1
	],
	"3303"=>[
		"N"=>"How Many Teams Will Score (6Way)",
		"C"=>1
	],
	"3305"=>[
		"N"=>"Total Fouls Committed By A Player",
		"C"=>1
	],
	"3307"=>[
		"N"=>"Exact Number Of Goals",
		"C"=>1
	],
	"3309"=>[
		"N"=>"Individual Total 1 Exact Number Of Goals",
		"C"=>1
	],
	"3311"=>[
		"N"=>"Individual Total 2 Exact Number Of Goals",
		"C"=>1
	],
	"3313"=>[
		"N"=>"Exact Number Of Goals",
		"C"=>1
	],
	"3315"=>[
		"N"=>"Individual Total 1 Exact Number Of Goals",
		"C"=>1
	],
	"3317"=>[
		"N"=>"Individual Total 2 Exact Number Of Goals",
		"C"=>1
	],
	"3319"=>[
		"N"=>"Exact Number Of Points",
		"C"=>1
	],
	"3321"=>[
		"N"=>"Exact Number Of Points (3Way)",
		"C"=>1
	],
	"3323"=>[
		"N"=>"Exact Number Of Points (5Way)",
		"C"=>1
	],
	"3325"=>[
		"N"=>"Exact Number Of Points (10Way)",
		"C"=>1
	],
	"3327"=>[
		"N"=>"Individual Total 1 Exact Number Of Points (3Way)",
		"C"=>1
	],
	"3329"=>[
		"N"=>"Individual Total 1 Exact Number Of Points (5Way)",
		"C"=>1
	],
	"3331"=>[
		"N"=>"Individual Total 2 Exact Number Of Points (3Way)",
		"C"=>1
	],
	"3333"=>[
		"N"=>"Individual Total 2 Exact Number Of Points (5Way)",
		"C"=>1
	],
	"3335"=>[
		"N"=>"Penalty, Total In The Interval (5m)",
		"C"=>1
	],
	"3337"=>[
		"N"=>"Heats, Handicap",
		"C"=>1
	],
	"3339"=>[
		"N"=>"Handicap.",
		"C"=>1
	],
	"3341"=>[
		"N"=>"Total Passes",
		"C"=>1
	],
	"3343"=>[
		"N"=>"Total Passes Completed",
		"C"=>1
	],
	"3345"=>[
		"N"=>"Total Matches Where Both Teams Will Score",
		"C"=>1
	],
	"3347"=>[
		"N"=>"Total Matches With TO",
		"C"=>1
	],
	"3349"=>[
		"N"=>"Player To Get Fastest Card",
		"C"=>1
	],
	"3351"=>[
		"N"=>"Exact Number Of Points In A Game (5Way)",
		"C"=>1
	],
	"3353"=>[
		"N"=>"Who Will Win Both Games",
		"C"=>1
	],
	"3355"=>[
		"N"=>"Player's Total Points",
		"C"=>1
	],
	"3357"=>[
		"N"=>"Total Shots In The Direction Of The Goal",
		"C"=>1
	],
	"3359"=>[
		"N"=>"To Happen In Time Interval",
		"C"=>1
	],
	"3361"=>[
		"N"=>"Not To Happen In Time Interval",
		"C"=>1
	],
	"3363"=>[
		"N"=>"First To Happen In Time Interval",
		"C"=>1
	],
	"3365"=>[
		"N"=>"Argentina, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3367"=>[
		"N"=>"Colombia, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3369"=>[
		"N"=>"Mexico, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3371"=>[
		"N"=>"Chile, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3373"=>[
		"N"=>"Peru, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3375"=>[
		"N"=>"Venezuela, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3377"=>[
		"N"=>"Team 1 To Score A Penalty For A Foul On Player",
		"C"=>1
	],
	"3379"=>[
		"N"=>"Team 2 To Score A Penalty For A Foul On Player",
		"C"=>1
	],
	"3381"=>[
		"N"=>"Team Will Not Be Losing",
		"C"=>1
	],
	"3383"=>[
		"N"=>"Total Points On A Tie-Break",
		"C"=>1
	],
	"3385"=>[
		"N"=>"Tie-Break Score After X Points",
		"C"=>1
	],
	"3387"=>[
		"N"=>"Total Points Of Highest Scoring Player",
		"C"=>1
	],
	"3389"=>[
		"N"=>"Maximum Points Scored In A Row",
		"C"=>1
	],
	"3391"=>[
		"N"=>"Number Of Team 1 Players To Score Points",
		"C"=>1
	],
	"3393"=>[
		"N"=>"Number Of Team 2 Players To Score Points",
		"C"=>1
	],
	"3395"=>[
		"N"=>"Team 1 Total Aces",
		"C"=>1
	],
	"3397"=>[
		"N"=>"Team 2 Total Aces",
		"C"=>1
	],
	"3399"=>[
		"N"=>"Team 1 Total Blocks",
		"C"=>1
	],
	"3401"=>[
		"N"=>"Team 2 Total Blocks",
		"C"=>1
	],
	"3403"=>[
		"N"=>"Maximum Points Difference",
		"C"=>1
	],
	"3405"=>[
		"N"=>"Player Total Offsides",
		"C"=>1
	],
	"3407"=>[
		"N"=>"To Win By (3Way).",
		"C"=>1
	],
	"3409"=>[
		"N"=>"Total Penalties Not Scored In Penalty Shootout",
		"C"=>1
	],
	"3411"=>[
		"N"=>"Total Penalties Scored In Penalty Shootout",
		"C"=>1
	],
	"3413"=>[
		"N"=>"Team 1 Total Penalties Taken In Penalty Shootout",
		"C"=>1
	],
	"3415"=>[
		"N"=>"Team 2 Total Penalties Taken In Penalty Shootout",
		"C"=>1
	],
	"3417"=>[
		"N"=>"Team 1 Total Penalties Not Scored In Penalty Shootout",
		"C"=>1
	],
	"3419"=>[
		"N"=>"Team 2 Total Penalties Not Scored In Penalty Shootout",
		"C"=>1
	],
	"3421"=>[
		"N"=>"Team 1 Total Penalties Scored In Penalty Shootout",
		"C"=>1
	],
	"3423"=>[
		"N"=>"Team 2 Total Penalties Scored In Penalty Shootout",
		"C"=>1
	],
	"3425"=>[
		"N"=>"Player To Score Home Run",
		"C"=>1
	],
	"3427"=>[
		"N"=>"Player To Score First Home Run",
		"C"=>1
	],
	"3429"=>[
		"N"=>"Match Result And Wins In Sets",
		"C"=>2
	],
	"3431"=>[
		"N"=>"Both Players To Win At Least One Set",
		"C"=>1
	],
	"3433"=>[
		"N"=>"4 Innings Result/Match Result",
		"C"=>1
	],
	"3435"=>[
		"N"=>"Highest Scoring Inning",
		"C"=>1
	],
	"3437"=>[
		"N"=>"Home Team Will Be Defending In 2nd Half Of 9th Inning",
		"C"=>1
	],
	"3439"=>[
		"N"=>"Even/Odd After Innings",
		"C"=>1
	],
	"3441"=>[
		"N"=>"Will There Be Grand Slam",
		"C"=>1
	],
	"3443"=>[
		"N"=>"Total Number Of Players To Reach Home Base In One Pitch",
		"C"=>1
	],
	"3445"=>[
		"N"=>"A Single Pitcher To Pitch For Whole Match And Prevent The Opposing Team From Scoring A Run",
		"C"=>1
	],
	"3447"=>[
		"N"=>"A Single Pitcher To Pitch For Whole Match And Let The Opposing Team Score A Number Of Runs",
		"C"=>1
	],
	"3451"=>[
		"N"=>"Team 1 Total Strike-Outs",
		"C"=>1
	],
	"3453"=>[
		"N"=>"Team 2 Total Strike-Outs",
		"C"=>1
	],
	"3455"=>[
		"N"=>"Number Of Strike-Outs By Player + Match Result",
		"C"=>1
	],
	"3457"=>[
		"N"=>"First Shot On Target In Time Interval",
		"C"=>1
	],
	"3459"=>[
		"N"=>"Handicap, Percentage Of Short Passes Completed",
		"C"=>1
	],
	"3461"=>[
		"N"=>"Handicap, Percentage Of Medium Passes Completed",
		"C"=>1
	],
	"3463"=>[
		"N"=>"Handicap, Percentage Of Long Passes Completed",
		"C"=>1
	],
	"3465"=>[
		"N"=>"Team Total, Percentage Of Short Passes Completed",
		"C"=>1
	],
	"3467"=>[
		"N"=>"Team Total, Percentage Of Medium Passes Completed",
		"C"=>1
	],
	"3469"=>[
		"N"=>"Team Total, Percentage Of Long Passes Completed",
		"C"=>1
	],
	"3471"=>[
		"N"=>"Handicap, Percentage Of Passes Completed",
		"C"=>1
	],
	"3473"=>[
		"N"=>"Team Total, Percentage Of Passes Completed",
		"C"=>1
	],
	"3475"=>[
		"N"=>"Team To Make Last Substitution",
		"C"=>1
	],
	"3477"=>[
		"N"=>"Player, Total Long Passes Completed",
		"C"=>1
	],
	"3479"=>[
		"N"=>"Player, Total Medium Passes Completed",
		"C"=>1
	],
	"3481"=>[
		"N"=>"Player, Total Short Passes Completed",
		"C"=>1
	],
	"3483"=>[
		"N"=>"Team Total, Attempts On Target Directly From Free Kick",
		"C"=>1
	],
	"3485"=>[
		"N"=>"Team Total, Solo Runs Into Penalty Area",
		"C"=>1
	],
	"3487"=>[
		"N"=>"Team Total, Runs Into Penalty Area",
		"C"=>1
	],
	"3489"=>[
		"N"=>"Team Total, Solo Runs Into The Attacking Third",
		"C"=>1
	],
	"3491"=>[
		"N"=>"Team Total, Runs Into The Attacking Third",
		"C"=>1
	],
	"3493"=>[
		"N"=>"Goals In A Row By A Single Team",
		"C"=>1
	],
	"3495"=>[
		"N"=>"Players, Total Passes To Each Other",
		"C"=>1
	],
	"3497"=>[
		"N"=>"Yellow Card For A Foul On Player",
		"C"=>1
	],
	"3499"=>[
		"N"=>"Penalty For A Foul On Player",
		"C"=>1
	],
	"3501"=>[
		"N"=>"Player 1 To Score, Player 2 Not To Score",
		"C"=>1
	],
	"3503"=>[
		"N"=>"Players, Equal Number Of Goals",
		"C"=>1
	],
	"3505"=>[
		"N"=>"Goalkeeper To Be Sent Off",
		"C"=>1
	],
	"3507"=>[
		"N"=>"Goalkeeper To Get Yellow Card",
		"C"=>1
	],
	"3509"=>[
		"N"=>"Team To Score First Penalty",
		"C"=>1
	],
	"3511"=>[
		"N"=>"Score In One Of The Sets",
		"C"=>1
	],
	"3513"=>[
		"N"=>"How Will First Point Be Won",
		"C"=>1
	],
	"3515"=>[
		"N"=>"Match Postponed Due To Rain",
		"C"=>1
	],
	"3517"=>[
		"N"=>"Correct Score - Group Bet.",
		"C"=>1
	],
	"3519"=>[
		"N"=>"Correct Score - Group Bet..",
		"C"=>1
	],
	"3521"=>[
		"N"=>"Correct Score - Group Bet...",
		"C"=>1
	],
	"3523"=>[
		"N"=>"Player 1, Result + Total",
		"C"=>1
	],
	"3525"=>[
		"N"=>"Player 2, Result + Total",
		"C"=>1
	],
	"3527"=>[
		"N"=>"Time Of First Tower Falling",
		"C"=>1
	],
	"3529"=>[
		"N"=>"Who Will Take First Tower",
		"C"=>1
	],
	"3531"=>[
		"N"=>"Will Hero Be Picked",
		"C"=>1
	],
	"3533"=>[
		"N"=>"Fatality In Round",
		"C"=>1
	],
	"3535"=>[
		"N"=>"Total Shots",
		"C"=>1
	],
	"3537"=>[
		"N"=>"Total Shots Even/Odd",
		"C"=>1
	],
	"3539"=>[
		"N"=>"Player, Total Shots",
		"C"=>1
	],
	"3541"=>[
		"N"=>"Player, Total Shots Even/Odd",
		"C"=>1
	],
	"3543"=>[
		"N"=>"To Take 1st And 2nd Places In The Final Table",
		"C"=>1
	],
	"3545"=>[
		"N"=>"World Champion",
		"C"=>1
	],
	"3547"=>[
		"N"=>"Place In The Final Table",
		"C"=>1
	],
	"3549"=>[
		"N"=>"Last Place",
		"C"=>1
	],
	"3551"=>[
		"N"=>"Best Batsman's Team",
		"C"=>1
	],
	"3553"=>[
		"N"=>"Leader After Laps",
		"C"=>1
	],
	"3555"=>[
		"N"=>"Players. Total Wickets",
		"C"=>1
	],
	"3557"=>[
		"N"=>"In Top 12",
		"C"=>1
	],
	"3559"=>[
		"N"=>"Next Goal, Handicap",
		"C"=>1
	],
	"3561"=>[
		"N"=>"Next Goal, Double Chance",
		"C"=>1
	],
	"3563"=>[
		"N"=>"Method Of First Batsman Dismissal",
		"C"=>1
	],
	"3565"=>[
		"N"=>"Egypt, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3567"=>[
		"N"=>"Slovenia, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3569"=>[
		"N"=>"Brazil, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3571"=>[
		"N"=>"Tunisia, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3573"=>[
		"N"=>"Qatar, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3575"=>[
		"N"=>"South Korea, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3577"=>[
		"N"=>"Angola, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3579"=>[
		"N"=>"Netherlands, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3581"=>[
		"N"=>"Montenegro, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"3583"=>[
		"N"=>"Total Sets Won",
		"C"=>1
	],
	"3585"=>[
		"N"=>"Total Sets Lost",
		"C"=>1
	],
	"3587"=>[
		"N"=>"First Replay Challenge",
		"C"=>1
	],
	"3589"=>[
		"N"=>"First Serve Fault",
		"C"=>1
	],
	"3591"=>[
		"N"=>"First To Win A Point By Ace",
		"C"=>1
	],
	"3593"=>[
		"N"=>"First To Win A Point By Blocking",
		"C"=>1
	],
	"3595"=>[
		"N"=>"Sets With The Same Total",
		"C"=>1
	],
	"3599"=>[
		"N"=>"Total Serve Faults",
		"C"=>1
	],
	"3601"=>[
		"N"=>"Last Point In The Match",
		"C"=>1
	],
	"3603"=>[
		"N"=>"Total Goals In Power Play",
		"C"=>1
	],
	"3605"=>[
		"N"=>"Total Short-Handed Goals",
		"C"=>1
	],
	"3607"=>[
		"N"=>"Total Fast Break Goals",
		"C"=>1
	],
	"3609"=>[
		"N"=>"Team 1, Тotal Goals In Fast Break",
		"C"=>1
	],
	"3611"=>[
		"N"=>"Team 2, Тotal Goals In Fast Break",
		"C"=>1
	],
	"3613"=>[
		"N"=>"Number Of Minutes Without A Goal",
		"C"=>1
	],
	"3615"=>[
		"N"=>"Team 1, Number Of Minutes Without A Goal",
		"C"=>1
	],
	"3617"=>[
		"N"=>"Team 2, Number Of Minutes Without A Goal",
		"C"=>1
	],
	"3619"=>[
		"N"=>"Last Offside",
		"C"=>1
	],
	"3621"=>[
		"N"=>"Last Shot On Target",
		"C"=>1
	],
	"3623"=>[
		"N"=>"How Many Sets In A Row Will A Team Win",
		"C"=>1
	],
	"3625"=>[
		"N"=>"Which Color Medals Will A Team Get Most",
		"C"=>1
	],
	"4626"=>[
		"N"=>"Players. Total Runs Even/Odd",
		"C"=>1
	],
	"5627"=>[
		"N"=>"Individual Total 1, First Pair Of Batsmen",
		"C"=>1
	],
	"5629"=>[
		"N"=>"Individual Total 2, First Pair Of Batsmen",
		"C"=>1
	],
	"6630"=>[
		"N"=>"Team 2 Best Bowler",
		"C"=>1
	],
	"6631"=>[
		"N"=>"Team 1, Number Of Runs By Player",
		"C"=>1
	],
	"6632"=>[
		"N"=>"Team 2, Number Of Runs By Player",
		"C"=>1
	],
	"6633"=>[
		"N"=>"Team 1, Total Runs By Player",
		"C"=>1
	],
	"6635"=>[
		"N"=>"Team 2, Total Runs By Player",
		"C"=>1
	],
	"6637"=>[
		"N"=>"Team 1, Total Runs By Player Even/Odd",
		"C"=>1
	],
	"6639"=>[
		"N"=>"Team 2, Total Runs By Player Even/Odd",
		"C"=>1
	],
	"6641"=>[
		"N"=>"Team To Take Last Penalty",
		"C"=>1
	],
	"6643"=>[
		"N"=>"Total Maps Even/Odd",
		"C"=>1
	],
	"6661"=>[
		"N"=>"Team 1, Player To Score Home Run",
		"C"=>1
	],
	"6663"=>[
		"N"=>"Team 2, Player To Score Home Run",
		"C"=>1
	],
	"6665"=>[
		"N"=>"Team 1, Player To Score First Home Run",
		"C"=>1
	],
	"6667"=>[
		"N"=>"Team 2, Player To Score First Home Run",
		"C"=>1
	],
	"6669"=>[
		"N"=>"At Least One Medal",
		"C"=>1
	],
	"6671"=>[
		"N"=>"Will There Be Deuce",
		"C"=>1
	],
	"6673"=>[
		"N"=>"At Least One Gold Medal",
		"C"=>1
	],
	"6675"=>[
		"N"=>"Will The Base Be Captured",
		"C"=>1
	],
	"6677"=>[
		"N"=>"Team 1 To Capture The Base",
		"C"=>1
	],
	"6679"=>[
		"N"=>"Team 2 To Capture The Base",
		"C"=>1
	],
	"6681"=>[
		"N"=>"To Score In Both Halves",
		"C"=>1
	],
	"6683"=>[
		"N"=>"Team To Score More Goals",
		"C"=>1
	],
	"6685"=>[
		"N"=>"Race To (2Way)",
		"C"=>1
	],
	"6687"=>[
		"N"=>"Player, Total Attacks",
		"C"=>1
	],
	"6689"=>[
		"N"=>"Player, Total Serves",
		"C"=>1
	],
	"6691"=>[
		"N"=>"Player, Total Attack Points",
		"C"=>1
	],
	"6693"=>[
		"N"=>"Player, Total Points By Blocking",
		"C"=>1
	],
	"6695"=>[
		"N"=>"Player, Total Attack Faults",
		"C"=>1
	],
	"6697"=>[
		"N"=>"Player, Total Serve Faults",
		"C"=>1
	],
	"6699"=>[
		"N"=>"Player, Total Blocking Faults",
		"C"=>1
	],
	"6701"=>[
		"N"=>"Player, Total Blocking Attempts",
		"C"=>1
	],
	"6703"=>[
		"N"=>"Player, Total Aces",
		"C"=>1
	],
	"6705"=>[
		"N"=>"Team 1, Total Players Who Scored In The Match",
		"C"=>1
	],
	"6707"=>[
		"N"=>"Team 2, Total Players Who Scored In The Match",
		"C"=>1
	],
	"6709"=>[
		"N"=>"Total Used Shuttlecocks",
		"C"=>1
	],
	"6711"=>[
		"N"=>"Correct Score In Any Set",
		"C"=>1
	],
	"6713"=>[
		"N"=>"Correct Score In Set",
		"C"=>1
	],
	"6715"=>[
		"N"=>"Percentage of Good Receptions",
		"C"=>1
	],
	"6717"=>[
		"N"=>"Percentage of Perfect Receptions",
		"C"=>1
	],
	"6719"=>[
		"N"=>"Rider From Which Country Will Win Stage",
		"C"=>1
	],
	"6721"=>[
		"N"=>"Yellow Jersey Winner",
		"C"=>1
	],
	"6723"=>[
		"N"=>"Total Innings Won In A Row",
		"C"=>1
	],
	"6725"=>[
		"N"=>"Team 1, Exact Number Of Scoring Innings",
		"C"=>1
	],
	"6727"=>[
		"N"=>"Team 2, Exact Number Of Scoring Innings",
		"C"=>1
	],
	"6729"=>[
		"N"=>"Team 1, Total Scoring Innings",
		"C"=>1
	],
	"6731"=>[
		"N"=>"Team 2, Total Scoring Innings",
		"C"=>1
	],
	"6733"=>[
		"N"=>"Exact Number Of Drawn Innings",
		"C"=>1
	],
	"6735"=>[
		"N"=>"Total Drawn Innings",
		"C"=>1
	],
	"6737"=>[
		"N"=>"Team 1, Exact Number Of Winning Innings",
		"C"=>1
	],
	"6739"=>[
		"N"=>"Team 2, Exact Number Of Winning Innings",
		"C"=>1
	],
	"6741"=>[
		"N"=>"Team 1, Total Winning Innings",
		"C"=>1
	],
	"6743"=>[
		"N"=>"Team 2, Total Winning Innings",
		"C"=>1
	],
	"6745"=>[
		"N"=>"Exact Number Of Innings With Total Over 1.5",
		"C"=>1
	],
	"6747"=>[
		"N"=>"Total Innings With Total Over 1.5",
		"C"=>1
	],
	"6749"=>[
		"N"=>"Winner By Points",
		"C"=>1
	],
	"6751"=>[
		"N"=>"Winner Of The Mountains Classification",
		"C"=>1
	],
	"6753"=>[
		"N"=>"Winner Of The Team Classification",
		"C"=>1
	],
	"6755"=>[
		"N"=>"SuperTotal",
		"C"=>1
	],
	"6757"=>[
		"N"=>"SuperHandicap",
		"C"=>1
	],
	"6759"=>[
		"N"=>"Divisibility Of The Drawn Number",
		"C"=>1
	],
	"6761"=>[
		"N"=>"The Lowest Drawn Number",
		"C"=>1
	],
	"6763"=>[
		"N"=>"The Highest Drawn Number",
		"C"=>1
	],
	"6765"=>[
		"N"=>"Total Odd Drawn Numbers",
		"C"=>1
	],
	"6767"=>[
		"N"=>"Total Even Drawn Numbers",
		"C"=>1
	],
	"6769"=>[
		"N"=>"The Sum Of All Drawn Numbers",
		"C"=>1
	],
	"6771"=>[
		"N"=>"The Lowest Drawn Number Even/Odd",
		"C"=>1
	],
	"6773"=>[
		"N"=>"The Highest Drawn Number Even/Odd",
		"C"=>1
	],
	"6775"=>[
		"N"=>"The Sum Of The Lowest And The Highest Drawn Numbers Even/Odd",
		"C"=>1
	],
	"6777"=>[
		"N"=>"Total Drawn Numbers",
		"C"=>1
	],
	"6779"=>[
		"N"=>"A Number To Be Drawn",
		"C"=>1
	],
	"6781"=>[
		"N"=>"Successive Numbers To Be Drawn",
		"C"=>1
	],
	"6783"=>[
		"N"=>"Identical Numbers To Be Drawn",
		"C"=>1
	],
	"6785"=>[
		"N"=>"First Drawn Number To Be Higher Than The Last Drawn Number",
		"C"=>1
	],
	"6787"=>[
		"N"=>"More Even Numbers To Be Drawn Than Odd Numbers",
		"C"=>1
	],
	"6789"=>[
		"N"=>"The Sum Of All Drawn Even Numbers Higher Than The Sum Of All Drawn Odd Numbers",
		"C"=>1
	],
	"6791"=>[
		"N"=>"The Sum Of All Drawn Numbers, Range",
		"C"=>1
	],
	"6793"=>[
		"N"=>"The Sum Of All Drawn Even Numbers",
		"C"=>1
	],
	"6795"=>[
		"N"=>"The Sum Of All Drawn Odd Numbers",
		"C"=>1
	],
	"6797"=>[
		"N"=>"The Sum Of The Lowest And The Highest Drawn Numbers",
		"C"=>1
	],
	"6799"=>[
		"N"=>"Total, Number",
		"C"=>1
	],
	"6801"=>[
		"N"=>"Even/Odd, Number",
		"C"=>1
	],
	"6803"=>[
		"N"=>"The Difference Between The Highest And The Lowest Drawn Numbers",
		"C"=>1
	],
	"6805"=>[
		"N"=>"Number Will Be Among Extra Numbers",
		"C"=>1
	],
	"6807"=>[
		"N"=>"The Lowest Of The Extra Numbers",
		"C"=>1
	],
	"6809"=>[
		"N"=>"The Sum Of The Extra Numbers",
		"C"=>1
	],
	"6811"=>[
		"N"=>"To Reach 1/16 Finals",
		"C"=>1
	],
	"6813"=>[
		"N"=>"Bonus Number Even/Odd",
		"C"=>1
	],
	"6815"=>[
		"N"=>"Bonus Number",
		"C"=>1
	],
	"6817"=>[
		"N"=>"Number Of Ball Higher Than Number Of Bonus Ball",
		"C"=>1
	],
	"6819"=>[
		"N"=>"Extra Number",
		"C"=>1
	],
	"6821"=>[
		"N"=>"Team 1, Total Runs",
		"C"=>1
	],
	"6823"=>[
		"N"=>"Team 2, Total Runs",
		"C"=>1
	],
	"6825"=>[
		"N"=>"Result From First Ball Of The Match",
		"C"=>1
	],
	"6827"=>[
		"N"=>"Last Number",
		"C"=>1
	],
	"6829"=>[
		"N"=>"The Sum Of All Drawn Numbers.",
		"C"=>1
	],
	"6831"=>[
		"N"=>"The Lowest Drawn Number.",
		"C"=>1
	],
	"6833"=>[
		"N"=>"The Highest Drawn Number.",
		"C"=>1
	],
	"6835"=>[
		"N"=>"Total Drawn Numbers",
		"C"=>1
	],
	"6837"=>[
		"N"=>"The Sum Of All Drawn Even Numbers Lower Than The Sum Of All Drawn Odd Numbers",
		"C"=>1
	],
	"6839"=>[
		"N"=>"The Sum Of All Drawn Even Numbers Equal To The Sum Of All Drawn Odd Numbers",
		"C"=>1
	],
	"6841"=>[
		"N"=>"The Sum Of The Lowest And The Highest Drawn Numbers.",
		"C"=>1
	],
	"6843"=>[
		"N"=>"More Odd Numbers To Be Drawn Than Even Numbers",
		"C"=>1
	],
	"6845"=>[
		"N"=>"Equal Number Of Even And Odd Numbers To Be Drawn",
		"C"=>1
	],
	"6847"=>[
		"N"=>"Last Number Even/Odd",
		"C"=>1
	],
	"6849"=>[
		"N"=>"The Sum Of All Drawn Numbers Even/Odd",
		"C"=>1
	],
	"6851"=>[
		"N"=>"Total Even Numbers Even/Odd",
		"C"=>1
	],
	"6853"=>[
		"N"=>"Total Odd Numbers Even/Odd",
		"C"=>1
	],
	"6855"=>[
		"N"=>"Team Not To Lose Any Home Matches",
		"C"=>1
	],
	"6857"=>[
		"N"=>"Player To Score First Goal In Europa League",
		"C"=>1
	],
	"6859"=>[
		"N"=>"Team To Concede In All Matches",
		"C"=>1
	],
	"6861"=>[
		"N"=>"Team Not To Score In Any Away Matches",
		"C"=>1
	],
	"6863"=>[
		"N"=>"Team Not To Score A Single Goal",
		"C"=>1
	],
	"6865"=>[
		"N"=>"Team To Score In All Home Matches",
		"C"=>1
	],
	"6867"=>[
		"N"=>"Team To Score In All Matches",
		"C"=>1
	],
	"6869"=>[
		"N"=>"Team To Score In All Away Matches",
		"C"=>1
	],
	"6871"=>[
		"N"=>"Team To Concede In All Away Matches",
		"C"=>1
	],
	"6873"=>[
		"N"=>"Team To Concede In All Home Matches",
		"C"=>1
	],
	"6875"=>[
		"N"=>"Total Goals Conceded By Team",
		"C"=>1
	],
	"6877"=>[
		"N"=>"Total Goals Conceded By Team In All Matches",
		"C"=>1
	],
	"6879"=>[
		"N"=>"Team 1 To Lose To Team 2 Home And Away",
		"C"=>1
	],
	"6881"=>[
		"N"=>"Penalty For A Foul By Player",
		"C"=>1
	],
	"6883"=>[
		"N"=>"Player To Save Penalty",
		"C"=>1
	],
	"6885"=>[
		"N"=>"Player To Score Against",
		"C"=>1
	],
	"6887"=>[
		"N"=>"Player To Be Sent Off",
		"C"=>1
	],
	"6889"=>[
		"N"=>"Top Team Goalscorer",
		"C"=>1
	],
	"6891"=>[
		"N"=>"Total Sendings Off",
		"C"=>1
	],
	"6893"=>[
		"N"=>"Total Draws",
		"C"=>1
	],
	"6895"=>[
		"N"=>"Total Defeats",
		"C"=>1
	],
	"6897"=>[
		"N"=>"Total Penalties Taken",
		"C"=>1
	],
	"6899"=>[
		"N"=>"Total Wins",
		"C"=>1
	],
	"6901"=>[
		"N"=>"Total Yellow Cards",
		"C"=>1
	],
	"6903"=>[
		"N"=>"Total Headed Goals",
		"C"=>1
	],
	"6905"=>[
		"N"=>"Total Goals From Direct Free Kicks",
		"C"=>1
	],
	"6907"=>[
		"N"=>"First Extra Number Even|Odd",
		"C"=>1
	],
	"6909"=>[
		"N"=>"First Extra Number",
		"C"=>1
	],
	"6911"=>[
		"N"=>"Last Extra Number Even|Odd",
		"C"=>1
	],
	"6913"=>[
		"N"=>"Last Extra Number",
		"C"=>1
	],
	"6915"=>[
		"N"=>"Comparison Between The First And Second Ball",
		"C"=>1
	],
	"6917"=>[
		"N"=>"Total Drawn Numbers, Interval",
		"C"=>1
	],
	"6919"=>[
		"N"=>"Winner Of The Race",
		"C"=>1
	],
	"6921"=>[
		"N"=>"Finishing Position In The Race",
		"C"=>1
	],
	"6923"=>[
		"N"=>"Best Time In The Race",
		"C"=>1
	],
	"6925"=>[
		"N"=>"Maximum Speed",
		"C"=>1
	],
	"6927"=>[
		"N"=>"Total Goals Scored By Team In A Match",
		"C"=>1
	],
	"6929"=>[
		"N"=>"Total Empty Net Goals Conceded By A Team",
		"C"=>1
	],
	"6931"=>[
		"N"=>"Total Goals Scored By Team From 1st To 3rd Minute",
		"C"=>1
	],
	"6933"=>[
		"N"=>"Total Shutouts By Team",
		"C"=>1
	],
	"6935"=>[
		"N"=>"Total Victories With Difference Of 3 Or More Goals",
		"C"=>1
	],
	"6937"=>[
		"N"=>"Total Defeats With Difference Of 3 Or More Goals",
		"C"=>1
	],
	"6939"=>[
		"N"=>"Total Shots In Match",
		"C"=>1
	],
	"6941"=>[
		"N"=>"Minimum Total Penalty Time In Match (Only 2 Min)",
		"C"=>1
	],
	"6943"=>[
		"N"=>"Maximum Total Penalty Time In Match (Only 2 Min)",
		"C"=>1
	],
	"6945"=>[
		"N"=>"Total Empty Net Goals Scored By Team",
		"C"=>1
	],
	"6947"=>[
		"N"=>"Player's First Break Point",
		"C"=>1
	],
	"6949"=>[
		"N"=>"Duration Of The Match",
		"C"=>1
	],
	"6951"=>[
		"N"=>"Goalkeeper To Touch The Puck In The First Minute Of The Match",
		"C"=>1
	],
	"6953"=>[
		"N"=>"Player, Shots On Goal",
		"C"=>1
	],
	"6955"=>[
		"N"=>"Player, Total Face-offs Won",
		"C"=>1
	],
	"6957"=>[
		"N"=>"Player, Total Penalty Time",
		"C"=>1
	],
	"6959"=>[
		"N"=>"Europe, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"6961"=>[
		"N"=>"North America, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"6963"=>[
		"N"=>"Correct Score. Special Bets",
		"C"=>1
	],
	"6965"=>[
		"N"=>"First To Happen.",
		"C"=>1
	],
	"6967"=>[
		"N"=>"First To Happen - Throw-in Or Foul.",
		"C"=>1
	],
	"6969"=>[
		"N"=>"First To Happen - Corner Or Goal Kick.",
		"C"=>1
	],
	"6971"=>[
		"N"=>"First To Happen - Offside Or Card.",
		"C"=>1
	],
	"6973"=>[
		"N"=>"Minute Of First Scored Goal Even/Odd",
		"C"=>1
	],
	"6975"=>[
		"N"=>"Minute Of First 2 Minute Penalty Even/Odd",
		"C"=>1
	],
	"6977"=>[
		"N"=>"Delayed Penalty To Be Served",
		"C"=>1
	],
	"6979"=>[
		"N"=>"Player To Score And Provide An Assist",
		"C"=>1
	],
	"6981"=>[
		"N"=>"Both Players To Score And Team 1 To Win",
		"C"=>1
	],
	"6983"=>[
		"N"=>"Both Players To Score And Team 2 To Win",
		"C"=>1
	],
	"6984"=>[
		"N"=>"Hits + Runs + Errors",
		"C"=>1
	],
	"6987"=>[
		"N"=>"First Replay Challenge To Be Successful",
		"C"=>1
	],
	"6989"=>[
		"N"=>"Godlike Streak",
		"C"=>1
	],
	"6991"=>[
		"N"=>"Courier Kill",
		"C"=>1
	],
	"6993"=>[
		"N"=>"Team 1 To Win At Least One Map",
		"C"=>1
	],
	"6995"=>[
		"N"=>"Team 2 To Win At Least One Map",
		"C"=>1
	],
	"6997"=>[
		"N"=>"Both Drivers Will Be Placed",
		"C"=>1
	],
	"6999"=>[
		"N"=>"Both Drivers Of The Team Will Be Classified",
		"C"=>1
	],
	"7001"=>[
		"N"=>"Both Teams To Score And Match Total",
		"C"=>1
	],
	"7003"=>[
		"N"=>"Team 1, Last Number In The Score",
		"C"=>1
	],
	"7005"=>[
		"N"=>"Team 2, Last Number In The Score",
		"C"=>1
	],
	"7007"=>[
		"N"=>"Match Total And Even/Odd",
		"C"=>1
	],
	"7009"=>[
		"N"=>"Both Teams To Score And Double Chance",
		"C"=>1
	],
	"7011"=>[
		"N"=>"Double Chance And Total",
		"C"=>1
	],
	"7013"=>[
		"N"=>"Ace",
		"C"=>1
	],
	"7015"=>[
		"N"=>"Knife Kill",
		"C"=>1
	],
	"7017"=>[
		"N"=>"Molotov Cocktail Kill",
		"C"=>1
	],
	"7019"=>[
		"N"=>"Open Frag",
		"C"=>1
	],
	"7020"=>[
		"N"=>"Total Knife Kills",
		"C"=>1
	],
	"7021"=>[
		"N"=>"Total Nashors Beaten",
		"C"=>1
	],
	"7022"=>[
		"N"=>"Total Dragons Beaten",
		"C"=>1
	],
	"7023"=>[
		"N"=>"Total Towers Taken",
		"C"=>1
	],
	"7025"=>[
		"N"=>"Team 1, Exact Number Of Maps Won",
		"C"=>1
	],
	"7027"=>[
		"N"=>"Team 2, Exact Number Of Maps Won",
		"C"=>1
	],
	"7029"=>[
		"N"=>"Win With Difference In Regular Time (3Way)",
		"C"=>1
	],
	"7031"=>[
		"N"=>"Win With Difference In Regular Time (3Way).",
		"C"=>1
	],
	"7032"=>[
		"N"=>"Team 1 To Win Both Rounds",
		"C"=>1
	],
	"7033"=>[
		"N"=>"Team 2 To Win Both Rounds",
		"C"=>1
	],
	"7035"=>[
		"N"=>"Player, Total Points + Rebounds",
		"C"=>1
	],
	"7037"=>[
		"N"=>"Player, Total Points + Assists",
		"C"=>1
	],
	"7039"=>[
		"N"=>"Player, Total Points + Rebounds + Assists",
		"C"=>1
	],
	"7041"=>[
		"N"=>"Player, Total Assists",
		"C"=>1
	],
	"7043"=>[
		"N"=>"Player Will Open The Scoring + Result",
		"C"=>1
	],
	"7045"=>[
		"N"=>"Knife Kill - Yes/No",
		"C"=>1
	],
	"7047"=>[
		"N"=>"Molotov Kill - Yes/No",
		"C"=>1
	],
	"7049"=>[
		"N"=>"HE Grenade Kill - Yes/No",
		"C"=>1
	],
	"7051"=>[
		"N"=>"Total Bouts Ending By Submission/Painful Locks/Chokeholds",
		"C"=>1
	],
	"7053"=>[
		"N"=>"Total Bouts Ending By KO/TKO",
		"C"=>1
	],
	"7055"=>[
		"N"=>"Total Bouts To Go The Distance",
		"C"=>1
	],
	"7057"=>[
		"N"=>"Bout Will End In The First 60 Seconds Of 1st Round",
		"C"=>1
	],
	"7059"=>[
		"N"=>"How The Bout Will Be Won",
		"C"=>1
	],
	"7061"=>[
		"N"=>"In Which Round Will The Bout Be Won",
		"C"=>1
	],
	"7067"=>[
		"N"=>"In Which Round Will The Bout Be Won",
		"C"=>1
	],
	"7069"=>[
		"N"=>"In Which Round Will Fighter 1 Win The Bout By Submission/Painful Lock/Chokehold",
		"C"=>1
	],
	"7071"=>[
		"N"=>"In Which Round Will Fighter 2 Win The Bout By Submission/Painful Lock/Chokehold",
		"C"=>1
	],
	"7073"=>[
		"N"=>"In Which Round Will Fighter 1 Win The Bout By KO/TKO/DQ",
		"C"=>1
	],
	"7075"=>[
		"N"=>"In Which Round Will Fighter 2 Win The Bout By KO/TKO/DQ",
		"C"=>1
	],
	"7077"=>[
		"N"=>"Win Inside The Distance",
		"C"=>1
	],
	"7079"=>[
		"N"=>"Total Moves",
		"C"=>1
	],
	"7081"=>[
		"N"=>"First Captured Piece",
		"C"=>1
	],
	"7083"=>[
		"N"=>"Last Captured Piece",
		"C"=>1
	],
	"7084"=>[
		"N"=>"Player 1 Total Moves",
		"C"=>1
	],
	"7085"=>[
		"N"=>"Player 1 Total Moves Even|Odd",
		"C"=>1
	],
	"7087"=>[
		"N"=>"Player 2 Total Moves",
		"C"=>1
	],
	"7089"=>[
		"N"=>"Player 2 Total Moves Even|Odd",
		"C"=>1
	],
	"7090"=>[
		"N"=>"Total Moves Even|Odd",
		"C"=>1
	],
	"7091"=>[
		"N"=>"Total Promotions",
		"C"=>1
	],
	"7093"=>[
		"N"=>"Total Chess Pieces At The End Of The Match (Incl Kings)",
		"C"=>1
	],
	"7095"=>[
		"N"=>"Win On Time, One Of The Players",
		"C"=>1
	],
	"7097"=>[
		"N"=>"First Check - Player",
		"C"=>1
	],
	"7099"=>[
		"N"=>"Match Handicap",
		"C"=>1
	],
	"7102"=>[
		"N"=>"Team 1, Player's Total Frags In Round",
		"C"=>1
	],
	"7103"=>[
		"N"=>"Team 2, Player's Total Frags In Round",
		"C"=>1
	],
	"7105"=>[
		"N"=>"To Be Knocked Down",
		"C"=>1
	],
	"7107"=>[
		"N"=>"Knockdown Or KO In First Round",
		"C"=>1
	],
	"7109"=>[
		"N"=>"To Be Knocked Down And Win",
		"C"=>1
	],
	"7111"=>[
		"N"=>"First To Knock Down Opponent",
		"C"=>1
	],
	"7113"=>[
		"N"=>"Both Fighters To Be Knocked Down",
		"C"=>1
	],
	"7115"=>[
		"N"=>"Knockout",
		"C"=>1
	],
	"7116"=>[
		"N"=>"First Move - \"White\"",
		"C"=>1
	],
	"7117"=>[
		"N"=>"First Move - \"Black\"",
		"C"=>1
	],
	"7119"=>[
		"N"=>"Team To Score Earliest First Goal",
		"C"=>1
	],
	"7120"=>[
		"N"=>"Yellow Card In Both Halves",
		"C"=>1
	],
	"7121"=>[
		"N"=>"Yellow Cards In A Row By A Single Team",
		"C"=>1
	],
	"7122"=>[
		"N"=>"Yellow Card For A Goal Celebration",
		"C"=>1
	],
	"7123"=>[
		"N"=>"Player To Open The Scoring",
		"C"=>1
	],
	"7124"=>[
		"N"=>"Player's Total + Result",
		"C"=>1
	],
	"7125"=>[
		"N"=>"Two Double Faults In A Row",
		"C"=>1
	],
	"7126"=>[
		"N"=>"First Player To Get A Break Point",
		"C"=>1
	],
	"7127"=>[
		"N"=>"Total Knights At The End Of The Match",
		"C"=>1
	],
	"7129"=>[
		"N"=>"Total Rooks At The End Of The Match",
		"C"=>1
	],
	"7131"=>[
		"N"=>"Total Pawns At The End Of The Match",
		"C"=>1
	],
	"7133"=>[
		"N"=>"Total Bishops At The End Of The Match",
		"C"=>1
	],
	"7135"=>[
		"N"=>"Total Queens At The End Of The Match",
		"C"=>1
	],
	"7137"=>[
		"N"=>"Which Player Will Open The Scoring + W1",
		"C"=>1
	],
	"7139"=>[
		"N"=>"Which Player Will Open The Scoring + W2",
		"C"=>1
	],
	"7141"=>[
		"N"=>"Which Player Will Be The First In Their Team To Score Points",
		"C"=>1
	],
	"7142"=>[
		"N"=>"1X2 + First Goal",
		"C"=>1
	],
	"7143"=>[
		"N"=>"Double Chance + First Goal",
		"C"=>1
	],
	"7144"=>[
		"N"=>"Total + First Goal In The Match",
		"C"=>1
	],
	"7145"=>[
		"N"=>"To Be Sent To The Stands",
		"C"=>1
	],
	"7146"=>[
		"N"=>"Team To Take The First Lane",
		"C"=>1
	],
	"7147"=>[
		"N"=>"To Beat First Roshan",
		"C"=>1
	],
	"7148"=>[
		"N"=>"Total Beaten Roshans",
		"C"=>1
	],
	"7149"=>[
		"N"=>"Total Destroyed Objects",
		"C"=>1
	],
	"7151"=>[
		"N"=>"Total Rounds (Actual Number Of Rounds)",
		"C"=>1
	],
	"7153"=>[
		"N"=>"Both Teams, Total",
		"C"=>1
	],
	"7155"=>[
		"N"=>"Leader After Rounds",
		"C"=>1
	],
	"7157"=>[
		"N"=>"Leader After Rounds Yes/No",
		"C"=>1
	],
	"7159"=>[
		"N"=>"Who Will Ask 13th Sector Question",
		"C"=>1
	],
	"7161"=>[
		"N"=>"Who Will Ask First Question",
		"C"=>1
	],
	"7163"=>[
		"N"=>"Who WIll Answer In Round",
		"C"=>1
	],
	"7165"=>[
		"N"=>"13th Sector, Who Will Answer",
		"C"=>1
	],
	"7167"=>[
		"N"=>"Total Correct Answers",
		"C"=>1
	],
	"7169"=>[
		"N"=>"Who Will Score And Match Total",
		"C"=>1
	],
	"7171"=>[
		"N"=>"How Many Points Will A Player Score + Team To Win",
		"C"=>1
	],
	"7173"=>[
		"N"=>"First Half Score + Match Score",
		"C"=>1
	],
	"7175"=>[
		"N"=>"Corner + Goal + Card In First Minutes Of The Match",
		"C"=>1
	],
	"7177"=>[
		"N"=>"Corners + Red Cards + Penalties",
		"C"=>1
	],
	"7179"=>[
		"N"=>"Goals + Red Cards + Posts And Crossbars",
		"C"=>1
	],
	"7181"=>[
		"N"=>"Who Will Score First and Second Goals",
		"C"=>1
	],
	"7183"=>[
		"N"=>"No Goals Or Corners",
		"C"=>1
	],
	"7185"=>[
		"N"=>"How Goals Will Be Scored",
		"C"=>1
	],
	"7187"=>[
		"N"=>"Goals + Red Cards + Penalties + Posts And Crossbars",
		"C"=>1
	],
	"7189"=>[
		"N"=>"Total Goals In Each Match",
		"C"=>1
	],
	"7191"=>[
		"N"=>"Goal In 1st Half In Each Match",
		"C"=>1
	],
	"7193"=>[
		"N"=>"Who Will Score First",
		"C"=>1
	],
	"7195"=>[
		"N"=>"Who Will Score",
		"C"=>1
	],
	"7197"=>[
		"N"=>"Who Will Lead After 1st Half",
		"C"=>1
	],
	"7199"=>[
		"N"=>"First To Have All",
		"C"=>1
	],
	"7201"=>[
		"N"=>"Posts And Crossbars + Red Cards + Corners",
		"C"=>1
	],
	"7203"=>[
		"N"=>"Score At Half-Time + Penalties + Red Cards",
		"C"=>1
	],
	"7205"=>[
		"N"=>"Who Will Score And Who Will Get Cards",
		"C"=>1
	],
	"7207"=>[
		"N"=>"Who Will Win + Both To Score + Total Goals + Red Cards",
		"C"=>1
	],
	"7209"=>[
		"N"=>"Players, Number Of Goals",
		"C"=>1
	],
	"7211"=>[
		"N"=>"Red Cards + Penalties + Posts And Crossbars",
		"C"=>1
	],
	"7213"=>[
		"N"=>"Player Will Score, Will Provide An Assist And Get A Card",
		"C"=>1
	],
	"7215"=>[
		"N"=>"Player To Score First Goal And To Get First Card",
		"C"=>1
	],
	"7217"=>[
		"N"=>"5th Place In Group",
		"C"=>1
	],
	"7219"=>[
		"N"=>"Outcome + Total Corners + Points For Cards",
		"C"=>1
	],
	"7221"=>[
		"N"=>"Total Corners + Points For Cards",
		"C"=>1
	],
	"7223"=>[
		"N"=>"Total Goals + Points For Cards",
		"C"=>1
	],
	"7225"=>[
		"N"=>"Team 1, Multi Corner",
		"C"=>1
	],
	"7227"=>[
		"N"=>"Team 2, Multi Corner",
		"C"=>1
	],
	"7229"=>[
		"N"=>"Total Points For Cards",
		"C"=>1
	],
	"7231"=>[
		"N"=>"Team 1, Total Points For Cards",
		"C"=>1
	],
	"7233"=>[
		"N"=>"Team 2, Total Points For Cards",
		"C"=>1
	],
	"7235"=>[
		"N"=>"Win In Both Halves",
		"C"=>1
	],
	"7237"=>[
		"N"=>"Who Will Win",
		"C"=>1
	],
	"7239"=>[
		"N"=>"All Home Teams To Score",
		"C"=>1
	],
	"7241"=>[
		"N"=>"To Win Half",
		"C"=>1
	],
	"7243"=>[
		"N"=>"Win To Nil",
		"C"=>1
	],
	"7245"=>[
		"N"=>"To Score First",
		"C"=>1
	],
	"7247"=>[
		"N"=>"Win With The Score",
		"C"=>1
	],
	"7249"=>[
		"N"=>"Win + Both Teams To Score",
		"C"=>1
	],
	"7251"=>[
		"N"=>"Team 1, Card In Interval",
		"C"=>1
	],
	"7253"=>[
		"N"=>"Team 1, No Card In Interval",
		"C"=>1
	],
	"7255"=>[
		"N"=>"Team 2, Card In Interval",
		"C"=>1
	],
	"7257"=>[
		"N"=>"Team 2, No Card In Interval",
		"C"=>1
	],
	"7259"=>[
		"N"=>"Penalty In Each Match",
		"C"=>1
	],
	"7261"=>[
		"N"=>"Win With The Score",
		"C"=>1
	],
	"7263"=>[
		"N"=>"Win The 1st Half And The Match",
		"C"=>1
	],
	"7265"=>[
		"N"=>"Win Or Draw",
		"C"=>1
	],
	"7267"=>[
		"N"=>"Goal From Outside Penalty Area + Penalty",
		"C"=>1
	],
	"7269"=>[
		"N"=>"Footjob",
		"C"=>1
	],
	"7271"=>[
		"N"=>"Anal",
		"C"=>1
	],
	"7273"=>[
		"N"=>"Asian Guy",
		"C"=>1
	],
	"7275"=>[
		"N"=>"Asian Chick",
		"C"=>1
	],
	"7277"=>[
		"N"=>"Black Guy",
		"C"=>1
	],
	"7279"=>[
		"N"=>"Black Chick",
		"C"=>1
	],
	"7281"=>[
		"N"=>"Tattooed Guy",
		"C"=>1
	],
	"7283"=>[
		"N"=>"Tattooed Chick",
		"C"=>1
	],
	"7285"=>[
		"N"=>"Squirting",
		"C"=>1
	],
	"7287"=>[
		"N"=>"Blonde",
		"C"=>1
	],
	"7289"=>[
		"N"=>"Brunette",
		"C"=>1
	],
	"7291"=>[
		"N"=>"Redhead",
		"C"=>1
	],
	"7293"=>[
		"N"=>"Non-Standard Hair Color",
		"C"=>1
	],
	"7295"=>[
		"N"=>"Sex With A Midget",
		"C"=>1
	],
	"7297"=>[
		"N"=>"Fisting",
		"C"=>1
	],
	"7299"=>[
		"N"=>"Pissing",
		"C"=>1
	],
	"7301"=>[
		"N"=>"Gloryhole",
		"C"=>1
	],
	"7303"=>[
		"N"=>"Tit Fucking",
		"C"=>1
	],
	"7305"=>[
		"N"=>"Toys",
		"C"=>1
	],
	"7307"=>[
		"N"=>"Strapon",
		"C"=>1
	],
	"7309"=>[
		"N"=>"Pantyhose",
		"C"=>1
	],
	"7311"=>[
		"N"=>"BDSM",
		"C"=>1
	],
	"7313"=>[
		"N"=>"Pregnancy",
		"C"=>1
	],
	"7315"=>[
		"N"=>"Condom",
		"C"=>1
	],
	"7317"=>[
		"N"=>"Lubricant",
		"C"=>1
	],
	"7319"=>[
		"N"=>"Uniform",
		"C"=>1
	],
	"7321"=>[
		"N"=>"Actress Smoking",
		"C"=>1
	],
	"7323"=>[
		"N"=>"Cum On Face, Tits or Belly",
		"C"=>1
	],
	"7325"=>[
		"N"=>"Cum On Back, Ass or Legs",
		"C"=>1
	],
	"7327"=>[
		"N"=>"Creampie",
		"C"=>1
	],
	"7329"=>[
		"N"=>"All Away Teams To Score",
		"C"=>1
	],
	"7331"=>[
		"N"=>"Player To Score First + Team To Win",
		"C"=>1
	],
	"7333"=>[
		"N"=>"Cards + Corners + Penalties",
		"C"=>1
	],
	"7335"=>[
		"N"=>"Match Events",
		"C"=>1
	],
	"7337"=>[
		"N"=>"To Score First Touchdown",
		"C"=>1
	],
	"7339"=>[
		"N"=>"Longest Touchdown In The Match",
		"C"=>1
	],
	"7341"=>[
		"N"=>"Time Of First Score",
		"C"=>1
	],
	"7343"=>[
		"N"=>"Team 1, Total Interceptions By Player",
		"C"=>1
	],
	"7345"=>[
		"N"=>"Team 2, Total Interceptions By Player",
		"C"=>1
	],
	"7347"=>[
		"N"=>"Team 1, Total Pass Completions By Player",
		"C"=>1
	],
	"7349"=>[
		"N"=>"Team 2, Total Pass Completions By Player",
		"C"=>1
	],
	"7351"=>[
		"N"=>"Team 1, Total Passing Yards By Player",
		"C"=>1
	],
	"7353"=>[
		"N"=>"Team 2, Total Passing Yards By Player",
		"C"=>1
	],
	"7355"=>[
		"N"=>"Team 1, Total Receiving Yards By Player",
		"C"=>1
	],
	"7357"=>[
		"N"=>"Team 2, Total Receiving Yards By Player",
		"C"=>1
	],
	"7359"=>[
		"N"=>"Team 1, Total Receptions By Player",
		"C"=>1
	],
	"7361"=>[
		"N"=>"Team 2, Total Receptions By Player",
		"C"=>1
	],
	"7363"=>[
		"N"=>"Team 1, Total Rushing Yards By Player",
		"C"=>1
	],
	"7365"=>[
		"N"=>"Team 2, Total Rushing Yards By Player",
		"C"=>1
	],
	"7367"=>[
		"N"=>"Team 1, Total Touchdown Passes By Player",
		"C"=>1
	],
	"7369"=>[
		"N"=>"Team 2, Total Touchdown Passes By Player",
		"C"=>1
	],
	"7371"=>[
		"N"=>"Longest Field Goal In The Match",
		"C"=>1
	],
	"7373"=>[
		"N"=>"Players, Match-Ups, Who Will Score First Goal",
		"C"=>1
	],
	"7375"=>[
		"N"=>"Players, Match-Ups, Both To Score",
		"C"=>1
	],
	"7377"=>[
		"N"=>"Players, Match-Ups, Points, Handicaps",
		"C"=>1
	],
	"7379"=>[
		"N"=>"Total Even/Odd",
		"C"=>1
	],
	"7381"=>[
		"N"=>"More Extra Cartridges Used Standing Than Prone",
		"C"=>1
	],
	"7383"=>[
		"N"=>"Player vs Team",
		"C"=>1
	],
	"7385"=>[
		"N"=>"All Firing Lines With Misses",
		"C"=>1
	],
	"7387"=>[
		"N"=>"3 Straight Scores By Either Team",
		"C"=>1
	],
	"7389"=>[
		"N"=>"Total Sacks",
		"C"=>1
	],
	"7391"=>[
		"N"=>"Team 1, Total Sacks",
		"C"=>1
	],
	"7393"=>[
		"N"=>"Team 2, Total Sacks",
		"C"=>1
	],
	"7395"=>[
		"N"=>"Score In Final 2 Minutes Of 1st Half",
		"C"=>1
	],
	"7397"=>[
		"N"=>"Player, Total Touchdowns",
		"C"=>1
	],
	"7399"=>[
		"N"=>"Player, Total Passing Yards",
		"C"=>1
	],
	"7401"=>[
		"N"=>"Event To Happen",
		"C"=>1
	],
	"7403"=>[
		"N"=>"Total Events To Happen",
		"C"=>1
	],
	"7405"=>[
		"N"=>"Result Of Drive",
		"C"=>1
	],
	"7407"=>[
		"N"=>"Player To Score Touchdown Anytime",
		"C"=>1
	],
	"7409"=>[
		"N"=>"Team 1, Player To Score Touchdown Anytime",
		"C"=>1
	],
	"7411"=>[
		"N"=>"Team 2, Player To Score Touchdown Anytime",
		"C"=>1
	],
	"7413"=>[
		"N"=>"Team 1 To Score In Each Quarter",
		"C"=>1
	],
	"7415"=>[
		"N"=>"Team 2 To Score In Each Quarter",
		"C"=>1
	],
	"7417"=>[
		"N"=>"Shortest Touchdown Scored (Yards)",
		"C"=>1
	],
	"7419"=>[
		"N"=>"Longest Touchdown Scored",
		"C"=>1
	],
	"7421"=>[
		"N"=>"Players, Match-Ups, Rushing Yards, Handicap",
		"C"=>1
	],
	"7423"=>[
		"N"=>"Players, Match-Ups, Passing Yards, Handicap",
		"C"=>1
	],
	"7425"=>[
		"N"=>"Players, Match-Ups, Receiving Yards, Handicap",
		"C"=>1
	],
	"7427"=>[
		"N"=>"All Firing Lines Without Misses",
		"C"=>1
	],
	"7429"=>[
		"N"=>"Team 1, Total Touchdowns",
		"C"=>1
	],
	"7431"=>[
		"N"=>"Team 2, Total Touchdowns",
		"C"=>1
	],
	"7433"=>[
		"N"=>"Team 1, Total Scored Field Goals",
		"C"=>1
	],
	"7435"=>[
		"N"=>"Team 2, Total Scored Field Goals",
		"C"=>1
	],
	"7437"=>[
		"N"=>"Total Punts",
		"C"=>1
	],
	"7439"=>[
		"N"=>"First Touchdown Time",
		"C"=>1
	],
	"7441"=>[
		"N"=>"First Field Goal Time",
		"C"=>1
	],
	"7443"=>[
		"N"=>"Longest Field Goal Scored",
		"C"=>1
	],
	"7445"=>[
		"N"=>"Total Misses In Prone Position",
		"C"=>1
	],
	"7447"=>[
		"N"=>"Total Misses In Standing Position",
		"C"=>1
	],
	"7449"=>[
		"N"=>"Who Will Score Next Touchdown",
		"C"=>1
	],
	"7451"=>[
		"N"=>"Team 1, Who Will Score Next Touchdown",
		"C"=>1
	],
	"7453"=>[
		"N"=>"Team 2, Who Will Score Next Touchdown",
		"C"=>1
	],
	"7455"=>[
		"N"=>"Down In Drive",
		"C"=>1
	],
	"7457"=>[
		"N"=>"Exact Number Of Frames",
		"C"=>1
	],
	"7459"=>[
		"N"=>"Team To Be First To Go Below N Points",
		"C"=>1
	],
	"7461"=>[
		"N"=>"Total (3Way).",
		"C"=>1
	],
	"7463"=>[
		"N"=>"Total (3Way)..",
		"C"=>1
	],
	"7465"=>[
		"N"=>"Total (3Way)...",
		"C"=>1
	],
	"7467"=>[
		"N"=>"Total (6Way)",
		"C"=>1
	],
	"7469"=>[
		"N"=>"Total: Team 1 x Team 2 (3Way)",
		"C"=>1
	],
	"7471"=>[
		"N"=>"Total: Team 1 x Team 2 (3Way).",
		"C"=>1
	],
	"7473"=>[
		"N"=>"Total: Team 1 x Team 2 (3Way)..",
		"C"=>1
	],
	"7475"=>[
		"N"=>"Total: Team 1 x Team 2 (6Way)",
		"C"=>1
	],
	"7477"=>[
		"N"=>"Total: Team 1 x Team 2",
		"C"=>1
	],
	"7479"=>[
		"N"=>"Total: 1st Half x 2nd Half (3Way)",
		"C"=>1
	],
	"7481"=>[
		"N"=>"Total: 1st Half x 2nd Half (3Way).",
		"C"=>1
	],
	"7483"=>[
		"N"=>"Total: 1st Half x 2nd Half (3Way)..",
		"C"=>1
	],
	"7485"=>[
		"N"=>"Total: 1st Half x 2nd Half (6Way)",
		"C"=>1
	],
	"7487"=>[
		"N"=>"Total: 1st Half x 2nd Half",
		"C"=>1
	],
	"7489"=>[
		"N"=>"Total Matches With TO (3Way)",
		"C"=>1
	],
	"7491"=>[
		"N"=>"Total Matches With TO (3Way).",
		"C"=>1
	],
	"7493"=>[
		"N"=>"Total Matches With TO (6Way)",
		"C"=>1
	],
	"7495"=>[
		"N"=>"Total Matches In Which Both Teams Will Score (3Way)",
		"C"=>1
	],
	"7497"=>[
		"N"=>"Total Matches In Which Both Teams Will Score (3Way).",
		"C"=>1
	],
	"7499"=>[
		"N"=>"Total Matches In Which Both Teams Will Score (6Way)",
		"C"=>1
	],
	"7501"=>[
		"N"=>"Correct Score..",
		"C"=>1
	],
	"7503"=>[
		"N"=>"All Firing Lines With Extra Cartridges",
		"C"=>1
	],
	"7505"=>[
		"N"=>"Players To Provide Assists",
		"C"=>1
	],
	"7507"=>[
		"N"=>"Players To Provide Assists.",
		"C"=>1
	],
	"7509"=>[
		"N"=>"Player Will Be Behind On Sets But Will Win The Match",
		"C"=>1
	],
	"7511"=>[
		"N"=>"First To Win A Number Of Sets",
		"C"=>1
	],
	"7513"=>[
		"N"=>"Total Aces In A Row",
		"C"=>1
	],
	"7515"=>[
		"N"=>"Total Blocks In A Row",
		"C"=>1
	],
	"7517"=>[
		"N"=>"How Many Teams Will Score (3Way)",
		"C"=>1
	],
	"7519"=>[
		"N"=>"Team 1, Win Or Draw In Each Period",
		"C"=>1
	],
	"7521"=>[
		"N"=>"Team 2, Win Or Draw In Each Period",
		"C"=>1
	],
	"7523"=>[
		"N"=>"Regular Time Result + Both Teams To Score",
		"C"=>1
	],
	"7525"=>[
		"N"=>"Total Touchdown Passes By Player",
		"C"=>1
	],
	"7527"=>[
		"N"=>"Total Completed Passes By Player",
		"C"=>1
	],
	"7529"=>[
		"N"=>"Total Pass Attempts By Player",
		"C"=>1
	],
	"7531"=>[
		"N"=>"Longest Pass By Player (Yards)",
		"C"=>1
	],
	"7533"=>[
		"N"=>"Longest Rush By Player (Yards)",
		"C"=>1
	],
	"7535"=>[
		"N"=>"First Touchdown Pass By Player (Yards)",
		"C"=>1
	],
	"7537"=>[
		"N"=>"Longest Punt (Yards)",
		"C"=>1
	],
	"7539"=>[
		"N"=>"Shortest Punt (Yards)",
		"C"=>1
	],
	"7541"=>[
		"N"=>"Total Interceptions",
		"C"=>1
	],
	"7543"=>[
		"N"=>"Total Fumbles Lost",
		"C"=>1
	],
	"7545"=>[
		"N"=>"Total Touchdown Passes",
		"C"=>1
	],
	"7547"=>[
		"N"=>"Total Kickoff Returns",
		"C"=>1
	],
	"7549"=>[
		"N"=>"Total Rushing Yards",
		"C"=>1
	],
	"7551"=>[
		"N"=>"Team 1, Total First Downs",
		"C"=>1
	],
	"7553"=>[
		"N"=>"Team 2, Total First Downs",
		"C"=>1
	],
	"7555"=>[
		"N"=>"Total Field Goals Missed",
		"C"=>1
	],
	"7557"=>[
		"N"=>"Total Touchdowns (Yards)",
		"C"=>1
	],
	"7559"=>[
		"N"=>"Team To Take First Instant Replay",
		"C"=>1
	],
	"7561"=>[
		"N"=>"Team To Get A First Down",
		"C"=>1
	],
	"7563"=>[
		"N"=>"First Sack",
		"C"=>1
	],
	"7565"=>[
		"N"=>"First Punt",
		"C"=>1
	],
	"7567"=>[
		"N"=>"Team To Get Most First Downs",
		"C"=>1
	],
	"7569"=>[
		"N"=>"Longest Punt",
		"C"=>1
	],
	"7571"=>[
		"N"=>"Winner, Starting Price",
		"C"=>1
	],
	"7572"=>[
		"N"=>"Which Team Will Perform Quadra Kill",
		"C"=>1
	],
	"7574"=>[
		"N"=>"Which Team Will Perform Penta Kill",
		"C"=>1
	],
	"7576"=>[
		"N"=>"Team To Perform Next Frag",
		"C"=>1
	],
	"7578"=>[
		"N"=>"Team To Destroy Next Barracks",
		"C"=>1
	],
	"7580"=>[
		"N"=>"Team To Perform Most Frags",
		"C"=>1
	],
	"7582"=>[
		"N"=>"Total Destroyed Towers",
		"C"=>1
	],
	"7584"=>[
		"N"=>"Total Beaten Dragons",
		"C"=>1
	],
	"7586"=>[
		"N"=>"Total Beaten Nashors",
		"C"=>1
	],
	"7588"=>[
		"N"=>"Total Destroyed Inhibitors",
		"C"=>1
	],
	"7590"=>[
		"N"=>"Both Teams To Beat Dragon",
		"C"=>1
	],
	"7592"=>[
		"N"=>"Both Teams To Beat Nashor",
		"C"=>1
	],
	"7594"=>[
		"N"=>"Both Teams To Beat Roshan",
		"C"=>1
	],
	"7596"=>[
		"N"=>"Both Teams To Destroy Inhibitor",
		"C"=>1
	],
	"7598"=>[
		"N"=>"Both Teams To Destroy Barracks",
		"C"=>1
	],
	"7600"=>[
		"N"=>"Total Destroyed Inhibitors.",
		"C"=>1
	],
	"7602"=>[
		"N"=>"Team To Destroy Next Inhibitor",
		"C"=>1
	],
	"7603"=>[
		"N"=>"Win By.",
		"C"=>1
	],
	"7605"=>[
		"N"=>"Win By..",
		"C"=>1
	],
	"7607"=>[
		"N"=>"Win By...",
		"C"=>1
	],
	"7609"=>[
		"N"=>"Win By....",
		"C"=>1
	],
	"7611"=>[
		"N"=>"Win By.....",
		"C"=>1
	],
	"7613"=>[
		"N"=>"Win By......",
		"C"=>1
	],
	"7615"=>[
		"N"=>"Leader After Quarter",
		"C"=>1
	],
	"7617"=>[
		"N"=>"Handicap After Quarter",
		"C"=>1
	],
	"7619"=>[
		"N"=>"Lead By Quarters",
		"C"=>1
	],
	"7621"=>[
		"N"=>"Leader In Each Quarter",
		"C"=>1
	],
	"7623"=>[
		"N"=>"Win By.......",
		"C"=>1
	],
	"7625"=>[
		"N"=>"Total Matches With TO (3Way)..",
		"C"=>1
	],
	"7627"=>[
		"N"=>"Minute The Bout Will End",
		"C"=>1
	],
	"7629"=>[
		"N"=>"Team To Beat First Rift Herald",
		"C"=>1
	],
	"7631"=>[
		"N"=>"Win (Refund If Placed)",
		"C"=>1
	],
	"7633"=>[
		"N"=>"Win Without Leader",
		"C"=>1
	],
	"7635"=>[
		"N"=>"Most 180s And Win Match",
		"C"=>1
	],
	"7637"=>[
		"N"=>"Most 180s And Lose Match",
		"C"=>1
	],
	"7639"=>[
		"N"=>"Checkout Score",
		"C"=>1
	],
	"7641"=>[
		"N"=>"Player 1, Highest Checkout",
		"C"=>1
	],
	"7643"=>[
		"N"=>"Player 2, Highest Checkout",
		"C"=>1
	],
	"7645"=>[
		"N"=>"Highest Checkout And Win Match",
		"C"=>1
	],
	"7647"=>[
		"N"=>"Highest Checkout And Lose Match",
		"C"=>1
	],
	"7649"=>[
		"N"=>"Player 1, Checkout",
		"C"=>1
	],
	"7651"=>[
		"N"=>"Player 2, Checkout",
		"C"=>1
	],
	"7653"=>[
		"N"=>"Checkout, Special",
		"C"=>1
	],
	"7655"=>[
		"N"=>"First 180",
		"C"=>1
	],
	"7657"=>[
		"N"=>"Last 180",
		"C"=>1
	],
	"7659"=>[
		"N"=>"Nine Dart Finish",
		"C"=>1
	],
	"7661"=>[
		"N"=>"Nine Dart Finish, Special",
		"C"=>1
	],
	"7663"=>[
		"N"=>"Player 1, Nine Dart Finish",
		"C"=>1
	],
	"7665"=>[
		"N"=>"Player 2, Nine Dart Finish",
		"C"=>1
	],
	"7667"=>[
		"N"=>"Win + Both To Throw A Number Of 180s",
		"C"=>1
	],
	"7669"=>[
		"N"=>"Both To Throw A Number Of 180s",
		"C"=>1
	],
	"7671"=>[
		"N"=>"Most 180s + Highest Checkout + Win Match",
		"C"=>1
	],
	"7673"=>[
		"N"=>"Most 180s + Highest Checkout + Lose Match",
		"C"=>1
	],
	"7675"=>[
		"N"=>"Most 180s, Handicap",
		"C"=>1
	],
	"7677"=>[
		"N"=>"Player 1 Average",
		"C"=>1
	],
	"7679"=>[
		"N"=>"Player 2 Average",
		"C"=>1
	],
	"7681"=>[
		"N"=>"Leg, Winning Checkout",
		"C"=>1
	],
	"7683"=>[
		"N"=>"Shooting Time In Standing Position",
		"C"=>1
	],
	"7685"=>[
		"N"=>"Shooting Time In Prone Position",
		"C"=>1
	],
	"7687"=>[
		"N"=>"Time At Shooting Range",
		"C"=>1
	],
	"7689"=>[
		"N"=>"Win Without Leaders",
		"C"=>1
	],
	"7691"=>[
		"N"=>"Win By........",
		"C"=>1
	],
	"7693"=>[
		"N"=>"First Point + Lead At HT + Win Match",
		"C"=>1
	],
	"7695"=>[
		"N"=>"Last Point + Lead At HT + Win Match",
		"C"=>1
	],
	"7697"=>[
		"N"=>"How Will First Points Be Scored",
		"C"=>1
	],
	"7699"=>[
		"N"=>"Total Points",
		"C"=>1
	],
	"7701"=>[
		"N"=>"Team 1, Total Points",
		"C"=>1
	],
	"7703"=>[
		"N"=>"Team 2, Total Points",
		"C"=>1
	],
	"7705"=>[
		"N"=>"Either Team To Win A Number Of Rounds In A Row",
		"C"=>1
	],
	"7707"=>[
		"N"=>"Regular Time Winner",
		"C"=>1
	],
	"7709"=>[
		"N"=>"Winner Of The Match",
		"C"=>1
	],
	"7711"=>[
		"N"=>"Player's Total In Regular Time",
		"C"=>1
	],
	"7713"=>[
		"N"=>"Total Frags In A Row",
		"C"=>1
	],
	"7715"=>[
		"N"=>"Max Frags In A Row",
		"C"=>1
	],
	"7717"=>[
		"N"=>"How Many Points In A Row Will 1st Team Win",
		"C"=>1
	],
	"7719"=>[
		"N"=>"How Many Points In A Row Will 2nd Team Win",
		"C"=>1
	],
	"7721"=>[
		"N"=>"How Many Points In A Row Will 1st Team Win.",
		"C"=>1
	],
	"7723"=>[
		"N"=>"How Many Points In A Row Will 2nd Team Win.",
		"C"=>1
	],
	"7725"=>[
		"N"=>"First To Pot The Red Ball",
		"C"=>1
	],
	"7727"=>[
		"N"=>"Most Tries",
		"C"=>1
	],
	"7729"=>[
		"N"=>"Team 1 To Win By + Total",
		"C"=>1
	],
	"7731"=>[
		"N"=>"Team 2 To Win By + Total",
		"C"=>1
	],
	"7733"=>[
		"N"=>"Handicap 1 + Total",
		"C"=>1
	],
	"7735"=>[
		"N"=>"Handicap 2 + Total",
		"C"=>1
	],
	"7737"=>[
		"N"=>"Player To Score A Try + Team To Win By",
		"C"=>1
	],
	"7739"=>[
		"N"=>"Player To Score A Try + Match Result",
		"C"=>1
	],
	"7741"=>[
		"N"=>"Player To Score First Try + Match Result",
		"C"=>1
	],
	"7743"=>[
		"N"=>"HT-FT To Win By",
		"C"=>1
	],
	"7745"=>[
		"N"=>"Total Points Scored By Teams",
		"C"=>1
	],
	"7747"=>[
		"N"=>"Who Will Score Tries",
		"C"=>1
	],
	"7749"=>[
		"N"=>"To Score First Or Last Tries",
		"C"=>1
	],
	"7751"=>[
		"N"=>"Unanswered Tries",
		"C"=>1
	],
	"7753"=>[
		"N"=>"Position Of First Tryscorer",
		"C"=>1
	],
	"7755"=>[
		"N"=>"Position Of Last Tryscorer",
		"C"=>1
	],
	"7757"=>[
		"N"=>"Position Of First Home Tryscorer",
		"C"=>1
	],
	"7759"=>[
		"N"=>"Position Of First Away Tryscorer",
		"C"=>1
	],
	"7761"=>[
		"N"=>"Tryfecta (First Three Tries)",
		"C"=>1
	],
	"7763"=>[
		"N"=>"Games Difference",
		"C"=>1
	],
	"7765"=>[
		"N"=>"Games Difference.",
		"C"=>1
	],
	"7767"=>[
		"N"=>"Player 1, 1st Service Game Total Points",
		"C"=>1
	],
	"7769"=>[
		"N"=>"Player 2, 1st Service Game Total Points",
		"C"=>1
	],
	"7771"=>[
		"N"=>"Player 1, 1st Service Game Score",
		"C"=>1
	],
	"7773"=>[
		"N"=>"Player 2, 1st Service Game Score",
		"C"=>1
	],
	"7775"=>[
		"N"=>"Who Will Score A Try",
		"C"=>1
	],
	"7777"=>[
		"N"=>"Who Will Score 2 Or More Tries",
		"C"=>1
	],
	"7779"=>[
		"N"=>"Who Will Score 3 Or More Tries",
		"C"=>1
	],
	"7781"=>[
		"N"=>"Team 1, Time Of 1st Try",
		"C"=>1
	],
	"7783"=>[
		"N"=>"Team 2, Time Of 1st Try",
		"C"=>1
	],
	"7785"=>[
		"N"=>"Time Of Last Try",
		"C"=>1
	],
	"7787"=>[
		"N"=>"Who Will Score Last Try",
		"C"=>1
	],
	"7789"=>[
		"N"=>"Team 1, Who Will Score Last Try",
		"C"=>1
	],
	"7791"=>[
		"N"=>"Team 2, Who Will Score Last Try",
		"C"=>1
	],
	"7793"=>[
		"N"=>"Total Goals + Behinds",
		"C"=>1
	],
	"7795"=>[
		"N"=>"Team 1, Total Goals + Behinds",
		"C"=>1
	],
	"7797"=>[
		"N"=>"Team 2, Total Goals + Behinds",
		"C"=>1
	],
	"7799"=>[
		"N"=>"Total Try Bands",
		"C"=>1
	],
	"7801"=>[
		"N"=>"Both Teams To Score A Number Of Points",
		"C"=>1
	],
	"7803"=>[
		"N"=>"Team 1, Exact Number Of Points",
		"C"=>1
	],
	"7805"=>[
		"N"=>"Team 2, Exact Number Of Points",
		"C"=>1
	],
	"7807"=>[
		"N"=>"Next Corner (2Way)",
		"C"=>1
	],
	"7809"=>[
		"N"=>"Next Yellow Card (2Way)",
		"C"=>1
	],
	"7811"=>[
		"N"=>"Player's Total Deaths In Regular Time",
		"C"=>1
	],
	"7813"=>[
		"N"=>"Nationality Of Winner",
		"C"=>1
	],
	"7815"=>[
		"N"=>"First Penalty Time",
		"C"=>1
	],
	"7817"=>[
		"N"=>"Will 1st Try Be Converted",
		"C"=>1
	],
	"7819"=>[
		"N"=>"Result For The Current Minute (2Way)",
		"C"=>1
	],
	"7821"=>[
		"N"=>"Team To Score More Tries",
		"C"=>1
	],
	"7823"=>[
		"N"=>"Half With Most Tries",
		"C"=>1
	],
	"7825"=>[
		"N"=>"Total Tries Even/Odd",
		"C"=>1
	],
	"7827"=>[
		"N"=>"Who Will Score First And Second Tries (In Any Order)",
		"C"=>1
	],
	"7829"=>[
		"N"=>"Team To Score Last Try",
		"C"=>1
	],
	"7831"=>[
		"N"=>"Position Of First Tryscorer",
		"C"=>1
	],
	"7833"=>[
		"N"=>"Win In Points Interval (2Way)",
		"C"=>1
	],
	"7835"=>[
		"N"=>"Team To Score First And Win",
		"C"=>1
	],
	"7837"=>[
		"N"=>"Which Team Will Score First And Win",
		"C"=>1
	],
	"7839"=>[
		"N"=>"Which Team Will Score First And Fail To Win",
		"C"=>1
	],
	"7841"=>[
		"N"=>"First Point + Lead At HT + Lose Match",
		"C"=>1
	],
	"7843"=>[
		"N"=>"Score First / Win The Match",
		"C"=>1
	],
	"7847"=>[
		"N"=>"HT-FT (5Way)",
		"C"=>1
	],
	"7849"=>[
		"N"=>"When Bout Will End",
		"C"=>1
	],
	"7851"=>[
		"N"=>"Sets Correct Score.",
		"C"=>1
	],
	"7853"=>[
		"N"=>"Player 1, 1st Service Game Total Points",
		"C"=>1
	],
	"7855"=>[
		"N"=>"Player 2, 1st Service Game Total Points",
		"C"=>1
	],
	"7857"=>[
		"N"=>"Double Chance In The Match + Both Teams To Score In 1st Half",
		"C"=>1
	],
	"7859"=>[
		"N"=>"Double Chance In The Match + Both Teams To Score In 2nd Half",
		"C"=>1
	],
	"7861"=>[
		"N"=>"Draw Or W2 (Without W1)",
		"C"=>1
	],
	"7863"=>[
		"N"=>"W1 Or Draw (Without W2)",
		"C"=>1
	],
	"7865"=>[
		"N"=>"End Of Round Leader, Including Current",
		"C"=>1
	],
	"7867"=>[
		"N"=>"End Of Round Leader Match-Ups, Including Current",
		"C"=>1
	],
	"7869"=>[
		"N"=>"Number Of 147 Breaks",
		"C"=>1
	],
	"7871"=>[
		"N"=>"The Highest Break",
		"C"=>1
	],
	"7873"=>[
		"N"=>"Player With The Highest Break",
		"C"=>1
	],
	"7875"=>[
		"N"=>"To Score First Goal And Win",
		"C"=>1
	],
	"7877"=>[
		"N"=>"To Score First Goal And Not Win",
		"C"=>1
	],
	"7879"=>[
		"N"=>"To Score First Goal And Draw",
		"C"=>1
	],
	"7881"=>[
		"N"=>"To Score First Goal And Lose",
		"C"=>1
	],
	"7883"=>[
		"N"=>"Total Stars",
		"C"=>1
	],
	"7885"=>[
		"N"=>"Total Maps",
		"C"=>1
	],
	"7887"=>[
		"N"=>"To Be Higher By Frags",
		"C"=>1
	],
	"7889"=>[
		"N"=>"To Be Higher By Deaths",
		"C"=>1
	],
	"7891"=>[
		"N"=>"Total Deaths",
		"C"=>1
	],
	"7893"=>[
		"N"=>"Total Frags In Regular Time",
		"C"=>1
	],
	"7895"=>[
		"N"=>"Total Deaths In Regular Time",
		"C"=>1
	],
	"7897"=>[
		"N"=>"To Be Higher By Frags In Regular Time",
		"C"=>1
	],
	"7899"=>[
		"N"=>"To Be Higher By Deaths In Regular Time",
		"C"=>1
	],
	"7901"=>[
		"N"=>"Top Driver Of The Team",
		"C"=>1
	],
	"7903"=>[
		"N"=>"Exact Number Of Points In Set",
		"C"=>1
	],
	"7905"=>[
		"N"=>"Player 1 Total Frames",
		"C"=>1
	],
	"7907"=>[
		"N"=>"Player 2 Total Frames",
		"C"=>1
	],
	"7909"=>[
		"N"=>"Total Frames Even/Odd",
		"C"=>1
	],
	"7911"=>[
		"N"=>"Score After Frame",
		"C"=>1
	],
	"7913"=>[
		"N"=>"Most Centuries",
		"C"=>1
	],
	"7915"=>[
		"N"=>"Total Breaks",
		"C"=>1
	],
	"7917"=>[
		"N"=>"Total Breaks 1",
		"C"=>1
	],
	"7919"=>[
		"N"=>"Total Breaks 2",
		"C"=>1
	],
	"7921"=>[
		"N"=>"Who Will Score A Century",
		"C"=>1
	],
	"7923"=>[
		"N"=>"Who Will Win 1st Frame And The Match",
		"C"=>1
	],
	"7925"=>[
		"N"=>"Who Will Lose 1st Frame And Win The Match",
		"C"=>1
	],
	"7927"=>[
		"N"=>"Next Point (2 Ways)",
		"C"=>1
	],
	"7929"=>[
		"N"=>"6th Place In Group",
		"C"=>1
	],
	"7931"=>[
		"N"=>"7th Place In Group",
		"C"=>1
	],
	"7933"=>[
		"N"=>"8th Place In Group",
		"C"=>1
	],
	"7935"=>[
		"N"=>"Player 1, Maximum Break (147 Points)",
		"C"=>1
	],
	"7937"=>[
		"N"=>"Player 2, Maximum Break (147 Points)",
		"C"=>1
	],
	"7939"=>[
		"N"=>"First To Foul",
		"C"=>1
	],
	"7941"=>[
		"N"=>"First To Make A 50+ Break",
		"C"=>1
	],
	"7943"=>[
		"N"=>"Score After Game In The Series",
		"C"=>1
	],
	"7945"=>[
		"N"=>"Team To Win The Game And The Series",
		"C"=>1
	],
	"7947"=>[
		"N"=>"Team To Win The Game And Lose The Series",
		"C"=>1
	],
	"7949"=>[
		"N"=>"Match To Go To A Deciding Frame",
		"C"=>1
	],
	"7951"=>[
		"N"=>"Challenge Time",
		"C"=>1
	],
	"7953"=>[
		"N"=>"Player 1, Highest Break",
		"C"=>1
	],
	"7955"=>[
		"N"=>"Player 2, Highest Break",
		"C"=>1
	],
	"7957"=>[
		"N"=>"Player's Total Deaths",
		"C"=>1
	],
	"7959"=>[
		"N"=>"Player's Total",
		"C"=>1
	],
	"7961"=>[
		"N"=>"Multi Goal",
		"C"=>1
	],
	"7963"=>[
		"N"=>"To Win End (2 Way)",
		"C"=>1
	],
	"7965"=>[
		"N"=>"Who Will Score Home Run And Win Match",
		"C"=>1
	],
	"7967"=>[
		"N"=>"After Innings Result And Match Result",
		"C"=>1
	],
	"7969"=>[
		"N"=>"Total Matches With Shootouts",
		"C"=>1
	],
	"7971"=>[
		"N"=>"Total Goals Scored By Defensemen",
		"C"=>1
	],
	"7973"=>[
		"N"=>"Total Braces",
		"C"=>1
	],
	"7975"=>[
		"N"=>"Total Hat-tricks",
		"C"=>1
	],
	"7977"=>[
		"N"=>"Leader After Each Quarter",
		"C"=>1
	],
	"7979"=>[
		"N"=>"When And How Will Bout End",
		"C"=>1
	],
	"7981"=>[
		"N"=>"Leader After Each Quarter By Total Points",
		"C"=>1
	],
	"7983"=>[
		"N"=>"Total Drop Goals",
		"C"=>1
	],
	"7985"=>[
		"N"=>"Either Team To Score A Certain Number Of Goals",
		"C"=>1
	],
	"7987"=>[
		"N"=>"Series Leader (2 Way)",
		"C"=>1
	],
	"7989"=>[
		"N"=>"Team To Never Be Behind In Series And To Win",
		"C"=>1
	],
	"7995"=>[
		"N"=>"Corner By Time Interval",
		"C"=>1
	],
	"7997"=>[
		"N"=>"To Be Higher (4 Way)",
		"C"=>1
	],
	"7999"=>[
		"N"=>"HT-FT Winning Margin",
		"C"=>1
	],
	"8001"=>[
		"N"=>"Series Leader (3 Way)",
		"C"=>1
	],
	"8003"=>[
		"N"=>"How Many Safety Car Periods Will There Be During The Race?",
		"C"=>1
	],
	"8005"=>[
		"N"=>"End Of Lap Position",
		"C"=>1
	],
	"8007"=>[
		"N"=>"Team With Most Points",
		"C"=>1
	],
	"8009"=>[
		"N"=>"How Many Games Will There Be In The Series",
		"C"=>1
	],
	"8011"=>[
		"N"=>"Team 1, Time Of First Goal",
		"C"=>1
	],
	"8013"=>[
		"N"=>"Team 2, Time Of First Goal",
		"C"=>1
	],
	"8015"=>[
		"N"=>"Time Of First Goal",
		"C"=>1
	],
	"8017"=>[
		"N"=>"Total Disposals, Match-Ups",
		"C"=>1
	],
	"8019"=>[
		"N"=>"Total Player Disposals",
		"C"=>1
	],
	"8021"=>[
		"N"=>"Number Of Player Disposals",
		"C"=>1
	],
	"8023"=>[
		"N"=>"Young Rider Classification Winner",
		"C"=>1
	],
	"8025"=>[
		"N"=>"Team 1, Player To Score First Goal",
		"C"=>1
	],
	"8027"=>[
		"N"=>"Team 2, Player To Score First Goal",
		"C"=>1
	],
	"8029"=>[
		"N"=>"Team 1, Player To Score Last Goal",
		"C"=>1
	],
	"8031"=>[
		"N"=>"Team 2, Player To Score Last Goal",
		"C"=>1
	],
	"8033"=>[
		"N"=>"Who Will Score Most Goals",
		"C"=>1
	],
	"8035"=>[
		"N"=>"Hole, Result.",
		"C"=>1
	],
	"8037"=>[
		"N"=>"Best On Disposals/Best On Goals",
		"C"=>1
	],
	"8039"=>[
		"N"=>"Disposals/Goals",
		"C"=>1
	],
	"8041"=>[
		"N"=>"Team 1, Total (3 Way).",
		"C"=>1
	],
	"8043"=>[
		"N"=>"Team 2, Total (3 Way).",
		"C"=>1
	],
	"8045"=>[
		"N"=>"Number Of Players Who Scored Exactly 2 Goals",
		"C"=>1
	],
	"8047"=>[
		"N"=>"Number Of Players Who Scored Exactly 3 Goals",
		"C"=>1
	],
	"8049"=>[
		"N"=>"Total Behinds.",
		"C"=>1
	],
	"8051"=>[
		"N"=>"Team 1, Total Behinds",
		"C"=>1
	],
	"8053"=>[
		"N"=>"Team 2, Total Behinds",
		"C"=>1
	],
	"8055"=>[
		"N"=>"Crystal Atom Winner",
		"C"=>1
	],
	"8057"=>[
		"N"=>"Player With Most Points",
		"C"=>1
	],
	"8059"=>[
		"N"=>"First Points In The Game",
		"C"=>1
	],
	"8061"=>[
		"N"=>"Assists Leader In The Series",
		"C"=>1
	],
	"8063"=>[
		"N"=>"Rebounds Leader In The Series",
		"C"=>1
	],
	"8065"=>[
		"N"=>"Hole, Match-Ups.",
		"C"=>1
	],
	"8067"=>[
		"N"=>"Hole, Match-Ups..",
		"C"=>1
	],
	"8069"=>[
		"N"=>"Hole, Over/Under Par.",
		"C"=>1
	],
	"8071"=>[
		"N"=>"Points Leader In The Series",
		"C"=>1
	],
	"8073"=>[
		"N"=>"Total Rebounds + Assists",
		"C"=>1
	],
	"8075"=>[
		"N"=>"Player Total + Match Result",
		"C"=>1
	],
	"8077"=>[
		"N"=>"Player Total + Match Total",
		"C"=>1
	],
	"8079"=>[
		"N"=>"Player Total",
		"C"=>1
	],
	"8081"=>[
		"N"=>"To Be Higher By Points In The Series",
		"C"=>1
	],
	"8083"=>[
		"N"=>"To Be Higher By Rebounds In The Series",
		"C"=>1
	],
	"8085"=>[
		"N"=>"To Be Higher By Assists In The Series",
		"C"=>1
	],
	"8087"=>[
		"N"=>"Player To Score A Goal And Get A Card",
		"C"=>1
	],
	"8089"=>[
		"N"=>"Player To Make An Assist And Get A Card",
		"C"=>1
	],
	"8091"=>[
		"N"=>"Player To Hit The Post Or Crossbar",
		"C"=>1
	],
	"8093"=>[
		"N"=>"Player To Score A Goal And Get A Yellow Card",
		"C"=>1
	],
	"8095"=>[
		"N"=>"Player To Win And Score A Penalty",
		"C"=>1
	],
	"8097"=>[
		"N"=>"Score During The Match",
		"C"=>1
	],
	"8099"=>[
		"N"=>"Team 1 To Win First Half With Difference",
		"C"=>1
	],
	"8101"=>[
		"N"=>"Team 2 To Win First Half With Difference",
		"C"=>1
	],
	"8103"=>[
		"N"=>"Position Of First Goalscorer",
		"C"=>1
	],
	"8105"=>[
		"N"=>"Team 1 First Attack To End In A Goal",
		"C"=>1
	],
	"8107"=>[
		"N"=>"Team 2 First Attack To End In A Goal",
		"C"=>1
	],
	"8109"=>[
		"N"=>"Team 1 To Win Seven-Meter Throw Series",
		"C"=>1
	],
	"8111"=>[
		"N"=>"Team 2 To Win Seven-Meter Throw Series",
		"C"=>1
	],
	"8113"=>[
		"N"=>"League Winner",
		"C"=>1
	],
	"8115"=>[
		"N"=>"Age Of Main Event Winner",
		"C"=>1
	],
	"8117"=>[
		"N"=>"Match-Ups, Best Finishing Position",
		"C"=>1
	],
	"8119"=>[
		"N"=>"Player, Final Hand",
		"C"=>1
	],
	"8121"=>[
		"N"=>"Last Flop Of Main Event",
		"C"=>1
	],
	"8123"=>[
		"N"=>"Region Of Main Event Winner",
		"C"=>1
	],
	"8125"=>[
		"N"=>"Total Players To Win Multiple Bracelets",
		"C"=>1
	],
	"8127"=>[
		"N"=>"Player To Finish In The Money",
		"C"=>1
	],
	"8129"=>[
		"N"=>"Total Players At The Main Event Final Table",
		"C"=>1
	],
	"8131"=>[
		"N"=>"Former Main Event Winner To Win A Bracelet",
		"C"=>1
	],
	"8133"=>[
		"N"=>"Total Players In Main Event",
		"C"=>1
	],
	"8135"=>[
		"N"=>"Winning Hand In Main Event",
		"C"=>1
	],
	"8137"=>[
		"N"=>"Main Event Winner",
		"C"=>1
	],
	"8139"=>[
		"N"=>"Flawless Victory In Round",
		"C"=>1
	],
	"8141"=>[
		"N"=>"Players, Points(Goal+Assist), Match-Ups, Handicaps",
		"C"=>1
	],
	"8143"=>[
		"N"=>"Players, Points(Goal+Assist), Specials, Totals",
		"C"=>1
	],
	"8145"=>[
		"N"=>"Players, First Field Goal",
		"C"=>1
	],
	"8147"=>[
		"N"=>"Players, First Free Throw",
		"C"=>1
	],
	"8149"=>[
		"N"=>"Players, Goals, Match-Ups, Handicaps",
		"C"=>1
	],
	"8151"=>[
		"N"=>"Players, Goals, Specials, Total",
		"C"=>1
	],
	"8153"=>[
		"N"=>"Who Will Win The Bout In First 60 Seconds",
		"C"=>1
	],
	"8155"=>[
		"N"=>"Type Of Last Score",
		"C"=>1
	],
	"8157"=>[
		"N"=>"Shortest Field Goal",
		"C"=>1
	],
	"8159"=>[
		"N"=>"Handiсap (Including Super Tie-Break)",
		"C"=>1
	],
	"8161"=>[
		"N"=>"Result + Total (Including Super Tie-Break)",
		"C"=>1
	],
	"8163"=>[
		"N"=>"Individual Total 1 (Including Super Tie-Break)",
		"C"=>1
	],
	"8165"=>[
		"N"=>"Individual Total 2 (Including Super Tie-Break)",
		"C"=>1
	],
	"8167"=>[
		"N"=>"Points In Group",
		"C"=>1
	],
	"8169"=>[
		"N"=>"Scored Goals In Group",
		"C"=>1
	],
	"8171"=>[
		"N"=>"Total Wins In Group",
		"C"=>1
	],
	"8173"=>[
		"N"=>"Total Draws In Group",
		"C"=>1
	],
	"8175"=>[
		"N"=>"Total Defeats In Group",
		"C"=>1
	],
	"8177"=>[
		"N"=>"Total Taken Corners In Group",
		"C"=>1
	],
	"8179"=>[
		"N"=>"Total Goals Conceded In Group",
		"C"=>1
	],
	"8181"=>[
		"N"=>"Total Yellow Cards In Group",
		"C"=>1
	],
	"8183"=>[
		"N"=>"Total Red Cards In Group",
		"C"=>1
	],
	"8185"=>[
		"N"=>"Total Goalkeeper's Yellow Cards In Group",
		"C"=>1
	],
	"8187"=>[
		"N"=>"Total Goalkeeper's Red Cards In Group",
		"C"=>1
	],
	"8189"=>[
		"N"=>"Total Headed Goals In Group",
		"C"=>1
	],
	"8191"=>[
		"N"=>"Total Goals From Direct Free Kicks In Group",
		"C"=>1
	],
	"8193"=>[
		"N"=>"Total Penalties Scored In Group",
		"C"=>1
	],
	"8195"=>[
		"N"=>"Total Own Goals In Group",
		"C"=>1
	],
	"8197"=>[
		"N"=>"Total Penalties Conceded In Group",
		"C"=>1
	],
	"8199"=>[
		"N"=>"Goal In Each Group Match",
		"C"=>1
	],
	"8201"=>[
		"N"=>"Headed Goal",
		"C"=>1
	],
	"8203"=>[
		"N"=>"VAR To Be Used",
		"C"=>1
	],
	"8205"=>[
		"N"=>"Awarded Goal To Be Disallowed By VAR",
		"C"=>1
	],
	"8207"=>[
		"N"=>"Disallowed Goal To Be Awarded By VAR",
		"C"=>1
	],
	"8215"=>[
		"N"=>"Goalkeeper To Touch The Ball In The First Minute Of The Match",
		"C"=>1
	],
	"8217"=>[
		"N"=>"Both Goalkeepers To Touch The Ball In The First Three Minutes Of The Match",
		"C"=>1
	],
	"8219"=>[
		"N"=>"Total Tie-Breaks",
		"C"=>1
	],
	"8221"=>[
		"N"=>"Total Tie-Breaks In First Set",
		"C"=>1
	],
	"8223"=>[
		"N"=>"Total Matches Where 1st Set Total Is Equal To 2nd Set Total",
		"C"=>1
	],
	"8225"=>[
		"N"=>"Total Matches With The Score",
		"C"=>1
	],
	"8227"=>[
		"N"=>"Total Matches Ending With The Score",
		"C"=>1
	],
	"8229"=>[
		"N"=>"A Goal To Be Awarded/Confirmed By VAR",
		"C"=>1
	],
	"8231"=>[
		"N"=>"A Goal To Be Disallowed/Not Confirmed By VAR",
		"C"=>1
	],
	"8233"=>[
		"N"=>"Player To Be Substituted",
		"C"=>1
	],
	"8235"=>[
		"N"=>"Team 1 To Lead After Player Scores",
		"C"=>1
	],
	"8237"=>[
		"N"=>"Team 2 To Lead After Player Scores",
		"C"=>1
	],
	"8239"=>[
		"N"=>"Team Of The Winner",
		"C"=>1
	],
	"8241"=>[
		"N"=>"Total (Including Super Tie-Break)",
		"C"=>1
	],
	"8243"=>[
		"N"=>"First Two Places (In The Specified Order)",
		"C"=>1
	],
	"8245"=>[
		"N"=>"Stage Of Elimination",
		"C"=>1
	],
	"8247"=>[
		"N"=>"Map, Total Stars",
		"C"=>1
	],
	"8249"=>[
		"N"=>"Time Out In The Fight",
		"C"=>1
	],
	"8251"=>[
		"N"=>"Points Deduction",
		"C"=>1
	],
	"8253"=>[
		"N"=>"Map, Total Points",
		"C"=>1
	],
	"8255"=>[
		"N"=>"Win In Round..",
		"C"=>1
	],
	"8257"=>[
		"N"=>"Stage Of Elimination - Quarter-Finals",
		"C"=>1
	],
	"8259"=>[
		"N"=>"Stage Of Elimination - Semi-Finals",
		"C"=>1
	],
	"8261"=>[
		"N"=>"Runner-up",
		"C"=>1
	],
	"8263"=>[
		"N"=>"Winner",
		"C"=>1
	],
	"8265"=>[
		"N"=>"Stage Of Elimination - Round 4",
		"C"=>1
	],
	"8267"=>[
		"N"=>"Stage Of Elimination - Round 3",
		"C"=>1
	],
	"8269"=>[
		"N"=>"Stage Of Elimination - Round 2",
		"C"=>1
	],
	"8271"=>[
		"N"=>"Stage Of Elimination - Round 1",
		"C"=>1
	],
	"8273"=>[
		"N"=>"In Which Round Will The Bout Be Won.",
		"C"=>1
	],
	"8275"=>[
		"N"=>"Maximum Number Of Creatures On The Table",
		"C"=>1
	],
	"8277"=>[
		"N"=>"Total Moves In The Game",
		"C"=>1
	],
	"8279"=>[
		"N"=>"Card Value On The Table",
		"C"=>1
	],
	"8281"=>[
		"N"=>"Total Legendary Cards Laid On The Table",
		"C"=>1
	],
	"8283"=>[
		"N"=>"Team 2 To Save Follow On",
		"C"=>1
	],
	"8285"=>[
		"N"=>"Teams To Be Drawn In The Same Group",
		"C"=>1
	],
	"8287"=>[
		"N"=>"Team 1 Total Runs In Over (3Way).",
		"C"=>1
	],
	"8289"=>[
		"N"=>"Team 1 Total Runs In Over (3Way)..",
		"C"=>1
	],
	"8291"=>[
		"N"=>"Team 1 Total Runs In Over (3Way)...",
		"C"=>1
	],
	"8293"=>[
		"N"=>"Team 1 Total Runs In Over (6Way).",
		"C"=>1
	],
	"8295"=>[
		"N"=>"Team 2 Total Runs In Over (3Way).",
		"C"=>1
	],
	"8297"=>[
		"N"=>"Team 2 Total Runs In Over (3Way)..",
		"C"=>1
	],
	"8299"=>[
		"N"=>"Team 2 Total Runs In Over (3Way)...",
		"C"=>1
	],
	"8301"=>[
		"N"=>"Team 2 Total Runs In Over (6Way).",
		"C"=>1
	],
	"8303"=>[
		"N"=>"Extra Totals (5Way)",
		"C"=>1
	],
	"8305"=>[
		"N"=>"Highest Opening Partnership (2Way)",
		"C"=>1
	],
	"8307"=>[
		"N"=>"Win + All Teams To Score",
		"C"=>1
	],
	"8309"=>[
		"N"=>"Total 180s Of Each Player",
		"C"=>1
	],
	"8311"=>[
		"N"=>"Total 180s Per Match",
		"C"=>1
	],
	"8313"=>[
		"N"=>"Total 180s + Total Legs",
		"C"=>1
	],
	"8315"=>[
		"N"=>"Player With The Highest Checkout",
		"C"=>1
	],
	"8317"=>[
		"N"=>"Player With The Most 180s",
		"C"=>1
	],
	"8319"=>[
		"N"=>"Winner Of The First Semi-Final",
		"C"=>1
	],
	"8321"=>[
		"N"=>"Total 170 Checkouts",
		"C"=>1
	],
	"8323"=>[
		"N"=>"Total Legs With Nine Dart Finish",
		"C"=>1
	],
	"8325"=>[
		"N"=>"Total Legs",
		"C"=>1
	],
	"8327"=>[
		"N"=>"Winner Of The Second Semi-Final",
		"C"=>1
	],
	"8329"=>[
		"N"=>"Who Will Win..",
		"C"=>1
	],
	"8331"=>[
		"N"=>"Conference Of The Winner",
		"C"=>1
	],
	"8333"=>[
		"N"=>"Division Of The Winner",
		"C"=>1
	],
	"8335"=>[
		"N"=>"Region Of The Winner",
		"C"=>1
	],
	"8337"=>[
		"N"=>"Total Secrets Used",
		"C"=>1
	],
	"8339"=>[
		"N"=>"Total Spells Used",
		"C"=>1
	],
	"8341"=>[
		"N"=>"Series Score",
		"C"=>1
	],
	"8343"=>[
		"N"=>"Total Minions Used",
		"C"=>1
	],
	"8345"=>[
		"N"=>"Team 1, Win By Enough Runs For A Follow On",
		"C"=>1
	],
	"8347"=>[
		"N"=>"Team 2, Win By Enough Runs For A Follow On",
		"C"=>1
	],
	"8349"=>[
		"N"=>"Knockdown In Round",
		"C"=>1
	],
	"8351"=>[
		"N"=>"Total Knockdowns In Bout",
		"C"=>1
	],
	"8353"=>[
		"N"=>"Series Double Chance",
		"C"=>1
	],
	"8355"=>[
		"N"=>"To Knock Down Opponent",
		"C"=>1
	],
	"8357"=>[
		"N"=>"Player To Make The Cut",
		"C"=>1
	],
	"8359"=>[
		"N"=>"Regular Season Rushing Yards",
		"C"=>1
	],
	"8361"=>[
		"N"=>"Regular Season Passing Yards",
		"C"=>1
	],
	"8363"=>[
		"N"=>"Regular Season Receiving Yards",
		"C"=>1
	],
	"8365"=>[
		"N"=>"Who Will Be Knocked Down",
		"C"=>1
	],
	"8367"=>[
		"N"=>"Who Will Be Knocked Out",
		"C"=>1
	],
	"8369"=>[
		"N"=>"Who Will Be Examined By The Doctor",
		"C"=>1
	],
	"8371"=>[
		"N"=>"What Corner To Throw In The Towel",
		"C"=>1
	],
	"8373"=>[
		"N"=>"Who Will To Have A Standing Count",
		"C"=>1
	],
	"8375"=>[
		"N"=>"Who Will Make The Playoffs",
		"C"=>1
	],
	"8377"=>[
		"N"=>"Who Will Not Make The Playoffs",
		"C"=>1
	],
	"8379"=>[
		"N"=>"Leader After Hole",
		"C"=>1
	],
	"8381"=>[
		"N"=>"Type Of Score (3Way)",
		"C"=>1
	],
	"8383"=>[
		"N"=>"Type Of Score (6Way)",
		"C"=>1
	],
	"8385"=>[
		"N"=>"Next Score",
		"C"=>1
	],
	"8387"=>[
		"N"=>"Total Wins..",
		"C"=>1
	],
	"8389"=>[
		"N"=>"Win (2Way)",
		"C"=>1
	],
	"8391"=>[
		"N"=>"How Bout Will End",
		"C"=>1
	],
	"8393"=>[
		"N"=>"Who Will Be Disqualified",
		"C"=>1
	],
	"8395"=>[
		"N"=>"Duration Of The Match.",
		"C"=>1
	],
	"8397"=>[
		"N"=>"Winner's Total Workers At The End Of The Match",
		"C"=>1
	],
	"8399"=>[
		"N"=>"Winner's Total Warriors At The End Of The Match",
		"C"=>1
	],
	"8401"=>[
		"N"=>"Winner's Total Buildings At The End Of The Match",
		"C"=>1
	],
	"8403"=>[
		"N"=>"Which Team Will Score Last And Win Game",
		"C"=>1
	],
	"8405"=>[
		"N"=>"Team To Score Last And Win Game",
		"C"=>1
	],
	"8407"=>[
		"N"=>"Which Team Will Score Last And Lose Game",
		"C"=>1
	],
	"8409"=>[
		"N"=>"Team With Highest Scoring Inning",
		"C"=>1
	],
	"8411"=>[
		"N"=>"Inning Of 1st Score",
		"C"=>1
	],
	"8413"=>[
		"N"=>"Inning Of Last Score",
		"C"=>1
	],
	"8415"=>[
		"N"=>"VoidRay",
		"C"=>1
	],
	"8417"=>[
		"N"=>"DarkTemplar",
		"C"=>1
	],
	"8419"=>[
		"N"=>"Thor",
		"C"=>1
	],
	"8421"=>[
		"N"=>"Battlecruiser",
		"C"=>1
	],
	"8423"=>[
		"N"=>"Ultralisk",
		"C"=>1
	],
	"8425"=>[
		"N"=>"BroodLord",
		"C"=>1
	],
	"8427"=>[
		"N"=>"Asian Team Total 1",
		"C"=>1
	],
	"8429"=>[
		"N"=>"Asian Team Total 2",
		"C"=>1
	],
	"8431"=>[
		"N"=>"Will There Be Grand Slam",
		"C"=>1
	],
	"8433"=>[
		"N"=>"Dealer To Score 21 Points",
		"C"=>1
	],
	"8435"=>[
		"N"=>"Player To Score 21 Points",
		"C"=>1
	],
	"8437"=>[
		"N"=>"21 Points Will Be Scored",
		"C"=>1
	],
	"8439"=>[
		"N"=>"Game To End Straight After Deal",
		"C"=>1
	],
	"8441"=>[
		"N"=>"Game To End On Bust",
		"C"=>1
	],
	"8443"=>[
		"N"=>"Dealer To Get A Card (Suit)",
		"C"=>1
	],
	"8445"=>[
		"N"=>"Player To Get A Card (Suit)",
		"C"=>1
	],
	"8447"=>[
		"N"=>"Dealer To Get A Card (Rank)",
		"C"=>1
	],
	"8449"=>[
		"N"=>"Player To Get A Card (Rank)",
		"C"=>1
	],
	"8451"=>[
		"N"=>"Dealer To Get A Card",
		"C"=>1
	],
	"8453"=>[
		"N"=>"Player To Get A Card",
		"C"=>1
	],
	"8455"=>[
		"N"=>"Place 15th To 16th",
		"C"=>1
	],
	"8457"=>[
		"N"=>"Place 17th To 18th",
		"C"=>1
	],
	"8459"=>[
		"N"=>"Place 18th To 20th",
		"C"=>1
	],
	"8461"=>[
		"N"=>"Next Ace (3Way)",
		"C"=>1
	],
	"8463"=>[
		"N"=>"Next Serve Fault (3Way)",
		"C"=>1
	],
	"8465"=>[
		"N"=>"To Be Higher On Points (2Way)",
		"C"=>1
	],
	"8467"=>[
		"N"=>"Win To Nil In One Of The Rounds",
		"C"=>1
	],
	"8469"=>[
		"N"=>"Total Points In Set",
		"C"=>1
	],
	"8471"=>[
		"N"=>"Minimum Total Points Per Goal In Match",
		"C"=>1
	],
	"8473"=>[
		"N"=>"Maximum Total Points Per Goal In Match",
		"C"=>1
	],
	"8475"=>[
		"N"=>"Most 180s In Match Including Handicap",
		"C"=>1
	],
	"8477"=>[
		"N"=>"Match Winning Checkout",
		"C"=>1
	],
	"8479"=>[
		"N"=>"First Checkout Points",
		"C"=>1
	],
	"8481"=>[
		"N"=>"Match To Finish On Bull",
		"C"=>1
	],
	"8483"=>[
		"N"=>"Match With Most 180s",
		"C"=>1
	],
	"8485"=>[
		"N"=>"Match With The Highest Checkout",
		"C"=>1
	],
	"8487"=>[
		"N"=>"Driver To Complete The Race",
		"C"=>1
	],
	"8489"=>[
		"N"=>"Will There Be \"Golden Point\"",
		"C"=>1
	],
	"8491"=>[
		"N"=>"Player To Receive \"Golden Point\"",
		"C"=>1
	],
	"8493"=>[
		"N"=>"Dealer To Receive \"Golden Point\"",
		"C"=>1
	],
	"8495"=>[
		"N"=>"Player To Go Bust",
		"C"=>1
	],
	"8497"=>[
		"N"=>"Dealer To Go Bust",
		"C"=>1
	],
	"8499"=>[
		"N"=>"Winning Hand",
		"C"=>1
	],
	"8501"=>[
		"N"=>"Color Of Last River Card",
		"C"=>1
	],
	"8503"=>[
		"N"=>"Suit Of Last River Card",
		"C"=>1
	],
	"8505"=>[
		"N"=>"Winner's Winning Hand",
		"C"=>1
	],
	"8507"=>[
		"N"=>"Color Of Last Turn Card",
		"C"=>1
	],
	"8509"=>[
		"N"=>"Suit Of Last Turn Card",
		"C"=>1
	],
	"8511"=>[
		"N"=>"Next Score (3Way)",
		"C"=>1
	],
	"8513"=>[
		"N"=>"Accumulator Outcomes",
		"C"=>1
	],
	"8515"=>[
		"N"=>"Team To Score The First Goal Of The Match Day",
		"C"=>1
	],
	"8517"=>[
		"N"=>"Total Matches In Which Team Will Score",
		"C"=>1
	],
	"8519"=>[
		"N"=>"Total Matches In Which Team Will Concede",
		"C"=>1
	],
	"8521"=>[
		"N"=>"Total Rounds Ending With Cards Beaten",
		"C"=>1
	],
	"8523"=>[
		"N"=>"Total Rounds Ending With Cards Taken",
		"C"=>1
	],
	"8525"=>[
		"N"=>"Player's Total Cards At The End Of The Game",
		"C"=>1
	],
	"8527"=>[
		"N"=>"Players. Total Hits",
		"C"=>1
	],
	"8529"=>[
		"N"=>"Total Stolen Bases",
		"C"=>1
	],
	"8531"=>[
		"N"=>"Total Doubles",
		"C"=>1
	],
	"8533"=>[
		"N"=>"Total Double Plays",
		"C"=>1
	],
	"8535"=>[
		"N"=>"First Turn",
		"C"=>1
	],
	"8537"=>[
		"N"=>"Who Will Take Last Card From Deck",
		"C"=>1
	],
	"8539"=>[
		"N"=>"Trump Suit",
		"C"=>1
	],
	"8541"=>[
		"N"=>"Total Rounds",
		"C"=>1
	],
	"8543"=>[
		"N"=>"Round (Cards Taken/Beaten)",
		"C"=>1
	],
	"8545"=>[
		"N"=>"Last Card In Deck To Be Face Card Or Ace",
		"C"=>1
	],
	"8547"=>[
		"N"=>"Player To Hold 4 Cards Of The Same Rank",
		"C"=>1
	],
	"8549"=>[
		"N"=>"Who Will Draw Trump Ace From The Deck",
		"C"=>1
	],
	"8551"=>[
		"N"=>"Shots On Target, Race",
		"C"=>1
	],
	"8553"=>[
		"N"=>"How Many Quarters Will Team 1 Win",
		"C"=>1
	],
	"8555"=>[
		"N"=>"How Many Quarters Will Team 2 Win",
		"C"=>1
	],
	"8557"=>[
		"N"=>"Team To Win A Half",
		"C"=>1
	],
	"8559"=>[
		"N"=>"How Many Points Will Team Score In Each Quarter",
		"C"=>1
	],
	"8561"=>[
		"N"=>"How Many Points Will Team Score In Each Half",
		"C"=>1
	],
	"8563"=>[
		"N"=>"Team 1 To Win All Quarters With Difference",
		"C"=>1
	],
	"8565"=>[
		"N"=>"Team 2 To Win All Quarters With Difference",
		"C"=>1
	],
	"8567"=>[
		"N"=>"Team 1 To Win Both Halves With Difference",
		"C"=>1
	],
	"8569"=>[
		"N"=>"Team 2 To Win Both Halves With Difference",
		"C"=>1
	],
	"8571"=>[
		"N"=>"Top Team Driver",
		"C"=>1
	],
	"8573"=>[
		"N"=>"Player's Total Points In Round",
		"C"=>1
	],
	"8575"=>[
		"N"=>"First Test Match / Series",
		"C"=>1
	],
	"8577"=>[
		"N"=>"First Win In Test Match",
		"C"=>1
	],
	"8579"=>[
		"N"=>"Player To Score And Provide An Assist",
		"C"=>1
	],
	"8581"=>[
		"N"=>"Players, Special",
		"C"=>1
	],
	"8583"=>[
		"N"=>"Who Will Lead The Series",
		"C"=>1
	],
	"8585"=>[
		"N"=>"How Many Matches Will Be Over By The End Of The Day",
		"C"=>1
	],
	"8587"=>[
		"N"=>"Will There Be Extra Time",
		"C"=>1
	],
	"8589"=>[
		"N"=>"Who Will Take Part In The Fight",
		"C"=>1
	],
	"8591"=>[
		"N"=>"Unit To Survive",
		"C"=>1
	],
	"8593"=>[
		"N"=>"Creature Type To Survive",
		"C"=>1
	],
	"8595"=>[
		"N"=>"What Level Creatures Will Survive",
		"C"=>1
	],
	"8597"=>[
		"N"=>"How Many Squads Will Survive",
		"C"=>1
	],
	"8599"=>[
		"N"=>"SpellPowers",
		"C"=>1
	],
	"8601"=>[
		"N"=>"Right-Footed Goal",
		"C"=>1
	],
	"8603"=>[
		"N"=>"Left-Footed Goal",
		"C"=>1
	],
	"8605"=>[
		"N"=>"Player To Score From The Penalty Area",
		"C"=>1
	],
	"8607"=>[
		"N"=>"Goal + Assist",
		"C"=>1
	],
	"8609"=>[
		"N"=>"Goal + Assist + Yellow Card",
		"C"=>1
	],
	"8611"=>[
		"N"=>"Throw-In",
		"C"=>1
	],
	"8613"=>[
		"N"=>"Player Not To Score Awarded Penalty",
		"C"=>1
	],
	"8615"=>[
		"N"=>"How Many Points Will The Winner Score",
		"C"=>1
	],
	"8619"=>[
		"N"=>"Who Will Commit The Next Foul",
		"C"=>1
	],
	"8621"=>[
		"N"=>"Fouls, Race",
		"C"=>1
	],
	"8623"=>[
		"N"=>"Best Competitor Of A Country",
		"C"=>1
	],
	"8625"=>[
		"N"=>"Best Competitor. Country Match-ups",
		"C"=>1
	],
	"8627"=>[
		"N"=>"First Dart",
		"C"=>1
	],
	"8629"=>[
		"N"=>"First Three Darts",
		"C"=>1
	],
	"8631"=>[
		"N"=>"Competitors Without Misses",
		"C"=>1
	],
	"8633"=>[
		"N"=>"Least Accurate Shooter (Total Misses)",
		"C"=>1
	],
	"8635"=>[
		"N"=>"Test Match / Series",
		"C"=>1
	],
	"8637"=>[
		"N"=>"Nine Dart Finishes",
		"C"=>1
	],
	"8639"=>[
		"N"=>"First To Destroy Inhibitor",
		"C"=>1
	],
	"8641"=>[
		"N"=>"Team 1, Total Frags",
		"C"=>1
	],
	"8643"=>[
		"N"=>"Team 2, Total Frags",
		"C"=>1
	],
	"8645"=>[
		"N"=>"First To Destroy Enemy's Tower",
		"C"=>1
	],
	"8647"=>[
		"N"=>"Total Collected Gold",
		"C"=>1
	],
	"8649"=>[
		"N"=>"Team 1, Total Gold Collected",
		"C"=>1
	],
	"8651"=>[
		"N"=>"Team 2, Total Gold Collected",
		"C"=>1
	],
	"8653"=>[
		"N"=>"First Frag",
		"C"=>1
	],
	"8655"=>[
		"N"=>"Players, Match-Ups, Rushing/Receiving Yards, Handicap",
		"C"=>1
	],
	"8659"=>[
		"N"=>"To Win Most Lags",
		"C"=>1
	],
	"8661"=>[
		"N"=>"Total Golden Breaks",
		"C"=>1
	],
	"8663"=>[
		"N"=>"Team 1 To Win 1st And 16th Rounds And The Map",
		"C"=>1
	],
	"8665"=>[
		"N"=>"Team 2 To Win 1st And 16th Rounds And The Map",
		"C"=>1
	],
	"8667"=>[
		"N"=>"Team 1, Difference In Number Of Rounds Won",
		"C"=>1
	],
	"8669"=>[
		"N"=>"Team 2, Difference In Number Of Rounds Won",
		"C"=>1
	],
	"8671"=>[
		"N"=>"Total Rounds Won By Time Ending",
		"C"=>1
	],
	"8673"=>[
		"N"=>"Total Rounds Won With Bomb Defusal",
		"C"=>1
	],
	"8675"=>[
		"N"=>"Total Rounds Won With Bomb Explosion",
		"C"=>1
	],
	"8677"=>[
		"N"=>"Most First Frags In All Rounds",
		"C"=>1
	],
	"8679"=>[
		"N"=>"Most Clutches Won",
		"C"=>1
	],
	"8681"=>[
		"N"=>"Cards",
		"C"=>1
	],
	"8683"=>[
		"N"=>"Winner In Half",
		"C"=>1
	],
	"8685"=>[
		"N"=>"Hero To Be Banned",
		"C"=>1
	],
	"8687"=>[
		"N"=>"Team 1 To Win Rounds",
		"C"=>1
	],
	"8689"=>[
		"N"=>"Team 2 To Win Rounds",
		"C"=>1
	],
	"8691"=>[
		"N"=>"Either Fighter To Win By Painful Lock/Chokehold",
		"C"=>1
	],
	"8693"=>[
		"N"=>"Total Touchdowns",
		"C"=>1
	],
	"8695"=>[
		"N"=>"Last TD",
		"C"=>1
	],
	"8697"=>[
		"N"=>"First TD / Match Winner",
		"C"=>1
	],
	"8699"=>[
		"N"=>"Any Time TD / Match Winner",
		"C"=>1
	],
	"8701"=>[
		"N"=>"Total Rush Attempts By Player",
		"C"=>1
	],
	"8703"=>[
		"N"=>"Player Performance / Match Winner",
		"C"=>1
	],
	"8705"=>[
		"N"=>"Type Of Turnover",
		"C"=>1
	],
	"8707"=>[
		"N"=>"Total Passing Yards",
		"C"=>1
	],
	"8709"=>[
		"N"=>"Total Receiving Yards",
		"C"=>1
	],
	"8711"=>[
		"N"=>"Total Rushing Yards",
		"C"=>1
	],
	"8713"=>[
		"N"=>"Total Receptions",
		"C"=>1
	],
	"8715"=>[
		"N"=>"Total Tackles + Assists",
		"C"=>1
	],
	"8717"=>[
		"N"=>"Total Rushing Yards + Receiving Yards",
		"C"=>1
	],
	"8723"=>[
		"N"=>"Exact Number Of Points In Group",
		"C"=>1
	],
	"8725"=>[
		"N"=>"When Will Bout End.",
		"C"=>1
	],
	"8727"=>[
		"N"=>"First Three Places (In The Specified Order)",
		"C"=>1
	],
	"8729"=>[
		"N"=>"Team 1, Each Period Total Under",
		"C"=>1
	],
	"8731"=>[
		"N"=>"Team 1, Each Period Total Over",
		"C"=>1
	],
	"8733"=>[
		"N"=>"Team 2, Each Period Total Under",
		"C"=>1
	],
	"8735"=>[
		"N"=>"Team 2, Each Period Total Over",
		"C"=>1
	],
	"8737"=>[
		"N"=>"Team 1, Both Halves Total Over",
		"C"=>1
	],
	"8739"=>[
		"N"=>"Team 1, Both Halves Total Under",
		"C"=>1
	],
	"8741"=>[
		"N"=>"Team 2, Both Halves Total Over",
		"C"=>1
	],
	"8743"=>[
		"N"=>"Team 2, Both Halves Total Under",
		"C"=>1
	],
	"8745"=>[
		"N"=>"Least Played Map",
		"C"=>1
	],
	"8747"=>[
		"N"=>"Most Played Map",
		"C"=>1
	],
	"8749"=>[
		"N"=>"Any Map To End With Score",
		"C"=>1
	],
	"8751"=>[
		"N"=>"Win Successive Frames",
		"C"=>1
	],
	"8753"=>[
		"N"=>"All Away Teams To Score",
		"C"=>1
	],
	"8755"=>[
		"N"=>"W1 In The First Minute Of Any Round",
		"C"=>1
	],
	"8757"=>[
		"N"=>"W2 In The First Minute Of Any Round",
		"C"=>1
	],
	"8759"=>[
		"N"=>"Total Decision Wins",
		"C"=>1
	],
	"8761"=>[
		"N"=>"All Bouts To End Inside The Distance",
		"C"=>1
	],
	"8763"=>[
		"N"=>"Winner Of The Tournament + Top Goalscorer In The Tournament",
		"C"=>1
	],
	"8765"=>[
		"N"=>"Multigoal_test",
		"C"=>1
	],
	"8767"=>[
		"N"=>"First Serve Outcome",
		"C"=>1
	],
	"8769"=>[
		"N"=>"Last Ace",
		"C"=>1
	],
	"8771"=>[
		"N"=>"European Champion",
		"C"=>1
	],
	"8773"=>[
		"N"=>"First Two Places (In Any Order)",
		"C"=>1
	],
	"8775"=>[
		"N"=>"Number Of Gold Medals By Athlete",
		"C"=>1
	],
	"8777"=>[
		"N"=>"Number Of Silver Medals By Athlete",
		"C"=>1
	],
	"8779"=>[
		"N"=>"Number Of Bronze Medals By Athlete",
		"C"=>1
	],
	"8781"=>[
		"N"=>"Number Of Medals By Athlete",
		"C"=>1
	],
	"8783"=>[
		"N"=>"Super Over",
		"C"=>1
	],
	"8785"=>[
		"N"=>"Team From Which Group Will Win The Tournament",
		"C"=>1
	],
	"8787"=>[
		"N"=>"Leader After Legs",
		"C"=>1
	],
	"8789"=>[
		"N"=>"Score After Legs",
		"C"=>1
	],
	"8791"=>[
		"N"=>"Leg, Total Darts To Win Leg",
		"C"=>1
	],
	"8793"=>[
		"N"=>"Most 180s + Highest Checkout",
		"C"=>1
	],
	"8795"=>[
		"N"=>"Match Result + Most 180s + Highest Checkout",
		"C"=>1
	],
	"8797"=>[
		"N"=>"Legs, Race",
		"C"=>1
	],
	"8799"=>[
		"N"=>"Leg, First Throw Total Points",
		"C"=>1
	],
	"8801"=>[
		"N"=>"Team 1, Multi Goal",
		"C"=>1
	],
	"8803"=>[
		"N"=>"Team 2, Multi Goal",
		"C"=>1
	],
	"8805"=>[
		"N"=>"OAR, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"8807"=>[
		"N"=>"Receiving Yards",
		"C"=>1
	],
	"8809"=>[
		"N"=>"Top Europe Team",
		"C"=>1
	],
	"8811"=>[
		"N"=>"Top Asia Team",
		"C"=>1
	],
	"8813"=>[
		"N"=>"Top South America Team",
		"C"=>1
	],
	"8815"=>[
		"N"=>"Top Africa Team",
		"C"=>1
	],
	"8817"=>[
		"N"=>"Top CONCACAF Team",
		"C"=>1
	],
	"8819"=>[
		"N"=>"Iran, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"8821"=>[
		"N"=>"Morocco, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"8823"=>[
		"N"=>"Costa Rica, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"8825"=>[
		"N"=>"Nigeria, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"8827"=>[
		"N"=>"Panama, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"8829"=>[
		"N"=>"Saudi Arabia, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"8831"=>[
		"N"=>"Senegal, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"8833"=>[
		"N"=>"Japan, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"8835"=>[
		"N"=>"Confederation Winner",
		"C"=>1
	],
	"8837"=>[
		"N"=>"1st Stoppage",
		"C"=>1
	],
	"8839"=>[
		"N"=>"More Tries Or Successful Penalties",
		"C"=>1
	],
	"8841"=>[
		"N"=>"Team To Get A Yellow Card",
		"C"=>1
	],
	"8843"=>[
		"N"=>"Team To Get Bonus Point Try",
		"C"=>1
	],
	"8845"=>[
		"N"=>"Type Of First Score",
		"C"=>1
	],
	"8847"=>[
		"N"=>"Successful Penalty In Time Interval",
		"C"=>1
	],
	"8849"=>[
		"N"=>"Team 1 To Score Penalty In Time Interval",
		"C"=>1
	],
	"8851"=>[
		"N"=>"Team 2 To Score Penalty In Time Interval",
		"C"=>1
	],
	"8853"=>[
		"N"=>"Team 1, More Tries Or Successful Penalties",
		"C"=>1
	],
	"8855"=>[
		"N"=>"Team 2, More Tries Or Successful Penalties",
		"C"=>1
	],
	"8857"=>[
		"N"=>"Team 1, Try In Time Interval",
		"C"=>1
	],
	"8859"=>[
		"N"=>"Team 2, Try In Time Interval",
		"C"=>1
	],
	"8861"=>[
		"N"=>"1st Innings Batting Team, TO Runs",
		"C"=>1
	],
	"8863"=>[
		"N"=>"Correct Score (17Way)",
		"C"=>1
	],
	"8865"=>[
		"N"=>"Score In Interval",
		"C"=>1
	],
	"8867"=>[
		"N"=>"Total Incomplete Runs",
		"C"=>1
	],
	"8869"=>[
		"N"=>"Competition Number Of The Winner Even/Odd",
		"C"=>1
	],
	"8871"=>[
		"N"=>"Sum Of The Competition Numbers Of All Skiers On The Podium",
		"C"=>1
	],
	"8873"=>[
		"N"=>"First To Win Leg Against Throw (Break)",
		"C"=>1
	],
	"8875"=>[
		"N"=>"Outcome + Method Of Win",
		"C"=>1
	],
	"8877"=>[
		"N"=>"Ball Potted With Break Shot",
		"C"=>1
	],
	"8879"=>[
		"N"=>"Jump Off The Table",
		"C"=>1
	],
	"8881"=>[
		"N"=>"Who Will Pot Solid-Colored Balls",
		"C"=>1
	],
	"8883"=>[
		"N"=>"Who Will Pot Striped Balls",
		"C"=>1
	],
	"8885"=>[
		"N"=>"Exact Number Of Unpotted Balls On The Table (At The End Of The Game)",
		"C"=>1
	],
	"8887"=>[
		"N"=>"Number Of Goals In A Single Minute",
		"C"=>1
	],
	"8889"=>[
		"N"=>"Legs Result/Match Result",
		"C"=>1
	],
	"8891"=>[
		"N"=>"Country Of The Higher Placed Athlete",
		"C"=>1
	],
	"8893"=>[
		"N"=>"Highest Scoring Match",
		"C"=>1
	],
	"8895"=>[
		"N"=>"One Of The Athletes To Take A Certain Position",
		"C"=>1
	],
	"8897"=>[
		"N"=>"Total Draws (3Way)",
		"C"=>1
	],
	"8899"=>[
		"N"=>"Total Draws",
		"C"=>1
	],
	"8901"=>[
		"N"=>"Team 1, Maximum Score Difference During The Match",
		"C"=>1
	],
	"8903"=>[
		"N"=>"Team 2, Maximum Score Difference During The Match",
		"C"=>1
	],
	"8905"=>[
		"N"=>"Highest Individual Score (Player's Runs)",
		"C"=>1
	],
	"8907"=>[
		"N"=>"How Many Nations Will Be Represented On The Podium (Top 3)",
		"C"=>1
	],
	"8909"=>[
		"N"=>"Last Throw + Checkout",
		"C"=>1
	],
	"8911"=>[
		"N"=>"Both Goalkeepers To Touch The Puck In The First Minutes Of The Match",
		"C"=>1
	],
	"8913"=>[
		"N"=>"Puck Will Enter The Net But A Goal Will Not Be Awarded",
		"C"=>1
	],
	"8915"=>[
		"N"=>"First Half Anytime Correct Score",
		"C"=>1
	],
	"8917"=>[
		"N"=>"Second Half Anytime Correct Score",
		"C"=>1
	],
	"8919"=>[
		"N"=>"How Many Nations Will Be Represented On The Podium (Top 3)",
		"C"=>1
	],
	"8921"=>[
		"N"=>"Player To Score Most Sixes",
		"C"=>1
	],
	"8923"=>[
		"N"=>"Total Drawn Matches",
		"C"=>1
	],
	"8925"=>[
		"N"=>"How Many Points Will The Winner Of The Group Score",
		"C"=>1
	],
	"8927"=>[
		"N"=>"How Many Points Will The Bottom Team Of The Group Score",
		"C"=>1
	],
	"8929"=>[
		"N"=>"Total Goals In The Matches",
		"C"=>1
	],
	"8931"=>[
		"N"=>"At Least One Team Will Score",
		"C"=>1
	],
	"8933"=>[
		"N"=>"At Least One Team Will Concede",
		"C"=>1
	],
	"8935"=>[
		"N"=>"How Many Teams Will Score Equal Number Of Points",
		"C"=>1
	],
	"8937"=>[
		"N"=>"Sum Of Points Scored By Teams",
		"C"=>1
	],
	"8939"=>[
		"N"=>"To Score Most Goals In The Group",
		"C"=>1
	],
	"8941"=>[
		"N"=>"To Score Fewest Goals In The Group",
		"C"=>1
	],
	"8943"=>[
		"N"=>"To Concede Most Goals In The Group",
		"C"=>1
	],
	"8945"=>[
		"N"=>"To Concede Fewest Goals In The Group",
		"C"=>1
	],
	"8947"=>[
		"N"=>"Which Player Will Have The Highest 3-Dart Average",
		"C"=>1
	],
	"8949"=>[
		"N"=>"Individual Total, Statistics",
		"C"=>1
	],
	"8951"=>[
		"N"=>"Which Player Will Be The First In Their Team To Score A Goal",
		"C"=>1
	],
	"8953"=>[
		"N"=>"Player Performance",
		"C"=>1
	],
	"8955"=>[
		"N"=>"At Least One Team Will Not Concede",
		"C"=>1
	],
	"8957"=>[
		"N"=>"Result After First Overs",
		"C"=>1
	],
	"8959"=>[
		"N"=>"Player's Total Frags",
		"C"=>1
	],
	"8961"=>[
		"N"=>"Player Position",
		"C"=>1
	],
	"8963"=>[
		"N"=>"League Of The Winning Team",
		"C"=>1
	],
	"8965"=>[
		"N"=>"Division Of The Winning Team",
		"C"=>1
	],
	"8967"=>[
		"N"=>"Shirt Of First Try Scorer",
		"C"=>1
	],
	"8969"=>[
		"N"=>"Shirt Of Last Try Scorer",
		"C"=>1
	],
	"8971"=>[
		"N"=>"Either Team To Win Both Halves",
		"C"=>1
	],
	"8973"=>[
		"N"=>"National Team To Score Most Goals At Group Stage",
		"C"=>1
	],
	"8975"=>[
		"N"=>"National Team To Score Fewest Goals At Group Stage",
		"C"=>1
	],
	"8977"=>[
		"N"=>"National Team To Concede Fewest Goals At Group Stage",
		"C"=>1
	],
	"8979"=>[
		"N"=>"Total Goals From Direct Free Kick",
		"C"=>1
	],
	"8981"=>[
		"N"=>"Total Goals Scored By Defenders",
		"C"=>1
	],
	"8983"=>[
		"N"=>"Total Goals Scored By Midfielders",
		"C"=>1
	],
	"8985"=>[
		"N"=>"Total Goals Scored By Forwards",
		"C"=>1
	],
	"8987"=>[
		"N"=>"Total Own Goals By Opponents",
		"C"=>1
	],
	"8989"=>[
		"N"=>"Total Penalties Against Team",
		"C"=>1
	],
	"8991"=>[
		"N"=>"Goals. Accumulator Outcomes",
		"C"=>1
	],
	"8993"=>[
		"N"=>"Corners. Accumulator Outcomes",
		"C"=>1
	],
	"8995"=>[
		"N"=>"Cards. Accumulator Outcomes",
		"C"=>1
	],
	"8997"=>[
		"N"=>"Win. Accumulator Outcomes",
		"C"=>1
	],
	"8999"=>[
		"N"=>"Both Teams To Score. Accumulator Outcomes",
		"C"=>1
	],
	"9001"=>[
		"N"=>"Minute Intervals. Accumulator Outcomes",
		"C"=>1
	],
	"9003"=>[
		"N"=>"Passes. Accumulator Outcomes",
		"C"=>1
	],
	"9005"=>[
		"N"=>"Shots. Accumulator Outcomes",
		"C"=>1
	],
	"9007"=>[
		"N"=>"Tackles. Accumulator Outcomes",
		"C"=>1
	],
	"9009"=>[
		"N"=>"Players. Accumulator Outcomes",
		"C"=>1
	],
	"9011"=>[
		"N"=>"In Which Group Will Most Goals Be Scored?",
		"C"=>1
	],
	"9013"=>[
		"N"=>"In Which Group Will Fewest Goals Be Scored?",
		"C"=>1
	],
	"9015"=>[
		"N"=>"Winner For The First Time In History",
		"C"=>1
	],
	"9017"=>[
		"N"=>"1st Try / Match Result",
		"C"=>1
	],
	"9019"=>[
		"N"=>"Time, Total Tries",
		"C"=>1
	],
	"9021"=>[
		"N"=>"Score First / 1st Half Result / Match Outcome",
		"C"=>1
	],
	"9023"=>[
		"N"=>"First Scoring Play (4Way)",
		"C"=>1
	],
	"9025"=>[
		"N"=>"Match Result + Both Teams To Score",
		"C"=>1
	],
	"9027"=>[
		"N"=>"Successful 40/20",
		"C"=>1
	],
	"9029"=>[
		"N"=>"Team Performance In Half",
		"C"=>1
	],
	"9030"=>[
		"N"=>"1st Try / 1st Half Result",
		"C"=>1
	],
	"9031"=>[
		"N"=>"Time, Total Points Even/Odd",
		"C"=>1
	],
	"9032"=>[
		"N"=>"Time Of First Converted Penalty",
		"C"=>1
	],
	"9033"=>[
		"N"=>"Team 1, Time Of First Converted Penalty",
		"C"=>1
	],
	"9034"=>[
		"N"=>"Team 2, Time Of First Converted Penalty",
		"C"=>1
	],
	"9035"=>[
		"N"=>"Handicap, Tries",
		"C"=>1
	],
	"9036"=>[
		"N"=>"Total Penalties Scored",
		"C"=>1
	],
	"9037"=>[
		"N"=>"Team 1, Total Tries (3Way)",
		"C"=>1
	],
	"9038"=>[
		"N"=>"Team 2, Total Tries (3Way)",
		"C"=>1
	],
	"9039"=>[
		"N"=>"Australia, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"9040"=>[
		"N"=>"Team To Be Undefeated",
		"C"=>1
	],
	"9041"=>[
		"N"=>"Last Undefeated Team",
		"C"=>1
	],
	"9042"=>[
		"N"=>"How Many Cars Will Fail To Complete Lap",
		"C"=>1
	],
	"9043"=>[
		"N"=>"Leader After The First Lap",
		"C"=>1
	],
	"9044"=>[
		"N"=>"Total Team Drivers To Be Classified",
		"C"=>1
	],
	"9045"=>[
		"N"=>"Least Successful Team",
		"C"=>1
	],
	"9046"=>[
		"N"=>"Winning Margin Over 2nd Place (sec).",
		"C"=>1
	],
	"9047"=>[
		"N"=>"Winning Margin Over 2nd Place (sec) (2Way)",
		"C"=>1
	],
	"9048"=>[
		"N"=>"Winning Margin Over 2nd Place (sec)..",
		"C"=>1
	],
	"9049"=>[
		"N"=>"Winning Margin Over 2nd Place (sec)...",
		"C"=>1
	],
	"9050"=>[
		"N"=>"Total In Minute (3Way)",
		"C"=>1
	],
	"9051"=>[
		"N"=>"One Of The Drivers To Take The Specified Place",
		"C"=>1
	],
	"9052"=>[
		"N"=>"How Many Different Drivers Will Lead The Race",
		"C"=>1
	],
	"9053"=>[
		"N"=>"Place",
		"C"=>1
	],
	"9054"=>[
		"N"=>"To Take The Lead First",
		"C"=>1
	],
	"9055"=>[
		"N"=>"Team 1, Number Of Runs By Any Player",
		"C"=>1
	],
	"9056"=>[
		"N"=>"Team 2, Number Of Runs By Any Player",
		"C"=>1
	],
	"9057"=>[
		"N"=>"Who Will Meet In The Semi-finals",
		"C"=>1
	],
	"9058"=>[
		"N"=>"Score By Halves",
		"C"=>1
	],
	"9059"=>[
		"N"=>"Total Points After Quarter",
		"C"=>1
	],
	"9060"=>[
		"N"=>"Correct Number Of Tries (3Way)",
		"C"=>1
	],
	"9061"=>[
		"N"=>"Coach To Request Extra Video Review",
		"C"=>1
	],
	"9062"=>[
		"N"=>"Head Coach To Be Ejected",
		"C"=>1
	],
	"9063"=>[
		"N"=>"Country Representative At The Final Table",
		"C"=>1
	],
	"9064"=>[
		"N"=>"Best Representative Of Which Country Will Be Higher",
		"C"=>1
	],
	"9065"=>[
		"N"=>"Team Driver To Win",
		"C"=>1
	],
	"9066"=>[
		"N"=>"Last Double Fault",
		"C"=>1
	],
	"9067"=>[
		"N"=>"Top Goalscorer - Country Representative",
		"C"=>1
	],
	"9068"=>[
		"N"=>"Player To Score Last Home Run",
		"C"=>1
	],
	"9069"=>[
		"N"=>"Playoff To Decide The Winner",
		"C"=>1
	],
	"9070"=>[
		"N"=>"Goal Frame To Be Displaced During the Match",
		"C"=>1
	],
	"9071"=>[
		"N"=>"Teams To Play 5 On 3",
		"C"=>1
	],
	"9072"=>[
		"N"=>"Fight With No Gloves",
		"C"=>1
	],
	"9073"=>[
		"N"=>"Goalie To Earn A Point",
		"C"=>1
	],
	"9074"=>[
		"N"=>"Broken Glass During The Match",
		"C"=>1
	],
	"9075"=>[
		"N"=>"Goal From The Neutral Zone",
		"C"=>1
	],
	"9076"=>[
		"N"=>"Penalties. Accumulator Outcomes",
		"C"=>1
	],
	"9077"=>[
		"N"=>"Team 1, Player To Score Last Home Run",
		"C"=>1
	],
	"9078"=>[
		"N"=>"Team 2, Player To Score Last Home Run",
		"C"=>1
	],
	"9079"=>[
		"N"=>"Any Player To Score Specified Number Of Goals",
		"C"=>1
	],
	"9080"=>[
		"N"=>"Who Will Score First And Second Goals (In Any Order)?",
		"C"=>1
	],
	"9081"=>[
		"N"=>"Any Player To Get Specified Number Of Disposals",
		"C"=>1
	],
	"9082"=>[
		"N"=>"Tie-Break Or Extra Games In The Final Set",
		"C"=>1
	],
	"9083"=>[
		"N"=>"Players, Total Assists",
		"C"=>1
	],
	"9084"=>[
		"N"=>"Team To Win Specified Quarters",
		"C"=>1
	],
	"9085"=>[
		"N"=>"Total Team Points In Each Quarter",
		"C"=>1
	],
	"9086"=>[
		"N"=>"Player To Score First And Last",
		"C"=>1
	],
	"9087"=>[
		"N"=>"Quarters, Points",
		"C"=>1
	],
	"9088"=>[
		"N"=>"Players, Total Points",
		"C"=>1
	],
	"9089"=>[
		"N"=>"Players, Total Rebounds",
		"C"=>1
	],
	"9090"=>[
		"N"=>"Players, Points And Assists",
		"C"=>1
	],
	"9091"=>[
		"N"=>"Players, Points And Rebounds",
		"C"=>1
	],
	"9092"=>[
		"N"=>"Player, Assists And Rebounds",
		"C"=>1
	],
	"9093"=>[
		"N"=>"Players, Points And Triple-Double",
		"C"=>1
	],
	"9094"=>[
		"N"=>"To Have A Triple-Double",
		"C"=>1
	],
	"9095"=>[
		"N"=>"Match Stats",
		"C"=>1
	],
	"9096"=>[
		"N"=>"Lowest Scoring Team Total",
		"C"=>1
	],
	"9097"=>[
		"N"=>"Team 1 To Win The Series Having Trailed During The Series",
		"C"=>1
	],
	"9098"=>[
		"N"=>"Team 2 To Win The Series Having Trailed During The Series",
		"C"=>1
	],
	"9099"=>[
		"N"=>"Total Teams To Score In Each Period",
		"C"=>1
	],
	"9100"=>[
		"N"=>"Total Teams To Score Specified Number Of Goals",
		"C"=>1
	],
	"9101"=>[
		"N"=>"Teams To Score The First 2 Goals",
		"C"=>1
	],
	"9102"=>[
		"N"=>"Team To Win In Regular Time By Goals",
		"C"=>1
	],
	"9103"=>[
		"N"=>"Most Valuable Player In The Final Series",
		"C"=>1
	],
	"9104"=>[
		"N"=>"Top Forward In The Final Series",
		"C"=>1
	],
	"9105"=>[
		"N"=>"Top Defenseman In The Final Series",
		"C"=>1
	],
	"9106"=>[
		"N"=>"Top Goalie In The Final Series",
		"C"=>1
	],
	"9107"=>[
		"N"=>"Score During The Match",
		"C"=>1
	],
	"9108"=>[
		"N"=>"Team To Concede And Win",
		"C"=>1
	],
	"9109"=>[
		"N"=>"Team To Score And Not To Win",
		"C"=>1
	],
	"9110"=>[
		"N"=>"Team Not To Win Or Not To Concede",
		"C"=>1
	],
	"9111"=>[
		"N"=>"Team To Win Or Not To Score",
		"C"=>1
	],
	"9112"=>[
		"N"=>"Team 1, Total Serve Faults",
		"C"=>1
	],
	"9113"=>[
		"N"=>"Team 2, Total Serve Faults",
		"C"=>1
	],
	"9114"=>[
		"N"=>"Total Goals In Each Period",
		"C"=>1
	],
	"9115"=>[
		"N"=>"Player's Total Shots",
		"C"=>1
	],
	"9116"=>[
		"N"=>"Player's Total Shots On Target",
		"C"=>1
	],
	"9117"=>[
		"N"=>"Player's Total Passes",
		"C"=>1
	],
	"9118"=>[
		"N"=>"Player's Total Assists",
		"C"=>1
	],
	"9119"=>[
		"N"=>"Player's Total Tackles",
		"C"=>1
	],
	"9120"=>[
		"N"=>"Player's Total Tackles",
		"C"=>1
	],
	"9121"=>[
		"N"=>"Confederation Of The Winner",
		"C"=>1
	],
	"9122"=>[
		"N"=>"National Team To Concede Most Goals At Group Stage",
		"C"=>1
	],
	"9123"=>[
		"N"=>"Who Will Record A Double-Double",
		"C"=>1
	],
	"9124"=>[
		"N"=>"Who Will Record A Triple-Double",
		"C"=>1
	],
	"9125"=>[
		"N"=>"Total Wins In Group",
		"C"=>1
	],
	"9126"=>[
		"N"=>"Win In Interval (4Way)",
		"C"=>1
	],
	"9127"=>[
		"N"=>"Match Winner + Winner By Corners",
		"C"=>1
	],
	"9128"=>[
		"N"=>"Match Winner + Total Corners",
		"C"=>1
	],
	"9129"=>[
		"N"=>"Both Teams To Score + Total Corners",
		"C"=>1
	],
	"9130"=>[
		"N"=>"Match Handicap + Handicap By Corners",
		"C"=>1
	],
	"9131"=>[
		"N"=>"Match Handicap + Total Corners",
		"C"=>1
	],
	"9132"=>[
		"N"=>"Match Handicap + Winner By Corners",
		"C"=>1
	],
	"9133"=>[
		"N"=>"Team 1, Total In The Match + Team 1, Total Corners",
		"C"=>1
	],
	"9134"=>[
		"N"=>"Team 1, Total In The Match + Team 2, Total Corners",
		"C"=>1
	],
	"9135"=>[
		"N"=>"Team 2, Total In The Match + Team 1, Total Corners",
		"C"=>1
	],
	"9136"=>[
		"N"=>"Team 2, Total In The Match + Team 2, Total Corners",
		"C"=>1
	],
	"9137"=>[
		"N"=>"Team 1 Total + Total Corners In The Match",
		"C"=>1
	],
	"9138"=>[
		"N"=>"Team 2 Total + Total Corners In The Match",
		"C"=>1
	],
	"9139"=>[
		"N"=>"Team 1, Total In The Match + Winner By Corners",
		"C"=>1
	],
	"9140"=>[
		"N"=>"Team 2, Total In The Match + Winner By Corners",
		"C"=>1
	],
	"9141"=>[
		"N"=>"Which Team Will Be The First To Score Points (2Way)?",
		"C"=>1
	],
	"9142"=>[
		"N"=>"Team Of Winning Driver",
		"C"=>1
	],
	"9143"=>[
		"N"=>"Frames Result/Match Result",
		"C"=>1
	],
	"9144"=>[
		"N"=>"Draw 0-0 + Both Teams To Score",
		"C"=>1
	],
	"9145"=>[
		"N"=>"Who Will Win",
		"C"=>1
	],
	"9146"=>[
		"N"=>"Player To Score Points In All Matches In The Series",
		"C"=>1
	],
	"9147"=>[
		"N"=>"Who Will Be Knocked Down And Go The Distance",
		"C"=>1
	],
	"9148"=>[
		"N"=>"Player To Make The Highest Break",
		"C"=>1
	],
	"9149"=>[
		"N"=>"How Will The Bout Be Won",
		"C"=>1
	],
	"9150"=>[
		"N"=>"Teams To Finish 8th In The Group Stage",
		"C"=>1
	],
	"9151"=>[
		"N"=>"Team 1 Total In Minute",
		"C"=>1
	],
	"9152"=>[
		"N"=>"Team 2 Total In Minute",
		"C"=>1
	],
	"9153"=>[
		"N"=>"How Will The Match Be Won",
		"C"=>1
	],
	"9154"=>[
		"N"=>"Number Of Matches With The Same Or Mirror Score",
		"C"=>1
	],
	"9155"=>[
		"N"=>"Team To Have Most Corners",
		"C"=>1
	],
	"9156"=>[
		"N"=>"Most Scoring Group",
		"C"=>1
	],
	"9157"=>[
		"N"=>"Least Scoring Group",
		"C"=>1
	],
	"9158"=>[
		"N"=>"At Least One Team Will Not Score",
		"C"=>1
	],
	"9159"=>[
		"N"=>"At Least One Team Will Score Specified Number Of Points",
		"C"=>1
	],
	"9160"=>[
		"N"=>"Top Goalscorer",
		"C"=>1
	],
	"9161"=>[
		"N"=>"Total Points Even/Odd",
		"C"=>1
	],
	"9162"=>[
		"N"=>"Match Winner By Rounds",
		"C"=>1
	],
	"9163"=>[
		"N"=>"Match Winner By Points",
		"C"=>1
	],
	"9164"=>[
		"N"=>"Canada, Top Goalscorer",
		"C"=>1
	],
	"9165"=>[
		"N"=>"Czech Republic, Top Goalscorer",
		"C"=>1
	],
	"9166"=>[
		"N"=>"Denmark, Top Goalscorer",
		"C"=>1
	],
	"9167"=>[
		"N"=>"Norway, Top Goalscorer",
		"C"=>1
	],
	"9168"=>[
		"N"=>"Russia, Top Goalscorer",
		"C"=>1
	],
	"9169"=>[
		"N"=>"USA, Top Goalscorer",
		"C"=>1
	],
	"9170"=>[
		"N"=>"Sweden, Top Goalscorer",
		"C"=>1
	],
	"9171"=>[
		"N"=>"Finland, Top Goalscorer",
		"C"=>1
	],
	"9172"=>[
		"N"=>"Both Teams To Score In Both Halves",
		"C"=>1
	],
	"9173"=>[
		"N"=>"Team 1 To Score In Both Halves",
		"C"=>1
	],
	"9174"=>[
		"N"=>"Team 2 To Score In Both Halves",
		"C"=>1
	],
	"9175"=>[
		"N"=>"Goals In Both Halves",
		"C"=>1
	],
	"9176"=>[
		"N"=>"Switzerland, Top Goalscorer",
		"C"=>1
	],
	"9177"=>[
		"N"=>"Which Team Will Score More Goals In Group Stage?",
		"C"=>1
	],
	"9178"=>[
		"N"=>"Which Team Will Concede More Goals In Group Stage?",
		"C"=>1
	],
	"9179"=>[
		"N"=>"Score In The Series After 2 Matches",
		"C"=>1
	],
	"9180"=>[
		"N"=>"Winning Hand",
		"C"=>1
	],
	"9181"=>[
		"N"=>"Player, Total 6's",
		"C"=>1
	],
	"9182"=>[
		"N"=>"Player, Total 4's",
		"C"=>1
	],
	"9183"=>[
		"N"=>"Win The Toss/ The Match",
		"C"=>1
	],
	"9184"=>[
		"N"=>"Top Team Runscorer And Top Team Wicket Taker",
		"C"=>1
	],
	"9185"=>[
		"N"=>"Top Team Runscorer",
		"C"=>1
	],
	"9186"=>[
		"N"=>"Team To Win, Top Runscorer",
		"C"=>1
	],
	"9187"=>[
		"N"=>"First Ball Of Match",
		"C"=>1
	],
	"9188"=>[
		"N"=>"Player's Total Runs, Interval",
		"C"=>1
	],
	"9189"=>[
		"N"=>"Team To Win By A Number Of Wickets Or Runs",
		"C"=>1
	],
	"9190"=>[
		"N"=>"Most Sixes And Win In Match",
		"C"=>1
	],
	"9191"=>[
		"N"=>"Player To Score Six And Out",
		"C"=>1
	],
	"9192"=>[
		"N"=>"Team To Win The Series Without Taking The Lead In The Series",
		"C"=>1
	],
	"9193"=>[
		"N"=>"Who Will Win The Lag",
		"C"=>1
	],
	"9194"=>[
		"N"=>"Winner Without Team",
		"C"=>1
	],
	"9195"=>[
		"N"=>"3Way Total",
		"C"=>1
	],
	"9196"=>[
		"N"=>"Individual 3Way Total 1",
		"C"=>1
	],
	"9197"=>[
		"N"=>"Individual 3Way Total 2",
		"C"=>1
	],
	"9198"=>[
		"N"=>"Total Goals Scored By One Of The Players",
		"C"=>1
	],
	"9199"=>[
		"N"=>"How Will First Goal Be Scored",
		"C"=>1
	],
	"9201"=>[
		"N"=>"How Will First Goal Be Scored By Team 1",
		"C"=>1
	],
	"9202"=>[
		"N"=>"How Will First Goal Be Scored By Team 2",
		"C"=>1
	],
	"9203"=>[
		"N"=>"Total Right-Footed Goals In The Match",
		"C"=>1
	],
	"9204"=>[
		"N"=>"Total Left-Footed Goals In The Match",
		"C"=>1
	],
	"9205"=>[
		"N"=>"Team 1, Total Right-Footed Goals In The Match",
		"C"=>1
	],
	"9206"=>[
		"N"=>"Team 1, Total Left-Footed Goals In The Match",
		"C"=>1
	],
	"9207"=>[
		"N"=>"Team 2, Total Right-Footed Goals In The Match",
		"C"=>1
	],
	"9208"=>[
		"N"=>"Team 2, Total Left-Footed Goals In The Match",
		"C"=>1
	],
	"9209"=>[
		"N"=>"To Reach Semi-finals",
		"C"=>1
	],
	"9210"=>[
		"N"=>"Tie-Break Leader",
		"C"=>1
	],
	"9211"=>[
		"N"=>"Win In Straight Sets",
		"C"=>1
	],
	"9212"=>[
		"N"=>"Winner And Number Of Legs Won",
		"C"=>1
	],
	"9213"=>[
		"N"=>"First Points And Winner",
		"C"=>1
	],
	"9214"=>[
		"N"=>"Player's And Team's Total Points",
		"C"=>1
	],
	"9215"=>[
		"N"=>"Player's Total Frags",
		"C"=>1
	],
	"9216"=>[
		"N"=>"Player's Total Deaths",
		"C"=>1
	],
	"9217"=>[
		"N"=>"Player To Survive In Round",
		"C"=>1
	],
	"9218"=>[
		"N"=>"Player's Total Frags In Round",
		"C"=>1
	],
	"9219"=>[
		"N"=>"First To Win A Return Point On A Tie-Break",
		"C"=>1
	],
	"9220"=>[
		"N"=>"To Reach The Final And Lose Final Series",
		"C"=>1
	],
	"9221"=>[
		"N"=>"Top Australasia Team",
		"C"=>1
	],
	"9222"=>[
		"N"=>"Eliminated By Penalty Shootout",
		"C"=>1
	],
	"9223"=>[
		"N"=>"Player To Score Winning Goal",
		"C"=>1
	],
	"9224"=>[
		"N"=>"Who Will Score",
		"C"=>1
	],
	"9225"=>[
		"N"=>"Type Of First Score",
		"C"=>1
	],
	"9226"=>[
		"N"=>"Team Wins The Match Having Been Behind",
		"C"=>1
	],
	"9227"=>[
		"N"=>"Serbia, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"9228"=>[
		"N"=>"Uruguay, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"9229"=>[
		"N"=>"Which Team Will Concede More Goals",
		"C"=>1
	],
	"9230"=>[
		"N"=>"Which Team Will Score More Goals",
		"C"=>1
	],
	"9231"=>[
		"N"=>"Which Team Will Score Most Goals",
		"C"=>1
	],
	"9232"=>[
		"N"=>"Team To Concede Most Goals",
		"C"=>1
	],
	"9233"=>[
		"N"=>"Team To Concede Fewest Goals",
		"C"=>1
	],
	"9234"=>[
		"N"=>"Player To Win All Sets",
		"C"=>1
	],
	"9235"=>[
		"N"=>"Total Wins From Behind In Regular Time",
		"C"=>1
	],
	"9236"=>[
		"N"=>"Score In Matches",
		"C"=>1
	],
	"9237"=>[
		"N"=>"Maximum Yellow Cards In Half",
		"C"=>1
	],
	"9238"=>[
		"N"=>"Maximum Red Cards In Half",
		"C"=>1
	],
	"9239"=>[
		"N"=>"Maximum Penalties In Half",
		"C"=>1
	],
	"9240"=>[
		"N"=>"How Many Teams Will Score Specified Number Of Goals",
		"C"=>1
	],
	"9241"=>[
		"N"=>"Maximum Corners In Half",
		"C"=>1
	],
	"9242"=>[
		"N"=>"Event",
		"C"=>1
	],
	"9244"=>[
		"N"=>"Event",
		"C"=>1
	],
	"9245"=>[
		"N"=>"Team To Score Fewest Goals",
		"C"=>1
	],
	"9247"=>[
		"N"=>"Team To Receive Fewest Yellow Cards",
		"C"=>1
	],
	"9248"=>[
		"N"=>"Team, Total Goals Scored In 1st Half",
		"C"=>1
	],
	"9249"=>[
		"N"=>"Team, Total Goals Scored In 2nd Half",
		"C"=>1
	],
	"9250"=>[
		"N"=>"Team, Total Bookings",
		"C"=>1
	],
	"9251"=>[
		"N"=>"Team, Total Substitutions",
		"C"=>1
	],
	"9252"=>[
		"N"=>"Team, Total Number Of Players Taking Part In The Match",
		"C"=>1
	],
	"9253"=>[
		"N"=>"First Four Places In Order",
		"C"=>1
	],
	"9254"=>[
		"N"=>"Top Club Goalscorer",
		"C"=>1
	],
	"9255"=>[
		"N"=>"Bayern Munich, Top Club Goalscorer In The Tournament",
		"C"=>1
	],
	"9256"=>[
		"N"=>"Barcelona, Top Club Goalscorer In The Tournament",
		"C"=>1
	],
	"9257"=>[
		"N"=>"Manchester City, Top Club Goalscorer In The Tournament",
		"C"=>1
	],
	"9258"=>[
		"N"=>"Manchester United, Top Club Goalscorer In The Tournament",
		"C"=>1
	],
	"9259"=>[
		"N"=>"Paris Saint-Germain, Top Club Goalscorer In The Tournament",
		"C"=>1
	],
	"9260"=>[
		"N"=>"Real Madrid, Top Club Goalscorer In The Tournament",
		"C"=>1
	],
	"9261"=>[
		"N"=>"Chelsea, Top Club Goalscorer In The Tournament",
		"C"=>1
	],
	"9262"=>[
		"N"=>"Player To Survive",
		"C"=>1
	],
	"9263"=>[
		"N"=>"Total Goals By Top Goalscorer",
		"C"=>1
	],
	"9264"=>[
		"N"=>"Total Conceded Goals In 1st Half",
		"C"=>1
	],
	"9265"=>[
		"N"=>"Total Conceded Goals In 2nd Half",
		"C"=>1
	],
	"9266"=>[
		"N"=>"Total Wins From Behind",
		"C"=>1
	],
	"9267"=>[
		"N"=>"Who Will Score First Points and How",
		"C"=>1
	],
	"9268"=>[
		"N"=>"Who Will Score Last Points and How",
		"C"=>1
	],
	"9269"=>[
		"N"=>"Triple-Doubles",
		"C"=>1
	],
	"9270"=>[
		"N"=>"Two Finalists",
		"C"=>1
	],
	"9271"=>[
		"N"=>"Total Yellow Cards In 1st Half",
		"C"=>1
	],
	"9272"=>[
		"N"=>"Total Yellow Cards In 2nd Half",
		"C"=>1
	],
	"9273"=>[
		"N"=>"Total Offsides In 1st Half",
		"C"=>1
	],
	"9274"=>[
		"N"=>"Total Offsides In 2nd Half",
		"C"=>1
	],
	"9275"=>[
		"N"=>"Converted Penalties",
		"C"=>1
	],
	"9276"=>[
		"N"=>"Unconverted Penalties",
		"C"=>1
	],
	"9277"=>[
		"N"=>"Penalties Awarded",
		"C"=>1
	],
	"9278"=>[
		"N"=>"Total Fouls In 1st Half",
		"C"=>1
	],
	"9279"=>[
		"N"=>"Total Fouls In 2nd Half",
		"C"=>1
	],
	"9280"=>[
		"N"=>"Total Corners In 1st Half",
		"C"=>1
	],
	"9281"=>[
		"N"=>"Total Corners In 2nd Half",
		"C"=>1
	],
	"9282"=>[
		"N"=>"Two-Point Field Goals",
		"C"=>1
	],
	"9283"=>[
		"N"=>"Three-Point Field Goals",
		"C"=>1
	],
	"9284"=>[
		"N"=>"Free Throws Scored",
		"C"=>1
	],
	"9285"=>[
		"N"=>"Rebounds",
		"C"=>1
	],
	"9286"=>[
		"N"=>"Assists",
		"C"=>1
	],
	"9287"=>[
		"N"=>"Steals",
		"C"=>1
	],
	"9288"=>[
		"N"=>"Blocks",
		"C"=>1
	],
	"9289"=>[
		"N"=>"Player's Double-Doubles",
		"C"=>1
	],
	"9290"=>[
		"N"=>"Goals In Group Stage",
		"C"=>1
	],
	"9291"=>[
		"N"=>"Regions Of Finalists",
		"C"=>1
	],
	"9292"=>[
		"N"=>"Team Stats",
		"C"=>1
	],
	"9293"=>[
		"N"=>"Team's Total Fouls",
		"C"=>1
	],
	"9294"=>[
		"N"=>"Team's Total Offsides",
		"C"=>1
	],
	"9295"=>[
		"N"=>"First Unforced Error",
		"C"=>1
	],
	"9296"=>[
		"N"=>"First Break Point",
		"C"=>1
	],
	"9297"=>[
		"N"=>"Final Forecast",
		"C"=>1
	],
	"9298"=>[
		"N"=>"Total Points",
		"C"=>1
	],
	"9299"=>[
		"N"=>"Ace And Double Fault",
		"C"=>1
	],
	"9300"=>[
		"N"=>"Unforced Errors",
		"C"=>1
	],
	"9301"=>[
		"N"=>"Games Won In A Row",
		"C"=>1
	],
	"9302"=>[
		"N"=>"Goals Conceded From Free Kicks",
		"C"=>1
	],
	"9303"=>[
		"N"=>"Lowest Scoring Match",
		"C"=>1
	],
	"9304"=>[
		"N"=>"3-Point Field Goal Percentage",
		"C"=>1
	],
	"9305"=>[
		"N"=>"Most Yellow Cards (Player)",
		"C"=>1
	],
	"9306"=>[
		"N"=>"Most Yellow Cards (Team)",
		"C"=>1
	],
	"9307"=>[
		"N"=>"Fewest Yellow Cards (Team)",
		"C"=>1
	],
	"9308"=>[
		"N"=>"Most Yellow Cards In A Single Match (Team)",
		"C"=>1
	],
	"9309"=>[
		"N"=>"Fastest Red Card",
		"C"=>1
	],
	"9310"=>[
		"N"=>"More Aces Than Double Faults",
		"C"=>1
	],
	"9311"=>[
		"N"=>"Points By Starting Five In Match",
		"C"=>1
	],
	"9312"=>[
		"N"=>"Points By Bench Players In Match",
		"C"=>1
	],
	"9313"=>[
		"N"=>"Team 1 To Score Specified Goal + Outcome",
		"C"=>1
	],
	"9314"=>[
		"N"=>"Team 2 To Score Specified Goal + Outcome",
		"C"=>1
	],
	"9315"=>[
		"N"=>"Team 1 To Score Specified Goal + Double Chance",
		"C"=>1
	],
	"9316"=>[
		"N"=>"Team 2 To Score Specified Goal + Double Chance",
		"C"=>1
	],
	"9317"=>[
		"N"=>"Total VAR Decisions",
		"C"=>1
	],
	"9318"=>[
		"N"=>"Player To Lose First Serve",
		"C"=>1
	],
	"9319"=>[
		"N"=>"Team Not To Score In Any Match",
		"C"=>1
	],
	"9320"=>[
		"N"=>"Matches In Which Team Will Score",
		"C"=>1
	],
	"9321"=>[
		"N"=>"Goal Stats",
		"C"=>1
	],
	"9322"=>[
		"N"=>"Both Teams To Score + Double Chance",
		"C"=>1
	],
	"9323"=>[
		"N"=>"Total Wins To Nil",
		"C"=>1
	],
	"9324"=>[
		"N"=>"Maximum Official Added Time Awarded In 1st Half",
		"C"=>1
	],
	"9325"=>[
		"N"=>"Maximum Official Added Time Awarded In 2nd Half",
		"C"=>1
	],
	"9326"=>[
		"N"=>"Total Maximum Official Added Time Awarded In Both Halves",
		"C"=>1
	],
	"9327"=>[
		"N"=>"Total Penalties Awarded In 1st Half",
		"C"=>1
	],
	"9328"=>[
		"N"=>"Total Penalties Awarded In 2nd Half",
		"C"=>1
	],
	"9329"=>[
		"N"=>"Total Substitutions In 1st Half",
		"C"=>1
	],
	"9330"=>[
		"N"=>"Total Substitutions In 2nd Half",
		"C"=>1
	],
	"9331"=>[
		"N"=>"Total Substitutions At Half-Time",
		"C"=>1
	],
	"9332"=>[
		"N"=>"Total Shots On Target In 1st Half",
		"C"=>1
	],
	"9333"=>[
		"N"=>"Total Shots On Target In 2nd Half",
		"C"=>1
	],
	"9334"=>[
		"N"=>"Total Red Cards In 1st Half",
		"C"=>1
	],
	"9335"=>[
		"N"=>"Total Red Cards In 2nd Half",
		"C"=>1
	],
	"9336"=>[
		"N"=>"Team Not To Win Any Match",
		"C"=>1
	],
	"9337"=>[
		"N"=>"Team To Win All Matches",
		"C"=>1
	],
	"9338"=>[
		"N"=>"Team To Lose All Matches",
		"C"=>1
	],
	"9339"=>[
		"N"=>"Team To Draw All Matches",
		"C"=>1
	],
	"9340"=>[
		"N"=>"Total Offsides, 1st Half",
		"C"=>1
	],
	"9341"=>[
		"N"=>"Total Offsides, 2nd Half",
		"C"=>1
	],
	"9342"=>[
		"N"=>"Juventus, Top Club Goalscorer In The Tournament",
		"C"=>1
	],
	"9343"=>[
		"N"=>"Liverpool, Top Club Goalscorer In The Tournament",
		"C"=>1
	],
	"9344"=>[
		"N"=>"Hidden Bagel In Set",
		"C"=>1
	],
	"9345"=>[
		"N"=>"1st and 4th Places In Group",
		"C"=>1
	],
	"9346"=>[
		"N"=>"Both Teams, Total In Time Interval",
		"C"=>1
	],
	"9347"=>[
		"N"=>"When Will First Goal Be Scored",
		"C"=>1
	],
	"9348"=>[
		"N"=>"Total Goal Minutes",
		"C"=>1
	],
	"9349"=>[
		"N"=>"Total Clean Sheets",
		"C"=>1
	],
	"9350"=>[
		"N"=>"Team, Exact Number Of Goals To Be Scored",
		"C"=>1
	],
	"9351"=>[
		"N"=>"Team, Exact Number Of Goals To Be Conceded",
		"C"=>1
	],
	"9352"=>[
		"N"=>"Last Shot On Target",
		"C"=>1
	],
	"9353"=>[
		"N"=>"Last Goal Kick",
		"C"=>1
	],
	"9354"=>[
		"N"=>"Tournament Final",
		"C"=>1
	],
	"9355"=>[
		"N"=>"How Many Times A Player Will Score In A Single Match",
		"C"=>1
	],
	"9356"=>[
		"N"=>"Highest Scoring Home Team Total",
		"C"=>1
	],
	"9357"=>[
		"N"=>"Highest Scoring Away Team Total",
		"C"=>1
	],
	"9358"=>[
		"N"=>"Player 2, Orbs To Be Taken In Round",
		"C"=>1
	],
	"9360"=>[
		"N"=>"First Goal Kick",
		"C"=>1
	],
	"9361"=>[
		"N"=>"First Substitution",
		"C"=>1
	],
	"9362"=>[
		"N"=>"Player's Goal + Yellow Card",
		"C"=>1
	],
	"9363"=>[
		"N"=>"Total Orbs To Be Taken In Round",
		"C"=>1
	],
	"9364"=>[
		"N"=>"Player 1, Orbs To Be Taken In Round",
		"C"=>1
	],
	"9365"=>[
		"N"=>"A Goal To Be Disallowed/Not Confirmed By VAR",
		"C"=>1
	],
	"9366"=>[
		"N"=>"A Penalty To Be Awarded/Confirmed By VAR",
		"C"=>1
	],
	"9367"=>[
		"N"=>"A Penalty Not To Be Awarded/Not To Be Confirmed By VAR",
		"C"=>1
	],
	"9368"=>[
		"N"=>"Player To Score And Get Red Card",
		"C"=>1
	],
	"9369"=>[
		"N"=>"Player To Score His First Goal With A Header",
		"C"=>1
	],
	"9370"=>[
		"N"=>"Player To Score His First Goal From Outside The Penalty Area",
		"C"=>1
	],
	"9371"=>[
		"N"=>"Player To Score His First Goal With Left Foot",
		"C"=>1
	],
	"9372"=>[
		"N"=>"Player To Score His First Goal With Right Foot",
		"C"=>1
	],
	"9373"=>[
		"N"=>"Player To Score His First Goal From A Penalty",
		"C"=>1
	],
	"9374"=>[
		"N"=>"Player To Score His First Goal From A Direct Free Kick",
		"C"=>1
	],
	"9375"=>[
		"N"=>"Player To Get Yellow Card For Simulation",
		"C"=>1
	],
	"9376"=>[
		"N"=>"Player To Be Substituted In 1st Half",
		"C"=>1
	],
	"9377"=>[
		"N"=>"Player To Be Substituted At Half-Time",
		"C"=>1
	],
	"9378"=>[
		"N"=>"Player To Be Substituted In 2nd Half",
		"C"=>1
	],
	"9379"=>[
		"N"=>"First Card Time",
		"C"=>1
	],
	"9380"=>[
		"N"=>"Team 1, Intermediate Total",
		"C"=>1
	],
	"9381"=>[
		"N"=>"Team 2, Intermediate Total",
		"C"=>1
	],
	"9382"=>[
		"N"=>"Throw-ins, Race",
		"C"=>1
	],
	"9383"=>[
		"N"=>"Offsides, Race",
		"C"=>1
	],
	"9384"=>[
		"N"=>"Goal Kicks, Race",
		"C"=>1
	],
	"9385"=>[
		"N"=>"Leader After Throw-ins",
		"C"=>1
	],
	"9386"=>[
		"N"=>"Leader After Offsides",
		"C"=>1
	],
	"9387"=>[
		"N"=>"Leader After Goal Kicks",
		"C"=>1
	],
	"9388"=>[
		"N"=>"Next Goal Kick (3Way)",
		"C"=>1
	],
	"9389"=>[
		"N"=>"Lowest Number Of Corners In Half",
		"C"=>1
	],
	"9390"=>[
		"N"=>"Lowest Number Of Corners Taken By One Team",
		"C"=>1
	],
	"9391"=>[
		"N"=>"Highest Number Of Corners Taken By One Team In One Half",
		"C"=>1
	],
	"9392"=>[
		"N"=>"Maximum Corner Difference In A Single Match",
		"C"=>1
	],
	"9393"=>[
		"N"=>"Highest Number Of Corners Taken By One Team",
		"C"=>1
	],
	"9394"=>[
		"N"=>"Leader After Corners",
		"C"=>1
	],
	"9395"=>[
		"N"=>"Interval Outcome (30m)",
		"C"=>1
	],
	"9396"=>[
		"N"=>"Double Chance (30m)",
		"C"=>1
	],
	"9397"=>[
		"N"=>"Total In Interval (30m)",
		"C"=>1
	],
	"9398"=>[
		"N"=>"Next Offside (3Way)",
		"C"=>1
	],
	"9399"=>[
		"N"=>"Corner Up To Minute",
		"C"=>1
	],
	"9400"=>[
		"N"=>"Yellow Card Up To Minute",
		"C"=>1
	],
	"9401"=>[
		"N"=>"Offside Up To Minute",
		"C"=>1
	],
	"9402"=>[
		"N"=>"Goal Kick Up To Minute",
		"C"=>1
	],
	"9403"=>[
		"N"=>"Medical Team, Stats",
		"C"=>1
	],
	"9404"=>[
		"N"=>"To Score In Both Halves",
		"C"=>1
	],
	"9405"=>[
		"N"=>"Last To Happen",
		"C"=>1
	],
	"9406"=>[
		"N"=>"Score After Two Goals",
		"C"=>1
	],
	"9407"=>[
		"N"=>"Score After Three Goals",
		"C"=>1
	],
	"9408"=>[
		"N"=>"Score After Four Goals",
		"C"=>1
	],
	"9409"=>[
		"N"=>"Temperature At Airport",
		"C"=>1
	],
	"9410"=>[
		"N"=>"Atmospheric Pressure At Airport",
		"C"=>1
	],
	"9411"=>[
		"N"=>"Wind Speed At Airport",
		"C"=>1
	],
	"9412"=>[
		"N"=>"Run Of Play And Match Result",
		"C"=>1
	],
	"9413"=>[
		"N"=>"Maximum Number Of Goals In Half",
		"C"=>1
	],
	"9414"=>[
		"N"=>"Matches With A Specific Number Of Goals",
		"C"=>1
	],
	"9415"=>[
		"N"=>"At Least One Match To End With The Specified Score",
		"C"=>1
	],
	"9416"=>[
		"N"=>"Total Points By Teams",
		"C"=>1
	],
	"9417"=>[
		"N"=>"Teams' Results",
		"C"=>1
	],
	"9418"=>[
		"N"=>"Match Result And Team Stats",
		"C"=>1
	],
	"9419"=>[
		"N"=>"Tackles",
		"C"=>1
	],
	"9420"=>[
		"N"=>"Total Sendings Off",
		"C"=>1
	],
	"9421"=>[
		"N"=>"Player, Post+Crossbar Hits",
		"C"=>1
	],
	"9422"=>[
		"N"=>"Total Penalties",
		"C"=>1
	],
	"9423"=>[
		"N"=>"Ball Will Enter The Net But A Goal Will Not Be Awarded",
		"C"=>1
	],
	"9424"=>[
		"N"=>"Hits, Handicap",
		"C"=>1
	],
	"9425"=>[
		"N"=>"Playoff Opponents",
		"C"=>1
	],
	"9427"=>[
		"N"=>"Match Result And Total Yellow Cards",
		"C"=>1
	],
	"9428"=>[
		"N"=>"Double Chance + Team 1 Total",
		"C"=>1
	],
	"9429"=>[
		"N"=>"Double Chance + Team 2 Total",
		"C"=>1
	],
	"9430"=>[
		"N"=>"Draw + Team 1 Total",
		"C"=>1
	],
	"9431"=>[
		"N"=>"Draw + Team 2 Total",
		"C"=>1
	],
	"9432"=>[
		"N"=>"W1 + Team 2 Total",
		"C"=>1
	],
	"9433"=>[
		"N"=>"W2 + Team 1 Total",
		"C"=>1
	],
	"9434"=>[
		"N"=>"Team With Top Bowler In Match",
		"C"=>1
	],
	"9435"=>[
		"N"=>"Who Will Score More Runs And Win",
		"C"=>1
	],
	"9436"=>[
		"N"=>"Total Runs In The Highest Scoring Over In Match",
		"C"=>1
	],
	"9437"=>[
		"N"=>"How Many Times Will A Player Score A Specific Number Of Goals In A Single Match",
		"C"=>1
	],
	"9438"=>[
		"N"=>"Penalty Awarded And Sending Off",
		"C"=>1
	],
	"9439"=>[
		"N"=>"Top Goalscorer",
		"C"=>1
	],
	"9440"=>[
		"N"=>"Goal In Numerical Advantage",
		"C"=>1
	],
	"9441"=>[
		"N"=>"Time Periods With No Goals",
		"C"=>1
	],
	"9442"=>[
		"N"=>"Time Periods With No Goals (By Team)",
		"C"=>1
	],
	"9443"=>[
		"N"=>"Most Goals, Head-To-Head",
		"C"=>1
	],
	"9444"=>[
		"N"=>"Most Goals, Head-To-Head (Including Extra Time, Without Penalty Shootout)",
		"C"=>1
	],
	"9445"=>[
		"N"=>"1st Wicket - Method Of Fall",
		"C"=>1
	],
	"9446"=>[
		"N"=>"Batsman's Total Runs",
		"C"=>1
	],
	"9447"=>[
		"N"=>"First Win During Regular Games",
		"C"=>1
	],
	"9448"=>[
		"N"=>"First Win During Regular Games - Which Game",
		"C"=>1
	],
	"9449"=>[
		"N"=>"Who Will Take The First Win During Regular Games?",
		"C"=>1
	],
	"9450"=>[
		"N"=>"Player Who Takes The First Win During Regular Games Will Become World Champion",
		"C"=>1
	],
	"9451"=>[
		"N"=>"Direction Of First Boundary",
		"C"=>1
	],
	"9452"=>[
		"N"=>"Next Post Or Crossbar",
		"C"=>1
	],
	"9453"=>[
		"N"=>"Goal Kick By An Outfield Player",
		"C"=>1
	],
	"9454"=>[
		"N"=>"Last Throw-in",
		"C"=>1
	],
	"9455"=>[
		"N"=>"First Shot Towards The Goal",
		"C"=>1
	],
	"9456"=>[
		"N"=>"First Throw-in",
		"C"=>1
	],
	"9457"=>[
		"N"=>"Number Of Finishers",
		"C"=>1
	],
	"9458"=>[
		"N"=>"Number Of Riders To Wear Yellow Jersey",
		"C"=>1
	],
	"9459"=>[
		"N"=>"King Of The Mountains. Match-Ups",
		"C"=>1
	],
	"9460"=>[
		"N"=>"Points Classification. Match-Ups",
		"C"=>1
	],
	"9461"=>[
		"N"=>"Team Classification. Match-Ups",
		"C"=>1
	],
	"9462"=>[
		"N"=>"Young Rider Classification. Match-Ups",
		"C"=>1
	],
	"9463"=>[
		"N"=>"Total Frames.",
		"C"=>1
	],
	"9465"=>[
		"N"=>"Total Stages Won",
		"C"=>1
	],
	"9466"=>[
		"N"=>"Number Of Goals Allowed By VAR In Playoff Matches, Including Extra Time",
		"C"=>1
	],
	"9467"=>[
		"N"=>"All Home Teams To Score",
		"C"=>1
	],
	"9468"=>[
		"N"=>"Highest Number Of VAR Reviews In A Match",
		"C"=>1
	],
	"9469"=>[
		"N"=>"Most Tackles",
		"C"=>1
	],
	"9470"=>[
		"N"=>"Most Metres Gained",
		"C"=>1
	],
	"9471"=>[
		"N"=>"Completion Rate",
		"C"=>1
	],
	"9472"=>[
		"N"=>"Team To Score In 1st Half And Win",
		"C"=>1
	],
	"9473"=>[
		"N"=>"Team To Score In 2nd Half And Win",
		"C"=>1
	],
	"9474"=>[
		"N"=>"Team To Score In 2nd Half And Take The Lead",
		"C"=>1
	],
	"9475"=>[
		"N"=>"Substitute Player To Score Winning Goal",
		"C"=>1
	],
	"9476"=>[
		"N"=>"Substitute Player To Get Yellow Card",
		"C"=>1
	],
	"9477"=>[
		"N"=>"Substitute Player To Get Red Card",
		"C"=>1
	],
	"9478"=>[
		"N"=>"Player To Get Yellow Card For Removing His Top",
		"C"=>1
	],
	"9479"=>[
		"N"=>"What Will The Elemental Dragon Be?",
		"C"=>1
	],
	"9480"=>[
		"N"=>"Team To Slay Rift Herald",
		"C"=>1
	],
	"9481"=>[
		"N"=>"3rd Place In The Tournament",
		"C"=>1
	],
	"9482"=>[
		"N"=>"Who Will Win Stage",
		"C"=>1
	],
	"9483"=>[
		"N"=>"Card Color On Last Flop",
		"C"=>1
	],
	"9484"=>[
		"N"=>"Who Will Break Serve",
		"C"=>1
	],
	"9485"=>[
		"N"=>"Server To Win 40-0 Or 40-15",
		"C"=>1
	],
	"9486"=>[
		"N"=>"Server To Win 40-0, 40-15 Or 40-30",
		"C"=>1
	],
	"9487"=>[
		"N"=>"Game Score After 3 Points",
		"C"=>1
	],
	"9488"=>[
		"N"=>"Correct Score In Set",
		"C"=>1
	],
	"9489"=>[
		"N"=>"Results In Sets",
		"C"=>1
	],
	"9490"=>[
		"N"=>"Substitute Player",
		"C"=>1
	],
	"9491"=>[
		"N"=>"Correct Score With Any Player Winning",
		"C"=>1
	],
	"9492"=>[
		"N"=>"Winning Checkout In The Tournament",
		"C"=>1
	],
	"9493"=>[
		"N"=>"Player With The Fewest 180s",
		"C"=>1
	],
	"9494"=>[
		"N"=>"Player With The Lowest Checkout",
		"C"=>1
	],
	"9495"=>[
		"N"=>"Number Of Darts To Win Leg",
		"C"=>1
	],
	"9496"=>[
		"N"=>"Player's Checkout Percentage",
		"C"=>1
	],
	"9497"=>[
		"N"=>"Player - Total Interceptions",
		"C"=>1
	],
	"9498"=>[
		"N"=>"Leader Of General Classification After Stage",
		"C"=>1
	],
	"9499"=>[
		"N"=>"First 100+ Checkout",
		"C"=>1
	],
	"9500"=>[
		"N"=>"First Three Places (In Any Order)",
		"C"=>1
	],
	"9501"=>[
		"N"=>"First Four Places (In Any Order)",
		"C"=>1
	],
	"9502"=>[
		"N"=>"Player To Get White Jersey",
		"C"=>1
	],
	"9503"=>[
		"N"=>"Last Throw",
		"C"=>1
	],
	"9504"=>[
		"N"=>"Total Wins By First Players",
		"C"=>1
	],
	"9505"=>[
		"N"=>"Total Wins By Second Players",
		"C"=>1
	],
	"9506"=>[
		"N"=>"Total Correct Score",
		"C"=>1
	],
	"9507"=>[
		"N"=>"Which Club Will Be Relegated?",
		"C"=>1
	],
	"9508"=>[
		"N"=>"Which Club Will Avoid Relegation?",
		"C"=>1
	],
	"9509"=>[
		"N"=>"Top Finland Goalscorer (Points)",
		"C"=>1
	],
	"9510"=>[
		"N"=>"Top Sweden Goalscorer (Points)",
		"C"=>1
	],
	"9511"=>[
		"N"=>"Top Finland Goalscorer (Goals)",
		"C"=>1
	],
	"9512"=>[
		"N"=>"Top Sweden Goalscorer (Goals)",
		"C"=>1
	],
	"9513"=>[
		"N"=>"Total In Round",
		"C"=>1
	],
	"9514"=>[
		"N"=>"Individual Total In Round",
		"C"=>1
	],
	"9515"=>[
		"N"=>"Total Even In Round",
		"C"=>1
	],
	"9516"=>[
		"N"=>"Handicap In Round",
		"C"=>1
	],
	"9517"=>[
		"N"=>"Winner Including Handicap",
		"C"=>1
	],
	"9518"=>[
		"N"=>"Best Players",
		"C"=>1
	],
	"9519"=>[
		"N"=>"Team Of The Year Goalkeeper",
		"C"=>1
	],
	"9520"=>[
		"N"=>"Team Of The Year Defender",
		"C"=>1
	],
	"9521"=>[
		"N"=>"Team Of The Year Midfielder",
		"C"=>1
	],
	"9522"=>[
		"N"=>"Team Of The Year Forward",
		"C"=>1
	],
	"9523"=>[
		"N"=>"To Be Selected For The Team Of The Year",
		"C"=>1
	],
	"9524"=>[
		"N"=>"Total Woodwork Hits",
		"C"=>1
	],
	"9525"=>[
		"N"=>"Most Kills",
		"C"=>1
	],
	"9526"=>[
		"N"=>"Highest Networth",
		"C"=>1
	],
	"9527"=>[
		"N"=>"Most Last Hits",
		"C"=>1
	],
	"9528"=>[
		"N"=>"Most Denies",
		"C"=>1
	],
	"9529"=>[
		"N"=>"Most Bounty Runes",
		"C"=>1
	],
	"9530"=>[
		"N"=>"Pick More (Strength) Hero",
		"C"=>1
	],
	"9531"=>[
		"N"=>"Pick More (Intelligence) Hero",
		"C"=>1
	],
	"9532"=>[
		"N"=>"Pick More (Agility) Hero",
		"C"=>1
	],
	"9533"=>[
		"N"=>"First To Use Glyph",
		"C"=>1
	],
	"9534"=>[
		"N"=>"First To Use Buyback",
		"C"=>1
	],
	"9535"=>[
		"N"=>"Result And Winning Hand",
		"C"=>1
	],
	"9536"=>[
		"N"=>"Leader After Races",
		"C"=>1
	],
	"9537"=>[
		"N"=>"Race To Points",
		"C"=>1
	],
	"9538"=>[
		"N"=>"Home Team Points",
		"C"=>1
	],
	"9539"=>[
		"N"=>"Away Team Points",
		"C"=>1
	],
	"9540"=>[
		"N"=>"Who Will Get Most Cards?",
		"C"=>1
	],
	"9541"=>[
		"N"=>"Outcome + Duration Of The Map",
		"C"=>1
	],
	"9542"=>[
		"N"=>"Total Straight Red Cards",
		"C"=>1
	],
	"9543"=>[
		"N"=>"King Of The Mountains. Rider In Top 3",
		"C"=>1
	],
	"9544"=>[
		"N"=>"Points Classification. Rider In Top 3",
		"C"=>1
	],
	"9545"=>[
		"N"=>"Teams, Match-Ups, Points, Handicaps",
		"C"=>1
	],
	"9546"=>[
		"N"=>"Total Wins In Away Matches",
		"C"=>1
	],
	"9547"=>[
		"N"=>"Total Buybacks",
		"C"=>1
	],
	"9548"=>[
		"N"=>"Leads By Net Worth",
		"C"=>1
	],
	"9549"=>[
		"N"=>"To Be Purchased",
		"C"=>1
	],
	"9550"=>[
		"N"=>"Team To Lose Map 1 And Win The Match",
		"C"=>1
	],
	"9551"=>[
		"N"=>"Team To Win Map 1 And Win The Match",
		"C"=>1
	],
	"9552"=>[
		"N"=>"Total Kills",
		"C"=>1
	],
	"9553"=>[
		"N"=>"To Be Higher By Headshots",
		"C"=>1
	],
	"9554"=>[
		"N"=>"Highest ADR",
		"C"=>1
	],
	"9555"=>[
		"N"=>"Team 1 To Be Losing By Specified Number Of Goals During The Match And To Win The Match",
		"C"=>1
	],
	"9556"=>[
		"N"=>"Team 2 To Be Losing By Specified Number Of Goals During The Match And To Win The Match",
		"C"=>1
	],
	"9557"=>[
		"N"=>"Winner's League",
		"C"=>1
	],
	"9558"=>[
		"N"=>"All Country Final",
		"C"=>1
	],
	"9559"=>[
		"N"=>"Highest Headshot %",
		"C"=>1
	],
	"9560"=>[
		"N"=>"Fewest Deaths",
		"C"=>1
	],
	"9561"=>[
		"N"=>"First Half / Full-time (5Way)",
		"C"=>1
	],
	"9562"=>[
		"N"=>"Player, Total Frags",
		"C"=>1
	],
	"9563"=>[
		"N"=>"Who Will Score A Certain Goal Number",
		"C"=>1
	],
	"9564"=>[
		"N"=>"Conference Of The Winning Team",
		"C"=>1
	],
	"9565"=>[
		"N"=>"Conference, Division Of The Winning Team",
		"C"=>1
	],
	"9566"=>[
		"N"=>"Lead After Day 1 - Lead After Day 2, Winner",
		"C"=>1
	],
	"9567"=>[
		"N"=>"Top Team Scorer",
		"C"=>1
	],
	"9568"=>[
		"N"=>"Total Maps Played",
		"C"=>1
	],
	"9569"=>[
		"N"=>"Top Russian Goalscorer",
		"C"=>1
	],
	"9570"=>[
		"N"=>"Top Team Scorer",
		"C"=>1
	],
	"9571"=>[
		"N"=>"Game To Have Break Point",
		"C"=>1
	],
	"9572"=>[
		"N"=>"Total Substitution Minutes",
		"C"=>1
	],
	"9573"=>[
		"N"=>"Double Substitution",
		"C"=>1
	],
	"9574"=>[
		"N"=>"Total Substitutions In Interval",
		"C"=>1
	],
	"9575"=>[
		"N"=>"Both Teams To Score TD In Each Half",
		"C"=>1
	],
	"9576"=>[
		"N"=>"Total Touchdowns Of Each Team In Each Half",
		"C"=>1
	],
	"9577"=>[
		"N"=>"First Scoring Play",
		"C"=>1
	],
	"9578"=>[
		"N"=>"Last Scoring Play",
		"C"=>1
	],
	"9579"=>[
		"N"=>"Team 1, First Scoring Play",
		"C"=>1
	],
	"9580"=>[
		"N"=>"Team 2, First Scoring Play",
		"C"=>1
	],
	"9581"=>[
		"N"=>"Team 1, Last Scoring Play",
		"C"=>1
	],
	"9582"=>[
		"N"=>"Team 2, Last Scoring Play",
		"C"=>1
	],
	"9583"=>[
		"N"=>"Points Deduction And Who To Win",
		"C"=>1
	],
	"9584"=>[
		"N"=>"How Much Times Both To Be Knocked Down",
		"C"=>1
	],
	"9585"=>[
		"N"=>"Knocked Down In Rounds Interval",
		"C"=>1
	],
	"9586"=>[
		"N"=>"Fighter To Be Knockdown In Rounds Interval",
		"C"=>1
	],
	"9587"=>[
		"N"=>"Knockdown And Win In Rounds Interval",
		"C"=>1
	],
	"9588"=>[
		"N"=>"How Much Times Fighter To Be Knockdown",
		"C"=>1
	],
	"9589"=>[
		"N"=>"Knockdown In A Certain Round",
		"C"=>1
	],
	"9590"=>[
		"N"=>"How Much Times Fighter To Be Knockdown Per Round",
		"C"=>1
	],
	"9591"=>[
		"N"=>"Both To Be Knockdown And Winner",
		"C"=>1
	],
	"9592"=>[
		"N"=>"Who To Be Knockdown And Who To Win By KO/TKO",
		"C"=>1
	],
	"9593"=>[
		"N"=>"Who To Be Knockdown And Win By Points",
		"C"=>1
	],
	"9594"=>[
		"N"=>"Both To Be Knockdown And Win By KO/TKO",
		"C"=>1
	],
	"9595"=>[
		"N"=>"Who To Be Knockdown And Win By UD",
		"C"=>1
	],
	"9596"=>[
		"N"=>"Who To Be Knockdown In Certain Rounds",
		"C"=>1
	],
	"9597"=>[
		"N"=>"Knockdown In Rounds Interval And Win In A Certain Round",
		"C"=>1
	],
	"9598"=>[
		"N"=>"How Much Times Fighter To Be Knocked Down And Who To Win",
		"C"=>1
	],
	"9599"=>[
		"N"=>"Knockdown In Each Of Three First Rounds",
		"C"=>1
	],
	"9600"=>[
		"N"=>"Both To Be Knocked Down And Who To Win In Rounds Interval",
		"C"=>1
	],
	"9601"=>[
		"N"=>"Both To Be Knocked Down And Win By Unanimous Decision",
		"C"=>1
	],
	"9602"=>[
		"N"=>"Highest Number Of Defeats",
		"C"=>1
	],
	"9604"=>[
		"N"=>"Hole In One",
		"C"=>1
	],
	"9605"=>[
		"N"=>"Top Canada Goalscorer (Points)",
		"C"=>1
	],
	"9606"=>[
		"N"=>"Top USA Goalscorer (Points)",
		"C"=>1
	],
	"9607"=>[
		"N"=>"Top Canada Goalscorer (Goals)",
		"C"=>1
	],
	"9608"=>[
		"N"=>"Top USA Goalscorer (Goals)",
		"C"=>1
	],
	"9609"=>[
		"N"=>"Additional Totals (2Way)",
		"C"=>1
	],
	"9610"=>[
		"N"=>"When Will The First Safety Car Appear",
		"C"=>1
	],
	"9611"=>[
		"N"=>"When Will The Last Safety Car Appear",
		"C"=>1
	],
	"9612"=>[
		"N"=>"Multi Outcomes - Teams",
		"C"=>1
	],
	"9613"=>[
		"N"=>"How Will Round End",
		"C"=>1
	],
	"9614"=>[
		"N"=>"First Drive Through The Pit Lane",
		"C"=>1
	],
	"9615"=>[
		"N"=>"Field Goal Shooting Percentage (Without Free Throws)",
		"C"=>1
	],
	"9616"=>[
		"N"=>"Court Order",
		"C"=>1
	],
	"9617"=>[
		"N"=>"Player, Total Blocks",
		"C"=>1
	],
	"9618"=>[
		"N"=>"Player, Total Rebounds And Blocks",
		"C"=>1
	],
	"9619"=>[
		"N"=>"Player, Total Turnovers",
		"C"=>1
	],
	"9620"=>[
		"N"=>"Player, Most Points In A Game",
		"C"=>1
	],
	"9621"=>[
		"N"=>"Player, Most Rebounds In A Game",
		"C"=>1
	],
	"9622"=>[
		"N"=>"Player, Most Steals In A Game",
		"C"=>1
	],
	"9623"=>[
		"N"=>"Player, Most Three Point Shots Made In A Game",
		"C"=>1
	],
	"9624"=>[
		"N"=>"To Be Higher By Bases Taken",
		"C"=>1
	],
	"9625"=>[
		"N"=>"Player, Total Wins",
		"C"=>1
	],
	"9626"=>[
		"N"=>"Player Wins Without Dropping A Set",
		"C"=>1
	],
	"9628"=>[
		"N"=>"Winner's Total Cards",
		"C"=>1
	],
	"9629"=>[
		"N"=>"Winner's Total Points",
		"C"=>1
	],
	"9630"=>[
		"N"=>"Bust",
		"C"=>1
	],
	"9631"=>[
		"N"=>"Who Will Make Longest Drive On Hole",
		"C"=>1
	],
	"9632"=>[
		"N"=>"Who Will Be The First Player To Lead After A Certain Number Of Holes",
		"C"=>1
	],
	"9633"=>[
		"N"=>"Who Will Make An Eagle In The Match",
		"C"=>1
	],
	"9634"=>[
		"N"=>"Nearest The Pin On Hole",
		"C"=>1
	],
	"9635"=>[
		"N"=>"Who Will Be The First Player To Go After A Certain Number Of Holes",
		"C"=>1
	],
	"9636"=>[
		"N"=>"Top Batsman Of The Match",
		"C"=>1
	],
	"9637"=>[
		"N"=>"Sector Number The Arrow Will Point To",
		"C"=>1
	],
	"9638"=>[
		"N"=>"Sector Number The Arrow Will Point To.",
		"C"=>1
	],
	"9639"=>[
		"N"=>"Sector Total The Arrow Will Point To",
		"C"=>1
	],
	"9640"=>[
		"N"=>"Even/Odd Number Of Sector The Arrow Will Point To",
		"C"=>1
	],
	"9641"=>[
		"N"=>"Sector Color The Arrow Will Point To",
		"C"=>1
	],
	"9642"=>[
		"N"=>"Even/Odd Number And Sector Color The Arrow Will Point To",
		"C"=>1
	],
	"9643"=>[
		"N"=>"The Arrow Will Point To The 1 Of 3 Indicated Sectors",
		"C"=>1
	],
	"9644"=>[
		"N"=>"The Arrow Will Point To The 1 Of 6 Indicated Sectors",
		"C"=>1
	],
	"9645"=>[
		"N"=>"The Arrow Will Point To The 1 Of 12 Indicated Sectors",
		"C"=>1
	],
	"9646"=>[
		"N"=>"The Arrow Will Point To The 1 Of 18 Indicated Sectors",
		"C"=>1
	],
	"9647"=>[
		"N"=>"The Arrow Will Point To The Sector From A Row:",
		"C"=>1
	],
	"9648"=>[
		"N"=>"Team Not To Lose Any Away Match",
		"C"=>1
	],
	"9649"=>[
		"N"=>"Player To Have The Highest 3-Dart Average",
		"C"=>1
	],
	"9650"=>[
		"N"=>"Player To Have The Lowest 3-Dart Average",
		"C"=>1
	],
	"9651"=>[
		"N"=>"Highest 3-Dart Average By Any Player",
		"C"=>1
	],
	"9652"=>[
		"N"=>"One Selected Number To Be Drawn",
		"C"=>1
	],
	"9653"=>[
		"N"=>"Two Selected Numbers To Be Drawn",
		"C"=>1
	],
	"9654"=>[
		"N"=>"Three Selected Numbers To Be Drawn",
		"C"=>1
	],
	"9655"=>[
		"N"=>"Even Numbers Over/Under",
		"C"=>1
	],
	"9656"=>[
		"N"=>"Sum Of Which Numbers Over",
		"C"=>1
	],
	"9657"=>[
		"N"=>"Sum Of Drawn Numbers",
		"C"=>1
	],
	"9658"=>[
		"N"=>"At Least One Number Of A Certain Color To Be Drawn",
		"C"=>1
	],
	"9659"=>[
		"N"=>"Total Drawn Numbers Of A Certain Color",
		"C"=>1
	],
	"9660"=>[
		"N"=>"How Many Numbers Of A Certain Color To Be Drawn",
		"C"=>1
	],
	"9661"=>[
		"N"=>"Color Of A Certain Drawn Number",
		"C"=>1
	],
	"9662"=>[
		"N"=>"Divisibility Of A One Of The Drawn Numbers",
		"C"=>1
	],
	"9663"=>[
		"N"=>"Total Even Numbers",
		"C"=>1
	],
	"9664"=>[
		"N"=>"Total Odds Numbers",
		"C"=>1
	],
	"9665"=>[
		"N"=>"Sum Of Drawn Numbers",
		"C"=>1
	],
	"9666"=>[
		"N"=>"The Difference Between The Highest And The Lowest Drawn Numbers",
		"C"=>1
	],
	"9667"=>[
		"N"=>"The Sum Between The Highest And The Lowest Drawn Numbers",
		"C"=>1
	],
	"9668"=>[
		"N"=>"The Sum Of Even Numbers",
		"C"=>1
	],
	"9669"=>[
		"N"=>"The Sum Of Odds Numbers",
		"C"=>1
	],
	"9670"=>[
		"N"=>"The Sum Between The Highest And The Lowest Drawn Numbers",
		"C"=>1
	],
	"9671"=>[
		"N"=>"The Difference Between The Highest And The Lowest Drawn Numbers",
		"C"=>1
	],
	"9672"=>[
		"N"=>"The Sum Of Drawn Numbers In Interval",
		"C"=>1
	],
	"9673"=>[
		"N"=>"First Drawn Number Of A Certain Color",
		"C"=>1
	],
	"9674"=>[
		"N"=>"All Drivers Of Three Teams In Top 6",
		"C"=>1
	],
	"9675"=>[
		"N"=>"Total Pieces After The End Of The Match Including Pawns",
		"C"=>1
	],
	"9676"=>[
		"N"=>"Only 2 Kings To Be Left At The End Of The Match",
		"C"=>1
	],
	"9677"=>[
		"N"=>"After How Many Moves Will The First Piece Be Taken",
		"C"=>1
	],
	"9678"=>[
		"N"=>"After How Many Moves Will The First Check Take Place",
		"C"=>1
	],
	"9679"=>[
		"N"=>"Which Player Will Take The First Piece",
		"C"=>1
	],
	"9680"=>[
		"N"=>"Last Moved Piece",
		"C"=>1
	],
	"9681"=>[
		"N"=>"Player's Total Assists",
		"C"=>1
	],
	"9682"=>[
		"N"=>"Numbers To Be Drawn More",
		"C"=>1
	],
	"9683"=>[
		"N"=>"The Sum Of Drawn Numbers - Even/Odd",
		"C"=>1
	],
	"9684"=>[
		"N"=>"First Drawn Number - Even/Odd",
		"C"=>1
	],
	"9685"=>[
		"N"=>"Last Drawn Number - Even/Odd",
		"C"=>1
	],
	"9686"=>[
		"N"=>"First And Second Drawn Numbers Will Be Of The Same Color",
		"C"=>1
	],
	"9687"=>[
		"N"=>"First And Second Drawn Numbers - Even/Odd",
		"C"=>1
	],
	"9688"=>[
		"N"=>"First And Last Drawn Numbers - Even/Odd",
		"C"=>1
	],
	"9689"=>[
		"N"=>"The Sum Of Blue Drawn Numbers",
		"C"=>1
	],
	"9690"=>[
		"N"=>"The Sum Of Black Drawn Numbers",
		"C"=>1
	],
	"9691"=>[
		"N"=>"Ball Of A Certain Color Will Not Be Drawn",
		"C"=>1
	],
	"9692"=>[
		"N"=>"First Drawn Number",
		"C"=>1
	],
	"9693"=>[
		"N"=>"Goal + Assist + Penalty",
		"C"=>1
	],
	"9694"=>[
		"N"=>"Total 170 Checkouts",
		"C"=>1
	],
	"9695"=>[
		"N"=>"Which Hand Will Win?",
		"C"=>1
	],
	"9696"=>[
		"N"=>"Winning Hemisphere",
		"C"=>1
	],
	"9697"=>[
		"N"=>"Card Value",
		"C"=>1
	],
	"9698"=>[
		"N"=>"Card Suit",
		"C"=>1
	],
	"9699"=>[
		"N"=>"Card Color",
		"C"=>1
	],
	"9700"=>[
		"N"=>"Number Card",
		"C"=>1
	],
	"9701"=>[
		"N"=>"Joker Card",
		"C"=>1
	],
	"9702"=>[
		"N"=>"To Be A Card With Digit Number",
		"C"=>1
	],
	"9703"=>[
		"N"=>"Jack Card",
		"C"=>1
	],
	"9704"=>[
		"N"=>"Queen Card",
		"C"=>1
	],
	"9705"=>[
		"N"=>"King Card",
		"C"=>1
	],
	"9706"=>[
		"N"=>"Ace Card",
		"C"=>1
	],
	"9707"=>[
		"N"=>"Stage Of Elimination - Group Stage",
		"C"=>1
	],
	"9708"=>[
		"N"=>"Stage Of Elimination - Main Round",
		"C"=>1
	],
	"9709"=>[
		"N"=>"Sum Total Of All 20 Drawn Numbers",
		"C"=>1
	],
	"9710"=>[
		"N"=>"Sum Total Of First 5 Drawn Numbers",
		"C"=>1
	],
	"9711"=>[
		"N"=>"Digit Number On The First Drawn Ball",
		"C"=>1
	],
	"9712"=>[
		"N"=>"Digit Number On The Last Drawn Ball",
		"C"=>1
	],
	"9713"=>[
		"N"=>"First Drawn Number To Contain One Digit",
		"C"=>1
	],
	"9714"=>[
		"N"=>"Last Drawn Number To Contain One Digit",
		"C"=>1
	],
	"9715"=>[
		"N"=>"Result",
		"C"=>1
	],
	"9716"=>[
		"N"=>"Dealer's Card Color",
		"C"=>1
	],
	"9717"=>[
		"N"=>"Player's Card Color",
		"C"=>1
	],
	"9718"=>[
		"N"=>"Dealer's Suit Color",
		"C"=>1
	],
	"9719"=>[
		"N"=>"Player's Suit Color",
		"C"=>1
	],
	"9720"=>[
		"N"=>"Dealer's Card Rank, Digit Number",
		"C"=>1
	],
	"9721"=>[
		"N"=>"Dealer's Card Rank",
		"C"=>1
	],
	"9722"=>[
		"N"=>"Dealer's Card Rank Over/Under",
		"C"=>1
	],
	"9723"=>[
		"N"=>"Player's Card Rank, Digit Number",
		"C"=>1
	],
	"9724"=>[
		"N"=>"Player's Card Rank",
		"C"=>1
	],
	"9725"=>[
		"N"=>"Player's Card Rank Over/Under",
		"C"=>1
	],
	"9726"=>[
		"N"=>"Will A Dealer To Have A Face Card",
		"C"=>1
	],
	"9727"=>[
		"N"=>"Will A Player To Have A Face Card",
		"C"=>1
	],
	"9728"=>[
		"N"=>"Dealer's Card Winning Suit",
		"C"=>1
	],
	"9729"=>[
		"N"=>"Player's Card Winning Suit",
		"C"=>1
	],
	"9730"=>[
		"N"=>"Digit Number On Player's Card",
		"C"=>1
	],
	"9731"=>[
		"N"=>"Player's Card - Jack",
		"C"=>1
	],
	"9732"=>[
		"N"=>"Player's Card - Queen",
		"C"=>1
	],
	"9733"=>[
		"N"=>"Player's Card - King",
		"C"=>1
	],
	"9734"=>[
		"N"=>"Player's Card - Ace",
		"C"=>1
	],
	"9735"=>[
		"N"=>"Digit Number On Dealer's Card",
		"C"=>1
	],
	"9736"=>[
		"N"=>"Dealer's Card - Jack",
		"C"=>1
	],
	"9737"=>[
		"N"=>"Dealer's Card - Queen",
		"C"=>1
	],
	"9738"=>[
		"N"=>"Dealer's Card - King",
		"C"=>1
	],
	"9739"=>[
		"N"=>"Dealer's Card - Ace",
		"C"=>1
	],
	"9740"=>[
		"N"=>"In Which Round Will Fighter Win The Bout",
		"C"=>1
	],
	"9741"=>[
		"N"=>"In Which Round Will Fighter To Be Knocked Down",
		"C"=>1
	],
	"9742"=>[
		"N"=>"Will Both Fighters To Be Knocked Down Simultaneously",
		"C"=>1
	],
	"9743"=>[
		"N"=>"Will Both Fighters To Be Knocked Down And Bout Duration",
		"C"=>1
	],
	"9744"=>[
		"N"=>"Method Of Victory",
		"C"=>1
	],
	"9745"=>[
		"N"=>"Game Handicap Points",
		"C"=>1
	],
	"9746"=>[
		"N"=>"Both Teams To Score A Try",
		"C"=>1
	],
	"9747"=>[
		"N"=>"Will There Be Try Converted",
		"C"=>1
	],
	"9748"=>[
		"N"=>"Last Number In Winner's Time",
		"C"=>1
	],
	"9749"=>[
		"N"=>"How Many Balls Will Be Potted With A Break Shot",
		"C"=>1
	],
	"9750"=>[
		"N"=>"Will There Be A Ball Potted WIth A Break Shot",
		"C"=>1
	],
	"9751"=>[
		"N"=>"Potted Ball With Break Shot - Even/Odd",
		"C"=>1
	],
	"9752"=>[
		"N"=>"Where There Be Foul After Break Shot",
		"C"=>1
	],
	"9753"=>[
		"N"=>"Will The Ball To Be Potted In Corner Pockets",
		"C"=>1
	],
	"9754"=>[
		"N"=>"The Sum Of Potted Balls After Break Shot",
		"C"=>1
	],
	"9755"=>[
		"N"=>"All Drawn Numbers Will Be Odd",
		"C"=>1
	],
	"9756"=>[
		"N"=>"All Drawn Numbers Will Be Even",
		"C"=>1
	],
	"9757"=>[
		"N"=>"Total Field Goals Scored",
		"C"=>1
	],
	"9758"=>[
		"N"=>"Total Team Field Goals Scored",
		"C"=>1
	],
	"9759"=>[
		"N"=>"Total Touchdowns",
		"C"=>1
	],
	"9760"=>[
		"N"=>"Time Of First Field Goal Scored",
		"C"=>1
	],
	"9761"=>[
		"N"=>"Team To Score In Both Halves",
		"C"=>1
	],
	"9762"=>[
		"N"=>"Team To Commit First Accepted Penalty",
		"C"=>1
	],
	"9763"=>[
		"N"=>"1st Quarter / Match Result",
		"C"=>1
	],
	"9764"=>[
		"N"=>"1st Quarter / Half Time",
		"C"=>1
	],
	"9765"=>[
		"N"=>"Time Of First Score",
		"C"=>1
	],
	"9766"=>[
		"N"=>"Total Team Field Goals Scored (3Way)",
		"C"=>1
	],
	"9767"=>[
		"N"=>"Total Touchdowns (3Way)",
		"C"=>1
	],
	"9768"=>[
		"N"=>"Successful 2 Point Conversion",
		"C"=>1
	],
	"9769"=>[
		"N"=>"First Offensive Play Of The Game",
		"C"=>1
	],
	"9770"=>[
		"N"=>"Total Pass Attempts",
		"C"=>1
	],
	"9771"=>[
		"N"=>"Total Pass Completions",
		"C"=>1
	],
	"9772"=>[
		"N"=>"Total Pass Yards",
		"C"=>1
	],
	"9773"=>[
		"N"=>"Team's First Touchdown",
		"C"=>1
	],
	"9774"=>[
		"N"=>"Team's Last Touchdown",
		"C"=>1
	],
	"9775"=>[
		"N"=>"Win By Number Of Cards",
		"C"=>1
	],
	"9776"=>[
		"N"=>"Win By Number Of Point",
		"C"=>1
	],
	"9777"=>[
		"N"=>"Will There Be 9 Dart Finish & 170 Checkout",
		"C"=>1
	],
	"9778"=>[
		"N"=>"Penalties Accepted",
		"C"=>1
	],
	"9779"=>[
		"N"=>"Player Longest Pass Received (Yards)",
		"C"=>1
	],
	"9780"=>[
		"N"=>"Special Team Or Defensive Touchdown Scored",
		"C"=>1
	],
	"9781"=>[
		"N"=>"Result.",
		"C"=>1
	],
	"9782"=>[
		"N"=>"Player Total Kicking Points",
		"C"=>1
	],
	"9783"=>[
		"N"=>"Who Will Take Part In The Fight",
		"C"=>1
	],
	"9784"=>[
		"N"=>"Maximum Series Of Attacks In Round - Total",
		"C"=>1
	],
	"9785"=>[
		"N"=>"Total Frames (3Way)",
		"C"=>1
	],
	"9786"=>[
		"N"=>"Batsman Total Runs Margin",
		"C"=>1
	],
	"9787"=>[
		"N"=>"Total Rush Attempts",
		"C"=>1
	],
	"9788"=>[
		"N"=>"Total Wickets Lost",
		"C"=>1
	],
	"9789"=>[
		"N"=>"1st Wicket - Method Of Fall - Caught",
		"C"=>1
	],
	"9790"=>[
		"N"=>"Team 1, Method Of First Dismissal In The First Innings",
		"C"=>1
	],
	"9791"=>[
		"N"=>"Team 2, Method Of First Dismissal In The First Innings",
		"C"=>1
	],
	"9792"=>[
		"N"=>"Macedonia, Top Goalscorer In The Tournament",
		"C"=>1
	],
	"9793"=>[
		"N"=>"Duration Of The Game (Sec)",
		"C"=>1
	],
	"9794"=>[
		"N"=>"Triple Kill",
		"C"=>1
	],
	"9795"=>[
		"N"=>"Quadra Kill",
		"C"=>1
	],
	"9796"=>[
		"N"=>"Penta Kill",
		"C"=>1
	],
	"9797"=>[
		"N"=>"Total Destroyed Equipment",
		"C"=>1
	],
	"9798"=>[
		"N"=>"Total Seized Initiatives",
		"C"=>1
	],
	"9799"=>[
		"N"=>"Highest Killing Spree",
		"C"=>1
	],
	"9800"=>[
		"N"=>"Time Of First Blood",
		"C"=>1
	],
	"9801"=>[
		"N"=>"Damage Handicap",
		"C"=>1
	],
	"9802"=>[
		"N"=>"Gold Handicap",
		"C"=>1
	],
	"9803"=>[
		"N"=>"Which Team Will Gain Most Passing Yards",
		"C"=>1
	],
	"9804"=>[
		"N"=>"Which Team Will Gain Most Rushing Yards",
		"C"=>1
	],
	"9805"=>[
		"N"=>"Will Map Be Completed",
		"C"=>1
	],
	"9806"=>[
		"N"=>"Mega Creeps To Be Spawned",
		"C"=>1
	],
	"9807"=>[
		"N"=>"Aegis Of The Immortal To Be Snatched",
		"C"=>1
	],
	"9808"=>[
		"N"=>"First Blood Before Creeps Spawn",
		"C"=>1
	],
	"9809"=>[
		"N"=>"Which Team Will Draw First Blood And Win The Map",
		"C"=>1
	],
	"9810"=>[
		"N"=>"Any Player To Get A Ultra Kill",
		"C"=>1
	],
	"9811"=>[
		"N"=>"Any Player To Get An Rampage",
		"C"=>1
	],
	"9812"=>[
		"N"=>"Total Coins",
		"C"=>1
	],
	"9813"=>[
		"N"=>"Total Super Sneakers",
		"C"=>1
	],
	"9814"=>[
		"N"=>"Total Jet Packs",
		"C"=>1
	],
	"9815"=>[
		"N"=>"Total Coin Magnets",
		"C"=>1
	],
	"9816"=>[
		"N"=>"Player With The Higher GPM",
		"C"=>1
	],
	"9817"=>[
		"N"=>"Team 1, Boundary In Over",
		"C"=>1
	],
	"9818"=>[
		"N"=>"Team 2, Boundary In Over",
		"C"=>1
	],
	"9819"=>[
		"N"=>"Completed Map, Map Duration",
		"C"=>1
	],
	"9820"=>[
		"N"=>"Completed Map, Total Rings",
		"C"=>1
	],
	"9821"=>[
		"N"=>"Number Of Checkpoints Passed",
		"C"=>1
	],
	"9822"=>[
		"N"=>"Checkpoint To Be Passed",
		"C"=>1
	],
	"9823"=>[
		"N"=>"Completed Maps",
		"C"=>1
	],
	"9824"=>[
		"N"=>"Which Team Will Receive Opening Kick-Off",
		"C"=>1
	],
	"9825"=>[
		"N"=>"Player, Longest Punt",
		"C"=>1
	],
	"9826"=>[
		"N"=>"Team 1 Total Rushing Yards",
		"C"=>1
	],
	"9827"=>[
		"N"=>"Team 2 Total Rushing Yards",
		"C"=>1
	],
	"9828"=>[
		"N"=>"Player, Total Passes Intercepted",
		"C"=>1
	],
	"9829"=>[
		"N"=>"Round Duration",
		"C"=>1
	],
	"9830"=>[
		"N"=>"Most Interceptions",
		"C"=>1
	],
	"9831"=>[
		"N"=>"Most Turnovers",
		"C"=>1
	],
	"9832"=>[
		"N"=>"Total Penalty Yards",
		"C"=>1
	],
	"9833"=>[
		"N"=>"First Pass Received (Yards)",
		"C"=>1
	],
	"9834"=>[
		"N"=>"Player With The Highest HLTV Rating",
		"C"=>1
	],
	"9835"=>[
		"N"=>"Most Kills In Single Map",
		"C"=>1
	],
	"9836"=>[
		"N"=>"Fewest Kills In Single Map",
		"C"=>1
	],
	"9837"=>[
		"N"=>"Most Aces In A Single Map",
		"C"=>1
	],
	"9838"=>[
		"N"=>"Top Team Runscorer Double",
		"C"=>1
	],
	"9839"=>[
		"N"=>"Total Distance Length Passed",
		"C"=>1
	],
	"9840"=>[
		"N"=>"Total Multipliers",
		"C"=>1
	],
	"9841"=>[
		"N"=>"Total Jet Packs",
		"C"=>1
	],
	"9842"=>[
		"N"=>"Winner And Player To Score Most Tries",
		"C"=>1
	],
	"9843"=>[
		"N"=>"Total Apples",
		"C"=>1
	],
	"9844"=>[
		"N"=>"Total Boxes",
		"C"=>1
	],
	"9845"=>[
		"N"=>"Total Lives",
		"C"=>1
	],
	"9846"=>[
		"N"=>"Drawn Ball From The Line",
		"C"=>1
	],
	"9847"=>[
		"N"=>"Drawn Ball From Interval",
		"C"=>1
	],
	"9848"=>[
		"N"=>"Drawn Ball",
		"C"=>1
	],
	"9849"=>[
		"N"=>"ZeusX27 Kill - Yes/No",
		"C"=>1
	],
	"9850"=>[
		"N"=>"1st Innings Batting Team, TO Runs - Even/Odd",
		"C"=>1
	],
	"9851"=>[
		"N"=>"Premiers / Least Wins Double",
		"C"=>1
	],
	"9852"=>[
		"N"=>"Minor Premiers / Least Wins Double",
		"C"=>1
	],
	"9853"=>[
		"N"=>"Top Point Scorer / Try Scorer Double",
		"C"=>1
	],
	"9854"=>[
		"N"=>"Race To Points (2Way)",
		"C"=>1
	],
	"9855"=>[
		"N"=>"First Tyrant",
		"C"=>1
	],
	"9856"=>[
		"N"=>"First Overlord",
		"C"=>1
	],
	"9857"=>[
		"N"=>"Players, Number Of Points",
		"C"=>1
	],
	"9858"=>[
		"N"=>"Players, Shots On Target",
		"C"=>1
	],
	"9859"=>[
		"N"=>"To Be Higher By Points (4Way)",
		"C"=>1
	],
	"9860"=>[
		"N"=>"To Be Higher By Goals (4Way)",
		"C"=>1
	],
	"9861"=>[
		"N"=>"To Be Higher By Goal Difference (4Way)",
		"C"=>1
	],
	"9862"=>[
		"N"=>"Lowest Number Of Goals Scored Match-Ups",
		"C"=>1
	],
	"9863"=>[
		"N"=>"Total Extra Time In Round",
		"C"=>1
	],
	"9864"=>[
		"N"=>"No Extra Time Total Rounds",
		"C"=>1
	],
	"9865"=>[
		"N"=>"Extra Time Round Leader",
		"C"=>1
	],
	"9866"=>[
		"N"=>"To Be Higher By Goal Difference (3Way)",
		"C"=>1
	],
	"9867"=>[
		"N"=>"Lowest Number Of Goals Scored Match-Ups (3Way)",
		"C"=>1
	],
	"9868"=>[
		"N"=>"VAR To Be Used By A Referee",
		"C"=>1
	],
	"9869"=>[
		"N"=>"Players, Plate Appearance",
		"C"=>1
	],
	"9870"=>[
		"N"=>"HE Grenade Kill",
		"C"=>1
	],
	"9871"=>[
		"N"=>"Total Highest Scoring Player",
		"C"=>1
	],
	"9872"=>[
		"N"=>"Total Lowest Scoring Player",
		"C"=>1
	],
	"9873"=>[
		"N"=>"Lowest Scoring Map Total Frags",
		"C"=>1
	],
	"9874"=>[
		"N"=>"Total Matches Duration...",
		"C"=>1
	],
	"9875"=>[
		"N"=>"Player With The Higher XPM",
		"C"=>1
	],
	"9876"=>[
		"N"=>"Total Frags, Pairs, Even/Odd",
		"C"=>1
	],
	"9877"=>[
		"N"=>"Total Frags, Pairs",
		"C"=>1
	],
	"9878"=>[
		"N"=>"Total Deaths, Pairs",
		"C"=>1
	],
	"9879"=>[
		"N"=>"To Make The Preliminary Final",
		"C"=>1
	],
	"9880"=>[
		"N"=>"Digit Number On The Drawn Ball",
		"C"=>1
	],
	"9881"=>[
		"N"=>"Who Last",
		"C"=>1
	],
	"9882"=>[
		"N"=>"What Place To Take",
		"C"=>1
	],
	"9883"=>[
		"N"=>"Who To Win A Point",
		"C"=>1
	],
	"9884"=>[
		"N"=>"Number Of Riders In Place Interval",
		"C"=>1
	],
	"9885"=>[
		"N"=>"Team, Total Corners In A Raw",
		"C"=>1
	],
	"9886"=>[
		"N"=>"Both Teams To Get Cards",
		"C"=>1
	],
	"9887"=>[
		"N"=>"Total Corners",
		"C"=>1
	],
	"9888"=>[
		"N"=>"Win To Nil In One Of The Sets",
		"C"=>1
	],
	"9889"=>[
		"N"=>"Total Orders Delivered",
		"C"=>1
	],
	"9890"=>[
		"N"=>"Total Tips",
		"C"=>1
	],
	"9891"=>[
		"N"=>"Maximum Number Of Predicted Events",
		"C"=>1
	],
	"9892"=>[
		"N"=>"First Rounds Outcome",
		"C"=>1
	],
	"9893"=>[
		"N"=>"Leader After Round",
		"C"=>1
	],
	"9894"=>[
		"N"=>"Goals From The Penalty Area",
		"C"=>1
	],
	"9895"=>[
		"N"=>"Goals From The Six-yard Box",
		"C"=>1
	],
	"9896"=>[
		"N"=>"1X2 Including OT",
		"C"=>1
	],
	"9897"=>[
		"N"=>"Both Teams To Score At Least In One Half",
		"C"=>1
	],
	"9898"=>[
		"N"=>"Win Or Correct Score",
		"C"=>1
	],
	"9899"=>[
		"N"=>"Too Many Men",
		"C"=>1
	],
	"9900"=>[
		"N"=>"Team 1 To Beat Roshans In Row",
		"C"=>1
	],
	"9901"=>[
		"N"=>"Team 2 To Beat Roshans In Row",
		"C"=>1
	],
	"9902"=>[
		"N"=>"Players, Total Steals And Blocks",
		"C"=>1
	],
	"9903"=>[
		"N"=>"Innings Start",
		"C"=>1
	],
	"9904"=>[
		"N"=>"Last Ball Of The Match",
		"C"=>1
	],
	"9905"=>[
		"N"=>"Boundary",
		"C"=>1
	],
	"9906"=>[
		"N"=>"Six",
		"C"=>1
	],
	"9907"=>[
		"N"=>"Team Results",
		"C"=>1
	],
	"9908"=>[
		"N"=>"Players, Runs",
		"C"=>1
	],
	"9909"=>[
		"N"=>"Wickets, Special bets",
		"C"=>1
	],
	"9910"=>[
		"N"=>"Intermediate Results",
		"C"=>1
	],
	"9911"=>[
		"N"=>"Players, Total 4's",
		"C"=>1
	],
	"9912"=>[
		"N"=>"Players, Total 6's",
		"C"=>1
	],
	"9913"=>[
		"N"=>"Match Result And Total",
		"C"=>1
	],
	"9914"=>[
		"N"=>"Match Result And Both To Score",
		"C"=>1
	],
	"9915"=>[
		"N"=>"Both Teams To Score Or Total",
		"C"=>1
	],
	"9916"=>[
		"N"=>"Both Teams To Have Offside",
		"C"=>1
	],
	"9917"=>[
		"N"=>"Next Number",
		"C"=>1
	],
	"9918"=>[
		"N"=>"1st Ball",
		"C"=>1
	],
	"9919"=>[
		"N"=>"Wides And Run-Outs",
		"C"=>1
	],
	"9920"=>[
		"N"=>"4's And 6's",
		"C"=>1
	],
	"9921"=>[
		"N"=>"Players,  Accumulator Outcomes",
		"C"=>1
	],
	"9922"=>[
		"N"=>"Opening Batsmen",
		"C"=>1
	],
	"9923"=>[
		"N"=>"Last Place Without Underdogs",
		"C"=>1
	],
	"9924"=>[
		"N"=>"Team 1 Best Player Of The Match",
		"C"=>1
	],
	"9925"=>[
		"N"=>"Team 2 Best Player Of The Match",
		"C"=>1
	],
	"9926"=>[
		"N"=>"Score After X Scored Goals",
		"C"=>1
	],
	"9927"=>[
		"N"=>"Team 1, Highest Scoring Period",
		"C"=>1
	],
	"9928"=>[
		"N"=>"Team 2, Highest Scoring Period",
		"C"=>1
	],
	"9929"=>[
		"N"=>"Team 1, All Goals Will Be Scored In One Side Of The Field",
		"C"=>1
	],
	"9930"=>[
		"N"=>"Team 2, All Goals Will Be Scored In One Side Of The Field",
		"C"=>1
	],
	"9931"=>[
		"N"=>"Result After First Overs (2Way)",
		"C"=>1
	],
	"9932"=>[
		"N"=>"Who Earliest",
		"C"=>1
	]
];


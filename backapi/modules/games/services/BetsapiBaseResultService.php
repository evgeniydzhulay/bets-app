<?php

namespace backapi\modules\games\services;

use common\modules\games\models\GameOutcomesModel;
use common\modules\games\models\BetsItemsModel;
use common\modules\games\models\EventOutcomesModel;

use Yii;
use yii\helpers\Json;
use yii\helpers\ArrayHelper;
use yii\helpers\BaseInflector;
use yii\db\Expression;

class BetsapiBaseResultService {

	protected $game;
	protected $sports_id;
	protected $game_id;

	const RESULT_STATUS_DEFEAT       = 0;
	const RESULT_STATUS_VICTORY      = 1;
	const RESULT_STATUS_HALF_VICTORY = 3;
	const RESULT_STATUS_HALF_DEFEAT  = 4;
	const RESULT_STATUS_REFUND       = 5;

	/**
	 * BetsapiBaseResultService constructor.
	 *
	 * @param $game
	 * @param $sports_id
	 * @param $game_id
	 */

	public function __construct ($game, $sports_id, $game_id) {
		$this->game = $game;
		$this->sports_id = $sports_id;
		$this->game_id = $game_id;
	}

	public function resultBetsItemsPrepare ($params) {
		$metodComparison = $this->getMetodComparison ($params['code_api']);
		$this->resultBetsItems([
			'metodComparison' => $metodComparison,
			'code_api' => $params['code_api'],
			'value' => $params['value'],
			'resultSportClass' => $this
		]);
	}

	/**
	 * @param array $params
	 *
	 * @return bool
	 */
	protected function resultBetsItems ($params) {
		if( ($metodComparison = $params['metodComparison']) === null)
			return false;

		if(($marketApi = BetsapiService::findMarket(BetsapiService::SOURCE_BETSAPI, $params['code_api'], $this->sports_id)) === null)
			return false;

		$market_id = $marketApi->market_id;

		$outcomes = GameOutcomesModel::find()->select([GameOutcomesModel::tableName().'.condition',BetsItemsModel::tableName().'.outcome_id'] )
			->andWhere([GameOutcomesModel::tableName().'.market_id' => $market_id])
			->innerJoin(EventOutcomesModel::tableName(), EventOutcomesModel::tableName().'.outcome_id = '. GameOutcomesModel::tableName().'.id' )->andWhere([
				EventOutcomesModel::tableName().'.is_banned' => false,
				EventOutcomesModel::tableName().'.game_id' => $this->game_id,
			])
			->innerJoin(BetsItemsModel::tableName(), BetsItemsModel::tableName().'.outcome_id = '. EventOutcomesModel::tableName().'.id' )->andWhere([
				BetsItemsModel::tableName().'.status' => BetsItemsModel::STATUS_ACTIVE
			])
			->asArray()
			->all();

		foreach($outcomes as $outcome){
			$condition = Json::decode($outcome['condition']);
			$outcomeResult = $params['resultSportClass']->$metodComparison($condition, $params['value']);

			if($outcomeResult !== null){
				switch((int)$outcomeResult){
					case self::RESULT_STATUS_VICTORY:
						$betsItemsStatus = BetsItemsModel::STATUS_VICTORY;
					break;
					case self::RESULT_STATUS_DEFEAT:
						$betsItemsStatus = BetsItemsModel::STATUS_DEFEAT;
					break;
					case self::RESULT_STATUS_HALF_VICTORY:
						$betsItemsStatus = BetsItemsModel::STATUS_HALF_VICTORY;
					break;
					case self::RESULT_STATUS_HALF_DEFEAT:
						$betsItemsStatus = BetsItemsModel::STATUS_HALF_DEFEAT;
					break;
					case self::RESULT_STATUS_REFUND:
						$betsItemsStatus = BetsItemsModel::STATUS_REFUND;
					break;
					default:
						return false;
					break;
				}

				$eventOutcome = EventOutcomesModel::findOne((int)$outcome['outcome_id']);
				if($eventOutcome != null ){
					$eventOutcome->setStatus($betsItemsStatus);
				}

			}
		}

	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultDrawNoBet($item, $value) {
		$itemParam = $item['opp'] ?? $item['name'];
		if($value[0] == $value[1]){
			return self::RESULT_STATUS_REFUND;
		} else {
			$valueString = $value[0] > $value[1] ? 'Home' : 'Away';
			return 	$itemParam == $valueString; 
		}
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultMatchSearchNotNull($item, $value) {
		if($value == null){
			return null;
		}
		$itemParam = $item['opp'] ?? $item['name'];
		return	strpos($itemParam, $value);
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultMatchSearch($item, $value) {
		$itemParam = $item['opp'] ?? $item['name'];
		return	strpos($itemParam, $value);
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultMatchSearchByValue($item, $value) {
		$itemParam = $item['opp'] ?? $item['name'];
		return	strpos($value, $itemParam);
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultExactMatch($item, $value) {
		$itemParam = $item['opp'] ?? $item['name'];
		return	$itemParam == $value;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultMatchHandicap($item, $value) {
		if( strpos($item['name'], 'Home') !== false ){
			return ($value[0] + $item['handicap']) > $value[1];
		} elseif( strpos($item['name'], 'Away') !== false ){
			return $value[0] < ($value[1] + $item['handicap']);
		}
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTotalsByHheader($item, $value) {
		if( strpos($item['header'], 'Over') !== false ){
			return $item['handicap'] < $value;
		} elseif( strpos($item['header'], 'Under') !== false ){
			return $item['handicap'] > $value;
		}
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTotalsByName($item, $value) {
		if( strpos($item['name'], 'Over') !== false ){
			return $item['handicap'] < $value;
		} elseif( strpos($item['name'], 'Under') !== false ){
			return $item['handicap'] > $value;
		}
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultOverUnderByName($item, $value) {
		if( strpos($item['name'], 'Over') !== false ){
			return $item['handicap'] < $value;
		} elseif( strpos($item['name'], 'Under') !== false ){
			return $item['handicap'] > $value;
		} elseif( strpos($item['name'], 'Exactly') !== false ){
			return $item['handicap'] == $value;
		}
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultOverUnderByHeader($item, $value) {
		if( strpos($item['header'], 'Over') !== false or strpos($item['header'], 'O') !== false ){
			return $item['handicap'] < $value;
		} elseif( strpos($item['header'], 'Under') !== false or strpos($item['header'], 'U') !== false){
			return $item['handicap'] > $value;
		} elseif( strpos($item['header'], 'Exactly') !== false or strpos($item['header'], 'E') !== false ){
			return $item['handicap'] == $value;
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultMoneyLine3Way($item, $value) {
		$valueResult = $value[0] > $value[1] ? 'Home' : $value[0] == $value[1] ? 'Tie' : 'Away';
		return	$item['name'] == $valueResult;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultCorrectSetScore($item, $value) {
		$valueResult1 = $value[0] > $value[1] ? 'Home' : 'Away';
		$valueResult1Short = $value[0] > $value[1] ? 1 : 2;
		if($valueResult1 != $item['header'] && $valueResult1Short != (int)$item['header']){
			return false;
		}
		$scoreArr = explode('-',$item['name']);
		if($valueResult1 == 'Home'){
			return ($value[0] == (int)$scoreArr[0] && $value[1] == (int)$scoreArr[1]);
		} else {
			return ($value[0] == (int)$scoreArr[1] && $value[1] == (int)$scoreArr[0]);
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultHandicap2Way($item, $value) {
		if( strpos($item['header'], '1') !== false or strpos($item['header'], 'Home') !== false){
			return ($value[0] + $item['handicap']) > $value[1];
		} elseif( strpos($item['header'], '2') !== false or strpos($item['header'], 'Away') !== false ){
			return $value[0] < ($value[1] + $item['handicap']);
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 *
	 * X handicap (+/-) for Away
	 */
	protected function resultHandicap3Way($item, $value) {
		$itemParam = $item['header'] ?? $item['name'];
		if( strpos($itemParam, '1') !== false or strpos($itemParam, 'Home') !== false){
			return ($value[0] + $item['handicap']) > $value[1];
		} elseif( strpos($itemParam, 'X') !== false or strpos($itemParam, 'Tie') !== false){
			return ($value[0] + $item['handicap']) == $value[1];
		} elseif( strpos($itemParam, '2') !== false or strpos($itemParam, 'Away') !== false){
			return $value[0] < ($value[1] + $item['handicap']);
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultPlayerOddEven($item, $value) {
		if($item['header'] == 'Home' or (int)$item['header'] == 1){
			$valueResult = $value[0];
		} elseif($item['header'] == 'Away' or (int)$item['header'] == 2 )	{
			$valueResult = $value[1];
		} else {
			return null;
		}
		$valueResultSting = $valueResult % 2 == 0 ? 'Even' : 'Odd';
		return $item['name'] == $valueResultSting;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultPlayerTotal($item, $value) {
		if( strpos($item['header'], 'Home') !== false or strpos($item['header'], '1') !== false ){
			$itemValue = $value[0];
		} elseif( strpos($item['header'], 'Away') !== false  or strpos($item['header'], '2') !== false ){
			$itemValue = $value[1];
		} else {
			return null;
		}
		if( strpos($item['name'], 'Over') !== false or strpos($item['name'], 'O ') !== false){
			return $item['handicap'] < $itemValue;
		} elseif( strpos($item['name'], 'Under') !== false or strpos($item['name'], 'U ') !== false ){
			return $item['handicap'] > $itemValue;
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultPlayerTotal3Way($item, $value) {
		if( strpos($item['header'], 'Home') !== false or strpos($item['header'], '1') !== false ){
			$valuePlayer = $value[0];
		} elseif( strpos($item['header'], 'Away') !== false  or strpos($item['header'], '2') !== false ){
			$valuePlayer = $value[1];
		} else {
			return null;
		}
		if( strpos($item['name'], 'Over') !== false or strpos($item['name'], 'O ') !== false){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue < $valuePlayer;
		} elseif( strpos($item['name'], 'Under') !== false or strpos($item['name'], 'U ') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue > $valuePlayer;
		} elseif( strpos($item['name'], 'Exactly') !== false or strpos($item['name'], 'E') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue == $valuePlayer;
		}elseif( strpos($item['name'], '-') !== false ){
			$itemValues = explode('-', $item['name']);
			return (int)$itemValues[0] <= $valuePlayer && (int)$itemValues[1] >= $valuePlayer;
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultWinningMargin($item, $value) {
		if( $item['header'] !== $value['player'] ){
			return false;
		}
		if( strpos($item['name'], '+') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue <= $value['margin'];
		} else {
			$itemValues = explode('-', $item['name']);
			return (int)$itemValues[0] <= $value['margin'] && (int)$itemValues[1] >= $value['margin'];
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultWinningMarginExact($item, $value) {
		if( $item['header'] !== $value['player'] ){
			return false;
		}
		if( strpos($item['name'], '+') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue <= $value['margin'];
		} else {
			return (int)$item['name'] == $value['margin'];
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTeamTotalOddEven($item, $value) {
		if (array_search($item['header'],['1','2','Home','Away']) === false)
			return null;

		if($item['header'] == 1 or $item['header'] == 'Home'){
			$valueResult = $value[0] % 2 == 0 ? 'Even' : 'Odd';
		} else {
			$valueResult = $value[1] % 2 == 0 ? 'Even' : 'Odd';
		}
		return	$item['name'] == $valueResult;
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTotalsOnlyName($item, $value) {
		$itemNum = (float)filter_var($item['name'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		if( strpos($item['name'], 'Over') !== false ){
			return $itemNum < $value;
		} elseif( strpos($item['name'], 'Under') !== false ){
			return $itemNum > $value;
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRaceTo($item, $value) {
		if ($item['name'] != $value['raceValue'])
			return null;

		return	$item['header'] == $value['player'];
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTotalBands($item, $value) {
		if( strpos($item['name'], '+') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue <= $value;
		} else {
			$itemValues = explode('-', $item['name']);
			return (int)$itemValues[0] <= $value && (int)$itemValues[1] >= $value;
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyDoubleResult($item, $value) {
		$itemValues = explode('-', $item['name']);
		return strpos($itemValues[0], $value['firstHalf']) && strpos($itemValues[1], $value['fullTime']);
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyDoubleResult5Way($item, $value) {
		if($value['firstHalf'] == 'Tie' or $value['fullTime'] == 'Tie' ){
			return  $item['name'] == 'Any Other';
		}
		$itemValues = explode('-', $item['name']);
		return strpos($itemValues[0], $value['firstHalf']) && strpos($itemValues[1], $value['fullTime']);
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyStartPlus($item, $value) {
		if( strpos($item['name'], 'Home') !== false ){
			return ($value['score'][0] + $value['start_plus']) > $value['score'][1];
		} elseif( strpos($item['name'], 'Away') !== false ){
			return $value['score'][0] < ($value['score'][1] + $value['start_plus']);
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyWinningMarginByName($item, $value) {
		if( strpos($item['name'], $value['player']) === false ){
			return false;
		}
		if($value['player'] == 'Tie'){
			return true;
		}
		if( strpos($item['name'], '+') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue <= $value['margin'];
		} elseif( strpos($item['name'], '-') !== false ) {
			preg_match('/(?<home>\d+) - (?<away>\d+)/', $event['text'], $matches);
			return (int)$matches['home'] <= $value['margin'] && (int)$matches['away'] >= $value['margin'];
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyWinningMarginByNameExact($item, $value) {
		if( strpos($item['name'], $value['player']) === false ){
			return false;
		}
		if($value['player'] == 'Tie'){
			return true;
		}
		if( strpos($item['name'], '+') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue <= $value['margin'];
		} elseif( strpos($item['name'], 'Exactly') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue == $value['margin'];
		} elseif( strpos($item['name'], '-') !== false ) {
			preg_match('/(?<start>\d+) - (?<end>\d+)/', $item['name'], $matches);
			return (int)$matches['start'] <= $value['margin'] && (int)$matches['end'] >= $value['margin'];
		}
		return null;
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugby3WayTotal($item, $value) {
		if( strpos($item['name'], 'Over') !== false){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue < $value;
		} elseif( strpos($item['name'], 'Under') !== false){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue > $value;
		} elseif( strpos($item['name'], 'To') !== false){
			$itemValues = explode('To', $item['name']);
			return (int)$itemValues[0] <= $value && (int)$itemValues[1] >= $value;
		}
		return null;
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyAlternativeTotal3Way($item, $value) {
		if( strpos($item['header'], 'O') !== false){
			$itemValue = preg_replace("/[^0-9]/", '', $item['handicap']);
			return $itemValue < $value;
		} elseif( strpos($item['header'], 'U') !== false){
			$itemValue = preg_replace("/[^0-9]/", '', $item['handicap']);
			return $itemValue > $value;
		} elseif( strpos($item['handicap'], '-') !== false){
			$itemValues = explode('-', $item['handicap']);
			return (int)$itemValues[0] <= $value && (int)$itemValues[1] >= $value;
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyTribet($item, $value) {
		//By 7 Or More
		if( $item['name'] == 'Any Other Result' && $value['margin'] < 7 ){
			return true;
		}
		return strpos($item['name'], $value['player']);
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyHtFtWinningMargin($item, $value) {
		if($item['name'] == 'Any Other Result' && ($value['firstHalf']['margin'] == 0 or $value['fullTime']['margin'] == 0) ){
			return true;
		}
		if( strpos($item['name'], '/') !== false ){
			list($itemFirstHalf,$itemFullTime) = explode('/', $item['name']);
			if( strpos($itemFirstHalf, $value['firstHalf']['player']) === false or strpos($itemFullTime, $value['fullTime']['player']) === false){
				return false;
			}
			if( strpos($itemFirstHalf, '+') !== false ){
				$itemValueFirstHalf = preg_replace("/[^0-9]/", '', $itemFirstHalf);
				if($itemValueFirstHalf > $value['firstHalf']['margin']){
					return false;
				}
			} elseif( strpos($itemFirstHalf, '-') !== false ) {
				preg_match('/(?<start>\d+) - (?<end>\d+)/', $itemFirstHalf, $matchesFirstHalf);
				if ($matchesFirstHalf['start'] > $value['firstHalf']['margin'] or $matchesFirstHalf['end'] < $value['firstHalf']['margin'] ){
					return false;
				}
			}
			if( strpos($itemFullTime, '+') !== false ){
				$itemValue = preg_replace("/[^0-9]/", '', $itemFullTime);
				return $itemValue <= $value['fullTime']['margin'];
			} elseif( strpos($itemFullTime, '-') !== false ) {
				preg_match('/(?<start>\d+) - (?<end>\d+)/', $itemFullTime, $matchesFullTime);
				return (int)$matchesFullTime['start'] <= $value['fullTime']['margin'] && (int)$matchesFullTime['end'] >= $value['fullTime']['margin'];
			}
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyToLeadAfterMinutes($item, $value) {
		if( $item['name'] == $value['time']){
			$valueResult = $value['score'][0] > $value['score'][1] ? '1' : $value['score'][0] == $value['score'][1] ? 'Tie' : '2';
			return	$item['header'] == $valueResult;
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyScore1stMatchResult($item, $value) {
		if( ($item['name'] == 'No Tryscorer' or $item['name'] == 'No 1st Half Tryscorer') && $value['scoreFirst'] == null ){
			return true;
		}
		if( strpos($item['name'], '&') !== false ){
			list($itemScoreFirst,$itemGamePeriod) = explode('&', $item['name']);
			$valueGamePeriodResult = $value['scoreFirst'] == $value['gamePeriodResult'] ? 'Win' : ($value['gamePeriodResult'] == 'Tie' ?? 'Lose');
			return strpos($itemScoreFirst, $value['scoreFirst']) && strpos($itemGamePeriod, $valueGamePeriodResult);
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyScore1stTry1stHalfResult($item, $value) {
		if( $item['name'] == 'Any Other' && ($value['scoreFirst'] == null or $value['firstHalf'] == 'Tie' or $value['fullTime'] == 'Tie' ) ){
			return true;
		}
		if( strpos($item['name'], '-') !== false ){
			list($itemScoreFirst,$itemFirstHalf,$itemFullTime) = explode('-', $item['name']);
			return strpos($itemScoreFirst, $value['scoreFirst']) && strpos($itemFirstHalf, $value['firstHalf']) && strpos($itemFullTime, $value['fullTime']);
		}
		return null;
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyTeamToWinBothHalves($item, $value) {
		if($value == 'Neither Team' && $item['name'] == 'No'){
			return true;
		}
		return ($item['header'] == $value && $item['name'] == 'Yes') or ($item['header'] != $value && $item['name'] == 'No');
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyFirstScoringPlay6Way($item, $value) {
		if($value['team'] == null){
			return self::RESULT_STATUS_REFUND;
		}
		$valueTeam = $value['team'] == 'Home' ? 1 : 2;
		return $item['header'] == $valueTeam && $item['name'] == $value['scoring_tipe'];
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyFirstScoringPlay4Way($item, $value) {
		if($value['team'] == null){
			return self::RESULT_STATUS_REFUND;
		}
		$valueTeam = $value['team'] == 'Home' ? 1 : 2;
		$valueScoringTipe = $value['scoring_tipe'] == 'Try' ?? 'Any Other';
		return $item['header'] == $valueTeam && $item['name'] == $valueScoringTipe;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyTeamToWinEitherHalf($item, $value) {
		if($item['name'] == 'Yes'){
			return $item['header'] == $value['1 half'] or $item['header'] == $value['2 half'];
		} elseif($item['name'] == 'No'){
			return $item['header'] != $value['1 half'] && $item['header'] != $value['2 half'];
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRugbyToScoreThreeUnansweredTries($item, $value) {
		if (array_search($item['header'],['1','2']) === false)
			return null;

		$itemHeader = $item['header'] == 1 ? 'home' : 'away';
		return $item['name'] == $value[$itemHeader];
	}
}

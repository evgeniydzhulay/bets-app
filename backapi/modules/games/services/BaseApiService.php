<?php
namespace backapi\modules\games\services;

use common\modules\games\helpers\GameHelpers;
use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventGameLocaleModel;
use common\modules\games\models\GameMarketsModel;
use common\modules\games\models\GameMarketsLocaleModel;
use common\modules\games\models\GameMarketsGroupsModel;
use common\modules\games\models\GameMarketsGroupsLocaleModel;
use common\modules\games\models\EventOutcomesModel;
use common\modules\games\models\EventTournamentModel;
use common\modules\games\models\EventTournamentLocaleModel;
use common\modules\games\models\EventParticipantsModel;
use common\modules\games\models\GameParticipantsModel;
use common\modules\games\models\GameParticipantsLocaleModel;
use common\modules\games\models\GameSportsModel;
use common\modules\games\models\GameSportsLocaleModel;
use common\modules\games\models\GameOutcomesModel;
use common\modules\games\models\GameOutcomesLocaleModel;

use common\modules\games\models\ApiGameSportsModel;
use common\modules\games\models\ApiEventTournamentModel;
use common\modules\games\models\ApiEventGameModel;
use common\modules\games\models\ApiGameParticipantsModel;
use common\modules\games\models\ApiGameParticipantsImagesModel;
use common\modules\games\models\ApiGameMarketsModel;
use common\modules\games\models\ApiGameMarketsGroupsModel;
use common\modules\games\models\ApiGameOutcomesModel;
use backapi\modules\games\services\Betsapi1xbetService;
use backapi\modules\games\services\BetsapiService;
use common\modules\games\queries\EventGameLocaleQuery;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;
use yii\helpers\BaseInflector;
use yii\helpers\Json;

class BaseApiService {

	public $lang_code = null;

	public function findEventOutcome($game_id, $outcome_id) {

		if(($model = EventOutcomesModel::find()->andWhere([
				'game_id' => $game_id,
				'outcome_id' => $outcome_id,
				])->one()) !== null){
			return $model;
		}else{
			return null;
		}
	}

	public function createEventOutcomes($params) {
		try {
			$eventOutcomesModel = new EventOutcomesModel();
			$eventOutcomesModel->setAttributes([
				'sports_id' => $params['sports_id'],
				'tournament_id' => $params['tournament_id'],
				'game_id' => $params['game_id'],
				'market_id' => $params['market_id'],
				'group_id' => $params['group_id'],
				'outcome_id' => $params['outcome_id'],
				'is_enabled' => (int) $params['is_enabled'],
				'rate' => (float) $params['rate'],
				//'is_hidden' => (int) $params['is_hidden'],
			]);
			$eventOutcomesModel->save();
			return $eventOutcomesModel;
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		return null;
	}

	public function findOutcome($service_api, $code_api) {

		if(($model = ApiGameOutcomesModel::find()->andWhere([
				'service_api'=>$service_api,
				'code_api'=> $code_api
			])->one()) !== null){
			return $model->outcome_id;
		}else{
			return null;
		}
	}

	public function findOutcomeByName($params){
		if(($outcomesLocale = GameOutcomesLocaleModel::find()->andWhere([
			'lang_code' => $params['lang'],
			'market_id' => $params['market_id'],
			])->all()) !== null){
				foreach($outcomesLocale as $item){
					foreach($params['names'] as $name){
						if($item->title == $name){
							return $item->outcome_id;
						}
					}
				}
				return null;
		}else{
			return null;
		}
	}

	public function createApiOutcome($params) {
		$apiGameOutcomesModel = new ApiGameOutcomesModel();
		$apiGameOutcomesModel->setAttributes([
			'outcome_id' => $params['outcome_id'],
			'service_api' => $params['service_api'],
			'code_api' => (string) $params['code_api'],
		]);
		$apiGameOutcomesModel->save();
	}

	public function createOutcome($params) {
		$tr = Yii::$app->db->beginTransaction();
		try {
			$model = new GameOutcomesModel(['scenario' => GameOutcomesModel::SCENARIO_CREATE]);
			$model->setAttributes([
				'sports_id' => $params['sports_id'],
				'market_id' => $params['market_id'],
				'group_id' => $params['group_id'],
				//'is_enabled' => GameOutcomesModel::STATUS_DISABLED,
				'is_enabled' => GameOutcomesModel::STATUS_ENABLED,
				'condition' => $params['condition'],
			]);

			if($model->save()){
				$locale = new GameOutcomesLocaleModel(['scenario' => GameOutcomesLocaleModel::SCENARIO_CREATE]);
				$locale->setAttributes([
					'sports_id' => $model->sports_id,
					'market_id' => $model->market_id,
					'group_id' => $model->group_id,
					'outcome_id' => $model->id,
					'lang_code' => $params['lang'],
					'title' => $params['title'],
					'is_active' => 1,
				]);
				$locale->save();

				$this->createEventOutcomes([
					'sports_id' => $model->sports_id,
					'tournament_id' => $params['tournament_id'],
					'game_id' => $params['game_id'],
					'market_id' => $model->market_id,
					'group_id' => $model->group_id,
					'outcome_id' => $model->id,
					'is_enabled' => $params['is_enabled'],
					'rate' => $params['rate'],
					//'is_hidden' => $params['is_hidden'],
				]);

				$this->createApiOutcome([
					'outcome_id' => $model->id,
					'service_api' => $params['service_api'],
					'code_api' => $params['code_api'],
				]);

				$tr->commit();
				return $model->id;
			}
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		$tr->rollBack();
		return null;
	}

	public function findMarket($service_api, $code_api, $sports_id ) {

		if(($model = ApiGameMarketsModel::find()->andWhere([
				'service_api' =>$service_api,
				'code_api' => $code_api,
				'sports_id' => $sports_id,
			])->one()) !== null){
			return $model;
		}else{
			return null;
		}
	}

	public function findMarketApiByMarketId($service_api, $market_id) {

		if(($model = ApiGameMarketsModel::find()->andWhere([
				'service_api' =>$service_api,
				'market_id' => $market_id,
			])->one()) !== null){
			return $model;
		}else{
			return null;
		}
	}

	public function findMarketByName($params){
		if(($marketsLocale = GameMarketsLocaleModel::find()->andWhere([
			'lang_code' => $params['lang'],
			'sports_id' => $params['sports_id'],
			'title' => $params['name'],
			])->one()) !== null){
				return $marketsLocale;
			}
		return null;
	}

	public function createApiMarket($params) {
		$apiGameMarketsModel = new ApiGameMarketsModel();
		$apiGameMarketsModel->setAttributes([
			'market_id' => $params['market_id'],
			'group_id' => $params['group_id'],
			'sports_id' => $params['sports_id'],
			'service_api' => $params['service_api'],
			'code_api' => (string) $params['code_api'],
		]);
		$apiGameMarketsModel->save();
	}

	public function createMarket($params) {
		$tr = Yii::$app->db->beginTransaction();
		try {
			$model = new GameMarketsModel(['scenario' => GameMarketsModel::SCENARIO_CREATE]);
			$model->setAttributes([
				'sports_id' => $params['sports_id'],
				'group_id' => $params['group_id'],
				//'is_enabled' => GameMarketsModel::STATUS_DISABLED,
				'is_enabled' => GameMarketsModel::STATUS_ENABLED,
			]);

			if($model->save()){
				$locale = new GameMarketsLocaleModel(['scenario' => GameMarketsLocaleModel::SCENARIO_CREATE]);
				$locale->setAttributes([
					'sports_id' => $model->sports_id,
					'market_id' => $model->id,
					'group_id' => $model->group_id,
					'lang_code' => $params['lang'],
					'title' => $params['title'],
					'is_active' => 1,
				]);
				$locale->save();

				$this->createApiMarket([
					'market_id' => $model->id,
					'group_id' => $model->group_id,
					'sports_id' => $model->sports_id,
					'service_api' => $params['service_api'],
					'code_api' => $params['code_api'],
				]);

				$tr->commit();
				return $model->id;
			}
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		$tr->rollBack();
		return null;
	}

	public function findMarketGroup($service_api, $code_api, $sports_id) {
		if(($model = ApiGameMarketsGroupsModel::find()->andWhere([
				'service_api'=>$service_api,
				'code_api'=> $code_api,
				'sports_id' => $sports_id,
			])->one()) !== null){
			return $model->group_id;
		}else{
			return null;
		}
	}

	public function createMarketGroup($params) {
		$tr = Yii::$app->db->beginTransaction();
		try {
			$model = new GameMarketsGroupsModel(['scenario' => GameMarketsGroupsModel::SCENARIO_CREATE]);
			$model->setAttributes(['sports_id' => $params['sports_id'],'sort_order' => 0]);

			if($model->save()){
				$locale = new GameMarketsGroupsLocaleModel(['scenario' => GameMarketsGroupsLocaleModel::SCENARIO_CREATE]);
				$locale->setAttributes([
					'sports_id' => $model->sports_id,
					'group_id' => $model->id,
					'lang_code' => $params['lang'],
					'title' => $params['title'],
					'is_active' => 1,
				]);
				$locale->save();

				$apiGameMarketsGroupsModel = new ApiGameMarketsGroupsModel();
				$apiGameMarketsGroupsModel->setAttributes([
					'group_id' => $model->id,
					'sports_id' => $model->sports_id,
					'service_api' => $params['service_api'],
					'code_api' => (string) $params['code_api'],
				]);
				$apiGameMarketsGroupsModel->save();
				$tr->commit();
				return $model->id;
			}
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		$tr->rollBack();
		return null;
	}

	public function findGame($service_api, $code_api) {

		if(($model = ApiEventGameModel::find()->andWhere([
				'service_api'=>$service_api,
				'code_api'=> $code_api
			])->one()) !== null){
			return $model;
		}else{
			return null;
		}
	}

	public function findGameByApiEventId($api_event_id) {
		if(($model = ApiEventGameModel::find()->andWhere([
				'api_event_id'=> $api_event_id,
				'is_main_provider' => true,
			])->one()) !== null){
			return $model;
		}else{
			return null;
		}
	}

	public function findGameResultByApiEventId($service_api, $api_event_id) {
		if(($model = ApiEventGameModel::find()->andWhere([
				'service_api'=>$service_api,
				'api_event_id'=> $api_event_id
			])->innerJoin(EventGameModel::tableName(), EventGameModel::tableName().'.id = '. ApiEventGameModel::tableName().'.game_id' )->andWhere([
				EventGameModel::tableName().'.is_resulted' => false,
			])->one()) !== null){
			return $model;
		}else{
			return null;
		}
	}

	public function findGameByName($params) {
		$this->lang_code = $params['lang'];
		$params['name'] = ucwords($params['name']);

		$query = EventGameModel::find()->andWhere([
					'sports_id' => $params['sports_id'],
					'tournament_id' => $params['tournament_id']
				])->with([
					'eventGameLocales' => function (EventGameLocaleQuery $query) {
						return $query->andWhere([
							'lang_code' => $this->lang_code,
						]);
					},
				]);
		if(!empty($params['starts_at'])){
			$query->andWhere(['starts_at' => $params['starts_at']]);
		}

		if(($games = $query->all()) !== null){
					$game_id = null;
					$percMax = 0;

				foreach($games as $game){
					$locale = GameHelpers::getLocalization($game['eventGameLocales']);
					if($locale->title == $params['name']){
						return $locale->game_id;
					}
					similar_text($locale->title, $params['name'], $perc);
					if($perc > 80 && $perc > $percMax){
						$percMax = $perc;
						$game_id = $locale->game_id;
					}
				}
				return $game_id;
		}else{
			return null;
		}
	}

	public function findGameByParticipants($params) {
		$query = EventGameModel::find()->addSelect([
				EventGameModel::tableName().'.id',
				'participants_count' => EventParticipantsModel::find()
					->where(EventGameModel::tableName().'.id = '.EventParticipantsModel::tableName().'.game_id')
					->select([
						'count' => new Expression('COUNT(*)')
					])->andWhere(['participant_id' => $params['participants']])
				])->andWhere([
					EventGameModel::tableName().'.sports_id' => $params['sports_id'],
					EventGameModel::tableName().'.tournament_id' => $params['tournament_id']
				])->having([
					'=', 'participants_count', 2
				]);
		if(!empty($params['starts_at'])){
			$query->andWhere(['starts_at' => $params['starts_at']]);
		} else {
			$query->orderBy(['starts_at' => SORT_DESC]);
		}

		if(($game = $query->one()) !== null){
			return $game->id;
		}else{
			return null;
		}
	}

	public function createApiGame($params) {
		$apiEventGameModel = new ApiEventGameModel();
		$apiEventGameModel->setAttributes([
			'game_id' => $params['game_id'],
			'sports_id' => $params['sports_id'],
			'tournament_id' => $params['tournament_id'],
			'service_api' => $params['service_api'],
			'code_api' => (string) $params['code_api'],
			'api_event_id' => (int) $params['api_event_id'],
			'is_main_provider' =>(int)!empty($params['is_main_provider']) ?? 0,
		]);
		$apiEventGameModel->save();
	}

	public function createGame($params) {
		$params['title'] = ucwords($params['title']);
		$tr = Yii::$app->db->beginTransaction();
		try {
			$model = new EventGameModel(['scenario' => EventGameModel::SCENARIO_CREATE]);
			$model->setAttributes([
				'sports_id' => $params['sports_id'],
				'tournament_id' => $params['tournament_id'],
				'starts_at' => (int) $params['starts_at'],
				//'is_enabled' => EventGameModel::STATUS_DISABLED,
				'is_enabled' => $params['is_enabled'],
				'is_live' => $params['is_live'],
			]);
			if($model->save()){

				$locale = new EventGameLocaleModel(['scenario' => EventGameLocaleModel::SCENARIO_CREATE]);
				$locale->setAttributes([
					'game_id' => $model->id,
					'sports_id' => $model->sports_id,
					'tournament_id' => $model->tournament_id,
					'lang_code' => $params['lang'],
					'title' => $params['title'],
					'is_active' => 1,
				]);
				$locale->save();

				foreach($params['participants'] as $sort_order => $participant_id){
					$eventParticipants = new EventParticipantsModel(['scenario' => EventParticipantsModel::SCENARIO_CREATE]);
					$eventParticipants->setAttributes([
						'sports_id' => $model->sports_id,
						'tournament_id' => $model->tournament_id,
						'game_id' => $model->id,
						'participant_id' => $participant_id,
						'sort_order' => $sort_order,
					]);
					$eventParticipants->save();
				}

				$this->createApiGame([
					'game_id' => $model->id,
					'sports_id' => $model->sports_id,
					'tournament_id' => $model->tournament_id,
					'service_api' => $params['service_api'],
					'code_api' => $params['code_api'],
					'api_event_id' => $params['api_event_id'],
					'is_main_provider' => 1,
				]);

				$tr->commit();
				return $model->id;
			}
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		$tr->rollBack();
		return null;
	}

	public function findParticipant($service_api, $code_api) {

		if(($model = ApiGameParticipantsModel::find()->andWhere([
				'service_api'=>$service_api,
				'code_api'=> $code_api
			])->one()) !== null){
			return $model;
		}else{
			return null;
		}
	}

	public function findParticipantByName($params){
		$params['name'] = ucwords($params['name']);
		if(($participantsLocale = GameParticipantsLocaleModel::find()->andWhere([
			'lang_code' => $params['lang'],
			'sports_id' => $params['sports_id'],
			])->all()) !== null){
				foreach($participantsLocale as $item){
					if($item->name == $params['name']){
						return $item;
					}
					similar_text($item->name, $params['name'], $perc);
					if($perc == 100){
						return $item;
					}
				}
				return null;
		}else{
			return null;
		}
	}

	public function createApiParticipant($params) {
		$apiGameParticipant = new ApiGameParticipantsModel();
		$apiGameParticipant->setAttributes([
			'participant_id' => $params['participant_id'],
			'service_api' => $params['service_api'],
			'code_api' => (string) $params['code_api'],
		]);
		$apiGameParticipant->save();
	}

	public function setApiParticipantImage($params) {
		if(($model = ApiGameParticipantsImagesModel::find()->andWhere([
				'participant_id' => $params['participant_id'],
				'service_api' => $params['service_api'],
			])->one()) === null){
			$apiGameParticipant = new ApiGameParticipantsImagesModel();
			$apiGameParticipant->setAttributes([
				'sports_id' => $params['sports_id'],
				'participant_id' => $params['participant_id'],
				'service_api' => $params['service_api'],
				'code_image' => (string) $params['code_image'],
			]);
			$apiGameParticipant->save();
		}
	}

	protected function uploadParticipantImage($image) {
		return null;
	}

	public function checkParticipantImage() {
		try {
			if(($apiImages = ApiGameParticipantsImagesModel::find()->andWhere([
					'service_api' => $this::SOURCE_BETSAPI,
					'is_checked' => false,
				])->all()) !== null){
				foreach($apiImages as $apiImage){
					$participant = GameParticipantsModel::findOne($apiImage->participant_id);
					if(empty($participant->image) && ($image = $this->uploadParticipantImage($apiImage->code_image)) !== null){
						$participant->image = $image;
						$participant->image_is_approved = false;
						$participant->save(false);
					}
					$apiImage->is_checked = true;
					$apiImage->save(false);
				}
			}
		} catch (\Throwable $e) {
			Yii::error($e);
		}
	}

	public function createParticipant($params) {
		$params['name'] = ucwords($params['name']);
		$tr = Yii::$app->db->beginTransaction();
		try {
			$model = new GameParticipantsModel(['scenario' => GameParticipantsModel::SCENARIO_CREATE]);
			$model->sports_id = $params['sports_id'];

			if($model->save()){
				$locale = new GameParticipantsLocaleModel(['scenario' => GameParticipantsLocaleModel::SCENARIO_CREATE]);
				$locale->setAttributes([
					'sports_id' => $model->sports_id, 
					'participant_id' => $model->id , 
					'lang_code' => $params['lang'],
					'name' => $params['name'],
					'is_active' => 1,
				]);
				$locale->save();

				$this->createApiParticipant([
					'participant_id' => $model->id,
					'service_api' => $params['service_api'],
					'code_api' => $params['code_api'],
				]);

				$tr->commit();
				return $model->id;
			}
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		$tr->rollBack();
		return null;
	}

	public function findTournament($service_api, $code_api) {

		if(($model = ApiEventTournamentModel::find()->andWhere([
				'service_api'=>$service_api,
				'code_api'=> $code_api
			])->one()) !== null){
			return $model->tournament_id;
		}else{
			return null;
		}
	}

	public function findTournamentByName($params){
		if(($tournamentLocale = EventTournamentLocaleModel::find()->andWhere([
			'lang_code' => $params['lang'],
			'sports_id' => $params['sports_id'],
			])->all()) !== null){
				$tournament_id = null;
				$percMax = 0;

				foreach($tournamentLocale as $item){
					if($item->title == $params['name']){
						return $item->tournament_id;
					}
					similar_text($item->title, $params['name'], $perc);
					if($perc >= 99 && $perc > $percMax){
						$percMax = $perc;
						$tournament_id = $item->tournament_id;
					}
				}
				return $tournament_id;
		}else{
			return null;
		}
	}

	public function createApiTournament($params) {
		$apiEventTournament = new ApiEventTournamentModel();
		$apiEventTournament->setAttributes([
			'tournament_id' => $params['tournament_id'],
			'service_api' => $params['service_api'],
			'code_api' => (string) $params['code_api'],
		]);
		$apiEventTournament->save();
	}

	public function createTournament($params) {
		$tr = Yii::$app->db->beginTransaction();
		try {
			$model = new EventTournamentModel(['scenario' => EventTournamentModel::SCENARIO_CREATE]);
			//$model->setAttributes(['sports_id' => $params['sports_id'], 'is_enabled' => EventTournamentModel::STATUS_DISABLED]);
			$model->setAttributes(['sports_id' => $params['sports_id'], 'is_enabled' => EventTournamentModel::STATUS_ENABLED]);

			if($model->save()){
				$locale = new EventTournamentLocaleModel(['scenario' => EventTournamentLocaleModel::SCENARIO_CREATE]);
				$locale->setAttributes([
					'sports_id' => $model->sports_id, 
					'tournament_id' => $model->id , 
					'lang_code' => $params['lang'],
					'title' => $params['title'],
					'is_active' => 1,
				]);
				$locale->save();

				$this->createApiTournament([
					'tournament_id' => $model->id,
					'service_api' => $params['service_api'],
					'code_api' => $params['code_api'],
				]);

				$tr->commit();
				return $model->id;
			}
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		$tr->rollBack();
		return null;
	}

	public function findSport($service_api, $code_api) {

		if(($model = ApiGameSportsModel::find()->andWhere([
				'service_api'=>$service_api,
				'code_api'=> $code_api
			])->one()) !== null){
			return $model->sports_id;
		}else{
			return null;
		}
	}

	public function findSportByName($params){
		if(($sportsLocale = GameSportsLocaleModel::find()->andWhere(['lang_code' => $params['lang']])->all()) !== null){
			foreach($sportsLocale as $item){
				similar_text($item->title, $params['name'], $perc);
				if($perc >= 95){
					return $item->sports_id;
				}
			}
		}
		return null;
	}

	public function createApiSport($params) {
		$apiGameSports = new ApiGameSportsModel();
		$apiGameSports->setAttributes([
			'sports_id' => $params['sports_id'],
			'service_api' => $params['service_api'],
			'code_api' => (string) $params['code_api'],
		]);
		$apiGameSports->save();
	}

	public function createSport($params) {
		$tr = Yii::$app->db->beginTransaction();
		try {
			$model = new GameSportsModel(['scenario' => GameSportsModel::SCENARIO_CREATE]);
			//$model->setAttributes(['is_enabled' => GameSportsModel::STATUS_DISABLED]);
			$model->setAttributes(['is_enabled' => GameSportsModel::STATUS_ENABLED]);

			if($model->save()){
				$locale = new GameSportsLocaleModel(['scenario' => GameSportsLocaleModel::SCENARIO_CREATE]);
				$locale->setAttributes([
					'sports_id' => $model->id,
					'lang_code' => $params['lang'],
					'title' => $params['title'],
					'is_active' => 1,
				]);
				$locale->save();

				$this->createApiSport([
					'sports_id' => $model->id,
					'service_api' => $params['service_api'],
					'code_api' => $params['code_api'],
				]);

				$tr->commit();
				return $model->id;
			}
		} catch (\Throwable $e) {
			Yii::error($e);
		}
		$tr->rollBack();
		return null;
	}

	public function processingPrematch($params) {
		$transaction = Yii::$app->db->beginTransaction();
		try {
			if(($gameApi = $this->findGame($params['service_api'], $params['event']['id'])) === null){
				if($params['event']['our_event_id'] > 0 && ($gameApi = $this->findGameByApiEventId($params['event']['our_event_id'])) !== null){
					$game_id = $gameApi->game_id;
					$this->createApiGame([
						'game_id' => $gameApi->game_id,
						'sports_id' => $gameApi->sports_id,
						'tournament_id' => $gameApi->tournament_id,
						'service_api' => $params['service_api'],
						'code_api' => $params['event']['id'],
						'api_event_id' => $gameApi->api_event_id,
						//'is_main_provider' => ($params['service_api'] == BetsapiService::SOURCE_BETSAPI) ? 1 : 0,
						'is_main_provider' => ($params['service_api'] == BetsapiService::SOURCE_BETSAPI) ? 0 : 1,
					]);
					//if($gameApi->service_api == Betsapi1xbetService::SOURCE_BETSAPI && $params['service_api'] != Betsapi1xbetService::SOURCE_BETSAPI){
					if($gameApi->service_api == BetsapiService::SOURCE_BETSAPI && $params['service_api'] != BetsapiService::SOURCE_BETSAPI){
						$gameApi->is_main_provider = false;
						$gameApi->save(false);
/*
						\Yii::$app->db->createCommand('UPDATE '.EventOutcomesModel::tableName().', '.ApiGameMarketsModel::tableName().'
							SET '.EventOutcomesModel::tableName().'.is_hidden = 1
							WHERE '.EventOutcomesModel::tableName().'.market_id = '.ApiGameMarketsModel::tableName().'.market_id 
							AND '.EventOutcomesModel::tableName().'.game_id = '.$gameApi->game_id.'
							AND '.ApiGameMarketsModel::tableName().'.service_api = '.Betsapi1xbetService::SOURCE_BETSAPI)
						->execute();*/
						\Yii::$app->db->createCommand('UPDATE '.EventOutcomesModel::tableName().', '.ApiGameMarketsModel::tableName().'
							SET '.EventOutcomesModel::tableName().'.is_hidden = 1
							WHERE '.EventOutcomesModel::tableName().'.market_id = '.ApiGameMarketsModel::tableName().'.market_id 
							AND '.EventOutcomesModel::tableName().'.game_id = '.$gameApi->game_id.'
							AND '.ApiGameMarketsModel::tableName().'.service_api = '.BetsapiService::SOURCE_BETSAPI)
						->execute();

					}
				}
			}
			if($gameApi === null){
				if(($tournament_id = $this->getTournament([
						'sports_id' => $params['sports_id'],
						'code_api' => $params['event']['league']['id'],
						'service_api' => $params['service_api'],
						'name' => $params['event']['league']['name'],
					])) === null){
						$transaction->commit();
						Yii::warning('Event id = '.$params['event']['id'].'.Tournament error:'.Json::encode($params['event']['league']));
						return null;
					}

				$params['event']['home']['name'] = BaseInflector::transliterate($params['event']['home']['name']);
				$params['event']['away']['name'] = BaseInflector::transliterate($params['event']['away']['name']);

				$participants = [];
				/*$eventResult = null;
				if($jsonDataResult = file_get_contents('https://api.betsapi.com/v1/bet365/result?token='.\Yii::$app->params['bet365.token'].'&event_id='.$params['event']['id'])){
					$dataResult =  Json::decode($jsonDataResult);
					$eventResult = $dataResult['results'][0];
				}*/

				if(($participant_id = $this->getParticipant([
					'sports_id' => $params['sports_id'],
					'name' => $params['event']['home']['name'],
					'code_api' => $params['event']['home']['id'],
					'service_api' => $params['service_api'],
					//'image' => (!empty($eventResult['home']) && !empty($eventResult['home']['image_id']) ) ? (int)$eventResult['home']['image_id'] : null
					'image' => null
				])) !== null){
					$participants[1] = $participant_id;
				} else {
					$transaction->commit();
					Yii::warning('Event id = '.$params['event']['id'].'.Participant error:'.Json::encode($params['event']['home']));
					return null;
				}
				if(($participant_id = $this->getParticipant([
					'sports_id' => $params['sports_id'],
					'name' => $params['event']['away']['name'],
					'code_api' => $params['event']['away']['id'],
					'service_api' => $params['service_api'],
					//'image' => (!empty($eventResult['home']) && !empty($eventResult['away']['image_id']) ) ? (int)$eventResult['away']['image_id'] : null
					'image' => null
				])) !== null){
					$participants[2] = $participant_id;
				} else {
					$transaction->commit();
					Yii::warning('Event id = '.$params['event']['id'].'.Participant error:'.Json::encode($params['event']['away']));
					return null;
				}

				//exec('php yii.php bet365/participant-image --event_id='.$params['event']['id'].' --participant_home_id='.$participants[1].' --participant_away_id='.$participants[2].' > /dev/null 2>/dev/null &');

				$eventName = $params['event']['home']['name'] . ' - ' . $params['event']['away']['name'];

				if(($game_id = $this->findGameByParticipants([
					'participants' => $participants,
					'sports_id' => $params['sports_id'],
					'tournament_id' => $tournament_id,
					'starts_at' => $params['event']['time'],
				])) !== null){
					$this->createApiGame([
						'game_id' => $game_id,
						'sports_id' => $params['sports_id'],
						'tournament_id' => $tournament_id,
						'service_api' => $params['service_api'],
						'code_api' => $params['event']['id'],
						'api_event_id' => $params['event']['our_event_id'],
					]);
				} else {
					$game_id = $this->createGame([
						'title' => $eventName,
						'starts_at' => $params['event']['time'],
						'lang' => Yii::$app->language,
						'service_api' => $params['service_api'],
						'code_api' => $params['event']['id'],
						'sports_id' => $params['sports_id'],
						'tournament_id' => $tournament_id,
						'participants' => $participants,
						'api_event_id' => $params['event']['our_event_id'],
						'is_live' => $params['is_live'],
						'is_enabled' =>  $params['is_enabled'] ?? EventGameModel::STATUS_ENABLED,
					]);
				}
			} else {
			$game_id = $gameApi->game_id;
				if($gameApi->api_event_id == 0 && (int)$params['event']['our_event_id'] > 0){
					$gameApi->api_event_id = (int)$params['event']['our_event_id'];
					$gameApi->save(false);
				}
				$eventGame = EventGameModel::findOne($gameApi->game_id);
				$eventGame->scenario = \backend\modules\games\models\EventGameModel::SCENARIO_UPDATE;
				if($eventGame->starts_at != $params['event']['time'] &&  (int)$params['event']['time'] > 0){
					$eventGame->starts_at = (int)$params['event']['time'];
					$eventGame->save(false);
				}
			}
			$transaction->commit();
			return $game_id;
		} catch (\Throwable $e) {
			Yii::error($e);
			$transaction->rollBack();
		}
		return null;
	}

	protected function getTournament($params){
		if(($tournament_id = $this->findTournament($params['service_api'], $params["code_api"])) !== null){
			return $tournament_id;
		} else {
			if(($tournament_id = $this->getTournamentByName([
				'name' => $params["name"],
				'code_api' => $params["code_api"],
				'sports_id' => $params["sports_id"],
				'service_api' => $params["service_api"],
			])) !== null){
				return $tournament_id;
			}
		}
		return null;
	}

	protected function getTournamentByName($params) {
		if(($tournament_id = $this->findTournamentByName([
				'name' => $params["name"],
				'sports_id' => $params["sports_id"],
				'lang' => Yii::$app->language,
			])) !== null){
				$this->createApiTournament([
					'tournament_id' => $tournament_id,
					'service_api' => $params['service_api'],
					'code_api' => $params["code_api"],
				]);
			return $tournament_id;
		} else {
			$tournament_id = $this->createTournament([
				'title' => $params["name"],
				'lang' => Yii::$app->language,
				'service_api' => $params['service_api'],
				'code_api' => $params["code_api"],
				'sports_id' => $params["sports_id"],
			]);
			return $tournament_id;
		}
		return null;
	}

	protected function getParticipant($params) {
		if(($participantApi = $this->findParticipant($params['service_api'], $params['code_api'])) !== null){
			/*$participant = GameParticipantsModel::findOne($participantApi->participant_id);
			if(empty($participant->image) && !empty($params['image'])){
				$this->setApiParticipantImage([
					'sports_id' => $participant->sports_id,
					'participant_id' => $participant->id,
					'service_api' => $params['service_api'],
					'code_image' => (int)$params['image'],
				]);
			}*/
			return $participantApi->participant_id;
		} else {
			if(($participantLocale = $this->findParticipantByName([
				'name' => trim($params['name']),
				'sports_id' => $params['sports_id'],
				'lang' => Yii::$app->language,
			])) !== null){
				$this->createApiParticipant([
					'participant_id' => $participantLocale->participant_id,
					'service_api' => $params['service_api'],
					'code_api' => $params['code_api'],
				]);
				/*$participant = GameParticipantsModel::findOne($participantLocale->participant_id);
				if(empty($participant->image) && !empty($params['image'])){
					$this->setApiParticipantImage([
						'sports_id' => $participant->sports_id,
						'participant_id' => $participant->id,
						'service_api' => $params['service_api'],
						'code_image' => (int)$params['image'],
					]);
				}*/
				return $participantLocale->participant_id;
			} else {
				$participant_id = $this->createParticipant([
					'name' => trim($params['name']),
					'lang' => Yii::$app->language,
					'service_api' => $params['service_api'],
					'code_api' => $params['code_api'],
					'sports_id' => $params['sports_id'],
				]);
				/*if(!empty($params['image'])){
					$this->setApiParticipantImage([
						'sports_id' => $params['sports_id'],
						'participant_id' => $participant_id,
						'service_api' => $params['service_api'],
						'code_image' => (int)$params['image'],
					]);
				}*/
				return $participant_id;
			}
		}
		return null;
	}
}

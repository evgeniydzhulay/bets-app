<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class CricketBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		if(!empty($this->game['ss']) && strpos($this->game['ss'], '-') !== false ){
			$scoreFullTime = explode('-',$this->game['ss']);
			$scoreFullTime[0] = (int)$scoreFullTime[0];
			$scoreFullTime[1] = (int)$scoreFullTime[1];
			$this->resultBetsItemsPrepare([
				'code_api' => 'draw_no_bet',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'to_win_the_match',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'The Draw' : 'Away'),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_runs_in_match',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'double_chance',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Draw' : 'Away'),
			]);
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'draw_no_bet':
				$metodComparison = 'resultDrawNoBet';
			break;
			case 'to_win_the_match':
				$metodComparison = 'resultExactMatch';
			break;
			case 'total_runs_in_match':
				$metodComparison = 'resultTotalRunsInMatch';
			break;
			case 'double_chance':
				$metodComparison = 'resultMatchSearch';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTotalRunsInMatch($item, $value) {
		$itemValue = (float)filter_var($item['name'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		if( strpos($item['name'], 'Over') !== false ){
			return $itemValue < $value;
		} elseif( strpos($item['name'], 'Under') !== false ){
			return $itemValue > $value;
		}
	}

}

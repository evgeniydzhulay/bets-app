<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class BoxingUFCBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		if(!empty($this->game['ss']) && strpos($this->game['ss'], '-') !== false ){
			$scoreFullTime = explode('-',$this->game['ss']);
			$scoreFullTime[0] = (int)$scoreFullTime[0];
			$scoreFullTime[1] = (int)$scoreFullTime[1];
			$this->resultBetsItemsPrepare([
				'code_api' => 'to_win_fight',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
			]);
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'to_win_fight':
				$metodComparison = 'resultExactMatch';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

}

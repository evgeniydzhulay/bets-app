<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class BaseballBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		if(!empty($this->game['scores'])){
			$teamWithHighestScoringInnings = ['score' => 0,'team' => null];
			foreach($this->game['scores'] as $scoreKey => $score){
				if(isset($score['home']) && isset($score['away']) && (int)$scoreKey > 0 ){
					$score['home'] = (int)$score['home'];
					$score['away'] = (int)$score['away'];
					if($score['home'] > $score['away']){
						$teamWithHighestScoringInnings = ($score['home'] < $teamWithHighestScoringInnings['score']) ?: ['score' => $score['home'], 'team' => 'Home'];
					} elseif($score['home'] < $score['away']) {
						$teamWithHighestScoringInnings = ($score['away'] < $teamWithHighestScoringInnings['score']) ?: ['score' => $score['away'], 'team' => 'Away'];
					} else {
						$teamWithHighestScoringInnings = ($score['home'] < $teamWithHighestScoringInnings['score']) ?: ['score' => $score['home'], 'team' => 'Tie'];
					}
				}
				switch($scoreKey){
					//Baseball - Set 1
					case 1:
						if(isset($score['home']) && isset($score['away'])){
							$this->resultBetsItemsPrepare([
								'code_api' => '1st_innings_winner',
								'value' => $score['home'] > $score['away'] ? 'Home' : ($score['home'] == $score['away'] ? 'Tie' : 'Away'),
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'a_run_in_the_1st_innings',
								'value' => ($score['home'] + $score['away']) > 0 ? 'Yes' : 'No',
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '1st_innings_runs',
								'value' => $score['home'] + $score['away'],
							]);
						}
					break;
					//Baseball - Set 2
					case 2:
					break;
					//Baseball - Set 3
					case 3:
						if(isset($score['home']) && isset($score['away'])
							&& isset($this->game['scores'][1]['home']) && isset($this->game['scores'][1]['away'])
							&& isset($this->game['scores'][2]['home']) && isset($this->game['scores'][2]['away'])
						){
							$score3Innings = [
								0 => $score['home'] + (int)$this->game['scores'][1]['home'] + (int)$this->game['scores'][2]['home'],
								1 => $score['away'] + (int)$this->game['scores'][1]['away'] + (int)$this->game['scores'][2]['away'],
							];
							$this->resultBetsItemsPrepare([
								'code_api' => '3_innings_line',
								'value' => $score3Innings,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '3_innings_total',
								'value' => $score3Innings[0] + $score3Innings[1],
							]);
						}
					break;
					//Baseball - Set 4
					case 4:
					break;
					//Baseball - Set 5
					case 5:
						if(!empty($score3Innings) && isset($score['home']) && isset($score['away'])
							&& isset($this->game['scores'][4]['home']) && isset($this->game['scores'][4]['away'])
						){
							$score4_1_2 = [
								0 => $score3Innings[0] + (int)$this->game['scores'][4]['home'] + $score['home'],
								1 => $score3Innings[1] + (int)$this->game['scores'][4]['away'] + $score['away'],
							];
							$this->resultBetsItemsPrepare([
								'code_api' => '4_1_2_innings_line',
								'value' => $score4_1_2,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '4_1_2_innings_total',
								'value' => $score4_1_2[0] + $score4_1_2[1],
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '5_innings_line',
								'value' => $score4_1_2,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '5_innings_total',
								'value' => $score4_1_2[0] + $score4_1_2[1],
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'alternative_5_innings_line',
								'value' => $score4_1_2,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'alternative_5_innings_total',
								'value' => $score4_1_2[0] + $score4_1_2[1],
							]);
						}
					break;
					//Baseball - Set 6
					case 6:
					break;
					//Baseball - Set 7
					case 7:
						if(!empty($score4_1_2) && isset($score['home']) && isset($score['away'])
							&& isset($this->game['scores'][6]['home']) && isset($this->game['scores'][6]['away'])
						){
							$score7Innings = [
								0 => $score4_1_2[0] + (int)$this->game['scores'][6]['home'] + $score['home'],
								1 => $score4_1_2[1] + (int)$this->game['scores'][6]['away'] + $score['away'],
							];
							$this->resultBetsItemsPrepare([
								'code_api' => '7_innings_run_line',
								'value' => $score7Innings,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '7_innings_total',
								'value' => $score7Innings[0] + $score7Innings[1],
							]);
						}
					break;
					//Baseball - Set 8
					case 8:
					break;
					//Baseball - Set 9
					case 9:
					break;
					//Baseball - hit - удар
					case 'hit':
						if(isset($score['home']) && isset($score['away'])){
							$this->resultBetsItemsPrepare([
								'code_api' => 'team_hits',
								'value' => array_values($score),
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'total_hits',
								'value' => $score['home'] + $score['away'],
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'most_hits',
								'value' => $score['home'] > $score['away'] ? 'Home' : ($score['home'] == $score['away'] ? 'Tie' : 'Away'),
							]);
						}
					break;
					//Baseball - ot
					case 'ot':
					break;
					//Baseball - run (1+2+3+4+5+6+7+8+9)
					case 'run':
						if(isset($score['home']) && isset($score['away'])){
							$scoreFullTime = array_values($score);
							$this->resultBetsItemsPrepare([
								'code_api' => 'money_line',
								'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'game_totals',
								'value' => $scoreFullTime[0] + $scoreFullTime[1],
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'run_line',
								'value' => $scoreFullTime,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '3_way_run_line',
								'value' => $scoreFullTime,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '3_way_total',
								'value' => $scoreFullTime[0] + $scoreFullTime[1],
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'game_totals_bands',
								'value' => $scoreFullTime[0] + $scoreFullTime[1],
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'alternative_run_line',
								'value' => $scoreFullTime,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'alternative_game_total',
								'value' => $scoreFullTime[0] + $scoreFullTime[1],
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'alternative_3_way_run_line',
								'value' => $scoreFullTime,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'alternative_3_way_total',
								'value' => $scoreFullTime[0] + $scoreFullTime[1],
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'game_total_odd_even',
								'value' => ($scoreFullTime[0] + $scoreFullTime[1]) % 2 == 0 ? 'Even' : 'Odd',
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'winning_margins',
								'value' => [
									'player' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
									'margin' => abs($scoreFullTime[0] - $scoreFullTime[1])
								]
							]);
							if(!empty($score4_1_2)){
								$score4_1_2Total = $score4_1_2[0] + $score4_1_2[1];
								$score2PartGameTotal = $scoreFullTime[0] + $scoreFullTime[1] - $score4_1_2Total;
								$this->resultBetsItemsPrepare([
									'code_api' => 'highest_scoring_period',
									'value' => $score4_1_2Total > $score2PartGameTotal ? 'First 5 Innings' : ($score4_1_2Total == $score2PartGameTotal ? 'Tie' : 'Rest Of The Game'),
								]);
							}
							$this->resultBetsItemsPrepare([
								'code_api' => 'team_totals',
								'value' =>  $scoreFullTime,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'team_total_odd_even',
								'value' =>  $scoreFullTime,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'team_1_total',
								'value' =>  $scoreFullTime,
							]);
						}
					break;
				}
			}

			$this->resultBetsItemsPrepare([
				'code_api' => 'extra_innings',
				'value' =>  !empty($this->game['scores']['ot']) ? 'Yes' : 'No'
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_with_highest_scoring_innings',
				'value' => $teamWithHighestScoringInnings['team']
			]);
			if(!empty($score4_1_2) && !empty($scoreFullTime) ){
				$this->resultBetsItemsPrepare([
					'code_api' => 'double_result',
					'value' => [
						'score4_1_2' => $score4_1_2[0] > $score4_1_2[1] ? 'Home' : ($score4_1_2[0] == $score4_1_2[1] ? 'Tie' : 'Away'),
						'fullTime' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
					]
				]);
			}

		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'money_line':
			case '1st_innings_winner':
			case 'a_run_in_the_1st_innings':
			case 'extra_innings':
			case 'team_with_highest_scoring_innings':
			case 'game_total_odd_even':
			case 'highest_scoring_period':
			case 'most_hits':
				$metodComparison = 'resultExactMatch';
			break;
			case 'game_totals':
			case '4_1_2_innings_total':
			case '3_innings_total':
			case '5_innings_total':
			case '7_innings_total':
			case 'total_hits':
				$metodComparison = 'resultTotalsByName';
			break;
			case 'run_line':
			case '4_1_2_innings_line':
			case '3_innings_line':
			case '5_innings_line':
			case '7_innings_run_line':
				$metodComparison = 'resultMatchHandicap';
			break;
			case '1st_innings_runs':
				$metodComparison = 'resultTotalsOnlyName';
			break;
			case '3_way_run_line':
			case 'alternative_3_way_run_line':
				$metodComparison = 'resultHandicap3Way';
			break;
			case '3_way_total':
				$metodComparison = 'resultOverUnderByName';
			break;
			case 'game_totals_bands':
				$metodComparison = 'resultGameTotalsBands';
			break;
			case 'alternative_run_line':
			case 'alternative_5_innings_line':
				$metodComparison = 'resultHandicap2Way';
			break;
			case 'alternative_game_total':
			case 'alternative_5_innings_total':
				$metodComparison = 'resultTotalsByHheader';
			break;
			case 'alternative_3_way_total':
				$metodComparison = 'resultOverUnderByHeader';
			break;
			case 'double_result':
				$metodComparison = 'resultBaseballDoubleResult';
			break;
			case 'winning_margins':
				$metodComparison = 'resultWinningMarginExact';
			break;
			case 'team_totals':
			case 'team_hits':
			case 'team_1_total':
				$metodComparison = 'resultPlayerTotal';
			break;
			case 'team_total_odd_even':
				$metodComparison = 'resultTeamTotalOddEven';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultGameTotalsBands($item, $value) {
		if( strpos($item['name'], 'Or Less') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue >= $value;
		}elseif( strpos($item['name'], 'Or More') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue <= $value;
		} else {
			$itemValues = explode('Or', $item['name']);
			return (int)preg_replace("/[^0-9]/", '', $itemValues[0]) == $value or (int)preg_replace("/[^0-9]/", '', $itemValues[1]) == $value;
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultBaseballDoubleResult($item, $value) {
		if($value['fullTime'] == 'Tie'){
			return self::RESULT_STATUS_REFUND;
		}
		$itemValues = explode('-', $item['name']);
		return strpos($itemValues[0], $value['score4_1_2']) && strpos($itemValues[1], $value['fullTime']);
	}
}

<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class HandballBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		$scoreFullTime = null;

		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				switch($scoreKey){
					//Handball - First Half
					case 1:
						$totalGoals1st = $score['home'] + $score['away'];
						$this->resultBetsItemsPrepare([
							'code_api' => 'half_time_result',
							'value' => array_values($score),
						]);
					break;
					//Handball - Second Half
					case 2:
						$scoreFullTime = [
							0 => $this->game['scores'][1]['home'] + $score['home'],
							1 => $this->game['scores'][1]['away'] + $score['away']
						];
						$this->resultBetsItemsPrepare([
							'code_api' => 'draw_no_bet',
							'value' => $scoreFullTime,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'match_handicap',
							'value' => $scoreFullTime,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'totals',
							'value' => $scoreFullTime[0] + $scoreFullTime[1],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'money_line_3_way',
							'value' => $scoreFullTime,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'handicap_3_way',
							'value' => $scoreFullTime,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'double_chance',
							'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'goals_odd_even',
							'value' => ($scoreFullTime[0] + $scoreFullTime[1]) % 2 == 0 ? 'Even' : 'Odd',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'team_total_odd_even',
							'value' => $scoreFullTime,
						]);

					break;
					//Handball - ET/penalties
					case 3:
					break;
					//Handball - Full time + ET (1+2+3)
					case 4:
					break;
				}
			}
		}

		if(!empty($this->game['stats'])){
			foreach($this->game['stats'] as $statType => $statData){
				switch($statType){
					case 'last_10_mins_score':
					break;
					case 'possession':
					break;
				}
			}
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'draw_no_bet':
				$metodComparison = 'resultDrawNoBet';
			break;
			case 'match_handicap':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'totals':
				$metodComparison = 'resultTotalsByName';
			break;
			case 'money_line_3_way':
				$metodComparison = 'resultMoneyLine3Way';
			break;
			case 'handicap_3_way':
				$metodComparison = 'resultHandicap3WayByName';
			break;
			case 'double_chance':
				$metodComparison = 'resultMatchSearch';
			break;
			case 'goals_odd_even':
				$metodComparison = 'resultExactMatch';
			break;
			case 'team_total_odd_even':
				$metodComparison = 'resultTeamTotalOddEven';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 *
	 * Tie: handicap (+/-) for Away
	 */
	protected function resultHandicap3WayByName($item, $value) {
		if( strpos($item['name'], 'Home') !== false ){
			return ($value[0] + $item['handicap']) > $value[1];
		} elseif( strpos($item['name'], 'Away') !== false ){
			return $value[0] < ($value[1] + $item['handicap']);
		} elseif( strpos($item['name'], 'Tie') !== false ){
			return $value[0] == ($value[1] + $item['handicap']);
		}
		return null;
	}
}

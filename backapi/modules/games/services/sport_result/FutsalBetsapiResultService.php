<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class FutsalBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				switch($scoreKey){
					//Futsal - 1st Half
					case 1:
					break;
					//Futsal - 2nd Half
					case 2:
					break;
				}
			}
		}

		if(!empty($this->game['ss']) && strpos($this->game['ss'], '-') !== false ){
			$scoreFullTime = explode('-',$this->game['ss']);
			$scoreFullTime[0] = (int)$scoreFullTime[0];
			$scoreFullTime[1] = (int)$scoreFullTime[1];
			$this->resultBetsItemsPrepare([
				'code_api' => 'handicap_betting',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'game_totals',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'draw_no_bet',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'money_line',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'handicap_3_way',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'game_total_odd_even',
				'value' => ($scoreFullTime[0] + $scoreFullTime[1]) % 2 == 0 ? 'Even' : 'Odd',
			]);
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'draw_no_bet':
				$metodComparison = 'resultDrawNoBet';
			break;
			case 'money_line':
			case 'game_total_odd_even':
				$metodComparison = 'resultExactMatch';
			break;
			case 'handicap_betting':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'game_totals':
				$metodComparison = 'resultTotalsByName';
			break;
			case 'handicap_3_way':
				$metodComparison = 'resultHandicap3Way';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

}

<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class IceHockeyBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){
		$scoreFullTime = null;
		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				switch($scoreKey){
					//Ice Hockey - 1st Period
					case 1:
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_period_puck_line_2_way',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_period_totals',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_period_money_line_2_way',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_period_puck_line_3_way',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_period_total_3_way',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_period_money_line_3_way',
							'value' => array_values($score),
						]);
					break;
					//Ice Hockey - 2nd Period
					case 2:
					break;
					//Ice Hockey - 3rd Period
					case 3:
					break;
					//Ice Hockey - over time
					case 4:
					break;
					//Ice Hockey - Full Time (1+2+3+4)
					case 5:
						$scoreFullTime = array_values($score);
						$this->resultBetsItemsPrepare([
							'code_api' => 'puck_line',
							'value' => $scoreFullTime,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'game_totals',
							'value' => $scoreFullTime[0] + $scoreFullTime[1],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'money_line_3_way',
							'value' => $scoreFullTime,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'puck_line_3_way',
							'value' => $scoreFullTime,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'total_3_way',
							'value' => $scoreFullTime[0] + $scoreFullTime[1],
						]);
					break;
				}
			}
		}

		if(!empty($this->game['stats'])){
			foreach($this->game['stats'] as $statType => $statData){
				switch($statType){
					case 'shots':
					break;
					case 'penalties':
					break;
					case 'goals_on_power_play':
					break;
					case 's7':
					break;
				}
			}
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'puck_line':
			case '1st_period_puck_line_2_way':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'game_totals':
			case '1st_period_totals':
				$metodComparison = 'resultTotalsByName';
			break;
			case 'money_line_3_way':
			case '1st_period_money_line_3_way':
				$metodComparison = 'resultMoneyLine3Way';
			break;
			case 'puck_line_3_way':
			case '1st_period_puck_line_3_way':
				$metodComparison = 'resultHandicap3Way';
			break;
			case 'total_3_way':
			case '1st_period_total_3_way':
				$metodComparison = 'resultOverUnderByName';
			break;
			case '1st_period_money_line_2_way':
				$metodComparison = 'resultDrawNoBet';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

}

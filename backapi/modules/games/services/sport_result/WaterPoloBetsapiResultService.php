<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class WaterPoloBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		$scoreFullTime = null;

		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){

			}
		}

		if(!empty($this->game['ss']) && strpos($this->game['ss'], '-') !== false ){
			$scoreFullTime = explode('-',$this->game['ss']);
			$scoreFullTime[0] = (int)$scoreFullTime[0];
			$scoreFullTime[1] = (int)$scoreFullTime[1];
			$this->resultBetsItemsPrepare([
				'code_api' => 'handicap_betting',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_goals_2_way',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'fulltime_result',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 1 : ($scoreFullTime[0] == $scoreFullTime[1] ? 'X' : 2),
			]);
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'fulltime_result':
				$metodComparison = 'resultExactMatch';
			break;
			case 'handicap_betting':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'total_goals_2_way':
				$metodComparison = 'resultTotalsByName';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

}

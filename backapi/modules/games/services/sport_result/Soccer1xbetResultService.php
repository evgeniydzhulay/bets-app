<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class Soccer1xbetResultService extends BetsapiBaseResultService {

	public function processingResults(){

		$scoreFullTime = null;

		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				switch($scoreKey){
					//soccer - 1st time
					case 1:
						$totalGoals1st = $score['home'] + $score['away'];
					break;
					//soccer - Full Time Score
					case 2:
						$scoreFullTime = array_values($score);

					break;
					//soccer - Extra Time Score
					case 3:
					break;
					//soccer - penalties
					case 4:
					break;
				}
			}

		}

		if(!empty($scoreFullTime)){
			$this->resultBetsItemsPrepare([
				'code_api' => '1', //1x2 (OT)
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 1 : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Draw' : 2),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '2766', //1X2 In Regular Time
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 1 : ($scoreFullTime[0] == $scoreFullTime[1] ? 'X In Regular Time' : 2),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '8', // Double Chance
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 1 : ($scoreFullTime[0] == $scoreFullTime[1] ? 'X' : 2),
			]);
		}

		if(!empty($this->game['stats'])){
			foreach($this->game['stats'] as $statType => $statData){
				switch($statType){
					case 'attacks':
					break;
					case 'ball_safe':
					break;
					case 'corners':
						$totalCorners = $statData[0] + $statData[1];
					break;
					case 'corner_f':
					break;
					case 'corner_h':
					break;
					case 'dangerous_attacks':
					break;
					case 'fouls':
					break;
					case 'goalattempts':
					break;
					case 'goals':

					break;
					case 'injuries':
					break;
					case 'offsides':
					break;
					case 'off_target':
					break;
					case 'on_target':
					break;
					case 'penalties':

					break;
					case 'possession_rt':
					break;
					case 'redcards':
					break;
					case 'saves':
					break;
					case 'shots_blocked':
					break;
					case 'substitutions':
					break;
					case 'yellowcards':
					break;
				}
			}
		}

		if(!empty($this->game['events'])){
			$resultEvents = [
				'first_team_to_score' => 'No Goals',
				'time_of_first_goal_brackets' => 'No Goal',
				'own_goal' => 'No',
				'first_card_received' => 'No Card',
				'total_goal_minutes' => 0,
				'goals_by_time' => [],
				'time_of_1st_team_goal' => [],
				'last_team_to_score' => 'No Goal',
				'a_red_card_in_the_match' => 'No'
			];

			foreach($this->game['events'] as $event){
				//ET - extra time
				if( strpos($event['text'], ' ET ') === false){
					if( strpos($event['text'], '1st Goal') !== false){
						if( strpos($event['text'], $this->game['home']['name']) !== false){
							$resultEvents['first_team_to_score'] = 'Home';
						} elseif( strpos($event['text'], $this->game['away']['name']) !== false){
							$resultEvents['first_team_to_score'] = 'Away';
						}

						if( strpos($event['text'],"' -") !== false){
								$resultEvents['time_of_first_goal_brackets'] = (int)explode("' -",$event['text'])[0];
						}
					}
					if( strpos($event['text'], 'Goal') !== false){
						if( strpos($event['text'], $this->game['home']['name']) !== false){
							$resultEvents['last_team_to_score'] = 'Home';
						} elseif( strpos($event['text'], $this->game['away']['name']) !== false){
							$resultEvents['last_team_to_score'] = 'Away';
						}
						if( strpos($event['text'],"' -") !== false){
							$goal_minute = (int)explode("' -",$event['text'])[0];
							$resultEvents['total_goal_minutes'] += $goal_minute;
							$resultEvents['goals_by_time'][] = $goal_minute;
							if( strpos($event['text'], $this->game['home']['name']) !== false && empty($resultEvents['time_of_1st_team_goal']['home'])){
								$resultEvents['time_of_1st_team_goal']['home'] = $goal_minute;
							} elseif( strpos($event['text'], $this->game['away']['name']) !== false && empty($resultEvents['time_of_1st_team_goal']['away'])){
								$resultEvents['time_of_1st_team_goal']['away'] = $goal_minute;
							}
						}
						if( strpos($event['text'],"00:00 - 09:59") !== false){
							$goals_score = explode("Goal",$event['text'])[0];
							list($homeGoals,$awayGoals) = explode(':',$goals_score);
							$this->resultBetsItemsPrepare([
								'code_api' => 'first_10_minutes_(00:00_09:59)',
								'value' => (int)$homeGoals + (int)$awayGoals,
							]);
						}
					}
					if( strpos($event['text'], 'Own Goal') !== false){
						$resultEvents['own_goal'] = 'Yes';
					}

					if( $resultEvents['first_card_received'] == 'No Card' && (strpos($event['text'], '1st Yellow Card') !== false or strpos($event['text'], '1st Red Card') !== false)){
						if( strpos($event['text'], $this->game['home']['name']) !== false){
							$resultEvents['first_card_received'] = 'Home';
						} elseif( strpos($event['text'], $this->game['away']['name']) !== false){
							$resultEvents['first_card_received'] = 'Away';
						}
					}

					if( strpos($event['text'], 'Red Card') !== false){
						$resultEvents['a_red_card_in_the_match'] = 'Yes';
					}
				}
			}


		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case '1': //1x2
			case '2766': //1X2 In Regular Time
			case '8': //Double Chance
				$metodComparison = 'resultMatchSearch';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

}

<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class Basketball1xbetResultService extends BetsapiBaseResultService {

	public function processingResults(){

		if(!empty($this->game['scores'])){
			$quarterCorrectScore = ['home' => 0,'away' => 0];
			$highestScoringQuarter = ['total' => 0,'quarter' => null];
			$teamHighestScoringQuarter = ['score' => 0,'team' => null];
			foreach($this->game['scores'] as $scoreKey => $score){
				switch($scoreKey){
					//Basketball - 1st Quarter
					case 1:
					break;
					//Basketball - 2nd Quarter
					case 2:
					break;
					//Basketball - Half (1+2)
					case 3:
					break;
					//Basketball - 3rd Quarter
					case 4:
					break;
					//Basketball - 4th Quarter
					case 5:
					break;
					//Basketball - overtime
					case 6:
					break;
					//Basketball - Full time (1+2+4+5+6)
					case 7:
						$scoreFullTime = array_values($score);
					break;
				}
			}

			if(!empty($scoreFullTime)){
				$this->resultBetsItemsPrepare([
					'code_api' => '1', //1x2 (OT)
					'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 1 : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Draw' : 2),
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => '2766', //1X2 In Regular Time
					'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 1 : ($scoreFullTime[0] == $scoreFullTime[1] ? 'X In Regular Time' : 2),
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => '8', // Double Chance
					'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 1 : ($scoreFullTime[0] == $scoreFullTime[1] ? 'X' : 2),
				]);
			}

		}


		if(!empty($this->game['stats'])){
			foreach($this->game['stats'] as $statType => $statData){
				switch($statType){
					case '2points':
					break;
					case '3points':
					break;
					case 'biggest_lead':
					break;
					case 'fouls':
					break;
					case 'free_throws':
					break;
					case 'free_throws_rate':
					break;
					case 'lead_changes':
					break;
					case 'maxpoints_inarow':
					break;
					case 'possession':
					break;
					case 'success_attempts':
					break;
					case 'timespent_inlead':
					break;
					case 'time_outs':
					break;
				}
			}
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}
	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case '1': //1x2 (OT)
			case '2766': //1X2 In Regular Time
			case '8': //Double Chance
				$metodComparison = 'resultMatchSearch';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

}

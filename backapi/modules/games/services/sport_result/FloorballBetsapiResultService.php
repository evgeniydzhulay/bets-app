<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class FloorballBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				switch($scoreKey){
					//Floorball - Set 1
					case 1:
					break;
					//Floorball - Set 2
					case 2:
					break;
					//Floorball - Set 3
					case 3:
					break;
					//Floorball - Full time
					case 4:
					break;
				}
			}
		}

		if(!empty($this->game['ss']) && strpos($this->game['ss'], '-') !== false ){
			$scoreFullTime = explode('-',$this->game['ss']);
			$scoreFullTime[0] = (int)$scoreFullTime[0];
			$scoreFullTime[1] = (int)$scoreFullTime[1];
			$this->resultBetsItemsPrepare([
				'code_api' => 'handicap',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'totals',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'money_line_3_way',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Draw' : 'Away'),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'money_line_2_way',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'double_chance',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Draw' : 'Away'),
			]);
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'money_line_3_way':
				$metodComparison = 'resultExactMatch';
			break;
			case 'money_line_2_way':
				$metodComparison = 'resultDrawNoBet';
			break;
			case 'totals':
				$metodComparison = 'resultTotalsByName';
			break;
			case 'handicap':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'double_chance':
				$metodComparison = 'resultMatchSearch';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

}

<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class AustralianRulesBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){
		$scoreFullTime = null;
		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				switch($scoreKey){
					//AustralianRules - Set 1
					case 1:
					break;
					//AustralianRules - Set 2
					case 2:
					break;
					//AustralianRules - Set 3
					case 3:
					break;
					//AustralianRules - Set 4
					case 4:
					break;
				}
			}
		}

		if(!empty($this->game['ss']) && strpos($this->game['ss'], '-') !== false ){
			$scoreArr = explode('-',$this->game['ss']);
			if( strpos($this->game['ss'], '(') !== false ){
				preg_match('#\((.*?)\)#', $scoreArr[0], $match);
				$scoreFullTime[0] = (int)$match[1];
				preg_match('#\((.*?)\)#', $scoreArr[1], $match);
				$scoreFullTime[1] = (int)$match[1];
				$homeGoals = (int)explode('.',$scoreArr[0])[0];
				$awayGoals = (int)explode('.',$scoreArr[1])[0];
				$this->resultBetsItemsPrepare([
					'code_api' => 'match_goals',
					'value' => $homeGoals + $awayGoals,
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'team_total_goals',
					'value' => [$homeGoals,$awayGoals],
				]);

			} else {
				$scoreFullTime[0] = (int)$scoreArr[0];
				$scoreFullTime[1] = (int)$scoreArr[1];
			}
			$this->resultBetsItemsPrepare([
				'code_api' => 'to_win',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'handicap',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'totals',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_handicaps',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_match_total',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_5_way',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'winning_margin',
				'value' => [
					'player' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
					'margin' => abs($scoreFullTime[0] - $scoreFullTime[1])
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'start_+24.5',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'start_+39.5',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_total_points',
				'value' =>  $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'both_teams_to_score_(points)',
				'value' =>  $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'game_total_odd_even',
				'value' => ($scoreFullTime[0] + $scoreFullTime[1]) % 2 == 0 ? 'Even' : 'Odd',
			]);
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'to_win':
				$metodComparison = 'resultDrawNoBet';
			break;
			case 'handicap':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'totals':
			case 'match_goals':
				$metodComparison = 'resultTotalsByName';
			break;
			case 'alternative_handicaps':
				$metodComparison = 'resultHandicap2Way';
			break;
			case 'alternative_match_total':
				$metodComparison = 'resultTotalsByHheader';
			break;
			case 'total_5_way':
			case 'match_goals_(bands)':
				$metodComparison = 'resultMatchRange';
			break;
			case 'winning_margin':
				$metodComparison = 'resultWinningMargin';
			break;
			case 'start_+24.5':
			case 'start_+39.5':
				$metodComparison = 'resultStartPlus';
			break;
			case 'team_total_points':
			case 'team_total_goals':
				$metodComparison = 'resultPlayerTotal';
			break;
			case 'both_teams_to_score_(points)':
				$metodComparison = 'resultBothTeamsScore';
			break;
			case 'team_goals_(bands)':
				$metodComparison = 'resultMatchRangePlayer';
			break;
			case 'game_total_odd_even':
				$metodComparison = 'resultExactMatch';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultMatchRange($item, $value) {
		if( strpos($item['name'], '+') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue <= $value;
		} else {
			$itemValues = explode('-', $item['name']);
			return (int)$itemValues[0] <= $value && (int)$itemValues[1] >= $value;
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultStartPlus($item, $value) {
		$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
		if( strpos($item['name'], 'Home') !== false){
			return ($value[0] + $itemValue) > $value[1];
		} elseif( strpos($item['header'], 'Away') !== false ){
			return $value[0] < ($value[1] + $itemValue);
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultBothTeamsScore($item, $value) {
		$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
		if( strpos($item['name'], 'Yes') !== false ){
			return $itemValue <= $value[0] && $itemValue <= $value[1];
		} elseif( strpos($item['name'], 'No') !== false ){
			return $itemValue > $value[0] or $itemValue > $value[1];
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultMatchRangePlayer($item, $value) {
		if( strpos($item['header'], 'Home') !== false ){
			$playerValue = $value[0];
		} elseif( strpos($item['header'], 'Away') !== false ){
			$playerValue = $value[1];
		} else {
			return null;
		}

		if( strpos($item['name'], '+') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue <= $playerValue;
		} else {
			$itemValues = explode('-', $item['name']);
			return (int)$itemValues[0] <= $playerValue && (int)$itemValues[1] >= $playerValue;
		}
		return null;
	}
}

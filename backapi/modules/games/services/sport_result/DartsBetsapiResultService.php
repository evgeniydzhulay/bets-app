<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class DartsBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		if(!empty($this->game['ss']) && strpos($this->game['ss'], '-') !== false ){
			$scoreFullTime = explode('-',$this->game['ss']);
			$scoreFullTime[0] = (int)$scoreFullTime[0];
			$scoreFullTime[1] = (int)$scoreFullTime[1];
			$this->resultBetsItemsPrepare([
				'code_api' => 'match_winner',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'handicap_2_way',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_handicaps',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'correct_leg_score',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_legs',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_total_legs',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'match_winner':
				$metodComparison = 'resultExactMatch';
			break;
			case 'handicap_2_way':
			case 'alternative_handicaps':
				$metodComparison = 'resultHandicap2Way';
			break;
			case 'correct_leg_score':
				$metodComparison = 'resultCorrectSetScore';
			break;
			case 'total_legs':
			case 'alternative_total_legs':
				$metodComparison = 'resultTotalsByHeader';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

}

<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class SnookerBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		if(!empty($this->game['ss']) && strpos($this->game['ss'], '-') !== false ){
			$scoreFullTime = explode('-',$this->game['ss']);
			$scoreFullTime[0] = (int)$scoreFullTime[0];
			$scoreFullTime[1] = (int)$scoreFullTime[1];
			$this->resultBetsItemsPrepare([
				'code_api' => 'to_win_match',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'match_handicap',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_match_handicap',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_frames_2_way',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_match_total',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_frames_3_way',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_frames_odd_even',
				'value' => ($scoreFullTime[0] + $scoreFullTime[1]) % 2 == 0 ? 'Even' : 'Odd',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'correct_score',
				'value' => $scoreFullTime,
			]);

		}

		if(!empty($this->game['events'])){
			$resultEvents = [
				'1st_frame_highest_break' => 0,
				'1st_frame_highest_break_player' => [
					0 => 0,
					1 => 0,
				],
				'frame_winner' => [
					0 => 0,
					1 => 0,
				],
			];

			foreach($this->game['events'] as $event){

				if( strpos($event['text'], 'Frame 1') !== false){
					if( strpos($event['text'], 'Break') !== false){
						preg_match('/(?<break>\d+) Break/', $event['text'], $matches);
						if($resultEvents['1st_frame_highest_break'] < (int)$matches['break']){
							$resultEvents['1st_frame_highest_break'] = (int)$matches['break'];
						}
						if( strpos($event['text'], $this->game['home']['name']) !== false){
							if($resultEvents['1st_frame_highest_break_player'][0] < (int)$matches['break']){
								$resultEvents['1st_frame_highest_break_player'][0] = (int)$matches['break'];
							}
						} elseif( strpos($event['text'], $this->game['away']['name']) !== false){
							if($resultEvents['1st_frame_highest_break_player'][1] < (int)$matches['break']){
								$resultEvents['1st_frame_highest_break_player'][1] = (int)$matches['break'];
							}
						}
					} elseif( strpos($event['text'], 'Foul') !== false){
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_foul',
							'value' => (strpos($event['text'], 'Yes') === false) ? 'Yes' : 'No'
						]);
					} elseif( strpos($event['text'], '1st Colour') !== false){
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_first_colour',
							'value' => $event['text']
						]);
					} elseif( strpos($event['text'], 'Last Points') !== false){
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_last_points',
							'value' => $event['text']
						]);
					} elseif( strpos($event['text'], '1st Ball') !== false){
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_to_pot_first_ball',
							'value' => (strpos($event['text'], $this->game['home']['name']) !== false) ? 'Home' : 'Away'
						]);
					} elseif( strpos($event['text'], 'Pot Last Ball') !== false){
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_to_pot_last_ball',
							'value' => (strpos($event['text'], $this->game['home']['name']) !== false) ? 'Home' : 'Away'
						]);
					} elseif( strpos($event['text'], 'Race to 30') !== false){
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_race_to_30',
							'value' => (strpos($event['text'], $this->game['home']['name']) !== false) ? 'Home' : 'Away'
						]);
					} elseif( preg_match('/(?<home>\d+)-(?<away>\d+)/', $event['text'], $matches)){
						$score1Frame = [0 => $matches['home'], 1 => $matches['away']];
						$winning_margin = abs($score1Frame[0] - $score1Frame[1]);
						if($resultEvents['1st_frame_highest_break'] < $winning_margin){
							$resultEvents['1st_frame_highest_break'] = $winning_margin;
						}
						if($score1Frame[0] > $score1Frame[1]){
							if($resultEvents['1st_frame_highest_break_player'][0] < $winning_margin){
								$resultEvents['1st_frame_highest_break_player'][0] = $winning_margin;
							}
						} else {
							if($resultEvents['1st_frame_highest_break_player'][1] < $winning_margin){
								$resultEvents['1st_frame_highest_break_player'][1] = $winning_margin;
							}
						}
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_winner',
							'value' => $score1Frame[0] > $score1Frame[1] ? 'Home' : 'Away',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_handicap_2_way',
							'value' => $score1Frame,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_total_points_2_way',
							'value' => $score1Frame[0] + $score1Frame[1],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_winning_margin',
							'value' => [
								'player' => $score1Frame[0] > $score1Frame[1] ? 'Home' : 'Away',
								'margin' => $winning_margin
							]
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_total_points_odd_even',
							'value' => ($score1Frame[0] + $score1Frame[1]) % 2 == 0 ? 'Even' : 'Odd',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_frame_player_points_odd_even',
							'value' => $score1Frame,
						]);
					}

				}

				if( preg_match('/(?<home>\d+)-(?<away>\d+)/', $event['text'], $matches)){
					$scoreFrame = [0 => $matches['home'], 1 => $matches['away']];
					if($scoreFrame[0] > $scoreFrame[1]){
						$resultEvents['frame_winner'][0]++;
					} else {
						$resultEvents['frame_winner'][1]++;
					}
					//race to  3 frames
					if(($resultEvents['frame_winner'][0] == 3 && $resultEvents['frame_winner'][1] < 3)
						or ($resultEvents['frame_winner'][1] == 3 && $resultEvents['frame_winner'][0] < 3)){
						$this->resultBetsItemsPrepare([
							'code_api' => 'race_to_(frames)',
							'value' => [
								'player' => $resultEvents['frame_winner'][0] > $resultEvents['frame_winner'][1] ? 'Home' : 'Away',
								'frames' => 3
							]
						]);
					}
					if( strpos($event['text'], 'Frame 4') !== false){
						$this->resultBetsItemsPrepare([
							'code_api' => 'leader_after_1st_four_frames',
							'value' => $resultEvents['frame_winner'][0] > $resultEvents['frame_winner'][1] ? 'Home' : ($resultEvents['frame_winner'][0] == $resultEvents['frame_winner'][1] ? 'Draw' : 'Away'),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'score_after_1st_four_frames',
							'value' => $resultEvents['frame_winner'],
						]);
					}

				}

			}

			$this->resultBetsItemsPrepare([
				'code_api' => '1st_frame_a_break_of_50+',
				'value' => $resultEvents['1st_frame_highest_break'] >= 50 ? 'Yes' : 'No',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '1st_frame_a_break_of_100+',
				'value' => $resultEvents['1st_frame_highest_break'] >= 100 ? 'Yes' : 'No',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '1st_frame_highest_break_total',
				'value' => $resultEvents['1st_frame_highest_break']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '1st_frame_highest_break',
				'value' => $resultEvents['1st_frame_highest_break_player'][0] > $resultEvents['1st_frame_highest_break_player'][1] ? 'Home' : 'Away',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '1st_frame_player_breaks',
				'value' => $resultEvents['1st_frame_highest_break_player'],
			]);
		}
	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'to_win_match':
			case 'total_frames_odd_even':
			case '1st_frame_winner':
			case '1st_frame_a_break_of_50+':
			case '1st_frame_a_break_of_100+':
			case '1st_frame_highest_break':
			case '1st_frame_total_points_odd_even':
			case 'leader_after_1st_four_frames':
			case '1st_frame_foul':
			case '1st_frame_to_pot_first_ball':
			case '1st_frame_to_pot_last_ball':
			case '1st_frame_race_to_30':
				$metodComparison = 'resultExactMatch';
			break;
			case 'match_handicap':
			case 'alternative_match_handicap':
			case '1st_frame_handicap_2_way':
				$metodComparison = 'resultHandicap2Way';
			break;
			case 'total_frames_2_way':
			case 'alternative_match_total':
				$metodComparison = 'resultTotalsByHheader';
			break;
			case 'total_frames_3_way':
				$metodComparison = 'resultOverUnderByHeader';
			break;
			case 'correct_score':
			case 'score_after_1st_four_frames':
				$metodComparison = 'resultCorrectSetScore';
			break;
			case '1st_frame_total_points_2_way':
				$metodComparison = 'resultFrameTotalPoints2Way';
			break;
			case '1st_frame_winning_margin':
				$metodComparison = 'resultWinningMargin';
			break;
			case '1st_frame_highest_break_total':
				$metodComparison = 'resultTotalBands';
			break;
			case '1st_frame_player_breaks':
				$metodComparison = 'resultFramePlayerBreaks';
			break;
			case '1st_frame_player_points_odd_even':
				$metodComparison = 'resultPlayerOddEven';
			break;
			case 'race_to_(frames)':
				$metodComparison = 'resultRaceToFrames';
			break;
			case '1st_frame_first_colour':
			case '1st_frame_last_points':
				$metodComparison = 'resultMatchSearchByValue';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultFrameTotalPoints2Way($item, $value) {
		$itemNum = (int)filter_var($item['name'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
		if( strpos($item['name'], '& Over') !== false ){
			return $itemNum <= $value;
		} elseif( strpos($item['name'], 'Under') !== false ){
			return $itemNum > $value;
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultRaceToFrames($item, $value) {
		if($item['name'] != $value['frames']){
			return null;
		}
		return $item['header'] == $value['player'];
	}

}

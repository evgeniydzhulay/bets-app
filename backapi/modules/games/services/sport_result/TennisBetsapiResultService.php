<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class TennisBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		$scoreFullTime = [
			'game_winner' => ['home' => 0,'away' => 0],
			'total_games' => ['home' => 0,'away' => 0],
		];

		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				$score['home'] > $score['away'] ? $scoreFullTime['game_winner']['home']++ : $scoreFullTime['game_winner']['away']++;
				$scoreFullTime['total_games']['home'] += $score['home'];
				$scoreFullTime['total_games']['away'] += $score['away'];
				switch($scoreKey){
					//Tennis - Set 1
					case 1:
						$this->resultBetsItemsPrepare([
							'code_api' => 'first_set_winner',
							'value' => $score['home'] > $score['away'] ? 'Home' : 'Away',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'first_set_handicap',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'first_set_score_any_player',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_set_total_games',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_set_game_score',
							'value' => array_values($score),
						]);
					break;
					//Tennis - Set 2
					case 2:
					break;
					//Tennis - Set 3
					case 3:
					break;
					//Tennis - Set 4
					case 4:
					break;
					//Tennis - Set 5
					case 5:
					break;
				}
			}

			$this->resultBetsItemsPrepare([
				'code_api' => 'to_win_match',
				'value' => $scoreFullTime['game_winner']['home'] > $scoreFullTime['game_winner']['away'] ? 'Home' : 'Away',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'handicap_2_way_games_won',
				'value' => array_values($scoreFullTime['total_games']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'match_total_games_odd_even',
				'value' => ($scoreFullTime['total_games']['home'] + $scoreFullTime['total_games']['away']) % 2 == 0 ? 'Even' : 'Odd',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'set_betting',
				'value' => array_values($scoreFullTime['game_winner']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'player_to_win_a_set',
				'value' => array_values($scoreFullTime['game_winner']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_handicap_2_way_games_won',
				'value' => array_values($scoreFullTime['total_games']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_games_2_way',
				'value' => $scoreFullTime['total_games']['home'] + $scoreFullTime['total_games']['away']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_total_games_2_way',
				'value' => $scoreFullTime['total_games']['home'] + $scoreFullTime['total_games']['away']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'player_total_games',
				'value' => array_values($scoreFullTime['total_games'])
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_player_total_games',
				'value' => array_values($scoreFullTime['total_games'])
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'double_result',
				'value' => [
					'match' => $scoreFullTime['game_winner']['home'] > $scoreFullTime['game_winner']['away'] ? 'Home' : 'Away',
					'1set' => $this->game['scores'][1]['home'] > $this->game['scores'][1]['away'] ? 'Home' : 'Away',
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_sets',
				'value' => count($this->game['scores'])
			]);
		}

		if(!empty($this->game['stats'])){
			foreach($this->game['stats'] as $statType => $statData){
				switch($statType){
					case 'aces':
						$this->resultBetsItemsPrepare([
							'code_api' => 'most_aces',
							'value' => $statData[0] > $statData[1] ? 'Home' : ($statData[0] == $statData[1] ? 'Tie' : 'Away'),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'total_aces',
							'value' => $statData[0] + $statData[1],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'player_aces',
							'value' => $statData
						]);
					break;
					case 'double_faults':
						$this->resultBetsItemsPrepare([
							'code_api' => 'most_double_faults',
							'value' => $statData[0] > $statData[1] ? 'Home' : ($statData[0] == $statData[1] ? 'Tie' : 'Away'),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'total_double_faults',
							'value' => $statData[0] + $statData[1],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'player_double_faults',
							'value' => $statData
						]);
					break;
					case 'win_1st_serve':
					break;
					case 'break_point_conversions':
					break;
				}
			}
		}

		if(!empty($this->game['events'])){
			$resultEvents = [
				'first_break_of_serve' => null,
				'tie_break_in_match' => 'No',
			];

			foreach($this->game['events'] as $event){
				if( $resultEvents['first_break_of_serve'] == null && (strpos($event['text'], 'break') !== false )){
					if( strpos($event['text'], $this->game['home']['name']) !== false){
						$resultEvents['first_break_of_serve'] = 'Home';
					} elseif( strpos($event['text'], $this->game['away']['name']) !== false){
						$resultEvents['first_break_of_serve'] = 'Away';
					}
				}
				if( strpos($event['text'], 'tie break') !== false){
					$resultEvents['tie_break_in_match'] = 'Yes';
				}

			}

			$this->resultBetsItemsPrepare([
				'code_api' => 'first_break_of_serve',
				'value' => $resultEvents['first_break_of_serve'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'tie_break_in_match',
				'value' => $resultEvents['tie_break_in_match'],
			]);

		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'to_win_match':
			case 'first_set_winner':
			case 'match_total_games_odd_even':
			case 'tie_break_in_match':
			case 'most_aces':
			case 'most_double_faults':
				$metodComparison = 'resultExactMatch';
			break;
			case 'handicap_2_way_games_won':
			case 'first_set_handicap':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'first_break_of_serve':
				$metodComparison = 'resultMatchSearchNotNull';
			break;
			case 'set_betting':
			case '1st_set_game_score':
				$metodComparison = 'resultCorrectSetScore';
			break;
			case 'player_to_win_a_set':
				$metodComparison = 'resultPlayerToWinASet';
			break;
			case 'first_set_score_any_player':
				$metodComparison = 'resultFirstSetScoreAnyPlayer';
			break;
			case 'alternative_handicap_2_way_games_won':
				$metodComparison = 'resultAlternativeHandicap2WayGamesWon';
			break;
			case 'total_games_2_way':
			case 'total_aces':
			case 'total_double_faults':
				$metodComparison = 'resultTotalsByName';
			break;
			case 'alternative_total_games_2_way':
			case '1st_set_total_games':
				$metodComparison = 'resultTotalsByHeader';
			break;
			case 'player_total_games':
			case 'alternative_player_total_games':
			case 'player_aces':
			case 'player_double_faults':
				$metodComparison = 'resultPlayerTotal';
			break;
			case 'double_result':
				$metodComparison = 'resultDoubleResult';
			break;
			case 'total_sets':
				$metodComparison = 'resultMatchSearch';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultPlayerToWinASet($item, $value) {
		if($item['header'] == 'Home'){
			$itemValue = $value[0];
		} elseif( $item['header'] == 'Away'){
			$itemValue = $value[1];
		}
		$itemValueName = $itemValue > 0 ? 'Yes' : 'No';
		return $item['name'] == $itemValueName;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultFirstSetScoreAnyPlayer($item, $value) {
		$scoreArr = explode('-',$item['name']);
		return ($value[0] == (int)$scoreArr[0] && $value[1] == (int)$scoreArr[1]);
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultAlternativeHandicap2WayGamesWon($item, $value) {
		if( strpos($item['header'], 'Home') !== false ){
			return ($value[0] + $item['handicap']) > $value[1];
		} elseif( strpos($item['header'], 'Away') !== false ){
			return $value[0] < ($value[1] + $item['handicap']);
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultDoubleResult($item, $value) {
		if( strpos($item['name'], $value['1set']) === false ){
			return false;
		}
		$matchStatus = $value['1set'] == $value['match'] ? 'Win Match' : 'Lose Match';
		return strpos($item['name'], $matchStatus);
	}

}

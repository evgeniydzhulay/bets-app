<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class GaelicSportsBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){
		$scoreFullTime = null;
		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				switch($scoreKey){
					//GaelicSports -  1
					case 1:
					break;
					//GaelicSports -  2
					case 2:
					break;
				}
			}
		}

		if(!empty($this->game['ss']) && strpos($this->game['ss'], '-') !== false ){
			$scoreArr = explode('-',$this->game['ss']);
			preg_match('#\((.*?)\)#', $scoreArr[0], $match);
			$scoreFullTime[0] = (int)$match[1];
			preg_match('#\((.*?)\)#', $scoreArr[1], $match);
			$scoreFullTime[1] = (int)$match[1];
			$this->resultBetsItemsPrepare([
				'code_api' => 'handicap_3_way',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'match_result',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Draw' : 'Away'),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'draw_no_bet',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'handicap_2_way',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_points_2_way',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'handicap_3_way':
				$metodComparison = 'resultHandicap3Way';
			break;
			case 'match_result':
				$metodComparison = 'resultExactMatch';
			break;
			case 'draw_no_bet':
				$metodComparison = 'resultDrawNoBet';
			break;
			case 'handicap_2_way':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'total_points_2_way':
				$metodComparison = 'resultTotalsByName';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

}

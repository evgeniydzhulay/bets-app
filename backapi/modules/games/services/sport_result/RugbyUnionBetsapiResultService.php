<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class RugbyUnionBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){
		if(!empty($this->game['ss']) && strpos($this->game['ss'], '-') !== false ){
			$scoreFullTime = explode('-',$this->game['ss']);
			$scoreFullTime[0] = (int)$scoreFullTime[0];
			$scoreFullTime[1] = (int)$scoreFullTime[1];
			$this->resultBetsItemsPrepare([
				'code_api' => 'handicap_2_way',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'to_win',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'handicap_3_way',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'match_result',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'winning_margin_4_way',
				'value' => [
					'player' => $scoreFullTime[0] > $scoreFullTime[1] ? '1' : '2',
					'margin' => abs($scoreFullTime[0] - $scoreFullTime[1])
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '1st_half_winning_margin',
				'value' => [
					'player' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
					'margin' => abs($scoreFullTime[0] - $scoreFullTime[1])
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_winning_margin',
				'value' => [
					'player' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
					'margin' => abs($scoreFullTime[0] - $scoreFullTime[1])
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'exact_winning_margin',
				'value' => [
					'player' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
					'margin' => abs($scoreFullTime[0] - $scoreFullTime[1])
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_points_odd_even',
				'value' => ($scoreFullTime[0] + $scoreFullTime[1]) % 2 == 0 ? 'Even' : 'Odd',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'to_qualify',
				'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'totals',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '3_way_total',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_handicap_2_way',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_handicap_3_way',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_total_2_way',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_total_3_way',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_points_3_way_(range)',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_points_(bands)',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'start_+12.5',
				'value' =>[
					'start_plus' => 12.5,
					'score' => $scoreFullTime
				 ]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'tribet',
				'value' => [
					'player' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
					'margin' => abs($scoreFullTime[0] - $scoreFullTime[1])
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'either_team_to_score_0_points',
				'value' => ($scoreFullTime[0] == 0 or $scoreFullTime[1] == 0) ? 'Yes' : 'No',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_total_points_2_way',
				'value' =>  $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'home_team_alternative_totals_2_way',
				'value' =>  $scoreFullTime[0],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'away_team_alternative_totals_2_way',
				'value' =>  $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_total_points_3_way',
				'value' =>  $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'home_team_alternative_totals_3_way',
				'value' =>  $scoreFullTime[0],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'away_team_alternative_total_3_way',
				'value' =>  $scoreFullTime[1],
			]);
		}

		if(!empty($this->game['events'])){
			$resultEvents = [
				'team_to_score_first' => null,
				'team_to_score_first_try' => 'No Try',
				'first_try_converted' => 'No',
				'team_to_score_last' => null,
				'team_to_score_last_try' => null,
				'time_of_1st_try' => 0,
				'time_of_last_try' => 0,
				'10_minute_total_tries' => 0,
				'10_minute_total_penalties_scored' => 0,
				'total_penalties_scored' => 0,
				'10_minute_team_total_points' => [
					'home' => 0,
					'away' => 0,
				],
				'10_minute_team_tries' => [
					'home' => 0,
					'away' => 0,
				],
				'20_minute_team_total_points' => [
					'home' => 0,
					'away' => 0,
				],
				'60_minute_team_total_points' => [
					'home' => 0,
					'away' => 0,
				],
				'1st_half_team_total_points' => [
					'home' => 0,
					'away' => 0,
				],
				'1st_half_team_tries' => [
					'home' => 0,
					'away' => 0,
				],
				'1st_half_race_to_(points)' => [
					10 => 'Neither',
					15 => 'Neither',
					20 => 'Neither'
				],
				'fulltime_team_total_points' => [
					'home' => 0,
					'away' => 0,
				],
				'fulltime_team_tries' => [
					'home' => 0,
					'away' => 0,
				],
				'fulltime_team_conversions' => [
					'home' => 0,
					'away' => 0,
				],
				'race_to_(points)' => [
					10 => 'Neither',
					15 => 'Neither',
					20 => 'Neither',
					25 => 'Neither',
					30 => 'Neither',
					35 => 'Neither',
					40 => 'Neither'
				],
				'first_scoring_play' => [
					'team' => null,
					'scoring_tipe' => null,
				],
				'unanswered_tries' => [
					'home' => 0,
					'away' => 0,
				],
				'to_score_three_unanswered_tries' => [
					'home' => "No",
					'away' => "No",
				],
			];

			foreach($this->game['events'] as $event){
				if( strpos($event['text'],"' -") !== false){
					$minute_event =  (int)explode("' -",$event['text'])[0];
					$team_event = null;
					if( strpos($event['text'], $this->game['home']['name']) !== false){
						$team_event = 'home';
					} elseif( strpos($event['text'], $this->game['away']['name']) !== false){
						$team_event = 'away';
					}else {
						continue;
					}
					if( strpos($event['text'],"Try") !== false){
						$point = 5;
						$resultEvents['team_to_score_first'] = $resultEvents['team_to_score_first'] ?? $team_event;
						$resultEvents['team_to_score_first_try'] = $resultEvents['team_to_score_first_try'] ?? $team_event;
						$resultEvents['fulltime_team_total_points'][$team_event] += $point;
						$resultEvents['fulltime_team_tries'][$team_event]++;
						$resultEvents['team_to_score_last'] = $team_event;
						$resultEvents['team_to_score_last_try'] = $team_event;
						$resultEvents['first_scoring_play'] = $resultEvents['first_scoring_play'] ?? ['team' => $team_event, 'scoring_tipe' => 'Try'];
						if($minute_event <= 10){
							$resultEvents['10_minute_total_tries']++;
							if($team_event != null){
								$resultEvents['10_minute_team_tries'][$team_event]++;
								$resultEvents['10_minute_team_total_points'][$team_event] += $point;
							}
						}
						if($minute_event <= 40 && $team_event != null){
							$resultEvents['1st_half_team_total_points'][$team_event] += $point;
							$resultEvents['1st_half_team_tries'][$team_event]++;
						}
						$resultEvents['20_minute_team_total_points'][$team_event] += (($minute_event <= 20 && $team_event != null) ? $point : 0 );
						$resultEvents['60_minute_team_total_points'][$team_event] += (($minute_event <= 60 && $team_event != null) ? $point : 0 );
						if($team_event == 'home'){
							$resultEvents['unanswered_tries']['away'] = 0;
						} else {
							$resultEvents['unanswered_tries']['home'] = 0;
						}
						$resultEvents['unanswered_tries'][$team_event]++;
						if($resultEvents['unanswered_tries'][$team_event] >= 3){
							$resultEvents['unanswered_tries'][$team_event] = 'Yes';
						}
					}
					if( strpos($event['text'],"Conversion") !== false){
						$point = 2;
						$resultEvents['team_to_score_first'] = $resultEvents['team_to_score_first'] ?? $team_event;
						$resultEvents['fulltime_team_total_points'][$team_event] += $point;
						$resultEvents['fulltime_team_conversions'][$team_event]++;
						$resultEvents['team_to_score_last'] = $team_event;
						if(($resultEvents['fulltime_team_tries']['home'] + $resultEvents['fulltime_team_tries']['away']) == 1){
							$resultEvents['first_try_converted'] = 'Yes';
						}
						if($minute_event <= 10 && $team_event != null){
							if($team_event != null){
								$resultEvents['10_minute_team_total_points'][$team_event] += $point;
							}
						}
						if($minute_event <= 40 && $team_event != null){
							$resultEvents['1st_half_team_total_points'][$team_event] += $point;
						}
						$resultEvents['20_minute_team_total_points'][$team_event] += (($minute_event <= 20 && $team_event != null) ? $point : 0 );
						$resultEvents['60_minute_team_total_points'][$team_event] += (($minute_event <= 60 && $team_event != null) ? $point : 0 );
					}
					if( strpos($event['text'],"Penalty") !== false){
						$point = 3;
						$resultEvents['team_to_score_first'] = $resultEvents['team_to_score_first'] ?? $team_event;
						$resultEvents['team_to_score_first_try'] = $resultEvents['team_to_score_first_try'] ?? $team_event;
						$resultEvents['fulltime_team_total_points'][$team_event] += $point;
						$resultEvents['first_scoring_play'] = $resultEvents['first_scoring_play'] ?? ['team' => $team_event, 'scoring_tipe' => 'Penalty'];
						$resultEvents['team_to_score_last'] = $team_event;
						$resultEvents['total_penalties_scored'] += $point;
						if($minute_event <= 10 && $team_event != null){
							$resultEvents['10_minute_team_total_points'][$team_event] += $point;
							$resultEvents['10_minute_total_penalties_scored'] += $point;
						}
						if($minute_event <= 40 && $team_event != null){
							$resultEvents['1st_half_team_total_points'][$team_event] += $point;
						}
						$resultEvents['20_minute_team_total_points'][$team_event] += (($minute_event <= 20 && $team_event != null) ? $point : 0 );
						$resultEvents['60_minute_team_total_points'][$team_event] += (($minute_event <= 60 && $team_event != null) ? $point : 0 );
					}
					if( strpos($event['text'],"Drop Goal") !== false){
						$point = 3;
						$resultEvents['team_to_score_first'] = $resultEvents['team_to_score_first'] ?? $team_event;
						$resultEvents['fulltime_team_total_points'][$team_event] += $point;
						$resultEvents['first_scoring_play'] = $resultEvents['first_scoring_play'] ?? ['team' => $team_event, 'scoring_tipe' => 'Drop Goal'];
						$resultEvents['team_to_score_last'] = $team_event;
						if($minute_event <= 10 && $team_event != null){
								$resultEvents['10_minute_team_total_points'][$team_event] += $point;
						}
						if($minute_event <= 40 && $team_event != null){
							$resultEvents['1st_half_team_total_points'][$team_event] += $point;
						}
						$resultEvents['20_minute_team_total_points'][$team_event] += (($minute_event <= 20 && $team_event != null) ? $point : 0 );
						$resultEvents['60_minute_team_total_points'][$team_event] += (($minute_event <= 60 && $team_event != null) ? $point : 0 );
					}
					if (count($resultEvents['1st_half_race_to_(points)']) > 0 && $minute_event <= 40){
						foreach ($resultEvents['1st_half_race_to_(points)'] as $point => $pointDefault ){
							$teamRace = null;
							if($resultEvents['1st_half_team_total_points']['home'] >= $point){
								$teamRace = 1;
							} elseif($resultEvents['1st_half_team_total_points']['away'] >= $point) {
								$teamRace = 2;
							}
							if($teamRace != null){
								$this->resultBetsItemsPrepare([
									'code_api' => '1st_half_race_to_(points)',
									'value' => [
										'player' => $teamRace,
										'raceValue' => $point
									]
								]);
								unset($resultEvents['1st_half_race_to_(points)'][$point]);
							}
						}
					}
					if (count($resultEvents['race_to_(points)']) > 0 ){
						foreach ($resultEvents['race_to_(points)'] as $point => $pointDefault ){
							$teamRace = null;
							if($resultEvents['fulltime_team_total_points']['home'] >= $point){
								$teamRace = 1;
							} elseif($resultEvents['fulltime_team_total_points']['away'] >= $point) {
								$teamRace = 2;
							}
							if($teamRace != null){
								$this->resultBetsItemsPrepare([
									'code_api' => 'race_to_(points)',
									'value' => [
										'player' => $teamRace,
										'raceValue' => $point
									]
								]);
								unset($resultEvents['race_to_(points)'][$point]);
							}
						}
					}
				} elseif( strpos($event['text'], 'Score at the end of First Half') !== false){
					if( preg_match('/(?<home>\d+)-(?<away>\d+)/', $event['text'], $matches)){
						$scoreFirstHalf = [0 => $matches['home'], 1 => $matches['away']];
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_start_+8.5',
							'value' =>[
								'start_plus' => 8.5,
								'score' => $scoreFirstHalf
							 ]
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_winning_margin',
							'value' => [
								'player' => $scoreFirstHalf[0] > $scoreFirstHalf[1] ? 'Home' : ($scoreFirstHalf[0] == $scoreFirstHalf[1] ? 'Tie' : 'Away'),
								'margin' => abs($scoreFirstHalf[0] - $scoreFirstHalf[1])
							]
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_handicap_2_way',
							'value' => $scoreFirstHalf,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_winner_2_way',
							'value' => $scoreFirstHalf[0] > $scoreFirstHalf[1] ? 'Home' : 'Away',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_handicap_3_way',
							'value' => $scoreFullTime,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_winner_3_way',
							'value' => $scoreFirstHalf[0] > $scoreFirstHalf[1] ? 'Home' : ($scoreFirstHalf[0] == $scoreFirstHalf[1] ? 'Tie' : 'Away'),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_team_total_points',
							'value' => $scoreFirstHalf,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_total_2_way',
							'value' => $scoreFirstHalf[0] + $scoreFirstHalf[1],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_total_3_way',
							'value' => $scoreFirstHalf[0] + $scoreFirstHalf[1],
						]);
						if(!empty($scoreFullTime)){
							$scoreSecondHalf = [
								0 => $scoreFullTime[0] - $scoreFirstHalf[0],
								1 => $scoreFullTime[1] - $scoreFirstHalf[1]
							];
							$this->resultBetsItemsPrepare([
							'code_api' => 'to_win_both_halves',
							'value' => (
								($scoreFirstHalf[0] > $scoreFirstHalf[1] && $scoreSecondHalf[0] > $scoreSecondHalf[1])
								or ($scoreFirstHalf[0] < $scoreFirstHalf[1] && $scoreSecondHalf[0] < $scoreSecondHalf[1])
								? 'Either Team' : 'Neither Team' ),
							]);
							$scoreFirstHalfTotal = $scoreFirstHalf[0] + $scoreFirstHalf[1];
							$scoreSecondHalfTotal = $scoreSecondHalf[0] + $scoreSecondHalf[1];
							$this->resultBetsItemsPrepare([
								'code_api' => 'highest_scoring_half',
								'value' => $scoreFirstHalfTotal > $scoreSecondHalfTotal ? 'First Half' : ($scoreFirstHalfTotal == $scoreSecondHalfTotal ? 'Tie' : 'Second Half'),
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'team_to_win_both_halves',
								'value' => $scoreFirstHalf[0] > $scoreFirstHalf[1] && $scoreSecondHalf[0] > $scoreSecondHalf[1] ? '1' : ($scoreFirstHalf[0] < $scoreFirstHalf[1] && $scoreSecondHalf[0] < $scoreSecondHalf[1] ? '2' : 'Neither Team'),
							]);
							$teamWin1Half = $scoreFirstHalf[0] > $scoreFirstHalf[1] ? 1 : ($scoreFirstHalf[0] == $scoreFirstHalf[1] ? 'Tie' : 2);
							$teamWin2Half = $scoreSecondHalf[0] > $scoreSecondHalf[1] ? 1 : ($scoreSecondHalf[0] == $scoreSecondHalf[1] ? 'Tie' : 2);
							$this->resultBetsItemsPrepare([
								'code_api' => 'team_to_win_either_half',
								'value' => [
									'1 half' => $teamWin1Half,
									'2 half' => $teamWin2Half,
								]
							]);

							$scoreHalfWin = [
								'1 half' => [
										'team' => ($teamWin1Half == 1 or $teamWin1Half == 'Tie') ? 'Home' : 'Away',
										'score' => ($teamWin1Half == 1 or $teamWin1Half == 'Tie') ? $scoreFirstHalf[0] : $scoreFirstHalf[1],
									],
								'2 half' => [
										'team' => ($teamWin2Half == 1 or $teamWin2Half == 'Tie') ? 'Home' : 'Away',
										'score' => ($teamWin2Half == 1 or $teamWin2Half == 'Tie') ? $scoreSecondHalf[0] : $scoreSecondHalf[1],
									]
							];
							$this->resultBetsItemsPrepare([
								'code_api' => 'team_with_highest_scoring_half',
								'value' => $scoreHalfWin['1 half']['score'] > $scoreHalfWin['2 half']['score'] ? $scoreHalfWin['1 half']['team'] : ($scoreHalfWin['1 half']['score'] == $scoreHalfWin['2 half']['score'] ? 'Tie' : $scoreHalfWin['2 half']['team'])
							]);
						}
					}
				}
			}
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_to_score_first',
				'value' => ($resultEvents['team_to_score_first'] != null) ? ucfirst($resultEvents['team_to_score_first']) : null
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_to_score_first_try',
				'value' => ($resultEvents['team_to_score_first_try'] != null) ? ucfirst($resultEvents['team_to_score_first_try']) : null
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_to_score_last',
				'value' => ($resultEvents['team_to_score_last'] != null) ? ucfirst($resultEvents['team_to_score_last']) : null
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_to_score_last_try',
				'value' => ($resultEvents['team_to_score_last_try'] != null) ? ucfirst($resultEvents['team_to_score_last_try']) : null
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'score_1st_try_match_result',
				'value' => [
					'scoreFirst' => ($resultEvents['team_to_score_first_try'] != null) ? ucfirst($resultEvents['team_to_score_first_try']) : null,
					'gamePeriodResult' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'score_first_full_time',
				'value' => [
					'scoreFirst' => ($resultEvents['team_to_score_first'] != null) ? ucfirst($resultEvents['team_to_score_first']) : null,
					'gamePeriodResult' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_team_total_points',
				'value' => array_values($resultEvents['10_minute_team_total_points']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_total_points_odd_even',
				'value' => ($resultEvents['10_minute_team_total_points']['home'] + $resultEvents['10_minute_team_total_points']['away']) % 2 == 0 ? 'Even' : 'Odd',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_total_tries',
				'value' => $resultEvents['10_minute_total_tries'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_total_penalties_scored',
				'value' => $resultEvents['10_minute_total_penalties_scored'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_penalties_scored',
				'value' => $resultEvents['total_penalties_scored'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_team_tries',
				'value' => array_values($resultEvents['10_minute_team_tries']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_winning_margin',
				'value' => [
					'player' => $resultEvents['10_minute_team_total_points']['home'] > $resultEvents['10_minute_team_total_points']['away'] ? 'Home' : ($resultEvents['10_minute_team_total_points']['home'] == $resultEvents['10_minute_team_total_points']['away'] ? 'Tie' : 'Away'),
					'margin' => abs($resultEvents['10_minute_team_total_points']['home'] - $resultEvents['10_minute_team_total_points']['away'])
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_handicap_2_way',
				'value' => array_values($resultEvents['10_minute_team_total_points']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_total_2_way',
				'value' => $resultEvents['10_minute_team_total_points']['home'] + $resultEvents['10_minute_team_total_points']['away']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_winner_2_way',
				'value' => $resultEvents['10_minute_team_total_points']['home'] > $resultEvents['10_minute_team_total_points']['away'] ? 'Home' : 'Away',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_winner_3_way',
				'value' => $resultEvents['10_minute_team_total_points']['home'] > $resultEvents['10_minute_team_total_points']['away'] ? 'Home' : ($resultEvents['10_minute_team_total_points']['home'] == $resultEvents['10_minute_team_total_points']['away'] ? 'Tie' : 'Away'),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_handicap_3_way',
				'value' => array_values($resultEvents['10_minute_team_total_points']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '10_minute_total_3_way',
				'value' => $resultEvents['10_minute_team_total_points']['home'] + $resultEvents['10_minute_team_total_points']['away']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'to_lead_after_(minutes)',
				'value' => [
					'time' => '20:00',
					'score' => array_values($resultEvents['20_minute_team_total_points'])
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'to_lead_after_(minutes)',
				'value' => [
					'time' => '60:00',
					'score' => array_values($resultEvents['60_minute_team_total_points'])
				]
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_total_points_odd_even',
				'value' => array_values($resultEvents['fulltime_team_total_points']),
			]);
			if (count($resultEvents['1st_half_race_to_(points)']) > 0){
				foreach ($resultEvents['1st_half_race_to_(points)'] as $point => $pointDefault ){
					$this->resultBetsItemsPrepare([
						'code_api' => '1st_half_race_to_(points)',
						'value' => [
							'player' => $pointDefault,
							'raceValue' => $point
						]
					]);
				}
			}
			if (count($resultEvents['race_to_(points)']) > 0){
				foreach ($resultEvents['race_to_(points)'] as $point => $pointDefault ){
					$this->resultBetsItemsPrepare([
						'code_api' => 'race_to_(points))',
						'value' => [
							'player' => $pointDefault,
							'raceValue' => $point
						]
					]);
				}
			}
			$this->resultBetsItemsPrepare([
				'code_api' => '1st_half_total_tries',
				'value' => $resultEvents['1st_half_team_tries']['home'] + $resultEvents['1st_half_team_tries']['away']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => '1st_half_team_tries',
				'value' => array_values($resultEvents['1st_half_team_tries']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_tries_2_way',
				'value' => $resultEvents['fulltime_team_tries']['home'] + $resultEvents['fulltime_team_tries']['away']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_total_tries_2_way',
				'value' => $resultEvents['fulltime_team_tries']['home'] + $resultEvents['fulltime_team_tries']['away']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_tries_3_way',
				'value' => $resultEvents['fulltime_team_tries']['home'] + $resultEvents['fulltime_team_tries']['away']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_total_tries_3_way',
				'value' => $resultEvents['fulltime_team_tries']['home'] + $resultEvents['fulltime_team_tries']['away']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_tries_(bands)',
				'value' => $resultEvents['fulltime_team_tries']['home'] + $resultEvents['fulltime_team_tries']['away']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_with_most_converted_tries',
				'value' => $resultEvents['fulltime_team_conversions']['home'] > $resultEvents['fulltime_team_conversions']['away'] ? 'Home' : ($resultEvents['fulltime_team_conversions']['home'] == $resultEvents['fulltime_team_conversions']['away'] ? 'Tie' : 'Away'),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'most_tries',
				'value' => $resultEvents['fulltime_team_tries']['home'] > $resultEvents['fulltime_team_tries']['away'] ? 'Home' : ($resultEvents['fulltime_team_tries']['home'] == $resultEvents['fulltime_team_tries']['away'] ? 'Tie' : 'Away'),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_to_score_most_tries_handicap',
				'value' => array_values($resultEvents['fulltime_team_tries']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_total_tries',
				'value' => array_values($resultEvents['fulltime_team_tries']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_total_tries_3_way',
				'value' => array_values($resultEvents['fulltime_team_tries']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'home_team_alternative_total_tries_3_way',
				'value' => $resultEvents['fulltime_team_tries']['home'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'away_team_alternative_total_tries_3_way',
				'value' => $resultEvents['fulltime_team_tries']['away'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'will_there_be_a_successful_drop_goal?',
				'value' => ($resultEvents['fulltime_team_conversions']['home'] > 0 or $resultEvents['fulltime_team_conversions']['away'] > 0) ? 'Yes' : 'No',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'first_try_converted',
				'value' => $resultEvents['first_try_converted'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'to_score_three_unanswered_tries',
				'value' => $resultEvents['to_score_three_unanswered_tries'],
			]);

			if(!empty($scoreFirstHalf)){
				$this->resultBetsItemsPrepare([
					'code_api' => 'double_result',
					'value' => [
						'firstHalf' => $scoreFirstHalf[0] > $scoreFirstHalf[1] ? 'Home' : ($scoreFirstHalf[0] == $scoreFirstHalf[1] ? 'Tie' : 'Away'),
						'fullTime' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
					]
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'double_result_5_way',
					'value' => [
						'firstHalf' => $scoreFirstHalf[0] > $scoreFirstHalf[1] ? 'Home' : ($scoreFirstHalf[0] == $scoreFirstHalf[1] ? 'Tie' : 'Away'),
						'fullTime' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
					]
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'ht_ft_winning_margin',
					'value' => [
						'firstHalf' =>	[
							'player' => $scoreFirstHalf[0] > $scoreFirstHalf[1] ? 'Home' : ($scoreFirstHalf[0] == $scoreFirstHalf[1] ? 'Tie' : 'Away'),
							'margin' => abs($scoreFirstHalf[0] - $scoreFirstHalf[1])
						],
						'fullTime' =>	[
							'player' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
							'margin' => abs($scoreFullTime[0] - $scoreFullTime[1])
						],
					]
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'first_scoring_play_6_way',
					'value' => $resultEvents['first_scoring_play']
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'first_scoring_play_4_way',
					'value' => $resultEvents['first_scoring_play']
				]);
				$scoreFirst = ($resultEvents['team_to_score_first_try'] != null) ? ucfirst($resultEvents['team_to_score_first_try']) : null;
				$firstHalfWin = $scoreFirstHalf[0] > $scoreFirstHalf[1] ? 'Home' : ($scoreFirstHalf[0] == $scoreFirstHalf[1] ? 'Tie' : 'Away');
				$fullTimeWin = $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away');
				$this->resultBetsItemsPrepare([
					'code_api' => 'score_1st_try_1st_half_result',
					'value' => [
						'scoreFirst' => $scoreFirst,
						'gamePeriodResult' => $scoreFirstHalf[0] > $scoreFirstHalf[1] ? 'Home' : ($scoreFirstHalf[0] == $scoreFirstHalf[1] ? 'Tie' : 'Away'),
					]
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'score_first_half_time_full_time',
					'value' => [
						'scoreFirst' => $scoreFirst,
						'firstHalf' => $firstHalfWin,
						'fullTime' => $fullTimeWin,
					]
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'team_scoring_first_wins_game',
					'value' => ($scoreFirst == $fullTimeWin) ? 'Yes' : 'No'
				]);
			}
		}
	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'to_win':
			case '1st_half_winner_2_way':
			case 'to_qualify':
			case '10_minute_winner_2_way':
				$metodComparison = 'resultDrawNoBet';
			break;
			case 'handicap_2_way':
			case '1st_half_handicap_2_way':
			case '10_minute_handicap_2_way':
			case 'team_to_score_most_tries_handicap':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'handicap_3_way':
			case '1st_half_handicap_3_way':
			case '10_minute_handicap_3_way':
				$metodComparison = 'resultHandicap3Way';
			break;
			case 'match_result':
			case '1st_half_winner_3_way':
			case 'to_win_both_halves':
			case 'highest_scoring_half':
			case 'total_points_odd_even':
			case '10_minute_winner_3_way':
			case 'either_team_to_score_0_points':
			case 'team_with_most_converted_tries':
			case 'most_tries':
			case 'team_scoring_first_wins_game':
			case 'will_there_be_a_successful_drop_goal?':
			case 'first_try_converted':
			case 'team_with_highest_scoring_half':
			case 'team_to_score_first_try':
				$metodComparison = 'resultExactMatch';
			break;
			case 'winning_margin_4_way':
				$metodComparison = 'resultWinningMargin';
			break;
			case 'double_result':
				$metodComparison = 'resultRugbyDoubleResult';
			break;
			case '1st_half_start_+8.5':
			case 'start_+12.5':
				$metodComparison = 'resultRugbyStartPlus';
			break;
			case '1st_half_winning_margin':
			case 'winning_margin':
			case 'alternative_winning_margin':
			case '10_minute_winning_margin':
				$metodComparison = 'resultRugbyWinningMarginByName';
			break;
			case 'double_result_5_way':
				$metodComparison = 'resultRugbyDoubleResult5Way';
			break;
			case 'exact_winning_margin':
				$metodComparison = 'resultRugbyWinningMarginByNameExact';
			break;
			case 'totals':
			case '10_minute_total_2_way':
			case '1st_half_total_tries':
			case '1st_half_total_2_way':
			case 'total_tries_2_way':
			case '10_minute_total_penalties_scored':
			case 'total_penalties_scored':
				$metodComparison = 'resultTotalsByName';
			break;
			case '3_way_total':
			case '10_minute_total_3_way':
			case '1st_half_total_3_way':
			case 'total_points_3_way_(range)':
				$metodComparison = 'resultRugby3WayTotal';
			break;
			case 'alternative_handicap_2_way':
				$metodComparison = 'resultHandicap2Way';
			break;
			case 'alternative_handicap_3_way':
				$metodComparison = 'resultHandicap3Way';
			break;
			case 'alternative_total_2_way':
			case 'alternative_total_tries_2_way':
			case 'home_team_alternative_totals_2_way':
			case 'away_team_alternative_totals_2_way':
				$metodComparison = 'resultTotalsByHheader';
			break;
			case 'alternative_total_3_way':
			case 'home_team_alternative_totals_3_way':
			case 'away_team_alternative_total_3_way':
				$metodComparison = 'resultRugbyAlternativeTotal3Way';
			break;
			case '10_minute_team_total_points':
			case '10_minute_team_tries':
			case '1st_half_team_total_points':
			case '1st_half_team_tries':
			case 'team_total_points_2_way':
			case 'team_total_tries':
				$metodComparison = 'resultPlayerTotal';
			break;
			case '10_minute_total_tries':
			case 'total_tries_3_way':
				$metodComparison = 'resultOverUnderByName';
			break;
			case '1st_half_race_to_(points)':
			case 'race_to_(points)':
				$metodComparison = 'resultRaceTo';
			break;
			case 'total_points_(bands)':
			case 'total_tries_(bands)':
				$metodComparison = 'resultTotalBands';
			break;
			case 'tribet':
				$metodComparison = 'resultRugbyTribet';
			break;
			case 'ht_ft_winning_margin':
				$metodComparison = 'resultRugbyHtFtWinningMargin';
			break;
			case 'to_lead_after_(minutes)':
				$metodComparison = 'resultRugbyToLeadAfterMinutes';
			break;
			case 'team_to_score_first':
			case 'team_to_score_last':
			case 'team_to_score_last_try':
			case '10_minute_total_points_odd_even':
				$metodComparison = 'resultMatchSearchNotNull';
			break;
			case 'score_1st_try_match_result':
			case 'score_first_full_time':
			case 'score_1st_try_1st_half_result':
				$metodComparison = 'resultRugbyScore1stMatchResult';
			break;
			case 'score_first_half_time_full_time':
				$metodComparison = 'resultRugbyScore1stTry1stHalfResult';
			break;
			case 'alternative_total_tries_3_way':
			case 'home_team_alternative_total_tries_3_way':
			case 'away_team_alternative_total_tries_3_way':
				$metodComparison = 'resultOverUnderByHeader';
			break;
			case 'team_total_points_3_way':
			case 'team_total_tries_3_way':
				$metodComparison = 'resultPlayerTotal3Way';
			break;
			case 'team_to_win_both_halves':
				$metodComparison = 'resultRugbyTeamToWinBothHalves';
			break;
			case 'first_scoring_play_6_way':
				$metodComparison = 'resultRugbyFirstScoringPlay6Way';
			break;
			case 'first_scoring_play_4_way':
				$metodComparison = 'resultRugbyFirstScoringPlay4Way';
			break;
			case 'team_total_points_odd_even':
				$metodComparison = 'resultPlayerOddEven';
			break;
			case 'team_to_win_either_half':
				$metodComparison = 'resultRugbyTeamToWinEitherHalf';
			break;
			case 'to_score_three_unanswered_tries':
				$metodComparison = 'resultRugbyToScoreThreeUnansweredTries';
			break;

			default:
				return null;
			break;
		}
		return $metodComparison;
	}
}

<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class BasketballBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		if(!empty($this->game['scores'])){
			$quarterCorrectScore = ['home' => 0,'away' => 0];
			$highestScoringQuarter = ['total' => 0,'quarter' => null];
			$teamHighestScoringQuarter = ['score' => 0,'team' => null];
			foreach($this->game['scores'] as $scoreKey => $score){
				switch($scoreKey){
					//Basketball - 1st Quarter
					case 1:
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_quarter_spread',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_quarter_totals',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_quarter_money_line',
							'value' => $score['home'] > $score['away'] ? 'Home' : 'Away',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_quarter_winning_margin',
							'value' => [
								'player' => $score['home'] > $score['away'] ? 'Home' : 'Away',
								'margin' => abs($score['home'] - $score['away'])
							]
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_quarter_double_chance_1',
							'value' => $score['home'] > $score['away'] ? 'Home' : ($score['home'] == $score['away'] ? 'Draw' : 'Away'),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_quarter_team_totals',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_quarter_margin_of_victory',
							'value' => [
								'player' => $score['home'] > $score['away'] ? 'Home' : 'Away',
								'margin' => abs($score['home'] - $score['away'])
							]
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_quarter_point_spread_3_way',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_quarter_total_3_way',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_quarter_money_line_3_way',
							'value' => $score['home'] > $score['away'] ? 'Home' : ($score['home'] == $score['away'] ? 'Tie' : 'Away'),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_quarter_total_odd_even',
							'value' => ($score['home'] + $score['away']) % 2 == 0 ? 'Even' : 'Odd',
						]);

						$highestScoringQuarter = ['total' => $score['home'] + $score['away'], 'quarter' => '1st Quarter'];
						if($score['home'] > $score['away']){
							$quarterCorrectScore['home']++;
							$teamHighestScoringQuarter = ['score' => $score['home'], 'team' => 'Home'];
						} elseif($score['home'] < $score['away']) {
							$teamHighestScoringQuarter = ['score' => $score['away'], 'team' => 'Away'];
							$quarterCorrectScore['away']++;
						} else {
							$teamHighestScoringQuarter = ['score' => $score['home'], 'team' => 'Tie'];
						}
					break;
					//Basketball - 2nd Quarter
					case 2:
						$this->resultBetsItemsPrepare([
							'code_api' => '2nd_quarter_margin_of_victory',
							'value' => [
								'player' => $score['home'] > $score['away'] ? 'Home' : 'Away',
								'margin' => abs($score['home'] - $score['away'])
							]
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '2nd_quarter_total_odd_even',
							'value' => ($score['home'] + $score['away']) % 2 == 0 ? 'Even' : 'Odd',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '2nd_quarter_spread',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '2nd_quarter_totals',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '2nd_quarter_money_line',
							'value' => $score['home'] > $score['away'] ? 'Home' : 'Away',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '2nd_quarter_point_spread_3_way',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '2nd_quarter_total_3_way',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '2nd_quarter_money_line_3_way',
							'value' => $score['home'] > $score['away'] ? 'Home' : ($score['home'] == $score['away'] ? 'Tie' : 'Away'),
						]);

						$totalScore = $score['home'] + $score['away'];
						if($highestScoringQuarter['total'] < $totalScore){
							$highestScoringQuarter['total'] = $totalScore;
							$highestScoringQuarter['quarter'] = '2nd Quarter';
						} elseif($highestScoringQuarter['total'] == $totalScore){
							$highestScoringQuarter['quarter'] = 'Tie';
						}
						if($score['home'] > $score['away']){
							$quarterCorrectScore['home']++;
							$teamHighestScoringQuarter = ($score['home'] < $teamHighestScoringQuarter['score']) ?: ['score' => $score['home'], 'team' => 'Home'];
						} elseif($score['home'] < $score['away']) {
							$teamHighestScoringQuarter = ($score['away'] < $teamHighestScoringQuarter['score']) ?: ['score' => $score['away'], 'team' => 'Away'];
							$quarterCorrectScore['away']++;
						} else {
							$teamHighestScoringQuarter = ($score['home'] < $teamHighestScoringQuarter['score']) ?: ['score' => $score['home'], 'team' => 'Tie'];
						}
					break;
					//Basketball - Half (1+2)
					case 3:
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_team_totals',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_winning_margin',
							'value' => [
								'player' => $score['home'] > $score['away'] ? 'Home' : 'Away',
								'margin' => abs($score['home'] - $score['away'])
							]
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_spread_3_way',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_totals_3_way',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_money_line_3_way',
							'value' => $score['home'] > $score['away'] ? 'Home' : ($score['home'] == $score['away'] ? 'Tie' : 'Away'),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_double_chance',
							'value' => $score['home'] > $score['away'] ? 'Home' : ($score['home'] == $score['away'] ? 'Draw' : 'Away'),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_spread',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_totals',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_money_line',
							'value' => $score['home'] > $score['away'] ? 'Home' : 'Away',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_total_odd_even',
							'value' => ($score['home'] + $score['away']) % 2 == 0 ? 'Even' : 'Odd',
						]);
					break;
					//Basketball - 3rd Quarter
					case 4:
						$this->resultBetsItemsPrepare([
							'code_api' => '3rd_quarter_spread',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '3rd_quarter_totals',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '3rd_quarter_money_line',
							'value' => $score['home'] > $score['away'] ? 'Home' : 'Away',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '3rd_quarter_margin_of_victory',
							'value' => [
								'player' => $score['home'] > $score['away'] ? 'Home' : 'Away',
								'margin' => abs($score['home'] - $score['away'])
							]
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '3rd_quarter_point_spread_3_way',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '3rd_quarter_total_3_way',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '3rd_quarter_money_line_3_way',
							'value' => $score['home'] > $score['away'] ? 'Home' : ($score['home'] == $score['away'] ? 'Tie' : 'Away'),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '3rd_quarter_total_odd_even',
							'value' => ($score['home'] + $score['away']) % 2 == 0 ? 'Even' : 'Odd',
						]);

						$totalScore = $score['home'] + $score['away'];
						if($highestScoringQuarter['total'] < $totalScore){
							$highestScoringQuarter['total'] = $totalScore;
							$highestScoringQuarter['quarter'] = '3rd Quarter';
						} elseif($highestScoringQuarter['total'] == $totalScore){
							$highestScoringQuarter['quarter'] = 'Tie';
						}
						if($score['home'] > $score['away']){
							$quarterCorrectScore['home']++;
							$teamHighestScoringQuarter = ($score['home'] < $teamHighestScoringQuarter['score']) ?: ['score' => $score['home'], 'team' => 'Home'];
						} elseif($score['home'] < $score['away']) {
							$teamHighestScoringQuarter = ($score['away'] < $teamHighestScoringQuarter['score']) ?: ['score' => $score['away'], 'team' => 'Away'];
							$quarterCorrectScore['away']++;
						} else {
							$teamHighestScoringQuarter = ($score['home'] < $teamHighestScoringQuarter['score']) ?: ['score' => $score['home'], 'team' => 'Tie'];
						}
					break;
					//Basketball - 4th Quarter
					case 5:
						$this->resultBetsItemsPrepare([
							'code_api' => '4th_quarter_spread',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '4th_quarter_totals',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '4th_quarter_money_line',
							'value' => $score['home'] > $score['away'] ? 'Home' : 'Away',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '4th_quarter_margin_of_victory',
							'value' => [
								'player' => $score['home'] > $score['away'] ? 'Home' : 'Away',
								'margin' => abs($score['home'] - $score['away'])
							]
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '4th_quarter_point_spread_3_way',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '4th_quarter_total_3_way',
							'value' => $score['home'] + $score['away'],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '4th_quarter_money_line_3_way',
							'value' => $score['home'] > $score['away'] ? 'Home' : ($score['home'] == $score['away'] ? 'Tie' : 'Away'),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '4th_quarter_total_odd_even',
							'value' => ($score['home'] + $score['away']) % 2 == 0 ? 'Even' : 'Odd',
						]);

						$totalScore = $score['home'] + $score['away'];
						if($highestScoringQuarter['total'] < $totalScore){
							$highestScoringQuarter['total'] = $totalScore;
							$highestScoringQuarter['quarter'] = '4th Quarter';
						} elseif($highestScoringQuarter['total'] == $totalScore){
							$highestScoringQuarter['quarter'] = 'Tie';
						}
						if($score['home'] > $score['away']){
							$quarterCorrectScore['home']++;
							$teamHighestScoringQuarter = ($score['home'] < $teamHighestScoringQuarter['score']) ?: ['score' => $score['home'], 'team' => 'Home'];
						} elseif($score['home'] < $score['away']) {
							$teamHighestScoringQuarter = ($score['away'] < $teamHighestScoringQuarter['score']) ?: ['score' => $score['away'], 'team' => 'Away'];
							$quarterCorrectScore['away']++;
						} else {
							$teamHighestScoringQuarter = ($score['home'] < $teamHighestScoringQuarter['score']) ?: ['score' => $score['home'], 'team' => 'Tie'];
						}
					break;
					//Basketball - overtime
					case 6:
					break;
					//Basketball - Full time (1+2+4+5+6)
					case 7:
						$scoreFullTime = array_values($score);
						if(!empty($this->game['scores'][3])){
							$score2ndHalf = [
								0 => $scoreFullTime[0] - $this->game['scores'][3]['home'],
								1 => $scoreFullTime[1] - $this->game['scores'][3]['away'],
							];
							$score1stHalfTotal = $this->game['scores'][3]['home'] + $this->game['scores'][3]['away'];
							$score2ndHalfTotal = $score2ndHalf[0] + $score2ndHalf[1];
							$this->resultBetsItemsPrepare([
								'code_api' => 'highest_scoring_half',
								'value' => $score1stHalfTotal > $score2ndHalfTotal ? '1st Half' : ($score1stHalfTotal == $score2ndHalfTotal ? 'Tie' : '2nd Half'),
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '2nd_half_spread',
								'value' => $score2ndHalf
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '2nd_half_totals',
								'value' => $score2ndHalfTotal,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '2nd_half_money_line',
								'value' => $score2ndHalf[0] > $score2ndHalf[1] ? 'Home' : 'Away',
							]);
						}
					break;
				}
			}

			if(!empty($scoreFullTime)){
				$this->resultBetsItemsPrepare([
					'code_api' => 'point_spread',
					'value' => $scoreFullTime,
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'game_totals',
					'value' => $scoreFullTime[0] + $scoreFullTime[1],
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'money_line',
					'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'handicap_betting',
					'value' => $scoreFullTime,
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => "result_and_both_teams_to_score_'x'_points",
					'value' => $scoreFullTime,
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'winning_margin_3_way',
					'value' => [
						'player' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
						'margin' => abs($scoreFullTime[0] - $scoreFullTime[1])
					]
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'winning_margin',
					'value' => [
						'player' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
						'margin' => abs($scoreFullTime[0] - $scoreFullTime[1])
					]
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'winning_margin_12_way',
					'value' => [
						'player' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
						'margin' => abs($scoreFullTime[0] - $scoreFullTime[1])
					]
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'winning_margin_7_way',
					'value' =>  abs($scoreFullTime[0] - $scoreFullTime[1])
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'double_result',
					'value' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : ($scoreFullTime[0] == $scoreFullTime[1] ? 'Tie' : 'Away'),
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'game_total_odd_even',
					'value' => ($scoreFullTime[0] + $scoreFullTime[1]) % 2 == 0 ? 'Even' : 'Odd',
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'team_totals',
					'value' =>  $scoreFullTime,
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'team_total_odd_even',
					'value' =>  $scoreFullTime,
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'alternative_game_total',
					'value' => $scoreFullTime[0] + $scoreFullTime[1],
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'alternative_point_spread',
					'value' => $scoreFullTime,
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'match_outcome',
					'value' => [
						'player' => $scoreFullTime[0] > $scoreFullTime[1] ? 'Home' : 'Away',
						'result' => !empty($this->game['scores'][6]) ? 'In Overtime' : 'In Regulation Time'
					]
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'game_total_(bands)_8_way',
					'value' => $scoreFullTime[0] + $scoreFullTime[1],
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'game_total_(bands)_3_way',
					'value' => $scoreFullTime[0] + $scoreFullTime[1],
				]);
			}

			$this->resultBetsItemsPrepare([
					'code_api' => 'highest_scoring_quarter',
					'value' => $highestScoringQuarter['quarter']
				]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_with_highest_scoring_quarter',
				'value' => $teamHighestScoringQuarter['team']
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'quarter_correct_score',
				'value' => array_values($quarterCorrectScore)
			]);

		}


		if(!empty($this->game['stats'])){
			foreach($this->game['stats'] as $statType => $statData){
				switch($statType){
					case '2points':
					break;
					case '3points':
					break;
					case 'biggest_lead':
					break;
					case 'fouls':
					break;
					case 'free_throws':
					break;
					case 'free_throws_rate':
					break;
					case 'lead_changes':
					break;
					case 'maxpoints_inarow':
					break;
					case 'possession':
					break;
					case 'success_attempts':
					break;
					case 'timespent_inlead':
					break;
					case 'time_outs':
					break;
				}
			}
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}
	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'point_spread':
			case '1st_half_spread':
			case '1st_quarter_spread':
			case '2nd_half_spread':
			case '2nd_quarter_spread':
			case '3rd_quarter_spread':
			case '4th_quarter_spread':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'alternative_point_spread':
				$metodComparison = 'resultHandicap2Way';
			break;
			case 'game_totals':
			case '1st_half_totals':
			case '1st_quarter_totals':
			case 'alternative_game_total':
			case '2nd_half_totals':
			case '2nd_quarter_totals':
			case '3rd_quarter_totals':
			case '4th_quarter_totals':
				$metodComparison = 'resultTotalsByHheader';
			break;
			case 'match_winner':
			case '1st_half_money_line_3_way':
			case 'highest_scoring_half':
			case 'highest_scoring_quarter':
			case 'game_total_odd_even':
			case '1st_quarter_money_line_3_way':
			case '1st_quarter_total_odd_even':
			case 'team_with_highest_scoring_quarter':
			case '1st_half_total_odd_even':
			case '2nd_quarter_total_odd_even':
			case '2nd_quarter_money_line_3_way':
			case '3rd_quarter_money_line_3_way':
			case '4th_quarter_money_line_3_way':
			case '3rd_quarter_total_odd_even':
			case '4th_quarter_total_odd_even':
				$metodComparison = 'resultExactMatch';
			break;
			case '1st_half_team_totals':
			case '1st_quarter_team_totals':
			case 'team_totals':
				$metodComparison = 'resultPlayerTotal';
			break;
			case '1st_half_spread_3_way':
			case 'handicap_betting':
			case '1st_quarter_point_spread_3_way':
			case '2nd_quarter_point_spread_3_way':
			case '3rd_quarter_point_spread_3_way':
			case '4th_quarter_point_spread_3_way':
				$metodComparison = 'resultHandicap3Way';
			break;
			case '1st_half_totals_3_way':
			case '1st_quarter_total_3_way':
			case '2nd_quarter_total_3_way':
			case '3rd_quarter_total_3_way':
			case '4th_quarter_total_3_way':
				$metodComparison = 'resultOverUnderByHeader';
			break;
			case '1st_half_double_chance':
			case 'double_result':
			case '1st_quarter_double_chance_1':
				$metodComparison = 'resultMatchSearch';
			break;
			case 'money_line':
			case '1st_half_money_line':
			case '1st_quarter_money_line':
			case '2nd_half_money_line':
			case '2nd_quarter_money_line':
			case '3rd_quarter_money_line':
			case '4th_quarter_money_line':
				$metodComparison = 'resultDrawNoBet';
			break;
			case "result_and_both_teams_to_score_'x'_points":
				$metodComparison = 'resultAndBothTeamsToScoreXPoints';
			break;
			case '1st_half_winning_margin':
			case 'winning_margin':
			case 'winning_margin_12_way':
			case '1st_quarter_winning_margin':
				$metodComparison = 'resultWinningMargin';
			break;
			case 'winning_margin_3_way':
				$metodComparison = 'resultWinningMargin3Way';
			break;
			case 'winning_margin_7_way':
			case 'game_total_(bands)_8_way':
				$metodComparison = 'resultWinningNoPlayer';
			break;
			case 'quarter_correct_score':
				$metodComparison = 'resultCorrectSetScore';
			break;
			case '1st_quarter_margin_of_victory':
			case '2nd_quarter_margin_of_victory':
			case '3rd_quarter_margin_of_victory':
			case '4th_quarter_margin_of_victory':
				$metodComparison = 'resultQuarterMarginOfVictory';
			break;
			case 'team_total_odd_even':
				$metodComparison = 'resultTeamTotalOddEven';
			break;
			case 'match_outcome':
				$metodComparison = 'resultMatchOutcome';
			break;
			case 'game_total_(bands)_3_way':
				$metodComparison = 'resultGameTotalBands3Way';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultAndBothTeamsToScoreXPoints($item, $value) {
		$valueResult1 = $value[0] > $value[1] ? 'Home' : 'Away';
		if(strpos($item['name'], $valueResult1) === false){
			return false;
		}
		if( strpos($item['name'], 'Yes') !== false ){
			return $item['header'] <= $value[0] && $item['header'] <= $value[1];
		} elseif( strpos($item['name'], 'No') !== false ){
			return $item['header'] > $value[0] or $item['header'] > $value[1];
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultWinningMargin3Way($item, $value) {
		//!!! only 6 margin
		if(strpos($item['name'], '6 Or More') !== false){
			return $value['margin'] >= 6 && $value['player'] == $value['header'];
		} elseif(strpos($item['name'], 'Any Other Result') !== false){
			return $value['margin'] < 6;
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultQuarterMarginOfVictory($item, $value) {
		//!!! only 3 margin
		if(strpos($item['name'], '3 Or More') !== false){
			return $value['margin'] >= 3 && $value['player'] == $value['header'];
		} elseif(strpos($item['name'], 'Any Other Result') !== false){
			return $value['margin'] < 3;
		}
		return null;
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultWinningNoPlayer($item, $value) {
		if( strpos($item['name'], '+') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $itemValue <= $value;
		} else {
			$itemValues = explode('-', $item['name']);
			return (int)$itemValues[0] <= $value && (int)$itemValues[1] >= $value;
		}
	}

	/**
	 * @param  array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultMatchOutcome($item, $value) {
		if( $item['header'] !== $value['player'] ){
			return false;
		}
		return	$item['name'] == $value['result'];
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultGameTotalBands3Way($item, $value) {
		if( strpos($item['name'], 'And Over') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $value >= $itemValue;
		} elseif( strpos($item['name'], 'And Under') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['name']);
			return $value <= $itemValue;
		} else{
			$itemValues = explode('-', $item['name']);
			return (int)$itemValues[0] <= $value && (int)$itemValues[1] >= $value;
		}
	}

}

<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class AmericanFootballBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		$scoreFullTime = null;

		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				switch($scoreKey){
					//AmericanFootball - 1 quarter
					case 1:
					break;
					//AmericanFootball - 2 quarter
					case 2:
					break;
					//AmericanFootball - 1 Half
					case 3:
					break;
					//AmericanFootball - 3 quarter
					case 4:
					break;
					//AmericanFootball - 4 quarter
					case 5:
					break;
					//AmericanFootball - Full Time Score
					case 7:
						$scoreFullTime = array_values($score);
					break;
				}
			}
		}

		if( $scoreFullTime != null){
			$this->resultBetsItemsPrepare([
				'code_api' => 'money_line',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'point_spread',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'game_totals',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'money_line':
				$metodComparison = 'resultDrawNoBet';
			break;
			case 'point_spread':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'game_totals':
				$metodComparison = 'resultTotalsByName';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

}

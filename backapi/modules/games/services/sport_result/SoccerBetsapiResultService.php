<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class SoccerBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		$scoreFullTime = null;

		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				switch($scoreKey){
					//soccer - 1st time
					case 1:
						$totalGoals1st = $score['home'] + $score['away'];
						$this->resultBetsItemsPrepare([
							'code_api' => 'half_time_result',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'half_time_double_chance',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'half_time_correct_score',
							'value' => implode('-', $score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_goals_odd_even',
							'value' => ($totalGoals1st) % 2 == 0 ? 'Even' : 'Odd',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_asian_handicap',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'alternative_1st_half_asian_handicap',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_goal_line',
							'value' => $totalGoals1st,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'alternative_1st_half_goal_line',
							'value' => $totalGoals1st,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'both_teams_to_score_in_1st_half',
							'value' => $score['home'] > 0 && $score['away'] > 0 ? 'Yes' : 'No',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'first_half_goals',
							'value' => $totalGoals1st,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'exact_1st_half_goals',
							'value' => $totalGoals1st,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'half_time_result_both_teams_to_score',
							'value' => array_values($score),
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'half_time_result_total_goals',
							'value' => array_values($score),
						]);
					break;
					//soccer - Full Time Score
					case 2:
						$scoreFullTime = array_values($score);
						if(!empty($this->game['scores'][1]) ){
							$srore2ndHalf = [
								'home' => $this->game['scores'][2]['home'] - $this->game['scores'][1]['home'],
								'away' => $this->game['scores'][2]['away'] - $this->game['scores'][1]['away']
							];
							$totalGoals2nd = $srore2ndHalf['home'] + $srore2ndHalf['away'];
							$this->resultBetsItemsPrepare([
								'code_api' => '2nd_half_goals_odd_even',
								'value' => ($totalGoals2nd) % 2 == 0 ? 'Even' : 'Odd',
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'both_teams_to_score_in_2nd_half',
								'value' => $srore2ndHalf['home'] > 0 && $srore2ndHalf['away'] > 0 ? 'Yes' : 'No',
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'both_teams_to_score_in_2nd_half',
								'value' =>[
									1 => $this->game['scores'][1]['home'] > 0 && $this->game['scores'][1]['away'] > 0 ? 'Yes' : 'No',
									2 => $srore2ndHalf['home'] > 0 && $srore2ndHalf['away'] > 0 ? 'Yes' : 'No'
								],
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '2nd_half_goals',
								'value' => $totalGoals2nd,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'exact_2nd_half_goals',
								'value' => $totalGoals2nd,
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '2nd_half_result',
								'value' => array_values($srore2ndHalf),
							]);
						}
					break;
					//soccer - Extra Time Score
					case 3:
					break;
					//soccer - penalties
					case 4:
					break;
				}
			}

			if(!empty($this->game['scores'][1]) && !empty($this->game['scores'][2])){
				$this->resultBetsItemsPrepare([
					'code_api' => 'home_team_highest_scoring_half',
					'value' => $this->game['scores'][1]['home'] > $srore2ndHalf['home'] ? '1st Half' : ($this->game['scores'][1]['home'] == $srore2ndHalf['home'] ? 'Tie' : '2nd Half')
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'away_team_highest_scoring_half',
					'value' => $this->game['scores'][1]['away'] > $srore2ndHalf['away'] ? '1st Half' : ($this->game['scores'][1]['away'] == $srore2ndHalf['away'] ? 'Tie' : '2nd Half')
				]);
				$this->resultBetsItemsPrepare([
					'code_api' => 'half_with_most_goals',
					'value' => $totalGoals1st > $totalGoals2nd ? '1st Half' : ($totalGoals1st == $totalGoals2nd ? 'Tie' : '2nd Half')
				]);
			}

			$this->resultBetsItemsPrepare([
				'code_api' => 'half_time_full_time',
				'value' => $this->game['scores'],
			]);

			if(!empty($this->game['scores'][4])){
				$method_of_victory = 'Penalties';
				$victory_scores = $this->game['scores'][4];
			} elseif( !empty($this->game['scores'][3])){
				$method_of_victory = 'Extra Time';
				$victory_scores = $this->game['scores'][3];
			} else {
				$method_of_victory = '90 Mins';
				$victory_scores = $this->game['scores'][2];
			}
			$this->resultBetsItemsPrepare([
				'code_api' => 'method_of_qualification',
				'value' => ['method_of_victory' => $method_of_victory, 'victory_scores' => $victory_scores],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'method_of_victory',
				'value' => ['method_of_victory' => $method_of_victory, 'victory_scores' => $victory_scores],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'to_qualify',
				'value' => $victory_scores['home'] > $victory_scores['away'] ? 'Home' : 'Away',
			]);
		}

		if(!empty($scoreFullTime)){
			$this->resultBetsItemsPrepare([
				'code_api' => 'correct_score',
				'value' => implode('-',$scoreFullTime),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'full_time_result',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'fulltime_result',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'double_chance',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'goals_odd_even',
				'value' => ($scoreFullTime[0] + $scoreFullTime[1]) % 2 == 0 ? 'Even' : 'Odd',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'home_team_odd_even_goals',
				'value' => $scoreFullTime[0] % 2 == 0 ? 'Even' : 'Odd',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'away_team_odd_even_goals',
				'value' => $scoreFullTime[1] % 2 == 0 ? 'Even' : 'Odd',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'goals_over_under',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'asian_handicap',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_asian_handicap',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'goal_line',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'alternative_goal_line',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'draw_no_bet',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'result_total_goals',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_goals_both_teams_to_score',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'both_teams_to_score',
				'value' => $scoreFullTime[0] > 0 && $scoreFullTime[1] > 0 ? 'Yes' : 'No',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'exact_total_goals',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'number_of_goals_in_match',
				'value' => $scoreFullTime[0] + $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'teams_to_score',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'clean_sheet',
				'value' => [
					$scoreFullTime[0] == 0 ? 'Yes' : 'No',
					$scoreFullTime[1] == 0 ? 'Yes' : 'No',
				],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'team_total_goals',
				'value' => $scoreFullTime,
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'home_team_exact_goals',
				'value' => $scoreFullTime[0],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'away_team_exact_goals',
				'value' => $scoreFullTime[1],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'result_both_teams_to_score',
				'value' => $scoreFullTime,
			]);
		}

		if(!empty($this->game['stats'])){
			foreach($this->game['stats'] as $statType => $statData){
				switch($statType){
					case 'attacks':
					break;
					case 'ball_safe':
					break;
					case 'corners':
						$totalCorners = $statData[0] + $statData[1];
						$this->resultBetsItemsPrepare([
							'code_api' => 'total_corners',
							'value' => $totalCorners,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'corners_2_way',
							'value' => $totalCorners,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'corners',
							'value' => $totalCorners,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'asian_total_corners',
							'value' => $totalCorners,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'alternative_corners',
							'value' => $totalCorners,
						]);
					break;
					case 'corner_f':
					break;
					case 'corner_h':
						$this->resultBetsItemsPrepare([
							'code_api' => 'first_half_corners',
							'value' => $statData[0] + $statData[1],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_half_asian_corners',
							'value' => $statData[0] + $statData[1],
						]);
					break;
					case 'dangerous_attacks':
					break;
					case 'fouls':
					break;
					case 'goalattempts':
					break;
					case 'goals':
						$this->resultBetsItemsPrepare([
							'code_api' => 'correct_score',
							'value' => implode('-',$statData),
						]);
						/*$this->resultBetsItemsPrepare([
							'code_api' => 'full_time_result',
							'value' => $statData,
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'double_chance',
							'value' => $statData,
						]);*/
						$this->resultBetsItemsPrepare([
							'code_api' => 'goals_odd_even',
							'value' => ($statData[0] + $statData[1]) % 2 == 0 ? 'Even' : 'Odd',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'home_team_odd_even_goals',
							'value' => $statData[0] % 2 == 0 ? 'Even' : 'Odd',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'away_team_odd_even_goals',
							'value' => $statData[1] % 2 == 0 ? 'Even' : 'Odd',
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'goals_over_under',
							'value' => $statData[0] + $statData[1],
						]);
						$this->resultBetsItemsPrepare([
							'code_api' => 'alternative_total_goals',
							'value' => $statData[0] + $statData[1],
						]);
						if($statData[0] == 0 && $statData[1] == 0){
							$this->resultBetsItemsPrepare([
								'code_api' => 'first_team_to_score',
								'value' => 'No Goals',
							]);
						}
					break;
					case 'injuries':
					break;
					case 'offsides':
					break;
					case 'off_target':
					break;
					case 'on_target':
					break;
					case 'penalties':
						$this->resultBetsItemsPrepare([
							'code_api' => 'a_penalty_in_the_match',
							'value' => $statData[0] > 0 && $statData[1] > 0 ? 'Yes' : 'No',
						]);
					break;
					case 'possession_rt':
					break;
					case 'redcards':
					break;
					case 'saves':
					break;
					case 'shots_blocked':
					break;
					case 'substitutions':
					break;
					case 'yellowcards':
					break;
				}
			}
		}

		if(!empty($this->game['events'])){
			$resultEvents = [
				'first_team_to_score' => 'No Goals',
				'time_of_first_goal_brackets' => 'No Goal',
				'own_goal' => 'No',
				'first_card_received' => 'No Card',
				'total_goal_minutes' => 0,
				'goals_by_time' => [],
				'time_of_1st_team_goal' => [],
				'last_team_to_score' => 'No Goal',
				'a_red_card_in_the_match' => 'No'
			];

			foreach($this->game['events'] as $event){
				//ET - extra time
				if( strpos($event['text'], ' ET ') === false){
					if( strpos($event['text'], '1st Goal') !== false){
						if( strpos($event['text'], $this->game['home']['name']) !== false){
							$resultEvents['first_team_to_score'] = 'Home';
						} elseif( strpos($event['text'], $this->game['away']['name']) !== false){
							$resultEvents['first_team_to_score'] = 'Away';
						}

						if( strpos($event['text'],"' -") !== false){
								$resultEvents['time_of_first_goal_brackets'] = (int)explode("' -",$event['text'])[0];
						}
					}
					if( strpos($event['text'], 'Goal') !== false){
						if( strpos($event['text'], $this->game['home']['name']) !== false){
							$resultEvents['last_team_to_score'] = 'Home';
						} elseif( strpos($event['text'], $this->game['away']['name']) !== false){
							$resultEvents['last_team_to_score'] = 'Away';
						}
						if( strpos($event['text'],"' -") !== false){
							$goal_minute = (int)explode("' -",$event['text'])[0];
							$resultEvents['total_goal_minutes'] += $goal_minute;
							$resultEvents['goals_by_time'][] = $goal_minute;
							if( strpos($event['text'], $this->game['home']['name']) !== false && empty($resultEvents['time_of_1st_team_goal']['home'])){
								$resultEvents['time_of_1st_team_goal']['home'] = $goal_minute;
							} elseif( strpos($event['text'], $this->game['away']['name']) !== false && empty($resultEvents['time_of_1st_team_goal']['away'])){
								$resultEvents['time_of_1st_team_goal']['away'] = $goal_minute;
							}
						}
						if( strpos($event['text'],"00:00 - 09:59") !== false){
							$goals_score = explode("Goal",$event['text'])[0];
							list($homeGoals,$awayGoals) = explode(':',$goals_score);
							$this->resultBetsItemsPrepare([
								'code_api' => 'first_10_minutes_(00:00_09:59)',
								'value' => (int)$homeGoals + (int)$awayGoals,
							]);
						}
					}
					if( strpos($event['text'], 'Own Goal') !== false){
						$resultEvents['own_goal'] = 'Yes';
					}

					if( $resultEvents['first_card_received'] == 'No Card' && (strpos($event['text'], '1st Yellow Card') !== false or strpos($event['text'], '1st Red Card') !== false)){
						if( strpos($event['text'], $this->game['home']['name']) !== false){
							$resultEvents['first_card_received'] = 'Home';
						} elseif( strpos($event['text'], $this->game['away']['name']) !== false){
							$resultEvents['first_card_received'] = 'Away';
						}
					}

					if( strpos($event['text'], 'Red Card') !== false){
						$resultEvents['a_red_card_in_the_match'] = 'Yes';
					}
				}
			}

			$this->resultBetsItemsPrepare([
				'code_api' => 'first_team_to_score',
				'value' => $resultEvents['first_team_to_score'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'time_of_first_goal_brackets',
				'value' => $resultEvents['time_of_first_goal_brackets'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'own_goal',
				'value' => $resultEvents['own_goal'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'first_card_received',
				'value' => $resultEvents['first_card_received'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_goal_minutes',
				'value' => $resultEvents['total_goal_minutes'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'early_goal',
				'value' => $resultEvents['goals_by_time'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'late_goal',
				'value' => $resultEvents['goals_by_time'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'time_of_1st_team_goal',
				'value' => $resultEvents['time_of_1st_team_goal'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'last_team_to_score',
				'value' => $resultEvents['last_team_to_score'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'a_red_card_in_the_match',
				'value' => $resultEvents['a_red_card_in_the_match'],
			]);
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'full_time_result':
			case 'fulltime_result':
			case 'half_time_result':
			case '2nd_half_result':
				$metodComparison = 'resultWhoWin';
			break;
			case 'double_chance':
			case 'half_time_double_chance':
				$metodComparison = 'resultDoubleChance';
			break;
			case 'half_time_full_time':
				$metodComparison = 'resultHalfTimeFullTime';
			break;
			case 'home_team_odd_even_goals':
			case 'away_team_odd_even_goals':
			case 'home_team_highest_scoring_half':
			case 'away_team_highest_scoring_half':
			case 'half_with_most_goals':
				$metodComparison = 'resultMatchSearch';
			break;
			case 'correct_score':
			case 'half_time_correct_score':
			case 'goals_odd_even':
			case '1st_half_goals_odd_even':
			case '2nd_half_goals_odd_even':
			case 'first_team_to_score':
			case 'both_teams_to_score':
			case 'both_teams_to_score_in_1st_half':
			case 'both_teams_to_score_in_2nd_half':
			case 'own_goal':
			case 'first_card_received':
			case 'last_team_to_score':
			case 'to_qualify':
			case 'a_red_card_in_the_match':
			case 'a_penalty_in_the_match':
				$metodComparison = 'resultExactMatch';
			break;
			case 'goals_over_under':
			case 'corners_2_way':
			case 'total_corners':
			case 'first_half_corners':
			case 'corners':
			case 'alternative_corners':
			case 'alternative_total_goals':
			case 'first_half_goals':
			case '2nd_half_goals':
			case 'first_10_minutes_(00:00_09:59)':
				$metodComparison = 'resultOverUnder';
			break;
			case 'asian_handicap':
			case '1st_half_asian_handicap':
			case 'alternative_asian_handicap':
			case 'alternative_1st_half_asian_handicap':
				$metodComparison = 'resultAsianHandicap';
			break;
			case 'goal_line':
			case '1st_half_goal_line':
			case '1st_half_asian_corners':
			case 'asian_total_corners':
			case 'alternative_goal_line':
			case 'alternative_1st_half_goal_line':
				$metodComparison = 'resultAsianTotal';
			break;
			case 'draw_no_bet':
				$metodComparison = 'resultDrawNoBet';
			break;
			case 'result_total_goals':
			case 'half_time_result_total_goals':
				$metodComparison = 'resultTotalGoals';
			break;
			case 'total_goals_both_teams_to_score':
				$metodComparison = 'resultTotalGoalsBothTeamsToScore';
			break;
			case 'exact_total_goals':
			case 'exact_1st_half_goals':
			case 'exact_2nd_half_goals':
			case 'home_team_exact_goals':
			case 'away_team_exact_goals':
				$metodComparison = 'resultExactTotalGoals';
			break;
			case 'number_of_goals_in_match':
				$metodComparison = 'resultNumberOfGoalsInMatch';
			break;
			case 'teams_to_score':
				$metodComparison = 'resultTeamsToScore';
			break;
			case 'both_teams_to_score_1st_half_2nd_half':
				$metodComparison = 'resultBothTeamsToScore1stHalf2ndHalf';
			break;
			case 'total_goal_minutes':
				$metodComparison = 'resultTotalGoalMinutes';
			break;
			case 'early_goal':
			case 'late_goal':
				$metodComparison = 'resultGoalsByTime';
			break;
			case 'time_of_first_goal_brackets':
				$metodComparison = 'resultTimeOfFirstGoalBrackets';
			break;
			case 'clean_sheet':
				$metodComparison = 'resultCleanSheet';
			break;
			case 'team_total_goals':
				$metodComparison = 'resultTeamTotalGoals';
			break;
			case 'method_of_qualification':
			case 'method_of_victory':
				$metodComparison = 'resultMethodOfVictory';
			break;
			case 'time_of_1st_team_goal':
				$metodComparison = 'resultTimeOf1stTeamGoal';
			break;
			case 'half_time_result_both_teams_to_score':
				$metodComparison = 'resultHalfTimeResultBothTeamsToScore';
			break;
			case 'result_both_teams_to_score':
				$metodComparison = 'resultResultBothTeamsToScore';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultWhoWin($item, $value) {
		$valueResult = $value[0] > $value[1] ? 1 : $value[0] == $value[1] ? 'X' : 2; 
		return	$item['opp'] == $valueResult;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultDoubleChance($item, $value) {
		$valueResultArr = $value[0] > $value[1] ? ['1X','12'] : $value[0] == $value[1] ? ['1X','X2'] : ['1X','X2'];
		return in_array($item['opp'], $valueResultArr);
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultHalfTimeFullTime($item, $value) {
		$valueHalfTime = $value[1]['home'] > $value[1]['away'] ? 'Home' : $value[1]['home'] == $value[1]['away'] ? 'Draw' : 'Away';
		$valueFullTime = $value[2]['home'] > $value[2]['away'] ? 'Home' : $value[2]['home'] == $value[2]['away'] ? 'Draw' : 'Away';
		$itemOppArr = explode('-',$item['opp']);
		return trim($itemOppArr[0]) == $valueHalfTime && trim($itemOppArr[1]) == $valueFullTime;
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultOverUnder($item, $value) {

		$itemParam = $item['goals'] ?? $item['handicap'] ?? $item['opp'];

		if( !empty($item['header']) && $item['header'] == 'Over'){
			return $itemParam < $value;
		} elseif( !empty($item['header']) && $item['header'] == 'Under'){
			return $itemParam > $value;
		} elseif( !empty($item['header']) && $item['header'] == 'Exactly'){
			return	$itemParam == $value;
		} elseif( strpos($itemParam, 'Over') !== false ){
			$itemValue = (float)str_replace('Over','',$itemParam);
			return $itemValue < $value;
		} elseif( strpos($itemParam, 'Under') !== false ){
			$itemValue = (float)str_replace('Under','',$itemParam);
			return $itemValue > $value;
		} elseif( strpos($itemParam, 'Exactly') !== false ){
			$itemValue = (float)str_replace('Exactly','',$itemParam);
			return $itemValue == $value;
		} elseif( strpos($itemParam, '-') !== false ){
			$itemValues = explode('-', $itemParam);
			return (float)$itemValues[0] <= $value && (float)$itemValues[1] >= $value;
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultAsianHandicap($item, $value) {
		if (array_search($item['header'],['Home','Away']) === false)
			return null;

		$itemOptions = explode(',', $item['opp']);
		$result = 0;
		/**
		* return - 1
		* win - 2
		* loss - 0
		*/
		$partBet = count($itemOptions);
		foreach($itemOptions as $itemOption){
			if($item['header'] == 'Home'){
				$result += (($value[0] + $itemOption == $value[1]) ? 1 : (($value[0] + $itemOption > $value[1]) ? 2 : 0)) / $partBet;
			} else {
				$result += (($value[0] == $value[1] + $itemOption)  ? 1 : (($value[0] < $value[1] + $itemOption) ? 2 : 0)) / $partBet;
			}
		}

		switch($result){
			case 0:
				$betsItemsStatus = self::RESULT_STATUS_DEFEAT;
			break;
			case 0.5:
				$betsItemsStatus = self::RESULT_STATUS_HALF_DEFEAT;
			break;
			case 1:
				$betsItemsStatus = self::RESULT_STATUS_REFUND;
			break;
			case 1.5:
				$betsItemsStatus = self::RESULT_STATUS_HALF_VICTORY;
			break;
			case 2:
				$betsItemsStatus = self::RESULT_STATUS_VICTORY;
			break;
			default:
				$betsItemsStatus = null;
			break;
		}
		return $betsItemsStatus;
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultAsianTotal($item, $value) {
		$itemParam = $item['goals'] ?? $item['opp'];
		$itemOptions = explode(',', $itemParam);
		$result = 0;
		/**
		* return - 1
		* win - 2
		* loss - 0
		*/
		$partBet = count($itemOptions);
		foreach($itemOptions as $itemOption){
			if( $item['header'] == 'Over'){
				$result += (((float)$itemOption == $value) ? 1 : ((float)$itemOption < $value ? 2 : 0)) / $partBet;
			} elseif( $item['header'] == 'Under'){
				$result += (((float)$itemOption == $value) ? 1 : ((float)$itemOption > $value ? 2 : 0)) / $partBet;
			}
		}

		switch($result){
			case 0:
				$betsItemsStatus = self::RESULT_STATUS_DEFEAT;
			break;
			case 0.5:
				$betsItemsStatus = self::RESULT_STATUS_HALF_DEFEAT;
			break;
			case 1:
				$betsItemsStatus = self::RESULT_STATUS_REFUND;
			break;
			case 1.5:
				$betsItemsStatus = self::RESULT_STATUS_HALF_VICTORY;
			break;
			case 2:
				$betsItemsStatus = self::RESULT_STATUS_VICTORY;
			break;
			default:
				$betsItemsStatus = null;
			break;
		}
		return $betsItemsStatus;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTotalGoals($item, $value) {
		$valueResult1 = $value[0] > $value[1] ? 'Home' : $value[0] == $value[1] ? 'Draw' : 'Away';
		if( strpos($item['opp'], $valueResult1) === false ){
			return false;
		}

		$valueResult2 = $value[0] + $value[1];
		$itemOppNum = (float)filter_var($item['opp'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

		if( strpos($item['opp'], 'Over') !== false ){
			return $itemOppNum < $valueResult2;
		} elseif( strpos($item['opp'], 'Under') !== false ){
			return $itemOppNum > $valueResult2;
		} elseif( strpos($item['opp'], 'Exactly') !== false ){

			return $itemOppNum == $valueResult2;
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTotalGoalsBothTeamsToScore($item, $value) {
		$valueResult1 = $value[0] > 0 && $value[1] > 0 ? 'Yes' : 'No';
		if( strpos($item['opp'], $valueResult1) === false ){
			return false;
		}

		$valueResult2 = $value[0] + $value[1];
		$itemOppNum = (float)filter_var($item['opp'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

		if( strpos($item['opp'], 'Over') !== false ){
			return $itemOppNum < $valueResult2;
		} elseif( strpos($item['opp'], 'Under') !== false ){
			return $itemOppNum > $valueResult2;
		} elseif( strpos($item['opp'], 'Exactly') !== false ){
			return $itemOppNum == $valueResult2;
		}
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultExactTotalGoals($item, $value) {
		$itemValue = preg_replace("/[^0-9]/", '', $item['opp']);
		if( strpos($item['opp'], '+') !== false ){
			return $itemValue <= $value;
		} else {
			return $itemValue == $value;
		}
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultNumberOfGoalsInMatch($item, $value) {
		if( strpos($item['opp'], 'or') !== false ){
			$itemOptions = explode('or', $item['opp']);
			foreach($itemOptions as $itemOption){
				$itemOption = preg_replace("/[^0-9]/", '', $itemOption);
				if($itemOption == $value)
					return true;
			}
			return false;
		} elseif( strpos($item['opp'], 'Over') !== false ){
			$itemValue = (float)filter_var($item['opp'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			return $itemValue < $value;
		} elseif( strpos($item['opp'], 'Under') !== false ){
			$itemValue = (float)filter_var($item['opp'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			return $itemValue > $value;
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTeamsToScore($item, $value) {
		$valueResult = 'No Goal';
		if($value[0] > 0 && $value[1] > 0){
			$valueResult = 'Both Teams';
		} elseif($value[0] > 0){
			$valueResult = 'Home Only';
		} elseif($value[1] > 0){
			$valueResult = 'Away Only';
		}
		return $item['opp'] == $valueResult;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultBothTeamsToScore1stHalf2ndHalf($item, $value) {
		$itemOptions = explode('/', $item['opp']);
		return trim($itemOptions[0]) == $value[1] && trim($itemOptions[1]) == $value[2];
	}

	/**
	 * @param int $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTotalGoalMinutes($item, $value) {
		if( strpos($item['opp'], 'To') !== false ){
			$itemOptions = explode('To', $item['opp']);
			$itemOptions[0] = preg_replace("/[^0-9]/", '', $itemOptions[0]);
			$itemOptions[1] = preg_replace("/[^0-9]/", '', $itemOptions[1]);
			return $itemOptions[0] < $value && $itemOptions[1] > $value;
		} elseif( strpos($item['opp'], 'Over') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['opp']);
			return $itemValue < $value;
		} elseif( strpos($item['opp'], 'Under') !== false ){
			$itemValue = preg_replace("/[^0-9]/", '', $item['opp']);
			return $itemValue > $value;
		}
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultGoalsByTime($item, $value) {
		$minute =  preg_replace("/[^0-9]/", '', explode(':',$item['opp'])[0]);
		if( strpos($item['opp'], 'before') !== false ){
			$time_part = 'before';
		}elseif( strpos($item['opp'], 'after') !== false ){
			$minute++;
			$time_part = 'after';
		} else {
			return null;
		}
		if(count($value) == 0){
			return (bool)strpos($item['opp'], 'No Goal');
		} else {
			foreach($value as $minuteValue){
				if($time_part == 'before'){
					return $minute >= $minuteValue;
				} else {
					return $minute <= $minuteValue;
				}
			}
			return (bool)strpos($item['opp'], 'No Goal');
		}
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTimeOfFirstGoalBrackets($item, $value) {
		if( strpos($item['opp'], 'No Goal') !== false ){
			return $itemValue == $value;
		} elseif ( strpos($item['opp'], 'Full Time') !== false ){
			$itemValue = (int)preg_replace("/[^0-9]/", '', $item['opp']);
			return $itemValue <= $value;
		} elseif ( strpos($item['opp'], '-') !== false ){
			$itemValues = explode('-', $item['opp']);
			return (int)$itemValues[0] <= $value && (int)$itemValues[1] >= $value;
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultCleanSheet($item, $value) {
		if($item['header'] == 'Home'){
			return $item['handicap'] == $value[0];
		} elseif($item['header'] == 'Away'){
			return $item['handicap'] == $value[1];
		}
		return null;
	}

	/**
	 * @param string $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTeamTotalGoals($item, $value) {
		if($item['header'] == 'Home'){
			$valueTeam = $item['handicap'] == $value[0];
		} elseif($item['header'] == 'Away'){
			$valueTeam = $item['handicap'] == $value[1];
		} else {
			return null;
		}

		if( strpos($item['handicap'], 'Over') !== false or strpos($item['handicap'], 'O ') !== false ){
			$itemValue = (float)filter_var($item['handicap'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			return $itemValue < $valueTeam;
		} elseif( strpos($item['handicap'], 'Under') !== false or strpos($item['handicap'], 'U ') !== false ){
			$itemValue = (float)filter_var($item['handicap'], FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);
			return $itemValue > $valueTeam;
		}
		return null;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultTimeOf1stTeamGoal($item, $value) {
		$minute =  preg_replace("/[^0-9]/", '', explode(':',$item['opp'])[0]);
		if( strpos($item['opp'], 'before') !== false ){
			$time_part = 'before';
		} elseif( strpos($item['opp'], 'after') !== false ){
			$minute++;
			$time_part = 'after';
		} else {
			return null;
		}
		if( strpos($item['opp'], 'Home ') !== false ){
			$team = 'home';
		} elseif( strpos($item['opp'], 'Away') !== false ){
			$team = 'away';
		} else {
			return null;
		}

		if(empty($value[$team])){
			return (bool)strpos($item['opp'], 'No Goal');
		} else {
			if($time_part == 'before'){
				if(strpos($item['opp'], 'No Goal') === false){
					return $minute >= $value[$team];
				} else {
					return $minute < $value[$team];
				}
			} else {
				if(strpos($item['opp'], 'No Goal') === false){
					return $minute <= $value[$team];
				} else {
					return $minute > $value[$team];
				}
			}
		}

	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultMethodOfVictory($item, $value) {
		if($value['method_of_victory'] != $item['goals'])
			return false;

		$victory = $value['victory_scores']['home'] > $value['victory_scores']['away'] ? 'Home' : 'Away';
		return 	$item['header'] == $victory;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultHalfTimeResultBothTeamsToScore($item, $value) {
		$valueResult1 = $value[0] > 0 && $value[1] > 0 ? 'Yes' : 'No';
		if( strpos($item['opp'], $valueResult1) === false ){
			return false;
		}
		$valueResult2 = $value[0] > $value[1] ? 'Home' : $value[0] == $value[1] ? 'Draw' : 'Away';
		if( strpos($item['opp'], $valueResult2) === false ){
			return false;
		}
		return true;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultResultBothTeamsToScore($item, $value) {
		$valueResult1 = $value[0] > 0 && $value[1] > 0 ? 'Yes' : 'No';
		if( $item['header'] != $valueResult1 ){
			return false;
		}
		$valueResult2 = $value[0] > $value[1] ? 'Home' : $value[0] == $value[1] ? 'Draw' : 'Away';
		return 	$item['name'] == $valueResult2;
	}

}

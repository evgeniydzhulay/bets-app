<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class TableTennisBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		$scoreFullTime = [
			'game_winner' => ['home' => 0,'away' => 0],
			'total_points' => 0
		];

		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				$score['home'] > $score['away'] ? $scoreFullTime['game_winner']['home']++ : $scoreFullTime['game_winner']['away']++;
				$scoreFullTime['total_points'] += $score['home'] + $score['away'];
				switch($scoreKey){
					//Table Tennis - Game 1
					case 1:
						$this->resultBetsItemsPrepare([
							'code_api' => '1st_game_winner',
							'value' => $score['home'] > $score['away'] ? 'Home' : 'Away',
						]);
					break;
					//Table Tennis - Game 2
					case 2:
					break;
					//Table Tennis - Game 3
					case 3:
					break;
					//Table Tennis - Game 4
					case 4:
					break;
					//Table Tennis - Game 5
					case 5:
					break;
					//Table Tennis - Game 6
					case 6:
					break;
					//Table Tennis - Game 7
					case 7:
					break;
					//Table Tennis - Game 7
					case 8:
					break;
					//Table Tennis - Game 7
					case 9:
					break;
				}
			}

			$this->resultBetsItemsPrepare([
				'code_api' => 'to_win_match',
				'value' => $scoreFullTime['game_winner']['home'] > $scoreFullTime['game_winner']['away'] ? 'Home' : 'Away',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'match_handicap',
				'value' => array_values($scoreFullTime['game_winner']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_points_2_way',
				'value' => $scoreFullTime['total_points'],
			]);
		}

		if(!empty($this->game['events'])){
			if(!empty($this->game['events'])){
				$resultEvents = [

				];

				foreach($this->game['events'] as $event){

				}
			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'to_win_match':
			case '1st_game_winner':
				$metodComparison = 'resultExactMatch';
			break;
			case 'match_handicap':
				$metodComparison = 'resultMatchHandicap';
			break;
			case 'total_points_2_way':
				$metodComparison = 'resultTotalsByName';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

}

<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class VolleyballBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		$scoreFullTime = [
			'game_winner' => ['home' => 0,'away' => 0],
			'total_points' => 0
		];

		if(!empty($this->game['scores'])){
			foreach($this->game['scores'] as $scoreKey => $score){
				$score['home'] > $score['away'] ? $scoreFullTime['game_winner']['home']++ : $scoreFullTime['game_winner']['away']++;
				$scoreFullTime['total_points'] += $score['home'] + $score['away'];
				switch($scoreKey){
					//Volleyball - Set 1
					case 1:
						if(isset($score['home']) && isset($score['away'])){
							$this->resultBetsItemsPrepare([
								'code_api' => 'first_set_winner',
								'value' => $score['home'] > $score['away'] ? 'Home' : 'Away',
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => 'first_set_total_points',
								'value' => $score['home'] + $score['away'],
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '1st_set_total_odd_even',
								'value' => ($score['home'] + $score['away']) % 2 == 0 ? 'Even' : 'Odd',
							]);
							$this->resultBetsItemsPrepare([
								'code_api' => '1st_set_to_go_to_extra_points',
								'value' => ($score['home'] > 25 or $score['away'] > 25) ? 'Yes' : 'No',
							]);
						}
					break;
					//Volleyball - Set 2
					case 2:
					break;
					//Volleyball - Set 3
					case 3:
					break;
					//Volleyball - Set 4
					case 4:
					break;
					//Volleyball - Set 5
					case 5:
					break;
				}
			}

			$this->resultBetsItemsPrepare([
				'code_api' => 'to_win_match',
				'value' => $scoreFullTime['game_winner']['home'] > $scoreFullTime['game_winner']['away'] ? 'Home' : 'Away',
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'match_handicap_sets',
				'value' => array_values($scoreFullTime['game_winner']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'total_points',
				'value' => $scoreFullTime['total_points'],
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'correct_set_score',
				'value' => array_values($scoreFullTime['game_winner']),
			]);
			$this->resultBetsItemsPrepare([
				'code_api' => 'match_total_odd_even',
				'value' => $scoreFullTime['total_points'] % 2 == 0 ? 'Even' : 'Odd',
			]);
		}


		if(!empty($this->game['stats'])){
			foreach($this->game['stats'] as $statType => $statData){
				switch($statType){
					case 'points_won_on_serve':
					break;
					case 'longest_streak':
					break;
				}
			}
		}

		if(!empty($this->game['events'])){
			$resultEvents = [

			];

			foreach($this->game['events'] as $event){

			}
		}

	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'to_win_match':
			case 'first_set_winner':
				$metodComparison = 'resultExactMatch';
			break;
			case 'match_handicap_sets':
				$metodComparison = 'resultMatchHandicapSets';
			break;
			case 'total_points':
			case 'first_set_total_points':
				$metodComparison = 'resultTotalsByName';
			break;
			case 'correct_set_score':
				$metodComparison = 'resultCorrectSetScore';
			break;
			case 'match_total_odd_even':
			case '1st_set_total_odd_even':
			case '1st_set_to_go_to_extra_points':
				$metodComparison = 'resultExactMatch';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

	/**
	 * @param array $value
	 * @param array $item
	 *
	 * @return bool
	 */
	protected function resultMatchHandicapSets($item, $value) {
		if( strpos($item['header'], '1') !== false ){
			return ($value[0] + $item['handicap']) > $value[1];
		} elseif( strpos($item['header'], '2') !== false ){
			return $value[0] < ($value[1] + $item['handicap']);
		}
	}

}

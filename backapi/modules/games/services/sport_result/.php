<?php

namespace backapi\modules\games\services\sport_result;

use backapi\modules\games\services\BetsapiBaseResultService;

use Yii;
use yii\helpers\ArrayHelper;
use yii\db\Expression;

class VolleyballBetsapiResultService extends BetsapiBaseResultService {

	public function processingResults(){

		if(!empty($this->game['scores'])){
					foreach($this->game['scores'] as $scoreKey => $score){
						switch($scoreKey){
							//Volleyball - Set 1
							case 1:
								$totalGoals1st = $score['home'] + $score['away'];
								$this->resultBetsItemsPrepare([
									'code_api' => 'half_time_result',
									'value' => array_values($score),
								]);

							break;
							//Volleyball - Set 2
							case 2:
							break;
							//Volleyball - Set 3
							case 3:
							break;
							//Volleyball - Set 4
							case 4:
							break;
							//Volleyball - Set 5
							case 5:
							break;
						}

					}

				}



				foreach($this->game['stats'] as $statType => $statData){
					switch($statType){
						case 'points_won_on_serve':
						break;
						case 'longest_streak':
						break;
					}
				}

				if(!empty($this->game['events'])){
					$resultEvents = [

					];

					foreach($this->game['events'] as $event){

					}
				}

	}

	public function resultBetsItemsPrepare ($params) {
		$metodComparison = $this->getMetodComparison ($params['code_api']);
		$this->resultBetsItems([
			'metodComparison' => $metodComparison,
			'code_api' => $params['code_api'],
			'value' => $params['value'],
		]);
	}

	public function getMetodComparison ($code_api) {
		switch($code_api){
			case 'full_time_result':
				$metodComparison = 'resultWhoWin';
			break;
			default:
				return null;
			break;
		}
		return $metodComparison;
	}

	
}

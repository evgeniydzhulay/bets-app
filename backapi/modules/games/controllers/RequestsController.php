<?php

namespace backapi\modules\games\controllers;

use backapi\modules\games\services\BetsapiService;
use common\modules\games\models\EventGameModel;
use Yii;
use yii\db\Expression;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\web\BadRequestHttpException;

class RequestsController extends Controller {

	/** @inheritdoc */
	public function behaviors() {
		return [
			'contentNegotiator' => [
				'class' => ContentNegotiator::class,
				'formats' => [
					'application/json' => Response::FORMAT_JSON
				],
			],
		];
	}

	public function actionNew() {
		$serviceClass = Yii::$app->request->post('serviceClass');
		$method = Yii::$app->request->post('method');
		$data = Yii::$app->request->post('data');
		if(empty($serviceClass) or empty($method) or empty($data) ){
			throw new BadRequestHttpException();
		}

		/*$serviceClass = 'BetsapiService';
		$method = 'event';
		//$method = 'prematch';
		//$method = 'inplay';
		$method = 'result';*/
		switch( $serviceClass ){
			case 'BetsapiService':
				$service = new BetsapiService();
			break;
			default:
				throw new BadRequestHttpException();
			break;
		}

    	return $service->$method($data);
	}
}

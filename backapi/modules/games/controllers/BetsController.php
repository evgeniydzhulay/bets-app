<?php

namespace backapi\modules\games\controllers;

use backapi\modules\games\services\BetsapiService;
use common\modules\games\models\BetsModel;
use common\modules\games\models\EventGameModel;
use Yii;
use yii\db\Expression;
use yii\filters\auth\QueryParamAuth;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\BadRequestHttpException;

class BetsController extends Controller {

	/** @inheritdoc */
	public function behaviors() {
		return [
			'authenticator' => [
				'class' => QueryParamAuth::class,
				'only' => ['place', 'victory', 'defeat', 'revert'],
				'tokenParam' => 'key'
			],
		];
	}

	/**
	 * @param int $uid
	 * @param string $key
	 * @param float $bet
	 * @param float $win
	 * @param string $game
	 * @return array
	 */
	public function actionPlace(int $uid, string $key, float $bet, float $win, string $game) {
		$model = new BetsModel();
		$model->scenario = BetsModel::SCENARIO_CREATE;
		$model->setAttributes([
			'user_id' => $uid,
			'amount_bet' => round($bet, 5),
			'amount_win' => round($win, 5),
			'status' => BetsModel::STATUS_ACTIVE,
		]);
		if($model->insertBet([])) {
			return [
				'success' => true,
				'bet_id' => $model->id,
				'user_id' => $model->user_id,
				'amount_bet' => $model->amount_bet,
				'amount_win' => $model->amount_win,
			];
		}
		return [
			'success' => false,
			'errors' => $model->errors
		];
	}

	/**
	 * @param int $uid
	 * @param string $key
	 * @param int $bet Bet ID
	 * @param float|null $amount
	 * @return array
	 */
	public function actionVictory(int $uid, string $key, int $bet, float $amount = null) {
		$bet = $this->getBet(['id' => $bet, 'user_id' => $uid]);
		return [
			'success' => $this->updateVictoryAmount($bet, $amount) && $bet->victory()
		];
	}

	/**
	 * @param int $uid
	 * @param string $key
	 * @param int $bet Bet ID
	 * @param float|null $amount
	 * @return array
	 */
	public function actionDefeat(int $uid, string $key, int $bet, float $amount = null) {
		$bet = $this->getBet(['id' => $bet, 'user_id' => $uid]);
		return [
			'success' => $this->updateVictoryAmount($bet, $amount) && $bet->defeat()
		];
	}

	/**
	 * @param int $uid
	 * @param string $key
	 * @param int $bet
	 * @param float|null $amount
	 * @return array
	 */
	public function actionRevert(int $uid, string $key, int $bet, float $amount = null) {
		$bet = $this->getBet(['id' => $bet, 'user_id' => $uid]);
		return [
			'success' => $this->updateVictoryAmount($bet, $amount) && $bet->revert()
		];
	}

	/**
	 * @param BetsModel $bet
	 * @param float|null $amount
	 * @return bool
	 */
	protected function updateVictoryAmount(BetsModel $bet, float $amount = null) {
		if(!is_null($amount) && $bet->amount_win != $amount) {
			$bet->scenario = BetsModel::SCENARIO_UPDATE;
			$bet->amount_win = $amount;
			return $bet->save(true, ['amount_win']);
		}
		return true;
	}

	/**
	 * @param $params
	 * @return BetsModel
	 */
	protected function getBet($params) {
		$bet = BetsModel::findOne($params);
		if(!$bet) {
			throw new NotFoundHttpException();
		}
		return $bet;
	}
}

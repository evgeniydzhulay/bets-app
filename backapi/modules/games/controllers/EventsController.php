<?php

namespace backapi\modules\games\controllers;

use common\modules\games\models\EventGameModel;
use common\modules\games\models\EventOutcomesModel;
use yii\web\Controller;

class EventsController extends Controller {

	public function actionIndex() {
		$outcomes = EventGameModel::find()->select([
			'g' => EventGameModel::tableName() . '.id',
			'o' => EventOutcomesModel::tableName() . '.id',
			'r' => EventOutcomesModel::tableName() . '.rate',
		])->innerJoinWith(['eventOutcomes'], false)->andWhere([
			EventGameModel::tableName() . '.is_finished' => false,
			EventOutcomesModel::tableName() . '.is_finished' => false,
		])->asArray()->all();

		$result = [];
		foreach ($outcomes as $element) {
			if(!\array_key_exists($element['g'], $result)) {
				$result[$element['g']] = [];
			}
			$result[$element['g']][] = [
				'o' => +$element['o'],
				'r' => +$element['r'],
			];
		}
		\Yii::$app->response->content = json_encode($result);
		\Yii::$app->response->send();
	}
}
<?php

return [
	'modules' => [
		'games' => [
			'controllerNamespace' => '\backapi\modules\games\controllers',
		],
	],
];

<?php

namespace backapi\modules\support\controllers;

use common\modules\support\models\SupportMessageModel;
use common\modules\support\models\SupportRequestModel;
use yii\db\Expression;
use yii\web\Controller;

class RequestsController extends Controller {

	public function actionNew() {
		return [
			'countPending' => SupportRequestModel::find()->andWhere([
				SupportRequestModel::tableName() .'.operator_id' => null,
				SupportRequestModel::tableName() .'.is_closed' => false,
				SupportRequestModel::tableName() .'.is_new' => true,
			])->joinWith(['lastMessage'], false)->count()
		];
	}

	public function actionOperator(int $id) {
		$requests = SupportRequestModel::find()->andWhere([
			'is_closed' => false
		])->andWhere([
			'or', ['operator_id' => $id], ['operator_id' => NULL]
		])->select([
			SupportRequestModel::tableName() .'.id',
			SupportRequestModel::tableName() .'.key',
			SupportRequestModel::tableName() .'.user_id',
			SupportRequestModel::tableName() .'.email',
			SupportRequestModel::tableName() .'.phone',
			SupportRequestModel::tableName() .'.name',
			SupportRequestModel::tableName() .'.theme',
			SupportMessageModel::tableName() .'.message',
			SupportRequestModel::tableName() .'.created_at',
			SupportMessageModel::tableName() .'.from_id',
			SupportMessageModel::tableName() .'.from_email',
			SupportMessageModel::tableName() .'.from_phone',
			SupportMessageModel::tableName() .'.from_name',
			'message_at' => SupportMessageModel::tableName() .'.created_at',
		])->joinWith(['lastMessage'], false)->asArray()->all();
		foreach($requests AS &$request) {
			$request['id'] = +$request['id'];
			$request['created_at'] = date('Y-m-d H:i:s', $request['created_at']);
			$request['message_at'] = date('Y-m-d H:i:s', $request['message_at']);
		}
		return ['data' => \array_values($requests)];
	}
}
<?php


namespace backapi\modules\support\controllers;

use common\modules\support\models\SupportAttachmentModel;
use common\modules\support\models\SupportRequestModel;
use Yii;
use common\modules\support\models\SupportMessageModel;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class MessageController extends Controller {

	public function actionSend(int $request, $key, $uid) {
		$this->getRequest(['id' => $request, 'key' => $key]);
		$model = Yii::createObject(SupportMessageModel::class);
		$model->setAttributes([
			'request_id' => $request,
			'from_id' => !empty($uid) ? $uid : null,
			'message' => Yii::$app->request->post('message'),
			'from_email' => Yii::$app->request->post('email'),
			'from_phone' => Yii::$app->request->post('phone'),
			'from_name' => Yii::$app->request->post('name'),
		]);
		if($model->save()) {
			$filesList = [];
			$files = Yii::$app->request->post('files', []);
			foreach($files AS $fileID) {
				$file = SupportAttachmentModel::findOne(['id' => $fileID, 'request_id' => $request]);
				if($file) {
					$file->message_id = $model->id;
					if($file->save(false, ['message_id'])) {
						$filesList[] = [
							'url' => $file->getUrl(),
							'name' => $file->name
						];
					}
				}
			}
			return [
				'id' => +$model->id,
				'request_id' => +$model->request_id,
				'attachments_count' => count($model->attachments),
				'message' => $model->message,
				'from_id' => $model->from_id,
				'from_email' => $model->from_email,
				'from_phone' => $model->from_phone,
				'from_name' => $model->from_name,
				'files' => $filesList,
				'is_new' => true,
				'created_at' => date('Y-m-d H:i:s', $model->created_at),
			];
		}
		return false;
	}

	/**
	 * @param $params
	 * @return SupportRequestModel
	 */
	protected function getRequest($params) {
		$request = SupportRequestModel::find()->andWhere($params)->one();
		if(!$request) {
			throw new NotFoundHttpException();
		}
		return $request;
	}
}
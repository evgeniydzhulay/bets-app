<?php

return [
	'modules' => [
		'support' => [
			'controllerNamespace' => '\backapi\modules\support\controllers',
		],
	],
];

<?php

namespace backapi\modules\user\controllers;

use backapi\modules\games\services\BetsapiService;
use common\modules\games\models\EventGameModel;
use common\modules\user\models\UserModel;
use Yii;
use yii\db\Expression;
use yii\filters\auth\QueryParamAuth;
use yii\web\Controller;
use yii\filters\ContentNegotiator;
use yii\web\Response;
use yii\web\BadRequestHttpException;

class AuthController extends Controller {

	/** @inheritdoc */
	public function behaviors() {
		return [
			'authenticator' => [
				'class' => QueryParamAuth::class,
				'only' => ['login'],
				'tokenParam' => 'key'
			],
		];
	}

	/**
	 * @param int $uid
	 * @param string $key
	 * @return array
	 */
	public function actionLogin(int $uid, string $key) {
		/** @var $user UserModel */
		$user = Yii::$app->user->identity;
		return [
			'success' => true,
			'id' => +$user->id,
			'purse' => [
				'amount' => +$user->purse_amount,
				'currency' => $user->purse_currency,
			],
		];
	}
}

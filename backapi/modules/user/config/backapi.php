<?php

use backapi\modules\user\controllers\AuthController;
use backapi\modules\user\models\UserAuthModel;

return [
	'modules' => [
		'user' => [
			'controllerMap' => [
				'auth' => AuthController::class,
			]
		],
	],
	'components' => [
		'user' => [
			'enableSession' => false,
			'enableAutoLogin' => false,
			'identityClass' => UserAuthModel::class,
		],
	],
];

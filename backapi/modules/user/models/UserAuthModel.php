<?php

namespace backapi\modules\user\models;

use common\modules\user\models\UserApplicationModel;
use common\modules\user\models\UserModel;
use yii\web\User;

class UserAuthModel extends UserModel {

	/** @inheritdoc */
	public static function findIdentityByAccessToken($token, $type = null) {
		$tokenModel = UserApplicationModel::find()->andWhere([
			'token' => $token, 'app_type' => UserApplicationModel::TYPE_WEB
		])->andWhere(['>', 'ended_at', time()])->limit(1)->one();

		if($tokenModel && ($user = self::findOne(['id' => $tokenModel->user_id]))) {
			$tokenModel->trigger(User::EVENT_AFTER_LOGIN);
			return $user;
		}
		return false;
	}
}